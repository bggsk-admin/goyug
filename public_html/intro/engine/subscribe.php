<?php session_start();

	/* ---------------------------------------------------
	 * SUBSCRIBE.PHP - Subbscribe users on your newsletter
	 * ---------------------------------------------------
	 *
	 * The script can work in two modes: MailChimp and Email
	 * MailChimp: The script saves emails to the MailChimp's list (http://mailchimp.com) - RECOMMEND
	 * File: The script saves all emails to the file on your server
	 * Email [default]: The script sends emails on your email address
	 *
	 */
	
	require_once 'config.php';
	require_once 'functions.php';
	
	// Some checks
	if ( in_array($_POST['email'], $_SESSION['emails'] ) ) {
		echo response( 0, 'Email was already used' );
		exit;
	}
	
	global $email;
		   $email = $_POST['email'];
		   
	if ( false == email_check( $email ) ) {
		echo response(0, 'Email address is invalid');
		exit;
	}
	
	// Choose mode
	switch ( conf('subscribe-type') ) {
		case 'file':
			echo filesave();
			break;
			
		case 'mailchimp': 
			echo mailchimp(); 
			break;
			
		default: echo email();
	}
	
	# File Save
	function filesave() {
		global $email;
			
		if ( true === conf('file-uniq') ) {
			
			$file = file( conf('file-path') );
			$email_repeat = false;
			
			foreach( $file as $index => $file_email ) {
				
				if ( $email == $file_email ) {
					$email_repeat = true;
					break;
				}
			}	
			
			if ( true === $email_repeat ) {
				return response( 0, 'Email was already used' );
			}
			
			$_SESSION['emails'][] = $email;
		}
		
		return response( file_put_contents( conf('file-path'), $email . "\n", FILE_APPEND ) );
	}
	
	# MailChimp [http://mailchimp.com]
	function mailchimp() {
	
		global $email;

		include('mailchimp/api.php');
		
		$MailChimp = new \Drewm\MailChimp( conf('mailchimp-api') );
		
		$result = $MailChimp->call( 'lists/subscribe', array(
                'id'                => conf('mailchimp-list-id'),
                'email'             => array( 'email' => $email ),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false
            ));
        
        $_SESSION['emails'][] = $email;
		return response( 1, $result );
	}
	 
	# Simple mail function
	function email() {
		global $email;
		
		$r = @mail( conf('email-address'), conf('email-subject'), $email, conf('email-from') );
		
		if ( true == $r ) {
			$_SESSION['emails'][] = $email;	
			return response( 1 );
		} 
		
		return response( 0, 'Something is wrong' );
	}
