<?php

	/* ------------------------------------------
	 * FEEDBACK.PHP - Send user's messages to you
	 * ------------------------------------------
	 */
		
	require_once 'config.php';
	require_once 'functions.php';
	

	// Receive all data
	$user_name 		= $_POST["name"];
	$user_email 		= $_POST["email"];
	$user_message 	= $_POST["message"];
	$user_ip 		= $_SERVER["REMOTE_ADDR"];
	$blacklist = array('207.204.227.234');
	
	// Some security
	htmlspecialchars( $user_name );
	htmlspecialchars( $user_email );
	htmlspecialchars( $user_message );
	
	# ========== CONFIGURATION ==========

	$admin_email	= conf('feedback-address');
	$admin_subject 	= conf('feedback-subject-admin');
	$user_subject	= conf('feedback-subject-user');
	
	$admin_email_template = conf('feedback-template-admin');
	$user_email_template  = conf('feedback-template-user');
	
	# ======== END CONFIGURATION ========
	
	// Mail to the best customer in the world <3
	// $admin_headers  = "From: " . $user_email . "\r\n";
	// $admin_headers .= "Reply-To: ". $user_email . "\r\n";

	$admin_headers  = "From: " . $admin_email . "\r\n";
	$admin_headers .= "Reply-To: ". $admin_email . "\r\n";
	$admin_headers .= "MIME-Version: 1.0\r\n";
	$admin_headers .= "Content-Type: text/html; charset=utf-8\r\n";
	
	ob_start();
	require $admin_email_template;
	$to_admin = ob_get_clean();

	// Отключаем фидбек насовсем (из-за спама) 22.09.2020		
	// if(!in_array($user_ip, $blacklist))
	// 	$admin_success = @mail( $admin_email, $admin_subject, $to_admin, $admin_headers );
	
	// Thanksgiving
	$user_headers  = "From: " . $admin_email . "\r\n";
	$user_headers .= "Reply-To: ". $admin_email . "\r\n";
	$user_headers .= "MIME-Version: 1.0\r\n";
	$user_headers .= "Content-Type: text/html; charset=utf-8\r\n";
	
	ob_start();
	require $user_email_template;
	$to_user = ob_get_clean();
	
	// Отключаем фидбек насовсем (из-за спама) 22.09.2020		
	// if(!in_array($user_ip, $blacklist))
	// 	$user_success = @mail( $user_email, $user_subject, $to_user, $user_headers );
	
	// Responce
	if ( true == $admin_success && true == $user_success ) {
		echo 1;
	} else {
		echo 0;
	}