/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {


    /* Float Labels
    **************************************************/
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });



    $(function() {
        return $(".modal").on("show.bs.modal", function() {
            var curModal;
            curModal = this;
            $(".modal").each(function() {
                if (this !== curModal) {
                    $(this).modal("hide");
                }
            });
        });
    });

    /* blur on modal open, unblur on close */
    $('#modal-select-city').on('show.bs.modal', function () {
       $('#home, header').addClass('blur');
    });

    $('#modal-select-city').on('hide.bs.modal', function () {
       $('#home, header').removeClass('blur');
    });

    $( "#f_city_name" ).focusin(function() {
        $('#modal-select-city').modal('show');
    });

    $('.city-selector').click(function(e){
        e.preventDefault();
        var uid = $(this).attr('data-uid');
        var name = $(this).attr('data-name');

        console.log(uid);

        $.ajax(
        {
            url: '/objects/ajax/',
            type: 'get',
            data: {action: "setcity", uid: uid, name: name},
            dataType: 'json',
            async: false,
            cache: false,
            success: function(data)
            {
                result = data;
            }
        });

        $("#f_city_name").val(name);
        $("#f_city").val(uid);
        $("#f_city_topmenu span").text(name);

        $('#modal-select-city').modal('hide');

        $('html, body').animate({ scrollTop: 0 }, 1500);

    });


    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $('form[data-async]').submit(function(event) {
        var $form = $(this);
        var $btn = $(this).children("button[type='submit']");

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (settings.context != undefined && settings.context.hasClass('btn')) {
                    settings.context.button('loading');
                }
            },
            complete: function() {
                this.button('reset');
            }
        });

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),

            success: function(data, status) {
                if (data.redirect) {
                    window.location.href = data.redirect;
                }
                if (data.error) {
                    $(data.error.elements)
                        .data("title", data.error.text)
                        .tooltip({
                            placement: 'right',
                            trigger: 'manual',
                            show: '100'
                        }).tooltip('show');

                    $(data.error.elements).closest('.form-group').removeClass('has-success').addClass('has-error');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $btn.button('reset');
            }

        });


        event.preventDefault();
    });

}); //JQuery END


//Активация кнопки регистрации
$(document).ready(function(){
    $('#register-privacy').on('click', function(e){

        if (e.target.checked) {
            $('#btn-register').attr('disabled', false);
            $(e.target.parentNode.parentNode).addClass('accept');
        } else {
            $('#btn-register').attr('disabled', true);
            $(e.target.parentNode.parentNode).removeClass('accept');
        }
    });
});


/***************************************************************************
Pure JS
****************************************************************************/
