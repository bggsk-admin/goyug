/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    (function() {

        var Parser = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            startBtn: '#startParse',
            progressBar: '#progress .progress-bar',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                this.bindEvents();

            },
            // Init - END

            //*******************************************************************************
            //   bindEvents
            //*******************************************************************************
            bindEvents: function() {
                
                var self = this;

                $(document).on('click', self.startBtn, function(event) {
                    event.preventDefault();

                    self.pars_city = $('#pars_city').val();
                    self.pars_page = $('#pars_page').val();

                    self.startParse(self.pars_city, self.pars_page);


                    // var start = Date.now(); 
                    // var timerId = setTimeout(function tick() {
                    //     timerId = setTimeout(function() {
                    //         var timePassed = Date.now() - start;
                    //         console.log(timePassed);

                    //         if (timePassed >= 8000) {
                    //             clearInterval(timer);
                    //             return;
                    //         }

                    //     }, 2000);
                    // }, 2000);


/*                    var i = 1;
                    var start = Date.now(); 
                    this.timer = setTimeout(function run() {
                      
                      var timePassed = Date.now() - start;
                      
                        // console.log(self.getCallback(token));

                        var r = self.checkParseJobStatus(token, function(response) {
                            console.log(response);
                        });
                        // console.log(r);

                        if (timePassed >= 5000) {
                        clearTimeout(self.timer);
                        return;
                        }
                                                  
                      setTimeout(run, 1000);
                    }, 1000);
*/
                    // var timer = setInterval(function() {
                    //   var timePassed = Date.now() - start;

                    //   if (timePassed >= 5000) {
                    //     clearInterval(timer);
                    //     return;
                    //   }

                    //     self.checkParseJobStatus(token, self.getCallback);

                    // }, 1000);


                });

            },
            // bindEvents - END

            //*******************************************************************************
            //   startParse
            //*******************************************************************************
            startParse: function(pars_city, pars_page) {

                var self = this;

                $(self.startBtn).button('loading');
                $('#loading-indicator').show();
                $('#parsing-results').hide();

                $.ajax(
                {
                    url: '/ai/parser/ajax',
                    type: 'get',
                    data: {method: "startparse", pars_city: pars_city, pars_page: pars_page},
                    dataType: 'json',
                    // async: false,
                }).done(function( data ) {

                    var d = data.response;

                    $('#pr-city').text(d.pt_city);
                    $('#pr-all-pages').text(d.pt_all_pages);
                    $('#pr-all-obj').text(d.pt_all_obj);
                    $('#pr-pages').text( parseInt(d.pt_cur_page) - parseInt(d.pt_start_page) + 1 );
                    $('#pr-cur-page').text( parseInt(d.pt_cur_page) );
                    $('#pr-cur-obj').text( d.pt_cur_obj );

                    $('#pr-start').text(d.pt_start);
                    $('#pr-end').text(d.pt_end);

                    $(self.startBtn).button('reset');
                    $('#loading-indicator').hide();
                    $('#parsing-results').show();

                });

            },

            //*******************************************************************************
            //   startParse
            //*******************************************************************************
            getCallback: function(result) {
                console.log('call');
                return result;
            },

            //*******************************************************************************
            //   checkStatParse
            //*******************************************************************************
            checkParseJobStatus: function(token, callback) {

                // $.ajax(
                // {
                //     url: '/ai/parser/ajax',
                //     type: 'get',
                //     data: {method: "checkparse", token: token},
                //     dataType: 'json',
                //     success: function(response) {
                //         callback(response);
                //     },
                // });

                jQuery.get('/ai/parser/ajax', { method: "checkparse", token: token }, function (callback) {
                    // use response here; jQuery passes it as the first parameter
                    // callback(response);
                    console.log(callback);
                });

                        // result = data.status;
                        // callback(status);
                        // $(self.progressBar).css("width", status + "%");

                // asyncTasks.push(function(_callback){
                //     $.ajax({
                //         url: '/ai/parser/ajax',
                //         type: 'get',
                //         data: {method: "checkparse", token: token},
                //         dataType: 'json',
                //         success: function(response) {
                //             result.response = response;
                //             _callback();
                //         }
                //     });
                // });

                // async.parallel(asyncTasks, function(){
                //     console.log(result);
                //     console.log('Done');
                // });

            },

            //*******************************************************************************
            //   startParse
            //*******************************************************************************
            getToken: function() {

                var token = null;

                $.ajax(
                {
                    context: $(self.startBtn),
                    url: '/ai/parser/ajax',
                    type: 'get',
                    data: {method: "gettoken"},
                    dataType: 'json',
                    async: false,
                    cache: false,                    
                    success: function(data){
                        token = data.token;
                    },
                });
                
                return token;
            },




        };

        Parser.init();

    })();

});