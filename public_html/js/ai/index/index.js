/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    var main_form = $("#main-form");

    $( "#stat_date_from, #stat_date_to" ).datepicker({
          numberOfMonths: 1,
          dateFormat: "dd.mm.yy",
          showOtherMonths: true,
          selectOtherMonths: true,
          changeMonth: true,
          changeYear: true
        });

    $( ".alert" ).hide();

    jQuery( document ).ajaxComplete( function($) {
        jQuery( "#stat-table" ).tablesorter();
    });

    var validobj = main_form.validate({
        rules: {
            "stat_date_from": {
                required: true,
            },
            stat_date_to: {
                required: true,
            }
        },
        messages: {
            "stat_date_from": {
                required: "Укажите дату",
            },
            stat_date_to: {
                required: "Укажите дату",
            }
        },
        showErrors: function(errorMap, errorList) {

            $.each(this.validElements(), function(index, element) {
                var $element = $(element);
                $element.data("title", "").tooltip("destroy");
                $element.closest('.form-group').removeClass('has-error').addClass('has-success');
            });

            $.each(errorList, function(index, error) {
                var $element = $(error.element);

                $element.tooltip("destroy")
                    .data("title", error.message)
                    .tooltip({
                        placement: 'top',
                        trigger: 'manual',
                        show: '100'
                    }).tooltip('show');

                $element.closest('.form-group').removeClass('has-success').addClass('has-error');

            });

        },
    });


    (function() {

        var StatGenerator = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            startBtn: '#startStatGen',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                this.bindEvents();

            },
            // Init - END

            //*******************************************************************************
            //   bindEvents
            //*******************************************************************************
            bindEvents: function() {
                
                var self = this;
    			var statGenParams = {};
                var currentDataActions = ["emails", "abonements_now", "objects", "ontop_real", "abonements_all_in_city"];

                $(document).on('click', self.startBtn, function(event) {
                    event.preventDefault();

                    statGenParams.stat_action = $('#stat_action').val();
                    statGenParams.stat_city = $('#stat_city').val();
                    statGenParams.stat_date_from = $('#stat_date_from').val();
                    statGenParams.stat_date_to = $('#stat_date_to').val();

                    if(validobj.form()) {
                        self.statGen(statGenParams); 
                    } else {
                        return false;
                    }

                });

                $( "#stat_action" ).change(function() {

                    if( currentDataActions.indexOf($( this ).val()) >= 0) {

                        $("#stat_date_from, #stat_date_to").attr('disabled','disabled');
                        $("#stat_date_from, #stat_date_to").val('');
                        $( "#no-dates-required" ).show();

                    } else {

                        $("#stat_date_from, #stat_date_to").removeAttr('disabled');
                        $( "#no-dates-required" ).hide();

                    }

                });



            },
            // bindEvents - END

            //*******************************************************************************
            //   statGen
            //*******************************************************************************
            statGen: function(statGenParams) {

                var self = this;
                var stat_params = statGenParams;

                $(self.startBtn).button('loading');
                $('#loading-indicator').show();
                $('#stat-result').hide();

                $.ajax(
                {
                    url: '/ai/stat/ajax',
                    type: 'get',
                    data: {
                    	method: "statgen",
                    	stat_params: stat_params
                    },
                    dataType: 'json',
                    // async: false,
                }).done(function( data ) {

                    var d = data.response;

                    $('#stat-result').html(d);

                    $(self.startBtn).button('reset');
                    $('#loading-indicator').hide();
                    $('#stat-result').show();

                });

            },

        };
        StatGenerator.init();
    })();
});