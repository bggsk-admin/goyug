$(document).ready(function() {

  /*------------------------------------------------------------------------------------
  /* GoogleMap Search Address
  --------------------------------------------------------------------------------------*/

  //Prevent 'enter' key press when in search input
  $("#search_address").focus(function() {
    $(this).keypress(function(event) {
      if (event.keyCode == 13) {
        event.preventDefault();
      }
    });
  });

  $("#search_address").click(function() {
    $('html,body').animate({
      scrollTop: $("#search_address").offset().top - 160
    }, 'slow');
  });

  function gmap_initialize() {

    var init_lng = $('#obj_addr_lon').val().length ? $('#obj_addr_lon').val() : 38.97871465;
    var init_lat = $('#obj_addr_lat').val().length ? $('#obj_addr_lat').val() : 45.05047394;

    var defPos = new google.maps.LatLng(init_lat, init_lng);

    var mapOptions = {
      center: defPos,
      zoom: 15
    };

    var map = new google.maps.Map(document.getElementById('googlemap'),
      mapOptions);

    var input = document.getElementById('search_address');

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
      position: defPos,
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)

    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {

      infowindow.close();
      marker.setVisible(false);

      var place = autocomplete.getPlace();

      console.log(place);

      if (!place.geometry) {
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17); // Why 17? Because it looks good.
      }
      marker.setIcon( /** @type {google.maps.Icon} */ ({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
      }));
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      var addr_arr = place.address_components;

      var addr_country_code = extractFromAdress(addr_arr, "country", "short");
      var addr_country = extractFromAdress(addr_arr, "country");
      var addr_zip = extractFromAdress(addr_arr, "postal_code");
      var addr_city = extractFromAdress(addr_arr, "locality");
      var addr_street = extractFromAdress(addr_arr, "route");
      var addr_number = extractFromAdress(addr_arr, "street_number");
      var addr_area_1 = extractFromAdress(addr_arr, "administrative_area_level_1");
      var addr_area_2 = extractFromAdress(addr_arr, "administrative_area_level_2");

      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');

      }

      infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
      infowindow.open(map, marker);

      $('#obj_addr_country_code').val(addr_country_code);
      $('#obj_addr_country').val(addr_country);
      $('#obj_addr_zip').val(addr_zip);
      $('#obj_addr_city').val(addr_city);
      $('#obj_addr_street').val(addr_street);
      $('#obj_addr_number').val(addr_number);

      $('#obj_addr_area_1').val(addr_area_1);
      $('#obj_addr_area_2').val(addr_area_2);

      $('#obj_addr_lon').val(place.geometry.location.lng());
      $('#obj_addr_lat').val(place.geometry.location.lat());
      $('#obj_addr_lon_view').val(place.geometry.location.lng());
      $('#obj_addr_lat_view').val(place.geometry.location.lat());

      //Автозаполнение названия объекта
      // $('#obj_name_ru').val(addr_street + ' ' + addr_number).change();

      $('html,body').animate({
        scrollTop: $("#main-form").offset().top - 200
      }, 'slow');

      $("#fs_coordinates, #fs_address").stop().css("background-color", "#FFFF9C").animate({
        backgroundColor: "#FFFFFF"
      }, 1000);



    });

  }

  function extractFromAdress(components, type, size) {
    for (var i = 0; i < components.length; i++)
      for (var j = 0; j < components[i].types.length; j++)
        if (components[i].types[j] == type)
          if (size == "short") {
            return components[i].short_name;
          } else {
            return components[i].long_name;
          }
    return "";
  }

  google.maps.event.addDomListener(window, 'load', gmap_initialize);

  //Init Map after "Address" tab shown
   $("input[data-target='#tab-geo']").on('shown.bs.tab', function(e) {
      gmap_initialize();
   });


  //-------------------------------------------------- Booking Calendar Pro -----------------------------------/
  /*  $('#booking-calendar').DOPBackendBookingCalendarPRO({
    'DataURL': '/ai/object/ajax/method/loadcalendar',
    'SaveURL': 'dopbcp/php-database/save.php',
    'DayNames': ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    'MonthNames': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    'CloseLabel': 'Закрыть',
    'BookedText': 'Бронь',
    'DateType': 2,

  });
  */

  /*------------------------------------------------------------------------------------
  /* Bootstrap Datetimepicker
  --------------------------------------------------------------------------------------*/

  $('#pick_cal_startdate').datetimepicker({
    language: 'ru',
    defaultDate: moment().format('L') + " 14:00",
  });

  $('#pick_cal_enddate').datetimepicker({
    language: 'ru',
    defaultDate: moment().add(3, 'days').format('L') + " 12:00",
  });

  /*------------------------------------------------------------------------------------
  /* FullCalendar init
  --------------------------------------------------------------------------------------*/

  $("#event-info").hide();
  var bookCal = $('#booking-calendar');

  bookCal.fullCalendar({

    header: {
      left: 'prev next today',
      center: 'title',
      right: 'prevYear nextYear'
    },
    buttonText: {
      today: 'Сегодня',
      month: 'month',
      week: 'week',
      day: 'day',
    },
    timezone: "UTC",

    firstDay: 1,
    contentHeight: 300,
    selectable: true,
    selectHelper: true,

    select: function(start, end) {
      $('#modal-add-event').modal('show');

      var cal_startdate = moment(start).format("DD.MM.YYYY 14:00");
      var cal_enddate = moment(end).format("DD.MM.YYYY 12:00");

      $('#cal_startdate').val(cal_startdate);
      $('#cal_enddate').val(cal_enddate);
    },
    eventClick: function(calEvent, jsEvent, view) {

      $("#event-info-id").val(calEvent.id);
      $("#event-info-title").text(calEvent.title);

      var startdate = moment(calEvent.start);
      var startdate_label = startdate.format("DD MMMM YYYY HH:mm (ddd)");
      $("#event-info-startdate-label").text(startdate_label);

      var enddate = moment(calEvent.end);
      var enddate_label = enddate.format("DD MMMM YYYY HH:mm (ddd)");
      $("#event-info-enddate-label").text(enddate_label);

      var diff_days = Math.round(enddate.diff(startdate, 'days', true));
      $("#event-info-days").text(diff_days);

      var diff_days_label = decOfNum(diff_days, ['день', 'дня', 'дней']);
      $("#event-info-days-label").text(diff_days_label);

      $("#event-info").show();

    },
    eventAfterAllRender: function( view ) {
      console.log("done");
    },

    editable: true,

    events: {

      url: '/ai/object/ajax/method/loadcalendar/',
      error: function() {
        $('#cal-warning').show();
      },
      success: function(data, status) {
        $('#cal-warning').hide();

        $.each(data.data, function(index, event) {
          bookCal.fullCalendar('renderEvent', event);
        });
      },
      data: function() {
        return {
          id: $('#obj_id').val()
        };
      },
    },

  });

  /*------------------------------------------------------------------------------------
  /* Calendar Events Table (list view)
  --------------------------------------------------------------------------------------*/
  var calendar_list = $('#calendar-list').dataTable({

    "ajax": {
      "url": "/ai/object/ajax/method/loadeventstable/",
      "type": "POST",
      "data": {
        id: $('#obj_id').val()
      },
    },

    "sDom": "<'row'<'col-xs-4'i><'col-xs-4'f><'col-xs-4'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
    "oLanguage": {
      "sLengthMenu": "_MENU_ записей на страницу "
    },
    "oLanguage": {
      "sUrl": "/js/dataTables.russian.txt"
    },
    "iDisplayLength": 50,

    "order": [
      // [11, "DESC"] //Default Order by obj_update
    ],

  });

  /*------------------------------------------------------------------------------------
  /* FullCalendar - Apply action
  --------------------------------------------------------------------------------------*/
  $("#btn-cal-apply").click(function(event) {

    var $btn = $(this);
    $btn.button('loading');

    var events = bookCal.fullCalendar('clientEvents');

    $.ajax({
      dataType: 'json',
      cache: false,
      type: 'post',
      url: '/ai/object/ajax/method/savecalendar/',
      data: "json=" + JSON.stringify(events) + "&id=" + $('#obj_id').val(),

    });

    $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
      backgroundColor: "#FFFFFF"
    }, 1000);


    $btn.button('reset');

    event.preventDefault();
  });

  /*------------------------------------------------------------------------------------
  /* FullCalendar - Reload action
  --------------------------------------------------------------------------------------*/
  $("#btn-cal-reload").click(function(event) {

    event.preventDefault();

    bookCal.fullCalendar("removeEvents");
    bookCal.fullCalendar("refetchEvents");

    $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
      backgroundColor: "#FFFFFF"
    }, 1000);

    // $('#calendar-list').dataTable().ajax.reload();
  });

  /*------------------------------------------------------------------------------------
  /* FullCalendar - Delete Event action
  --------------------------------------------------------------------------------------*/
  $("#modal-delete-event .btn-danger").click(function(event) {

    var eventID = parseInt($("#event-info-id").val());

    bookCal.fullCalendar('removeEvents', [eventID]);


    //------------- Apply Events ------------------------------
    var events = bookCal.fullCalendar('clientEvents');
    $.ajax({
      dataType: 'json',
      cache: false,
      type: 'post',
      url: '/ai/object/ajax/method/savecalendar/',
      data: "json=" + JSON.stringify(events) + "&id=" + $('#obj_id').val(),

    });
    //------------- Apply Events - END  ------------------------------

    $("#event-info").hide();

    $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
      backgroundColor: "#FFFFFF"
    }, 1000);



    $('#modal-delete-event').modal('hide');
    event.preventDefault();
  });

  /*------------------------------------------------------------------------------------
  /* FullCalendar - Add Event action
  --------------------------------------------------------------------------------------*/
  $("#modal-add-event .btn-primary").click(function(event) {

    var cal_startdate = $('#cal_startdate').val();
    cal_startdate = moment(cal_startdate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");

    var cal_enddate = $('#cal_enddate').val();
    cal_enddate = moment(cal_enddate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");

    var cal_status = $('#cal_status').val();
    var eventData = "";

    var cal_title = "";
    if (cal_status == "booked") cal_title += "Объект забронирован";
    if (cal_status == "closed") cal_title += "Объект недоступен";

    eventData = {
      id: Math.random(),
      title: cal_title,
      start: cal_startdate,
      end: cal_enddate,
      status: cal_status,
      type: "event",
      className: "event-" + cal_status
    };

    bookCal.fullCalendar('renderEvent', eventData, false); // stick? = true
    bookCal.fullCalendar('unselect');

    $('#modal-add-event').modal('hide');

    $("#fs_calendar").stop().css("background-color", "#FFFF9C").animate({
      backgroundColor: "#FFFFFF"
    }, 1000);
  });

  //-------------------------------------------------- Show First Tab (Geneal) & Calendar Tab -----------------------/
  $('#subnavbar-tabs input:first').tab('show');
  $('#subnavbar-tabs label:first').toggleClass('active');
  $('#calendar-view-tabs label:first').toggleClass('active');

  //-------------------------------------------------- Send Ajax Form -----------------------------------/
  $(".btn-apply").click(function(event) {
    var $form = $('form[data-async]');
    var $btn = $(this);

    $.ajax({
      context: $btn,
      dataType: 'json',
      cache: false,
      type: $form.attr('method'),
      url: '/ai/object/ajax',
      data: $form.serialize(),

      success: function(data, status) {
        if (data.error) {
          //$(data.error.elements)
          //.data("title", data.error.text)
          //.tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');

          //$(data.error.elements).closest('.form-group').removeClass('has-success').addClass('has-error');
        }

        var form = data.form;

        //Update object-name in infobar
        $('#info_obj_name_ru').text(form.obj_name_ru);

        $('#info_obj_uid').text(form.obj_uid);

        //Update address in infobar
        var addr1 = "",
          addr2 = "";
        addr1 += (form.obj_addr_street) ? form.obj_addr_street + " " : "";
        addr1 += (form.obj_addr_number) ? form.obj_addr_number : "";
        $('#info_addr_1').text(addr1);

        addr2 += (form.obj_addr_entrance) ? "пд. " + form.obj_addr_entrance + " " : "";
        addr2 += (form.obj_addr_floor) ? "эт. " + form.obj_addr_floor + " " : "";
        addr2 += (form.obj_addr_room) ? "кв. " + form.obj_addr_room + " " : "";
        $('#info_addr_2').text(addr2);

        //Update dates in infobar
        $('#info_obj_update').text(form.obj_update);

        $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
          backgroundColor: "#F7F7F9"
        }, 500);

        $("fieldset").stop().css("background-color", "#FFFF9C").animate({
          backgroundColor: "#FFF"
        }, 500);

      },
      error: function(xhr, ajaxOptions, thrownError) {
        $btn.button('reset');
      }

    });

    event.preventDefault();
  });

  //------------- Get smartfilters By Object Type ID ------------------//
  $.ajax({
    type: 'POST',
    dataType: 'json',
    url: '/ai/object/ajax',
    data: {
      method: "getsmartsbyobjtype",
      objtypeid: $('#ot_object_type').val()
    },

    success: function(data, status) {
      if (data.error) {} else {

        //------------------- Update sf_smartfilters select2 -------------------
        var sf_smartfilters;

        if (data.objsmarts.length > 0) {
          $.each(data.objsmarts, function(index, value) {
            sf_smartfilters += '<option value="' + data.objsmarts[index].id + '">' + data.objsmarts[index].val + '</option>';
          });
          // $('#sf_smartfilters').html(sf_smartfilters);
        } else {
          $('#sf_smartfilters').html("");
        }

      }
    },
    error: function(xhr, ajaxOptions, thrownError) {}
  });

  //Выставляем значения "все апартаменты" или "все квартиры" в зависимости от типа поля
  //Апартамент
  // if ($('#ot_object_type').val() == "9") $("#sf_smartfilters").select2("val", "11");
  //Квартира
  // if ($('#ot_object_type').val() == "2") $("#sf_smartfilters").select2("val", "20");


  //-------------------------------------------------- Fileupload -----------------------------------/

  $(function() {
    'use strict';

    $('#fileupload .fileupload-actions').hide();

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        url: '/ai/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
      })
      .bind('fileuploadadd', function(e, data) {
        $('#fileupload .fileupload-actions').show({
          duration: 500
        });
      });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
      'option',
      'redirect',
      window.location.href.replace(
        /\/[^\/]*$/,
        '/cors/result.html?%s'
      )
    );

    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');

    $.ajax({
      url: $('#fileupload').fileupload('option', 'url'),
      dataType: 'json',
      context: $('#fileupload')[0]
    }).always(function() {
      $(this).removeClass('fileupload-processing');
    }).done(function(result) {
      $(this).fileupload('option', 'done')
        .call(this, $.Event('done'), {
          result: result
        });
    });
  });

  //-------------------------------------------------- Сортировка картинок -----------------------------------/
  $(function() {
    $('#sortable').sortable({
      placeholder: 'ui-state-highlight',
      cursor: 'move',
      update: function(e, ui) {
        var element_id = ui.item.context.id.replace('tr-', '');
        var newOrderSrc = $('#sortable').sortable('toArray');
        var newOrder = [];
        $.map(newOrderSrc, function(e, i){
          newOrder.push(e.replace('tr-', ''));
        });
        //Отправка на сервер
        $.ajax({
          type: 'GET',
          url: '/ai/files/order/',
          data: 'order='+JSON.stringify(newOrder),
          error: function(){ console.log('Ошибка!'); },
          success: function(data){ }
        });
      }
    });
    $('#sortable').disableSelection();
  });

});
//Document Ready - END

// ******************************************** Dec of Num ********************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
function decOfNum(number, titles) {
  cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
