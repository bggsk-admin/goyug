/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};




$(document).ready(function() {

    //-------------------------------------------------- Hide Filterbar -----------------------------------/
    $(document).on('click', '.subnavbar-filter-toggle', function(event) {
        event.preventDefault();

        $(this).button('toggle');
        $(".subnavbar-filter").slideToggle();
    });

    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-rebuild-action").click(function(event) {

        var $btn = $(this);
        $btn.button('loading');


        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: "POST",
            url: '/ai/object/ajax',
            data: {
                method: "rebuild"
            },

            success: function(data, status) {

                $(".navbar-subnavbar, .navbar-breadcrumb, .breadcrumb").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#F4F3ED"
                }, 500);

                $btn.button('reset');
            },

            error: function(xhr, ajaxOptions, thrownError) {
                new PNotify({
                    title: "Over Here",
                    text: thrownError,
                    type: "error",
                    // addclass: "stack-bar-top",
                    cornerclass: "",
                    width: "50%",
                    // stack: stack_bar_top
                });
                $btn.button('reset');
            }

        });

        event.preventDefault();
    });

    var tableHeight = $(window).height() - 450;

    var datatable = $('#watable').dataTable({

        dom: "<'row'<'col-xs-12'f>><'row'<'col-xs-6'l><'col-xs-6'C>><'row'<'col-xs-4'i><'col-xs-8'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        stateSave: true,
        "colVis": {
            "buttonText": "Скрыть / Показать колонки",
            exclude: [0, 1, 2, 3, 14],
            restore: "Восстановить",
            showAll: "Показать все",
            overlayFade: 0,
        },
        "oLanguage": {
            "sLengthMenu": "_MENU_ записей на страницу ",
        },
        "order": [
            [11, "DESC"] //Default Order by obj_update
        ],
        // "bAutoWidth": false,
        "aoColumnDefs": [{
            "aTargets": [0, 1, 2, 3],
            "bSortable": false
        }, {
            "aTargets": [6, 7, 8, 10, 13, 14, 15, 16, 17, 18],
            "bVisible": false
        }, {
            "aTargets": 15,
            "iDataSort": 14, // Hidden field: obj_update
        }],
        "oLanguage": {
            "sUrl": "/js/dataTables.russian.txt"
        },
        "iDisplayLength": 50,

        "scrollY": tableHeight + "px",
        // "scrollCollapse": true,        

        initComplete: function () 
        {
            var api = this.api();
            var searchState = JSON.parse( localStorage.getItem('DataTables_watable_' + window.location.pathname) ).columns;

            // FIlter Columns
            var filterColumns = [5, 6, 7, 8, 9, 12, 13, 16, 17, 18];

            $.each(filterColumns, function(ind, el) 
            {
                var col = api.column( el );
                var filterVal = searchState[el].search.search;

                col.data().unique().sort().each( function ( d, j ) {
                    $("#col_" + el + "_filter").append( '<option value="'+d+'">'+d+'</option>' );
                } );            

                if(filterVal !== "")
                {
                    $('#col_'+el+'_filter').val(filterVal);
                    console.log(filterVal);
                }

            });


            $('.column_filter').on( 'keyup click change', function () {

                var column = api.column( $(this).attr('data-column') );
/*                var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                );
*/
                column.search( $(this).val() ).draw();
            });



        },
    });

    $(".subnavbar-filter").hide();

    $('.content').on( 'click', function () {
        $(".subnavbar-filter-toggle").button('toggle');
        $(".subnavbar-filter").slideUp();
    } );


});
