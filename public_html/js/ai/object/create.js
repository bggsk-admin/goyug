$(document).ready(function() {

    var main_form = $("#main-form");

    //** Hide infobar ********************************************************/
    $('.navbar-infobar').hide();
    $('body').addClass('no-infobar');

    /******************************************** Select2 *************************************/
    /*        $(".select2").select2({
                allowClear: true
        })
                .rules('add', 'required');
*/

    /******************************************** Cjeck default checkboxes *************************************/
/*    $('#subnavbar-tabs input[data-target="#tab-attributes"]').tab('show');
    $('#rst_restriction-1').attr('checked','checked');
    $('#rst_restriction-1').prettyCheckable('check');    
    alert($('#rst_restriction-1').attr('checked'));
*/




    /*------------------------------------------------------------------------------------
    /* GoogleMap Search Address
    --------------------------------------------------------------------------------------*/
    
    //Prevent 'enter' key press when in search input
    $("#search_address").focus(function() {
        $(this).keypress(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    });

    $("#search_address").click(function() {
        $('html,body').animate({
            scrollTop: $("#search_address").offset().top - 160
        }, 'slow');
    });

    function gmap_initialize() {

        var init_lng = $('#obj_addr_lon').val().length ? $('#obj_addr_lon').val() : 38.97871465;
        var init_lat = $('#obj_addr_lat').val().length ? $('#obj_addr_lat').val() : 45.05047394;

        var defPos = new google.maps.LatLng(init_lat, init_lng);

        var mapOptions = {
            center: defPos,
            zoom: 15
        };

        var map = new google.maps.Map(document.getElementById('googlemap'),
            mapOptions);

        var input = document.getElementById('search_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        
        var marker = new google.maps.Marker({
            position: defPos,
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)

        });

        google.maps.event.addListener(autocomplete, 'place_changed', function() {

            infowindow.close();
            marker.setVisible(false);

            var place = autocomplete.getPlace();

            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setIcon( /** @type {google.maps.Icon} */ ({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            var addr_arr = place.address_components;

            var addr_country_code = extractFromAdress(addr_arr, "country", "short");
            var addr_country = extractFromAdress(addr_arr, "country");
            var addr_zip = extractFromAdress(addr_arr, "postal_code");
            var addr_city = extractFromAdress(addr_arr, "locality");
            var addr_street = extractFromAdress(addr_arr, "route");
            var addr_number = extractFromAdress(addr_arr, "street_number");
            var addr_area_1 = extractFromAdress(addr_arr, "administrative_area_level_1");
            var addr_area_2 = extractFromAdress(addr_arr, "administrative_area_level_2");

            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');

            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);

            $('#obj_addr_country_code').val(addr_country_code);
            $('#obj_addr_country').val(addr_country);
            $('#obj_addr_zip').val(addr_zip);
            $('#obj_addr_city').val(addr_city);
            $('#obj_addr_street').val(addr_street);
            $('#obj_addr_number').val(addr_number);

            $('#obj_addr_area_1').val(addr_area_1);
            $('#obj_addr_area_2').val(addr_area_2);

            $('#obj_addr_lon').val(place.geometry.location.lng());
            $('#obj_addr_lat').val(place.geometry.location.lat());
            $('#obj_addr_lon_view').val(place.geometry.location.lng());
            $('#obj_addr_lat_view').val(place.geometry.location.lat());

            //Автозаполнение названия объекта
            $('#obj_name_ru').val(addr_street + ' ' + addr_number).change();

            $('html,body').animate({
                scrollTop: $("#main-form").offset().top - 200
            }, 'slow');

            $("#fs_coordinates, #fs_address").stop().css("background-color", "#FFFF9C").animate({
                backgroundColor: "#FFFFFF"
            }, 1000);



        });

    }

    function extractFromAdress(components, type, size) {
        for (var i = 0; i < components.length; i++)
            for (var j = 0; j < components[i].types.length; j++)
                if (components[i].types[j] == type)
                    if (size == "short") {
                        return components[i].short_name;
                    } else {
                        return components[i].long_name;
                    }
        return "";
    }

    google.maps.event.addDomListener(window, 'load', gmap_initialize);

    //Init Map after "Address" tab shown
     $("input[data-target='#tab-geo']").on('shown.bs.tab', function(e) {
          gmap_initialize();
     });

    //Обработка кода объекта - обновление кода типа объекта
    $("#sf_smartfilters").select2("enable", false);

    $('#ot_object_type').change(function() {

/*        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ai/object/ajax',
            data: {
                method: "getobjtypecode",
                objtypeid: $('#ot_object_type').val()
            },

            success: function(data, status) {
                if (data.error) {} else {
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });
*/
        //------------- Get smartfilters By Object Type ID ------------------//
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ai/object/ajax',
            data: {
                method: "getsmartsbyobjtype",
                objtypeid: $('#ot_object_type').val()
            },

            success: function(data, status) {
                if (data.error) {} else {

                    //------------------- Update sf_smartfilters select2 -------------------
                    var sf_smartfilters;
                    $("#sf_smartfilters").select2("val", "");

                    if (data.objsmarts.length > 0) {
                        $.each(data.objsmarts, function(index, value) {
                            sf_smartfilters += '<option value="' + data.objsmarts[index].id + '">' + data.objsmarts[index].val + '</option>';
                        });
                        $('#sf_smartfilters').html(sf_smartfilters);
                        $("#sf_smartfilters").select2("enable", true);
                    } else {
                        $('#sf_smartfilters').html("");
                        $("#sf_smartfilters").select2("enable", false);
                    }

                    //В зависимости от типа выбранного жилья, выставляем дефолтные значения для полей:
                    // - смартфильтр
                    // - период оплаты
                    // - минимальный период проживания
                    
                    //_______________________________________Апартаменты_______________________________
                    if ($('#ot_object_type').val() == "9") 
                    {
                        $("#sf_smartfilters").select2("val", "11");
                        $("#obj_price_period").select2("val", "2");
                        $("#obj_price_minstay").val("2");
                    }

                    //_______________________________________Квартира_______________________________
                    if ($('#ot_object_type').val() == "2") 
                    {
                        $("#sf_smartfilters").select2("val", "20");
                        $("#obj_price_period").select2("val", "1");
                        $("#obj_price_minstay").val("6");
                    }

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });

    });



    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $(".btn-apply").click(function(event) {
        var $form = $('form[data-async]');
        var $btn = $(this);

        if (main_form.valid()) {

            $.ajax({
                context: $btn,
                dataType: 'json',
                cache: false,
                type: $form.attr('method'),
                url: '/ai/object/ajax',
                data: $form.serialize(),

                success: function(data, status) {
                    //Update Infobar
                    $('#info_obj_name_ru').text(data.form.obj_name_ru);
                    // $('#info_obj_codename').text(data.form.obj_codename);

                    //Update address in infobar
                    $('#info_addr_1').text(data.form.obj_addr_street + ' ' + data.form.obj_addr_number);

                    var addr2 = "";
                    addr2 += (data.form.obj_addr_entrance) ? "пд. " + data.form.obj_addr_entrance + " " : "";
                    addr2 += (data.form.obj_addr_floor) ? "эт. " + data.form.obj_addr_floor + " " : "";
                    addr2 += (data.form.obj_addr_room) ? "кв. " + data.form.obj_addr_room + " " : "";
                    $('#info_addr_2').text(addr2);

                    //Update dates in infobar
                    $('#info_obj_update').text(data.form.obj_update);
                    $('#info_obj_create').text(data.form.obj_create);

                    //Update ID
                    $('#obj_id').val(data.form.obj_id);

                    //Show infobar
                    $('.navbar-infobar').show();
                    $('body').removeClass('no-infobar');

                    $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
                        backgroundColor: "#F7F7F9"
                    }, 500);

                    $("fieldset").stop().css("background-color", "#FFFF9C").animate({
                        backgroundColor: "#FFF"
                    }, 500);


                },
                error: function(xhr, ajaxOptions, thrownError) {
                    new PNotify({
                        title: "Over Here",
                        text: thrownError,
                        type: "error",
                        // addclass: "stack-bar-top",
                        cornerclass: "",
                        width: "50%",
                        // stack: stack_bar_top
                    });

                    $btn.button('reset');
                }

            });
        }


        event.preventDefault();
    });


    function coordToCode(lon, lat) {
        lon = lon.replace(/\./g, '');
        lon = lon.substring(0, 6);

        lat = lat.replace(/\./g, '');
        lat = lat.substring(0, 6);

        return lon + '' + lat;

    }


    //-------------------------------------------------- jQuery Validate -----------------------------------/

    // jQuery.validator.addMethod("lettersonly", function(value, element) {
    //         return this.optional(element) || /^[a-z0-9_-]+$/i.test(value);
    // }, "Please use only a-z0-9_-");

    if ($.fn.validate) {
        var validobj = main_form.validate({
            rules: {
                "ot_object_type[]": {
                    required: true,
                },
                obj_name_ru: {
                    required: true,
                }
            },
            messages: {
                "ot_object_type[]": {
                    required: "Укажите тип объекта",
                },
                obj_name_ru: {
                    required: "Укажите название объекта",
                }
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function(index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");
                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function(index, error) {
                    var $element = $(error.element);

                    //Show errors on Select2 fields
                    if ($element.hasClass("select2-offscreen")) {
                        $("#s2id_" + $element.attr("id") + " ul").addClass("has-error");

                        $("#s2id_" + $element.attr("id") + " ul").tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');


                        //Show errors on other regular fields
                    } else {

                        $element.tooltip("destroy")
                            .data("title", error.message)
                            .tooltip({
                                placement: 'top',
                                trigger: 'manual',
                                show: '100'
                            }).tooltip('show');

                        $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                    }

                });

            },
        });

    } else {
        alert("Validate method not loaded");
    }

    //Select2 Error processing
    //************************************************************
    $(document).on("change", ".select2-offscreen", function() {
        if (!$.isEmptyObject(validobj.submitted)) {
            $("#s2id_" + $(this).attr("id") + " ul").removeClass("has-error");
            validobj.form();
        }
    });

    $(document).on("select2-opening", function(arg) {
        $("#s2id_" + $(arg.target).attr("id") + " ul").toggleClass("has-error");
    });

    //-------------------------------------------------- Fileupload -----------------------------------/
    $(function() {
        'use strict';

        $('#fileupload .fileupload-actions').hide();

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            url: '/ai/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        })
            .bind('fileuploadadd', function(e, data) {
                $('#fileupload .fileupload-actions').show({
                    duration: 500
                });
            });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
            )
        );

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');

        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function() {
            $(this).removeClass('fileupload-processing');
        }).done(function(result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {
                    result: result
                });
        });

    });

});
