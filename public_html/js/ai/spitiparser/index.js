/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    (function() {

        var Parser = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            startBtn: '#startParse',
            progressBar: '#progress .progress-bar',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                this.bindEvents();

            },
            // Init - END

            //*******************************************************************************
            //   bindEvents
            //*******************************************************************************
            bindEvents: function() {
                
                var self = this;

                $(document).on('click', self.startBtn, function(event) {
                    event.preventDefault();

                    self.pars_city = $('#pars_city').val();
                    self.pars_page = $('#pars_page').val();

                    self.startParse(self.pars_city, self.pars_page);

                });

            },
            // bindEvents - END

            //*******************************************************************************
            //   startParse
            //*******************************************************************************
            startParse: function(pars_city, pars_page) {

                var self = this;

                $(self.startBtn).button('loading');
                $('#loading-indicator').show();
                $('#parsing-results').hide();

                $.ajax(
                {
                    url: '/ai/spitiparser/ajax',
                    type: 'get',
                    data: {method: "startparse", pars_city: pars_city, pars_page: pars_page},
                    dataType: 'json',
                    // async: false,
                }).done(function( data ) {

                    var d = data.response;

                    $('#pr-city').text(d.pt_city);
                    $('#pr-all-pages').text(d.pt_all_pages);
                    $('#pr-all-obj').text(d.pt_all_obj);
                    $('#pr-pages').text( parseInt(d.pt_cur_page) - parseInt(d.pt_start_page) + 1 );
                    $('#pr-cur-page').text( parseInt(d.pt_cur_page) );
                    $('#pr-cur-obj').text( d.pt_cur_obj );

                    $('#pr-start').text(d.pt_start);
                    $('#pr-end').text(d.pt_end);

                    $(self.startBtn).button('reset');
                    $('#loading-indicator').hide();
                    $('#parsing-results').show();

                });

            },

        };

        Parser.init();

    })();

});