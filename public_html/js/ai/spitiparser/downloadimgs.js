/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    (function() {

        var Parser = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            startBtn: '#startDownload',
            resultsTbl: '#download-results',
            progressBar: '#progress .progress-bar',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                this.bindEvents();

            },
            // Init - END

            //*******************************************************************************
            //   bindEvents
            //*******************************************************************************
            bindEvents: function() {
                
                var self = this;

                $(document).on('click', self.startBtn, function(event) {
                    event.preventDefault();

                    self.pars_city = $('#pars_city').val();

                    self.startDownload(self.pars_city);

                });

            },
            // bindEvents - END

            //*******************************************************************************
            //   startDownload
            //*******************************************************************************
            startDownload: function(pars_city) {

                var self = this;

                $(self.startBtn).button('loading');
                $('#loading-indicator').show();
                $(self.resultsTbl).hide();

                $.ajax(
                {
                    url: '/ai/spitiparser/ajax',
                    type: 'get',
                    data: {method: "startdownload", pars_city: pars_city},
                    dataType: 'json',
                    // async: false,
                }).done(function( data ) {

                    var d = data.response;

                    $('#pr-city').text(d.pt_city);
                    $('#pr-all-pages').text(d.pt_all_pages);
                    $('#pr-all-obj').text(d.pt_all_obj);
                    $('#pr-pages').text( parseInt(d.pt_cur_page) - parseInt(d.pt_start_page) + 1 );
                    $('#pr-cur-page').text( parseInt(d.pt_cur_page) );
                    $('#pr-cur-obj').text( d.pt_cur_obj );

                    $('#pr-start').text(d.pt_start);
                    $('#pr-end').text(d.pt_end);

                    $(self.startBtn).button('reset');
                    $('#loading-indicator').hide();
                    $(self.resultsTbl).show();

                });

            },

        };

        Parser.init();

    })();

});