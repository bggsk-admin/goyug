/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    $(".panel").hide();
    $(".panel-body").text("");

    //*******************************************************************************
    //   Define some vars
    //*******************************************************************************
    $(".btn-ajax-action").click(function(event) {

        event.preventDefault();

        var method = $(this).attr("data-method");

        var $btn = $(this);
        $btn.button('loading');

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: "POST",
            url: '/ai/tools/ajax',
            data: {
                method: method
            },

            success: function(data, status) {

                $(".navbar-subnavbar, .navbar-breadcrumb, .breadcrumb").stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#F4F3ED"
                }, 500);

                $(".panel-body").html(data.response);
                $(".panel").show();

                $btn.button('reset');
            },

            error: function(xhr, ajaxOptions, thrownError) {
                new PNotify({
                    title: "Over Here",
                    text: thrownError,
                    type: "error",
                    // addclass: "stack-bar-top",
                    cornerclass: "",
                    width: "50%",
                    // stack: stack_bar_top
                });
                $btn.button('reset');
            }

        });

    });

});
