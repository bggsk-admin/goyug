/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

    (function() {

        var Parser = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            startBtn: '#parseOwners',
            progressBar: '#progress .progress-bar',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                this.bindEvents();

            },
            // Init - END

            //*******************************************************************************
            //   bindEvents
            //*******************************************************************************
            bindEvents: function() {
                
                var self = this;

                $(document).on('click', self.startBtn, function(event) {
                    event.preventDefault();

                    self.pars_city = $('#pars_city').val();
                    self.owners_list = $('#owners_list').val();

                    self.startParse(self.pars_city, self.owners_list);

                });

            },
            // bindEvents - END

            //*******************************************************************************
            //   startParse
            //*******************************************************************************
            startParse: function(pars_city, owners_list) {

                var self = this;

                $(self.startBtn).button('loading');
                $('#loading-indicator').show();
                $('#parsing-results').hide();

                $.ajax(
                {
                    url: '/ai/sutochnoparser/ajax',
                    type: 'get',
                    data: {method: "parseowners", pars_city: pars_city, owners_list: owners_list},
                    dataType: 'json',
                    // async: false,
                }).done(function( data ) {

                    var d = data.response;

                    // console.log(d);

                    $('#pr-city').text(d.pt_city);
                    $('#pr-all-owners').text(d.pt_all_obj);
                    $('#pr-cur-owner').text( d.pt_cur_obj);

                    $('#pr-start').text(d.pt_start);
                    $('#pr-end').text(d.pt_end);

                    $(self.startBtn).button('reset');
                    $('#loading-indicator').hide();
                    $('#parsing-results').show();

                });

            },

        };

        Parser.init();

    })();

});