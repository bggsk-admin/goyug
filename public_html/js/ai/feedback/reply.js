$(document).ready(function() {

  //-------------------------------------------------- Send Ajax Form -----------------------------------/
  $(".btn-reply").on('click', function(event) {

    event.preventDefault();

    var $btn = $(this);
    var $form = $('form[data-async]');

    $btn.attr('disabled', true);

    $.ajax({

      dataType: 'JSON',
      type: 'POST',
      url: '/ai/feedback/ajax',
      data: $form.serialize(),

      success: function(data) {

        //Ответ сервера
        var response = data.reply;

        //Успех
        if(response.status){

          $btn.text('Отправлено');
          $form.find('textarea').val('');

          $("body").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFF"
          }, 1000);

          $('.feedback-history-list').prepend('\
          <div class="media answer">\
            <div class="media-left media-top">\
              <img class="media-object" src="/images/feedback-support.png" alt="Пользователь">\
            </div>\
            <div class="media-body">\
              <h4 class="media-heading">Ответ тех. поддержки</h4>\
              <div class="date">' + response.fb_date + '</div>\
            </div>\
            <div class="media-text">\
              ' + response.fb_text + '\
            </div>\
          </div>');
        }
        //Ошибка
        else {
          $btn.text('Ошибка отправки');
        }
      },

      error: function(xhr, ajaxOptions, thrownError) {
        $btn.text('Ошибка отправки');
      },

      complete: function(){
        $btn.text('Отправить');
        $btn.attr('disabled', false);
      }

    });
  });
});