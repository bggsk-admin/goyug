$(document).ready(function() {

    $("#alert-phone-exist").hide();

    $("#u_phone_1").mask("+7 (999) 999-9999");
    $("#u_phone_2").mask("+7 (999) 999-9999");
    $("#u_phone_3").mask("+99 (999) 999-9999");

    //-------------------------------------------------- jQuery Validate -----------------------------------/
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z0-9_-]+$/i.test(value);
        }, "Please use only a-z0-9_-");


    if ($.fn.validate) {
        $('#main-form').validate({
            rules: {
                u_email: {
                    minlength: 6,
                    required: false,
                },
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function (index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");

                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function (index, error) {
                    var $element = $(error.element);

                    $element.tooltip("destroy")
                    .data("title", error.message)
                    .tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');

                    $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                });

            }

        });
    } else { alert("Validate method not loaded"); }

    //Срабатывание поиска при наборе запроса (с задержкой или нет)
    $("#u_phone_1, #u_phone_2, #u_phone_3").bindWithDelay("keyup", function() {
        ajaxCheckPhone($(this).val(), $(this));
    }, 0);


    /******************************************** CheckPhone *************************************/
    function ajaxCheckPhone(phone, obj) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ai/owner/ajax',
            data: {
                method: "checkphone",
                phone: phone,
                u_id: $("#u_id").val()

            },
            success: function(data, status) {

                var d = data.data;

                if(d.length >= 1)
                {
                    var el = d[0];

                    obj.addClass("has-error");
                    $("#alert-phone-exist").show();
                    $("#phone-exist-details").text(el.u_lastname + " " + el.u_firstname + " " + el.u_patronomyc);


                } else {
                    obj.removeClass("has-error");
                    $("#phone-exist-details").text("");
                    $("#alert-phone-exist").hide();
                }

                if (data.error) {} else {

                }

            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });

    } // - END - ajaxCheckPhone



});// END JQUERY