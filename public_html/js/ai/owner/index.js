/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};




$(document).ready(function() {

    // var tableHeight = $(window).height() - 350;

    var datatable = $('#watable').dataTable({

        // "sDom": "<'row'<'col-lg-12'f>r><'row'<'col-lg-3'i><'col-lg-9'p>r>t<'row'<'col-xs-3'i><'col-xs-9'p>>",
        dom: "<'row'<'col-xs-12'f>><'row'<'col-xs-6'l><'col-xs-6'C>><'row'<'col-xs-4'i><'col-xs-8'p>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        stateSave: true,
        "colVis": {
            "buttonText": "Скрыть / Показать колонки",
            exclude: [0, 1, 2],
            restore: "Восстановить",
            showAll: "Показать все",
            overlayFade: 0,
        },
        "oLanguage": {
            "sLengthMenu": "_MENU_ записей на страницу "
        },
        "order": [
            [3, "DESC"] //Default Order by obj_update
        ],
        // "bAutoWidth": false,
        "aoColumnDefs": [
            {
                "aTargets": [0, 1, 2],
                "bSortable": false
            }, 
        ],
        "oLanguage": {
            "sUrl": "/js/dataTables.russian.txt"
        },
        "iDisplayLength": 100,
        
        // "scrollY": tableHeight + "px", 
        // "scrollCollapse": true,        
    
    });

/*    new $.fn.dataTable.FixedHeader( datatable, {
        "offsetTop": 111,
         "top": true,
         "zTop": 11
    });
*/

});
