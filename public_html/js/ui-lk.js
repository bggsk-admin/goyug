//Сохранение E-mail владельца
function saveUserEmail(){

  //Процесс
  //$('.obj-view .menu .curcle-progress').fadeIn("fast");

  $.ajax({

    type: 'POST',
    url: '/user/ajax',
    data: 'action=saveUserEmail&email='+$('#user-email').val(),
    error: function() {
      alert('Ошибка сохранения! Пожалуйста, повторите попытку позднее.');
    },
    success: function(data) {

      //Ответ
      response = data.response;

      //console.log(response)

      //Ошибок нет
      if (response.status) {
        //console.log('ok')
        $('#user-email-reason').html('Благодарим! E-mail сохранен.');
        $('#user-email-reason').removeClass('user-email-reason');
        $('#user-email-submit').html('E-mail сохранен');
        $('#user-email-submit').attr('disabled', true);
        $('#user-email').attr('disabled', true);
        setTimeout(function(){$('div.user-email').fadeOut('fast')}, 3000);
      }
      //Ошибки есть
      else {
        //console.log('no')
        $('#user-email-reason').addClass('user-email-reason');
        $('#user-email-reason').html(response.reason);
        //setTimeout(function(){$('#user-email-reason').html('')}, 3000);
      }

      //Процесс
      //$('.obj-view .menu .curcle-progress').fadeOut("fast");
    }
  });
}

//Скрыть уведомление о удачном пробуждении
function userAwakeOK(){

  $.ajax({

    type: 'POST',
    url: '/user/ajax',
    data: 'action=userawakeok',
    dataType: 'JSON',
    error: function() {
      console.log('Ошибка сохранения! Пожалуйста, повторите попытку позднее.');
    },
    success: function(data) {

      //Ответ
      response = data.response;

      //Ошибок нет
      if (response.status) {
        $('#user-awake-success').fadeOut('fast');
      }
      //Ошибки есть
      else {
        $('#user-awake-success').fadeOut('fast');
        console.log(response.reason)
      }
    }
  });
}

//Скрыть уведомление об абонплате
function hideNotification(){

  $.ajax({

    type: 'POST',
    url: '/user/ajax',
    data: 'action=hidenotification&notification=rental',
    dataType: 'JSON',
    error: function() {
      console.log('Ошибка сохранения! Пожалуйста, повторите попытку позднее.');
    },
    success: function(data) {

      //Ответ
      response = data.response;

      //Ошибок нет
      if (response.status) {
        $('.rental-notification').fadeOut('fast');
      }
      //Ошибки есть
      else {
        $('.rental-notification').fadeOut('fast');
        console.log(response.reason)
      }
    }
  });
}

//Настройки тостера
$(document).ready(function(){
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "100",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "500",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
});