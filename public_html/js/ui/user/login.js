$(document).ready(function() {

    //-------------------------------------------------- Ajax context setup -----------------------------------/
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (settings.context != undefined && settings.context.hasClass('btn')) {
                settings.context.button('loading');
            }
        },
        complete: function() {
            this.button('reset');
        }
    });

    //-------------------------------------------------- Send Ajax Form -----------------------------------/
    $('form[data-async]').submit(function(event) {
        var $form = $(this);
        var $btn = $(this).children("button[type='submit']");

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),

            success: function(data, status) {
                if (data.redirect) {
                    window.location.href = data.redirect;
                }
                if (data.error) {
                    $(data.error.elements)
                        .data("title", data.error.text)
                        .tooltip({
                            placement: 'right',
                            trigger: 'manual',
                            show: '100'
                        }).tooltip('show');

                    $(data.error.elements).closest('.form-group').removeClass('has-success').addClass('has-error');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $btn.button('reset');
            }

        });


        event.preventDefault();
    });

    //-------------------------------------------------- jQuery Validate -----------------------------------/
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9_-]+$/i.test(value);
    }, "Please use only a-z0-9_-");


    if ($.fn.validate) {
        $('#form-signin').validate({
            rules: {
                u_username: {
                    minlength: 3,
                    maxlength: 40,
                    required: true,
                    // lettersonly: true
                },
                u_password: {
                    minlength: 3,
                    maxlength: 40,
                    required: true,
                    // lettersonly: true
                },
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function(index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");

                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function(index, error) {
                    var $element = $(error.element);

                    $element.tooltip("destroy")
                        .data("title", error.message)
                        .tooltip({
                            placement: 'right',
                            trigger: 'manual',
                            show: '100'
                        }).tooltip('show');

                    $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                });

            }

        });
    } else {
        alert("Validate method not loaded");
    }

    //-------------------------------------------------- Forgot -----------------------------------/
    $('button.btn-forgot').on('click', function(e){

        e.preventDefault();

        $btn = $(this);
        $form = $btn.closest('form');
        $reason = $('#reason');

        $.ajax({
            dataType: 'JSON',
            cache: false,
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function(data) {
                var response = data.forgot;
                if (response.status) {
                    responseSuccess();
                    setTimeout(function(){ $('#modal-forgot').modal('hide'); }, 5000);
                } else {
                    responseError(response.reason);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                responseError('Ошибка отправки - попробуйте позже');
            }
        });

        function responseError(text){
            $reason.css('color', 'red').text(text);
        }

        function responseSuccess(){
            $reason.css('color', 'green').text('На указанный адрес выслан одноразовый доступ в ваш личный кабинет');
        }

    });

}); //JQuery END
