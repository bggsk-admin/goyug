$(document).ready(function() {

    /*------------------------------------------------------------------------------------
    /* FullCalendar init
    --------------------------------------------------------------------------------------*/
    var bookCal_1 = $('#booking-calendar-1');
    var bookCal_2 = $('#booking-calendar-2');

    var bookCal_3 = $('#booking-calendar-3');
    var bookCal_4 = $('#booking-calendar-4');

    var allBookCal = $('#booking-calendar-1, #booking-calendar-2, #booking-calendar-3, #booking-calendar-4');

    var bookCal_3_4 = $('.fc-calendar-3-4');
    bookCal_3_4.hide();

    var fcOptions = {

        header: {
            left: '',
            center: 'title',
            right: ''
        },

        buttonText: {
            today: 'Сегодня',
            month: 'Месяц',
        },

        lang: 'ru',
        timezone: 'local',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        firstDay: 1,
        selectable: false,
        selectHelper: true,
        eventOverlap: false,

        eventRender: function(event, element, view) {

            // console.log(element);

        },

        select: function(start, end) {
            // $('#modal-add-event').modal('show');
            console.log('click-day');
            // var cal_startdate = moment(start).format("DD.MM.YYYY 14:00");
            // var cal_enddate = moment(end).format("DD.MM.YYYY 12:00");
            // $('#cal_startdate').val(cal_startdate);
            // $('#cal_enddate').val(cal_enddate);
            // var cal_startdate = $('#cal_startdate').val();
            // var cal_startdate = moment(cal_startdate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");
            // var cal_enddate = $('#cal_enddate').val();
            // var cal_enddate = moment(cal_enddate, "DD.MM.YYYY HH:mm").format("YYYY-MM-DD HH:mm");
            var cal_startdate = start;
            var cal_enddate = end;
            // var cal_status = $('#cal_status').val();
            var cal_status = "closed";
            var eventData = "";
            var cal_title = "Недоступно для бронирования";
            // if (cal_status == "booked") cal_title += "Объект забронирован";
            // if (cal_status == "closed") cal_title += "Объект недоступен";
            eventData = {
                id: Math.random(),
                // title: cal_title,
                title: "",
                start: cal_startdate,
                end: cal_enddate,
                status: cal_status,
                type: "event",
                className: "event-" + cal_status
            };
            bookCal.fullCalendar('renderEvent', eventData, false); // stick? = true
            bookCal.fullCalendar('unselect');
            // $('#modal-add-event').modal('hide');
            //------------- Apply Events ------------------------------
            var events = bookCal.fullCalendar('clientEvents');
            $.ajax({
                url: '/user/ajax',
                type: 'POST',
                data: {
                    action: "saveCal",
                    json: JSON.stringify(events),
                    id: $("#booking-calendar").data("objid")
                },
                dataType: 'json',
                cache: false,
                success: function(data, status) {},
            });
            $("#booking-calendar").stop().css("background-color", "#FFFF9C").animate({
                backgroundColor: "#FFFFFF"
            }, 1000);
        },

        dayClick: function(date, jsEvent, view) {

            // alert('Clicked on: ' + date.format("YYYY-MM-DD 00:00"));
            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            // alert('Current view: ' + view.name);

            var eventDate = moment(date).format("YYYY-MM-DD");
            var eventId = moment(date).format("YYYYMMDD");
            var eventBox = $('[data-date="' + eventDate + '"]');
            var events;


            //------------- Create Event ------------------------------
            if(!eventBox.hasClass("fc-day-selected"))
            {
                eventData = {
                    id: eventId,
                    title: "",
                    start: date.format("YYYY-MM-DD 00:00"),
                    end: date.format("YYYY-MM-DD 23:59"),
                    status: "closed",
                    type: "event",
                    className: "event-closed",
                };

                allBookCal.fullCalendar('renderEvent', eventData, false);

                //------------- Write into DB ------------------------------
                events = bookCal_1.fullCalendar('clientEvents');

                $.ajax({
                    url: '/user/ajax',
                    type: 'POST',
                    data: {
                        action: "saveCal",
                        json: JSON.stringify(events),
                        id: $("#booking-calendar-1").data("objid")
                    },
                    dataType: 'json',
                    cache: false,
                    success: function(data, status) {},
                });

                allBookCal.stop().css("background-color", "#FFFF9C").animate({
                    backgroundColor: "#FFFFFF"
                }, 1000);

                //Тостер
                toastr.error('День закрыт');

                eventBox.addClass('fc-day-selected').data("eventid", date.format("YYYYMMDD"));

            //------------- Remove Event ------------------------------
            } else {

                console.log(eventId);

                eventBox.removeClass('fc-day-selected');
                allBookCal.fullCalendar('removeEvents', [eventId]);

                //------------- Write into DB ------------------------------
                events = bookCal_1.fullCalendar('clientEvents');

                console.log(events);

                $.ajax({
                    url: '/user/ajax',
                    type: 'POST',
                    data: {
                        action: "saveCal",
                        json: JSON.stringify(events),
                        id: $("#booking-calendar-1").data("objid")
                    },
                    dataType: 'json',
                    cache: false,
                    success: function(data, status) {},
                });

                //Тостер
                toastr.success('День открыт');
            }
        },

        editable: false,

        events: {
            url: '/user/ajax',
            type: 'POST',
            data: {
                action: "loadCal",
                id: $("#booking-calendar-1").data("objid")
            },
            error: function() {
            },
            success: function(data, status) {

                $.each(data.data, function(index, event) {

                bookCal_1.fullCalendar('renderEvent', event, false);

                    var eventDate = moment(event.start).format("YYYY-MM-DD");
                    var eventId = moment(event.start).format("YYYYMMDD");
                    var eventBox = $('[data-date="' + eventDate + '"]');

                    if(!eventBox.hasClass("fc-day-selected"))
                        eventBox.addClass('fc-day-selected')
                        .data("eventid", eventId)
                    ;

                });

            },
        },

    };

    var date = new Date();
    var curMonth = date.getMonth();

    allBookCal.fullCalendar(fcOptions);
    bookCal_2.fullCalendar('gotoDate', moment().startOf('month').add(1, 'months').add(1, 'days'));

    $('#fc-prevmon').click(function() {
        allBookCal.fullCalendar('prev');
    });
    $('#fc-today').click(function() {
        bookCal_1.fullCalendar('today');
        bookCal_2.fullCalendar('gotoDate', moment().startOf('month').add(1, 'months').add(1, 'days'));
        bookCal_3.fullCalendar('gotoDate', moment().startOf('month').add(2, 'months').add(1, 'days'));
        bookCal_4.fullCalendar('gotoDate', moment().startOf('month').add(3, 'months').add(1, 'days'));
    });
    $('#fc-nextmon').click(function() {
        allBookCal.fullCalendar('next');
    });

    $('#fc-2m').click(function() {
        bookCal_3_4.hide();
    });

    $('#fc-4m').click(function() {
       bookCal_3_4.show();
        bookCal_3.fullCalendar('gotoDate', moment( bookCal_1.fullCalendar('getDate') ).startOf('month').add(2, 'months').add(1, 'days'));
        bookCal_4.fullCalendar('gotoDate', moment( bookCal_1.fullCalendar('getDate') ).startOf('month').add(3, 'months').add(1, 'days'));
    });


    /*------------------------------------------------------------------------------------
    /* FullCalendar - Delete Event action
    --------------------------------------------------------------------------------------*/
    $("#modal-delete-event .btn-primary").click(function(event) {
        var eventID = parseInt($("#event-info-id").val());
        bookCal.fullCalendar('removeEvents', [eventID]);

        //------------- Apply Events ------------------------------
        var events = bookCal.fullCalendar('clientEvents');
        $.ajax({
            url: '/user/ajax',
            type: 'POST',
            data: {
                action: "saveCal",
                json: JSON.stringify(events),
                id: $("#booking-calendar").data("objid")
            },
            dataType: 'json',
            cache: false,
            success: function(data, status) {},
        });
        //------------- Apply Events - END  ------------------------------
        $("#booking-calendar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFFFFF"
        }, 1000);
        $('#modal-delete-event').modal('hide');
        event.preventDefault();
    });


}); //JQuery END
// ******************************************** Dec of Num ********************************


//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
function decOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}