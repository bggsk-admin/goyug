$(document).ready(function(){

  //Поднять объект
  $('.btn-ontop').on('click', function(e){

    e.preventDefault();

    //Список выделяемых
    var form = $('#ontop-form-admin');

    //console.log(obj_id);

    $.ajax({

      dataType: 'json',
      cache: false,
      type: 'POST',
      url: '/user/ajax',
      data: form.serialize(),

      success: function(data) {

        var response = data.ontop;

        //Успех
        if(response.status){
          //console.log(response)

          //Тостер
          toastr.success('Объект поднят');
          setTimeout(function(){location.pathname = '/user/rooms'}, 1000);
        }
        //Ошибка
        else{
          console.log('Error marking');
        }
      },

      error: function(xhr, ajaxOptions, thrownError) {
        console.log('Error marking');
      }
    });
  });

  //Способ оплаты
  $('.payment form input[type="radio"]').on('click', function(e){

    var $form = $('#ontop-form-owner');
    var $formActions = $('#ontop-form-actions');
    var $radio = $(this);

    if($radio.val() == 'GYLK'){
      $form.attr('action', $formActions.find('input[name="action-goyug"]').val());
    } else {
      $form.attr('action', $formActions.find('input[name="action-yandex"]').val());
    }

  });

  //Кол-во поднятий
  $('input[name="ontopAutoUpdateCount"]').on('click', function(e){

    var $sum = $('input[name="sum"]');
    var $radio = $(this);

    switch($radio.val()*1){
      case 1:
        $sum.val(25);
        break;
      case 2:
        $sum.val(45);
        break;
      case 3:
        $sum.val(65);
        break;
      case 5:
        $sum.val(105);
        break;
      case 10:
        $sum.val(220);
        break;
        
      case 20:
        $sum.val(420);
        break;

      case 30:
        $sum.val(620);//620
        break;

      default:
        break;
    }

  });

});