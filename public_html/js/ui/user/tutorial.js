$(document).ready(function() {


    var slider = $('.bxslider-tutorial').bxSlider({
        auto: false,
        autoControls: false,
        infiniteLoop: false,
        pager: false,

        onSlideNext: function($navigation, oldIndex, newIndex){
            var value = $navigation.attr('progress-val');
            $('.progress-bar').css('width', value + '%').attr('aria-valuenow', value); 
        },
        onSlidePrev: function($navigation, oldIndex, newIndex){
            var value = $navigation.attr('progress-val');
            $('.progress-bar').css('width', value + '%').attr('aria-valuenow', value); 
        },
    });

    $(".go-next-slide").click(function(e) {
        e.preventDefault();
        slider.goToNextSlide();
    });

    $(".go-slide").click(function(e) {
        e.preventDefault();

        var slide_num = $(this).attr('go-slide-num');
        slider.goToSlide(slide_num-1);
    });



}); //JQuery END
