//Вкладки
function selectTab(active){
  $('.list-group .list-group-item').removeClass('active');
  $('.block').removeClass('active');
  $('#tab-' + active).addClass('active');
  $('#btn-' + active).addClass('active');
}

//Блоки
function goTo(active){
  var targetTop = $('#block-desc-' + active).offset().top - 140;
  $('html, body').animate({ scrollTop: targetTop }, 400);
}