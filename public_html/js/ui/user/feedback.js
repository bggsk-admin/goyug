$(document).ready(function() {

  //-------------------------------------------------- Send Ajax Form -----------------------------------/
  $(".btn-save").on('click', function(event) {

    event.preventDefault();

    //Процесс
    $('.obj-view .menu .curcle-progress').fadeIn("fast");

    //Снимаем выделение полей
    $('.form-group textarea').css('border-color', '#BABBBA');

    //Снимаем ошибки
    $('label span').html('');

    var $form = $('form[data-async]');
    var $btn = $(this);

    //console.log($form);
    //console.log($btn);

    $.ajax({

      context: $btn,
      dataType: 'JSON',
      cache: false,
      type: 'POST',
      url: '/user/ajax',
      data: $form.serialize(),

      success: function(data) {

        //Ответ сервера
        var response = data.response;

        //Успех
        if(response.status){
          $(".btn-save").html('Отправлено');
          $(".panel-body").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFF"
          }, 1000);
          //Тостер
          toastr.success('Ваше сообщение отправлено');
          $('form').hide();
          $('#success').fadeIn();
          $('#text').val('');
          setTimeout(function(){
            $(".btn-save").html('Отправить');
            $('form').fadeIn();
            $('#success').hide();
          }, 3000);
        }
        //Ошибка
        else {
          //Выделение поля
          if(response.class){
            //Выделяем поле
            $('.form-group.' + response.class + ' textarea').css('border-color', 'maroon');
            //Ошибка
            $('label[for="' + response.class + '"] span').html(response.reason);
            //Изменяем кнопку
            $(".btn-save").html('Ошибка');
            setTimeout(function(){
              $(".btn-save").html('Отправить');
            }, 2000);
          }
        }

        //Процесс
        $('.obj-view .menu .curcle-progress').fadeOut("fast");

      },

      error: function(xhr, ajaxOptions, thrownError) {

        $btn.button('reset');

        //Процесс
        $('.obj-view .menu .curcle-progress').fadeOut("fast");
      }

    });
  });
});