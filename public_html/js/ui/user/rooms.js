$(document).ready(function(){

  //Выделение объектов
  $('.btn-mark-out').on('click', function(e){

    e.preventDefault();

    //Список выделяемых
    var marked = [];

    $('.mark-out').each(function(index, elem){
      marked.push(elem.dataset.id)
    });

    //console.log(marked);

    $.ajax({

      dataType: 'json',
      cache: false,
      type: 'POST',
      url: '/user/ajax',
      data: 'marked=' + JSON.stringify(marked) + '&action=markOutObjects',

      success: function(data) {

        var response = data;

        //Успех
        if(response.status){
          //console.log(response.marked)
          //Включаем выделение
          for(var i = 0; i < response.marked.length; i++){
            $('#mark-out-' + response.marked[i]).addClass('active');
            $('#marked-to-' + response.marked[i]).html(response.markedTo);
          }

          //Тостер
          toastr.success('Объекты выделены');
        }
        //Ошибка
        else{
          console.log('Error marking');
        }
      },

      error: function(xhr, ajaxOptions, thrownError) {
        console.log('Error marking');
      }
    });
  });

  //Включение объектов
  $('.btn-enable').on('click', function(e){

    e.preventDefault();

    $btn = $(this);

    $.ajax({

      dataType: 'json',
      type: 'POST',
      url: '/user/ajax',
      data: 'obj_id=' + $btn.data('obj-id') + '&action=objEnable',
      success: function(data) {
        var response = data.obj_enable;
        //Успех
        if(response.status){
          //console.log(response.marked)
          //Включаем выделение
          location.pathname = '/user/rooms';
        }
        //Ошибка
        else{
          console.log(response.reason);
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log('Error enable');
      }
    });
  });

  //Выключение объектов
  $('.btn-disable').on('click', function(e){

    e.preventDefault();

    $btn = $(this);

    $.ajax({

      dataType: 'json',
      type: 'POST',
      url: '/user/ajax',
      data: 'obj_id=' + $btn.data('obj-id') + '&action=objDisable',
      success: function(data) {
        var response = data.obj_disable;
        //Успех
        if(response.status){
          //console.log(response.marked)
          //Включаем выделение
          location.pathname = '/user/rooms';
        }
        //Ошибка
        else{
          console.log(response.reason);
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log('Error enable');
      }
    });
  });

  //Сегодня свободно
  /*
  $('.btn-isempty').on('click', function(event){

    event.preventDefault();

    var $btn = $(this);
    var $label = $('#isempty-label-' + $btn.data('obj-id'));

    $.ajax({

      dataType: 'json',
      cache: false,
      type: 'POST',
      url: '/user/ajax',
      data: 'obj_id=' + $btn.data('obj-id') + '&action=isempty',

      success: function(data) {

        var response = data.isempty;

        //Успех
        if(response.status){

          //Свободный
          if(response.isempty){
            toastr.info('Объект выделен как свободный.<br />' + response.isempty_deactivate);
            $btn.addClass('btn-isempty-true');
            $btn.addClass('active');
            $label.addClass('isempty-label-true');
            $('#' + response.uid + '.card' + ' .one-isempty').show();
            $('#' + response.uid + '.card' + ' .one-isempty .deactivate').text(response.deactivate);
          }
          //Не свободный
          else {
            toastr.warning('Объект сегодня занят');
            $btn.removeClass('btn-isempty-true');
            $btn.removeClass('active');
            $label.removeClass('isempty-label-true');
            $('#' + response.uid + '.card' + ' .one-isempty').hide();
          }
        }
        //Ошибка
        else{
          console.log('Ошибка маркера сегодня свободен');
        }
      },

      error: function(xhr, ajaxOptions, thrownError) {
        console.log('Ошибка маркера сегодня свободен');
      }
    });
  });
*/

  //Выделить фото - лейбл
  /*
  setInterval(function(){
    $('.btn-photomark span.new').animate({top: -3}, 80).delay(60).animate({top: -1}, 100);
  }, 12000);*/
});