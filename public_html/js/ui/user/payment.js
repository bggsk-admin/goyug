//Только целые числа
function checkNumberFields(e, k){

	var str = jQuery(e).val();
	var new_str = s = "";
	//var addMoneySumVisual = $('form #addmoney-sum');

	for(var i=0; i < str.length; i++){

		s = str.substr(i,1);

		//Если цифра
		if(s!=" " && isNaN(s) == false){
			new_str += s;
		}
	}

	if(eval(new_str) == 0){ new_str = ""; }

	jQuery(e).val(new_str);
	//addMoneySumVisual.html(new_str);
}

//Валидация введенной суммы
$(document).ready(function(){

	var addMoneySumInput = $('form [name="sum"]');

	//Пользователь отпускает клавишу клавиатуры
	addMoneySumInput.keyup(function(event){
	    checkNumberFields(this, event);
	//Пользователь нажимает клавишу клавиатуры и удерживает её в нажатом состоянии
	}).keypress(function(event){
	    checkNumberFields(this, event);
	//Поле теряет фокус
	}).change(function(event){
	    checkNumberFields(this, event);
	}).click(function(){
		this.select();
	});
});