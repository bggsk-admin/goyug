//Вкладки
function selectTab(active){
  $('.btn-group.tabs .btn').removeClass('active');
  $('.block').removeClass('active');
  $('#tab-' + active).addClass('active');
  $('#btn-' + active).addClass('active');
  location.hash = active;
}

$(document).ready(function() {

  //Открытие вкладок по якорю
  var url = window.location;
  var registredAnchors = ["general", "properties", "address", "photo"];
  if(url.hash){
    //Якорь
    var anchor = url.hash.replace('#', '');
    //Если якорь известен, показываем вкладку
    if(registredAnchors.indexOf(anchor) != -1){
      selectTab(anchor);
    }
  }

  /*------------------------------------------------------------------------------------
  /* GoogleMap Search Address
  --------------------------------------------------------------------------------------*/

  //Prevent 'enter' key press when in search input
  $("#search_address").focus(function() {
    $(this).keypress(function(event) {
      if (event.keyCode == 13) {
        event.preventDefault();
      }
    });
  });

  $("#search_address").click(function() {
    $('html,body').animate({
      scrollTop: $("#search_address").offset().top - 160
    }, 'slow');
  });

  //Карта и гугл-адрес
  var map, input, marker, autocomplete;
  var markers = [];

  function gmap_initialize() {

    var init_lng = $('#obj_addr_lon').val().length ? $('#obj_addr_lon').val() : 38.97871465;
    var init_lat = $('#obj_addr_lat').val().length ? $('#obj_addr_lat').val() : 45.05047394;

    var defPos = new google.maps.LatLng(init_lat, init_lng);
    //console.log(defPos)
    var mapOptions = {
      center: defPos,
      zoom: 17,
      scrollwheel: false
    };

    map = new google.maps.Map(document.getElementById('googlemap'), mapOptions);

    input = document.getElementById('search_address');

    autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();

    marker = new google.maps.Marker({
      position: defPos,
      map: map,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29),
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    });
    markers.push(marker);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {

      infowindow.close();
      marker.setVisible(false);

      var place = autocomplete.getPlace();

      //console.log(place);

      if (!place.geometry) return;

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17); // Why 17? Because it looks good.
      }
      marker.setIcon( /** @type {google.maps.Icon} */ ({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
      }));
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      var addr_arr = place.address_components;

      var addr_country_code = extractFromAdress(addr_arr, "country", "short");
      var addr_country = extractFromAdress(addr_arr, "country");
      var addr_zip = extractFromAdress(addr_arr, "postal_code");
      var addr_city = extractFromAdress(addr_arr, "locality");
      var addr_street = extractFromAdress(addr_arr, "route");
      var addr_number = extractFromAdress(addr_arr, "street_number");
      var addr_area_1 = extractFromAdress(addr_arr, "administrative_area_level_1");
      var addr_area_2 = extractFromAdress(addr_arr, "administrative_area_level_2");

      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');

      }

      infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
      infowindow.open(map, marker);

      $('#obj_addr_country_code').val(addr_country_code);
      $('#obj_addr_country').val(addr_country);
      $('#obj_addr_zip').val(addr_zip);
      $('#obj_addr_city').val(addr_city);
      $('#obj_addr_street').val(addr_street);
      $('#obj_addr_number').val(addr_number);

      $('#obj_addr_area_1').val(addr_area_1);
      $('#obj_addr_area_2').val(addr_area_2);

      $('#obj_addr_lon').val(place.geometry.location.lng());
      $('#obj_addr_lat').val(place.geometry.location.lat());
      $('#obj_addr_lon_view').val(place.geometry.location.lng());
      $('#obj_addr_lat_view').val(place.geometry.location.lat());

      //Автозаполнение названия объекта
      $('#obj_name_ru').val(addr_street + ' ' + addr_number).change();

      $('html,body').animate({
        scrollTop: $("#main-form").offset().top - 200
      }, 'slow');

      $("#fs_coordinates, #fs_address").stop().css("background-color", "#FFFF9C").animate({
        backgroundColor: "#FFFFFF"
      }, 1000);
    });

    //Перетаскивание маркера
    google.maps.event.addListener(marker, "dragend", function(event) {
        position_x = marker.getPosition().lat();
        position_y = marker.getPosition().lng();
        //console.log(position_x + ' ' + position_y);
        $('#obj_addr_lon').val(position_y);
        $('#obj_addr_lat').val(position_x);
        $('#obj_addr_lon_view').val(position_y);
        $('#obj_addr_lat_view').val(position_x);
    });

  }

  function extractFromAdress(components, type, size) {
    for (var i = 0; i < components.length; i++)
      for (var j = 0; j < components[i].types.length; j++)
        if (components[i].types[j] == type)
          if (size == "short") {
            return components[i].short_name;
          } else {
            return components[i].long_name;
          }
    return "";
  }

  google.maps.event.addDomListener(window, 'load', gmap_initialize);

  //Инициализация при открытии вкладки
  var firstMapView = true;
  $('#btn-address').on('click', function(e) {
    if(firstMapView){
      gmap_initialize();
      firstMapView = false;
    }
  });

  /*------------------------------------------------------------------------------------
  /* Город!
  --------------------------------------------------------------------------------------*/

  //Заполнение при загрузке
  setTimeout(function(){
    changeCityChoosen();
  }, 1000);
  initModuleNoteLabel();

  //При смене города
  $('#obj_addr_city_uid').on('change', function(){
    changeCityChoosen();
    editModule('city');
  });

  //При полном заполнении адреса
  $('#obj_addr_street, #obj_addr_number, #obj_addr_floor, #obj_addr_elevation').on('keyup', function(e){
    editModule('addr');
    updateModuleNoteLabel(e);
  });

  //Редактирование модуля
  function editModule(moduleName) {

    switch(moduleName){

      //Город
      case 'city':

        //Выбранный город
        var $cityChoosen = $('#obj_addr_city_uid option:selected');

        //Город выбран
        if($cityChoosen.val()){
          showModule('addr');
        } else {
          hideModule('addr');
          hideModule('placemark');
        }

        break;

      //Адрес
      case 'addr':

        //Заполнены все поля
        if( $('#obj_addr_street').val() && $('#obj_addr_number').val() && $('#obj_addr_floor').val() && $('#obj_addr_elevation').val() ){

          //Прописываем адрес в поиске Гугл
          $(input).val( $('#obj_addr_city_uid option:selected').text() + ' ' + $('#obj_addr_street').val() + ' ' + $('#obj_addr_number').val() );

          showModule('placemark');

          if(firstMapView){
            gmap_initialize();
            firstMapView = false;
          }

          //Геокодер
          $.ajax({
            dataType: 'json',
            cache: false,
            type: 'GET',
            url: 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyATQ9YSNhMLfhS6Qpv9lHr84Pvd0rkfUuk&address=' + $('#obj_addr_city_uid option:selected').text() + ',+' + $('#obj_addr_street').val() + ',+' + $('#obj_addr_number').val(),
            success: function(data, status) {
              clearMarkers();
              setMarker(data.results[0].geometry.location);
            },
            error: function(xhr, ajaxOptions, thrownError) { }
          });
        }
        //Заполнены не все поля
        else {
          hideModule('placemark');
        }

        break;
    }
  }

  //Показ модуля
  function showModule(moduleName) {
    $('.module-' + moduleName).fadeIn('fast');
  }

  //Скрытие модуля
  function hideModule(moduleName) {
    $('.module-' + moduleName).fadeOut('fast');

    switch(moduleName){

      case 'addr':

        $('#obj_addr_street').val('');
        $('#obj_addr_number').val('');
        $('#obj_addr_floor').val('');
        $('#obj_addr_elevation').val('');

        break;

      case 'placemark':

        $('#obj_addr_lon').val('');
        $('#obj_addr_lat').val('');
        $('#obj_addr_lon_view').val('');
        $('#obj_addr_lat_view').val('');

        break;
    }
  }

  //Показывает / скрывает метки в лейбле
  function updateModuleNoteLabel(e) {
    var $input = $(e.currentTarget);
    var $label = $("label[for='"+$input.attr('id')+"']");
    //console.log($label)
    if($input.val()){
      $label.find('.module-label-note').css('display', 'none');
    } else {
      $label.find('.module-label-note').css('display', 'inline-block');
    }
  }

  //Инициализация меток в лейблах
  function initModuleNoteLabel() {
    var inputs = [];
        inputs.push('obj_addr_street');
        inputs.push('obj_addr_number');
        inputs.push('obj_addr_floor');
        inputs.push('obj_addr_elevation');
    for (var i = inputs.length - 1; i >= 0; i--) {
      var $input = $('#' + inputs[i]);
      var $label = $("label[for='"+$input.attr('id')+"']");
      if($input.val()){
        $label.find('.module-label-note').css('display', 'none');
      } else {
        $label.find('.module-label-note').css('display', 'inline-block');
      }
    }
  }

  //Прописывает город в поле названия города
  function changeCityChoosen(){
    var $cityChoosen = $('#obj_addr_city_uid option:selected');
    if($cityChoosen.val()){
      $('#obj_addr_city').val($cityChoosen.text());
    } else {
      $('#obj_addr_city').val('');
    }
  }

  //Устанавливаем маркер в центр карты
  function setMarker(coords){

    //Меняем карту
    map.setCenter(coords);
    map.setZoom(17);

    //Новые координаты
    $('#obj_addr_lon').val(coords.lng);
    $('#obj_addr_lat').val(coords.lat);
    $('#obj_addr_lon_view').val(coords.lng);
    $('#obj_addr_lat_view').val(coords.lat);

    //Ставим новый маркер
    marker = new google.maps.Marker({
      position: coords,
      map: map,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29),
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    });
    markers.push(marker);

    //Перетаскивание маркера
    google.maps.event.addListener(marker, "dragend", function(event) {
      position_x = marker.getPosition().lat();
      position_y = marker.getPosition().lng();
      $('#obj_addr_lon').val(position_y);
      $('#obj_addr_lat').val(position_x);
      $('#obj_addr_lon_view').val(position_y);
      $('#obj_addr_lat_view').val(position_x);
    });
  }

  //Очистка маркеров с карты
  function clearMarkers() {
    //console.log('Очистка маркеров');
    //console.log(markers);
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }

  //Только целые числа
  function checkNumberFields(e, k){

    var str = jQuery(e).val();
    var new_str = s = "";
    var addMoneySumVisual = $('form #addmoney-sum');

    for(var i=0; i < str.length; i++){

      s = str.substr(i,1);

      //Если цифра
      if(s!=" " && isNaN(s) == false){
        new_str += s;
      }
    }

    if(eval(new_str) == 0){ new_str = ""; }

    jQuery(e).val(new_str);
    addMoneySumVisual.html(new_str);
  }

  //Валидация на целое число
  $(document).ready(function(){

    var addMoneySumInput = $('input#obj_addr_elevation');

    //Пользователь отпускает клавишу клавиатуры
    addMoneySumInput.keyup(function(event){
        checkNumberFields(this, event);
    //Пользователь нажимает клавишу клавиатуры и удерживает её в нажатом состоянии
    }).keypress(function(event){
        checkNumberFields(this, event);
    //Поле теряет фокус
    }).change(function(event){
        checkNumberFields(this, event);
    }).click(function(){
      this.select();
    });
  });

  /*------------------------------------------------------------------------------------
  /* Обрезка названия
  --------------------------------------------------------------------------------------*/
  var oldObjNameRu = $('#obj_name_ru').val();
  var $input = $('#obj_name_ru');
  var $wrapper = $input.closest('.form-group');

  if(oldObjNameRu.length > 30){
    $wrapper.addClass('form-group-error');
    var newObjNameRu = oldObjNameRu.substring(0, 30);
    $input.val(newObjNameRu);
  }

  $('#obj_name_ru').on('keyup', function(e){
    //Слишком длинное описание
    if($input.val().length > 30){
      $input.val(oldObjNameRu);
      $wrapper.addClass('form-group-error');
    } else {
      oldObjNameRu = $('#obj_name_ru').val();
      $wrapper.removeClass('form-group-error');
    }
  });
  /* -----------------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------------------
  /* Bootstrap Datetimepicker
  --------------------------------------------------------------------------------------*/

  $('#pick_cal_startdate').datetimepicker({
    language: 'ru',
    defaultDate: moment().format('L') + " 14:00",
  });

  $('#pick_cal_enddate').datetimepicker({
    language: 'ru',
    defaultDate: moment().add(3, 'days').format('L') + " 12:00",
  });

  //Заезд
  $('#obj_checkin').datetimepicker({
    pickDate: false
  });

  //Выезд
  $('#obj_checkout').datetimepicker({
    pickDate: false
  });

  //-------------------------------------------------- Send Ajax Form -----------------------------------/
  $(".btn-save").click(function(event) {

    //Процесс
    $('.obj-view .menu .curcle-progress').fadeIn("fast");

    var $form = $('form[data-async]');
    var $btn = $(this);
        $btn.attr('disabled', true);

    //console.log($form);
    //console.log($btn);

    $.ajax({

      context: $btn,
      dataType: 'json',
      cache: false,
      type: $form.attr('method'),
      url: '/user/ajax',
      data: $form.serialize(),

      success: function(data, status) {

        if (data.error) {
          //$(data.error.elements)
          //.data("title", data.error.text)
          //.tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');
          //$(data.error.elements).closest('.form-group').removeClass('has-success').addClass('has-error');
        }

        var form = data.form;

        if(form.status){

          //Update object-name
          //$('#info_obj_name_ru').text(form.obj_name_ru);

          //Update uid
          //$('#info_obj_uid').text(form.obj_uid);

          //Update address in infobar
          var addr1 = "",
              addr2 = "";
          addr1 += (form.obj_addr_street) ? form.obj_addr_street + " " : "";
          addr1 += (form.obj_addr_number) ? form.obj_addr_number : "";
          $('#info_addr_1').text(addr1);

          addr2 += (form.obj_addr_entrance) ? "пд. " + form.obj_addr_entrance + " " : "";
          addr2 += (form.obj_addr_floor) ? "эт. " + form.obj_addr_floor + " " : "";
          addr2 += (form.obj_addr_room) ? "кв. " + form.obj_addr_room + " " : "";
          $('#info_addr_2').text(addr2);

          //Update dates in infobar
          //$('#info_obj_update').text(form.obj_update);
          /*
          $(".navbar-infobar").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#F7F7F9"
          }, 500);*/

          $(".panel-body").stop().css("background-color", "#FFFF9C").animate({
            backgroundColor: "#FFF"
          }, 1000);

          //Тостер
          toastr.success('Объект сохранен');

          //Процесс
          $('.obj-view .menu .curcle-progress').fadeOut("fast");

        } else {

          //Тостер
          toastr.error(form.reason);

          //Процесс
          $('.obj-view .menu .curcle-progress').fadeOut("fast");

        }

      },

      error: function(xhr, ajaxOptions, thrownError) {

        $btn.button('reset');

        //Процесс
        $('.obj-view .menu .curcle-progress').fadeOut("fast");
      },

      complete: function(){
        $btn.attr('disabled', false);
      }

    });

    event.preventDefault();

  });

  //-------------------------------------------------- Fileupload -----------------------------------//

  $(function() {

    'use strict';

    $('#fileupload .fileupload-actions').hide();

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        url: '/files/index/upload_ucid/' + $('#upload_ucid').val() + '/upload_id/' + $('#upload_id').val(),
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
      })
      .bind('fileuploadadd', function(e, data) {
        //$('#fileupload .fileupload-actions').show({
        //  duration: 500
        //});
      });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
      'option',
      'redirect',
      window.location.href.replace(
        /\/[^\/]*$/,
        '/cors/result.html?%s'
      )
    );

    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');

    $.ajax({
      url: $('#fileupload').fileupload('option', 'url'),
      dataType: 'json',
      context: $('#fileupload')[0]
    }).always(function() {
      $(this).removeClass('fileupload-processing');
    }).done(function(result) {
      $(this).fileupload('option', 'done')
        .call(this, $.Event('done'), {
          result: result
        });
    });
  });

  // jQuery Pretty Checkable
  $('input.pretty').prettyCheckable({
    labelPosition: 'right'
  });

  $('.form-help').tooltip({
    trigger: 'hover focus',
    placement: 'auto',
    container: 'body'
  });

  $('.toggle_item').hide();
  $('.toggle').click(function() {
    $(this).nextAll(".toggle_item:first").slideToggle('slow', function() {});
    return false;
  });
  // jQuery Pretty Checkable end
});


// ******************************************** Dec of Num ********************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
function decOfNum(number, titles) {
  cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

$(document).ready(function(){

  //Сортировка картинок
  $(function() {
    $('#sortable').sortable({
      placeholder: 'ui-state-highlight',
      cursor: 'move',
      update: function(e, ui) {
        var element_id = ui.item.context.id.replace('tr-', '');
        var newOrderSrc = $('#sortable').sortable('toArray');
        var newOrder = [];
        $.map(newOrderSrc, function(e, i){
          newOrder.push(e.replace('tr-', ''));
        });
        //Отправка на сервер
        $.ajax({
          type: 'GET',
          url: '/files/order/',
          data: 'order='+JSON.stringify(newOrder),
          error: function(){ console.log('Ошибка!'); },
          success: function(data){ }
        });
      }
    });
    $('#sortable').disableSelection();
  });

  //Switchery
  var elem = document.querySelector('#obj_enable_dummy');
  //var elem = document.querySelector('.obj-view input[type=checkbox]');
  var init = new Switchery(elem);

  //Switchenry - обновление лейбла
  var publicEnable = elem.getAttribute('checked');
  elem.onchange = function(){

    //Затенение текста
    if(publicEnable){
      $('.obj-view .submenu .public label').html('Не опубликовано');
      $('.obj-view .submenu .public label').removeClass('active');
      $('#obj_enable').val(0);
      publicEnable = false;
    } else {
      $('.obj-view .submenu .public label').html('Опубликовано');
      $('.obj-view .submenu .public label').addClass('active');
      $('#obj_enable').val(1);
      publicEnable = true;
    }

    /*
    //Затенение текста
    if(publicEnable){
      $('.obj-view .submenu .public label').html('Не опубликовано');
      $('.obj-view .submenu .public label').removeClass('active');
      //Деактивация
      $('.obj-view .row.parts label').css('color', '#cdcdcd');
      $('.obj-view .row.parts input').css('border-color', '#cdcdcd');
      $('.obj-view .row.parts input').css('color', '#cdcdcd');
      $('.obj-view .row.parts textarea').css('border-color', '#cdcdcd');
      $('.obj-view .row.parts textarea').css('color', '#cdcdcd');
      $('.obj-view .row.parts select').css('border-color', '#cdcdcd');
      $('.obj-view .row.parts select').css('color', '#cdcdcd');
      $('.obj-view .row.parts .panel-title').css('color', '#cdcdcd');
      publicEnable = false;
    } else {
      $('.obj-view .submenu .public label').html('Опубликовано');
      $('.obj-view .submenu .public label').addClass('active');
      //Активация
      $('.obj-view .row.parts label').css('color', '#343434');
      $('.obj-view .row.parts input').css('border-color', '#ccc');
      $('.obj-view .row.parts input').css('color', '#343434');
      $('.obj-view .row.parts textarea').css('border-color', '#ccc');
      $('.obj-view .row.parts textarea').css('color', '#343434');
      $('.obj-view .row.parts select').css('border-color', '#ccc');
      $('.obj-view .row.parts select').css('color', '#343434');
      $('.obj-view .row.parts .panel-title').css('color', '#343434');
      publicEnable = true;
    }
    */
  }
});