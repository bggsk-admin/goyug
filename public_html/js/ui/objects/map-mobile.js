/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

	(function() {

		var Mapbox = {

			//*******************************************************************************
			//   Define some vars
			//*******************************************************************************
			topNavbarH: 100,
			mapDivName: "map-canvas",
			mapDiv: $('#map-canvas'),
			token: 'pk.eyJ1IjoibWFsYW1kYXIiLCJhIjoiUWF0QVVKTSJ9.Kn2I-QVSUu4Fco8OEOxN1w',
			// token: '',

			// mapName: 'malamdar.579e4606',
			mapName: 'malamdar.m0k879io',
			defaultPos: [parseFloat($('#objects-map').data('lat')), parseFloat($('#objects-map').data('lng'))],

			onmap_filters: $('#onmap-filters'),

			venueList: '#venueList',
			venueListContent: '#venueList .panelContent',
			venueListTMPL: '#venueListTMPL',
			venuePopupTMPL: '#venuePopupTMPL',
			venuePhoto: '.photoContainer',

			venueView: '#venueView',
			venueViewContent: '#venueView .panelContent',
			venueImgSlider: "#venueImgSlider",

			filters: '#filters',
			filtersTrigger: '#filters .panelTrigger',
			filterItem: '.filterItem',
			filterRooms: $('#filter-rooms input[type="checkbox"]'),
			filterObjtype: $('#filter-objtype input[type="checkbox"]'),
			filterObjIsempty: $('#filter-isempty input[type="checkbox"]'),

			firstClickFilters: false,

			//Запрос списка объектов по фильтру
			query: { from: 0, onpage: 10 },

			streetview: '#streetview',

			zoomSlider: '.leaflet-control-zoomslider',

			mapObjects: [],

			//*******************************************************************************
			//   Init
			//*******************************************************************************
			init: function() {

				// var urlParams = this.parseURL();
				this.cityUID = $('#objects-map').data('cityuid');
				this.priceMINdef = $('#objects-map').data('minprice');
				this.priceMAXdef = $('#objects-map').data('maxprice');

				L.mapbox.accessToken = this.token;

				this.setsize();

				this.map = L.mapbox
					.map(this.mapDivName, this.mapName, {
						zoomControl: false,
						minZoom: 12,
					})
					.setView(this.defaultPos, 10);

				L.control.zoomslider().addTo(this.map);

				// Switch OFF Zoomer
				// this.map.zoomControl.removeFrom(this.map);

				this.layer = L.mapbox.featureLayer().addTo(this.map);

				//Маркеры на карте
				this.addMarkers();
				//this.addPopups();
				//Список объектов
				this.loadVenueList();

				// Disable map controls
				// this.map.dragging.disable();
				// this.map.touchZoom.disable();
				// this.map.doubleClickZoom.disable();
				this.map.scrollWheelZoom.disable();

				this.bindEvents();
				this.bindUiEvents();

				this.infoPanel();

				this.imgSlider = $(venueImgSlider).bxSlider({
					minSlides: 1,
					maxSlides: 1,
					moveSlides: 1,
					slideMargin: 0,
					infiniteLoop: false,
					hideControlOnEnd: true,
					pager: false,
					auto: false,
					autoControls: false,
				});

				//*******************************************************************************
				//   noUiSlider - Price slider
				//*******************************************************************************
				$(".f-price-slider").noUiSlider({
					start: [this.priceMINdef, this.priceMAXdef],
					step: 100,
					connect: true,
					range: {
						'min': [this.priceMINdef],
						'max': [this.priceMAXdef]
					},
					serialization: {
						lower: [
							$.Link({
								target: $('#f-price-min'),
								method: "text"
							}),
						],
						upper: [
							$.Link({
								target: $('#f-price-max'),
								method: "text"
							}),
						],
						format: {
							decimals: 0,
							mark: ','
						}
					}
				});

				$("#f-price-min-label").text(this.priceMINdef);
				$("#f-price-max-label").text(this.priceMAXdef);
				$("#f-price-mid-label").text(((this.priceMAXdef - this.priceMINdef) / 2 + this.priceMINdef));
			},
			// Init - END

			//*******************************************************************************
			//   setsize
			//*******************************************************************************
			setsize: function() {

				this.mapDiv
					.css("width", $(window).width())
					.css("height", $(window).height() - this.topNavbarH);

				$(this.venueList + ", " + this.venueListContent)
					.css("height", $(window).height() - this.topNavbarH);

				$(this.venueView + ", " + this.venueViewContent)
					.css("height", $(window).height() - this.topNavbarH);

				$(this.streetview)
					.css("height", $(window).height() - this.topNavbarH)
					.css("width", $(window).width() - $(this.venueList).width());

				$(this.filters).find('.panelContent')
					.css("height", $(window).height() - this.topNavbarH);

			},
			// setsize - END

			//*******************************************************************************
			//   bindEvents
			//*******************************************************************************
			bindEvents: function() {

				var self = this;

				$(window).on('resize', $.proxy(function() {
					this.setsize();
					this.fitMarkers();
				}, this));

				this.layer.on('mouseover', function(e) {
					// e.layer.openPopup();
				});

				this.layer.on('click', function(e) {

					var marker = e.layer,
						p = marker.feature.properties;

					Mapbox.switchSlidePanel($(venueView));
					//Кнопка скрытия объекта
					if(!$('.panelTriggerAddon.objects-view').hasClass('opened')){
						$('.panelTriggerAddon.objects-view').addClass('opened');
					}
					self.loadVenueCard(p.uid, p.id, p.idx);
				});

				this.map.on('click mousedown', function(e) {

					var fPanel = $(filters);
					var vvPanel = $(venueView);

					if (fPanel.hasClass('panelOpened')) self.switchSlidePanel(fPanel);
					if (vvPanel.hasClass('panelOpened')) self.switchSlidePanel(vvPanel);

					// console.log("click");
				});

			},
			// bindEvents - END

			//*******************************************************************************
			//   bindUiEvents
			//*******************************************************************************
			bindUiEvents: function() {

				var self = this;

				// --------------------------------------------
				// Slide Panels
				// --------------------------------------------
				$(document).on('click', '.panelTrigger', function(event) {

					event.preventDefault();

					var panel = $(this).parent();
					//console.log(panel)
					Mapbox.switchSlidePanel(panel);
				});

				// --------------------------------------------
				// Дополнительные кнопки управления
				// --------------------------------------------
				$(document).on('click', '.panelTriggerAddon', function(event) {

					event.preventDefault();

					//console.log(event)

					//Кнопка
					var $btn = $(this);

					switch($btn.data('target')){
						case 'venueView':
							$('.panelTriggerAddon.filters-list').removeClass('hide');
							break;
						case 'filters':
							this.firstClickFilters = !this.firstClickFilters;
							if(this.firstClickFilters) $('.panelTriggerAddon.filters-list').addClass('pressed');
							else                  $('.panelTriggerAddon.filters-list').removeClass('pressed');
							break;
					}

					//Скрыть объект
					if($btn.data('target') != '#modal-paramsorder') {
						$('.panelTriggerAddon.objects-view').removeClass('opened');
					}

					//Определение вызываемой панели панели
					var panel = $('#' + $btn.data('target'));
					//Открываем панель
					Mapbox.switchSlidePanel(panel);
				});

				// --------------------------------------------
				// Регистрация просмотра номера
				// --------------------------------------------
				$(document).on('click', '.showtelext', function(event){

					event.preventDefault();

					//Кнопка
					var $btn = $(this);

					//console.log($btn.data('id'))

					//Открытие
					$.ajax({
						url: '/objects/ajax/',
						type: 'get',
						data: { action: "showtelextviewmobile", obj_id: $btn.data('id') },
						dataType: 'json',
						cache: false,
						success: function(data){
							var response = data;
							//console.log(response);
						},
						error: function(){
							console.log('Ошибка отправки');
						}
					});

					///////////////////
					//Загрузка номера//
					///////////////////

						//Блокируем кнопку
						$btn.attr('disabled', true);
						$btn.html('Идет загрузка...');

						//Отправка уведомления о просмотре номера
						$.ajax({
							url: '/objects/ajax/',
							type: 'POST',
							data: { action: "showtelextmobile", obj_id: $btn.data('id') },
							dataType: 'json',
							cache: false,
							success: function(data){
								var response = data.showtelextmobile_result;
								//console.log(response);
								//Успех
								if(response.status){
									$btn.removeClass('btn-success');
									$btn.removeClass('showtelext');
									$btn.addClass('btn-call');
									$btn.attr('href', 'tel:' + response.phone);
									$btn.text(response.phone);
								}
								//Ошибка
								else {
									$btn.text(response.reason);
								}

								//Деблокируем кнопку
								$btn.attr('disabled', false);
								//setTimeout(function(){ $btn.html('Показать телефон'); }, 3000);
							},
							error: function(){

								console.log('Ошибка отправки');

								$('#showtelext-reason').html('Ошибка - попробуйте позже...');

								//Деблокируем кнопку
								$btn.attr('disabled', false);
								$btn.html('Показать телефон');
							}
						});

					///////////////////////////////
					//Загрузка номера - окончание//
					///////////////////////////////

				});

				// --------------------------------------------
				// Показать еще вперед
				// --------------------------------------------
				$(document).on('click', '.show-more.next', function(event) {
					event.preventDefault();
					self.queryVenueList();
				});

				// --------------------------------------------
				// Показать еще назад
				// --------------------------------------------
				$(document).on('click', '.show-more.prev', function(event) {
					event.preventDefault();
					self.query.from -= self.query.onpage*2;
					if(self.query.from < 0) self.query.from = 0;
					self.queryVenueList();
				});

				// --------------------------------------------
				// Предпросмотр
				// --------------------------------------------
				$(document).on('click', '.card', function(event) {

					event.preventDefault();

					$(".card").removeClass("card-active");
					$(this).closest( ".card" ).addClass("card-active");

					var panel = $(venueView);
					var uid = $(this).data("uid");
					var obj_id = $(this).data("id");
					var idx = $(this).data("idx");

					if (panel.hasClass('panelClosed')) Mapbox.switchSlidePanel(panel);

					//Кнопка фильтра
					if(!$('.panelTriggerAddon.filters-list').hasClass('hide')){
						$('.panelTriggerAddon.filters-list').addClass('hide');
					}

					//Кнопка скрытия объекта
					if(!$('.panelTriggerAddon.objects-view').hasClass('opened')){
						$('.panelTriggerAddon.objects-view').addClass('opened');
					}

					self.loadVenueCard(uid, obj_id, idx);

					$(self.zoomSlider).show();
					$(self.streetview).hide();

					self.layer.eachLayer(function(marker) {
						if (marker.feature.properties.uid === uid) {
							var lat = marker.getLatLng().lat;
							var lng = marker.getLatLng().lng;

							self.map.setView(L.latLng(lat + 0.004, lng - 0.01), 15, {
								animate: false
							});
							marker.openPopup();
						}
					});
				});

				// --------------------------------------------
				// Number Spinner (filter form)
				// --------------------------------------------
				$(document).on('click', '.number-spinner button', function() {
					var btn = $(this),
						oldValue = btn.closest('.number-spinner').find('input').val().trim(),
						newVal = 0;

					if (btn.attr('data-dir') == 'up') {
						newVal = parseInt(oldValue) + 1;
					} else {
						if (oldValue > 1) {
							newVal = parseInt(oldValue) - 1;
						} else {
							newVal = 1;
						}
					}
					btn.closest('.number-spinner').find('input').val(newVal).change();
				});

				// --------------------------------------------
				// Filter
				// --------------------------------------------
				$(self.filterItem).on('change', function() {
					self.filtersChange();
				});

			}, // bindUiEvents - END

			//*******************************************************************************
			//   switchSlidePanel
			//*******************************************************************************
			switchSlidePanel: function(panel) {
				var self = this;
				var easing_effect = 'easeOutBounce';
				var animation_speed = 500;

				// Close Panel
				if (panel.hasClass('panelOpened')) {
					panel.switchClass("panelOpened", "panelClosed", animation_speed, easing_effect,

						function() {
							if ($(self.venueView).hasClass('panelClosed') && !$(self.filtersTrigger).is(":visible")) {
								// Show Filters Trigger
								$(self.filtersTrigger).slideToggle({
									duration: 100,
									easing: easing_effect
								});

								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueViewOpened", animation_speed, easing_effect);
							}

							if ($(self.venueList).hasClass('panelClosed') && !$(self.zoomSlider).hasClass('venueListClosed')) {
								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueListClosed", animation_speed, easing_effect);
							}

						});
				}

				// Open Panel
				if (panel.hasClass('panelClosed')) {
					panel.switchClass("panelClosed", "panelOpened", animation_speed, easing_effect,

						function() {
							if ($(self.venueView).hasClass('panelOpened')) {
								// Hide Filters Trigger
								$(self.filtersTrigger).slideToggle({
									duration: 100,
									easing: easing_effect
								});

								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueViewOpened", animation_speed, easing_effect);

								if ($(self.filters).hasClass('panelOpened')) {
									$(self.filters).switchClass("panelOpened", "panelClosed", animation_speed, easing_effect);
								}
							}

							if ($(self.venueList).hasClass('panelOpened') && $(self.zoomSlider).hasClass('venueListClosed')) {
								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueListClosed", animation_speed, easing_effect);
							}

						});

				}

			}, // switchSlidePanel - END

			//*******************************************************************************
			//   filtersChange
			//*******************************************************************************
			filtersChange: function() {

				var self = this;

				// Rooms Filter
				var filterRoomsChecked = [];
				for (var i = 0; i < this.filterRooms.length; i++) {
					if (this.filterRooms[i].checked) {
						filterRoomsChecked.push(this.filterRooms[i].value);
					}
				}

				// Objtype Filter
				var filterObjtypeChecked = [];
				for (var j = 0; j < this.filterObjtype.length; j++) {
					if (this.filterObjtype[j].checked) {
						filterObjtypeChecked.push(this.filterObjtype[j].value);
					}
				}

				// Guests Filter
				var filterGuests = $('#filter-guests .filterItem').val();

				// Filter-Buttons
				// var filterObjOnTop = parseInt($('#filter-ontop').val(), 10);
				// var filterObjIsempty = parseInt($('#filter-isempty').val(), 10);
				// var filterObjSale = parseInt($('#filter-sale').val(), 10);

				//console.log(filterObjOnTop + " - " + filterObjIsempty + " - " + filterObjSale);

				// console.log(filterObjIsempty);

				// Min Stay Filter
				// var filterMinstay = $('#filter-minstay .filterItem').val();

				// Sleepers
				// var filterSleepers = $('#filter-sleepers .filterItem').val();

				// Price Filter
				var filterPriceMIN = ($(".f-price-slider").val()[0] === undefined) ? 0 : parseInt($(".f-price-slider").val()[0]);
				var filterPriceMAX = ($(".f-price-slider").val()[1] === undefined) ? 5000 : parseInt($(".f-price-slider").val()[1]);

				console.log(filterPriceMIN + "  --  " + filterPriceMAX);

				this.layer.setFilter(function(f) {

					if (
						// filterIBChecked.indexOf(parseInt(f.properties.instantbook)) >= 0 &&
						// parseInt(f.properties.minstay) <= parseInt(filterMinstay) &&
						// (f.properties.isempty == filterObjIsempty || filterObjIsempty === 0) &&
						// (f.properties.top == filterObjOnTop || filterObjOnTop === 0) &&
						// (f.properties.sale == filterObjSale || filterObjSale === 0) &&
						filterRoomsChecked.indexOf(f.properties.rooms) >= 0 &&
						// filterObjtypeChecked.indexOf(f.properties.ot_uid) >= 0 &&
						// parseInt(f.properties.guests) >= parseInt(filterGuests) &&
						f.properties.price >= filterPriceMIN &&
						f.properties.price <= filterPriceMAX
					) {
						$('#' + f.properties.uid).show();
						//console.log('+ #' + f.properties.uid);
						return true;
					} else {
						$('#' + f.properties.uid).hide();
						//console.log('- #' + f.properties.uid);
						return false;
					}

				});

				this.infoPanel();
				this.query.from = 0;
				this.queryVenueList();

				$('#venueList .panelContent').animate({scrollTop: 0}, '500');

				// self.map.fitBounds(self.layer.getBounds());
				// self.map.fitBounds();

			}, // filtersChange - END

			//*******************************************************************************
			//   addMarkers
			//*******************************************************************************
			addMarkers: function() {

				var self = this;

				this.layer.loadURL('/data/venue/' + self.cityUID + '.geojson');

				this.layer.on('layeradd', function(e) {

					var marker = e.layer,
						p = marker.feature.properties,
						icon = marker.feature.properties.icon;

					icon.iconSize = [40, 46];
					icon.iconAnchor = [25, 25];
					icon.popupAnchor = [-6, -25];
					icon.html = p.price;
					// icon.className = "marker-pin pin-" . p.rooms;

					marker.setIcon(L.divIcon(icon));
				});

				//this.fitMarkers();
			},
			// addMarkers - END

			//*******************************************************************************
			//   Info Panel (Show current City name & count objects)
			//*******************************************************************************
			infoPanel: function() {

				var foundObjects = 0;

				this.layer.eachLayer(function(marker) {
					foundObjects++;
				});

				$('.foundItemsLabel').text(foundObjects);
				$('.filterFoundNumber').text(foundObjects);
				$('.foundItemsText').text(this.decOfNum(foundObjects, ['предложение', 'предложения', 'предложений']));

			},

			//*******************************************************************************
			//   fitMarkers
			//*******************************************************************************
			fitMarkers: function() {
				var self = this;

				// *** Fit map to markers ***
				self.layer.on('ready', function() {
					self.map.fitBounds(self.layer.getBounds());
				});
			},
			// fitMarkers - END

			//*******************************************************************************
			//   loadVenueCard
			//*******************************************************************************
			loadVenueCard: function(uid, obj_id, idx) {

				var self = this;

				// Send View to Object Statistic
				$.ajax({
					url: '/objects/ajax/',
					type: 'get',
					data: {action: "sendobjcardview", obj_id: obj_id},
					dataType: 'json',
				});

				// Clear Content
				$(self.venueImgSlider).html("");

				//console.log(idx);

				//Восстановление кнопки номера телефона
				$('#showtelext').addClass('btn-success');
				$('#showtelext').addClass('showtelext');
				$('#showtelext').removeClass('btn-call');
				$('#showtelext').attr('href', '');
				$('#showtelext').text('Показать номер');

				//Точный выбор
				if(idx){

					var p = self.layer._geojson.features[idx].properties;
					var images = p.images;

					console.log(p);

					for (var i = 0; i < images.length; i++) {
						$(self.venueImgSlider).append('<li><img alt="" title="" src="/upload/object/' + p.id + '/x500/' + images[i] + '"></li>');
					}

					self.imgSlider.reloadSlider();

					$('[data-block="id"]').data("id", p.id);
					$('[data-block="price"]').text(p.price);
					$('[data-block="name"]').text(p.title);
					$('[data-block="address"]').text(p.addr_street + ' ' + p.addr_number);
					$('[data-block="checkin"]').text(p.checkin);
					$('[data-block="checkout"]').text(p.checkout);

					$('[data-block="acc_name"]').text(p.acc_name);
					$('[data-block="minstay"]').text(p.minstay);
					$('[data-block="ot_name"]').text(p.ot_name);

					$('[data-block="sqr"]').text(p.sqr);
					$('[data-block="rooms"]').text(p.rooms);
					$('[data-block="floor"]').text(p.addr_floor);

					$('[data-block="bathrooms"]').text(p.bathrooms);
					$('[data-block="bedrooms"]').text(p.bedrooms);
					$('[data-block="sleepers"]').text(p.sleepers);
					$('[data-block="guests"]').text(p.guests);
					$('[data-block="beds"]').text(p.beds);

					$('.venueBookBtn').attr('href', $('base').attr('href') + p.id);
				}
				//Полный проход
				else {
					self.layer.eachLayer(function(layer) {

						var p = layer.toGeoJSON().properties;

						if (p.uid == uid) {

							var images = p.images;

							//console.log(p);

							for (var i = 0; i < images.length; i++) {
								$(self.venueImgSlider).append('<li><img alt="" title="" src="/upload/object/' + p.id + '/x500/' + images[i] + '"></li>');
							}

							self.imgSlider.reloadSlider();

							$('[data-block="id"]').data("id", p.id);
							$('[data-block="price"]').text(p.price);
							$('[data-block="name"]').text(p.title);
							$('[data-block="address"]').text(p.addr_street + ' ' + p.addr_number);
							$('[data-block="checkin"]').text(p.checkin);
							$('[data-block="checkout"]').text(p.checkout);

							$('[data-block="acc_name"]').text(p.acc_name);
							$('[data-block="minstay"]').text(p.minstay);
							$('[data-block="ot_name"]').text(p.ot_name);

							$('[data-block="sqr"]').text(p.sqr);
							$('[data-block="rooms"]').text(p.rooms);
							$('[data-block="floor"]').text(p.addr_floor);

							$('[data-block="bathrooms"]').text(p.bathrooms);
							$('[data-block="bedrooms"]').text(p.bedrooms);
							$('[data-block="sleepers"]').text(p.sleepers);
							$('[data-block="guests"]').text(p.guests);
							$('[data-block="beds"]').text(p.beds);

							$('.venueBookBtn').attr('href', $('base').attr('href') + p.id);
						}
					});
				}
				// slider.reloadSlider();

			}, // loadVenueCard- END

			//*******************************************************************************
			//   loadVenueList
			//*******************************************************************************
			loadVenueList: function() {

				var self = this;

				$(self.venueListContent).html('');

				self.layer.on('ready', function() {

					var foundItems = 0;

					//Все объекты
					self.mapObjects = self.layer._geojson.features;
					foundItems = self.layer._geojson.features.length;

					for( var i = 0; i < self.mapObjects.length; i++ ){
						self.mapObjects[i].properties.idx = i;
						self.layer._geojson.features[i].properties.idx = i;
					}

					//Обновление информации
					$('[data-block="found"]').text(foundItems);
					$('.filterFoundNumber').text(foundItems);
					$('.foundItemsText').text(self.decOfNum(foundItems, ['предложение', 'предложения', 'предложений']));

					console.log('Найдено: ' + foundItems);

					//Запрашиваем список по фильтру
					self.queryVenueList();
				});

			},

			//*******************************************************************************
			// Загрузка списка объектов по фильтру
			//*******************************************************************************
			queryVenueList: function() {

				var self = this;

				console.log('From in: ' + self.query.from);

				//Кнопка назад
				if(self.query.from > 0){
					$('.pagination .prev').attr('disabled', false);
				} else {
					$('.pagination .prev').attr('disabled', true);
				}

				//Тушим блок списка
				$('#card-loading').fadeIn('fast');
				$(self.venueListContent).empty();
				$(self.venueListContent).css('display', 'none');

				//Отсечка объектов
				var limit = self.query.from + self.query.onpage;
				if( parseInt($('[data-block="found"]').html()) <= limit ) {
					limit = parseInt($('[data-block="found"]').html());
				}

				console.log('Limit: ' + limit);

				//Добавляем требуемые объекты
				for (var i = self.query.from; i < limit; i++) {

					//Не превышаем количество объектов
					if(i < self.mapObjects.length){

						//console.log(i);
						var p = self.mapObjects[i].properties;
						var geo = self.mapObjects[i].geometry.coordinates;
						var image = '<img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/upload/object/' + p.id + '/x100/' + p.images[0] + '" src="">';
						var objlistTemplate = $(self.venueListTMPL).html();
						var address = p.addr_street + ' ' + p.addr_number;
						//var ibVisibilityClass = (parseInt(p.instantbook)) ? "" : "hide";
						var isempty = ( p.isempty != undefined && p.isempty == 1 ) ? '<div class="isempty-label">Свободна</div>' : '';

						//Время поднятия
						if( p.ontop_mark != undefined && p.ontop_mark == 1 ) {
							var ontop_date = new Date();
								ontop_date.setTime(p.ontop*1000);
							var ontop = '<div class="ontop">Поднято: ' + ontop_date.toLocaleString() + ' </div>';
						} else {
							var ontop = '';
						}

						var content = objlistTemplate
							.replace(/{{uid}}/g, p.uid)
							.replace(/{{obj_id}}/g, p.id)
							.replace(/{{idx}}/g, p.idx)
							.replace(/{{title}}/g, p.title)
							.replace(/{{ontop}}/g, ontop)
							.replace(/{{address}}/g, address)
							.replace(/{{rooms}}/g, p.rooms)
							.replace(/{{guests}}/g, p.guests)
							.replace(/{{sqr}}/g, p.sqr)
							.replace(/{{isempty}}/g, isempty)
							.replace(/{{image}}/g, image)
							.replace(/{{price}}/g, p.price)
							.replace(/{{lat}}/g, geo[1])
							.replace(/{{lng}}/g, geo[0]);

						//Добавляем карточку
						$(self.venueListContent).append(content);

						//Выделение поднятого объекта
						if( p.ontop_mark != undefined && p.ontop_mark == 1 ){
							$('#' + p.uid).addClass('ontop');
						}
					}
				}//for

				//Проявляем блок списка
				setTimeout(function() { $('#card-loading').fadeOut('fast'); }, 1000);
				$(self.venueListContent).fadeIn('fast');

				//Увеличиваем отсчет
				self.query.from += self.query.onpage;

				//Кнопка вперед
				if( self.query.from >= parseInt($('[data-block="found"]').html()) ) {
					$('.pagination .next').attr('disabled', true);
				} else {
					$('.pagination .next').attr('disabled', false);
				}

				$('#venueList .panelContent').animate({scrollTop: 0}, '500');

				console.log('From out: ' + self.query.from);
			},

			//*******************************************************************************
			//   reloadVenueList
			//*******************************************************************************
			reloadVenueList: function() {

				var self = this;

				$('.card').hide();

				self.layer.eachLayer(function(layer) {
					var p = layer.toGeoJSON().properties;
					$('#' + p.uid).show();
				});

			},

			//*******************************************************************************
			//   parseURL
			//*******************************************************************************
			parseURL: function() {

				var self = this;

				var url = document.location.href;

				var parser = document.createElement('a');
				parser.href = url;
				var pathname = parser.pathname;

				var params = pathname.split("/");

				var pairs = [];

				for (var i = 0; i < params.length; i++) {
					if (i % 2 !== 0) pairs[params[i]] = params[i + 1];
				}

				return pairs;

			}, // parseURL- END


			//*******************************************************************************
			//   Dec Of Num
			//*******************************************************************************
			//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
			decOfNum: function(number, titles) {
				cases = [2, 0, 1, 1, 1, 2];
				return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
			},
		};

		Mapbox.init();

	})();

	////Срочное заселение
	//Заезд
	$('#paramsorder-checkin').datetimepicker({
		pickTime: false
	});
	//Выезд
	$('#paramsorder-checkout').datetimepicker({
		pickTime: false
	});

	////Открытие формы по хешу в УРЛ
	var url = window.location;
	if(url.hash == '#paramsorder'){
		$('#modal-paramsorder').modal('show');
	}
});


// ViewHotOrder
//*******************************************************************************
var firstViewParamsOrder = true;
$("#paramsorder-toggle").click(function(e) {
	e.preventDefault();

	//Только первое открытие
	if(firstViewParamsOrder){

		//Отправка уведомления о просмотре номера
		$.ajax({
			url: '/objects/ajax/',
			type: 'get',
			data: { action: "viewparamsorder", addr_city_uid: $('input[name=addr_city_uid]').val() },
			dataType: 'json',
			cache: false,
			success: function(data){
				var response = data;
				//console.log(response);
			},
			error: function(){
				console.log('Ошибка отправки');
			}
		});

		firstViewParamsOrder = false;
	}

});

// SendParamsOrder
//*******************************************************************************
function sendParamsOrder(){

	//Блокируем кнопку
	$('#paramsorder-button').attr('disabled', true);
	$('#paramsorder-button').html('Идет отправка...');

	//Данные пользователя
	var paramsorder = {
		name: $('#paramsorder-name').val(),
		phone: $('#paramsorder-phone').val(),
		checkin: $('#paramsorder-checkin').val(),
		checkout: $('#paramsorder-checkout').val(),
		guests: $('#paramsorder-guests').val(),
		price: $('#paramsorder-price').val(),
		code: $('#paramsorder-code').val(),
		district: $('#paramsorder-district').val(),
		comment: $('#paramsorder-comment').val()
	};

	//Отправка уведомления о просмотре номера
	$.ajax({
		url: '/objects/ajax/',
		type: 'POST',
		data: { action: "paramsorder", addr_city_uid: $('input[name=addr_city_uid]').val(), addr_city: $('input[name=addr_city]').val(), paramsorder: paramsorder },
		dataType: 'json',
		cache: false,
		success: function(data){
			var response = data.paramsorder_result;

			//Подтверждение номера
			if(response.step == 'code'){
				$('.paramsorder-form-info').hide();
				$('.paramsorder-form-code').show();
				$('#paramsorder-reason').html(response.reason);
				$('#paramsorder-form-code-onphone').text(response.phone);
			}
			//Возврат на заполнение формы
			else if(response.step == 'info') {
				$('.paramsorder-form-info').show();
				$('.paramsorder-form-code').hide();
			}

			//Успех
			if(response.status){
				$('#paramsorder-reason').html(response.reason);
				$('#paramsorder-name').val('');
				$('#paramsorder-phone').val('');
				$('#paramsorder-guests').val('');
				$('#paramsorder-price').val('');
				$('#paramsorder-checkin').val('');
				$('#paramsorder-checkout').val('');
				$('#paramsorder-code').val('');
				$('#paramsorder-district').val('');
				$('#paramsorder-comment').val('');
				$('#modal-paramsorder .form-group.floating-label-form-group').removeClass('floating-label-form-group-with-value');
				$('#modal-paramsorder .query').hide();
				$('#paramsorder-reason').css('font-weight', 'bold');
				$('#modal-paramsorder .modal-body').fadeOut('fast');
				setTimeout(function(){
					$('#modal-paramsorder').modal('hide');
				}, 3000);
				$('#modal-paramsorder').animate({ scrollTop: 0 }, '500');
			}
			//Ошибка
			else {
				$('.paramsorder-info').hide();
				$('#paramsorder-reason').html(response.reason);

				$('#modal-paramsorder').animate({ scrollTop: $(document).height() }, '500');
			}

			//Деблокируем кнопку
			$('#paramsorder-button').attr('disabled', false);
			$('#paramsorder-button').html('Отправить');
		},
		error: function(){

			console.log('Ошибка отправки');

			$('.paramsorder-info').hide();
			$('#paramsorder-reason').html('Ошибка получения данных - попробуйте позже');

			//Деблокируем кнопку
			$('#paramsorder-button').attr('disabled', false);
			$('#paramsorder-button').html('Отправить');

			$('#modal-paramsorder').animate({ scrollTop: $(document).height() }, '500');
		}
	});
}
function sendParamsOrderRollback(){
	$('.paramsorder-form-info').show();
	$('.paramsorder-form-code').hide();
	$('#paramsorder-code').val('');
}