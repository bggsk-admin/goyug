/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {

	//Работа с картой
	(function() {

		var Mapbox = {

			//*******************************************************************************
			//   Define some vars
			//*******************************************************************************
			topNavbarH: 100,
			mapDivName: "map-canvas",
			mapDiv: $('#map-canvas'),

			mapName: 'malamdar.579e4606',

			// GoYug.Pub
			token: 'pk.eyJ1IjoibWFsYW1kYXIiLCJhIjoiNnpNNFJEUSJ9.tAP-en7mfiSHAB4bD4gD1g',
			// token: '',

			defaultPos: [parseFloat($('#objects-map').data('lat')), parseFloat($('#objects-map').data('lng'))],

			onmap_filters: $('#onmap-filters'),

			venueList: '#venueList',
			venueListContent: '#venueList .panelContent',
			venueListTMPL: '#venueListTMPL',
			venuePopupTMPL: '#venuePopupTMPL',
			venuePhoto: '.photoContainer',

			venueView: '#venueView',
			venueViewContent: '#venueView .panelContent',
			venueImgSlider: "#venueImgSlider",

			filters: '#filters',
			filtersTrigger: '#filters .panelTrigger',
			filterItem: '.filterItem',
			filterRooms: $('#filter-rooms input[type="checkbox"]'),
			filterObjtype: $('#filter-objtype input[type="checkbox"]'),
			filterObjIsempty: $('#filter-isempty input[type="checkbox"]'),

			streetview: '#streetview',

			zoomSlider: '.leaflet-control-zoomslider',
			geojson: geojson,


			//*******************************************************************************
			//   Init
			//*******************************************************************************
			init: function() {

				// var urlParams = this.parseURL();
				this.cityUID = $('#objects-map').data('cityuid');
				this.cityZoom = $('#objects-map').data('zoom');
				this.priceMINdef = $('#objects-map').data('minprice');
				this.priceMAXdef = $('#objects-map').data('maxprice');

				L.mapbox.accessToken = this.token;

				this.setsize();

				this.map = L.mapbox
					// .map(this.mapDivName, this.mapName, {
					.map(this.mapDivName, null, {
						zoomControl: false,
						minZoom: 5,
						tileSize: 512,
						zoomOffset: -1
					})
					.addLayer(L.mapbox.styleLayer('mapbox://styles/malamdar/ckgv2uiel1adk19qjq9xuawjd'))
					.setView(this.defaultPos, this.cityZoom);


					// {
					// 	zoomControl: false,
					// 	minZoom: 5,
					// 	tileSize: 512,
					// 	zoomOffset: -1
					// }

				L.control.zoomslider().addTo(this.map);

				// Switch OFF Zoomer
				// this.map.zoomControl.removeFrom(this.map);

				this.layer = L.mapbox.featureLayer()
				.addTo(this.map);

				this.addMarkers();
				// this.addPopups();
				// this.loadVenueList();

				// Disable map controls
				// this.map.dragging.disable();
				// this.map.touchZoom.disable();
				// this.map.doubleClickZoom.disable();
				this.map.scrollWheelZoom.disable();

				this.bindEvents();
				this.bindUiEvents();

				// this.imgSlider = $(venueImgSlider).bxSlider({
				//     minSlides: 1,
				//     maxSlides: 1,
				//     moveSlides: 1,
				//     slideMargin: 0,
				//     infiniteLoop: false,
				//     hideControlOnEnd: true,
				//     pager: false,
				//     auto: false,
				//     autoControls: false,
				// });

				this.infoPanel();

				// this.myFitBounds();
				// this.map.fitBounds();
				// this.fitMarkers();

				//*******************************************************************************
				//   noUiSlider - Price slider
				//*******************************************************************************
				$(".f-price-slider").noUiSlider({
					start: [this.priceMINdef, this.priceMAXdef],
					step: 100,
					connect: true,
					range: {
						'min': [this.priceMINdef],
						'max': [this.priceMAXdef]
					},
					serialization: {
						lower: [
							$.Link({
								target: $('#f-price-min'),
								method: "text"
							}),
						],
						upper: [
							$.Link({
								target: $('#f-price-max'),
								method: "text"
							}),
						],
						format: {
							decimals: 0,
							mark: ','
						}
					}
				});

				$("#f-price-min-label").text(this.priceMINdef);
				$("#f-price-max-label").text(this.priceMAXdef);
				$("#f-price-mid-label").text(((this.priceMAXdef - this.priceMINdef) / 2 + this.priceMINdef));


			},
			// Init - END

			//*******************************************************************************
			//   setsize
			//*******************************************************************************
			setsize: function() {

				this.mapDiv
					.css("width", $(window).width())
					.css("height", $(window).height() - this.topNavbarH);

				$(this.venueList + ", " + this.venueListContent)
					.css("height", $(window).height() - this.topNavbarH);

				$(this.venueView + ", " + this.venueViewContent)
					.css("height", $(window).height() - this.topNavbarH);

				$(this.streetview)
					.css("height", $(window).height() - this.topNavbarH)
					.css("width", $(window).width() - $(this.venueList).width());

			},
			// setsize - END

			//*******************************************************************************
			//   Fit Bounds with offset
			//*******************************************************************************
			myFitBounds: function() {

				var self = this;
				var bounds = [];
				var boundsZoom = 0;

				var topMarkersLatLng = [];

				self.layer.eachLayer(function(marker)
				{
					var p = marker.feature.properties;
					var geo = marker.feature.geometry.coordinates;

					if(parseInt( p.top ) == 1)
					{
						// topMarkersLatLng[p.id] = [geo[1], geo[0]];
						topMarkersLatLng.push([geo[1], geo[0]]);
					}
				});

				if(topMarkersLatLng.length < 1)
				{
					bounds = this.layer.getBounds();
					boundsZoom = this.map.getBoundsZoom(bounds);
				} else {
					bounds = new L.LatLngBounds(topMarkersLatLng);
					boundsZoom = this.map.getBoundsZoom(bounds) - 3;

				}

				// console.log(topMarkersLatLng.length);
				// console.log(bounds);

				var boundsCenter = L.latLngBounds(bounds).getCenter();

				this.map.setView([boundsCenter.lat, boundsCenter.lng], boundsZoom);
			},
			// setsize - END

			//*******************************************************************************
			//   bindEvents
			//*******************************************************************************
			bindEvents: function() {
				var self = this;

				$(window).on('resize', $.proxy(function() {
					this.setsize();
					this.fitMarkers();
				}, this));

				// this.layer.on('mouseover', function(e) {
					// e.layer.openPopup();
				// });

				/*
							this.layer.on('mouseout', function(e) {
								e.layer.closePopup();
							});
				*/

				this.layer.on('click', function(e) {

					var marker = e.layer,
						p = marker.feature.properties;

					$(self.venueListContent).scrollTo("#" + p.uid, 300, {
						// easing: 'easeOutBounce'
					});

					$(".card").removeClass("card-active");
					$("#" + p.uid).addClass("card-active");

					e.layer.openPopup();
				});

				this.map.on('click mousedown', function(e) {

					var fPanel = $(filters);
					var vvPanel = $(venueView);

					if (fPanel.hasClass('panelOpened')) self.switchSlidePanel(fPanel);
					if (vvPanel.hasClass('panelOpened')) self.switchSlidePanel(vvPanel);

					// console.log("click");
				});

				this.map.on('dragend',         function (e) {
					console.log('    ] dragend');
					console.log(self.map.getZoom());
					console.log(self.map.getCenter());
				});


			},
			// bindEvents - END

			//*******************************************************************************
			//   bindUiEvents
			//*******************************************************************************
			bindUiEvents: function() {
				var self = this;

				// --------------------------------------------
				// Slide Panels
				// --------------------------------------------
				$(document).on('click', '.panelTrigger', function(event) {

					event.preventDefault();

					var panel = $(this).parent();
					//console.log(panel)
					Mapbox.switchSlidePanel(panel);
				});

				// --------------------------------------------
				// Фильтр-кнопка "Свободно Сегодня"
				// --------------------------------------------
				$(document).on('click', '.stat-orders', function(event) {

					event.preventDefault();

					var filter = $("#" + $(this).data('filter'));
					var filterVal = filter.val();

					if(filterVal == 1)
					{
						filter.val(0);
						$(this).css('background-color', '#fff9d5');
					} else {
						filter.val(1);
						$(this).css('background-color', '#04e267');
					}

					self.filtersChange();
					self.fitMarkers();

				});

				// --------------------------------------------
				// Фильтр-кнопка "Выделенные объекты"
				// --------------------------------------------
				$(document).on('click', '#stat-orders', function(event) {

					event.preventDefault();


					if($('#filter-ontop .filterItem').is(':checked'))
					{
						$('#filter-ontop .filterItem').prop('checked', false);
						$('#stat-orders').css('background-color', '#fff9d5');
					} else {
						$('#filter-ontop .filterItem').prop('checked', true);
						$('#stat-orders').css('background-color', '#04e267');
					}


					self.filtersChange();
					self.fitMarkers();

				});

				// --------------------------------------------
				// Дополнительные кнопки управления
				// --------------------------------------------
				$(document).on('click', '.panelTriggerAddon', function(event) {

					event.preventDefault();

					//console.log(event)

					//Кнопка
					var btn = event.target;

					//Меняем кнопку
					if($(btn).hasClass('pressed')){
						$(btn).removeClass('pressed');
						//Текст кнопки
						if(btn.dataset.target == 'venueList'){
							$(btn).html('Карта');
						} else if (btn.dataset.target == 'filters'){
							$(btn).html('Фильтр');
						}
					} else {
						$(btn).addClass('pressed');
						//Текст кнопки
						if(btn.dataset.target == 'venueList'){
							$(btn).html('Объекты');
						} else if (btn.dataset.target == 'filters'){
							$(btn).html('Скрыть фильтр');
						}
					}

					//Скрыть объект
					$('.panelTriggerAddon.objects-view').removeClass('opened');

					//Определение вызываемой панели панели
					var panel = $('#' + btn.dataset.target);
					//Открываем панель
					Mapbox.switchSlidePanel(panel);
				});

				// --------------------------------------------
				// Click on Venue Name -> Show VenueView Panel
				// --------------------------------------------
				/*            $(document).on('click', '.venueName, .photoContainer', function(event) {
								event.preventDefault();

								var panel = $(venueView);
								var uid = $(this).data("uid");

								if(panel.hasClass('panelClosed')) Mapbox.switchSlidePanel(panel);

								self.loadVenueCard(uid);

								$(self.zoomSlider).show();
								$(self.streetview).hide();

								self.layer.eachLayer(function(marker)
								{
									if (marker.feature.properties.uid === uid)
									{
										var lat = marker.getLatLng().lat;
										var lng = marker.getLatLng().lng;

										self.map.setView(L.latLng(lat + 0.004, lng - 0.01), 15, {animate: false});
										marker.openPopup();
									}
								});


							});
				*/
				// --------------------------------------------
				//  Click on Venue Card
				//  -------------------------------------------
				// $(document).on('click', '.venueName', function(event) {
					// event.preventDefault();
				// });

				$(document).on('click', '.venueName, .photoCol', function(event) {

					event.preventDefault();

					$(".card").removeClass("card-active");
					$(this).closest( ".card" ).addClass("card-active");

					var panel = $(venueView);
					var uid = $(this).data("uid");
					var obj_id = $(this).data("id");

					if (panel.hasClass('panelClosed')) Mapbox.switchSlidePanel(panel);

					//Кнопка скрытия объекта
					if(!$('.panelTriggerAddon.objects-view').hasClass('opened')){
						$('.panelTriggerAddon.objects-view').addClass('opened');
					}

					self.loadVenueCard(uid, obj_id);

					$(self.zoomSlider).show();
					$(self.streetview).hide();

					self.layer.eachLayer(function(marker) {
						if (marker.feature.properties.uid === uid) {
							var lat = marker.getLatLng().lat;
							var lng = marker.getLatLng().lng;

							self.map.setView(L.latLng(lat + 0.004, lng - 0.01), 15, {
								animate: false
							});
							marker.openPopup();
						}
					});
				});

				// --------------------------------------------
				//  MouseOver on Venue Card (open marker popup)
				//  -------------------------------------------
				/*            $(document).on('mouseover', '.card', function(event) {

								var uid = $(this).data("uid");

								if(!$(self.venueView).hasClass('panelOpened'))
								{
									self.layer.eachLayer(function(marker)
									{
										if (marker.feature.properties.uid === uid)
										{
											var lat = marker.getLatLng().lat;
											var lng = marker.getLatLng().lng;

											self.map.setView(L.latLng(lat + 0.004, lng - 0.01), 15, {animate: false});
											marker.openPopup();
										}
									});
								}
							});
				*/
				// --------------------------------------------
				// Open Streetview
				// --------------------------------------------
				$(document).on('click', '.openStreetview', function(event) {
					event.preventDefault();

					var lat = $(this).data("lat");
					var lng = $(this).data("lng");
					var address = $(this).data("address");
					var panorama;

					$(self.streetview + ' .svNotAvailable').hide();

					$(self.zoomSlider).hide();
					$(self.streetview).show();
					$(self.streetview + ' .svLabelAddress').html(address);

					var sv = new google.maps.StreetViewService();
					var loc = new google.maps.LatLng(lat, lng);

					sv.getPanoramaByLocation(loc, 50, function(data, status) {
						// console.log(status);
						// console.log(data.location.latLng);

						var panoramaOptions = {
							position: loc,
							pov: {
								heading: 34,
								pitch: 10
							},
							linksControl: false,
							panControl: false,
							zoomControl: false,
							addressControlOptions: {
								position: google.maps.ControlPosition.BOTTOM_CENTER
							},
							visible: true,
						};

						$('#svPano').show();
						panorama = new google.maps.StreetViewPanorama(document.getElementById('svPano'), panoramaOptions);

						if (status == google.maps.StreetViewStatus.OK) {

						} else {

							$('#svPano').hide();
							$(self.streetview + ' .svNotAvailable').show();

						}
					});
				});

				// --------------------------------------------
				// Close Streetview
				// --------------------------------------------
				$(document).on('click', '.closeStreetview', function(event) {
					event.preventDefault();

					$(self.zoomSlider).show();
					$(self.streetview).hide();
				});

				// --------------------------------------------
				// Number Spinner (filter form)
				// --------------------------------------------
				$(document).on('click', '.number-spinner button', function() {
					var btn = $(this),
						oldValue = btn.closest('.number-spinner').find('input').val().trim(),
						newVal = 0;

					if (btn.attr('data-dir') == 'up') {
						newVal = parseInt(oldValue) + 1;
					} else {
						if (oldValue > 1) {
							newVal = parseInt(oldValue) - 1;
						} else {
							newVal = 1;
						}
					}
					btn.closest('.number-spinner').find('input').val(newVal).change();
				});

				// --------------------------------------------
				// Filter
				// --------------------------------------------
				$(self.filterItem).on('change', function() {
					self.filtersChange();
					self.fitMarkers();
				});

			}, // bindUiEvents - END

			//*******************************************************************************
			//   switchSlidePanel
			//*******************************************************************************
			switchSlidePanel: function(panel) {
				var self = this;
				var easing_effect = 'easeOutBounce';
				var animation_speed = 500;

				// Close Panel
				if (panel.hasClass('panelOpened')) {
					panel.switchClass("panelOpened", "panelClosed", animation_speed, easing_effect,

						function() {
							if ($(self.venueView).hasClass('panelClosed') && !$(self.filtersTrigger).is(":visible")) {
								// Show Filters Trigger
								$(self.filtersTrigger).slideToggle({
									duration: 100,
									easing: easing_effect
								});

								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueViewOpened", animation_speed, easing_effect);
							}

							if ($(self.venueList).hasClass('panelClosed') && !$(self.zoomSlider).hasClass('venueListClosed')) {
								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueListClosed", animation_speed, easing_effect);
							}

						});
				}

				// Open Panel
				if (panel.hasClass('panelClosed')) {
					panel.switchClass("panelClosed", "panelOpened", animation_speed, easing_effect,

						function() {
							if ($(self.venueView).hasClass('panelOpened')) {
								// Hide Filters Trigger
								$(self.filtersTrigger).slideToggle({
									duration: 100,
									easing: easing_effect
								});

								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueViewOpened", animation_speed, easing_effect);

								if ($(self.filters).hasClass('panelOpened')) {
									$(self.filters).switchClass("panelOpened", "panelClosed", animation_speed, easing_effect);
								}
							}

							if ($(self.venueList).hasClass('panelOpened') && $(self.zoomSlider).hasClass('venueListClosed')) {
								// Shift Zoom Slider
								$(self.zoomSlider).toggleClass("venueListClosed", animation_speed, easing_effect);
							}

						});

				}

			}, // switchSlidePanel - END

			//*******************************************************************************
			//   filtersChange
			//*******************************************************************************
			filtersChange: function() {

				var self = this;

				// Rooms Filter
				var filterRoomsChecked = [];
				for (var i = 0; i < this.filterRooms.length; i++) {
					if (this.filterRooms[i].checked) {
						filterRoomsChecked.push(this.filterRooms[i].value);
					}
				}

				// Objtype Filter
				var filterObjtypeChecked = [];
				for (var j = 0; j < this.filterObjtype.length; j++) {
					if (this.filterObjtype[j].checked) {
						filterObjtypeChecked.push(this.filterObjtype[j].value);
					}
				}

				// Guests Filter
				var filterGuests = $('#filter-guests .filterItem').val();

				// Filter-Buttons
				var filterObjOnTop = parseInt($('#filter-ontop').val(), 10);
				var filterObjIsempty = parseInt($('#filter-isempty').val(), 10);
				var filterObjSale = parseInt($('#filter-sale').val(), 10);

				console.log(filterObjOnTop + " - " + filterObjIsempty + " - " + filterObjSale);


				// console.log(filterObjIsempty);

				// Min Stay Filter
				// var filterMinstay = $('#filter-minstay .filterItem').val();

				// Sleepers
				// var filterSleepers = $('#filter-sleepers .filterItem').val();

				// Price Filter
				var filterPriceMIN = ($(".f-price-slider").val()[0] === undefined) ? 0 : parseInt($(".f-price-slider").val()[0]);
				var filterPriceMAX = ($(".f-price-slider").val()[1] === undefined) ? 5000 : parseInt($(".f-price-slider").val()[1]);


				console.log(filterPriceMIN + "  --  " + filterPriceMAX);

				this.layer.setFilter(function(f) {

					if (
						// filterIBChecked.indexOf(parseInt(f.properties.instantbook)) >= 0 &&
						// parseInt(f.properties.minstay) <= parseInt(filterMinstay) &&

						(f.properties.isempty == filterObjIsempty || filterObjIsempty === 0) &&
						(f.properties.top == filterObjOnTop || filterObjOnTop === 0) &&
						(f.properties.sale == filterObjSale || filterObjSale === 0) &&

						filterRoomsChecked.indexOf(f.properties.rooms) >= 0 &&
						// filterObjtypeChecked.indexOf(f.properties.ot_uid) >= 0 &&
						// parseInt(f.properties.guests) >= parseInt(filterGuests) &&
						f.properties.price >= filterPriceMIN &&
						f.properties.price <= filterPriceMAX
					) {
						$('#' + f.properties.uid).show();

						return true;

					} else {
						$('#' + f.properties.uid).hide();
						return false;
					}

				});

				this.infoPanel();

				// self.map.fitBounds(self.layer.getBounds());
				// self.map.fitBounds();


			}, // filtersChange - END

			//*******************************************************************************
			//   addMarkers
			//*******************************************************************************
			addMarkers: function() {

				var self = this;

				// this.layer.loadURL('/data/venue/' + self.cityUID + '.geojson');

				// this.layer.on('layeradd', function(e) {

				//     var marker = e.layer,
				//         p = marker.feature.properties,
				//         icon = marker.feature.properties.icon;

				//         icon.iconSize = [40, 46];
				//         icon.iconAnchor = [25, 25];
				//         icon.popupAnchor = [-6, -25];
				//         icon.html = p.price;

				//         marker.setIcon(L.divIcon(icon));
				// });

				var foundObjects = 0;

				this.layer
				.on('layeradd', function(e) {

					var   marker = e.layer,
							p = marker.feature.properties,
							icon = marker.feature.properties.icon;

					var popupContent = $("#popup-" + p.uid).html();

					if(p.top == 1) marker.setZIndexOffset(9999);
					else marker.setZIndexOffset(1);

					marker.setIcon(L.divIcon(icon));

					marker.bindPopup(popupContent, {
						closeButton: false,
						minWidth: 400,
						keepInView: false,
					});


				});

				/*                .on('ready', function(e) {

									var clusterGroup = new L.MarkerClusterGroup();
									e.target.eachLayer(function(layer) {
										clusterGroup.addLayer(layer);
									});

									self.map.addLayer(clusterGroup);
								})
				*/

				this.layer.setGeoJSON(self.geojson);

			},
			// addMarkers - END

			//*******************************************************************************
			//   Info Panel (Show current City name & count objects)
			//*******************************************************************************
			infoPanel: function() {

				var foundObjects = 0;

				this.layer.eachLayer(function(marker) {
					foundObjects++;
				});

				$('.foundItemsLabel').text(foundObjects);
				$('.filterFoundNumber').text(foundObjects);
				//$('.foundItemsText').text(this.decOfNum(foundObjects, ['предложение', 'предложения', 'предложений']));

			},

			//*******************************************************************************
			//   addPopups
			//*******************************************************************************
			addPopups: function() {

				var self = this;

				self.layer.on('layeradd', function(e) {

					var marker = e.layer;
					var p = marker.feature.properties;
					var geo = marker.feature.geometry.coordinates;

					// var popupTemplate = $(self.venuePopupTMPL).html();

					// var address = p.addr_street + ' ' + p.addr_number;
					// var image = '<img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/upload/object/' + p.id + '/x100/' + p.images[0] + '" src="">';

					// var content = popupTemplate
					//     .replace(/{{image}}/g, image)
					//     .replace(/{{uid}}/g, p.uid)
					//     .replace(/{{price}}/g, p.price)
					//     .replace(/{{address}}/g, address)
					//     .replace(/{{rooms}}/g, p.rooms)
					//     .replace(/{{guests}}/g, p.guests)
					//     .replace(/{{sqr}}/g, p.sqr)

					// .replace(/{{lat}}/g, geo[1])
					//     .replace(/{{lng}}/g, geo[0])

					// .replace(/{{rating}}/g, p.rating)
					//     .replace(/{{title}}/g, p.title);

					// console.log(content);

					// Create custom popup content
									var popupContent =
										'<div id="' + feature.properties.id + '" class="popup">' +
											'<h2>' + feature.properties.title + '</h2>ыыы' +
										'</div>';

					// http://leafletjs.com/reference.html#popup

					// console.log($("popup-" + p.uid).html());

					// marker.bindPopup($("popup-" + p.uid).html(), {
					marker.bindPopup(popupContent, {
						closeButton: false,
						minWidth: 400,
						keepInView: false,
					});

				}); // on layeradd

			}, // addPopups - END

			//*******************************************************************************
			//   fitMarkers
			//*******************************************************************************
			fitMarkers: function() {
				var self = this;

				// *** Fit map to markers ***
				self.layer.on('ready', function() {

					var bounds = self.layer.getBounds();

					var zoom = self.map.getBoundsZoom(bounds);

					var pxlBounds = self.map.getPixelBounds();
					var bottomLeft = pxlBounds.getBottomLeft();
					var topRight = pxlBounds.getTopRight();

					var swBound = self.map.unproject([parseInt(bottomLeft.x), parseInt(bottomLeft.y)]);
					var neBound = self.map.unproject([parseInt(topRight.x), parseInt(topRight.y)]);

					var trueBounds = new L.LatLngBounds(swBound, neBound);

					// self.map.fitBounds(trueBounds);
					// self.map.fitBounds(bounds);

					var trueZoom = self.map.getBoundsZoom(trueBounds);
					self.map.setView(L.latLngBounds(trueBounds).getCenter(), trueZoom);

					// console.log(bounds);
					// console.log(trueBounds);
					// console.log(swBound + " -- " + trueZoom);

					// console.log(bottomLeft.x + " -- " + topRight.x);
					// console.log(swBound);
					// console.log(self.layer.getBounds().length);
				});

			},
			// fitMarkers - END

			//*******************************************************************************
			//   loadVenueCard
			//*******************************************************************************
			loadVenueCard: function(uid, obj_id) {

				var self = this;

				// Send View to Object Statistic
				$.ajax(
				{
					url: '/objects/ajax/',
					type: 'get',
					data: {action: "sendobjcardview", obj_id: obj_id},
					dataType: 'json',
				});

				// Clear Content
				$(self.venueImgSlider).html("");

				self.layer.eachLayer(function(layer) {

					var p = layer.toGeoJSON().properties;

					if (p.uid == uid) {

						// console.log(p);

						var venueCard = $("#venueViewTMPL").html();

						var images = p.images;
						var imagesHTML = "";

						for (var i = 0; i < images.length; i++) {
							imagesHTML += '<li><img alt="" title="" src="/upload/object/' + p.id + '/x500/' + images[i] + '"></li>';
						}

						var content = venueCard
							.replace(/{{price}}/g, p.price)
							.replace(/{{title}}/g, p.title)
							.replace(/{{address}}/g, 'г. ' + p.addr_city + ', ' + p.addr_street + ' ' + p.addr_number)

							.replace(/{{checkin}}/g, p.checkin)
							.replace(/{{checkout}}/g, p.checkout)
							.replace(/{{minstay}}/g, p.minstay)

							.replace(/{{sqr}}/g, p.sqr)
							.replace(/{{rooms}}/g, p.rooms)
							.replace(/{{sleepers}}/g, p.sleepers)
							.replace(/{{guests}}/g, p.guests);

							// console.log(imagesHTML);

						$("#venueView .panelContent").html(content);
						$("#venueImgSlider").html(imagesHTML);

						// self.imgSlider.reloadSlider();

						$(self.venueImgSlider).bxSlider({
							minSlides: 1,
							maxSlides: 1,
							moveSlides: 1,
							slideMargin: 0,
							infiniteLoop: false,
							hideControlOnEnd: true,
							pager: false,
							auto: false,
							autoControls: false,
						});

						$('.venueBookBtn').attr("href", p.link);


					   //  Object Calendar
						//*******************************************************************************
						/*
						var date = new Date();
						var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
						var disabledDays = p.calendar;
						console.log(p.calendar);

						$( "#venueCalendar" ).datepicker( "destroy" );
						$( "#venueCalendar" ).datepicker({

							disabledDays: disabledDays,
							numberOfMonths: 1,
							dayNamesMin: [ "ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" ],
							monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
							currentText: "Сегодня",
							firstDay: 1,
							buttonImageOnly: true,
							showButtonPanel: false,
							minDate: new Date(y, m, d),
							hideIfNoPrevNext: true,
							nextText: "<i class=icon-chevron-right></i>",
							prevText: "<i class=icon-chevron-left></i>",
							beforeShowDay: function(date) {

								var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();

								for (i = 0; i < disabledDays.length; i++) {
									if($.inArray(y + '-' + (m+1) + '-' + d, disabledDays) != -1) {
										return [true, 'obj-close-event', ''];
									}
								}
								return [true];
							},


						});
						$( "#venueCalendar" ).datepicker( "refresh" );
						*/
					}
				});


				// slider.reloadSlider();

			}, // loadVenueCard- END

			//*******************************************************************************
			//   loadVenueList
			//*******************************************************************************
			loadVenueList: function() {

				var self = this;

				$(self.venueListContent).html("");

				self.layer.on('ready', function() {

					var foundItems = 0;
					self.layer.eachLayer(function(layer) {

						var p = layer.toGeoJSON().properties;
						var geo = layer.toGeoJSON().geometry.coordinates;

						// var p = layer.feature.properties;
						var image = '<img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/upload/object/' + p.id + '/x100/' + p.images[0] + '" src="">';

						var objlistTemplate = $(self.venueListTMPL).html();

						var address = p.addr_street + ' ' + p.addr_number;
						var ibVisibilityClass = (parseInt(p.instantbook)) ? "" : "hide";

						var content = objlistTemplate
							.replace(/{{image}}/g, image)
							.replace(/{{uid}}/g, p.uid)
							.replace(/{{obj_id}}/g, p.id)
							.replace(/{{price}}/g, p.price)
							.replace(/{{address}}/g, address)
							.replace(/{{rooms}}/g, p.rooms)
							.replace(/{{guests}}/g, p.guests)
							.replace(/{{sqr}}/g, p.sqr)

						.replace(/{{lat}}/g, geo[1])
							.replace(/{{lng}}/g, geo[0])

						.replace(/{{rating}}/g, p.rating)
							// .replace(/{{instantbook}}/g, ibVisibilityClass)
							.replace(/{{title}}/g, p.title);

						$(self.venueListContent).append(content);

						foundItems++;

					});

					// console.log(foundItems);
					$('[data-block="found"]').text(foundItems);
					$('.filterFoundNumber').text(foundItems);
					//$('.foundItemsText').text(self.decOfNum(foundItems, ['предложение', 'предложения', 'предложений']));

				});

			}, // loadVenueList - END

			//*******************************************************************************
			//   parseURL
			//*******************************************************************************
			parseURL: function() {

				var self = this;

				var url = document.location.href;

				var parser = document.createElement('a');
				parser.href = url;
				var pathname = parser.pathname;

				var params = pathname.split("/");

				var pairs = [];

				for (var i = 0; i < params.length; i++) {
					if (i % 2 !== 0) pairs[params[i]] = params[i + 1];
				}

				return pairs;

			}, // parseURL- END


			//*******************************************************************************
			//   Dec Of Num
			//*******************************************************************************
			//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
			decOfNum: function(number, titles) {
				cases = [2, 0, 1, 1, 1, 2];
				return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
			},

		};

		Mapbox.init();

	})();

	//Запрос статистики заявок
	/*
	(function(){
		//Контейнер статистики
		var $statOrders = $('#stat-orders');
		var statOrdersLast = $statOrders.data('stat-orders-init');
		var statOrdersInterval = setInterval(function(){
			//Обновление
			$.ajax({
				url: '/objects/ajax/',
				type: 'GET',
				data: { action: "statorders", city_uid: $statOrders.data('city-uid') },
				dataType: 'JSON',
				cache: false,
				success: function(data){
					var statOrders = data.statOrders;
					if(statOrders > statOrdersLast){
						$statOrders.find('.stat-orders-plus').animate({opacity: 1}, 100);
						$statOrders.find('.stat-orders-count').text(statOrders);
						statOrdersLast = statOrders;
						setTimeout(function(){
							$statOrders.find('.stat-orders-plus').animate({opacity: 0}, 800);
						}, 600);
					}
				},
				error: function(){
					console.log('Ошибка...');
				}
			});
		}, 60000);
	})();
	*/

	////Срочное заселение
	//Заезд
	$('#hotorder-checkin').datetimepicker({
	    pickTime: false
	});
	//Выезд
	$('#hotorder-checkout').datetimepicker({
	    pickTime: false
	});

	////Запрос по параметрам
	//Заезд
	$('#paramsorder-checkin').datetimepicker({
	    pickTime: false
	});
	//Выезд
	$('#paramsorder-checkout').datetimepicker({
	    pickTime: false
	});

	////Открытие формы по хешу в УРЛ
	var url = window.location;
	if(url.hash == '#hotorder'){
		$('#modal-hotorder').modal('show');
	}
});


// ViewHotOrder
//*******************************************************************************
var firstViewHotOrder = true;
$("#hotorder-toggle").click(function(e) {
    e.preventDefault();

    //Только первое открытие
    if(firstViewHotOrder){

        //Отправка уведомления о просмотре номера
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: { action: "viewhotorder", addr_city_uid: $('input[name=addr_city_uid]').val() },
            dataType: 'json',
            cache: false,
            success: function(data){
                var response = data;
                //console.log(response);
            },
            error: function(){
                console.log('Ошибка отправки');
            }
        });

        firstViewHotOrder = false;
    }

});

// ViewParamsOrder
//*******************************************************************************
var firstViewParamsOrder = true;
$("#paramsorder-toggle").click(function(e) {
    e.preventDefault();

    //Только первое открытие
    if(firstViewParamsOrder){

        //Отправка уведомления о просмотре номера
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: { action: "viewparamsorder", addr_city_uid: $('input[name=addr_city_uid]').val() },
            dataType: 'json',
            cache: false,
            success: function(data){
                var response = data;
                //console.log(response);
            },
            error: function(){
                console.log('Ошибка отправки');
            }
        });

        firstViewParamsOrder = false;
    }

});

// SendHotOrder
//*******************************************************************************
function sendHotOrder(){

	//Блокируем кнопку
	$('#hotorder-button').attr('disabled', true);
	$('#hotorder-button').html('Идет отправка...');

	//Данные пользователя
	var hotorder = {
		name: $('#hotorder-name').val(),
		phone: $('#hotorder-phone').val(),
		checkin: $('#hotorder-checkin').val(),
		checkout: $('#hotorder-checkout').val(),
		guests: $('#hotorder-guests').val(),
		price: $('#hotorder-price').val(),
		code: $('#hotorder-code').val(),
		district: $('#hotorder-district').val()
	};

	//Отправка уведомления о просмотре номера
	$.ajax({
		url: '/objects/ajax/',
		type: 'POST',
		data: { action: "hotorder", addr_city_uid: $('input[name=addr_city_uid]').val(), addr_city: $('input[name=addr_city]').val(), hotorder: hotorder },
		dataType: 'json',
		cache: false,
		success: function(data){
			var response = data.hotorder_result;

			//Подтверждение номера
			if(response.step == 'code'){
				$('.hotorder-form-info').hide();
				$('.hotorder-form-code').show();
				$('#hotorder-reason').html(response.reason);
				$('#hotorder-form-code-onphone').text(response.phone);
			}
			//Возврат на заполнение формы
			else if(response.step == 'info') {
				$('.hotorder-form-info').show();
				$('.hotorder-form-code').hide();
			}

			//Успех
			if(response.status){
				$('#hotorder-reason').html(response.reason);
				$('#hotorder-name').val('');
				$('#hotorder-phone').val('');
				$('#hotorder-guests').val('');
				$('#hotorder-price').val('');
				$('#hotorder-checkin').val('');
				$('#hotorder-checkout').val('');
				$('#hotorder-code').val('');
				$('#hotorder-district').val('');
				$('#modal-hotorder .form-group.floating-label-form-group').removeClass('floating-label-form-group-with-value');
				$('#modal-hotorder .query').hide();
				$('#hotorder-reason').css('font-weight', 'bold');
				$('#modal-hotorder .modal-body').fadeOut('fast');
				setTimeout(function(){
					$('#modal-hotorder').modal('hide');
				}, 3000);
			}
			//Ошибка
			else {
				$('.hotorder-info').hide();
				$('#hotorder-reason').html(response.reason);
			}

			//Деблокируем кнопку
			$('#hotorder-button').attr('disabled', false);
			$('#hotorder-button').html('Отправить');
		},
		error: function(){

			console.log('Ошибка отправки');

			$('.hotorder-info').hide();
			$('#hotorder-reason').html('Ошибка получения данных - попробуйте позже');

			//Деблокируем кнопку
			$('#hotorder-button').attr('disabled', false);
			$('#hotorder-button').html('Отправить');
		}
	});
}
function sendHotOrderRollback(){
	$('.hotorder-form-info').show();
	$('.hotorder-form-code').hide();
	$('#hotorder-code').val('');
}

// SendParamsOrder
//*******************************************************************************
function sendParamsOrder(){

	//Блокируем кнопку
	$('#paramsorder-button').attr('disabled', true);
	$('#paramsorder-button').html('Идет отправка...');

	//Данные пользователя
	var paramsorder = {
		name: $('#paramsorder-name').val(),
		phone: $('#paramsorder-phone').val(),
		email: $('#paramsorder-email').val(),
		checkin: $('#paramsorder-checkin').val(),
		checkout: $('#paramsorder-checkout').val(),
		guests: $('#paramsorder-guests').val(),
		price: $('#paramsorder-price').val(),
		code: $('#paramsorder-code').val(),
		district: $('#paramsorder-district').val(),
		comment: $('#paramsorder-comment').val()
	};

	//Отправка уведомления о просмотре номера
	$.ajax({
		url: '/objects/ajax/',
		type: 'POST',
		data: { action: "paramsorder", addr_city_uid: $('input[name=addr_city_uid]').val(), addr_city: $('input[name=addr_city]').val(), paramsorder: paramsorder },
		dataType: 'json',
		cache: false,
		success: function(data){
			var response = data.paramsorder_result;

			//Подтверждение номера
			if(response.step == 'code'){
				$('.paramsorder-form-info').hide();
				$('.paramsorder-form-code').show();
				$('#paramsorder-reason').html(response.reason);
				$('#paramsorder-form-code-onphone').text(response.phone);
			}
			//Возврат на заполнение формы
			else if(response.step == 'info') {
				$('.paramsorder-form-info').show();
				$('.paramsorder-form-code').hide();
			}

			//Успех
			if(response.status){
				$('#paramsorder-reason').html(response.reason);
				$('#paramsorder-name').val('');
				$('#paramsorder-phone').val('');
				$('#paramsorder-email').val('');
				$('#paramsorder-guests').val('');
				$('#paramsorder-price').val('');
				$('#paramsorder-checkin').val('');
				$('#paramsorder-checkout').val('');
				$('#paramsorder-code').val('');
				$('#paramsorder-district').val('');
				$('#paramsorder-comment').val('');
				$('#modal-paramsorder .form-group.floating-label-form-group').removeClass('floating-label-form-group-with-value');
				$('#modal-paramsorder .query').hide();
				$('#paramsorder-reason').css('font-weight', 'bold');
				$('#modal-paramsorder .modal-body').fadeOut('fast');
				setTimeout(function(){
					$('#modal-paramsorder').modal('hide');
				}, 3000);
			}
			//Ошибка
			else {
				$('.paramsorder-info').hide();
				$('#paramsorder-reason').html(response.reason);
			}

			//Деблокируем кнопку
			$('#paramsorder-button').attr('disabled', false);
			$('#paramsorder-button').html('Отправить');
		},
		error: function(){

			console.log('Ошибка отправки');

			$('.paramsorder-info').hide();
			$('#paramsorder-reason').html('Ошибка получения данных - попробуйте позже');

			//Деблокируем кнопку
			$('#paramsorder-button').attr('disabled', false);
			$('#paramsorder-button').html('Отправить');
		}
	});
}
function sendParamsOrderRollback(){
	$('.paramsorder-form-info').show();
	$('.paramsorder-form-code').hide();
	$('#paramsorder-code').val('');
}

// Close panelButtons
//*******************************************************************************
$("#venueList .panelButtons .close").click(function(e) {

	$block = $('#venueList .panelButtons');
	$trigger = $('#venueList .panelTrigger');

	$block.fadeOut('fast');
	$trigger.hide();
	$trigger.css('margin-top', 0);

	setTimeout(function(){
		$trigger.fadeIn('fast');
	}, 200);

});