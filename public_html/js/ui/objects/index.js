var sidebarWidth = 430;
// var sidebarWidth = 845;
var topNavbarHeight = 50;
var filtersBarHeight = 75;
var paginatorHeight = 40;
var mapContainer = "#map-canvas";
var panoContainer = "#panorama-box";
var panoContainerAddr = "#panorama-box-address";

/***************************************************************************
jQuery
****************************************************************************/
$(document).ready(function() {


    $(document).on('click', '#rewrite-url', function(event) {
        event.preventDefault();

        var url = document.location.href;
        var parser = document.createElement('a');
        parser.href = url;
        var pathname = parser.pathname;        
        var params = pathname.split("/"); 
        
        console.log(params);

        // window.history.pushState('page2', 'Title', '&page2.php');
    });


    var mapLocations;
    var gmap;
    var url_data;

    //*******************************************************************************
    //   Init Layout Sizes
    //*******************************************************************************
    initlayoutSizes();

    //*******************************************************************************
    //   Easing "Elasout" effect
    //*******************************************************************************
    $.easing.elasout = function(x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    };


    //*******************************************************************************
    //   Select Dropdown - Smartfilter
    //*******************************************************************************
    /*    var smarts = $.getSmartsForSelect();
        
        $("#f_smart").heapbox({
            'effect': {
                'type': 'slide',
                'speed': 'fast'
            },
             "onChange": function(value){
                updatePricesBySmartID(value);
            },
        });

        var json = JSON.stringify(smarts.items);

        $("#f_smart").heapbox("set", json);*/


    //*******************************************************************************
    //   Select Dropdown - Object Type
    //*******************************************************************************
    var objtypes = $.getObjTypesForSelect();
    
    $("#f_objtype").heapbox({
        'effect': {
            'type': 'slide',
            'speed': 'fast'
        },
         "onChange": function(value){
            updatePricesByObjtypeID(value);
        },
    });

    var json = JSON.stringify(objtypes.items);

    $("#f_objtype").heapbox("set", json);


    //*******************************************************************************
    //   Hide Em All
    //*******************************************************************************
    hidePanoramaBoxes();
    hideFilters();
    $('[data-toggle="tooltip"]').tooltip();

    //*******************************************************************************
    //   noUiSlider - Price slider
    //*******************************************************************************
    $(".f-price-slider").noUiSlider({
        start: [1500, 2500],
        step: 5,
        connect: true,
        range: {'min': [0],'max': [30000]},
        serialization: {
            lower: [
                $.Link({ target: $('#f-price-min'), method: "text" }),
            ],
            upper: [
                $.Link({
                    target: $('#f-price-max'),
                    method: "text"
                }),
            ],
            format: {
                decimals: 0,
                mark: ','
            }
        }
    });

    //*******************************************************************************
    //  noUiSlider  - Guests slider
    //*******************************************************************************
    $(".f-guests-slider").noUiSlider({
        start: [1],
        step: 1,
        connect: 'lower',
        range: {
            'min': [1],
            'max': [10]
        },
        serialization: {
            lower: [
                $.Link({
                    target: $('#f-guests-min'),
                    method: "text"
                }),
            ],
            format: {
                decimals: 0,
                mark: ','
            }
        }

    });

    //*******************************************************************************
    //   MODAL - City Selector
    //*******************************************************************************
    $('#modal-select-city').on('show.bs.modal', function () {
       $('#objects-area').addClass('blur');
    });

    $('#modal-select-city').on('hide.bs.modal', function () {
       $('#objects-area').removeClass('blur');
    });
    
    $( "#f_city_name" ).focusin(function() {
        $('#modal-select-city').modal('show');        
    });

    $('.city-selector').click(function(e){
        e.preventDefault();
        var uid = $(this).attr('data-uid');
        var name = $(this).attr('data-name');

        $.ajax(
        {
            url: '/objects/ajax/',
            type: 'get',
            data: {action: "setcity", uid: uid, name: name},
            dataType: 'json',
            async: false,
            cache: false,
            success: function(data) 
            {
                result = data;
            }
        });            

        $("#f_city_name").val(name);
        $("#f_city").val(uid);
        $("#f_city_topmenu span").text(name);

        $('#modal-select-city').modal('hide');        

        $( "#apply-filters" ).trigger( "click" );
    });


    //*******************************************************************************
    //   Map: Resize & Init actions
    //*******************************************************************************
    var ajaxData = updateFiletrsByUrl();
    
    google.maps.event.addDomListener(window, 'load', mapInit(ajaxData));
    google.maps.event.addDomListener(window, 'resize', function() {

        initlayoutSizes();
        gmap.refresh;

    });

    //*******************************************************************************
    //   Create Objects List
    //*******************************************************************************
    objListInit(ajaxData);

    //   ADD To Favorites
    //*******************************************************************************
    $(document).on('click', '.add-favorites', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');

/*        highlightObjectItem(index);
        showOverlay(index);
        showPanoramaBox(index);
*/

    });

    //   Sort buttons
    //*******************************************************************************
    $(document).on('click', '.f_sort_btn', function(event) {
        event.preventDefault();

        var f_sort_type = $(this).attr('data-sort-type');
        var f_sort_dest = $(this).attr('data-sort-dest');

        //Update Hidden fields - Sort
        $("#f_sort_type").val([f_sort_type]);
        $("#f_sort_dest").val([f_sort_dest]);

        var ajax_data = updateFiletrsByForm();

        //Reload Map & Objects
        gmapInit(ajax_data);

        //Reload Map & Objects
        // gmapInit(ajax_data);

    });

  //   OPEN Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.obj-title', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');
        var obj_id = $(this).attr('data-obj-id');

        highlightObjectItem(index);
        showOverlay(index, obj_id);
        hidePanoramaBoxes();

    });    

    //   CLOSE Object Map Bubble action
    //*******************************************************************************
    $(document).on('click', '.overlay-box-close', function(event) {
        event.preventDefault();
        hideOverlays();
    });


    //   OPEN StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.open-panorama', function(event) {
        event.preventDefault();

        var index = $(this).attr('data-obj-index');
        var obj_id = $(this).attr('data-obj-id');

        highlightObjectItem(index);
        showOverlay(index, obj_id);
        showPanoramaBox(index);

    });

    //   CLOSE StreetView Panorama action
    //*******************************************************************************
    $(document).on('click', '.close-panorama', function(event) {
        event.preventDefault();
        hidePanoramaBoxes();
    });

    //   APPLY Filters action
    //*******************************************************************************
    $(document).on('click', '#apply-filters', function(event) {
        event.preventDefault();

        var ajaxData = updateFiletrsByForm();

        //Reload Map & Objects
        // mapInit(ajaxData);
        mapRefresh(ajaxData);

        // ajaxData.offset = 0;
        objListInit(ajaxData);
        
        closeFilters();
    });



    //   OPEN Filters Panel
    //*******************************************************************************
    $(".open-filters").click(function(e) {
        e.preventDefault();
        openFilters();
    });

    //   CLOSE Filters Panel
    //*******************************************************************************
    $(".close-filters").click(function(e) {
        e.preventDefault();
        closeFilters();
    });



    //  Claendar
    //*******************************************************************************
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();

    $.datepicker.regional['ru-RU'] = {
        closeText: 'Готово',
        currentText: 'Сегодня',
        dateFormat: 'd M yy',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
        firstDay: 1,
        isRTL: false,
        minDate: new Date(yyyy, mm, dd),
        monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        monthNamesShort: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
        nextText: '&nbsp;',
        prevText: '&nbsp;',
        showMonthAfterYear: false,
        weekHeader: 'Wk',
        yearSuffix: '',

        beforeShow: function() {
            $("#ui-datepicker-div").addClass("ui-calendar-default")
        },

        beforeShowDay: function(a) {
            var b = "";

            try {
                var dateFrom = $("#f_arrdate").datepicker("getDate"),
                    dateTo = $("#f_depdate").datepicker("getDate");

                return a >= dateFrom && dateTo >= a && (b = "range_day"), [!0, b]

            } catch (e) {
                return [!0, b]
            }
        },

        onSelect: function() {

            var a = $(this).attr("id");
            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");

            if (a == "f_arrdate" && !dateTo) {

                // var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);
                var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() +1);
                $("#f_depdate").datepicker("setDate", newDate);
            }


        },
    };

    $.datepicker.setDefaults($.datepicker.regional['ru-RU']);

    $("#f_arrdate").datepicker({
        changeMonth: true,
        numberOfMonths: 2,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");
            // var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 2);
            var newDate = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate() + 1);

            $("#f_depdate").datepicker("option", "minDate", newDate);
            $("#f_depdate").datepicker("show");
        },

    });

    $("#f_depdate").datepicker({
        changeMonth: true,
        numberOfMonths: 2,

        onClose: function(selectedDate) {

            var dateFrom = $("#f_arrdate").datepicker("getDate");
            var dateTo = $("#f_depdate").datepicker("getDate");
            // var newDate = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate() - 2);
            var newDate = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate() - 1);

            if (!dateFrom) {
                $("#f_arrdate").datepicker("setDate", newDate);
                $("#f_arrdate").datepicker("show");

            }

            //$("#search_date_from").datepicker("option", "maxDate", selectedDate);
        },

    });



}); //========================= JQuery END ==============================================


// ******************************************** Dec of Num ********************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
function decOfNum(number, titles)  
{  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}  

jQuery.extend
(
    {
        getOverlayContent: function(obj_id) 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getoverlaycontent", obj_id: obj_id},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            console.log(result);
            return result;
    
        },
        getSmarts: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getsmarts"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        },
        getObjTypes: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getobjtypes"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        },
        getSmartsForSelect: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "sendobjcardview"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        },
        getObjTypesForSelect: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getobjtypesselect"},
                dataType: 'json',
                async: false,
                cache: false,
                success: function(data) 
                {
                    result = data;
                }
            });            

            // console.log(result);
            return result;
    
        },
        getCities: function() 
        {
            var result = null;

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getcities"},
                dataType: 'json',
                async: false,
                cache: false,
                
                success: function(data) 
                {
                    result = data.items.list;
                    // console.log(result);
                }
            });            

            return result;
        },

    }
);

// ******************************************** Filters Panel functions ********************************
function updateFiletrsByUrl() {

    var params = parseUrl();

    // var smarts = $.getSmarts();
    var objtypes = $.getObjTypes();
    var cities = $.getCities();
    
    //Get Parameters
    var f_minprice = parseInt(params["f_minprice"]) || 0;
    var f_maxprice = parseInt(params["f_maxprice"]) || 0;

    var f_arrdate = params["f_arrdate"];
    var f_depdate = params["f_depdate"];

    var f_guests = params["f_guests"];

    var f_objtype = params["f_objtype"];
    var f_objtype_name = objtypes.items[f_objtype]["ot_name_ru"];

    var f_sort_type = params["f_sort_type"];
    var f_sort_dest = params["f_sort_dest"];
    
    var f_city = params["f_city"];
    var f_page = params["f_page"];
    f_page = (f_page > 0) ? parseInt(f_page) : 1;

    f_minprice = (f_minprice > 0) ? f_minprice : parseInt(objtypes.items[f_objtype]["ot_minprice"]);
    f_maxprice = (f_maxprice > 0) ? f_maxprice : parseInt(objtypes.items[f_objtype]["ot_maxprice"]);

    //Если крайние цены равны (например когда в смарте один объект) то разносим диапазон вручную
    if(f_minprice == f_maxprice)
    {
        f_minprice -= 500;
        f_maxprice += 500;
    }

    //Update Filters Panel - Dates
    if(f_arrdate === undefined && f_depdate === undefined && f_arrdate !== null && f_depdate !== null)
    {
        $("#filters-panel-dates .value-arr").text("Дата не указана");
        $("#filters-panel-dates").addClass("btn-warn");
    } 
    else 
    {
        $("#filters-panel-dates").removeClass("btn-warn");

        $("#filters-panel-dates .value-arr").text(moment(f_arrdate, "YYYY-MM-DD").format('D MMMM '));
        $("#filters-panel-dates .value-dep").text(moment(f_depdate, "YYYY-MM-DD").format('D MMMM'));
        $("#f_arrdate").val(moment(f_arrdate, "YYYY-MM-DD").format('D MMMM YYYY'));
        $("#f_depdate").val(moment(f_depdate, "YYYY-MM-DD").format('D MMMM YYYY'));
    }

    //Update Filters Panel - Object Type
    $("#f_objtype").heapbox("select", f_objtype);
    $("#filters-panel-objtype .value").text(f_objtype_name);

    //Update Filters Panel - Prices
    $("#filters-panel-price .value-min").text(f_minprice);
    $("#filters-panel-price .value-max").text(f_maxprice);
    // $("#f-price-mid-label").text(((f_maxprice - f_minprice)/2 + f_minprice));

    //Update Sliders - Prices
    $(".f-price-slider").noUiSlider({
        start: [f_minprice, f_maxprice],
    }, true);


    //Update Filters Panel - Guests
    $("#filters-panel-guests .value").text(f_guests);

    //Update Sliders - Guests
    $(".f-guests-slider").noUiSlider({
        start: [f_guests],
        step: 1,
        range: {
            'min': [1],
            'max': [10]
        },
    }, true);

    

    //Update Hidden fields - Sort
    $("#f_sort_type").val(f_sort_type);
    $("#f_sort_dest").val(f_sort_dest);
    
    $("#f_page").val(f_page);
    
    $("#f_city").val(f_city);
    $("#f_city_topmenu span").text(cities[f_city].name);

    $(".btn-sort").removeClass("btn-sort-active");
    $(".sort-type").removeClass("btn-sort-active");
    $(".sort-" + f_sort_type + "-" + f_sort_dest).addClass("btn-sort-active");
    $(".sort-" + f_sort_type).addClass("btn-sort-active");


    return params;
}

//*******************************************************************************
//   Init Layout Sizes
//*******************************************************************************
function initlayoutSizes() 
{
    $("#objects-list")
        .css("height", $(window).height() - topNavbarHeight - filtersBarHeight);

    $("#objects-list .wrapper").css("height", $(window).height() - topNavbarHeight - filtersBarHeight - 60);
    
    $("#objects-map, #map-canvas, #panorama-box")
        .css("width", $(window).width() - sidebarWidth)
        .css("height", $(window).height() - topNavbarHeight);
    
    $(panoContainer)
        .css("top", topNavbarHeight + filtersBarHeight + "px")
        .css("left", sidebarWidth + "px");

    $(panoContainerAddr)
        .css("left", (($("#map-canvas").width()/2)-($(panoContainerAddr).width()/2))+ "px");

}

function updateFiletrsByForm() {

        var objtypes = $.getObjTypes();

        var f_minprice =  $(".f-price-slider").val()[0];
        var f_maxprice =  $(".f-price-slider").val()[1];
        var f_guests =  $(".f-guests-slider").val();
        var f_objtype =  $("#f_objtype").val();
        var f_objtype_name = objtypes.items[f_objtype]["ot_name_ru"];

        var f_arrdate = $("#f_arrdate").datepicker("getDate");
        var f_depdate = $("#f_depdate").datepicker("getDate");

        var f_sort_type =  $("#f_sort_type").val();
        var f_sort_dest =  $("#f_sort_dest").val();
        
        var f_city =  $("#f_city").val();
        
        var f_page =  $("#f_page").val();

        $(".btn-sort").removeClass("btn-sort-active");
        $(".sort-type").removeClass("btn-sort-active");
        $(".sort-" + f_sort_type + "-" + f_sort_dest).addClass("btn-sort-active");
        $(".sort-" + f_sort_type).addClass("btn-sort-active");
    
        //Update Filters Panel
        $("#filters-panel-price .value-min").text(f_minprice);
        $("#filters-panel-price .value-max").text(f_maxprice);
        $("#filters-panel-guests .value").text(f_guests);
        $("#filters-panel-objtype .value").text(f_objtype_name);

        //Update Filters Panel - Dates
        if( (f_arrdate === undefined && f_depdate === undefined) || (f_arrdate === null && f_depdate === null))
        {
            $("#filters-panel-dates").addClass("btn-warn");
            $("#filters-panel-dates .value-arr").text("Дата не указана");
        }
        else
        {
            $("#filters-panel-dates").removeClass("btn-warn");

            //Update Filters Panel - Dates
            $("#filters-panel-dates .value-arr").text(moment(f_arrdate).format('D MMMM '));
            $("#filters-panel-dates .value-dep").text(moment(f_depdate).format('D MMMM'));
        }

        var ajax_data = {
            f_minprice: f_minprice,
            f_maxprice: f_maxprice,
            f_guests: f_guests,
            f_sort_type: f_sort_type,
            f_sort_dest: f_sort_dest,
            f_objtype: f_objtype,
            f_city: f_city,
            f_page: f_page,
            f_arrdate: moment(f_arrdate).format('YYYY-MM-DD '),
            f_depdate: moment(f_depdate).format('YYYY-MM-DD'),
        };

    return ajax_data;
}

function updatePricesBySmartID(sf_uid)
{
        var smarts = $.getSmarts();

        var f_minprice =  parseInt(smarts.items[sf_uid]["sf_minprice"]);
        var f_maxprice =  parseInt(smarts.items[sf_uid]["sf_maxprice"]);

        //Если крайние цены равны (например когда в смарте один объект) то разнозим диапазон вручную
        if(f_minprice == f_maxprice)
        {
            f_minprice -= 500;
            f_maxprice += 500;
        }

        //Update Filters Panel - Prices
        $("#filters-panel-price .value-min, #f-price-min-label").text(f_minprice);
        $("#filters-panel-price .value-max, #f-price-max-label").text(f_maxprice);
        $("#f-price-mid-label").text(((f_maxprice - f_minprice)/2 + f_minprice));

        //Update Sliders - Prices
        $(".f-price-slider").noUiSlider({
            start: [f_minprice, f_maxprice],
            step: 50,
            range: {
                'min': f_minprice,
                'max': f_maxprice
            },
        }, true);

}

function updatePricesByObjtypeID(ot_uid)
{
        var objtypes = $.getObjTypes();

        var range_minprice =  parseInt(objtypes.items[ot_uid]["ot_minprice"]);
        var range_maxprice =  parseInt(objtypes.items[ot_uid]["ot_maxprice"]);

        var f_minprice =  $(".f-price-slider").val()[0];
        var f_maxprice =  $(".f-price-slider").val()[1];

        //Если крайние цены равны (например когда в смарте один объект) то разнозим диапазон вручную
        if(range_minprice == range_maxprice)
        {
            range_minprice -= 500;
            range_maxprice += 500;
        }

        if(range_minprice >= f_minprice) f_minprice = range_minprice;
        if(range_maxprice <= f_maxprice) f_maxprice = range_maxprice;
        if( f_minprice >= range_maxprice || f_maxprice <= range_minprice) 
        {
            f_minprice = range_minprice;
            f_maxprice = range_maxprice;
        }

        //Update Filters Panel - Prices
        $("#filters-panel-price .value-min").text(f_minprice);
        $("#filters-panel-price .value-max").text(f_maxprice);
        
        // Update Slider Labels
        $("#f-price-min-label").text(range_minprice);
        $("#f-price-max-label").text(range_maxprice);
        $("#f-price-mid-label").text(((range_maxprice - range_minprice)/2 + range_minprice));

        // //Update Slider - Prices
        $(".f-price-slider").noUiSlider({
            start: [f_minprice, f_maxprice],
            step: 50,
            range: {
                'min': range_minprice,
                'max': range_maxprice
            },
        }, true);

}

function parseUrl() {

    var url = document.location.href;

    var parser = document.createElement('a');
    parser.href = url;
    var pathname = parser.pathname;

    var params = pathname.split("/"); 

    var pairs = [];

    for (var i = 0; i < params.length; i++) 
    { 
        if(i % 2 !== 0) pairs[ params[i] ] = params[i+1];
    }     

    return pairs;
}

function updateFiltersPanel() {

    var f_found = _.size(mapLocations);
    var f_found_label = decOfNum( f_found, ['объект', 'объекта', 'объектов']);

    $("#filters-panel-found .value").text(f_found);
    $("#filters-panel-found .found-label").text(f_found_label);
    
}

function hideFilters() {
    $("#objects-filter-full").hide();
}

function openFilters() {
    $("#objects-filter").animate({
        height: "500px"
    }, 200 );

    $("#objects-filter-short").hide();
    $("#objects-filter-full").fadeIn(10);
}

function closeFilters() {
    $("#objects-filter").animate({
        height: filtersBarHeight + "px"
    }, 200);
    
    $("#objects-filter-full").hide();
    $("#objects-filter-short").fadeIn().animate(10);
}

//******************************************** Google Map functions ********************************

//   OPEN Object Map Bubble action
//*******************************************************************************
function showOverlay(index, obj_id) 
{

    // console.log(mapLocations.index);

    // var lat = mapLocations.obj_id.lat;
    // var lng = mapLocations.obj_id.lng;

    // gmap.setCenter(lat, lng);
    // gmap.setZoom(14);


    if (!document.getElementById("#obj-tooltip-" + index)) 
    {
        // Get Overlay
        var overlayData = $.getOverlayContent(obj_id);
        var d = overlayData.response;

        var lat = d.lat;
        var lng = d.lng;

        var new_lat = parseFloat(lat) + 0.02;
        gmap.setZoom(13);
        gmap.setCenter(new_lat, lng);

        var overlayTmpl = $('#object_overlay_template').html();
        var overlayContent = overlayTmpl
            .replace(/{{obj_index}}/g, index)
            .replace(/{{obj_id}}/g, d.id)
            .replace(/{{obj_title_image}}/g, d.img)
            .replace(/{{obj_name}}/g, d.name)
            .replace(/{{obj_guests}}/g, d.guests)
            .replace(/{{obj_sqr}}/g, d.sqr)
            .replace(/{{obj_rooms}}/g, d.rooms)
            .replace(/{{obj_price}}/g, d.price)
            .replace(/{{obj_stayperiod}}/g, d.stayperiod)
            .replace(/{{obj_uid}}/g, d.uid);

        $("#objects-list .wrapper").append(overlayContent);
    }

    gmap.removeOverlays();
    gmap.drawOverlay({
        lat: lat,
        lng: lng,
        content: $("#obj-tooltip-" + index).html(),
        verticalAlign: 'top',
        horizontalAlign: 'center',
        layer: "floatPane",
    });

    $('#objects-list .wrapper').scrollTo("#obj-item-box-" + index, 500, {
        easing: 'elasout'
    });
    // $('#objects-list').mCustomScrollbar("scrollTo", "#obj-item-box-" + index);    

}

//   CLOSE Object Map Bubble action
//*******************************************************************************
function hideOverlays() {
    gmap.removeOverlays();
    $(".thumbnail").removeClass("selected");
}

function highlightObjectItem(index) {
    $(".thumbnail").removeClass("selected");
    $("#obj-item-box-" + index + " .thumbnail").toggleClass("selected");
}

function showPanoramaBox(index) 
{

    var lat = mapLocations[index]['lat_strview'];
    var lng = mapLocations[index]['lng_strview'];
    
    $(panoContainer).show();
    $(panoContainerAddr + " span").text(mapLocations[index]['name']);
    console.log(mapLocations);

    panorama = GMaps.createPanorama({
        el: panoContainer,
        lat: lat,
        lng: lng,
    });

}

function hidePanoramaBoxes() {
    $("#panorama-box").hide();
}

// Merge second object into first
function merge(set1, set2){
  for (var key in set2){
    if (set2.hasOwnProperty(key))
      set1[key] = set2[key];
  }
  return set1;
}

//*******************************************************************************
//   Objects List Init
//*******************************************************************************
function objListInit(ajaxDataExt) 
{
    $('#preloader').show();
    //*****************************************************************************

    //Processing Data for Ajax
    var ajaxData = { action: "getobjects", only_locations: 0 };    
    if(ajaxDataExt !== "")
    {
        merge(ajaxData, ajaxDataExt);
    }

    $.ajax(
    {
        url: '/objects/ajax/',
        dataType: 'json',
        data: ajaxData,

        async: false,
        cache: false,

        success: function(data) 
        {
            objs = data.locations;
            // console.log(objs);

            var objlistTemplate = $('#object_list_template').html();
            $("#objects-list .wrapper").html("");

            $.each(objs, function( index, item ) {

            // for (var i = 0; i < objs.length; i++) {
                    // var item = objs[i];

                    var obj_images = "";

                    //Images Slider
                    //*****************************************************************************//
                    if (item.details.images.length) {

                        obj_images = '<ul class="bxslider" id="bxslider' + item.index + '">';

                        $.each(item.details.images, function(index, value) {
                            obj_images += "<li><a href='/objects/view/uid/" + item.details.uid + "'><img src='/upload/object/" + item.details.id + "/x400/" + value + "'></a></li>";
                        });

                        obj_images += "</ul>";
                    } else {

                        obj_images = '<ul class="bxslider" id="bxslider' + item.index + '"><li><img src="/images/ui/no-image.png"></li></ul>';
                    }


                    var content = objlistTemplate
                        .replace(/{{obj_index}}/g, item.index)
                        .replace(/{{obj_price}}/g, item.details.price)
                        .replace(/{{obj_uid}}/g, item.details.uid)
                        .replace(/{{obj_id}}/g, item.details.id)
                        .replace(/{{obj_images}}/g, obj_images)
                        .replace(/{{obj_name}}/g, item.details.name)
                        .replace(/{{ot_name}}/g, item.details.ot_name)
                        .replace(/{{obj_acc_type}}/g, item.details.acc_type)
                        .replace(/{{obj_rating}}/g, item.details.rating)
                        .replace(/{{obj_guests}}/g, item.details.guests)
                        .replace(/{{obj_sqr}}/g, item.details.sqr)
                        .replace(/{{obj_stayperiod}}/g, item.details.stayperiod)
                        .replace(/{{obj_rooms}}/g, item.details.rooms);

                    $("#objects-list .wrapper").append(content);

                    if ($.fn.bxSlider) {
                        var slider = $('#bxslider' + item.index).bxSlider({
                            minSlides: 1,
                            maxSlides: 1,
                            moveSlides: 1,
                            slideMargin: 0,
                            infiniteLoop: false,
                            hideControlOnEnd: true,
                            pager: false,
                            auto: false,
                            autoControls: false,
                        });
                    } else {
                        alert("bxSlider not installed");
                    }

            }); // each

        } // success
    
    });  // ajax        

    //*****************************************************************************
    $('#preloader').hide();
}

//*******************************************************************************
//   Map Refresh
//*******************************************************************************
function mapRefresh(ajaxDataExt) 
{
    //Processing Data for Ajax
    var ajaxData = {
        action: "getobjects",
        only_locations: 1,
        no_limit: 1
    };    

    if(ajaxDataExt !== "")
    {
        merge(ajaxData, ajaxDataExt);
    }

    gmap.removeMarkers();

    jQuery.ajax({
        
        url: '/objects/ajax/',
        dataType: 'json',
        data: ajaxData,

        success: function(data) {

            mapLocations = data.locations;

            $.each(mapLocations, function( index, item ) {

            // for (var i = 0; i < mapLocations.length; i++) {

                // var item = mapLocations[i];

/*                var svgIcon = {
                    path: 'M75.992,29.955C75.992,15.074,63.908,3,48.996,3C34.081,3,22,15.074,22,29.955   c0,5.57,1.689,10.74,4.582,15.039l22.804,53.191l22.018-53.191C73.742,40.746,75.992,35.525,75.992,29.955z M48.712,47.646   c-10.227,0-18.522-8.487-18.522-18.957c0-10.463,8.296-18.95,18.522-18.95c10.23,0,18.526,8.487,18.526,18.95   C67.238,39.16,58.942,47.646,48.712,47.646z',
                    fillColor: item.color,
                    fillOpacity: 1,
                    anchor: new google.maps.Point(48, 96),
                    scale: 0.3,
                    strokeWeight: 1,
                    strokeColor: '#FFFFFF',

                };
*/
                var imgicon = {
                    url: item.icon,
                    // size: null,
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(26, 26),
                    // scaledSize: new google.maps.Size(52, 52)
                };


                gmap.addMarker({
                    lat: item.lat,
                    lng: item.lng,
                    // icon: svgIcon,
                    icon: imgicon,
                    zIndex: 1,
                    click: (function(t) {
                        return function() {

                            showOverlay(t.index, t.id);
                            // highlightObjectItem(t.details.index);

                        };
                    })(item)
                });

            }); // each

            gmap.fitZoom();

            // $('#preloader').hide();

            //   Update Filters panel
            //*******************************************************************************
            updateFiltersPanel();

        } // -- success --
    });

}




//*******************************************************************************
//   Map Init
//*******************************************************************************
function mapInit(ajaxDataExt) 
{
    $('#preloader').show();

    //Processing Data for Ajax
    var ajaxData = {
        action: "getobjects",
        only_locations: 1,
        no_limit: 1
    };    

    if(ajaxDataExt !== "")
    {
        merge(ajaxData, ajaxDataExt);
    }

    gmap = new GMaps({
        div: mapContainer,
        fitZoom: true,
        lat: 45.037754,
        lng: 39.007491,
        click: function(e) {
            hideOverlays();
        },
        scrollwheel: false,
    });

    jQuery.ajax({
        
        url: '/objects/ajax/',
        dataType: 'json',
        data: ajaxData,

        success: function(data) {

            mapLocations = data.locations;

            // for (var i = 0; i < mapLocations.length; i++) {

            $.each(mapLocations, function( index, item ) {
            // });                

                // var item = mapLocations[i];

/*                var svgIcon = {
                    path: 'M75.992,29.955C75.992,15.074,63.908,3,48.996,3C34.081,3,22,15.074,22,29.955   c0,5.57,1.689,10.74,4.582,15.039l22.804,53.191l22.018-53.191C73.742,40.746,75.992,35.525,75.992,29.955z M48.712,47.646   c-10.227,0-18.522-8.487-18.522-18.957c0-10.463,8.296-18.95,18.522-18.95c10.23,0,18.526,8.487,18.526,18.95   C67.238,39.16,58.942,47.646,48.712,47.646z',
                    fillColor: item.color,
                    fillOpacity: 1,
                    anchor: new google.maps.Point(48, 96),
                    scale: 0.3,
                    strokeWeight: 1,
                    strokeColor: '#FFFFFF',

                };
*/
                var imgicon = {
                    url: item.icon,
                    // size: new google.maps.Size(82, 113),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(26, 26),
                    // scaledSize: new google.maps.Size(20.5, 28.25),
                };


                gmap.addMarker({
                    lat: item.lat,
                    lng: item.lng,
                    // icon: svgIcon,
                    icon: imgicon,
                    zIndex: 1,
                    click: (function(t) {
                        return function() {

                            showOverlay(t.index, t.id);
                            // highlightObjectItem(t.details.index);

                        };
                    })(item)
                });

             }); // each

            
            var total_pages = Math.ceil(_.size(mapLocations) / 20);

            //   Paginator
            //*******************************************************************************
            $('#objects-paginator').bootpag({
                total: total_pages,
                maxVisible: 5,
                leaps: false,
                page: $("#f_page").val(),
                firstLastUse: true,
                first: '←',
                last: '→',        
            }).on('page', function(event, num){
                $("#f_page").val(num);
                
                var ajaxData = updateFiletrsByForm();
                objListInit(ajaxData);
            });

            gmap.fitZoom();

            $('#preloader').hide();

            //   Update Filters panel
            //*******************************************************************************
            updateFiltersPanel();

        } // -- success --
    });


}