/***************************************************************************
jQuery START
****************************************************************************/
$(document).ready(function() {

    var item;
    var slider;

    // Image Slider
    //*******************************************************************************
    if ($.fn.bxSlider) {
        slider = $('.bxslider').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideMargin: 0,
            infiniteLoop: false,
            hideControlOnEnd: true,
            pager: true,
            auto: false,
            autoControls: false,
            pagerCustom: '.bx-pager',
            preloadImages: 'all',
            onSliderLoad: function(){
                $('#object-info-images ul').css('visibility', 'visible');
            }
        });

    } else {
        alert("bxSlider not installed");
    }

    //  Object Calendar
    //*******************************************************************************
    var date = new Date();
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();

    $( "#obj-calendar" ).datepicker({

        numberOfMonths: 2,
        dayNamesMin: [ "ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" ],
        monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
        currentText: "Сегодня",
        firstDay: 1,
        buttonImageOnly: true,
        showButtonPanel: false,
        minDate: new Date(y, m, d),
        hideIfNoPrevNext: true,
        nextText: "<i class=icon-chevron-right></i>",
        prevText: "<i class=icon-chevron-left></i>",

        beforeShowDay: function(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();

            for (i = 0; i < disabledDays.length; i++) {
                if($.inArray(y + '-' + (m+1) + '-' + d,disabledDays) != -1) {
                    //return [false];
                    return [true, 'obj-close-event', ''];
                }
            }
            return [true];

        },
    });

    //  Hide Billing Info
    //*******************************************************************************
    $(".obj-view-phones").hide();

    //  Inner links scroll
    //*******************************************************************************
    $(".inner-link:not(.back)").click(function(e) {
        e.preventDefault();

        $('body').scrollTo($(this).attr('href'), 500, {
            // easing: 'elasout'
            offset: -110
        });

        $(".inner-link").removeClass('active');
        $(this).addClass('active');

    });

    //  Show contacts button trigger
    //*******************************************************************************
    var firstShowTel = true;
    $("#showtel").click(function(e) {

        e.preventDefault();

        //Только первое открытие
        if(firstShowTel){

            //Отправка уведомления о просмотре номера
            $.ajax({
                url: '/objects/ajax/',
                type: 'get',
                data: { action: "showtel", obj_id: $('input[name=obj_id]').val() },
                dataType: 'json',
                cache: false,
                success: function(data){
                    var response = data;
                    //console.log(response);
                },
                error: function(){
                    console.log('Ошибка отправки');
                }
            });

            firstShowTel = false;

        }

    });

    //*******************************************************************************
    var firstShowSendOrder = true;
    $("#send-request-to-owner").click(function(e) {

        e.preventDefault();

        //Только первое открытие
        if(firstShowSendOrder){

            //Отправка уведомления о просмотре номера
            $.ajax({

                url: '/objects/ajax/',
                type: 'get',
                data: { action: "showsendorder", obj_id: $('input[name=obj_id]').val() },
                dataType: 'json',
                cache: false,
                success: function(data){

                    var response = data;
                    //console.log(response);

                },
                error: function(){

                    console.log('Ошибка отправки');

                }

            });

            firstShowSendOrder = false;

        }

    });

    //  ShowtelExt
    //*******************************************************************************
    $("#showtelext").click(function(e) {
        e.preventDefault();

        //Только первое открытие
        if(firstShowTel){

            //Отправка уведомления о просмотре номера
            $.ajax({
                url: '/objects/ajax/',
                type: 'get',
                data: { action: "showtelextview", obj_id: $('input[name=obj_id]').val() },
                dataType: 'json',
                cache: false,
                success: function(data){
                    var response = data;
                    //console.log(response);
                },
                error: function(){
                    console.log('Ошибка отправки');
                }
            });

            firstShowTel = false;
        }

    });

    //  Send Ring Query
    //*******************************************************************************
    /*
    $("#send-ring-query-btn").click(function(e) {
        e.preventDefault();

        $('#send-ring-query-btn').hide();
        $('#send-ring-query-text').html('Идет соединениe...');

        $('#modal-showtelext .result').append('<p class="text-center" id="send-ring-query-process"><img src="/images/loading.gif" alt="Загрузка" /></p>');

        //Отправка уведомления о просмотре номера
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: { action: "sendringquery", obj_id: $('input[name=obj_id]').val() },
            dataType: 'json',
            cache: false,
            success: function(data){
                var response = data.sendringquery;
                //console.log(response);
                if(response.status){

                } else {
                    $('#send-ring-query-text').html('В данный момент сервис недоступен. Приносим извинения.');
                    $('#send-ring-query-process').hide();
                }
            },
            error: function(){
                console.log('Ошибка заказа бесплатного звонка');
            }
        });

    });
*/

    //  Sending result calling
    //*******************************************************************************
    $(".btn-callback").click(function(e) {
        e.preventDefault();

        //Отправка уведомления о результате звонка
        $.ajax({
            url: '/objects/ajax/',
            type: 'get',
            data: { action: "callback", result: e.target.dataset.result, obj_id: $('input[name=obj_id]').val() },
            dataType: 'json',
            cache: false,
            success: function(data){
                var response = data.callback_result;
                //console.log(response);
                if(response.status){
                    $('#modal-show-owners-contacts form').hide();
                    $('#modal-show-owners-contacts.modal .modal-body').css('padding-bottom', '10px');
                    $('#modal-show-owners-contacts .modal-body').append('<h3>Благодарим за ответ!</h3>');
                    setTimeout(function(){$('#modal-show-owners-contacts').modal('hide')}, 2400);
                }
            },
            error: function(){
                console.log('Ошибка отправки');
            }
        });

    });

    (function() {

    var Mapbox = {

            //*******************************************************************************
            //   Define some vars
            //*******************************************************************************
            mapDivName: "obj-view-map-canvas",
            mapDiv: $('#obj-view-map-canvas'),
            objParams: $('#obj-view'),
            token: 'pk.eyJ1IjoibWFsYW1kYXIiLCJhIjoiUWF0QVVKTSJ9.Kn2I-QVSUu4Fco8OEOxN1w',
            mapName: 'malamdar.m0k879io',

            //*******************************************************************************
            //   Init
            //*******************************************************************************
            init: function() {

                var self = this;

                this.objLat = parseFloat(this.objParams.data('lat'));
                this.objLng = parseFloat(this.objParams.data('lng'));

                L.mapbox.accessToken = this.token;

                // this.setsize();

                this.map = L.mapbox
                    .map(this.mapDivName, this.mapName, {
                        zoomControl: true,
                        // minZoom: 12,
                    })
                    .setView([this.objLat, this.objLng], 16)
                    ;

                    L.mapbox.featureLayer({
                        type: 'Feature',
                        geometry: {
                            type: 'Point',
                            coordinates: [
                              this.objLng,
                              this.objLat,
                            ]
                        },
                        properties: {
                            title: '',
                            description: '',
                            'marker-size': 'large',
                            'marker-color': '#FB4151',
                            'marker-symbol': 'building'
                        }
                    }).addTo(self.map);

                this.map.scrollWheelZoom.disable();

            },
            // Init - END

        }; // Mapbox - END

        Mapbox.init();

    })();

});
//========================= JQuery END ====================

//  Send Order
//*******************************************************************************
function sendOrder(){

    //Блокируем кнопку
    $('#order-object-button').attr('disabled', true);
    $('#order-object-button').html('Идет отправка...');

    //Данные пользователя
    var order = {
        name: $('#order-object-name').val(),
        phone: $('#order-object-phone').val(),
        email: $('#order-object-email').val(),
        checkin: $('#order-object-checkin').val(),
        checkout: $('#order-object-checkout').val(),
        comment: $('#order-object-comment').val()
    };

    //Отправка уведомления
    $.ajax({

        url: '/objects/ajax/',
        type: 'POST',
        data: { action: "sendorder", obj_id: $('input[name=obj_id]').val(), order: order },
        dataType: 'json',
        cache: false,
        success: function(data){

            var response = data.sendorder_result;
            //console.log(response);
            //Успех
            if(response.status){

                $('.order-object-info').hide();
                $('#order-object-reason').html('Заявка успешно отправлена');
                $('#order-object-name').val('');
                $('#order-object-phone').val('');
                $('#order-object-email').val('');
                $('#order-object-checkin').val('');
                $('#order-object-checkout').val('');
                $('#order-object-comment').val('');
                $('#modal-send-request-to-owner .form-group.floating-label-form-group').removeClass('floating-label-form-group-with-value');
                setTimeout(function(){$('#modal-send-request-to-owner').modal('hide')}, 2600);
                setTimeout(function(){$('#order-object-reason').html('')}, 3000);

            }
            //Ошибка
            else {

                $('.order-object-info').hide();
                $('#order-object-reason').html(response.reason);

            }

            //Деблокируем кнопку
            $('#order-object-button').attr('disabled', false);
            $('#order-object-button').html('Отправить');

        },
        error: function(){

            console.log('Ошибка отправки');

            //Деблокируем кнопку
            $('#order-object-button').attr('disabled', false);
            $('#order-object-button').html('Отправить');

        }

    });

}

//  ShowtelExt
//*******************************************************************************
function showTelExt(){

    //Блокируем кнопку
    $('#showtelext-button').attr('disabled', true);
    $('#showtelext-button').html('Идет отправка...');

    //Данные пользователя
    var order = {
        name: $('#showtelext-name').val(),
        phone: $('#showtelext-phone').val(),
        email: $('#showtelext-email').val(),
        checkin: $('#showtelext-checkin').val(),
        checkout: $('#showtelext-checkout').val()
    };

    //Отправка уведомления о просмотре номера
    $.ajax({
        url: '/objects/ajax/',
        type: 'POST',
        data: { action: "showtelext", obj_id: $('input[name=obj_id]').val(), order: order },
        dataType: 'json',
        cache: false,
        success: function(data){
            var response = data.showtelext_result;
            //console.log(response);
            //Успех
            if(response.status){

                $('.showtelext-info').html('Благодарим!');
                $('#showtelext-reason').hide();
                $('#showtelext-name').val('');
                $('#showtelext-phone').val('');
                $('#showtelext-email').val('');
                $('#showtelext-checkin').val('');
                $('#showtelext-checkout').val('');
                $('#modal-showtelext .form-group.floating-label-form-group').removeClass('floating-label-form-group-with-value');
                $('#modal-showtelext .query').hide();

                //Контакты
                $('#modal-showtelext .result').fadeIn('fast');
                $('#modal-showtelext .result .phone').html(response.phone);
                $('#modal-showtelext .result .email').html(response.email);

                //Предзаполняем форму заявки
                $('#order-object-name').val(order.name);
                $('#order-object-phone').val(order.phone);
                $('#order-object-email').val(order.email);
                $('#order-object-checkin').val(order.checkin);
                $('#order-object-checkout').val(order.checkout);
            }
            //Ошибка
            else {
                $('.showtelext-info').hide();
                $('#showtelext-reason').html(response.reason);
            }

            //Деблокируем кнопку
            $('#showtelext-button').attr('disabled', false);
            $('#showtelext-button').html('Показать контакты');
        },
        error: function(){

            console.log('Ошибка отправки');

            $('.showtelext-info').hide();
            $('#showtelext-reason').html('Ошибка получения данных - попробуйте позже');

            //Деблокируем кнопку
            $('#showtelext-button').attr('disabled', false);
            $('#showtelext-button').html('Показать контакты');
        }
    });
}

//   Update Booking by Session
//*******************************************************************************
function updateBookingBySess() {

    var dateFrom = $("input[name=b_arrdate_ymd]").val();
    var dateTo = $("input[name=b_depdate_ymd]").val();

    // console.log(dateFrom);

    if (!_.isUndefined(dateFrom) && !_.isUndefined(dateTo) && !_.isNull(dateFrom) && !_.isNull(dateTo) && !_.isEmpty(dateFrom) && !_.isEmpty(dateTo)) {
        calcBooking(dateFrom, dateTo);
    } else {

        // $("#b_depdate").attr("disabled", "disabled");
        // $("#book-now").attr("disabled", "disabled");
    }

}

/*------------------------------------------------------------------------------------
/* Bootstrap Datetimepicker
--------------------------------------------------------------------------------------*/
/*
$('#pick_cal_startdate').datetimepicker({
    language: 'ru',
    defaultDate: moment().format('L') + " 14:00",
});

$('#pick_cal_enddate').datetimepicker({
    language: 'ru',
    defaultDate: moment().add(3, 'days').format('L') + " 12:00",
});
*/

////Показать контакты расширенно
var date_now = new Date();
//Заезд
var showtelext_checkin = $('#showtelext-checkin').datetimepicker({
    pickTime: false,
    minDate: date_now
});

//Выезд
var showtelext_checkout = $('#showtelext-checkout').datetimepicker({
    pickTime: false,
    minDate: date_now
});

////Заявка на бронь
//Заезд
$('#order-object-checkin').datetimepicker({
    pickTime: false
});

//Выезд
$('#order-object-checkout').datetimepicker({
    pickTime: false
});

//   Parse URL
//*******************************************************************************
function parseUrl() {

    var url = document.location.href;

    var parser = document.createElement('a');
    parser.href = url;
    var pathname = parser.pathname;

    var params = pathname.split("/");
    var pairs = [];

    for (var i = 0; i < params.length; i++) {
        if (i % 2 !== 0) pairs[params[i]] = params[i + 1];
    }

    return pairs;
}

//   Dec of Num
//*******************************************************************************
//Пример: decOfNum(5, ['секунда', 'секунды', 'секунд'])
function decOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

function showPanoramaBox(index) {

    var lat = item[0]['details']['lat_strview'];
    var lng = item[0]['details']['lng_strview'];

    var box = "#panorama-box";

    $(box).show();

    panorama = GMaps.createPanorama({
        el: box,
        lat: lat,
        lng: lng,
    });

}
