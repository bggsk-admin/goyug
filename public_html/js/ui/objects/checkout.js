/***************************************************************************
jQuery START
****************************************************************************/
$(document).ready(function() {

    // $("#book-goto-pay").attr("disabled", "disabled");

    //  Signin & Signup coming soon
    //*******************************************************************************
    $('#signin-coming-soon, #signup-coming-soon').hide();
    $("#book-signup-btn").click(function(e) {
        e.preventDefault();
        $('#signup-coming-soon').show( "fast", function() {});
    });
    $("#book-signin-btn").click(function(e) {
        e.preventDefault();
        $('#signin-coming-soon').show( "fast", function() {});
    });


    $("#book-signup-btn").hide();
    $("#book-signin-btn").hide();

    $("#book-signin-form .form-group").click(function(e) {

        $(this).addClass('form-group-active');
        $("#book-signup-form .form-group").removeClass('form-group-active');

        $("#book-signin-btn").show();
        $("#book-signup-btn").hide();

    });

    $("#book-signup-form .form-group").click(function(e) {

        $(this).addClass('form-group-active');
        $("#book-signin-form .form-group").removeClass('form-group-active');
        
        $("#book-signup-btn").show();
        $("#book-signin-btn").hide();

    });

    //  Validate Signin & Signup form
    //*******************************************************************************

        $("#book-signin-form").bootstrapValidator({
            fields: {
                b_login: {
                    trigger: "keyup",
                    validators: {
                        emailAddress: {
                            message: 'Укажите электронную почту'
                        },
                        notEmpty: {
                            message: 'Укажите электронную почту'
                        },
                    }
                },
                b_password: {
                    trigger: "keyup",
                    validators: {
                        notEmpty: {
                            message: 'Укажите пароль'
                        },
                    }
                },
            },
            submitButtons: '#book-signin-btn',
            live: 'enabled',
        });

        $("#book-signup-form").bootstrapValidator({
            fields: {
                
                b_email: {
                    trigger: "keyup",
                    validators: {
                        emailAddress: {
                            message: 'Укажите электронную почту'
                        },
                        notEmpty: {
                            message: 'Укажите электронную почту'
                        },
                    }
                },
                b_password: {
                    trigger: "keyup",
                    validators: {
                        notEmpty: {
                            message: 'Укажите пароль'
                        },
                    }
                },
                b_lastname: {
                    trigger: "keyup",
                    validators: {
                        notEmpty: {
                            message: 'Укажите фамилию'
                        },
                    }
                },
                b_firstname: {
                    trigger: "keyup",
                    validators: {
                        notEmpty: {
                            message: 'Укажите имя'
                        },
                    }
                },
                b_phone: {
                    trigger: "keyup",
                    validators: {
                        notEmpty: {
                            message: 'Укажите номер телефона'
                        },
                    }
                },
            },
            submitButtons: '#book-signup-btn',
            live: 'enabled',
        });


    //  Masked Input
    //*******************************************************************************
    $("#b_phone").mask("+7 (999) 999-9999");


    // Ajax context setup
    //*******************************************************************************
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (settings.context != undefined && settings.context.hasClass('btn')) {
                settings.context.button('loading');
            }
        },
        complete: function() {
            this.button('reset');
        }
    });

    // Send Ajax Form
    //*******************************************************************************
    $('form[data-async]').submit(function(event) {
        var $form = $(this);
        var $alert = $(this, ".alert .alert-text");
        var $btn = $(this).children("button[type='submit']");

        $.ajax({
            context: $btn,
            dataType: 'json',
            cache: false,
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),

            success: function(data, status) {
                if (data.redirect) {
                    window.location.href = data.redirect;
                }
                if (data.error) {

                    // $alert.text("ddddd");
                    // $alert.show();

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $btn.button('reset');
            }

        });


        event.preventDefault();
    });


});
//========================= JQuery END ====================

//   Dec of Num
//   Пример: decOfNum(5, ['секунда', 'секунды', 'секунд']) 
//*******************************************************************************
function decOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
