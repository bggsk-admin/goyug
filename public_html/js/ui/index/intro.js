/***************************************************************************
jQuery
****************************************************************************/

$(document).ready(function() {

    //-------------------------------------------------- Validate Reg Form -----------------------------------/
    if ($.fn.validate) {
        $('#modal-signup-form').validate({
            rules: {
                msf_email: {
                    minlength: 6,
                    required: true,
                    email: true
                },
                msf_name: {
                    minlength: 2,
                    required: true,
                },
                msf_mobile: {
                    required: true,
                },
                msf_password: {
                    minlength: 6,
                    required: true,
                },
            },
            showErrors: function(errorMap, errorList) {

                $.each(this.validElements(), function (index, element) {
                    var $element = $(element);
                    $element.data("title", "").tooltip("destroy");

                    $element.closest('.form-group').removeClass('has-error').addClass('has-success');
                });

                $.each(errorList, function (index, error) {
                    var $element = $(error.element);

                    $element.tooltip("destroy")
                    .data("title", error.message)
                    .tooltip({placement:'right', trigger: 'manual', show: '100'}).tooltip('show');

                    $element.closest('.form-group').removeClass('has-success').addClass('has-error');
                });

            }

        });
    } else { alert("Validate method not loaded"); }

    //-------------------------------------------------- Masked Input -----------------------------------/
    $("#msf_mobile").mask("+7 (999) 999-9999");





    var search_form = $("#main-search-form");

    
    //   Generate URL when search form submit action
    //*******************************************************************************
    search_form.submit(function(event) {

        // event.preventDefault();

        if (search_form.valid()) {

            var f_guests = $("#f_guests").val();
            // var f_objtype = $("#f_objtype").val();
            var f_city = $("#f_city").val();
            
            var action = search_form.attr("action");
            action = 'http://' + f_city + "." + action;

            // console.log(action);
            // return false;

            search_form.attr("action", action);
        }
        return true;
    });


    function getCities() {

            var result = null;

            // console.log("getcities");

            $.ajax(
            {
                url: '/objects/ajax/',
                type: 'get',
                data: {action: "getcities"},
                dataType: 'json',
                async: false,
                cache: false,
                
                success: function(data) 
                {
                    result = data.items.list;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  console.log(textStatus + " in pushJsonData: " + errorThrown + " " + jqXHR);
                }
            });            

            return result;
    }



  /* Initialize Google Maps */

    function googleMap() {

        $('.map').each(function (i, e) {

            $map = $(e);
            $map_lat = $map.attr('data-mapLat');
            $map_lon = $map.attr('data-mapLon');
            $map_zoom = parseInt($map.attr('data-mapZoom'));
            $map_title = $map.attr('data-mapTitle');
            $map_info = $map.attr('data-info');
            $map_img = $map.attr('data-img');
            $map_color = $map.attr('data-color');
            $map_height = $map.attr('data-height');

            var latlng = new google.maps.LatLng($map_lat, $map_lon);
            
            var options = {
                scrollwheel: false,
                draggable: true,
                zoomControl: true,
                disableDoubleClickZoom: false,
                disableDefaultUI: false,
                zoom: $map_zoom,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            /* Map's style */
            var 
                // blue1 = "#bfdcf1", //Water
                blue1 = "#789A9F", //Water
                // blue2 = "#E9F0F5"; // Ground
                blue2 = "#EDEADD"; // Ground

                var styles = [
                {
                    "elementType": "geometry.stroke",
                    "stylers": [{"visibility": "on"}]
                }, 
                {
                    "featureType": "water",
                    "stylers": [{"color": blue1}]
                }, 
                {
                    "featureType": "water",
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "on"}]
                }, 
                {
                    "featureType": "landscape.natural",
                    "stylers": [{
                        "color": blue2
                    }]
                }, {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "road",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "landscape.man_made",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "administrative",
                    "stylers": [{
                        "visibility": "on"
                    }]
                }],
                textcolor = "#FFFFFF";


            var cities = getCities();
            // console.log(cities);

            var styledMap = new google.maps.StyledMapType(styles, {
                name: "Styled Map"
            });

            var map = new google.maps.Map($map[0], options);

            // list of the coordinates/positions
            var latlnglist = [];

            var bounds = new google.maps.LatLngBounds();

            var markers = [], marker;

            var icon = {
                url: $map_img,
                size: null,
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(26, 26),
                scaledSize: new google.maps.Size(52, 52)
            };

            var city_ind = 0;

            $.each( cities, function ( key, city )
            {
                var city_id = city.id;
                // marker position
                var position = new google.maps.LatLng(city.lat, city.lng);

                // extending position into list
                latlnglist.push(position);
                bounds.extend(position);

                var markerOptions = {
                    position: position,
                    icon: icon,
                    map: map
                };

                // creating the marker
                marker = new google.maps.Marker(markerOptions);

                // creating list of markers
                markers.push(marker);

                var infoboxOptions = {
                    content: '<div class="infobox-inner"><a href="' + city.maplink + '">' + city.name + '</a></div>',
                    disableAutoPan: false,
                    maxWidth: 0,
                    zIndex: null,
                    boxStyle: {
                        width: "280px"
                    },
                    closeBoxURL: "",
                    pixelOffset: new google.maps.Size(-140, 30),
                    infoBoxClearance: new google.maps.Size(1, 1)
                };                

                markers[city_ind].infobox = new InfoBox(infoboxOptions);

                google.maps.event.addListener(marker, 'click', (function (marker, city_ind) 
                {
                    return function () 
                    {
                        for (h = 0; h < markers.length; h++) 
                        {
                            markers[h].infobox.close();
                        }
                        
                        var infowindow = new google.maps.InfoWindow();
                        infowindow.close();

                        markers[city_ind].infobox.open(map, this);
                        map.panTo(marker.position);

                    };
                })(marker, city_ind));

                city_ind++;

            });            

            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            $map.css({
                'height': $map_height + 'em'
            });

            // center map on resize
            google.maps.event.addDomListener(window, "resize", function () {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });

            // center map on resize
            google.maps.event.addDomListener(map, "click", function () 
            {
                for (h = 0; h < markers.length; h++) 
                {
                    markers[h].infobox.close();
                }
            });



        });

    } // googleMap

    if ($('.map').length) {
        googleMap();
    }

    $(function() {
        return $(".modal").on("show.bs.modal", function() {
            var curModal;
            curModal = this;
            $(".modal").each(function() {
                if (this !== curModal) {
                    $(this).modal("hide");
                }
            });
        });
    });

    /* blur on modal open, unblur on close */
    $('#modal-select-city').on('show.bs.modal', function () {
       $('#home, header').addClass('blur');
    });

    $('#modal-select-city').on('hide.bs.modal', function () {
       $('#home, header').removeClass('blur');
    });
    
    $( "#f_city_name" ).focusin(function() {
        $('#modal-select-city').modal('show');        
    });

    $('.city-selector').click(function(e){
        e.preventDefault();
        var uid = $(this).attr('data-uid');
        var name = $(this).attr('data-name');

        console.log(uid);

        $.ajax(
        {
            url: '/objects/ajax/',
            type: 'get',
            data: {action: "setcity", uid: uid, name: name},
            dataType: 'json',
            async: false,
            cache: false,
            success: function(data) 
            {
                result = data;
            }
        });            

        $("#f_city_name").val(name);
        $("#f_city").val(uid);
        $("#f_city_topmenu span").text(name);

        $('#modal-select-city').modal('hide');        

        $('html, body').animate({ scrollTop: 0 }, 1500);
        
    });

}); //========================= JQuery - END ==============================================
