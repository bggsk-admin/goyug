<?

print_r(get_include_path());

require_once 'Zend/Mail.php';
require_once 'Zend/Mail/Transport/Sendmail.php';

$transport = new Zend_Mail_Transport_Sendmail();

$mail = new Zend_Mail();
$mail->addTo('bggsk@bk.ru')
     ->setSubject('Zend Mail StandAlone Test')
     ->setBodyText("Hello,\nThis is a Zend Mail message...\n")
     ->setFrom('info@goyug.com');

try {
    $mail->send($transport);
    echo "Message sent!<br />\n";
} catch (Exception $ex) {
    echo "Failed to send mail! " . $ex->getMessage() . "<br />\n";
}

?>