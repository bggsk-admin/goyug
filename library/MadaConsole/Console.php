<?php
require_once "MadaConsole/Debugger/Debugger.php";
require_once "MadaConsole/Debugger/DebuggerPlugin.php";
require_once "MadaConsole/Debugger/DebuggerProfiler.php";

/**
 * A plugin that show a debug console under your web page. Works even in Facebook FBML pages.
 * 
 * Usage:
 * If you use the ini configuration, simply add this in your config:
 * 
 * pluginPaths.MadaConsole = "MadaConsole"
 * resources.console = true 
 * 
 * 
 * If you use the Bootstrap class, add this method to it:
 * 
 * 	public function _initConsole() {
 *		return MadaConsole_Console::initialize($this);
 *	}
 *
 */
class MadaConsole_Console extends Zend_Application_Resource_ResourceAbstract {
	 
	/**
	 * Initialize the resource, used when configured throught the ini configuration.
	 * 
	 * If you use the ini configuration, simply add this:
	 * pluginPaths.MadaConsole = "MadaConsole"
	 * resources.console = true
	 * to your ini instead of calling this method. 
	 *
	 * To initialize the plugin in your Bootstrap class use the initialize method.
	 * 
	 * @return Debugger
	 */
	public function init() {
		self::initialize($this->getBootstrap());
		
		return Debugger::getInstance();
	}
	
	/**
	 * Initialize the console plugin
	 * 
	 * Use this method to initialize the console plugin in your code (eg: in your Bootstrap class).
	 * 
	 * If you use the ini configuration, simply add this:
	 * pluginPaths.MadaConsole = "MadaConsole"
	 * resources.console = true
	 * to your ini instead of calling this method. 
	 *
	 * @param Zend_Application_Bootstrap_Bootstrapper $bootstrap
	 */
	public static function initialize(Zend_Application_Bootstrap_Bootstrapper $bootstrap) {
		$bootstrap->bootstrap("FrontController"); //Ensure the front controller is here
		$bootstrap->getResource("FrontController")->registerPlugin(new DebuggerPlugin());
		
		//Db is optional:
		if(null != $bootstrap->getPluginResource("db")) {
			$zendDb = $bootstrap->getPluginResource("db")->getDbAdapter();
			$profiler = new DebuggerProfiler($zendDb);
			$profiler->setEnabled(true);
			$zendDb->setProfiler($profiler);
		}
	}
	
}

//Useful functions:

/**
 * Add a debug message.
 * It return the same object passed, so it can be used inline: $var = debug($value);
 * 
 * @param string|array|Object|Zend_Db_Select|Zend_Db_Table_Row_Abstract|Zend_Db_Table_Rowset_Abstract
 * @return the parameter passed
 */
function debug($msg) {
	Debugger::getInstance()->debug($msg);
	return $msg;
}

/**
 * Add an error message.
 * It return the same object passed, so it can be used inline: $var = debug($value);
 * 
 * @param string|array|Object|Zend_Db_Select|Zend_Db_Table_Row_Abstract|Zend_Db_Table_Rowset_Abstract
 * @return the parameter passed
 */
function error($msg) {
	Debugger::getInstance()->error($msg);
	return $msg;
}
