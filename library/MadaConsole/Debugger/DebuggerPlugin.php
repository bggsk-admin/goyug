<?php
require_once 'MadaConsole/Debugger/Debugger.php';

class DebuggerPlugin extends Zend_Controller_Plugin_Abstract
{
	
    public function dispatchLoopShutdown()
    {
    	$this->getResponse()->appendBody(Debugger::getInstance()->printMessages());
    }
}
