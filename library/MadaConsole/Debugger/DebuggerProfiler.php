<?php
require_once "MadaConsole/Debugger/Debugger.php";

class DebuggerProfiler extends Zend_Db_Profiler
{
	private $db;
	
    /**
     * Constructor
     *
     * @return void
     */
    public function __construct($db)
    {
    	$this->db = $db;
    }


    /**
     * Intercept the query end and log the profiling data.
     *
     * @param  integer $queryId
     * @throws Zend_Db_Profiler_Exception
     * @return void
     */
    public function queryEnd($queryId)
    {
        parent::queryEnd($queryId);
        
        if (!$this->getEnabled()) {
            return;
        }
        
        $q = $this->getQueryProfile($queryId);
        Debugger::getInstance()->debugSql($this->db->quoteInto($q->getQuery(), $q->getQueryParams()), $q->getElapsedSecs());
    }
    
}
