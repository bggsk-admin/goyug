<?php

class MyDbProfilerLog extends Zend_Db_Profiler {

/**
 * Zend_Log instance
 * @var Zend_Log
 */
protected $_log;

/**
 * counter of the total elapsed time
 * @var double 
 */
protected $_totalElapsedTime;


public function __construct($enabled = false) {
    parent::__construct($enabled);

    $this->_log = new Zend_Log();
    $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/db-queries.log');
    $this->_log->addWriter($writer);
}

/**
 * Intercept the query end and log the profiling data.
 *
 * @param  integer $queryId
 * @throws Zend_Db_Profiler_Exception
 * @return void
 */
public function queryEnd($queryId) {
    $state = parent::queryEnd($queryId);

    if (!$this->getEnabled() || $state == self::IGNORED) {
        return;
    }

    // get profile of the current query
    $profile = $this->getQueryProfile($queryId);



        // update totalElapsedTime counter
        $this->_totalElapsedTime += $profile->getElapsedSecs();

        // create the message to be logged
        $message = "\r\n";
        $message .= date("Y-m-d H:i:s") . "\r\n";
        $message .= round($profile->getElapsedSecs(), 5) . "\r\n";
        $message .= $profile->getQuery() . "\r\n";

        $mysql_fat_logfile = '/home/c/c74542/goyug.com/application/ui/data/logs/db-fat-queries.log';
        if(floatval(round($profile->getElapsedSecs(), 5)) > 0.1)
        {
            file_put_contents($mysql_fat_logfile, $message, FILE_APPEND | LOCK_EX);  
        }

        // log the message as INFO message
        $this->_log->info($message);

}

}

?>