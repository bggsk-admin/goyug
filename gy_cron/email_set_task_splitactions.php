<?php //Рассылка письма

  //Библиотека
  include('cron_lib.php');

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылки письма';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //uid писем
  $uids = Array();

  //Регистрация рассылки
  $eso = Array();

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $path = "../gy_cron";
  }
  //На сервере
  else {
    $path = "/home/c/c74542/goyug.com/gy_cron";
  }

  //Проверяем создавалась ли уже рассылка
  if(!file_exists($path."/email_set_task_splitactions.txt")){

    //////////////////////////////////////////
    ///Рассылка для всех владельцев///////////
    //////////////////////////////////////////

      //////////////////////
      //Находим владельцев//
      //////////////////////

        //Список рассылки
        $users = Array();

        //Лист отправки - все владельцы
        $query = "SELECT DISTINCT(obj_u_id), obj_object.obj_addr_city_uid as city_uid, u_user.u_id as id, LCASE(u_user.u_email) as email, trim(concat(upper(mid(u_user.u_firstname,1,1)), mid(u_user.u_firstname,2))) as name, u_user.u_authtoken as authtoken, u_user.u_money
                  FROM `obj_object`
                  LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
                  WHERE
                    obj_object.obj_enable AND
                    u_user.u_email != '' AND
                    (/*obj_object.obj_addr_city_uid = 'ufa' OR obj_object.obj_addr_city_uid = 'stavropol' OR */obj_object.obj_addr_city_uid = 'nnovgorod'/* OR obj_object.obj_addr_city_uid = 'moscow' OR obj_object.obj_addr_city_uid = 'spb'*/) AND
                    obj_object.obj_u_id NOT IN (
                      SELECT cf_u_id FROM cf_cashflow WHERE cf_transaction != 'ontop_dummy' AND cf_mode = 'balanceoff' GROUP BY cf_u_id ORDER BY cf_u_id
                    )
                  ORDER BY obj_object.obj_addr_city_uid";
        //echo $query;
        $result = giveTable($query);

        $count = mysql_num_rows($result);

        if($count){
          while($rows = mysql_fetch_assoc($result)){
            unset($rows['obj_u_id']);
            $users[] = $rows;
          }//while
        }

        $log[] = 'Найдено '.$count.' адресатов';

        // echo '<pre>';
        // print_r($users);
        // echo '</pre>';

      //////////////////////////////////
      //Находим владельцев - окончание//
      //////////////////////////////////

      //////////////////
      //Сплит отправка//
      //////////////////

        $half_count = ceil($count/2);

        $log[] = '2 группы по: '.$half_count.' владельцев';

        //Идентификаторы рассылки
        $eso_uid = Array();
        $eso_uid[1] = md5('action_rub_1000_nnov'.$count.date("Y-m-d"));
        $eso_uid[2] = md5('action_rub_1000_nnov'.$half_count.date("Y-m-d"));

        echo $eso_uid[1].'<br />';
        echo $eso_uid[2].'<br />';

        //Заголовки писем
        $subjects = Array();
        $subjects[1] = 'Внимание! Вам активированы 1000 рублей.';
        $subjects[2] = 'Срочно! 1000 рублей на любые услуги ресурса.';

        //Исключенные адреса
        $emails_restric = Array();

        $i = 1;

        //Пользователи
        foreach ($users as $user_key => $user) {

          //Сегмент 1
          if($i <= $half_count){
            $segment_idx = 1;
          }
          //Сегмент 2
          else {
            $segment_idx = 2;
          }

          echo 'Сегмент '.$segment_idx.' запись - '.$i.'<br />';

          ///////////////////////////////////
          //Формируем индивидуальные письма//
          ///////////////////////////////////

            //Пропускаем исключенные из рассылки E-mail
            if(in_array($user['email'], $emails_restric)){
              $log[] = 'Исключение из рассылки: '.$user['email'];
              continue;
            }

            //Одноразовый токен
            $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));
            //Создаем, если он еще не назначен
            if($user['authtoken'] == ''){
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
              $user['authtoken'] = $authtoken;
            }

            //Таблица celt_cron_email_list_tasks
            $celt = Array();

            //Идентификатор письма
            $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
            $uids[] = $celt['celt_uid'];

            //Время создания
            $celt['celt_date_create'] = date("Y-m-d H:i:s");

            //Письмо
            $celt['celt_email_subject'] = $subjects[$segment_idx];
            $celt['celt_email_body'] = '
            <!DOCTYPE html>
            <html lang="ru">
            <head>
                <meta charset="UTF-8">
                <title>'.$subjects[$segment_idx].'</title>
            </head>
            <body>

            <p>Уважаемый, Владелец!</p>

            <p>Мы активировали Вам 1000 рублей в личном кабинете. Вы можете провести свой личный "Тест Драйв" сайта по посуточной аренде Goyug.com и воспользоваться любыми услугами ресурса.</p>

            <p>Потратить бонусные средства необходимо до 27 мая. Для их активации просто перейдите по ссылке:</p>
            <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/action_rub_1000_nnov_'.$segment_idx.'/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Активировать бонус</a>

            <p>
              --<br />
              С уважением,<br />
              команда Goyug.com
            </p>
            <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid[$segment_idx].'/u_id/'.$user['id'].'/">
            </body>
            </html>';

            // echo '<pre>';
            // print_r($celt);
            // echo '</pre>';

            //Запись в базу
            $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

            //Адресаты
            $cel = Array();
            $cel['cel_celt_uid'] = $celt['celt_uid'];
            $cel['cel_email'] = $user['email'];
            $cel['cel_date_stage'] = date("Y-m-d H:i:s");

            //Запись в базу
            $result_cel = insertToTable('cel_cron_email_list', $cel);

            // echo '<pre>';
            // print_r($cel);
            // echo '</pre>';

            //Останов в случае ошибки
            if(!$result_celt || !$result_cel){
              die(mysql_error());
            }

            //Прописываем акцию пользователю
            $action_stage = date("Y-m-d H:i:s");
            //$action_actual_to = date("Y-m-d H:i:s", time() + 3*(60*60*24));
            $action_actual_to = date("2016-06-02 23:59:59");
            goToTable("UPDATE u_user SET `u_action_enable` = 1, `u_action_type` = 'action_rub_1000_nnov_$segment_idx', `u_action_stage` = '$action_stage', `u_action_actual_to` = '$action_actual_to' WHERE u_id = $user[id]");

          ///////////////////////////////////////////////
          //Формируем индивидуальные письма - окончание//
          ///////////////////////////////////////////////

          $i++;

        }//foreach

        ////////////////////////
        //Регистрация рассылки//
        ////////////////////////

          $eso['eso_uid'] = $eso_uid[1];
          $eso['eso_mod'] = 'action_rub_1000_nnov_1';
          $eso['eso_count'] = $half_count;
          $eso['eso_date'] = date("Y-m-d");
          $eso['eso_desc'] = 'Сегмент 1 | ПОЛУЧИ 1000 РУБЛЕЙ - город ННОВГОРОД';
          if(!insertToTable('eso_email_stat_opens', $eso)){
            myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
          }

          $eso['eso_uid'] = $eso_uid[2];
          $eso['eso_mod'] = 'action_rub_1000_nnov_2';
          $eso['eso_count'] = $count - $half_count;
          $eso['eso_date'] = date("Y-m-d");
          $eso['eso_desc'] = 'Сегмент 2 | ПОЛУЧИ 1000 РУБЛЕЙ - город ННОВГОРОД';
          if(!insertToTable('eso_email_stat_opens', $eso)){
            myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
          }

        ////////////////////////////////////
        //Регистрация рассылки - окончание//
        ////////////////////////////////////

        //Для избежания повторной генерации писем создаем файл
        $text = "Задача поставлена для ".$count." адресатов.\n\r";
        $text .= "Из них исключено из рассылки ".count($emails_restric)." адресатов.\n\r";
        foreach ($uids as $key => $uid) {
          $text .= $uid."\n\r";
        }
        file_put_contents($path."/email_set_task_splitactions.txt", $text);

      //////////////////////////////
      //Сплит отправка - окончание//
      //////////////////////////////

    //////////////////////////////////////////////////////
    ///Рассылка для всех владельцев - окончание///////////
    //////////////////////////////////////////////////////

  } else {
    $log[] = 'Рассылка уже назначена';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылки письма</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
  }
?>
