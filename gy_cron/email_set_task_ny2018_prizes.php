<?php //Рассылка письма

  //Библиотека
  include('cron_lib.php');

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылки письма';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //uid писем
  $uids = Array();

  //Регистрация рассылки
  $eso = Array();

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $path = "../gy_cron";
  }
  //На сервере
  else {
    $path = "/var/www/goyug/gy_cron";
  }

// ID рассылки
$src_mod = "ny2018_prizes_5_5";
$eso_desc = "Новый Год 2018 (призы) 5-5";


  //Проверяем создавалась ли уже рассылка
  if(!file_exists($path."/email_set_task_ny2018_prizes.txt")){

    //////////////////////////////////////////
    ///Рассылка для всех владельцев///////////
    //////////////////////////////////////////

      //////////////////////
      //Находим владельцев//
      //////////////////////

        //Список рассылки
        $users = Array();

        //Лист отправки - все владельцы
        $query = "SELECT DISTINCT(obj_u_id), 
obj_object.obj_addr_city_uid AS city_uid, 
u_user.u_id AS id, 
LCASE(u_user.u_email) as email, 
TRIM(CONCAT(UPPER(MID(u_user.u_firstname,1,1)), MID(u_user.u_firstname,2))) AS name, 
u_user.u_authtoken AS authtoken, 
u_user.u_money
FROM obj_object
LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
WHERE u_user.u_email != ''
AND NOT ISNULL(u_user.u_email)
AND u_user.u_email LIKE '%@%.%'
AND obj_object.obj_addr_city_uid IN ('alushta','astrakhan','bryansk','vladimir','volgograd','vologda','voronezh','ekaterinburg','zheleznovodsk','ivanovo','irkutsk','kazan','kaliningrad','kemerovo','kirov','kislovodsk','kostroma','krasnodar','krasnoyarsk','kursk','lipetsk','magnitogorsk','moscow','nvartovsk','nnovgorod','novokuznetsk','novosibirsk','omsk','orel','orenburg','penza','perm','pskov','pyatigorsk','rostovnadonu','samara','spb','saransk','saratov','sevastopol','smolensk','sochi','stavropol','surgut','syktyvkar','taganrog','tver','tolyatty','tomsk','tula','tyumen','ulanude','ulyanovsk','ufa','khabarovsk','chelyabinsk','yalta','yaroslavl')
ORDER BY city_uid LIMIT 1133 OFFSET 4532";

// [X] - 11/01 - OFFSET 0
// [X] - 12/01 - OFFSET 1133
// [X] - 13/01 - OFFSET 2266
// [X] - 14/01 - OFFSET 3399
// [X] - 15/01 - OFFSET 4532

        //echo $query;
        $result = giveTable($query);

        if(mysql_num_rows($result)){
          while($rows = mysql_fetch_assoc($result)){
            unset($rows['obj_u_id']);
            $users[] = $rows;
          }//while
        }

        $log[] = 'Найдено '.count($users). ' адресатов';

      //////////////////////////////////
      //Находим владельцев - окончание//
      //////////////////////////////////

      ///////////////////////////////////
      //Формируем индивидуальные письма//
      ///////////////////////////////////

        //Идентификатор рассылки
        $eso_uid = md5($src_mod.count($users).date("Y-m-d"));

        foreach ($users as $key => $user) 
        {

          //Одноразовый токен
          $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));

          //Создаем, если он еще не назначен
          if($user['authtoken'] == '')
          {
            goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
            $user['authtoken'] = $authtoken;
          }

          //Таблица celt_cron_email_list_tasks
          $celt = Array();

          //Идентификатор письма
          $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
          $uids[] = $celt['celt_uid'];

          //Время создания
          $celt['celt_date_create'] = date("Y-m-d H:i:s");

          //Письмо
          $celt['celt_email_subject'] = 'GOYUG || Победители 2018!';
          
          $celt['celt_email_body'] =
          '<!DOCTYPE html>
          <html lang="ru">
          <head>
              <meta charset="UTF-8">
              <title>'.$celt['celt_email_subject'].'</title>
          </head>
          <body>

            <h2>Goyug.com - квартиры посуточно</h2>

            <p>Уважаемый Владелец!<p>
            <p>Сегодня у нас два повода Вам написать!</p>
            <p>Во-первых, поздравляем Вас с прошедшими праздниками, надеемся они принесли Вам радость.</p>
            <p>Во-вторых, наш Новогодний розыгрыш призов состоялся и мы готовы огласить и НАГРАДИТЬ ПОБЕДИТЕЛЕЙ.</p>
            <p>В нашей акции приняли участие более 1000 владельцев, и среди присланных заявок, случайным образом были выбраны 50 победителей, которые получили 2018 рублей на свой личный счет в личном кабинете.</p>
            
            <p><h3>Вот список ID владельцев-победителей:</h3></p>

            <p>
            162<br>
            771<br>
            876<br>
            906<br>
            1011<br>
            1132<br>
            1758<br>
            1792<br>
            1945<br>
            2087<br>
            2313<br>
            2318<br>
            2326<br>
            2380<br>
            2395<br>
            2658<br>
            2786<br>
            2884<br>
            3036<br>
            3226<br>
            3819<br>
            4091<br>
            4138<br>
            4162<br>
            4335<br>
            4647<br>
            4691<br>
            4850<br>
            4949<br>
            5070<br>
            5280<br>
            5426<br>
            5618<br>
            5699<br>
            5769<br>
            5895<br>
            6037<br>
            6240<br>
            6487<br>
            6575<br>
            6862<br>
            7063<br>
            7321<br>
            7322<br>
            7430<br>
            7522<br>
            7864<br>
            </p>

          <p>Чтобы узнать свой ID, войдите в личный кабинет, используя свое имя пользователя и пароль, в верхнем левом углу будет указан ваш ID.</p>

          <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/'.$src_mod.'/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Перейти в Личный Кабинет</a>

          <p>Выигранные средства Вы сможете потратить на нашем ресурсе, в течение всего 2018 года.</p>
          
          <p><br></p>
          <p>Благодарим за то что Вы с нами. Мы рады оказать свою помощь в Вашем нелегком бизнесе!</p>

          <p>
            --<br />
            С уважением,<br />
            команда GOYUG.COM
          </p>

          <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">

        </body>
        </html>';

          // ************* Запись в базу *********************************
          $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

          //Адресаты
          $cel = Array();
          $cel['cel_celt_uid'] = $celt['celt_uid'];
          $cel['cel_email'] = $user['email'];
          $cel['cel_reply'] = 'info@goyug.com';
          $cel['cel_date_stage'] = date("Y-m-d H:i:s");

          // ************* Запись в базу *********************************
          $result_cel = insertToTable('cel_cron_email_list', $cel);

          // Останов в случае ошибки
          if(!$result_celt || !$result_cel){
            die(mysql_error());
          }

        }
        //foreach

        //Для избежания повторной генерации писем создаем файл
        $text = "Задача поставлена для ".count($users)." адресатов.\n\r";

        foreach ($uids as $key => $uid) {
          $text .= $uid."\n\r";
        }

        file_put_contents($path."/email_set_task_ny2018_prizes.txt", $text);

      ///////////////////////////////////////////////
      //Формируем индивидуальные письма - окончание//
      ///////////////////////////////////////////////

      ////////////////////////
      //Регистрация рассылки//
      ////////////////////////
      $eso['eso_uid'] = $eso_uid;
      $eso['eso_mod'] = $src_mod;
      $eso['eso_count'] = count($users);
      $eso['eso_date'] = date("Y-m-d");
      $eso['eso_desc'] = $eso_desc;
        
        // ************* Запись в базу *********************************
        if(!insertToTable('eso_email_stat_opens', $eso))
        {
          myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
        }

      ////////////////////////////////////
      //Регистрация рассылки - окончание//
      ////////////////////////////////////

    //////////////////////////////////////////////////////
    ///Рассылка для всех владельцев - окончание///////////
    //////////////////////////////////////////////////////

  } else {
    $log[] = 'Рассылка уже назначена';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылки письма</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
  }
?>