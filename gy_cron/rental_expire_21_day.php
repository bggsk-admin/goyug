<?php //Предупреждение о просрочке абонимента - 21 день

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Стоимость услуги
  $rental_price = 100;

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  // Города с АБОНПЛАТОЙ
  $rental_cities = array(
    "kislovodsk", 
    "nnovgorod", 
    "sochi", "sevastopol", "spb", 
    "krasnodar", "saratov", "tyumen", 
    "novosibirsk", "ekaterinburg", "perm", 
    "kazan", "ufa", "voronezh", 
    "moscow", "omsk", "rostovnadonu", "volgograd",
    "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
    "zheleznovodsk",
    "pyatigorsk", "yalta", "yaroslavl", "tomsk",
    "bryansk", "tula", "magnitogorsk", "ivanovo",
    "khabarovsk", "vladimir", "penza", "kirov", "vologda",
    "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
    "astrakhan", "tolyatty", "orel", "orenburg", "tver",
    "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
);


  $rental_cities_query = implode("','", $rental_cities);

  //Выбираем пользователей
  $query = "SELECT *
FROM u_user AS u
LEFT JOIN (SELECT obj_u_id, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
ON obj.obj_u_id = u.u_id
WHERE obj_addr_city_uid IN ('" . $rental_cities_query. "')
AND u.u_rental = 0
AND u.u_rental_expire_21_day = 0
AND u.u_rental_deactivate <= NOW() - INTERVAL 21 DAY";

  $result = giveTable($query);

//
  //////////////////////////
  //Получаем пользователей//
  //////////////////////////

    $users = Array();

    if(mysql_num_rows($result)){

      while($rows = mysql_fetch_assoc($result)){

        //Пользователь умышленно отключил услугу - не трогаем его
        if($rows['u_rental_nextpay'] == '0000-00-00 00:00:00'){

        }
        //Пользователь не отключил услугу
        else if( $rows['u_rental_nextpay'] != '0000-00-00 00:00:00' ){
          $users[] = $rows;
        }

      }//while

    } else {
      $log[] = 'Нет объектов для напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день.';
    }

  //////////////////////////////////////
  //Получаем пользователей - окончание//
  //////////////////////////////////////

  //////////////
  //Отключение//
  //////////////

    foreach($users as $user){

      /////////////////////////
      ///Send E-mail///////////
      /////////////////////////

        //Пользователь найден
        if($user['u_id']){

          //Прописываем однократный токен, если он не назначен еще
          if(!$user['u_authtoken']){
            $authtoken = md5($user['u_email'].$user['u_id'].rand(1000, 9999));
            goToTable("UPDATE u_user SET `u_authtoken` = '".$authtoken."' WHERE u_id = ".$user['u_id']);
          } else {
            $authtoken = $user['u_authtoken'];
          }

          //Отмечаем отправку уведомления
          goToTable("UPDATE u_user SET `u_rental_expire_21_day` = 1 WHERE u_id = ".$user['u_id']);

          //Если указан E-mail - отправляем уведомление
          if($user['u_email']){
            //Письмо
            $email = Array();
            $email['to'] = $user['u_email'];
            $email['subject'] = 'GOYUG.COM - Ваши объявления недоступны для просмотра на сайте';
            $email['body'] = '
            <h2>Goyug.com - квартиры посуточно</h2>
            <p>Уважаемый владелец!</p>
            <p>Срок действия вашего абонемента истек</p>
            <p>На вашем счете в личном кабинете не достаточно средств для продления месячного абонемента, на нашем ресурсе.</p>
            <p>В данный момент ваши объявления НЕДОСТУПНЫ для просмотра на сайте GOYUG.COM</p>
            <p>Дата списания средств за абонплату: '.giveRealDataFull($user['u_rental_deactivate']).'</p>
            
            <p>Вам необходимо пополнить баланс в личном кабинете сайта.</p>
            <p>Стоимость месячного абонемента - '.$rental_price.' рублей.</p>
            
            <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/rental_expire_21_day/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
          }
          //Если не указан E-mail - оптравляем оповещение
          else {
            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день - E-mail владельца не указан';
            $email['body'] = '
            <h2>Внимание!</h2>
            <p>Владельцу с ID '.$user['u_id'].' не отправлено уведомление о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день, т.к. у него не указан E-mail.</p>';

            //Лог
            $errors[] = 'Владельцу с ID '.$user['u_id'].' не отправлено уведомление о напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день, т.к. у него не указан E-mail.';
          }
        }
        //Пользователь не найден
        else {

          //Письмо
          $email = Array();
          $email['to'] = 'info@goyug.com';
          $email['subject'] = 'Напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день - владелец не найден';
          $email['body'] = '
          <h2>Ахтунг!</h2>
          <p>Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

          //Лог
          $errors[] = 'Напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день. Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
        }

        //Отправка
        myMail($email['to'], $email['subject'], $email['body']);

      /////////////////////////
      ///Send E-mail end///////
      /////////////////////////

    }//foreach

  //////////////////////////
  //Отключение - окончание//
  //////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  echo "<h2>Скрипт напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день</h2>\n\r";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>'."\n\r";
  }
  //Отправка лога
  //myMail('marselos@gmail.com', 'Скрипт деактивации услуги', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт напоминание о ПРОСРОЧКЕ услуги АБОНЕНТСКАЯ ПЛАТА - 21 день</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения АБОНЕНТСКАЯ ПЛАТА', $bodymail);
  }
?>
