<?php //Снятие выделения поднятого объекта

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт удаления меток отправки веера';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  ////Тестируем временной сегмент
  //Путь к папке с файлами карт сайта
  /*
  //На сервере
  if( strpos($_SERVER['HTTP_HOST'], 'goyug.com') !== false ){
      $path = "/home/c/c74542/goyug.com/time_segment/";
  }
  //Локально
  else {
      $path = "../time_segment/";
  }
  */
  $path = "/home/c/c74542/goyug.com/time_segment/";
  if(!file_exists($path)) mkdir($path);

  //Текущий час
  $current_hour = date("G");

  //Проверка сегментов
  /*
  $segments[] = Array();
  $segments['from_0_to_8'] = file_exists($path.'from_0_to_8');
  $segments['from_8_to_16'] = file_exists($path.'from_8_to_16');
  $segments['from_16_to_0'] = file_exists($path.'from_16_to_0');
  */

  //Удаление блок-файла сегмента
  function removeBlockFile($segment){
    global $path;
    //Все файлы
    $files = scandir($path);
    $files = array_diff($files, Array(".", ".."));

    //Удаляем файлы нужного сегмента
    foreach ($files as $key => $file) {
      if(strpos($file, $segment) !== false){
        unlink($path.$file);
      }
    }
  }

  //Интервал 1
  if($current_hour >= 0 && $current_hour < 8){
    removeBlockFile('from_8_to_16');
    removeBlockFile('from_16_to_0');
  }
  //Интервал 2
  else if ($current_hour >= 8 && $current_hour < 16){
    removeBlockFile('from_0_to_8');
    removeBlockFile('from_16_to_0');
  }
  //Интервал 3
  else if ($current_hour >= 16 && $current_hour <=23){
    removeBlockFile('from_0_to_8');
    removeBlockFile('from_8_to_16');
  }
  //Если не найдено интервала
  else {
    file_put_contents($path.'bad_time_cron', $current_hour);
  }
  ////Тестируем временной сегмент - окончание

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  /*
  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт снятия выделения объектов</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт снятия выделения', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия выделения объектов</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка удаления меток отправки веера', $bodymail);
  }
?>
