<?php //Генерация для Яндекс.Реалти

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $realty_folder = "../public_html/realty";
  }
  //На сервере
  else {
    $realty_folder = "/var/www/goyug/public_html/realty";
  }

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт генерации Яндекс.Реалти';
  $log[] = 'Начало работы скрипта: '.$date_current;

  //Выбираем города
  $queryCities = "SELECT ct_uid, ct_name_ru FROM ct_city WHERE ct_enable /*AND ct_uid = 'krasnodar'*/ ORDER BY ct_uid";
  $resultCities = giveTable($queryCities);

  ////Пишем файлы
  //Корневая папка реалти
  if(!file_exists($realty_folder)) mkdir($realty_folder);

  //Проход по городам
  if(mysql_num_rows($resultCities)){

    //Проход по городам
    while($rowsCities = mysql_fetch_assoc($resultCities)){

      ////Магия
      //Поддомены
      $file_path = $realty_folder.'/'.$rowsCities['ct_uid'].".feed.xml";

      $xml = new DomDocument('1.0', 'utf-8');

      //Корень фида
      $root = $xml->appendChild($xml->createElement('realty-feed'));
      $root_xmlns = $root->appendChild($xml->createAttribute('xmlns'));
      $root_xmlns->value = 'http://webmaster.yandex.ru/schemas/feed/realty/2010-06';

      //Дата генерации
      $offer = $root->appendChild($xml->createElement('generation-date'));
      $offer_generation_date = $offer->appendChild($xml->createTextNode(date("c")));

      //Объекты
      $resultObjects = giveTable(
        "SELECT obj.obj_id, DATE_FORMAT(obj.obj_create, '%Y-%m-%dT%TZ') AS obj_create, DATE_FORMAT(obj.obj_update, '%Y-%m-%dT%TZ') AS obj_update,
         obj.obj_addr_country, obj.obj_addr_city_uid, obj.obj_addr_city, obj.obj_addr_street, obj.obj_addr_number,
         obj.obj_price, obj.obj_sqr, obj.obj_rooms,
         u.u_phone_1,
         f.f_name
         FROM obj_object AS obj
         LEFT JOIN u_user AS u ON u.u_id = obj.obj_u_id
         LEFT JOIN f_files AS f ON f.f_uid = obj.obj_id
         WHERE
          obj.obj_addr_city_uid = '$rowsCities[ct_uid]' AND
          obj.obj_enable AND
          obj.obj_price >= 500 AND
          u.u_phone_1 != '' AND
          f.f_name != ''
         GROUP BY obj.obj_id
         ORDER BY obj.obj_id DESC");

      $rowsCount = 0;

      while($rowObject = mysql_fetch_assoc($resultObjects)){

        //Дата модификации
        $lastmod = $rowObject['obj_update'];
        if($lastmod == '0000-00-00 00:00:00' || !$lastmod) $lastmod = $rowObject['obj_create'];
        if($lastmod == '0000-00-00 00:00:00' || !$lastmod) $lastmod = date("с");

        //Объявление
        $offer = $root->appendChild($xml->createElement('offer'));
        $offer_internal_id = $offer->appendChild($xml->createAttribute('internal-id'));
        $offer_internal_id->value = $rowObject['obj_id'];

        //Тип сделки
        $offer_type = $offer->appendChild($xml->createElement('type'));
        $offer_type->appendChild($xml->createTextNode('аренда'));

        //Тип недвижимости
        $offer_property_type = $offer->appendChild($xml->createElement('property-type'));
        $offer_property_type->appendChild($xml->createTextNode('жилая'));

        //Категория
        $offer_category = $offer->appendChild($xml->createElement('category'));
        $offer_category->appendChild($xml->createTextNode('квартира'));

        //URL объявления на сайте
        $offer_url = $offer->appendChild($xml->createElement('url'));
        $offer_url->appendChild($xml->createTextNode('http://'.$rowObject['obj_addr_city_uid'].'.goyug.com/'.$rowObject['obj_id']));

        //Дата создания
        $offer_creation_date = $offer->appendChild($xml->createElement('creation-date'));
        $offer_creation_date->appendChild($xml->createTextNode($rowObject['obj_create']));

        //Дата редактирования
        $offer_last_update_date = $offer->appendChild($xml->createElement('last-update-date'));
        $offer_last_update_date->appendChild($xml->createTextNode($lastmod));

        //Местоположение объекта
        $offer_location = $offer->appendChild($xml->createElement('location'));
          //Страна
          $offer_location_country = $offer_location->appendChild($xml->createElement('country'));
          $offer_location_country->appendChild($xml->createTextNode( $rowObject['obj_addr_country'] ? $rowObject['obj_addr_country'] : 'Россия' ));
          //Название населенного пункта
          $offer_location_locality_name = $offer_location->appendChild($xml->createElement('locality-name'));
          $offer_location_locality_name->appendChild($xml->createTextNode($rowObject['obj_addr_city']));
          //Улица и номер дома
          $offer_location_address = $offer_location->appendChild($xml->createElement('address'));
          $offer_location_address->appendChild($xml->createTextNode($rowObject['obj_addr_street'].', '.$rowObject['obj_addr_number']));

        //Информация о продавце или арендодателе
        $offer_sales_agent = $offer->appendChild($xml->createElement('sales-agent'));

          //Контактный номер телефона
          $offer_sales_agent_phone = $offer_sales_agent->appendChild($xml->createElement('phone'));
          $offer_sales_agent_phone->appendChild($xml->createTextNode(getHumanTel($rowObject['u_phone_1'])));

          //Тип продавца или арендодателя
          $offer_sales_agent_category = $offer_sales_agent->appendChild($xml->createElement('category'));
          $offer_sales_agent_category->appendChild($xml->createTextNode('владелец'));

        //Информация о стоимости
        $offer_price = $offer->appendChild($xml->createElement('price'));

          //Цена (сумма указывается без пробелов)
          $offer_price_value = $offer_price->appendChild($xml->createElement('value'));
          $offer_price_value->appendChild($xml->createTextNode($rowObject['obj_price']));

          //Валюта, в которой указана цена
          $offer_price_currency = $offer_price->appendChild($xml->createElement('currency'));
          $offer_price_currency->appendChild($xml->createTextNode('RUB'));

          //Единица времени для длительности аренды
          $offer_price_period = $offer_price->appendChild($xml->createElement('period'));
          $offer_price_period->appendChild($xml->createTextNode('день'));

        //Общая площадь
        $offer_area = $offer->appendChild($xml->createElement('area'));

          //Площадь (числовое значение)
          $offer_area_value = $offer_area->appendChild($xml->createElement('value'));
          $offer_area_value->appendChild($xml->createTextNode($rowObject['obj_sqr']));

          //Единица площади (рекомендуемые значения — «кв. м», «sq.m»)
          $offer_area_unit = $offer_area->appendChild($xml->createElement('unit'));
          $offer_area_unit->appendChild($xml->createTextNode('кв.м'));

        //Фотография (может быть несколько тегов)
        $resultImages = giveTable(
          "SELECT f_name
           FROM f_files
           WHERE
            f_uid = $rowObject[obj_id] AND
            f_name != ''
           ORDER BY f_order");
        if(mysql_num_rows($resultImages)){
          while($rowImage = mysql_fetch_assoc($resultImages)){
            $offer_image = $offer->appendChild($xml->createElement('image'));
            $offer_image->appendChild($xml->createTextNode('http://'.$rowObject['obj_addr_city_uid'].'.goyug.com/upload/object/'.$rowObject['obj_id'].'/'.$rowImage['f_name']));
          }
        }

        //Общее количество комнат в квартире
        $offer_rooms = $offer->appendChild($xml->createElement('rooms'));
        $offer_rooms->appendChild($xml->createTextNode($rowObject['obj_rooms']));

        $rowsCount++;

      }//while
      //Объекты - окончание
      ////Магия - окончание

      //Сохраняем файл
      $xml->formatOutput = true;
      $xml->save($file_path);

      //Лог
      $log[] = 'Яндекс.Реалти для города '.$rowsCities['ct_name_ru'].' сгенерирована. Объектов: '.$rowsCount;
    }//while

  } else {
    //Лог
    $log[] = 'Нет городов для генерации реалти.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  echo "<h2>Скрипт генерации Яндекс.Реалти</h2>";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>'."\n\r";
  }
  //Отправка лога
  //myMail('marselos@gmail.com', 'Скрипт генерации Яндекс.Реалти', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт генерации Яндекс.Реалти</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в генерации Яндекс.Реалти', $bodymail);
  }

?>