<?php //Установка оплаты за размещение

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт установки оплаты за размещение';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  // Города в которых включаем АБОНПЛАТУ в данный момент
  $rental_cities = array("syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa");
  $rental_cities_query = implode("','", $rental_cities);

  //Группу пользователей
  $query = "SELECT u.u_id, u.u_email, obj.obj_addr_city_uid
FROM u_user as u
LEFT JOIN obj_object as obj ON obj.obj_u_id = u.u_id
WHERE obj.obj_addr_city_uid IN ('" . $rental_cities_query. "')
AND NOT ISNULL(u.u_email)
AND u.u_email LIKE '%@%.%'
GROUP BY u.u_id";

  $result = giveTable($query);

  //Прописываем токены пользователям
  if(mysql_num_rows($result)){
    while($user = mysql_fetch_assoc($result)){

      //Включение
      //$date_activate = date("Y-m-d H:i:s");
      
      // kislovodsk - 1.09.2016
      
      // nnovgorod - 1.11.2016
      
      // sochi - 1.02.2017 (старт выставлен на 1.01.2017 "задним числом", чтобы триал занял 2 недели, с 15го января, и абонплата стартовала 1.02.2017)
      // sevastopol - 1.02.2017
      // spb - 1.02.2017
      
      // krasnodar - 1.03.2017
      // saratov - 1.03.2017
      // tyumen - 1.03.2017
      
      // novosibirsk - 1.04.2017
      // ekaterinburg - 1.04.2017
      // perm - 1.04.2017
      
      // kazan - 2.05.2017
      // ufa - 2.05.2017
      // voronezh - 2.05.2017
      
      // moscow - 10.07.2017
      // omsk - 10.07.2017
      // rostovnadonu - 10.07.2017
      // volgograd - 10.07.2017

      // krasnoyarsk - 1.09.2017
      // chelyabinsk - 1.09.2017
      // irkutsk - 1.09.2017
      // stavropol - 1.09.2017
      // samara - 1.09.2017
      // 
      // zheleznovodsk - 1.10.2017
      // 
      // pyatigorsk - 1.12.2017
      // yalta - 1.12.2017
      // yaroslavl - 1.12.2017
      // tomsk - 1.12.2017
      // 
      // bryansk - 1.02.2018
      // tula - 1.02.2018
      // magnitogorsk - 1.02.2018
      // ivanovo - 1.02.2018
      // 
      // khabarovsk - 1.04.2018
      // vladimir - 1.04.2018
      // penza - 1.04.2018
      // kirov - 1.04.2018
      // vologda - 1.04.2018
      // 
      // surgut - 1.05.2018
      // lipetsk - 1.05.2018
      // ulyanovsk - 1.05.2018
      // kemerovo - 1.05.2018
      // kursk - 1.05.2018
      // 
      // astrakhan - 1.06.2018
      // tolyatty - 1.06.2018
      // orel - 1.06.2018
      // orenburg - 1.06.2018
      // tver - 1.06.2018
      // 
      // -- 1.07.2018 --
      // syktyvkar
      // saransk
      // ryazan
      // pskov
      // novocherkassk
      // adler
      // anapa

      $date_activate = "2018-06-20 09:00:01";
      $date_activate_a = explode(' ', $date_activate);
      $date_activate_a_date = explode('-', $date_activate_a[0]);
      $date_activate_a_time = explode(':', $date_activate_a[1]);

      //Списание следующего платежа
      // $date_nextpay = date("Y-m-d H:i:s", mktime($date_activate_a_time[0], $date_activate_a_time[1], $date_activate_a_time[2], $date_activate_a_date[1], $date_activate_a_date[2], $date_activate_a_date[0]) + (60*60*24)*30);
      $date_nextpay = "2018-07-01 09:00:01";

      //Деактивация услуги
      // $date_deactivate = date("Y-m-d H:i:s", mktime($date_activate_a_time[0], $date_activate_a_time[1], $date_activate_a_time[2], $date_activate_a_date[1], $date_activate_a_date[2], $date_activate_a_date[0]) + (60*60*24)*30);
      $date_deactivate = "2018-07-01 09:00:01";

      if(!goToTable("UPDATE u_user SET `u_rental` = 1, `u_rental_activate` = '$date_activate', `u_rental_nextpay` = '$date_nextpay', `u_rental_deactivate` = '$date_deactivate' WHERE u_id = ".$user['u_id'])){
        $errors[] = 'Ошибка назначения абонплаты, u_id: '.$user['u_id'].'<br />'.mysql_error();
      }
      else {
        $log[] = 'Пользователю назначена абонплата, u_id: '.$user['u_id'];
      }
    }//while
  } else {
    $log[] = 'Нет пользователей в группе.';
    $errors[] = 'Нет пользователей в группе.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт установки оплаты за размещение</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт установки оплаты за размещение', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт установки оплаты за размещение</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Скрипт установки оплаты за размещение - ошибка', $bodymail);
  }
?>
