<?php
//Рассылка письма

//Библиотека
include('cron_lib.php');

//Лог
$log = Array();
$errors = Array();
$log[] = 'Вас приветствует скрипт рассылки письма';
$log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

//uid писем
$uids = Array();

//Регистрация рассылки
$eso = Array();

//Путь к папке с файлами карт сайта
//Локально
if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
  $path = "../gy_cron";
}
//На сервере
else {
  $path = "/var/www/goyug/gy_cron";
}

//////////////////////////////////////////
/// ВНОСИТЬ ИЗМЕНЕНИЯ ТУТ      ///////////
//////////////////////////////////////////

// ID рассылки
$src_mod = "springbonus";
$eso_desc = "Весенняя акция для городов-абонентов";

// Расписание рассылки с кол-вом имейлов
// [день_месяца] => [OFFSET в базе]
$query_offset_shedule = array(
  '26' => '0',
  '27' => '1085',
  '28' => '2170'
);
$cur_day = date('j');
$query_offset = $query_offset_shedule[$cur_day];


//Проверяем создавалась ли уже рассылка
if(array_key_exists ( $cur_day , $query_offset_shedule )){
// if(!file_exists($path."/email_set_task_ontopsale.txt")){

  //////////////////////////////////////////
  ///Рассылка для всех владельцев///////////
  //////////////////////////////////////////

  //////////////////////
  //Находим владельцев//
  //////////////////////

  //Список рассылки
  $users = Array();

  //Лист отправки - все владельцы
  // Если надо разослать всем кроме виртуалов
  // AND (u.u_notes != 'virtrental' OR ISNULL(u.u_notes) OR u.u_notes = '')

  $query = "
  SELECT 
      obj_u_id, 
      obj.obj_addr_city_uid AS city_uid, 
      u.u_id AS id,
      LCASE(u.u_email) as email, 
      TRIM(CONCAT(UPPER(MID(u.u_firstname,1,1)), MID(u.u_firstname,2))) AS name, 
      u.u_authtoken AS authtoken, 
      u.u_money
  FROM u_user AS u

  LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
  ON obj.obj_u_id = u.u_id

  WHERE u_rental = 0
  AND u_role = 'owner'
  AND obj.obj_addr_city_uid IN ('kislovodsk', 'nnovgorod', 'sochi', 'sevastopol', 'spb', 'krasnodar', 'saratov', 'tyumen', 'novosibirsk', 'ekaterinburg', 'perm', 'kazan', 'ufa', 'voronezh', 'moscow', 'omsk', 'rostovnadonu', 'volgograd', 'krasnoyarsk', 'chelyabinsk', 'irkutsk', 'stavropol', 'samara', 'zheleznovodsk', 'pyatigorsk', 'yalta', 'yaroslavl', 'tomsk', 'bryansk', 'tula', 'magnitogorsk', 'ivanovo', 'khabarovsk', 'vladimir', 'penza', 'kirov', 'vologda')
  AND (u.u_email != '' AND NOT ISNULL(u.u_email) AND u.u_email LIKE '%@%.%')
  AND u.u_id NOT IN (28,36,39,58,76,100,105,121,215,294,347,393,398,412,413,528,534,545,550,558,583,584,587,618,625,637,640,667,671,678,682,708,731,759,764,771,773,784,861,865,868,888,925,966,970,991,1053,1120,1164,1168,1181,1197,1202,1207,1229,1273,1290,1292,1314,1316,1321,1327,1336,1339,1357,1358,1377,1390,1402,1421,1472,1591,1659,1663,1710,1745,1790,1791,1811,1814,1836,1848,1865,1871,1936,1967,1976,2052,2216,2229,2236,2243,2249,2380,2441,2456,2485,2510,2512,2517,2573,2585,2597,2634,2729,2738,2753,2757,2783,2789,2790,2858,2980,2991,2993,2998,3002,3020,3047,3054,3055,3077,3085,3124,3144,3150,3185,3193,3226,3310,3396,3401,3432,3445,3493,3506,3539,3545,3554,3580,3612,3696,3718,3728,3749,3751,3757,3759,3761,3770,3793,3807,3893,3896,3899,3932,4010,4037,4061,4063,4079,4162,4166,4191,4226,4231,4237,4295,4312,4331,4335,4345,4485,4495,4520,4525,4606,4624,4640,4646,4679,4685,4689,4691,4721,4756,4773,4850,4976,4996,5348,5349,5361,5570,5588,5589,5609,5625,5634,5691,5880,5917,5951,5959,5975,5984,6012,6487,7011,7048,7050,7063,7072,7095,7175,7649,7677,8478,8519,8551,8554,8555,8558,8603,8634,8644,8673,8685,8750,8755,8846,8849,8853,8856,8926,8951,8960,8969,8983,8986,9043,9046,9111,9135,9210,9224,9228,9242,9267,9302,9389,9412,9426,9427,9428,9448,9459,9477,9480,9481,9482,9485,9487,9489,9491,9493,9495,9498,9500,9502,9504,9506,9508,9513,9515,9517,9519,9539,9542,9566,9568,9570,9572,9574,9585,9612,9624,9648,9651,9673,9675,9677,9679,9693,9711,9714,9719,9721,9724,9726,9732,9733,9737,9739,9740,9744,9745,9746,9748,9749,9751,9752,9753,9754,9758,9765,9773)

  LIMIT 1085 OFFSET " . $query_offset;

  //echo $query;
  $result = giveTable($query);

  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      unset($rows['obj_u_id']);
      $users[] = $rows;
    }//while
  }

  $log[] = 'Найдено '.count($users). ' адресатов';

  //////////////////////////////////
  //Находим владельцев - окончание//
  //////////////////////////////////

  ///////////////////////////////////
  //Формируем индивидуальные письма//
  ///////////////////////////////////

  //Идентификатор рассылки
  $eso_uid = md5($src_mod.count($users).date("Y-m-d"));

  foreach ($users as $key => $user) 
  {

    //Одноразовый токен
    $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));

    //Создаем, если он еще не назначен
    if($user['authtoken'] == '')
    {
      goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
      $user['authtoken'] = $authtoken;
    }

    //Таблица celt_cron_email_list_tasks
    $celt = Array();

    //Идентификатор письма
    $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
    $uids[] = $celt['celt_uid'];

    //Время создания
    $celt['celt_date_create'] = date("Y-m-d H:i:s");

    //Письмо
    $celt['celt_email_subject'] = 'GOYUG || Весенние Бонусы!';
          
    $celt['celt_email_body'] =
    '<!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>'.$celt['celt_email_subject'].'</title>
    </head>
    <body>

      <h2>Goyug.com - квартиры посуточно</h2>

      <p>Уважаемый Владелец!</p>
      <p>При оплате абонемента в течении апреля и мая мы подготовили для вас <strong>ВЕСЕННЕЕ ПРЕДЛОЖЕНИЕ</strong> и приятный <strong>БОНУС!</strong></p>

      <p>Оплатите абонемент на 1, 2 или 3 месяца и мы поднимем один из ваших объектов соответственно на 30, 60 или 90 дней <strong>БЕСПЛАТНО.</strong></p>

      <p>Для того чтобы получить поднятие объекта в подарок, зайдите в свой личный кабинет по ссылке ниже, пополните свой личный счет и нажмите кнопку "Подключить абонемент".</p>
      <p>После этого пришлите нам ID своего объекта, который вы хотите поднять, через форму обратной связи в личном кабинете (ID можно узнать на странице со списком ваших объектов)</p>

      <p>... и получите бонус за продление абонемента:</p>

      <ul>
        <li>Продление абонемента на 1 месяц (100 рублей) = поднятие объекта на 30 дней бесплатно (Бонус 220 рублей)</li>
        <li>--\\-- 2 месяца = поднятие на 60 дней бесплатно (Бонус 420 рублей)</li>
        <li>--\\-- 3 месяца = поднятие на 90 дней бесплатно (Бонус 620 рублей)</li>
      </ul>

      <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/'.$src_mod.'/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Перейти в Личный Кабинет</a>

      <p><br></p>
      <p>Благодарим за то что Вы с нами. Мы рады оказать свою помощь в Вашем нелегком бизнесе!</p>

      <p>
        --<br />
        С уважением,<br />
        команда GOYUG.COM
      </p>

      <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">

    </body>
    </html>';

    // ************* Запись в базу *********************************
    $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

    //Адресаты
    $cel = Array();
    $cel['cel_celt_uid'] = $celt['celt_uid'];
    $cel['cel_email'] = $user['email'];
    $cel['cel_reply'] = 'info@goyug.com';
    $cel['cel_date_stage'] = date("Y-m-d H:i:s");

    // ************* Запись в базу *********************************
    $result_cel = insertToTable('cel_cron_email_list', $cel);

    // Останов в случае ошибки
    if(!$result_celt || !$result_cel){
      die(mysql_error());
    }

  }
  //foreach

  //Для избежания повторной генерации писем создаем файл
  $text = "Задача поставлена для ".count($users)." адресатов.\n\r";

  foreach ($uids as $key => $uid) {
    $text .= $uid."\n\r";
  }

  file_put_contents($path."/email_set_task_springbonus.txt", $text);

  ///////////////////////////////////////////////
  //Формируем индивидуальные письма - окончание//
  ///////////////////////////////////////////////

  ////////////////////////
  //Регистрация рассылки//
  ////////////////////////
  $eso['eso_uid'] = $eso_uid;
  $eso['eso_mod'] = $src_mod;
  $eso['eso_count'] = count($users);
  $eso['eso_date'] = date("Y-m-d");
  $eso['eso_desc'] = $eso_desc;
  
  // ************* Запись в базу *********************************
  if(!insertToTable('eso_email_stat_opens', $eso))
  {
    myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
  }

} else {
  $log[] = 'Рассылка уже назначена';
}
////////////////////////////////////
//Регистрация рассылки - окончание//
////////////////////////////////////

//////////////////////////////////////////////////////
///Рассылка для всех владельцев - окончание///////////
//////////////////////////////////////////////////////

//Окончание работы
$log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

////Оповещение каждого выполнения
//Сериализация лога
$bodymail = "<h2>Скрипт рассылки письма</h2>";
foreach ($log as $value) {
  $bodymail .= '<p>'.$value.'</p>';
}

//Отправка лога
usleep(500000);
myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

////Оповещение только в случае ошибки
if(count($errors)){
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($errors as $error) {
    $bodymail .= '<p>'.$error.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
}

?>