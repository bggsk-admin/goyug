<?php //Генерация одноразовых токенов для группы пользователей

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт генерации одноразовых токенов группе пользователей';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Группу пользователей
  $query = "SELECT u_id, u_email FROM u_user WHERE u_email != '' AND u_authtoken = ''";
  $result = giveTable($query);

  //Прописываем токены пользователям
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      $token = md5($rows['u_email'].$rows['u_id'].rand(1000, 9999));
      if(!goToTable("UPDATE u_user SET `u_authtoken` = '$token' WHERE u_id = $rows[u_id]")){
        $errors[] = 'Ошибка назначения одноразового токена, u_id: '.$rows['u_id'].'<br />'.mysql_error();
      }
      else
        $log[] = 'Пользователю назначен одноразовый токен, u_id: '.$rows['u_id'].', token: '.$token;
    }//while
  } else {
    $log[] = 'Нет пользователей в группе.';
    $errors[] = 'Нет пользователей в группе.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт генерации одноразовых токенов группе пользователей</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт формирования одноразовых токенов', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт генерации одноразовых токенов группе пользователей</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в генерации одноразовых токенов', $bodymail);
  }
?>
