<?php //Деактивация функции ЗАПРОС ПО ПАРАМЕТРАМ

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Стоимость функции
  $paramsorder_price = 0;

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Выбираем оъекты
  $query = "SELECT obj.obj_id, obj.obj_u_id, obj.obj_paramsorder_nextpay, obj.obj_paramsorder_deactivate, obj.obj_addr_city, obj.obj_addr_city_uid, obj.obj_addr_street, obj.obj_addr_number,
                   u.u_id, u.u_money, u.u_email, u.u_authtoken
            FROM obj_object as obj
            LEFT JOIN u_user as u ON obj.obj_u_id = u.u_id
            WHERE obj_paramsorder AND !obj_phantom AND obj_paramsorder_deactivate <= '$date_current' AND obj_u_id AND u_id LIMIT 200";
  $result = giveTable($query);

  /////////////////////////
  //Разбиваем на сегменты//
  /////////////////////////
    $objects_to_nextpay = Array();
    $objects_to_deactivate = Array();
    $objects_to_deactivate_with_notification = Array();

    if(mysql_num_rows($result)){

      while($rows = mysql_fetch_assoc($result)){

        //На деактивацию
        if($rows['obj_paramsorder_nextpay'] == '0000-00-00 00:00:00'){
          $objects_to_deactivate[] = $rows;
        }
        //На продление функции
        else if( $rows['obj_paramsorder_nextpay'] != '0000-00-00 00:00:00' && $rows['u_money'] >= $paramsorder_price ){
          $objects_to_nextpay[] = $rows;
        }
        //На деактивацию с уведомением о недостаточных средствах
        else if( $rows['obj_paramsorder_nextpay'] != '0000-00-00 00:00:00' && $rows['u_money'] < $paramsorder_price ){
          $objects_to_deactivate_with_notification[] = $rows;
        }

      }//while
    } else {
      $log[] = 'Нет объектов для деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ.';
    }
  /////////////////////////////////////
  //Разбиваем на сегменты - окончание//
  /////////////////////////////////////

  ///////////////
  //Деактивация//
  ///////////////
    foreach($objects_to_deactivate as $object){

      if(!goToTable("UPDATE obj_object SET obj_paramsorder = 0 WHERE obj_id = $object[obj_id]")){
        $errors[] = 'Ошибка деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'].'<br />'.mysql_error();
      }
      else {

        /////////////////////////
        ///Send E-mail///////////
        /////////////////////////

          //Пользователь найден
          if($object['u_id']){

            //Прописываем однократный токен, если он не назначен еще
            if(!$object['u_authtoken']){
              $authtoken = md5($object['u_email'].$object['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $object[u_id]");
            } else {
              $authtoken = $object['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($object['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $object['u_email'];
              $email['subject'] = 'Статус объекта «ЗАПРОС ПО ПАРАМЕТРАМ» деактивирован';
              $email['body'] = '
              <h2>Уважаемый владелец!</h2>
              <p>Статус вашего объекта «ЗАПРОС ПО ПАРАМЕТРАМ» по адресу: г. '.$object['obj_addr_city'].', '.$object['obj_addr_street'].' '.$object['obj_addr_number'].' деактивирован.</p>
              <h3>Повторная активация функции</h3>
              <p>Для того, чтобы повторно активировать функцию «ЗАПРОС ПО ПАРАМЕТРАМ», необходимо зайти в ваш Личный Кабинет и перейти по ссылке «Включить «ЗАПРОС ПО ПАРАМЕТРАМ» в блоке управления объектом.</p>
              <p>Стоимость функции «ЗАПРОС ПО ПАРАМЕТРАМ» '.$paramsorder_price.' рублей <em>за 30 суток</em>.</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/paramsorder_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Деактивация функции ЗАПРОС ПО ПАРАМЕТРАМ - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$object['u_id'].' не отправлено уведомление о деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ, т.к. у него не указан E-mail.</p>';

              //Лог
              $errors[] = 'Владельцу с ID '.$object['u_id'].' не отправлено уведомление о деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ, т.к. у него не указан E-mail.';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Деактивация функции ЗАПРОС ПО ПАРАМЕТРАМ - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Деактивация функции объекта ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
        /////////////////////////
        ///Send E-mail end///////
        /////////////////////////

        $log[] = 'На объекте деактивирована услуга ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'];
      }
    }//foreach
  ///////////////////////////
  //Деактивация - окончание//
  ///////////////////////////

  /////////////
  //Продление//
  /////////////
    foreach($objects_to_nextpay as $object){

      //Повторно вытаскиваем пользователя для того, чтобы проверить баланс
      $result_row_u_user = giveTable("SELECT u_money FROM u_user WHERE u_id = ".$object['u_id']." LIMIT 1");
      if(mysql_num_rows($result_row_u_user)){
        $row_u_user = mysql_fetch_assoc($result_row_u_user);

        //Проверяем достаточность средств на балансе
        if($row_u_user['u_money'] >= $paramsorder_price){

          ////////////////////////
          //Все норм. продлеваем//
          ////////////////////////

            //Дата следующего списания
            $date_compound_a = explode(" ", $object['obj_paramsorder_nextpay']);
            $date_date_a = explode("-", $date_compound_a[0]);
            $date_time_a = explode(":", $date_compound_a[1]);

            $date_nextpay = date("Y-m-d H:i:s", mktime( $date_time_a[0], $date_time_a[1], $date_time_a[2], $date_date_a[1], $date_date_a[2], $date_date_a[0] ) + (60*60*24)*30);
            $date_deactivate = $date_nextpay;

            //Продление
            if(goToTable("UPDATE obj_object SET `obj_paramsorder` = 1, `obj_paramsorder_nextpay` = '$date_nextpay', `obj_paramsorder_deactivate` = '$date_deactivate' WHERE obj_id = $object[obj_id]")){

              ////////////////////
              //Списание средств//
              ////////////////////

                //Транзакция
                $transaction = Array(
                  "cf_u_id" => $object['u_id'],
                  "cf_obj_id" => $object['obj_id'],
                  "cf_transaction" => "sale",
                  "cf_table" => "obj_object",
                  "cf_table_id" => $object['obj_id'],
                  "cf_sum" => $paramsorder_price,
                  "cf_mode" => "minus",
                  "cf_date" => date("Y-m-d H:i:s")
                  );

                //Выполнение транзакции
                $resultTransaction = insertToTable('cf_cashflow', $transaction);
                $cf_id = 0;

                //Если транзакция зарегистрирована
                if($resultTransaction) {

                  //ID транзакции
                  $cf_id = mysql_insert_id();

                  //Вычитаем денежку со счета
                  $row_u_user['u_money'] -= $paramsorder_price;

                  //Если баланс ушел в минус - обнуляем его
                  if($row_u_user['u_money'] < 0) $row_u_user['u_money'] = 0;

                  //Сохранение пользователя
                  $resultUserUpdate = goToTable("UPDATE u_user SET `u_money` = '".$row_u_user['u_money']."' WHERE u_id = ".$object['u_id']);

                  //Успех транзакции
                  if($resultUserUpdate) {
                    $cf_success = 1;
                  } else {
                    $cf_success = 0;
                  }

                  //Обновляем транзакцию
                  $transactionUpdate = goToTable("UPDATE cf_cashflow SET `cf_success` = '".$cf_success."' WHERE cf_id = ".$cf_id);

                  if($transactionUpdate){
                    $log[] = 'Списание денег за продление функции ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].'. Операция проведена успешна.';
                  } else {
                    $errors[] = 'Ошибка списания денег за продление функции ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].'. Транзакция не подтверждена.';
                  }
                } else {
                  $errors[] = 'Ошибка списания денег за продление функции ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].'. Транзакция не зарегистрирована. Срочно все проверьте!';
                }

              ////////////////////////////////
              //Списание средств - окончание//
              ////////////////////////////////

              /////////////////////////
              ///Send E-mail///////////
              /////////////////////////

                //Пользователь найден
                if($object['u_id']){

                  //Прописываем однократный токен, если он не назначен еще
                  if(!$object['u_authtoken']){
                    $authtoken = md5($object['u_email'].$object['u_id'].rand(1000, 9999));
                    goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $object[u_id]");
                  } else {
                    $authtoken = $object['u_authtoken'];
                  }

                  //Если указан E-mail - отправляем уведомление
                  if($object['u_email']){
                    //Письмо
                    $email = Array();
                    $email['to'] = $object['u_email'];
                    $email['subject'] = 'Статус объекта «ЗАПРОС ПО ПАРАМЕТРАМ» продлен';
                    $email['body'] = '
                    <h2>Уважаемый владелец!</h2>
                    <p>Статус вашего объекта «ЗАПРОС ПО ПАРАМЕТРАМ» по адресу: г. '.$object['obj_addr_city'].', '.$object['obj_addr_street'].' '.$object['obj_addr_number'].' <strong>продлен на 30 суток</strong>.</p>
                    <p>С вашего баланса списано <strong>'.$paramsorder_price.' рублей.</strong></p>
                    <p>Деактивация функции доступна в вашем Личном Кабинете.</p>
                    <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/paramsorder_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
                  }
                  //Если не указан E-mail - оптравляем оповещение
                  else {
                    //Письмо
                    $email = Array();
                    $email['to'] = 'info@goyug.com';
                    $email['subject'] = 'Продление функции ЗАПРОС ПО ПАРАМЕТРАМ - E-mail владельца не указан';
                    $email['body'] = '
                    <h2>Внимание!</h2>
                    <p>Владельцу с ID '.$object['u_id'].' не отправлено уведомление о продлении функции ЗАПРОС ПО ПАРАМЕТРАМ, т.к. у него не указан E-mail.</p>';
                  }
                }
                //Пользователь не найден
                else {

                  //Письмо
                  $email = Array();
                  $email['to'] = 'info@goyug.com';
                  $email['subject'] = 'Продление функции ЗАПРОС ПО ПАРАМЕТРАМ - владелец не найден';
                  $email['body'] = '
                  <h2>Ахтунг!</h2>
                  <p>Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

                  //Лог
                  $errors[] = 'Продление функции объекта ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
                }

                //Отправка
                myMail($email['to'], $email['subject'], $email['body']);

              /////////////////////////
              ///Send E-mail end///////
              /////////////////////////

              $log[] = 'На объекте продлена услуга ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'];
            }
            else {
              $errors[] = 'Ошибка продления функции ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'].'<br />'.mysql_error();
            }

          ////////////////////////////////////
          //Все норм. продлеваем - окончание//
          ////////////////////////////////////

        } else {
          $errors[] = 'Ошибка продления функции ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].'. Не хватает бабла для продления функции. Проверьте на всякий случай!';
          $objects_to_deactivate_with_notification[] = $object;
        }
      } else {
        $errors[] = 'Ошибка продления функции ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].' не найден. Срочно все проверьте!';
      }
    }//foreach
  /////////////////////////
  //Продление - окончание//
  /////////////////////////

  /////////////////////////////
  //Деактивация с оповещением//
  /////////////////////////////
    foreach($objects_to_deactivate_with_notification as $object){

      if(!goToTable("UPDATE obj_object SET obj_paramsorder = 0 WHERE obj_id = $object[obj_id]")){
        $errors[] = 'Ошибка деактивации с уведомлением функции ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'].'<br />'.mysql_error();
      }
      else {

        /////////////////////////
        ///Send E-mail///////////
        /////////////////////////

          //Пользователь найден
          if($object['u_id']){

            //Прописываем однократный токен, если он не назначен еще
            if(!$object['u_authtoken']){
              $authtoken = md5($object['u_email'].$object['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $object[u_id]");
            } else {
              $authtoken = $object['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($object['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $object['u_email'];
              $email['subject'] = 'Статус объекта «ЗАПРОС ПО ПАРАМЕТРАМ» деактивирован';
              $email['body'] = '
              <h2>Уважаемый владелец!</h2>
              <p>Статус вашего объекта «ЗАПРОС ПО ПАРАМЕТРАМ» по адресу: г. '.$object['obj_addr_city'].', '.$object['obj_addr_street'].' '.$object['obj_addr_number'].' <strong>деактивирован в связи с недостаточным балансом вашего счета для продления функции</strong>.</p>
              <h3>Повторная активация функции</h3>
              <p>Для того, чтобы повторно активировать функцию «ЗАПРОС ПО ПАРАМЕТРАМ», необходимо зайти в ваш Личный Кабинет, пополнить баланс, а затем перейти по ссылке «Включить «ЗАПРОС ПО ПАРАМЕТРАМ» в блоке управления объектом.</p>
              <p>Стоимость функции «ЗАПРОС ПО ПАРАМЕТРАМ» '.$paramsorder_price.' рублей <em>за 30 суток</em>.</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/paramsorder_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Деактивация с уведомлением функции ЗАПРОС ПО ПАРАМЕТРАМ - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$object['u_id'].' не отправлено уведомление о деактивации с уведомлением функции ЗАПРОС ПО ПАРАМЕТРАМ, т.к. у него не указан E-mail.</p>';

              //Лог
              $errors[] = 'Владельцу с ID '.$object['u_id'].' не отправлено уведомление о деактивации с уведомлением функции ЗАПРОС ПО ПАРАМЕТРАМ, т.к. у него не указан E-mail.';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Деактивация с уведомлением функции ЗАПРОС ПО ПАРАМЕТРАМ - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Деактивация функции с уведомлением объекта ЗАПРОС ПО ПАРАМЕТРАМ. Владелец с ID '.$object['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
        /////////////////////////
        ///Send E-mail end///////
        /////////////////////////

        $log[] = 'На объекте деактивирована с уведомлением услуга ЗАПРОС ПО ПАРАМЕТРАМ, obj_id: '.$object['obj_id'];
      }
    }//foreach
  /////////////////////////////////////////
  //Деактивация с оповещением - окончание//
  /////////////////////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");


  ////Оповещение каждого выполнения
  //Сериализация лога
  echo "<h2>Скрипт деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ объектов</h2>\n\r";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>'."\n\r";
  }

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт деактивации функции ЗАПРОС ПО ПАРАМЕТРАМ объектов</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения ЗАПРОС ПО ПАРАМЕТРАМ', $bodymail);
  }
?>
