<?php

/**
* Базовый класс
*/

//PHPMailer - библиотека отправки письма
require 'vendor/PHPMailer/PHPMailerAutoload.php';

//Работа с базой данных
include('class/DataBase.class.php');

class Base {

	private $tag = 'model Base';
	private $pathToLog = 'log';

	function __construct() {

		$tag = '__construct';

		$this->log($tag, 'MailAutomate started');

	}

	//Лог
	public function log($tag, $message) {

		if(!file_exists($this->pathToLog)) mkdir($this->pathToLog);

		//Если файл более 1Мб - создаем новый, текущий в архив
		if(file_exists($this->pathToLog.'/log.txt')){
			$filesize = filesize($this->pathToLog.'/log.txt');

			if($filesize >= 1024 * 1024){
				rename($this->pathToLog.'/log.txt', $this->pathToLog.'/log_'.date("Y_m_d_H_i_s").'.txt');
			}
		}

		$fHandler = fopen($this->pathToLog.'/log.txt', "a");

		if(strpos($message, 'MailAutomate started') !== false){
			fwrite($fHandler, "\t"."\t"."\n\r");
		}
		fwrite($fHandler, $this->uDate("Y-m-d H:i:s:u")."\t".$tag."\t".$message."\n\r");

		fclose($fHandler);
	}

//****************************************************************************************************
//
//          Отправка письма - Автоматизация - ZEND MAIL version (by marselos) 1.07.2017
//
//****************************************************************************************************
public function sendMail($to, $subject, $body) {

    $sender = "GOYUG.com | Квартиры, апартаменты, дома посуточно";
    $from = "info@goyug.com";
    $replyto = "info@goyug.com";
    $tag = 'sendMail';

    //Случай нескольких E-mail
    $to_a = explode(',', $to);

    require_once '/usr/share/php/Zend/Mail.php';
    require_once '/usr/share/php/Zend/Mail/Transport/Sendmail.php';

    $transport = new Zend_Mail_Transport_Sendmail();
    $mail = new Zend_Mail('UTF-8');

    $mail->setBodyHTML($body);
    $mail->setFrom($from, $sender);
    $mail->setReplyTo($replyto, '');
    
    for($i = 0; $i < count($to_a); $i++){
        $mail->addTo($to_a[$i], '');
    }

    $mail->setSubject($subject);
    // $mail->send($transport);  
        
    if(!$mail->send($transport))
    {
        $this->log($this->tag, 'Ошибка отправки письма из cron. PHPMailer Error: '.$mail->ErrorInfo);
        // mail('marselos@gmail.com', 'MA Error', 'Ошибка отправки письма из cron. PHPMailer Error: '.$mail->ErrorInfo);
        return false;
    } else {
        return true;
    }

// $mail = new PHPMailer;

// $mail->isSendmail();
// $mail->CharSet = 'utf-8';

// $mail->From = 'info@goyug.com';
// $mail->FromName = 'Сайт goyug.com';
// for($i = 0; $i < count($to_a); $i++){
  // $mail->addAddress($to_a[$i]);
// }
// $mail->addReplyTo('info@goyug.com');
// $mail->Sender = 'info@goyug.com';

// $mail->isHTML(true);
// $mail->Subject = $subject;
// $mail->Body = $body;

}
//****************************************************************************************************

	//Отправка СМС
	public function sendSMS($template_name_tech, $user) {

		$tag = 'sendSMS';

        /////////////////////
        //Даные mainsms//////
        /////////////////////
        //Логин СМС-дисконт
        $mainsms_project = 'goyug';
        //API Key
        $mainsms_key = '76e238b2b1f1a';
        //Номер отправки
        $mainsms_recipients = $user['phone'];
        //Отправитель
        $mainsms_sender = 'GOYUG.COM';
        //Подпись
        $mainsms_sign = '';
        //Тестовая отправка
        $mainsms_test = 0;
        //Текст сообщения
        $mainsms_text = ''; //будет указан позже
        /////////////////////////////////
        //Даные mainsms - окончание//////
        /////////////////////////////////

        /////////////////////////////
        //Шаблоны смс-сообщений//////
        /////////////////////////////
        $templates = Array();
        $templates['awake_fail'] = 'Ваши бъекты отключены. Для включения, войдите в свой Личный Кабинет.';
        /////////////////////////////////////////
        //Шаблоны смс-сообщений - окончание//////
        /////////////////////////////////////////

        //Если не задан телефон отправки или шаблон - ничего не делаем
        if( !$mainsms_recipients || !isset($templates[$template_name_tech]) ){
        	$this->log($this->tag, 'Телефон или шаблон не указан: '.$mainsms_recipients.', '.$template_name_tech);

        	return false;
        } else {
            //Текст смс
            $mainsms_text = $templates[$template_name_tech];

            //Режим отправки Fake / Live
            $mainsms_mod = (strpos($_SERVER['HOSTNAME'], 'goyug.com') !== false) ? 'live' : 'fake';

            //Live
            if($mainsms_mod == 'live'){
                //Подписание
                $mainsms_params = Array($mainsms_project, $mainsms_sender, $mainsms_text, $mainsms_recipients, $mainsms_test);
                ksort($mainsms_params);
                $mainsms_params[] = $mainsms_key;
                $mainsms_sign = md5(sha1(implode(';', $mainsms_params)));

                //Адрес API
                $ch = curl_init('http://mainsms.ru/api/mainsms/message/send');

                //Установка адреса
                curl_setopt($ch, CURLOPT_POST, 1);

                //Запрос
                curl_setopt($ch, CURLOPT_POSTFIELDS, "project=$mainsms_project&sender=$mainsms_sender&message=$mainsms_text&recipients=$mainsms_recipients&sign=$mainsms_sign&test=$mainsms_test");

                //Отключение вывода в браузер + возврат в переменную
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                //Выполняем запрос curl
                $result = curl_exec($ch);

                //Разбор ответа
                if( !isset($result) || $result == '' ){
                    $this->log($this->tag, 'Ошибка отправки СМС: '.$mainsms_recipients.', с текстом: '.$mainsms_text);
                    return false;
                } else {
                    $result = json_decode($result, true);
                    $mainsms_uid = implode(',', $result['messages_id']);
                    $this->log($this->tag, 'Реальная отправка СМС-сообщения на номер: '.$mainsms_recipients.', с текстом: '.$mainsms_text.', код смс: '.$mainsms_uid);
                    return true;
                }

                //Закрываем соединение
                curl_close($ch);
            } else {
            	$this->log($this->tag, 'Виртуальная отправка СМС-сообщения на номер: '.$mainsms_recipients.', с текстом: '.$mainsms_text);
            	return true;
            }
        }
	}

	//Время с миллисекундами
	private function uDate($format, $utimestamp = null) {
		if (is_null($utimestamp))
			$utimestamp = microtime(true);

		$timestamp = floor($utimestamp);
		$milliseconds = round(($utimestamp - $timestamp) * 1000);

		return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
	}
}
?>