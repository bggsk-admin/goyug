<?php

/**
* Шаблоны писем.
* Здесь хранятся шаблоны писем.
*/
class Template extends Base {

	private $template = Array();
	private $styles = Array();
	private $strings = Array();

	function __construct() {

		$tag = '__construct';

		$this->log($tag, 'model Template init start...');

		$this->template['name'] = '';
		$this->template['subject'] = '';
		$this->template['body'] = '';

		$this->styles['btn_danger'] = 'style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;"';
		$this->styles['text_muted'] = 'style="color: gray;"';

		$this->strings['sitename'] = 'Goyug.com';
		$this->strings['siteurl'] = 'goyug.com';
		$this->strings['sign_in'] = 'Войти в личный кабинет GOYUG.COM';
		$this->strings['signature'] = '---<br />С Уважением,<br /> администрация сервиса<br />';
		$this->strings['unsubscribe'] = 'Если письмо попало к вам по ошибке, или вы не хотите получать уведомления о новых функциях сайта, просто пришлите ответ с текстом "Отписать" и мы уберем ваш E-mail из рассылки.';

		$this->log($tag, 'model Template init end');
	}

	public function getTemplate($template_name_tech, $user = '') {

		if(!is_array($user)){
			$user = Array();
			$user['authtoken'] = 'expired';
		}

		switch ($template_name_tech) {

			case 'activate':
				$this->template['name'] = 'Активация';
				$this->template['subject'] = 'Доступ в личный кабинет';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>Вас приветствует сервис посуточной аренды жилья <strong>'.$this->strings['sitename'].'</strong>.</p>
				<p>Предлагаем Вашему вниманию ресурс, на котором можно разместить информацию об объектах, сдаваемых в аренду. На данный момент размещение и добавление всех объектов осуществляется <strong>бесплатно</strong>.</p>
				<p>Часть Ваших объектов уже внесена в нашу базу, и они стали доступны для посетителей сайта. Поэтому просим Вас в самое ближайшее время зайти и отредактировать данные по Вашим объектам (цены, фото, параметры и т.д), а также добавить новые.</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/activate/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<!--<p>Ссылка на сайт: <a href="http://'.$this->strings['siteurl'].'">'.$this->strings['siteurl'].'</a></p>-->
				<p>Будем надеяться на долгое и плодотворное сотрудничество с Вами!</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>';
				break;

			case 'hello':
				$this->template['name'] = 'Приветствие';
				$this->template['subject'] = 'Регистрация пользователя на портале '.$this->strings['sitename'];
				$this->template['body'] = '
				<h2>Поздравляем!</h2>
				<p>Вы зарегистрированы в сервисе посуточной аренды <strong>'.$this->strings['sitename'].'</strong>.</p>
				<p>Теперь Вы можете:</p>
				<ul>
					<li>размещать информацию о своих объектах в неограниченном количестве</li>
					<li>вести календарь занятости объектов;</li>
					<li>получать заявки и звонки от посетителей портала.</li>
				</ul>
				<p>Будем надеяться на долгое и плодотворное сотрудничество с Вами!</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>';
				break;

			case 'isempty':
				$this->template['name'] = 'СВОБОДНА СЕГОДНЯ';
				$this->template['subject'] = 'Функция "Квартира СВОБОДНА СЕГОДНЯ" доступна в вашем Личном Кабинете';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>У нас появилась новая функция для срочной сдачи жилья - "СВОБОДНА СЕГОДНЯ". Для использования этой функции перейдите по ссылке "Активировать «Свободна сегодня»" напротив вашего объекта в личном кабинете и Ваш объект будет выделен в списке зеленым значком "Свободна сегодня".</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/isempty/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'sale':
				$this->template['name'] = 'СКИДКИ';
				$this->template['subject'] = 'Функция "СКИДКИ" доступна в вашем Личном Кабинете';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>У нас появилась новая функция - "СКИДКИ". Для использования этой функции нажмите кнопку "Активировать «Скидку»" напротив вашего объекта в личном кабинете и Ваш объект будет помечен специальным знаком "Скидки".</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/sale/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'photomark':
				$this->template['name'] = 'ВЫДЕЛЕНИЕ В СПИСКЕ';
				$this->template['subject'] = 'Функция "ВЫДЕЛЕНИЕ В СПИСКЕ" доступна в вашем Личном Кабинете';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>У нас появилась новая <strong>бесплатная</strong> функция - "ВЫДЕЛЕНИЕ В СПИСКЕ". Для использования этой функции нажмите кнопку "Выделить объект" напротив вашего объекта в личном кабинете и Ваш объект будет выделен среди остальных объявлений синим прямоугольником.</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/photomark/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'awake1':
				$this->template['name'] = 'Пробуждение - письмо 1';
				$this->template['subject'] = 'Ваши об-ты могут быть удалены через 2 недели';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>Вас приветствует сервис посуточной аренды жилья '.$this->strings['sitename'].'!</p>
				<p>Вы не заходили в свой личный кабинет более 1-го месяца. В этом случае, ваши объекты могут быть автоматически удалены.</p>
				<p>Чтобы этого не произошло, войдите в свой личный кабинет по ссылке:</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/awake1/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>Если эти объекты Вам больше не нужны, просто проигнорируйте это сообщение. Объекты будут автоматически удалены через 2 недели.</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'awake2':
				$this->template['name'] = 'Пробуждение - письмо 2';
				$this->template['subject'] = 'Ваши об-ты могут быть удалены через неделю';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>Вас приветствует сервис посуточной аренды жилья '.$this->strings['sitename'].'!</p>
				<p>Вы не заходили в свой личный кабинет более 1-го месяца. В этом случае, ваши объекты могут быть автоматически удалены.</p>
				<p>Чтобы этого не произошло, войдите в свой личный кабинет по ссылке:</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/awake2/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>Если эти объекты Вам больше не нужны, просто проигнорируйте это сообщение. Объекты будут автоматически удалены через 1 неделю.</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'awake3':
				$this->template['name'] = 'Пробуждение - письмо 3';
				$this->template['subject'] = 'Ваши об-ты могут быть удалены через 3 дня';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>Вас приветствует сервис посуточной аренды жилья '.$this->strings['sitename'].'!</p>
				<p>Вы не заходили в свой личный кабинет более 1-го месяца. В этом случае, ваши объекты могут быть автоматически удалены.</p>
				<p>Чтобы этого не произошло, войдите в свой личный кабинет по ссылке:</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/awake3/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>Если эти объекты Вам больше не нужны, просто проигнорируйте это сообщение. Объекты будут автоматически удалены через 3 дня.</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			case 'objects_disabled':
				$this->template['name'] = 'Объекты выключены';
				$this->template['subject'] = 'Ваши объекты были отключены';
				$this->template['body'] = '
				<h2>Здравствуйте!</h2>
				<p>Вас приветствует сервис посуточной аренды жилья '.$this->strings['sitename'].'!</p>
				<p>Вы не заходили в свой личный кабинет более 1-го месяца и не отреагировали ни на одно письмо об отключении. Теперь Ваши объекты отключены.</p>
				<p>Для того, чтобы снова активировать Ваши объекты, зайдите в личный кабинет по ссылке:</p>
				<p>
					<a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/autoemail/src_mod/objects_disabled/" '.$this->styles['btn_danger'].'>'.$this->strings['sign_in'].'</a>
				</p>
				<p>Если эти объекты Вам больше не нужны, просто проигнорируйте это сообщение.</p>
				<p>'.$this->strings['signature'].' '.$this->strings['sitename'].'</p>
				<hr />
				<p '.$this->styles['text_muted'].'>'.$this->strings['unsubscribe'].'</p>';
				break;

			default:
				# code...
				break;
		}

		//Теги HTML
		$this->template['body'] = '
        <!DOCTYPE html>
        <html lang="ru">
        <head>
            <meta charset="UTF-8">
            <title>'.$this->template['subject'].'</title>
        </head>
        <body>'
        .$this->template['body'].
        '</body>
        </html>';

		return $this->template;
	}
}

?>