<?php

class DataBase {

    public static $mConnect;	// Хранит результат соединения с базой данных
    public static $mSelectDB;	// Хранит результат выбора базы данных

    //Данные БД
    private static $dbhost;
    private static $dbuser;
    private static $dbpassword;
    private static $dbname;

    //Метод создает соединение с базой данных
    public static function connect(){

        if(!isset($_SERVER['HTTP_HOST'])) $_SERVER['HTTP_HOST'] = 'goyug.com';
        if(!isset($_SERVER['HOSTNAME'])) $_SERVER['HOSTNAME'] = 'goyug.com';

        self::$dbhost = strpos($_SERVER['HTTP_HOST'], 'goyug-cron') !== false ? "localhost" : "localhost";
        self::$dbuser = strpos($_SERVER['HTTP_HOST'], 'goyug-cron') !== false ? "root" : "goyugdev";
        self::$dbpassword = strpos($_SERVER['HTTP_HOST'], 'goyug-cron') !== false ? "" : "shryb1ciz9dan";
        self::$dbname = strpos($_SERVER['HTTP_HOST'], 'goyug-cron') !== false ? "goyug" : "goyug";

        // Пробуем создать соединение с базой данных
        self::$mConnect = mysql_connect(self::$dbhost, self::$dbuser, self::$dbpassword);

        // Если подключение не прошло, вывести сообщение об ошибке..
        if(!self::$mConnect){
            echo "<p>К сожалению, не удалось подключиться к серверу MySQL</p>";
            exit();
            return false;
        }

        // Пробуем выбрать базу данных
        self::$mSelectDB = mysql_select_db(self::$dbname, self::$mConnect);

        // Если база данных не выбрана, вывести сообщение об ошибке..
        if(!self::$mSelectDB){

            echo "<p>".mysql_error()."</p>";
            exit();
            return false;
        }

        mysql_query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");
        mysql_query("SET CHARACTER SET 'utf8'");

        // Возвращаем результат
        return self::$mConnect;
    }

    // Метод закрывает соединение с базой данных
    public static function close(){
        // Возвращает результат
        return mysql_close(self::$mConnect);
    }

    //Коллекция
    public static function getRows($query){

        self::connect();

        $result = mysql_query($query);

        if($result){
            if(mysql_num_rows($result)){
                $output = Array();
                while($rows = mysql_fetch_assoc($result)){
                    $output[] = $rows;
                }
                return $output;
            } else {
                return Array();
            }
        } else {
            return false;
        }

        self::close();
    }

    //Строка
    public static function getRow($query){

        self::connect();

        $result = mysql_query($query);

        if($result){
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            } else {
                return false;
            }
        } else {
            return false;
        }

        self::close();
    }

    //Ассоциативная запись
    function insert($table_name, $row){

        //Запрос
        $query = "INSERT INTO $table_name (";
            //Промежуточный массив параметров
            foreach($row as $key => $val){
            $query_temp_key[] = "`$key`";
            $query_temp_val[] = "'$val'";
        }
        //Преобразование списка ключей в строку
        $query .= implode(",", $query_temp_key).") VALUES (";
        //Преобразование списка значений в строку
        $query .= implode(",", $query_temp_val).")";

        return self::goToTable($query);
    }

    //Произвольная запись
    public static function goToTable($query){

        self::connect();

        if(mysql_query($query)) return true;
        else                    return false;

        self::close();
    }
}
?>