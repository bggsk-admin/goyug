<?php

/**
* Действия.
* Здесь назначаются письма на отправку.
*/
class Action extends Base {

	private $tag = 'model Action';

	private $deltaOneDay;
	private $deltaTenMinutes;
	private $deltaFiveMinutes;

	private $sendEmailCountLimit = 20;
	private $sendEmailTimeout = 500000;

	function __construct() {

		$tag = '__construct';

		$this->log($tag, 'model Action init start...');

		$this->deltaOneDay = 60 * 60 * 24;
		$this->deltaTenMinutes = 60 * 10;
		$this->deltaFiveMinutes = 60 * 5;

		$this->log($tag, 'model Action init end');
	}

	//Обновление статусов и формирование писем для отправки
	public function update() {

		// Города с АБОНПЛАТОЙ
		$rental_cities = array(
			"kislovodsk", 
			"nnovgorod", 
			"sochi", "sevastopol", "spb", 
			"krasnodar", "saratov", "tyumen", 
			"novosibirsk", "ekaterinburg", "perm", 
			"kazan", "ufa", "voronezh", 
			"moscow", "omsk", "rostovnadonu", "volgograd",
	    "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
	    "zheleznovodsk",
	    "pyatigorsk", "yalta", "yaroslavl", "tomsk",
    	"bryansk", "tula", "magnitogorsk", "ivanovo",
    	"khabarovsk", "vladimir", "penza", "kirov", "vologda",
    	"surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
      "astrakhan", "tolyatty", "orel", "orenburg", "tver",
      "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
      
		);

		$rental_cities_query = implode("','", $rental_cities);


		$this->log($this->tag, 'updateMethod start...');

		//////////////////////
		//Новые пользователи//
		//////////////////////
		$this->log($this->tag, 'updateMethod new users start...');

		//Только что зарегистрировавшиеся пользователи
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE u_isnew";

		$users = DataBase::getRows($query);

		//Задаем цепочку писем для новых пользователей
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем письмо приветствия
			$this->setEmail('hello', $user, date("Y-m-d H:i:s", $currentTimestamp));
			$this->log($this->tag, 'updateMethod new users - set hello im to '.$user['id']);

			//Назначаем письмо функция СВОБОДНА СЕГОДНЯ
			$this->setEmail('isempty', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay));
			$this->log($this->tag, 'updateMethod new users - set isempty dl to '.$user['id']);

			//Назначаем письмо функция РАСПРОДАЖА
			$this->setEmail('sale', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			$this->log($this->tag, 'updateMethod new users - set sale dl to '.$user['id']);

			//Назначаем письмо функция ВЫДЕЛИТЬ ОБЪЯВЛЕНИЕ
			//$this->setEmail('photomark', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			//$this->log($this->tag, 'updateMethod new users - set photomark dl to '.$user['id']);

			//Снимаем метку нового пользователя
			$setIsNewToFalse = $this->setIsNewToFalse($user);
			if($setIsNewToFalse){
				$this->log($this->tag, 'updateMethod new users - set isnew to FALSE success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod new users - set isnew to FALSE error: '.$user['id'].' ('.$setIsNewToFalse.')');
			}
		}

		$this->log($this->tag, 'updateMethod new users end');
		//////////////////////////////////
		//Новые пользователи - окончание//
		//////////////////////////////////


		///////////////
		//Пробуждение//
		///////////////
		$this->log($this->tag, 'updateMethod awake users start...');


		##############################################################
		//Шаг 1
		$this->log($this->tag, 'updateMethod awake users awake1 start...');

		//Пользователи не заходили в ЛК более 30 дней и еще не получили Письмо №1
		//или пользователи, которые уже пробудились, но опять не заходили в ЛК более 30 дней
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email, obj.obj_addr_city_uid
				  FROM u_user as u
				  LEFT JOIN obj_object as obj ON obj.obj_u_id = u.u_id
				  WHERE
				   	obj.obj_addr_city_uid NOT IN ('" . $rental_cities_query. "') AND
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_lastlogin <= '".date("Y-m-d H:i:s", mktime() - $this->deltaOneDay*30)."' AND
				  	(
				  		u.u_next_email_task = '' OR
				  		u.u_next_email_task = 'awake_success' OR
				  		u.u_next_email_task = 'awake_success_sms' OR
				  		u.u_next_email_task = 'awake_end' OR
				  		u.u_next_email_task = 'activate_awake_end'
				  	)";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №1 и ставим метку на отправку Письма №2
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №1 пробуждения
			$setEmail = $this->setEmail('awake1', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod awake users - set awake1 im sucess '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake1 im error '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №2
			$setNextEmailTask = $this->setNextEmailTask('awake2', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*7));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod awake users - set awake2 dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake2 dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 1 - окончание
		$this->log($this->tag, 'updateMethod awake users awake1 end');
		##############################################################


		##############################################################
		//Шаг 2
		$this->log($this->tag, 'updateMethod awake users awake2 start...');

		//Пользователи не заходили в ЛК более 30 дней и не проснулись после Письма №1
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'awake2' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №2 и ставим метку на отправку Письма №3
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №2 пробуждения
			$setEmail = $this->setEmail('awake2', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod awake users - set awake2 im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake2 im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №3
			$setNextEmailTask = $this->setNextEmailTask('awake3', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod awake users - set awake3 dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake3 dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 2 - окончание
		$this->log($this->tag, 'updateMethod awake users awake2 end');
		##############################################################


		##############################################################
		//Шаг 3
		$this->log($this->tag, 'updateMethod awake users awake3 start...');

		//Пользователи не заходили в ЛК более 30 дней и не проснулись после Письма №2
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'awake3' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №3 и ставим метку на отправку Письма №4
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №3 пробуждения
			$setEmail = $this->setEmail('awake3', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod awake users - set awake3 im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake3 im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №4
			$setNextEmailTask = $this->setNextEmailTask('objects_disabled', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*3));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod awake users - set objects_disabled dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set objects_disabled dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 3 - окончание
		$this->log($this->tag, 'updateMethod awake users awake3 end');
		##############################################################


		##############################################################
		//Шаг 4
		$this->log($this->tag, 'updateMethod awake users objects_disabled start...');

		//Пользователи не заходили в ЛК более 30 дней и не проснулись после всех предупредительных писем
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'objects_disabled' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №4 и ставим метку окончание попытки пробуждения
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №4 пробуждения
			$setEmail = $this->setEmail('objects_disabled', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod awake users - set objects_disabled im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set objects_disabled im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Отключаем объекты владельца
			$disableObjects = $this->disableObjects($user);
			if($disableObjects){
				$this->log($this->tag, 'updateMethod awake users - do objects_disabled im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - do objects_disabled im error: '.$user['id'].' ('.$disableObjects.')');
			}

			//Ставим метку окончания пробуждения
			$setNextEmailTask = $this->setNextEmailTask('awake_fail', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod awake users - set awake_fail pm success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - set awake_fail pm error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 4 - окончание
		$this->log($this->tag, 'updateMethod awake users objects_disabled end');
		##############################################################


		##############################################################
		//Шаг 5
		$this->log($this->tag, 'updateMethod awake users awake_fail start...');

		//Пользователи не заходили в ЛК более 30 дней и не проснулись после всех предупредительных писем
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_phone_1 as phone, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_phone_1 != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'awake_fail' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем СМС и ставим метку отправки СМС
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Отправляем СМС
			// Отключено 15.02.2017 by marselos
			// $sendSMS = $this->sendSMS('awake_fail', $user);
			// if($sendSMS){
			// 	$this->log($this->tag, 'updateMethod awake users - send sms awake_fail im success '.$user['id']);
			// } else {
			// 	$this->log($this->tag, 'updateMethod awake users - send sms awake_fail im error: '.$user['id'].' ('.$sendSMS.')');
			// }

			//Ставим метку отправленной смс
			$setNextEmailTask = $this->setNextEmailTask('awake_fail_sms', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod awake users - send sms awake_fail_sms pm success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod awake users - send sms awake_fail_sms pm error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 5 - окончание
		$this->log($this->tag, 'updateMethod awake users awake_fail end');
		##############################################################


		$this->log($this->tag, 'updateMethod awake users end');
		///////////////////////////
		//Пробуждение - окончание//
		///////////////////////////


		/////////////
		//Активация//
		/////////////
		$this->log($this->tag, 'updateMethod activate_awake users start...');


		##############################################################
		//Удачная активация - переназначение на рассылку возможностей сайта
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake_success start...');

		//Вошедшие по активации пользователи
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  (
				  	u.u_next_email_task = 'activate_awake_success'
				  )";

		$users = DataBase::getRows($query);

		//Задаем цепочку писем для вошедших по активации пользователей
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем письмо приветствия
			$this->setEmail('hello', $user, date("Y-m-d H:i:s", $currentTimestamp));
			$this->log($this->tag, 'updateMethod activate_awake_success - set hello im to '.$user['id']);

			//Назначаем письмо функция СВОБОДНА СЕГОДНЯ
			$this->setEmail('isempty', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay));
			$this->log($this->tag, 'updateMethod activate_awake_success - set isempty dl to '.$user['id']);

			//Назначаем письмо функция РАСПРОДАЖА
			$this->setEmail('sale', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			$this->log($this->tag, 'updateMethod activate_awake_success - set sale dl to '.$user['id']);

			//Назначаем письмо функция ВЫДЕЛИТЬ ОБЪЯВЛЕНИЕ
			//$this->setEmail('photomark', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			//$this->log($this->tag, 'updateMethod activate_awake_success - set photomark dl to '.$user['id']);

			//Ставим метку успешной активации нового пользователя
			$setActivateAwakeSuccess = $this->setActivateAwakeSuccess($user);
			if($setActivateAwakeSuccess){
				$this->log($this->tag, 'updateMethod activate_awake_success - set u_next_email_task to activate_awake_end success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake_success - set u_next_email_task to activate_awake_end error: '.$user['id'].' ('.$setActivateAwakeSuccess.')');
			}
		}

		//Удачная активация - переназначение на рассылку возможностей сайта - окончание
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake_success end');
		##############################################################


		##############################################################
		//Шаг 1
		$this->log($this->tag, 'updateMethod activate_awake users activate start...');

		//Письмо-приглашение в сервис для тех, кого мы сами добавили
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_creater IN ('lvika', 'administrator', 'content01', 'content02', 'parser_spiti', 'parser_avito') AND
				  	u.u_lastlogin IS NULL AND
				  	(
				  		u.u_next_email_task = ''
				  	)";

		$users = DataBase::getRows($query);

		//Отправляем Письмо-приглашение и ставим метку на отправку Письма №1 пробуждения
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо-приглашение
			$setEmail = $this->setEmail('activate', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate im sucess '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate im error '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №1
			$setNextEmailTask = $this->setNextEmailTask('activate_awake1', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*3));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake1 dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake1 dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 1 - окончание
		$this->log($this->tag, 'updateMethod activate_awake users activate end');
		##############################################################


		##############################################################
		//Шаг 2
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake1 start...');

		//Письмо №1 - активация-пробуждение
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'activate_awake1' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №1 и ставим метку на отправку Письма №2
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №1 активации-пробуждения
			$setEmail = $this->setEmail('awake1', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake1 im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake1 im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №2
			$setNextEmailTask = $this->setNextEmailTask('activate_awake2', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*7));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake2 dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake2 dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 2 - окончание
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake1 end');
		##############################################################


		##############################################################
		//Шаг 3
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake2 start...');

		//Письмо №2 - активация-пробуждение
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'activate_awake2' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №2 и ставим метку на отправку Письма №3
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №2 активации-пробуждения
			$setEmail = $this->setEmail('awake2', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake2 im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake2 im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку отправки Письма №3
			$setNextEmailTask = $this->setNextEmailTask('activate_awake3', $user, date("Y-m-d H:i:s", $currentTimestamp + $this->deltaOneDay*4));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake3 dl success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake3 dl error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 3 - окончание
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake2 end');
		##############################################################


		##############################################################
		//Шаг 4
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake3 start...');

		//Письмо №3 - активация-пробуждение
		$query = "SELECT u.u_id as id, u.u_authtoken as authtoken, u.u_firstname as name, u.u_email as email
				  FROM u_user as u
				  WHERE
				  	u.u_email != '' AND
				  	u.u_role = 'owner' AND
				  	u.u_next_email_task = 'activate_awake3' AND
				  	u.u_next_email_date <= '".date("Y-m-d H:i:s")."'";

		$users = DataBase::getRows($query);

		//Отправляем Письмо №3 и останавливаем рассылку
		foreach ($users as $user_key => $user) {

			//Текущий штамп времени
			$currentTimestamp = mktime();

			//Назначаем токен
			if($user['authtoken'] == ''){
				$user['authtoken'] = $this->generateAuthToken($user);
			}

			//Назначаем Письмо №3 активации-пробуждения
			$setEmail = $this->setEmail('awake3', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setEmail){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake3 im success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake3 im error: '.$user['id'].' ('.$setEmail.')');
			}

			//Ставим метку окончания активации
			$setNextEmailTask = $this->setNextEmailTask('activate_awake_fail', $user, date("Y-m-d H:i:s", $currentTimestamp));
			if($setNextEmailTask){
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake_fail pm success '.$user['id']);
			} else {
				$this->log($this->tag, 'updateMethod activate_awake users - set activate_awake_fail pm error: '.$user['id'].' ('.$setNextEmailTask.')');
			}
		}

		//Шаг 4 - окончание
		$this->log($this->tag, 'updateMethod activate_awake users activate_awake3 end');
		##############################################################


		$this->log($this->tag, 'updateMethod activate_awake users end');

		/////////////////////////
		//Активация - окончание//
		/////////////////////////

		$this->log($this->tag, 'updateMethod end');
	}

	//Отправка писем
	public function start() {

		$this->log($this->tag, 'startMethod start...');

		$date_current = date("Y-m-d H:i:s");

		//Собираем письма на отправку
		$query = "SELECT * FROM mail_automate_tasks WHERE !exec AND date_send <= '$date_current' LIMIT ".$this->sendEmailCountLimit;
		$mats = DataBase::getRows($query);

		//Отправляем письма
		foreach ($mats as $mat_key => $mat) {
			$result = $this->sendMail($mat['to'], $mat['subject'], $mat['body']);
			if($result){
				$this->setExecToTrue($mat);
				$this->log($this->tag, 'startMethod email sent success '.$mat['to'].' ('.$mat['name'].')');
			} else {
				$this->log($this->tag, 'startMethod email sent error '.$mat['to'].' ('.$mat['name'].')');
			}
			usleep($this->sendEmailTimeout);
		}

		$this->log($this->tag, 'startMethod end');
	}

	//Назначение письма
	private function setEmail($template_name_tech, $user, $datetime) {
		global $Template;

		$template = $Template->getTemplate($template_name_tech, $user);
		$mat = Array(
			'name' => $template['name'],
			'to' => $user['email'],
			'subject' => $template['subject'],
			'body' => $template['body'],
			'date_stage' => date("Y-m-d H:i:s"),
			'date_send' => $datetime
			);

		$result = DataBase::insert('mail_automate_tasks', $mat);

		return $result;
	}

	//Снимаем метку нового пользователя
	private function setIsNewToFalse($user) {
		$query = "UPDATE u_user SET `u_isnew` = 0 WHERE u_id = ".$user['id'];
		return DataBase::goToTable($query);
	}

	//Ставим метку успешной активации нового пользователя
	private function setActivateAwakeSuccess($user) {
		$query = "UPDATE u_user SET `u_next_email_task` = 'activate_awake_end' WHERE u_id = ".$user['id'];
		return DataBase::goToTable($query);
	}

	//Ставим метку следующего письма
	private function setNextEmailTask($template_name_tech, $user, $datetime) {
		$query = "UPDATE u_user SET `u_next_email_task` = '".$template_name_tech."', `u_next_email_date` = '".$datetime."' WHERE u_id = ".$user['id'];
		return DataBase::goToTable($query);
	}

	//Ставим метку успешной отправки письма
	private function setExecToTrue($mat) {
		$query = "UPDATE mail_automate_tasks SET `exec` = 1, `date_exec` = '".date("Y-m-d H:i:s")."' WHERE id = ".$mat['id'];
		return DataBase::goToTable($query);
	}

	//Отключаем объекты владельца
	private function disableObjects($user) {
		$query = "UPDATE obj_object SET `obj_enable` = 0, `obj_updater` = 'ma', `obj_update` = '".date("Y-m-d H:i:s")."' WHERE obj_u_id = ".$user['id'];
		return DataBase::goToTable($query);
	}

	//Генерация токена доступа
	private function generateAuthToken($user) {
		$authtoken = md5($user['email'].$user['id'].rand(1000, 9999));
		$query = "UPDATE u_user SET `u_authtoken` = '".$authtoken."' WHERE u_id = ".$user['id'];
		$result = DataBase::goToTable($query);

		if($result) return $authtoken;
		else 		return '';
	}
}
?>