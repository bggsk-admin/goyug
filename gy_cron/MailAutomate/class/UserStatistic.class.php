<?php

/**
* Генерация статистики.
*/
class UserStatistic extends Base {

	private $tag = 'model UserStatistic';
	private $sendReport = true;
	private $pathToStatFile = 'stat';

	function __construct() {

		$tag = '__construct';

		$this->log($tag, 'model UserStatistic init start...');

		//Buisness logic

		$this->log($tag, 'model UserStatistic init end');
	}

	//Генератор
	public function generate() {

		$this->log($this->tag, 'generateMethod start...');

		//////////////////////////
		//Файл отчета - открытие//
		//////////////////////////
		if(!file_exists($this->pathToStatFile)) mkdir($this->pathToStatFile);
		//Если файл более 1Мб - создаем новый, текущий в архив
		$writeHead = false;
		if(file_exists($this->pathToStatFile.'/stat.csv')){
			$filesize = filesize($this->pathToStatFile.'/stat.csv');
			if($filesize >= 1024 * 1024){
				rename($this->pathToStatFile.'/stat.csv', $this->pathToStatFile.'/stat_'.date("Y_m_d_H_i_s").'.csv');
				$writeHead = true;
			}
		} else {
			$writeHead = true;
		}
		$fHandler = fopen($this->pathToStatFile.'/stat.csv', "a", '"');

		//Заголовок полей - если новый файл
		if($writeHead){
			$arrayHead = Array(
				"Дата",
				"Активные",
				"Плательщики",
				"Проснувшиеся",
				"Не проснувшиеся",
				"В процессе пробуждения",
				"Активированные",
				"Не активированные",
				"В процессе активации",

            	"Сумма платежей",

            	"Кол-во владельцев общее",
            	"Кол-во владельцев самозарегистрированных",

            	"Кол-во объектов общее",
            	"Кол-во объектов включенных",
            	"Кол-во объектов сегодня",

            	"Опция СВОБОДНА СЕГОДНЯ",
            	"Опция ПОДНЯТИЕ",
            	"Опция ВЫДЕЛИТЬ РАМКОЙ",
            	"Опция РАСПРОДАЖА",

            	"Действие клик ПОКАЗАТЬ КОНТАКТЫ",
            	"Действие заполнение формы ПОКАЗАТЬ КОНТАКТЫ"
				);
			fputcsv($fHandler, $arrayHead, ";");
		}

		//Цифры статистики
		$arrayBody = Array();
		$arrayBody[] = date("Y-m-d");



		////////////////////////////////////////////////////////////
		/////////////Выбираем владельцев по статусу/////////////////
		////////////////////////////////////////////////////////////

			#####################################################################################
			#~! Активные (Проявили активность за последние 30 дней)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusActiveUsers start...');

			$query = "SELECT COUNT(*) as count
	  				  FROM u_user
	  				  WHERE `u_lastlogin` > NOW() - INTERVAL 30 DAY";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusActiveUsers end');
			#####################################################################################


			#####################################################################################
			#~! Плательщики (Проявили активность за последние 30 дней да еще и заплатили)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusPaymentActiveUsers start...');

			$query = "SELECT COUNT(DISTINCT cf_u_id) as count
					  FROM cf_cashflow
					  WHERE cf_date > NOW() - INTERVAL 30 DAY AND cf_sum
					  ORDER BY cf_date DESC";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusPaymentActiveUsers end');
			#####################################################################################


			#####################################################################################
			#~! Проснувшиеся (Зашли в ЛК по письмам пробуждения)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusAwakedUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('awake_success', 'awake_success_sms', 'awake_end')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusAwakedUsers end');
			#####################################################################################


			#####################################################################################
			#~! Не проснувшиеся (НЕ зашли в ЛК по письмам пробуждения)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusNoneAwakedUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('awake_fail', 'awake_fail_sms')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusNoneAwakedUsers end');
			#####################################################################################


			#####################################################################################
			#~! В процессе пробуждения (Начали получать письма пробуждения, но еще не зашли в ЛК)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusInAwakeProcessUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('awake2', 'awake3', 'objects_disabled')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusInAwakeProcessUsers end');
			#####################################################################################


			#####################################################################################
			#~! Активированные (Зашли в ЛК по письмам активации)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusActivatedUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('activate_awake_success', 'activate_awake_end')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusActivatedUsers end');
			#####################################################################################


			#####################################################################################
			#~! Не активированные (НЕ зашли в ЛК по письмам активации)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusNoneActivatedUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('activate_awake_fail')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusNoneActivatedUsers end');
			#####################################################################################


			#####################################################################################
			#~! В процессе активации (Начали получать письма активации, но еще не зашли в ЛК)
			#####################################################################################
			$this->log($this->tag, 'generateMethod statusInActivateProcessUsers start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_next_email_task` IN ('activate_awake1', 'activate_awake2', 'activate_awake3')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod statusInActivateProcessUsers end');
			#####################################################################################

		////////////////////////////////////////////////////////////////////////
		/////////////Выбираем владельцев по статусу - окончание/////////////////
		////////////////////////////////////////////////////////////////////////



		/////////////////////////////////////
		/////////////Платежи/////////////////
		/////////////////////////////////////

			#####################################################################################
			#~! Сумма платежей за текущий день
			#####################################################################################
			$this->log($this->tag, 'generateMethod paymentToday start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT SUM(cf_sum) as sum
					  FROM cf_cashflow
					  WHERE
					  	cf_date LIKE '%$date_today%' AND
					  	cf_sum AND
					  	cf_mode IN ('balanceoff')
					  ORDER BY cf_date DESC";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['sum'];

			$this->log($this->tag, 'generateMethod paymentToday end');
			#####################################################################################

		/////////////////////////////////////////////////
		/////////////Платежи - окончание/////////////////
		/////////////////////////////////////////////////



		/////////////////////////////////////////////////
		/////////////Владельцы и объекты/////////////////
		/////////////////////////////////////////////////

			#####################################################################################
			#~! Количество владельцев общее
			#####################################################################################
			$this->log($this->tag, 'generateMethod usersCountGeneral start...');

			$query = "SELECT COUNT(*) as count
	  				  FROM u_user
	  				  WHERE u_active";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod usersCountGeneral end');
			#####################################################################################


			#####################################################################################
			#~! Количество владельцев самозарегистрированных
			#####################################################################################
			$this->log($this->tag, 'generateMethod usersCountSelfRegistred start...');

			$query = "SELECT COUNT(*) as count
					  FROM u_user
					  WHERE `u_creater` NOT IN ('admin', 'lvika', 'content01', 'content02', 'parser_spiti', 'parser_avito')";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod usersCountSelfRegistred end');
			#####################################################################################


			#####################################################################################
			#~! Количество объектов общее
			#####################################################################################
			$this->log($this->tag, 'generateMethod objectsCountGeneral start...');

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE 1";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod objectsCountGeneral end');
			#####################################################################################


			#####################################################################################
			#~! Количество объектов включенных
			#####################################################################################
			$this->log($this->tag, 'generateMethod objectsCountEnabled start...');

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE obj_enable";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod objectsCountEnabled end');
			#####################################################################################


			#####################################################################################
			#~! Количество объектов за сегодня
			#####################################################################################
			$this->log($this->tag, 'generateMethod objectsCountToday start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE obj_create LIKE '%$date_today%'";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod objectsCountToday end');
			#####################################################################################

		/////////////////////////////////////////////////////////////
		/////////////Владельцы и объекты - окончание/////////////////
		/////////////////////////////////////////////////////////////



		///////////////////////////////////
		/////////////Опции/////////////////
		///////////////////////////////////

			#####################################################################################
			#~! Количеств опций СВОБОДНО СЕГОДНЯ
			#####################################################################################
			$this->log($this->tag, 'generateMethod optionsIsempty start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE obj_isempty AND !obj_phantom";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod optionsIsempty end');
			#####################################################################################


			#####################################################################################
			#~! Количеств опций ПОДНЯТИЕ
			#####################################################################################
			$this->log($this->tag, 'generateMethod optionsOntop start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM cf_cashflow
					  WHERE
					  	cf_transaction = 'ontop' AND
					  	cf_date LIKE '%$date_today%'";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod optionsOntop end');
			#####################################################################################


			#####################################################################################
			#~! Количеств опций ВЫДЕЛИТЬ РАМКОЙ
			#####################################################################################
			$this->log($this->tag, 'generateMethod optionsPhotomark start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE obj_photomark";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod optionsPhotomark end');
			#####################################################################################


			#####################################################################################
			#~! Количеств опций РАСПРОДАЖА
			#####################################################################################
			$this->log($this->tag, 'generateMethod optionsSale start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM obj_object
					  WHERE obj_sale AND !obj_phantom";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod optionsSale end');
			#####################################################################################

		///////////////////////////////////////////////
		/////////////Опции - окончание/////////////////
		///////////////////////////////////////////////



		///////////////////////////////////
		/////////////Действия//////////////
		///////////////////////////////////

			#####################################################################################
			#~! Клики Показать контакты
			#####################################################################################
			$this->log($this->tag, 'generateMethod eventsShowTelExt start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM l_log
					  WHERE
					  	l_date LIKE '%$date_today%' AND
					  	l_result = 'showtelextview'";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod eventsShowTelExt end');
			#####################################################################################


			#####################################################################################
			#~! Отправка формы Показать контакты
			#####################################################################################
			$this->log($this->tag, 'generateMethod eventsSendOrder start...');

			$date_today = date("Y-m-d"); //date("Y-m-d", strtotime("-1 days"));
			//$date_today = '2016-01-15';

			$query = "SELECT COUNT(*) as count
					  FROM l_log
					  WHERE
					  	l_date LIKE '%$date_today%' AND
					  	l_result = 'showtelext' AND
					  	l_status = 'success'";
			$result = DataBase::getRow($query);

			$arrayBody[] = $result['count'];

			$this->log($this->tag, 'generateMethod eventsSendOrder end');
			#####################################################################################

		///////////////////////////////////////////////
		/////////////Действия - окончание//////////////
		///////////////////////////////////////////////


		//////////////////////////
		//Файл отчета - закрытие//
		//////////////////////////
		fputcsv($fHandler, $arrayBody, ";");
		fclose($fHandler);

		$this->log($this->tag, 'generateMethod end');
	}

}
?>