<?php

	$path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']); // определяем директорию скрипта
	chdir($path_parts['dirname']); // задаем директорию выполнение скрипта

	//Базовый класс
	include('class/Base.class.php');

	//Шаблоны отправки
	include('class/Template.class.php');

	//Действия
	include('class/Action.class.php');

	//Статистика
	include('class/UserStatistic.class.php');

	$Base = new Base();
	$Template = new Template();
	$Action = new Action();
	$UserStatistic = new UserStatistic();

	$templates_name_tech = Array(
		'activate',
		'hello',
		'isempty',
		'sale',
		'photomark',
		'awake1',
		'awake2',
		'awake3',
		'objects_disabled'
		);
?>

<!-- Просмотр всех шаблонов -->
<?php if( isset($_GET['mod']) && $_GET['mod'] == 'showtemplates' ): ?>
<h1>Шаблоны писем</h1>
<?php foreach ($templates_name_tech as $template_name_tech_key => $template_name_tech) {
	$template = $Template->getTemplate($template_name_tech); ?>
	<div style="background: #ededed; padding: 5px 20px; margin-bottom: 20px;">
		<h3 style="text-align: right;">
			<?=$template['name']?> (<?=$template_name_tech?>)
		</h3>
		<h2>
			<?=$template['subject']?>
		</h2>
		<div class="body">
			<?=$template['body']?>
		</div>
	</div>
<?php } ?>
<?php endif; ?>