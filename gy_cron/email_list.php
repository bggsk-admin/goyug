<?php //Рассылка писем

  //Библиотека
  include('cron_lib.php');

  // require 'PHPMailer/PHPMailerAutoload.php';


  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылок';
  $log[] = 'Начало работы скрипта: '.$date_current;

  //Выбираем нужные письма
  $emails = Array();
  $query = "SELECT DISTINCT(cel_cron_email_list.cel_celt_uid), celt_cron_email_list_tasks.celt_email_subject, celt_cron_email_list_tasks.celt_email_body, celt_cron_email_list_tasks.celt_date_create
            FROM `cel_cron_email_list`
            LEFT JOIN celt_cron_email_list_tasks ON celt_cron_email_list_tasks.celt_uid = cel_cron_email_list.cel_celt_uid
            WHERE !cel_cron_email_list.cel_exec";
  $result = giveTable($query);
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      $emails[$rows['cel_celt_uid']]['subject'] = $rows['celt_email_subject'];
      $emails[$rows['cel_celt_uid']]['body'] = $rows['celt_email_body'];
    }//while
    $log[] = 'Обнаружено писем: '.count($emails);
  } else {
    $log[] = 'Нет необходимости рассылать';
  }

  //Выбираем адреса для отправки
  
  $query = "SELECT cel_cron_email_list.*, u_user.u_id, u_user.u_subscribe_token
            FROM `cel_cron_email_list`
            LEFT JOIN u_user ON cel_cron_email_list.cel_email = u_user.u_email
            WHERE !cel_cron_email_list.cel_exec
            LIMIT 60";

  $result = giveTable($query);
  if(mysql_num_rows($result)){
    while($recipient = mysql_fetch_assoc($result)){
      //Письмо есть
      if(array_key_exists($recipient['cel_celt_uid'], $emails)){

        //Отправляем (каждую 1 сек.)
        usleep(1000000);

        //Генерируем токен управления рассылками, если его еще нет
        if(!$recipient['u_subscribe_token']){
          $subscribe_token = md5($recipient['u_id'].rand(1000, 999999).time());
          $query_subscribe_token = "UPDATE u_user SET `u_subscribe_token` = '".$subscribe_token."' WHERE u_id = ".$recipient['u_id'];
          goToTable($query_subscribe_token);
          $recipient['u_subscribe_token'] = $subscribe_token;
        }

        //Отписка
        $unsubscribe = '
        <hr />
        <p style="color: gray;">Если письмо попало к вам по ошибке, или вы не хотите получать уведомления о запросах, <a href="http://goyug.com/user/subscribe/token/'.$recipient['u_subscribe_token'].'/direction/unsubscribe/mode/email/">отпишитесь от получения запросов</a>.</p>';

        //Текст письма
        $body = $emails[$recipient['cel_celt_uid']]['body'];
        //Добавляем ссылку для отписки, если необходимо
        $body = str_replace('%unsubscribe%', $unsubscribe, $body);

        //Процедура отправки письма
        myMail($recipient['cel_email'], $emails[$recipient['cel_celt_uid']]['subject'], $body, $recipient['cel_reply']);

        // **********************************************************

        //Фиксируем отправку
        goToTable("UPDATE cel_cron_email_list SET `cel_exec` = '1', `cel_date_exec` = '".date("Y-m-d H:i:s")."' WHERE cel_id = '$recipient[cel_id]'");
        $log[] = 'Отправлено письмо на адрес: '.$recipient['cel_email'];
      }
      //Письма нет - откладываем отправку.
      //Обработка будет в следующий обход.
      //Возможно адреса добавлены уже после поиска писем.
      else {
        $errors[] = 'Письмо '.$recipient['cel_celt_uid'].' для получателя с ID = '.$recipient['cel_id'].' не найдено';
      }
    }//while
  } else {
    $log[] = 'Нет контактов для рассылки';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.$date_current;

  ////Оповещение каждого выполнения
  /*
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылок</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылок', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылок</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке', $bodymail);
  }
?>
