<?php //Проверка отправки писем

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $errors = Array();
  $log = Array();
  $log[] = 'Вас приветствует скрипт генерации карты сайта';
  $log[] = 'Начало работы скрипта: '.$date_current;

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.$date_current;

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт генерации карты сайта</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Проверка отправки почты', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт генерации карты сайта</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в генерации карты сайта', $bodymail);
  }
?>
