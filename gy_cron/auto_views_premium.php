<?php //Оптимизация просмотров

  //Библиотека
  include('cron_lib.php');

  $time_start = time();

  //Объекты к обновлению статы
  $objects = Array();

  //Данные по текущему обновлению
  $updateCounts = Array();

  //Берем все объекты
  $query_all_objects = "SELECT obj_id, obj_ontop_mark, obj_isempty, obj_sale FROM obj_object WHERE obj_enable AND (obj_ontop_mark OR obj_isempty OR obj_sale) ORDER BY RAND() LIMIT 400";
  $result_all_objects = giveTable($query_all_objects);
  if(mysql_num_rows($result_all_objects)){
    while($object = mysql_fetch_assoc($result_all_objects)){
      $objects[] = $object;
    }//while
  }

  //Обновляем стату
  foreach ($objects as $object_key => $object) {

    //Текущая дата
    $current_date = date("Y-m-d");

    //Кол-во просмотров
    $osp_count = 1;

    //Только подняты
    if($object['obj_ontop_mark'] && !$object['obj_isempty'] && !$object['obj_sale']){
      $osp_count = rand(1, 4);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Подняты и свободны
    else if($object['obj_ontop_mark'] && $object['obj_isempty'] && !$object['obj_sale']){
      $osp_count = rand(1, 4);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Подняты и на распродаже
    else if($object['obj_ontop_mark'] && !$object['obj_isempty'] && $object['obj_sale']){
      $osp_count = rand(1, 4);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Подняты, свободны и на распродаже
    else if($object['obj_ontop_mark'] && $object['obj_isempty'] && $object['obj_sale']){
      $osp_count = rand(2, 5);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Только свободны
    else if(!$object['obj_ontop_mark'] && $object['obj_isempty'] && !$object['obj_sale']){
      $osp_count = rand(1, 3);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Только на распродаже
    else if(!$object['obj_ontop_mark'] && !$object['obj_isempty'] && $object['obj_sale']){
      $osp_count = rand(1, 3);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //Свободны и на распродаже
    else if(!$object['obj_ontop_mark'] && $object['obj_isempty'] && $object['obj_sale']){
      $osp_count = rand(2, 3);
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }
    //По умолчанию
    else {
      $query_update_stat_permanent = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + $osp_count) WHERE `osp_obj_id` = ".$object['obj_id'];
      $query_update_stat_daily = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + $osp_count) WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` = '".$current_date."'";
    }

    //Счетчик постоянный
    $query_probe = "SELECT * FROM osp_objstat_permanent WHERE `osp_obj_id` = ".$object['obj_id'];
    $result_probe = giveTable($query_probe);
    if(mysql_num_rows($result_probe) == 0){
      $query_medicine = "INSERT INTO osp_objstat_permanent (`osp_obj_id`, `osp_count`) VALUES ('".$object['obj_id']."', '".$osp_count."')";
      if(!goToTable($query_medicine)){
        //TODO message error
      }
    } else {
      if(!goToTable($query_update_stat_permanent)){
        //TODO make warning message
      }
    }

    //Счетчик по дням
    $query_probe = "SELECT * FROM osd_objstat_daily WHERE `osd_obj_id` = ".$object['obj_id']." AND `osd_date` LIKE '%".$current_date."%'";
    $result_probe = giveTable($query_probe);
    if(mysql_num_rows($result_probe) == 0){
      $query_medicine = "INSERT INTO osd_objstat_daily (`osd_obj_id`, `osd_count`, `osd_date`) VALUES ('".$object['obj_id']."', '".$osp_count."', '".$current_date."')";
      if(!goToTable($query_medicine)){
        //TODO message error
      }
    } else {
      if(!goToTable($query_update_stat_daily)){
        //TODO make warning message
      }
    }
    /*
    //Счетчик постоянный
    if(!goToTable($query_update_stat_permanent)){
      //TODO make warning message
    } else {
      if(mysql_affected_rows() == 0){
        $query_medicine = "INSERT INTO osp_objstat_permanent (`osp_obj_id`, `osp_count`) VALUES ('".$object['obj_id']."', '".$osp_count."')";
        if(!goToTable($query_medicine)){
          //TODO message error
        }
      }
    }

    //Счетчик по дням
    if(!goToTable($query_update_stat_daily)){
      //TODO make warning message
    } else {
      if(mysql_affected_rows() == 0){
        $query_medicine = "INSERT INTO osd_objstat_daily (`osd_obj_id`, `osd_count`, `osd_date`) VALUES ('".$object['obj_id']."', '".$osp_count."', '".$current_date."')";
        if(!goToTable($query_medicine)){
          //TODO message error
        }
      }
    }
    */
    $updateCounts[$osp_count]++;
  }//foreach

  //////////////////////////
  //Статистика результатов//
  //////////////////////////
  /*
    $query_val_5 = "SELECT COUNT(*) as count FROM `os_objstat` WHERE os_date LIKE '%".date("Y-m-d H:i")."%' AND os_useragent = 'HackZilla' AND os_val = 5";
    $query_val_4 = "SELECT COUNT(*) as count FROM `os_objstat` WHERE os_date LIKE '%".date("Y-m-d H:i")."%' AND os_useragent = 'HackZilla' AND os_val = 4";
    $query_val_3 = "SELECT COUNT(*) as count FROM `os_objstat` WHERE os_date LIKE '%".date("Y-m-d H:i")."%' AND os_useragent = 'HackZilla' AND os_val = 3";
    $query_val_2 = "SELECT COUNT(*) as count FROM `os_objstat` WHERE os_date LIKE '%".date("Y-m-d H:i")."%' AND os_useragent = 'HackZilla' AND os_val = 2";
    $query_val_1 = "SELECT COUNT(*) as count FROM `os_objstat` WHERE os_date LIKE '%".date("Y-m-d H:i")."%' AND os_useragent = 'HackZilla' AND os_val = 1";

    $result_val_5 = giveTable($query_val_5);
    $result_val_4 = giveTable($query_val_4);
    $result_val_3 = giveTable($query_val_3);
    $result_val_2 = giveTable($query_val_2);
    $result_val_1 = giveTable($query_val_1);

    $objects_5 = mysql_fetch_assoc($result_val_5);
    $objects_4 = mysql_fetch_assoc($result_val_4);
    $objects_3 = mysql_fetch_assoc($result_val_3);
    $objects_2 = mysql_fetch_assoc($result_val_2);
    $objects_1 = mysql_fetch_assoc($result_val_1);
    */
  ////////////////////////////////////
  $time_end = time();
?>

<h2>
  <?="Затронуто: ".count($objects)." объектов за ".($time_end - $time_start)." секунд"?>
</h2>

<h3>Кол-во просмотров 6</h3>
<p><?=$updateCounts[6]?></p>

<h3>Кол-во просмотров 5</h3>
<p><?=$updateCounts[5]?></p>

<h3>Кол-во просмотров 4</h3>
<p><?=$updateCounts[4]?></p>

<h3>Кол-во просмотров 3</h3>
<p><?=$updateCounts[3]?></p>

<h3>Кол-во просмотров 2</h3>
<p><?=$updateCounts[2]?></p>

<h3>Кол-во просмотров 1</h3>
<p><?=$updateCounts[1]?></p>

<h3>Кол-во просмотров 0</h3>
<p><?=$updateCounts[0]?></p>

<pre>
  <?//=print_r($objects);?>
</pre>