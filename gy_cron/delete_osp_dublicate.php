<?php //Удаление дублей в таблице постоянных просмотров

  //Библиотека
  include('cron_lib.php');

  $time_start = time();

  //Объекты к рассмотрению
  $objects = Array();

  //Выбираем все уникальные значения
  $query_all_objects = "SELECT DISTINCT osp_obj_id as obj_id FROM osp_objstat_permanent WHERE 1 ORDER BY osp_obj_id";
  $result_all_objects = giveTable($query_all_objects);
  if(mysql_num_rows($result_all_objects)){
    while($object = mysql_fetch_assoc($result_all_objects)){
      $objects[] = $object;
    }//while
  }

  //Определяем идентификатор строки с максимальным значением (его оставим и обновим значение на то, что должно было быть)
  foreach ($objects as $object_key => $object) {
    $query = "SELECT osp_id FROM osp_objstat_permanent WHERE osp_obj_id = $object[obj_id] ORDER BY osp_count DESC LIMIT 1";
    $result = giveTable($query);
    if(mysql_num_rows($result)){
      $osp_row = mysql_fetch_assoc($result);
      $osp_id_max = $osp_row['osp_id'];
    } else {
      $osp_id_max = 0;
    }
    $objects[$object_key]['max_id'] = $osp_id_max;
  }//foreach

  //Определяем общие просмотры
  foreach ($objects as $object_key => $object) {
    $query = "SELECT SUM(osp_count) as count_sum FROM osp_objstat_permanent WHERE osp_obj_id = $object[obj_id]";
    $result = giveTable($query);
    if(mysql_num_rows($result)){
      $osp_row = mysql_fetch_assoc($result);
      $osp_count_real = $osp_row['count_sum'];
    } else {
      $osp_count_real = 0;
    }
    $objects[$object_key]['osp_count_real'] = $osp_count_real;
  }//foreach

  //Обновляем просмотры
  foreach ($objects as $object_key => $object) {
    $query = "UPDATE osp_objstat_permanent SET osp_count = $object[osp_count_real] WHERE osp_id = $object[max_id]";
    if(!goToTable($query)){
      die("Ошибка <br />".mysql_error());
    }
  }//foreach

  //Удаляем лишние записи
  foreach ($objects as $object_key => $object) {
    $query = "DELETE FROM osp_objstat_permanent WHERE osp_obj_id = $object[obj_id] AND osp_id != $object[max_id]";
    if(!goToTable($query)){
      die("Ошибка <br />".mysql_error());
    }
  }

  $time_end = time();
?>

<h2>
  <?="Уникализировано: ".count($objects)." объектов за ".($time_end - $time_start)." секунд"?>
</h2>

<pre>
  <?=print_r($objects)?>
</pre>