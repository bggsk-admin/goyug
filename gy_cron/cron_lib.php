<?php //Упрощенная библиотека для работы с базой данных GoYug.com

//PHPMailer
require 'PHPMailer/PHPMailerAutoload.php';

if(!isset($_SERVER['HTTP_HOST'])) $_SERVER['HTTP_HOST'] = 'goyug.com';
if(!isset($_SERVER['HOSTNAME'])) $_SERVER['HOSTNAME'] = 'goyug.com';

if($_SERVER['HTTP_HOST'] == 'goyug-cron'){

  $dbhost = "localhost";
  $dbuser = "root";
  $dbpassword = "";
  $dbname = "goyug";

} else {

  $dbhost = "localhost";
  $dbuser = "goyugdev";
  $dbpassword = "shryb1ciz9dan";
  $dbname = "goyug";
}

//Чтение базы
function giveTable($query){

  global $dbhost, $dbname, $dbpassword, $dbuser;

  $link = mysql_connect($dbhost, $dbuser, $dbpassword)
    or die("Невозможно установить связь с базой данных");

  mysql_select_db($dbname, $link)
    or die("Невозможно выбрать базу данных");

  mysql_query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");
  mysql_query("SET CHARACTER SET 'utf8'");

  // 27/12 - Marsel.
  //mysql_query("SET profiling = 1;");
  // --

  if($result = mysql_query($query)){

    // ----------------------------------------------------------------
    /*
    $exec_time_result=mysql_query("SELECT query_id, SUM(duration) FROM information_schema.profiling GROUP BY query_id ORDER BY query_id DESC LIMIT 1;");
    if (mysql_errno()) { die( "ERROR ".mysql_errno($link) . ": " . mysql_error($link) ); }
    $exec_time_row = mysql_fetch_array($exec_time_result);

    $message = "\r\n";
    $message .= date("Y-m-d H:i:s") . "\r\n";
    $message .= $exec_time_row[1] . "\r\n";
    $message .= $query . "\r\n";

    $mysql_logfile = '/home/c/c74542/goyug.com/application/ui/data/logs/db-cron-queries.log';
    $mysql_fat_logfile = '/home/c/c74542/goyug.com/application/ui/data/logs/db-fat-queries.log';
    file_put_contents($mysql_logfile, $message, FILE_APPEND | LOCK_EX);

    if(floatval($exec_time_row[1]) > 0.1)
    {
      file_put_contents($mysql_fat_logfile, $message, FILE_APPEND | LOCK_EX);
    }
    */
    // ----------------------------------------------------------------

    return $result;
  }
  else return false;
}

//Запись в базу
function goToTable($query){

  global $dbhost, $dbname, $dbpassword, $dbuser;

  $link = mysql_connect($dbhost, $dbuser, $dbpassword)
    or die("Невозможно установить связь с базой данных");

  mysql_select_db($dbname, $link)
    or die("Невозможно выбрать базу данных");

  mysql_query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");
  mysql_query("SET CHARACTER SET 'utf8'");

  // 27/12 - Marsel.
  //mysql_query("SET profiling = 1;");
  // --

  if(mysql_query($query)){

    // ----------------------------------------------------------------
    /*
    $exec_time_result=mysql_query("SELECT query_id, SUM(duration) FROM information_schema.profiling GROUP BY query_id ORDER BY query_id DESC LIMIT 1;");
    if (mysql_errno()) { die( "ERROR ".mysql_errno($link) . ": " . mysql_error($link) ); }
    $exec_time_row = mysql_fetch_array($exec_time_result);

    $message = "\r\n";
    $message .= date("Y-m-d H:i:s") . "\r\n";
    $message .= $exec_time_row[1] . "\r\n";
    $message .= $query . "\r\n";

    $mysql_logfile = '/home/c/c74542/goyug.com/application/ui/data/logs/db-cron-queries.log';
    $mysql_fat_logfile = '/home/c/c74542/goyug.com/application/ui/data/logs/db-fat-queries.log';
    file_put_contents($mysql_logfile, $message, FILE_APPEND | LOCK_EX);

    if(floatval($exec_time_row[1]) > 0.1)
    {
      file_put_contents($mysql_fat_logfile, $message, FILE_APPEND | LOCK_EX);
    }
    */
    // ----------------------------------------------------------------

    return true;
  }
  else return false;
}

//Вставка новой строки
function insertToTable($table_name, $row){

  //Запрос
  $query = "INSERT INTO $table_name (";
  //Промежуточный массив параметров
  foreach($row as $key => $val){
    $query_temp_key[] = "`$key`";
    $query_temp_val[] = "'$val'";
  }
  //Преобразование списка ключей в строку
  $query .= implode(",", $query_temp_key).") VALUES (";
  //Преобразование списка значений в строку
  $query .= implode(",", $query_temp_val).")";

  return goToTable($query);
}

//Название месяца в именительном падеже
function giveRealMonthIp($month){

  $month_a = Array(
  "01" => "Январь",
  "02" => "Февраль",
  "03" => "Март",
  "04" => "Апрель",
  "05" => "Май",
  "06" => "Июнь",
  "07" => "Июль",
  "08" => "Август",
  "09" => "Сентябрь",
  "10" => "Октябрь",
  "11" => "Ноябрь",
  "12" => "Декабрь"
  );

  return strtr($month, $month_a);
}

//Название месяца в родительном падеже
function giveRealMounth($month){

  $month_a = Array(
  "01" => "Января",
  "02" => "Февраля",
  "03" => "Марта",
  "04" => "Апреля",
  "05" => "Мая",
  "06" => "Июня",
  "07" => "Июля",
  "08" => "Августа",
  "09" => "Сентября",
  "10" => "Октября",
  "11" => "Ноября",
  "12" => "Декабря"
  );

  return strtr($month, $month_a);
}

//Дата
function giveRealData($data){

  $data_a = explode(" ",$data);
  $data_time_a = explode(":", $data_a[1]);

  $real_data = explode("-",$data_a[0]);
  $real_data[1] = giveRealMounth($real_data[1]);

  $output = $real_data[2]." ".$real_data[1]." ".$real_data[0]." г.";

  return $output;
}

//Дата и время
function giveRealDataFull($data){

  $data_a = explode(" ",$data);
  $data_time_a = explode(":", $data_a[1]);

  $real_data = explode("-",$data_a[0]);
  $real_data[1] = giveRealMounth($real_data[1]);

  $output = $real_data[2]." ".$real_data[1]." ".$real_data[0]." г. в ".$data_time_a[0].":".$data_time_a[1];

  return $output;
}

//Расширение файла
function giveFileExt($filename){

  return pathinfo($filename, PATHINFO_EXTENSION);
}

//Транслитерация
function translit($str){

  $tr = Array(

  //Заглавные буквы
  "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d", "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
  "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n", "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
  "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch", "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
  "Э"=>"e","Ю"=>"yu","Я"=>"ya",

  //Строчные буквы
  "а"=>"a","б"=>"b", "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j", "з"=>"z","и"=>"i",
  "й"=>"y","к"=>"k","л"=>"l", "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r", "с"=>"s","т"=>"t",
  "у"=>"u","ф"=>"f","х"=>"h", "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y", "ы"=>"y","ь"=>"",
  "э"=>"e","ю"=>"yu","я"=>"ya",

  //Знаки и спецсимволы
  " "=> "_", "/"=> "_", "-" => "_", "(" => "_", ")" => "_", "," => "", "&" => "and", ";" => ""
  );

  return mb_strtolower(strtr($str, $tr));
}

//Вырезка картинки
function extrude($src, $dst_path, $w, $h, $crop = true){

  //Масштабирование
  $zoom = false;

  //Получаем ресурс изображения
  if(@imagecreatefromjpeg($src)){
    $src = imagecreatefromjpeg($src);
    $file_format = "jpg";
  }
  else if(@imagecreatefrompng($src)){
    $src = imagecreatefrompng($src);
    $file_format = "png";
  }
  else if(@imagecreatefromgif($src)){
    $src = imagecreatefromgif($src);
    $file_format = "gif";
  }

  //Исходный размер изображения
  $srcWidth = imagesx($src);
  $srcHeight = imagesy($src);

  //Пришедшие значения
  $maxWidth = $w;
  $maxHeight = $h;

  $k = $crop ? min($srcHeight/$maxHeight, $srcWidth/$maxWidth) : max($srcHeight/$maxHeight, $srcWidth/$maxWidth);
  $k = !$zoom && $k < 1 ? 1 : $k;

  $xDiff = $srcWidth/$k - $maxWidth > 0 ? $srcWidth/$k - $maxWidth : 0;
  $yDiff = $srcHeight/$k - $maxHeight > 0 ? $srcHeight/$k - $maxHeight: 0;

  //Если пришел jpg
  if($file_format == 'jpg'){

    //Создаем изображение
    $dst = imagecreatetruecolor($srcWidth/$k-$xDiff, $srcHeight/$k-$yDiff);

    //Накладываем пришедшее изображение
    imagecopyresampled($dst, $src, 0, 0, $xDiff/2*$k, $yDiff/2*$k, $srcWidth/$k-$xDiff, $srcHeight/$k-$yDiff, $srcWidth-$xDiff*$k, $srcHeight-$yDiff*$k);

    //Сохраняем изображение
    imagejpeg($dst, $dst_path, 100);

    //Уничтожаем ресурс файла
    imagedestroy($src);
  }
  //Если пришел png или gif
  else if($file_format == 'png' || $file_format == 'gif'){

    //Создаем изображение
    $dst = imagecreatetruecolor($srcWidth/$k-$xDiff, $srcHeight/$k-$yDiff);
    imagesavealpha($dst, true);

    //Заполняем изображение прозрачной заливкой
    $color = imagecolorallocatealpha($dst, 0x00, 0x00, 0x00, 127);
    imagefill($dst, 0, 0, $color);

    //Накладываем пришедшую картинку
    imagecopyresampled($dst, $src, 0, 0, $xDiff/2*$k, $yDiff/2*$k, $srcWidth/$k-$xDiff, $srcHeight/$k-$yDiff, $srcWidth-$xDiff*$k, $srcHeight-$yDiff*$k);

    //Сохраняем файл
    imagepng($dst, $dst_path);

    //Уничтожаем ресурс файла
    imagedestroy($dst);
  }
}

//****************************************************************************************************
//
//          Отправка письма
//
//****************************************************************************************************
function myMail($to, $subject, $html, $reply = "")
{
        $sender = "GOYUG.com | Квартиры, апартаменты, дома посуточно";
        $from = "info@goyug.com";
        $replyto = "info@goyug.com";

        // **********************************************************
        // Отправка письма через PHPMailer
        // **********************************************************
        // $mail = new PHPMailer;
        // $mail->setFrom($from, $sender);
        // $mail->addReplyTo($replyto, $sender);
        // $mail->addAddress($to, '');
        // $mail->Subject = $subject;
        // $mail->msgHTML($html);
        // $mail->send();

        // **********************************************************
        // Отправка письма через Zend Mail
        // **********************************************************
        // require_once '/home/ZendFramework-1.12.3/library/Zend/Mail.php';
        // require_once '/home/ZendFramework-1.12.3/library/Zend/Mail/Transport/Sendmail.php';

        require_once '/usr/share/php/Zend/Mail.php';
        require_once '/usr/share/php/Zend/Mail/Transport/Sendmail.php';

        $transport = new Zend_Mail_Transport_Sendmail();
        $mail = new Zend_Mail('UTF-8');

        $mail->setBodyHTML($html);
        $mail->setFrom($from, $sender);
        $mail->setReplyTo($replyto, '');
        $mail->addTo($to, '');
        $mail->setSubject($subject);
        $mail->send($transport);  
        


        // $newline = "\n";
        // $boundary = '----=_NextPart_' . md5(time());
        // $sender = "GOYUG.com | Квартиры, апартаменты, дома посуточно";
        // $from = "info@goyug.com";
        // $replyto = "info@goyug.com";

        // $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

        // $header = 'MIME-Version: 1.0' . $newline;
        // $header .= 'From: =?UTF-8?B?' . base64_encode($sender) . '?=' . ' <' . $from . '>' . $newline;
        // $header .= 'Reply-To: =?UTF-8?B?' . base64_encode($replyto) . '?=' . ' <' . $from . '>' . $newline;
        // $header .= 'Return-Path: ' . $from . $newline;
        // $header .= 'Content-Type: multipart/related; boundary="' . $boundary . '"' . $newline . $newline;

        // $message = 'Content-Type: text/html; charset="utf-8"' . $newline;
        // $message .= 'Content-Transfer-Encoding: 8bit' . $newline . $newline;
        // $message .= $html . $newline;

        // ini_set('sendmail_from', $from);
        // mail($to, $subject, $message, $header, '-f' . $from);

        // mail($to, $subject, $html, $headers, "-finfo@goyug.com"); 

}
//****************************************************************************************************


//****************************************************************************************************
//
//          Отправка письма
//
//****************************************************************************************************
function sendMail($to, $subject, $bodyHTML){

  //Картинка
  $body_images['emailimg']['path'] = "http://goyug.com/stat/emailimg/uid/bbbf65361df107986eda8738ef6ffdd2/u_id/528/";

  //Случай нескольких E-mail
  $to_a = explode(',', $to);

  $mail = new PHPMailer;

  $mail->isSendmail();
  $mail->CharSet = 'utf-8';

  $mail->From = 'info@goyug.com';
  $mail->FromName = 'Сайт goyug.com';
  for($i = 0; $i < count($to_a); $i++){
    $mail->addAddress($to_a[$i]);
  }
  $mail->addReplyTo('info@goyug.com');
  $mail->Sender = 'info@goyug.com';
  //$mail->addBCC('bcc@example.com');

  $mail->AddEmbeddedImage($body_images['emailimg']['path'], 'emailimg', '1.png');
  $mail->isHTML(true);
  $mail->Subject = $subject;
  $mail->Body = $bodyHTML;

  if(!$mail->send()) {
    myMail("marselos@gmail.com", "Ошибка отправки письма из cron", 'Mailer Error: '.$mail->ErrorInfo);
    return false;
  } else {
    return true;
  }
}
//****************************************************************************************************


//Фотографии объекта
function getObjectImages($obj_id = 0){

  $res = Array();

  if ($obj_id > 0) {

    $rows = Array();

    //Вытаскиваем фотки
    $query = "SELECT f_name
              FROM f_files
              WHERE f_ucid = 'object'
              AND f_uid = '".$obj_id."'
              ORDER BY f_order";

    $result = giveTable($query);

    if(mysql_num_rows($result)){

      while($file = mysql_fetch_assoc($result)){
        $res[] = $file['f_name'];
      }//while
    }
  }

  return $res;
}

//Человекочитаемый телефон
function getHumanTel($tel) {
  if(mb_strlen($tel, 'utf-8') == 11){
    $tel_part_1 = substr($tel, 1, 3);
    $tel_part_2 = substr($tel, 4, 3);
    $tel_part_3 = substr($tel, 7, 2);
    $tel_part_4 = substr($tel, 9, 2);
    $tel = "+7 ($tel_part_1) $tel_part_2-$tel_part_3-$tel_part_4";
  }
  return $tel;
}
?>