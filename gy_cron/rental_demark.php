<?php //Отключение услуги АБОНЕНТСКАЯ ПЛАТА

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Стоимость услуги
  $rental_price = 100;

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт деактивации услуги АБОНЕНТСКАЯ ПЛАТА';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Выбираем пользователей
  $query = "SELECT u.u_id, u.u_rental_nextpay, u.u_rental_deactivate, u.u_money, u.u_email, u.u_authtoken
            FROM u_user as u
            WHERE u_rental AND u_rental_deactivate <= '".$date_current."'";
  $result = giveTable($query);

  /////////////////////////
  //Разбиваем на сегменты//
  /////////////////////////
    $users_to_nextpay = Array();
    $users_to_deactivate = Array();
    $users_to_deactivate_with_notification = Array();

    if(mysql_num_rows($result)){

      while($rows = mysql_fetch_assoc($result)){

        //На деактивацию
        if($rows['u_rental_nextpay'] == '0000-00-00 00:00:00'){
          $users_to_deactivate[] = $rows;
        }
        //На продление услуги
        else if( $rows['u_rental_nextpay'] != '0000-00-00 00:00:00' && $rows['u_money'] >= $rental_price ){
          $users_to_nextpay[] = $rows;
        }
        //На деактивацию с уведомением о недостаточных средствах
        else if( $rows['u_rental_nextpay'] != '0000-00-00 00:00:00' && $rows['u_money'] < $rental_price ){
          $users_to_deactivate_with_notification[] = $rows;
        }

      }//while
    } else {
      $log[] = 'Нет владельцев для деактивации услуги АБОНЕНТСКАЯ ПЛАТА.';
    }
  /////////////////////////////////////
  //Разбиваем на сегменты - окончание//
  /////////////////////////////////////

  //////////////
  //Отключение//
  //////////////
    foreach($users_to_deactivate as $user){

      if(!goToTable("UPDATE u_user SET u_rental = 0 WHERE u_id = ".$user['u_id'])){
        $errors[] = 'Ошибка деактивации услуги АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'].'<br />'.mysql_error();
      }
      else {

        /////////////////////////
        ///Send E-mail///////////
        /////////////////////////

          //Пользователь найден
          if($user['u_id']){

            //Прописываем однократный токен, если он не назначен еще
            if(!$user['u_authtoken']){
              $authtoken = md5($user['u_email'].$user['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '".$authtoken."' WHERE u_id = ".$user['u_id']);
            } else {
              $authtoken = $user['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($user['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $user['u_email'];
              $email['subject'] = 'GOYUG.COM - Ваши объявления недоступны для просмотра на сайте';
              $email['body'] = '
              <h2>Goyug.com - квартиры посуточно</h2>
              <p>Уважаемый владелец!</p>
              <p>Услуга «АБОНЕНТСКАЯ ПЛАТА» отключена. Теперь пользователи не смогут просматривать ваши объекты на сайте.</p>
              <h3>Повторная активация услуги</h3>
              <p>Для того, чтобы повторно подключить услугу «АБОНЕНТСКАЯ ПЛАТА», необходимо зайти в ваш Личный Кабинет и подключить ее в разделе «Профиль».</p>
              <p>Стоимость месячного абонемента - '.$rental_price.' рублей.</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/rental_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Отключение услуги АБОНЕНТСКАЯ ПЛАТА - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$user['u_id'].' не отправлено уведомление о деактивации услуги АБОНЕНТСКАЯ ПЛАТА, т.к. у него не указан E-mail.</p>';

              //Лог
              $errors[] = 'Владельцу с ID '.$user['u_id'].' не отправлено уведомление о деактивации услуги АБОНЕНТСКАЯ ПЛАТА, т.к. у него не указан E-mail.';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Отключение услуги АБОНЕНТСКАЯ ПЛАТА - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Отключение услуги объекта АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
        /////////////////////////
        ///Send E-mail end///////
        /////////////////////////

        $log[] = 'У пользователя деактивирована услуга АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'];
      }
    }//foreach
  //////////////////////////
  //Отключение - окончание//
  //////////////////////////

  /////////////
  //Продление//
  /////////////
    foreach($users_to_nextpay as $user){

      //Проверяем достаточность средств на балансе
      if($user['u_money'] >= $rental_price){

        ////////////////////////
        //Все норм. продлеваем//
        ////////////////////////

          //Дата следующего списания
          $date_compound_a = explode(" ", $user['u_rental_nextpay']);
          $date_date_a = explode("-", $date_compound_a[0]);
          $date_time_a = explode(":", $date_compound_a[1]);

          $date_nextpay = date("Y-m-d H:i:s", mktime( $date_time_a[0], $date_time_a[1], $date_time_a[2], $date_date_a[1], $date_date_a[2], $date_date_a[0] ) + (60*60*24)*30);
          $date_deactivate = $date_nextpay;

          //Продление
          if(goToTable("UPDATE u_user SET `u_rental` = 1, `u_rental_nextpay` = '".$date_nextpay."', `u_rental_deactivate` = '".$date_deactivate."' WHERE u_id = ".$user['u_id'])){

            //Снимаем отметки о упреждении
            goToTable("UPDATE u_user SET 'u_rental_warning_7_day' = 0, 'u_rental_warning_3_day' = 0, 'u_rental_warning_1_day' = 0, 'u_rental_expire_3_day' = 0,  'u_rental_expire_7_day' = 0, 'u_rental_expire_14_day' = 0, 'u_rental_expire_21_day' = 0 WHERE u_id = ".$user['u_id']);

            ////////////////////
            //Списание средств//
            ////////////////////

              //Транзакция
              $transaction = Array(
                "cf_u_id" => $user['u_id'],
                "cf_obj_id" => "",
                "cf_transaction" => "rental",
                "cf_table" => "u_user",
                "cf_table_id" => $user['u_id'],
                "cf_sum" => $rental_price,
                "cf_mode" => "minus",
                "cf_date" => date("Y-m-d H:i:s")
                );

              //Выполнение транзакции
              $resultTransaction = insertToTable('cf_cashflow', $transaction);
              $cf_id = 0;

              //Если транзакция зарегистрирована
              if($resultTransaction) {

                //ID транзакции
                $cf_id = mysql_insert_id();

                //Вычитаем денежку со счета
                $user['u_money'] -= $rental_price;

                //Если баланс ушел в минус - обнуляем его
                if($user['u_money'] < 0) $user['u_money'] = 0;

                //Сохранение пользователя
                $resultUserUpdate = goToTable("UPDATE u_user SET `u_money` = '".$user['u_money']."' WHERE u_id = ".$user['u_id']);

                //Успех транзакции
                if($resultUserUpdate) {
                  $cf_success = 1;
                } else {
                  $cf_success = 0;
                }

                //Обновляем транзакцию
                $transactionUpdate = goToTable("UPDATE cf_cashflow SET `cf_success` = '".$cf_success."' WHERE cf_id = ".$cf_id);

                if($transactionUpdate){
                  $log[] = 'Списание денег за продление услуги АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].'. Операция проведена успешна.';
                } else {
                  $errors[] = 'Ошибка списания денег за продление услуги АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].'. Транзакция не подтверждена.';
                }
              } else {
                $errors[] = 'Ошибка списания денег за продление услуги АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].'. Транзакция не зарегистрирована. Срочно все проверьте!';
              }

            ////////////////////////////////
            //Списание средств - окончание//
            ////////////////////////////////

            /////////////////////////
            ///Send E-mail///////////
            /////////////////////////

              //Пользователь найден
              if($user['u_id']){

                //Прописываем однократный токен, если он не назначен еще
                if(!$user['u_authtoken']){
                  $authtoken = md5($user['u_email'].$user['u_id'].rand(1000, 9999));
                  goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[u_id]");
                } else {
                  $authtoken = $user['u_authtoken'];
                }

                //Если указан E-mail - отправляем уведомление
                if($user['u_email']){
                  //Письмо
                  $email = Array();
                  $email['to'] = $user['u_email'];
                  $email['subject'] = 'Услуга «АБОНЕНТСКАЯ ПЛАТА» продлена';
                  $email['body'] = '
                  <h2>Уважаемый владелец!</h2>
                  <p>Услуга «АБОНЕНТСКАЯ ПЛАТА» <strong>продлена на 30 суток</strong>.</p>
                  <p>С вашего баланса списано <strong>'.$rental_price.' рублей.</strong></p>
                  <p>Отключение услуги доступна в вашем Личном Кабинете.</p>
                  <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/rental_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
                }
                //Если не указан E-mail - оптравляем оповещение
                else {
                  //Письмо
                  $email = Array();
                  $email['to'] = 'info@goyug.com';
                  $email['subject'] = 'Продление услуги АБОНЕНТСКАЯ ПЛАТА - E-mail владельца не указан';
                  $email['body'] = '
                  <h2>Внимание!</h2>
                  <p>Владельцу с ID '.$user['u_id'].' не отправлено уведомление о продлении услуги АБОНЕНТСКАЯ ПЛАТА, т.к. у него не указан E-mail.</p>';
                }
              }
              //Пользователь не найден
              else {

                //Письмо
                $email = Array();
                $email['to'] = 'info@goyug.com';
                $email['subject'] = 'Продление услуги АБОНЕНТСКАЯ ПЛАТА - владелец не найден';
                $email['body'] = '
                <h2>Ахтунг!</h2>
                <p>Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

                //Лог
                $errors[] = 'Продление услуги АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
              }

              //Отправка
              myMail($email['to'], $email['subject'], $email['body']);

            /////////////////////////
            ///Send E-mail end///////
            /////////////////////////

            $log[] = 'На объекте продлена услуга АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'];
          }
          else {
            $errors[] = 'Ошибка продления услуги АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'].'<br />'.mysql_error();
          }

        ////////////////////////////////////
        //Все норм. продлеваем - окончание//
        ////////////////////////////////////

      } else {
        $errors[] = 'Ошибка продления услуги АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].'. Не хватает бабла для продления услуги. Проверьте на всякий случай!';
        $users_to_deactivate_with_notification[] = $user;
      }

    }//foreach
  /////////////////////////
  //Продление - окончание//
  /////////////////////////

  ////////////////////////////
  //Отключение с оповещением//
  ////////////////////////////
    foreach($users_to_deactivate_with_notification as $user){

      if(!goToTable("UPDATE u_user SET u_rental = 0 WHERE u_id = ".$user['u_id'])){
        $errors[] = 'Ошибка отключения с уведомлением услуги АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'].'<br />'.mysql_error();
      }
      else {

        /////////////////////////
        ///Send E-mail///////////
        /////////////////////////

          //Пользователь найден
          if($user['u_id']){

            //Прописываем однократный токен, если он не назначен еще
            if(!$user['u_authtoken']){
              $authtoken = md5($user['u_email'].$user['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '".$authtoken."' WHERE u_id = ".$user['u_id']);
            } else {
              $authtoken = $user['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($user['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $user['u_email'];
              $email['subject'] = 'GOYUG.COM - Ваши объявления недоступны для просмотра на сайте';
              $email['body'] = '
              <h2>Goyug.com - квартиры посуточно</h2>
              <p>Уважаемый владелец!</p>
              <p>На вашем счету недостаточно средств для продления абонемента.</p>
              <p>В данный момент ваши объявления недоступны для просмотра на сайте GOYUG.COM. Вам необходимо пополнить баланс в личном кабинете сайта.</p>
              <p>Стоимость месячного абонемента - '.$rental_price.' рублей.</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/rental_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Отключение с уведомлением услуги АБОНЕНТСКАЯ ПЛАТА - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$user['u_id'].' не отправлено уведомление о деактивации с уведомлением услуги АБОНЕНТСКАЯ ПЛАТА, т.к. у него не указан E-mail.</p>';

              //Лог
              $errors[] = 'Владельцу с ID '.$user['u_id'].' не отправлено уведомление о деактивации с уведомлением услуги АБОНЕНТСКАЯ ПЛАТА, т.к. у него не указан E-mail.';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Отключение с уведомлением услуги АБОНЕНТСКАЯ ПЛАТА - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Отключение услуги с уведомлением объекта АБОНЕНТСКАЯ ПЛАТА. Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
        /////////////////////////
        ///Send E-mail end///////
        /////////////////////////

        $log[] = 'У владельца деактивирована с уведомлением услуга АБОНЕНТСКАЯ ПЛАТА, u_id: '.$user['u_id'];
      }
    }//foreach
  ////////////////////////////////////////
  //Отключение с оповещением - окончание//
  ////////////////////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  echo "<h2>Скрипт деактивации услуги АБОНЕНТСКАЯ ПЛАТА</h2>\n\r";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>'."\n\r";
  }
  //Отправка лога
  //myMail('marselos@gmail.com', 'Скрипт деактивации услуги', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт деактивации услуги АБОНЕНТСКАЯ ПЛАТА</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения АБОНЕНТСКАЯ ПЛАТА', $bodymail);
  }
?>
