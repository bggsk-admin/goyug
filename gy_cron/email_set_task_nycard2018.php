<?php //Рассылка письма

  //Библиотека
  include('cron_lib.php');

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылки письма';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //uid писем
  $uids = Array();

  //Регистрация рассылки
  $eso = Array();

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $path = "../gy_cron";
  }
  //На сервере
  else {
    $path = "/var/www/goyug/gy_cron";
  }

// ID рассылки
$src_mod = "nycard2018";
$eso_desc = "Новый Год 2018 (поздравление)";


  //Проверяем создавалась ли уже рассылка
  if(!file_exists($path."/email_set_task_nycard2018.txt")){

    //////////////////////////////////////////
    ///Рассылка для всех владельцев///////////
    //////////////////////////////////////////

      //////////////////////
      //Находим владельцев//
      //////////////////////

        //Список рассылки
        $users = Array();

        //Лист отправки - все владельцы
        $query = "SELECT DISTINCT(obj_u_id), 
obj_object.obj_addr_city_uid AS city_uid, 
u_user.u_id AS id, 
LCASE(u_user.u_email) as email, 
TRIM(CONCAT(UPPER(MID(u_user.u_firstname,1,1)), MID(u_user.u_firstname,2))) AS name, 
u_user.u_authtoken AS authtoken, 
u_user.u_money
FROM obj_object
LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
WHERE u_user.u_rental=1
AND (u_user.u_notes != 'virtrental' OR ISNULL(u_user.u_notes) OR u_user.u_notes = '')
AND u_user.u_email != ''
AND NOT ISNULL(u_user.u_email)
AND u_user.u_email LIKE '%@%.%'
ORDER BY city_uid";

        //echo $query;
        $result = giveTable($query);

        if(mysql_num_rows($result)){
          while($rows = mysql_fetch_assoc($result)){
            unset($rows['obj_u_id']);
            $users[] = $rows;
          }//while
        }

        $log[] = 'Найдено '.count($users). ' адресатов';

      //////////////////////////////////
      //Находим владельцев - окончание//
      //////////////////////////////////

      ///////////////////////////////////
      //Формируем индивидуальные письма//
      ///////////////////////////////////

        //Идентификатор рассылки
        $eso_uid = md5($src_mod.count($users).date("Y-m-d"));

        foreach ($users as $key => $user) 
        {

          //Одноразовый токен
          $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));

          //Создаем, если он еще не назначен
          if($user['authtoken'] == '')
          {
            goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
            $user['authtoken'] = $authtoken;
          }

          //Таблица celt_cron_email_list_tasks
          $celt = Array();

          //Идентификатор письма
          $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
          $uids[] = $celt['celt_uid'];

          //Время создания
          $celt['celt_date_create'] = date("Y-m-d H:i:s");

          //Письмо
          $celt['celt_email_subject'] = 'GOYUG || Поздравляем с Новым годом и Рождеством!';
          
          $celt['celt_email_body'] =
          '<!DOCTYPE html>
          <html lang="ru">
          <head>
              <meta charset="UTF-8">
              <title>'.$celt['celt_email_subject'].'</title>
          </head>
          <body>

            <h2>Goyug.com - квартиры посуточно</h2>

            <p>Уважаемый Владелец!</p>
            <p>Поздравляем Вас с наступающими Новым годом и Рождеством!</p>

            <p>В наступающем Новом году хотим пожелать Вам успехов и процветания, а также здоровья, личного счастья и стабильности!</p>
            <p>С легкостью преодолеть все проблемы уходящего года и нацелиться на развитие и благополучие!</p>

            <p>А в мы, в свою очередь, предложим Вам еще больше возможностей по управлению Вашими объектами и будем продолжать поддерживать Вас в вашем деле.</p>

            <p>Будьте с нами в Новом году!</p>

            <p>
              --<br />
              С уважением,<br />
              команда GOYUG.COM
            </p>

            <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">

          </body>
          </html>';

          // ************* Запись в базу *********************************
          $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

          //Адресаты
          $cel = Array();
          $cel['cel_celt_uid'] = $celt['celt_uid'];
          $cel['cel_email'] = $user['email'];
          $cel['cel_reply'] = 'info@goyug.com';
          $cel['cel_date_stage'] = date("Y-m-d H:i:s");

          // ************* Запись в базу *********************************
          $result_cel = insertToTable('cel_cron_email_list', $cel);

          // Останов в случае ошибки
          if(!$result_celt || !$result_cel){
            die(mysql_error());
          }

        }
        //foreach

        //Для избежания повторной генерации писем создаем файл
        $text = "Задача поставлена для ".count($users)." адресатов.\n\r";

        foreach ($uids as $key => $uid) {
          $text .= $uid."\n\r";
        }

        file_put_contents($path."/email_set_task_nycard2018.txt", $text);

      ///////////////////////////////////////////////
      //Формируем индивидуальные письма - окончание//
      ///////////////////////////////////////////////

      ////////////////////////
      //Регистрация рассылки//
      ////////////////////////
        $eso['eso_uid'] = $eso_uid;
        $eso['eso_mod'] = $src_mod;
        $eso['eso_count'] = count($users);
        $eso['eso_date'] = date("Y-m-d");
        $eso['eso_desc'] = $eso_desc;
        
        // ************* Запись в базу *********************************
        if(!insertToTable('eso_email_stat_opens', $eso))
        {
          myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
        }

      ////////////////////////////////////
      //Регистрация рассылки - окончание//
      ////////////////////////////////////

    //////////////////////////////////////////////////////
    ///Рассылка для всех владельцев - окончание///////////
    //////////////////////////////////////////////////////

  } else {
    $log[] = 'Рассылка уже назначена';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылки письма</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
  }
?>