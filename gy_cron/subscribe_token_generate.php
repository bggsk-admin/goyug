<?php //Генерация токенов для управления рассылками

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт генерации токенов для управления рассылками';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Пользователи
  $query = "SELECT u_id FROM u_user WHERE u_subscribe_token = ''";
  $result = giveTable($query);

  //Прописываем токены
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      $token = md5($rows['u_id'].rand(1000, 999999).time());
      if(!goToTable("UPDATE u_user SET `u_subscribe_token` = '$token' WHERE u_id = $rows[u_id]")){
        $errors[] = 'Ошибка назначения токена, u_id: '.$rows['u_id'].'<br />'.mysql_error();
      }
      else
        $log[] = 'Пользователю назначен токен, u_id: '.$rows['u_id'].', token: '.$token;
    }//while
  } else {
    $log[] = 'Нет пользователей.';
    $errors[] = 'Нет пользователей.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  /*
  $bodymail = "<h2>Скрипт генерации токенов для управления рассылками</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт генерации токенов для управления рассылками', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт генерации токенов для управления рассылками</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в генерации токенов для управления рассылками', $bodymail);
  }
?>
