<?php //Фантомы!

include("cron_lib.php");

//Фильтры
$filters = Array();
// $filters['min_obj_count'] = 20;

//Города в которых НЕ проводить фантомизацию
// $not_phantom_cities = Array();
// $not_phantom_cities_sql = (count($not_phantom_cities)>0) ? "AND ct_uid NOT IN ('" . implode("','", $not_phantom_cities) . "')" : "";

//Города абонементщики
$rentalCities = array(
        "kislovodsk", 
        "nnovgorod", 
        "sochi", "sevastopol", "spb", 
        "krasnodar", "saratov", "tyumen", 
        "novosibirsk", "ekaterinburg", "perm", 
        "kazan", "ufa", "voronezh", 
        "moscow", "omsk", "rostovnadonu", "volgograd",
        "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
        "zheleznovodsk",
        "pyatigorsk", "yalta", "yaroslavl", "tomsk",
        "bryansk", "tula", "magnitogorsk", "ivanovo"
      );
$rentalCities_sql = "IN ('" . implode("','", $rentalCities) . "')";

//Города для фантомизации с объектами
$phantoms = Array();

/////////////////////////////////////////////////////
//Получение городов, где необходимо фантомизировать//
/////////////////////////////////////////////////////
	$query_cities = "SELECT ct_uid, ct_objects FROM ct_city WHERE ct_objects > 20 ORDER BY ct_objects DESC";

	$result_cities = giveTable($query_cities);
	if(mysql_num_rows($result_cities)){
		$row = 0;
		while($city = mysql_fetch_assoc($result_cities)){
			$phantoms[$row]['city_uid'] = $city['ct_uid'];
			$phantoms[$row]['city_count'] = $city['ct_objects'];
			$row++;
		}//while
	}
	unset($row);
/////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
//Заполнение городов объектами, подлежащих фантомизации//
/////////////////////////////////////////////////////////
	foreach ($phantoms as $city_key => $city) {

		$rental_sql = (in_array($city['city_uid'], $rentalCities)) ? "AND u.u_notes = 'virtrental' AND obj.obj_virtual = 1" : "AND ISNULL(u.u_lastlogin)";

		$query_objects = "
SELECT obj_u_id, obj_id
FROM u_user AS u

LEFT JOIN (SELECT obj_id, obj_enable, obj_virtual, obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object) AS obj
ON obj.obj_u_id = u.u_id

WHERE obj_addr_city_uid = '".$city['city_uid']."'

".$rental_sql."

AND obj.obj_enable = 1
ORDER BY obj.obj_addr_city_uid 
LIMIT 100";

		$result_objects = giveTable($query_objects);
		if(mysql_num_rows($result_objects)){
			while($object = mysql_fetch_assoc($result_objects)){
				$phantoms[$city_key]['city_objects_phantom_count']++;
				$phantoms[$city_key]['city_objects_phantom_items'][] = $object;
			}//while
		}
	}
/////////////////////////////////////////////////////////


///////////////////////////////
//Обработка по каждому городу//
///////////////////////////////

	//CLEANUP
	goToTable("UPDATE obj_object
			   SET `obj_phantom` = 0, `obj_ontop_mark` = 0, `obj_photomark` = 0, `obj_isempty` = 0, `obj_sale` = 0
			   WHERE obj_phantom");

	foreach ($phantoms as $city_key => $city) {

		//ONTOP
		shuffle($city['city_objects_phantom_items']);
		foreach ($city['city_objects_phantom_items'] as $object_key => $object) {
			//Новый расклад
			if($object_key <= 4){
				//Разбег в минус 10 минут
				$time_delta = time() - (rand(60, 600));
				goToTable("UPDATE obj_object SET `obj_phantom`= 1, `obj_ontop` = ".$time_delta.", `obj_ontop_mark` = 1, `obj_photomark` = 1, `obj_ontop_deactivate` = '".date("Y-m-d H:i:s", $time_delta + (60*60*24)*3)."' WHERE obj_id = ".$object['obj_id']);
			}
		}

		//ISEMPTY
		shuffle($city['city_objects_phantom_items']);
		//От четверти до половины от всех имеющихся потенциальных фантомных
		$limit_objects = rand( ceil($city['city_objects_phantom_count']/14), ceil($city['city_objects_phantom_count']/10) );
		foreach ($city['city_objects_phantom_items'] as $object_key => $object) {
			//Новый расклад
			if($object_key <= $limit_objects){
				goToTable("UPDATE obj_object
						   SET `obj_phantom`= 1, `obj_isempty` = 1, `obj_isempty_activate` = '0000-00-00 00:00:00', `obj_isempty_nextpay` = '0000-00-00 00:00:00', `obj_isempty_deactivate` = '0000-00-00 00:00:00'
						   WHERE obj_id = ".$object['obj_id']);
			}
		}

		//SALE
		shuffle($city['city_objects_phantom_items']);
		//От шестой части до четверти от всех имеющихся потенциальных фантомных
		$limit_objects = rand( ceil($city['city_objects_phantom_count']/18), ceil($city['city_objects_phantom_count']/12) );
		foreach ($city['city_objects_phantom_items'] as $object_key => $object) {
			//Новый расклад
			if($object_key <= $limit_objects){
				goToTable("UPDATE obj_object
						   SET `obj_phantom`= 1, `obj_sale` = 1, `obj_sale_activate` = '0000-00-00 00:00:00', `obj_sale_nextpay` = '0000-00-00 00:00:00', `obj_sale_deactivate` = '0000-00-00 00:00:00'
						   WHERE obj_id = ".$object['obj_id']);
			}
		}
	}
///////////////////////////////

////////////////////////////////////
//Статистика по фантомным объектам//
////////////////////////////////////
	$query_objects_all_phantom = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom";
	$query_objects_isempty = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_isempty";
	$query_objects_sale = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_sale";
	$query_objects_ontop = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_ontop_mark";
	$query_objects_ontop_isempty = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_ontop_mark AND obj_isempty";
	$query_objects_ontop_sale = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_ontop_mark AND obj_sale";
	$query_objects_full_features = "SELECT COUNT(*) as count FROM `obj_object` WHERE obj_phantom AND obj_ontop_mark AND obj_isempty AND obj_sale";

	$result_objects_all_phantom = giveTable($query_objects_all_phantom);
	$result_objects_isempty = giveTable($query_objects_isempty);
	$result_objects_sale = giveTable($query_objects_sale);
	$result_objects_ontop = giveTable($query_objects_ontop);
	$result_objects_ontop_isempty = giveTable($query_objects_ontop_isempty);
	$result_objects_ontop_sale = giveTable($query_objects_ontop_sale);
	$result_objects_full_features = giveTable($query_objects_full_features);

	$objects_all_phantom = mysql_fetch_assoc($result_objects_all_phantom);
	$objects_isempty = mysql_fetch_assoc($result_objects_isempty);
	$objects_sale = mysql_fetch_assoc($result_objects_sale);
	$objects_ontop = mysql_fetch_assoc($result_objects_ontop);
	$objects_ontop_isempty = mysql_fetch_assoc($result_objects_ontop_isempty);
	$objects_ontop_sale = mysql_fetch_assoc($result_objects_ontop_sale);
	$objects_full_features = mysql_fetch_assoc($result_objects_full_features);
////////////////////////////////////
?>

<pre>
	<?//=print_r($phantoms);?>
</pre>

<h1>Дата: <?=date("Y-m-d H:i:s")?></h1>

<h2>Всего фантомных объектов</h2>
<p><?=$objects_all_phantom['count']?></p>

<h2>Свободны сегодня</h2>
<p><?=$objects_isempty['count']?></p>

<h2>Распродажа</h2>
<p><?=$objects_sale['count']?></p>

<h2>Подняты</h2>
<p><?=$objects_ontop['count']?></p>

<h2>Подняты и Свободны сегодня</h2>
<p><?=$objects_ontop_isempty['count']?></p>

<h2>Подняты и на распродаже</h2>
<p><?=$objects_ontop_sale['count']?></p>

<h2>Все фичи</h2>
<p><?=$objects_full_features['count']?></p>