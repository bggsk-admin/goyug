<?php //Генерация Sitemap

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $sitemaps_folder = "../public_html/sitemaps";
  }
  //На сервере
  else {
    $sitemaps_folder = "/var/www/goyug/public_html/sitemaps";
  }

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт генерации карты сайта';
  $log[] = 'Начало работы скрипта: '.$date_current;

  //Выбираем города
  $queryCities = "SELECT ct_uid, ct_name_ru FROM ct_city WHERE ct_enable";
  $resultCities = giveTable($queryCities);

  ////Пишем файлы
  //Корневая папка сайтмапов
  if(!file_exists($sitemaps_folder)) mkdir($sitemaps_folder);

  //Проход по городам
  if(mysql_num_rows($resultCities)){

    //Основной адрес
    $file_path_general = $sitemaps_folder.'/sitemap.xml';
    $xml_general = new DomDocument('1.0', 'utf-8');

    //Корень основной
    $root_general = $xml_general->appendChild($xml_general->createElement('urlset'));
    $root_general_namespace = $root_general->appendChild($xml_general->createAttribute('xmlns'));
    $root_general_namespace->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    //Корень основной окончание

    //Основной
    $url_general = $root_general->appendChild($xml_general->createElement('url'));

    $url_loc_general = $url_general->appendChild($xml_general->createElement('loc'));
    $url_loc_general->appendChild($xml_general->createTextNode("http://goyug.com"));

    $url_lastmod_general = $url_general->appendChild($xml_general->createElement('lastmod'));
    $url_lastmod_general->appendChild($xml_general->createTextNode(substr($date_current, 0, strpos($date_current, ' '))));

    $changefreq_general = $url_general->appendChild($xml_general->createElement('changefreq'));
    $changefreq_general->appendChild($xml_general->createTextNode("weekly"));

    $priority_general = $url_general->appendChild($xml_general->createElement('priority'));
    $priority_general->appendChild($xml_general->createTextNode("1"));
    //Основной - окончание

    //Правила - только для основного
    $url_general = $root_general->appendChild($xml_general->createElement('url'));

    $url_loc_general = $url_general->appendChild($xml_general->createElement('loc'));
    $url_loc_general->appendChild($xml_general->createTextNode("http://goyug.com/privacy"));

    $url_lastmod_general = $url_general->appendChild($xml_general->createElement('lastmod'));
    $url_lastmod_general->appendChild($xml_general->createTextNode(substr($date_current, 0, strpos($date_current, ' '))));

    $changefreq_general = $url_general->appendChild($xml_general->createElement('changefreq'));
    $changefreq_general->appendChild($xml_general->createTextNode("yearly"));

    $priority_general = $url_general->appendChild($xml_general->createElement('priority'));
    $priority_general->appendChild($xml_general->createTextNode("0.5"));
    //Правила - только для основного - окончание

    //Проход по городам
    while($rowsCities = mysql_fetch_assoc($resultCities)){

      ////Магия
      //Поддомены
      $file_path = $sitemaps_folder.'/'.$rowsCities['ct_uid'].".sitemap.xml";

      $xml = new DomDocument('1.0', 'utf-8');

      //Корень поддомены
      $root = $xml->appendChild($xml->createElement('urlset'));
      $root_namespace = $root->appendChild($xml->createAttribute('xmlns'));
      $root_namespace->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
      //Корень поддомены окончание

      //Карта поддомены
      $url = $root->appendChild($xml->createElement('url'));

      $url_loc = $url->appendChild($xml->createElement('loc'));
      $url_loc->appendChild($xml->createTextNode("http://".$rowsCities['ct_uid'].".goyug.com"));

      $url_lastmod = $url->appendChild($xml->createElement('lastmod'));
      $url_lastmod->appendChild($xml->createTextNode(substr($date_current, 0, strpos($date_current, ' '))));

      $changefreq = $url->appendChild($xml->createElement('changefreq'));
      $changefreq->appendChild($xml->createTextNode("weekly"));

      $priority = $url->appendChild($xml->createElement('priority'));
      $priority->appendChild($xml->createTextNode("1"));
      //Карта поддомены - окончание

      //Города для основного
      $url_general = $root_general->appendChild($xml_general->createElement('url'));

      $url_loc_general = $url_general->appendChild($xml_general->createElement('loc'));
      $url_loc_general->appendChild($xml_general->createTextNode("http://".$rowsCities['ct_uid'].".goyug.com"));

      $url_lastmod_general = $url_general->appendChild($xml_general->createElement('lastmod'));
      $url_lastmod_general->appendChild($xml_general->createTextNode(substr($date_current, 0, strpos($date_current, ' '))));

      $changefreq_general = $url_general->appendChild($xml_general->createElement('changefreq'));
      $changefreq_general->appendChild($xml_general->createTextNode("weekly"));

      $priority_general = $url_general->appendChild($xml_general->createElement('priority'));
      $priority_general->appendChild($xml_general->createTextNode("1"));
      //Города для основного - окончание

      //Объекты
      $resultObjects = giveTable("SELECT obj_id, DATE_FORMAT(obj_create, '%Y-%m-%d') AS obj_create, DATE_FORMAT(obj_update, '%Y-%m-%d') AS obj_update FROM obj_object WHERE obj_addr_city_uid = '$rowsCities[ct_uid]' AND obj_enable ORDER BY obj_id DESC");
      while($rowsObjects = mysql_fetch_array($resultObjects)){

        //Дата модификации
        $lastmod = $rowsObjects['obj_update'];
        if($lastmod == '0000-00-00' || !$lastmod) $lastmod = $rowsObjects['obj_create'];
        if($lastmod == '0000-00-00' || !$lastmod) $lastmod = date("Y-m-d");

        $url = $root->appendChild($xml->createElement('url'));

        $url_loc = $url->appendChild($xml->createElement('loc'));
        $url_loc->appendChild($xml->createTextNode("http://".$rowsCities['ct_uid'].".goyug.com/$rowsObjects[obj_id]"));

        $url_lastmod = $url->appendChild($xml->createElement('lastmod'));
        $url_lastmod->appendChild($xml->createTextNode($lastmod));

        $changefreq = $url->appendChild($xml->createElement('changefreq'));
        $changefreq->appendChild($xml->createTextNode("monthly"));

        $priority = $url->appendChild($xml->createElement('priority'));
        $priority->appendChild($xml->createTextNode("1"));
      }//while
      //Объекты - окончание

      //Сохраняем файл
      $xml->formatOutput = true;
      $xml->save($file_path);
      ////Магия - окончание

      //Лог
      $log[] = 'Карта сайта для города '.$rowsCities['ct_name_ru'].' сгенерирована.';
    }//while

    //Сохраняем файл
    $xml_general->formatOutput = true;
    $xml_general->save($file_path_general);

  } else {
    //Лог
    $log[] = 'Нет городов для генерации карт сайта.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт генерации карты сайта</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт генерации карты сайта', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт генерации карты сайта</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в генерации карты сайта', $bodymail);
  }
?>
