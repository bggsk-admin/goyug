<?php //Снятие выделения поднятого объекта

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт снятия выделения поднятых объектов';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Выбираем оъекты
  $query = "SELECT obj_id, obj_u_id, obj_phantom, obj_ontop_deactivate, obj_ontop, obj_ontop_auto_update_count, obj_addr_city, obj_addr_city_uid, obj_addr_street, obj_addr_number FROM obj_object WHERE obj_ontop_mark AND !obj_phantom AND obj_ontop_deactivate <= '$date_current'";
  $result = giveTable($query);

  //Снимаем выделение
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){

      //Автоподнятие
      if($rows['obj_ontop_auto_update_count']) {

        //Выделено до
        $date_compound_a = explode(" ", $rows['obj_ontop_deactivate']);
        $date_date_a = explode("-", $date_compound_a[0]);
        $date_time_a = explode(":", $date_compound_a[1]);
        $date_next_ontop_deactivate = date("Y-m-d H:i:s", mktime( $date_time_a[0], $date_time_a[1], $date_time_a[2], $date_date_a[1], $date_date_a[2], $date_date_a[0] ) + (60*60*24)*3);

        //Текущее время
        $timestamp = mktime( $date_time_a[0], $date_time_a[1], $date_time_a[2], $date_date_a[1], $date_date_a[2], $date_date_a[0] );

        if(!goToTable("UPDATE obj_object SET obj_ontop = '$timestamp', obj_ontop_auto_update_count = (obj_ontop_auto_update_count - 1), obj_ontop_deactivate = '$date_next_ontop_deactivate' WHERE obj_id = $rows[obj_id]")){
          $errors[] = 'Ошибка автопродления поднятия объекта, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();
        }
        else {

          $log[] = 'Объект автоподнялся, obj_id: '.$rows['obj_id'];

          /////////////////////////
          ///Send E-mail///////////
          /////////////////////////

          //Достаем пользователя
          $query_u_user = "SELECT u_id, u_email, u_authtoken FROM u_user WHERE u_id = ".$rows['obj_u_id']." LIMIT 1";
          $result_u_user = giveTable($query_u_user);

          //Пользователь найден
          if(mysql_num_rows($result_u_user)){

            $row_u_user = mysql_fetch_assoc($result_u_user);

            //Прописываем однократный токен, если он не назначен еще
            if(!$row_u_user['u_authtoken']){
              $authtoken = md5($row_u_user['u_email'].$row_u_user['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $row_u_user[u_id]");
            } else {
              $authtoken = $row_u_user['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($row_u_user['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $row_u_user['u_email'];
              $email['subject'] = 'Автоподнятие вашего объекта';
              $email['body'] = '
              <h2>Уважаемый владелец!</h2>
              <p>Ваш объект по адресу: г. '.$rows['obj_addr_city'].', '.$rows['obj_addr_street'].' '.$rows['obj_addr_number'].' автоматически поднят на 1-е место в списке.</p>
              <p>Осталось автоподнятий: '.($rows['obj_ontop_auto_update_count'] - 1).'</p>
              <p>Объект выделен до: '.$date_next_ontop_deactivate.'</p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Автоподнятие объекта - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$rows['obj_u_id'].' не отправлено уведомление об автоподнятии объекта, т.е. у него не указан E-mail.</p>';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Автоподнятие объекта - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Автоподнятие объекта. Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
          /////////////////////////
          ///Send E-mail end///////
          /////////////////////////
        }
      }
      //Снятие выделения
      else {

        if(!goToTable("UPDATE obj_object SET obj_ontop_mark = 0, obj_photomark = 0 WHERE obj_id = $rows[obj_id]")){
          $errors[] = 'Ошибка снятия выделения объекта, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();
        }
        else {

          $log[] = 'С объекта снято выделение, obj_id: '.$rows['obj_id'];

          /////////////////////////
          ///Send E-mail///////////
          /////////////////////////

          //Достаем пользователя
          $query_u_user = "SELECT u_id, u_email, u_authtoken FROM u_user WHERE u_id = ".$rows['obj_u_id']." LIMIT 1";
          $result_u_user = giveTable($query_u_user);

          //Пользователь найден
          if(mysql_num_rows($result_u_user)){

            $row_u_user = mysql_fetch_assoc($result_u_user);

            //Прописываем однократный токен, если он не назначен еще
            if(!$row_u_user['u_authtoken']){
              $authtoken = md5($row_u_user['u_email'].$row_u_user['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $row_u_user[u_id]");
            } else {
              $authtoken = $row_u_user['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($row_u_user['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $row_u_user['u_email'];
              $email['subject'] = 'Срок выделения вашего объекта истек';
              $email['body'] = '
              <h2>Уважаемый владелец!</h2>
              <p>С вашего объекта по адресу: г. '.$rows['obj_addr_city'].', '.$rows['obj_addr_street'].' '.$rows['obj_addr_number'].' снято выделение.</p>
              <p>Объект был поднят: '.giveRealDataFull(date("Y-m-d H:i:s", $rows['obj_ontop'])).'</p>
              <p>Объект был выделен до: '.giveRealDataFull($rows['obj_ontop_deactivate']).'</p>
              <h3>Повторное выделение</h3>
              <p>Для того, чтобы повторно выделить объект в списке и на карте, необходимо выполнить поднятие объекта из вашего Личного Кабинета:</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/ontop_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'Снятие выделения поднятого объекта - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$rows['obj_u_id'].' не отправлено уведомление о снятии выделения поднятого объекта, т.е. у него не указан E-mail.</p>';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Снятие выделения поднятого объекта - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Снятие выделения объекта. Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
          /////////////////////////
          ///Send E-mail end///////
          /////////////////////////
        }
      }

    }//while
  } else {
    $log[] = 'Нет объектов для снятия выделения.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  echo "<h2>Скрипт снятия выделения объектов</h2>\n\r";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>'."\n\r";
  }
  //Отправка лога
  //myMail('marselos@gmail.com', 'Скрипт снятия выделения', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия выделения объектов</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения', $bodymail);
  }
?>
