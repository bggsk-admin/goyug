<?php //Предупреждение о списании за абонимент

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Стоимость услуги
  $rental_price = 100;

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Выбираем пользователей
  $query = "SELECT u.u_id, u.u_rental_nextpay, u.u_rental_deactivate, u.u_money, u.u_email, u.u_authtoken
            FROM u_user as u
            WHERE u_rental AND !u_rental_warning_3_day AND u_rental_deactivate <= NOW() + INTERVAL 3 DAY";
  $result = giveTable($query);

  //////////////////////////
  //Получаем пользователей//
  //////////////////////////

    $users = Array();

    if(mysql_num_rows($result)){

      while($rows = mysql_fetch_assoc($result)){

        //Пользователь умышленно отключил услугу - не трогаем его
        if($rows['u_rental_nextpay'] == '0000-00-00 00:00:00'){

        }
        //Пользователь не отключил услугу
        else if( $rows['u_rental_nextpay'] != '0000-00-00 00:00:00' ){
          $users[] = $rows;
        }

      }//while

    } else {
      $log[] = 'Нет объектов для напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня.';
    }

  //////////////////////////////////////
  //Получаем пользователей - окончание//
  //////////////////////////////////////

  //////////////
  //Отключение//
  //////////////

    foreach($users as $user){

      /////////////////////////
      ///Send E-mail///////////
      /////////////////////////

        //Пользователь найден
        if($user['u_id']){

          //Прописываем однократный токен, если он не назначен еще
          if(!$user['u_authtoken']){
            $authtoken = md5($user['u_email'].$user['u_id'].rand(1000, 9999));
            goToTable("UPDATE u_user SET `u_authtoken` = '".$authtoken."' WHERE u_id = ".$user['u_id']);
          } else {
            $authtoken = $user['u_authtoken'];
          }

          //Отмечаем отправку уведомления
          goToTable("UPDATE u_user SET `u_rental_warning_3_day` = 1 WHERE u_id = ".$user['u_id']);

          //Если указан E-mail - отправляем уведомление
          if($user['u_email']){
            //Письмо
            $email = Array();
            $email['to'] = $user['u_email'];
            $email['subject'] = 'GOYUG.COM - Срок действия вашего абонемента истекает через 3 дня';
            $email['body'] = '
            <h2>Goyug.com - квартиры посуточно</h2>
            <p>Уважаемый владелец!</p>
            <p>Просим вас, убедиться, что на вашем счете в личном кабинете имеется достаточно средств для продления месячного абонемента, на нашем ресурсе.</p>
            <p>Дата списания средств за абонплату: '.giveRealDataFull($user['u_rental_deactivate']).'</p>
            <p>Стоимость месячного абонемента - '.$rental_price.' рублей.</p>
            <p>Обращаем ваше внимание, что иначе ваши объявления будут недоступны для просмотра на сайте.</p>
            <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/rental_warning_3_day/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
          }
          //Если не указан E-mail - оптравляем оповещение
          else {
            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'Напоминание об услуге АБОНЕНТСКАЯ ПЛАТА - 3 дня - E-mail владельца не указан';
            $email['body'] = '
            <h2>Внимание!</h2>
            <p>Владельцу с ID '.$user['u_id'].' не отправлено уведомление о напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня, т.к. у него не указан E-mail.</p>';

            //Лог
            $errors[] = 'Владельцу с ID '.$user['u_id'].' не отправлено уведомление о напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня, т.к. у него не указан E-mail.';
          }
        }
        //Пользователь не найден
        else {

          //Письмо
          $email = Array();
          $email['to'] = 'info@goyug.com';
          $email['subject'] = 'Напоминание об услуге АБОНЕНТСКАЯ ПЛАТА - 3 дня - владелец не найден';
          $email['body'] = '
          <h2>Ахтунг!</h2>
          <p>Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

          //Лог
          $errors[] = 'Напоминание об услуге АБОНЕНТСКАЯ ПЛАТА - 3 дня. Владелец с ID '.$user['u_id'].' не был найден в базе сайта. Срочно все проверьте!';
        }

        //Отправка
        myMail($email['to'], $email['subject'], $email['body']);

      /////////////////////////
      ///Send E-mail end///////
      /////////////////////////

    }//foreach

  //////////////////////////
  //Отключение - окончание//
  //////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "";

  $bodymail = "<h2>Скрипт напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня</h2>\n\r";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>'."\n\r";
  }
  //Отправка лога
  // myMail('marselos@gmail.com, 160961@mail.ru', 'Скрипт напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт напоминание о списании за услугу АБОНЕНТСКАЯ ПЛАТА - 3 дня</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения АБОНЕНТСКАЯ ПЛАТА', $bodymail);
  }
?>
