<?php //Генерация статистики по объектам

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт снятия генерации статистики по городам';
  $log[] = 'Начало работы скрипта: '.$date_current;

  /////////////////
  //Бизнес-логика//
  /////////////////

  //Выбираем оъекты
  $query = "SELECT obj.obj_addr_city_uid AS city_uid, obj.obj_addr_city as city_name, count(obj.obj_id) as object_count, obj_addr_lat as object_lat, obj_addr_lon as object_lon, max(obj.obj_price) as maxprice, min(obj.obj_price) as minprice
            FROM obj_object as obj
            WHERE obj.obj_addr_city_uid != '' AND obj.obj_enable
            GROUP BY obj.obj_addr_city_uid
            ORDER BY object_count DESC";

  $result = giveTable($query);

  //Формирование массива данных
  if(mysql_num_rows($result) > 0){

    while($city_stat = mysql_fetch_assoc($result)){

      //print_r($city_stat);

      $query_update_ct = "UPDATE ct_city
                          SET ct_objects = ".$city_stat['object_count'].",
                          ct_minprice = '".$city_stat['minprice']."',
                          ct_maxprice = '".$city_stat['maxprice']."',
                          ct_lat = '".round($city_stat['object_lat'], 6)."',
                          ct_lng = '".round($city_stat['object_lon'], 6)."'
                          WHERE ct_uid = '".$city_stat['city_uid']."'";
      //echo $query_update_ct.'<br />';
      if(!goToTable($query_update_ct)){
        $errors[] = 'Ошибка генерации статистики по городам<br /> '.mysql_error();
      } else {
        $log[] = 'Обновлен город: '.$city_stat['city_uid'];
      }
    }//while
  }

    //----------------------------------------------------------------
    // Исправляем названия городов
    //----------------------------------------------------------------

    // Железноводск
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'jeleznovodsk', 'zheleznovodsk')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'jeleznovodsk'");

    // Астрахань
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'astrahan', 'astrakhan')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'astrahan'");

    // Сочи
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'bolshoysochi', 'sochi')");
    goToTable("UPDATE obj_object SET obj_addr_city = REPLACE(obj_addr_city, 'Большой Сочи', 'Сочи')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'bolshoysochi'");

    // Воронеж
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'voronej', 'voronezh')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'voronej'");

    // Усть-Лабинск
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'ustjlabinsk', 'ustlabinsk')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'ustjlabinsk'");

    // Нижний Новгород
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'nijniynovgorod', 'nnovgorod')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'nijniynovgorod'");

    // Санкт-Петербург
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'sanktpeterburg', 'spb')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'sanktpeterburg'");

    // Москва
    goToTable("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'moskva', 'moscow')");
    goToTable("DELETE FROM ct_city WHERE ct_uid = 'moskva'");


  /////////////////////////////
  //Бизнес-логика - окончание//
  /////////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.$date_current;

  /*
  ////Оповещение каждого выполнения
  print_r($log);
  //Сериализация лога
  $bodymail = "<h2>Скрипт снятия генерации статистики по городам</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт генерации статистики по городам', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия генерации статистики по городам</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка генерации статистики по городам', $bodymail);
  }
?>