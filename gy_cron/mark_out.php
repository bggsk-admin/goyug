<?php //Проверка выделения на карте

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт снятия выделения объектов';
  $log[] = 'Начало работы скрипта: '.$date_current;

  //Выбираем просроченные метки
  $query = "SELECT obj_id FROM obj_object WHERE obj_mark_out AND obj_mark_out_deactivate <= '$date_current'";
  $result = giveTable($query);

  //Снимаем выделение
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      if(!goToTable("UPDATE obj_object SET obj_mark_out = 0 WHERE obj_id = $rows[obj_id]")){
        //$log[] = 'Ошибка снятия выделения объекта, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();
        $errors[] = 'Ошибка снятия выделения объекта, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();
      }
      else
        $log[] = 'С объекта снято выделение, obj_id: '.$rows['obj_id'];
    }//while
  } else {
    $log[] = 'Нет объектов для снятия выделения.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.$date_current;

  /*
  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт снятия выделения объектов</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт снятия выделения', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия выделения объектов</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения', $bodymail);
  }
?>
