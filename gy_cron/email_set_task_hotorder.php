<?php //Рассылка письма

  //Библиотека
  include('cron_lib.php');

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылки письма';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //uid писем
  $uids = Array();

  //Регистрация рассылки
  $eso = Array();

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $path = "../gy_cron";
  }
  //На сервере
  else {
    $path = "/home/c/c74542/goyug.com/gy_cron";
  }

  //Проверяем создавалась ли уже рассылка
  if(!file_exists($path."/email_set_task_hotorder.txt")){

    //////////////////////////////////////////
    ///Рассылка для всех владельцев///////////
    //////////////////////////////////////////

      //////////////////////
      //Находим владельцев//
      //////////////////////

        //Список рассылки
        $users = Array();

        //Лист отправки - все владельцы
        $query = "SELECT DISTINCT(obj_u_id), obj_object.obj_addr_city_uid as city_uid, u_user.u_id as id, LCASE(u_user.u_email) as email, trim(concat(upper(mid(u_user.u_firstname,1,1)), mid(u_user.u_firstname,2))) as name, u_user.u_phone_1 as login, u_user.u_authtoken as authtoken, u_user.u_password as password
                  FROM `obj_object`
                  LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
                  WHERE obj_object.obj_enable AND u_user.u_email != '' AND obj_object.obj_hotorder
                  ORDER BY obj_object.obj_addr_city_uid";
        $result = giveTable($query);

        if(mysql_num_rows($result)){
          while($rows = mysql_fetch_assoc($result)){
            unset($rows['obj_u_id']);
            $users[] = $rows;
          }//while
        }

        //echo '<pre>';
        //print_r($users);
        //echo '</pre>';

        $log[] = 'Найдено '.count($users). ' адресатов';

      //////////////////////////////////
      //Находим владельцев - окончание//
      //////////////////////////////////

      ///////////////////////////////////
      //Формируем индивидуальные письма//
      ///////////////////////////////////

        //Исключенные адреса
        $emails_restric = Array();

        //Идентификатор рассылки
        $eso_uid = md5('hotorder'.count($users).date("Y-m-d"));

        foreach ($users as $key => $user) {

          //Пропускаем исключенные из рассылки E-mail
          if(in_array($user['email'], $emails_restric)){
            $log[] = 'Исключение из рассылки: '.$user['email'];
            continue;
          }

          //Одноразовый токен
          $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));
          //Создаем, если он еще не назначен
          if($user['authtoken'] == ''){
            goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
            $user['authtoken'] = $authtoken;
          }

          //Таблица celt_cron_email_list_tasks
          $celt = Array();

          //Идентификатор письма
          $celt['celt_uid'] = md5(microtime().rand(0, 1000));
          $uids[] = $celt['celt_uid'];

          //Время создания
          $celt['celt_date_create'] = date("Y-m-d H:i:s");

          //Письмо
          $celt['celt_email_subject'] = 'Функция "Срочное Заселение" доступна в вашем Личном Кабинете';
          $celt['celt_email_body'] = '
          <!DOCTYPE html>
          <html lang="ru">
          <head>
              <meta charset="UTF-8">
              <title>Функция "Срочное Заселение" доступна в вашем Личном Кабинете!</title>
          </head>
          <body>

          <h3>Здравствуйте, '.$user['name'].'!</h3>
          <p>У нас появилась новая функция - "Срочное Заселение". На данный момент, функиция является совершенно БЕСПЛАТНОЙ.</p>

          <h3>Как включить?</h3>
          <p>Для использования этой функции нажмите кнопку "Включить «Срочное заселение»" напротив вашего объекта в личном кабинете.</p>

          <h3>Как это работает?</h3>
          <p>Гость, который подыскивает срочный вариант, отправляет запрос на срочное заселение с карты вашего города.</p>
          <p>Если ваш объект помечен как доступный для срочного заселения, и отвечает параметрам заданными гостем в запросе, то вы получите запрос на почту и сможете моментально связаться с гостем, предложив ему свой вариант.</p>

          <h3>Чем это отличается от статуса "Свободна сегодня"?</h3>
          <p>Установив статус "Срочное Заселение", вы соглашаетесь принять гостя в любое время суток.</p>
          <p>Мы заранее активировали эту функцию для ваших объектов. Если вы хотите отключить ее:</p>
          <ol>
            <li>Зайдите в свой личный кабинет.</li>
            <li>Откройте список объектов.</li>
            <li>Нажмите кнопку "Отключить «Срочное Заселение»" напротив нужного объекта (или всех).</li>
          </ol>

          <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/hotorder/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a>

          <p>Искренне ваша команда GOYUG.COM</p>
          <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">
          </body>
          </html>';

          //echo '<pre>';
          //print_r($celt);
          //echo '</pre>';

          //Запись в базу
          $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

          //Адресаты
          $cel = Array();
          $cel['cel_celt_uid'] = $celt['celt_uid'];
          $cel['cel_email'] = $user['email'];
          $cel['cel_date_stage'] = date("Y-m-d H:i:s");

          //Запись в базу
          $result_cel = insertToTable('cel_cron_email_list', $cel);

          //echo '<pre>';
          //print_r($cel);
          //echo '</pre>';

          //Останов в случае ошибки
          if(!$result_celt || !$result_cel){
            die(mysql_error());
          }
        }//foreach

        //Для избежания повторной генерации писем создаем файл
        $text = "Задача поставлена для ".count($users)." адресатов.\n\r";
        $text .= "Из них исключено из рассылки ".count($emails_restric)." адресатов.\n\r";
        foreach ($uids as $key => $uid) {
          $text .= $uid."\n\r";
        }
        file_put_contents($path."/email_set_task_hotorder.txt", $text);

      ///////////////////////////////////////////////
      //Формируем индивидуальные письма - окончание//
      ///////////////////////////////////////////////

      ////////////////////////
      //Регистрация рассылки//
      ////////////////////////
      $eso['eso_uid'] = $eso_uid;
      $eso['eso_mod'] = 'hotorder';
      $eso['eso_count'] = count($users);
      $eso['eso_date'] = date("Y-m-d");
      $eso['eso_desc'] = 'Рассылка по функции СРОЧНОЕ ЗАСЕЛЕНИЕ';
      if(!insertToTable('eso_email_stat_opens', $eso)){
        myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
      }
      ////////////////////////////////////
      //Регистрация рассылки - окончание//
      ////////////////////////////////////

    //////////////////////////////////////////////////////
    ///Рассылка для всех владельцев - окончание///////////
    //////////////////////////////////////////////////////
  } else {
    $log[] = 'Рассылка уже назначена';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылки письма</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
  }
?>
