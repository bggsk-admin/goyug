<?php
//Рассылка письма

//Библиотека
include('cron_lib.php');

//Лог
$log = Array();
$errors = Array();
$log[] = 'Вас приветствует скрипт рассылки письма';
$log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

//uid писем
$uids = Array();

//Регистрация рассылки
$eso = Array();

//Путь к папке с файлами карт сайта
//Локально
if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
  $path = "../gy_cron";
}
//На сервере
else {
  $path = "/var/www/goyug/gy_cron";
}

//////////////////////////////////////////
/// ВНОСИТЬ ИЗМЕНЕНИЯ ТУТ      ///////////
//////////////////////////////////////////

// ID рассылки
$src_mod = "springbonus";
$eso_desc = "Весенняя акция для городов-абонентов 2";

// Расписание рассылки с кол-вом имейлов
// [день_месяца] => [OFFSET в базе]
$query_offset_limit = 1400;

$query_offset_shedule = array(
  '24' => 0,
  '25' => $query_offset_limit,
  '26' => $query_offset_limit * 2
);
$cur_day = date('j');
$query_offset = $query_offset_shedule[$cur_day];


//Проверяем создавалась ли уже рассылка
if(array_key_exists ( $cur_day , $query_offset_shedule )){
// if(!file_exists($path."/email_set_task_ontopsale.txt")){

  //////////////////////////////////////////
  ///Рассылка для всех владельцев///////////
  //////////////////////////////////////////

  //////////////////////
  //Находим владельцев//
  //////////////////////

  //Список рассылки
  $users = Array();

  //Лист отправки - все владельцы
  // Если надо разослать всем кроме виртуалов
  // AND (u.u_notes != 'virtrental' OR ISNULL(u.u_notes) OR u.u_notes = '')

  $query = "
SELECT
  obj_u_id, 
  obj.obj_addr_city_uid AS city_uid, 
  u.u_id AS id,
  LCASE(u.u_email) as email, 
  TRIM(CONCAT(UPPER(MID(u.u_firstname,1,1)), MID(u.u_firstname,2))) AS name, 
  u.u_authtoken AS authtoken, 
  u.u_money

FROM u_user u

LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
ON obj.obj_u_id = u.u_id

WHERE ((u_rental_deactivate < DATE_SUB(NOW(), INTERVAL 2 MONTH) OR ISNULL(u_rental_deactivate))
OR obj.obj_addr_city_uid IN ('surgut', 'lipetsk', 'ulyanovsk', 'kemerovo', 'kursk'))
AND (u.u_email != '' AND NOT ISNULL(u.u_email) AND u.u_email LIKE '%@%.%')

LIMIT ".$query_offset_limit." OFFSET " . $query_offset;

  //echo $query;
  $result = giveTable($query);

  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){
      unset($rows['obj_u_id']);
      $users[] = $rows;
    }//while
  }

  $log[] = 'Найдено '.count($users). ' адресатов';

  //////////////////////////////////
  //Находим владельцев - окончание//
  //////////////////////////////////

  ///////////////////////////////////
  //Формируем индивидуальные письма//
  ///////////////////////////////////

  //Идентификатор рассылки
  $eso_uid = md5($src_mod.count($users).date("Y-m-d"));

  foreach ($users as $key => $user) 
  {

    //Одноразовый токен
    $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));

    //Создаем, если он еще не назначен
    if($user['authtoken'] == '')
    {
      goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
      $user['authtoken'] = $authtoken;
    }

    //Таблица celt_cron_email_list_tasks
    $celt = Array();

    //Идентификатор письма
    $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
    $uids[] = $celt['celt_uid'];

    //Время создания
    $celt['celt_date_create'] = date("Y-m-d H:i:s");

    //Письмо
    $celt['celt_email_subject'] = 'GOYUG || Весенние Бонусы!';
          
    $celt['celt_email_body'] =
    '<!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>'.$celt['celt_email_subject'].'</title>
    </head>
    <body>

      <h2>Goyug.com - квартиры посуточно</h2>

      <p>Уважаемый Владелец!</p>
      <p>При оплате абонемента в течении апреля и мая мы подготовили для вас <strong>ВЕСЕННЕЕ ПРЕДЛОЖЕНИЕ</strong> и приятный <strong>БОНУС!</strong></p>

      <p>Оплатите абонемент на 1, 2 или 3 месяца и мы поднимем один из ваших объектов соответственно на 30, 60 или 90 дней <strong>БЕСПЛАТНО.</strong></p>

      <p>Для того чтобы получить поднятие объекта в подарок, зайдите в свой личный кабинет по ссылке ниже, пополните свой личный счет и нажмите кнопку "Подключить абонемент".</p>
      <p>После этого пришлите нам ID своего объекта, который вы хотите поднять, через форму обратной связи в личном кабинете (ID можно узнать на странице со списком ваших объектов)</p>

      <p>... и получите бонус за продление абонемента:</p>

      <ul>
        <li>Продление абонемента на 1 месяц (100 рублей) = поднятие объекта на 30 дней бесплатно (Бонус 220 рублей)</li>
        <li>--\\-- 2 месяца = поднятие на 60 дней бесплатно (Бонус 420 рублей)</li>
        <li>--\\-- 3 месяца = поднятие на 90 дней бесплатно (Бонус 620 рублей)</li>
      </ul>

      <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/'.$src_mod.'/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Перейти в Личный Кабинет</a>

      <p><br></p>
      <p>Благодарим за то что Вы с нами. Мы рады оказать свою помощь в Вашем нелегком бизнесе!</p>

      <p>
        --<br />
        С уважением,<br />
        команда GOYUG.COM
      </p>

      <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">

    </body>
    </html>';

    // ************* Запись в базу *********************************
    $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

    //Адресаты
    $cel = Array();
    $cel['cel_celt_uid'] = $celt['celt_uid'];
    $cel['cel_email'] = $user['email'];
    $cel['cel_reply'] = 'info@goyug.com';
    $cel['cel_date_stage'] = date("Y-m-d H:i:s");

    // ************* Запись в базу *********************************
    $result_cel = insertToTable('cel_cron_email_list', $cel);

    // Останов в случае ошибки
    if(!$result_celt || !$result_cel){
      die(mysql_error());
    }

  }
  //foreach

  //Для избежания повторной генерации писем создаем файл
  $text = "Задача поставлена для ".count($users)." адресатов.\n\r";

  foreach ($uids as $key => $uid) {
    $text .= $uid."\n\r";
  }

  file_put_contents($path."/email_set_task_springbonus.txt", $text);

  ///////////////////////////////////////////////
  //Формируем индивидуальные письма - окончание//
  ///////////////////////////////////////////////

  ////////////////////////
  //Регистрация рассылки//
  ////////////////////////
  $eso['eso_uid'] = $eso_uid;
  $eso['eso_mod'] = $src_mod;
  $eso['eso_count'] = count($users);
  $eso['eso_date'] = date("Y-m-d");
  $eso['eso_desc'] = $eso_desc;
  
  // ************* Запись в базу *********************************
  if(!insertToTable('eso_email_stat_opens', $eso))
  {
    myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
  }

} else {
  $log[] = 'Рассылка уже назначена';
}
////////////////////////////////////
//Регистрация рассылки - окончание//
////////////////////////////////////

//////////////////////////////////////////////////////
///Рассылка для всех владельцев - окончание///////////
//////////////////////////////////////////////////////

//Окончание работы
$log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

////Оповещение каждого выполнения
//Сериализация лога
$bodymail = "<h2>Скрипт рассылки письма</h2>";
foreach ($log as $value) {
  $bodymail .= '<p>'.$value.'</p>';
}

//Отправка лога
usleep(500000);
myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

////Оповещение только в случае ошибки
if(count($errors)){
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($errors as $error) {
    $bodymail .= '<p>'.$error.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
}

?>