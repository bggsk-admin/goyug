<?php //Снятие выделения ВЫДЕЛИТЬ ОБЪЕКТ

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт снятия выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //Выбираем оъекты
  $query = "SELECT obj_id, obj_u_id, obj_phantom, obj_photomark_deactivate, obj_addr_city, obj_addr_city_uid, obj_addr_street, obj_addr_number FROM obj_object WHERE obj_photomark AND !obj_phantom AND obj_photomark_deactivate <= '$date_current'";
  $result = giveTable($query);

  //Снимаем выделение
  if(mysql_num_rows($result)){
    while($rows = mysql_fetch_assoc($result)){

      if(!goToTable("UPDATE obj_object SET obj_photomark = 0 WHERE obj_id = $rows[obj_id]")){

        //$log[] = 'Ошибка снятия выделения объекта ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();
        $errors[] = 'Ошибка снятия выделения объекта ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ, obj_id: '.$rows['obj_id'].'<br />'.mysql_error();

      }
      else {

        $log[] = 'С объекта снято выделение ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ, obj_id: '.$rows['obj_id'];

        /////////////////////////
        ///Send E-mail///////////
        /////////////////////////
        /*
          //Достаем пользователя
          $query_u_user = "SELECT u_id, u_email, u_authtoken FROM u_user WHERE u_id = ".$rows['obj_u_id']." LIMIT 1";
          $result_u_user = giveTable($query_u_user);

          //Пользователь найден
          if(mysql_num_rows($result_u_user)){

            $row_u_user = mysql_fetch_assoc($result_u_user);

            //Прописываем однократный токен, если он не назначен еще
            if(!$row_u_user['u_authtoken']){
              $authtoken = md5($row_u_user['u_email'].$row_u_user['u_id'].rand(1000, 9999));
              goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $row_u_user[u_id]");
            } else {
              $authtoken = $row_u_user['u_authtoken'];
            }

            //Если указан E-mail - отправляем уведомление
            if($row_u_user['u_email']){
              //Письмо
              $email = Array();
              $email['to'] = $row_u_user['u_email'];
              $email['subject'] = 'GOYUG.COM | Статус объекта «ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ» деактивирован';
              $email['body'] = '
              <h2>Уважаемый владелец!</h2>
              <p>Статус вашего объекта «ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ» по адресу: г. '.$rows['obj_addr_city'].', '.$rows['obj_addr_street'].' '.$rows['obj_addr_number'].' деактивирован.</p>
              <h3>Повторное выделение</h3>
              <p>Для того, чтобы повторно указать статус «ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ», необходимо зайти в ваш Личный Кабинет и повторно активировать статус.</p>
              <p>Статус «ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ» активен <strong>3 дня</strong> с момента активации.</p>
              <p><a href="http://goyug.com/user/authtoken/token/'.$authtoken.'/src_type/email/src_mod/photomark_demark/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>';
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'GOYUG.COM | Снятие выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$rows['obj_u_id'].' не отправлено уведомление о снятии выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ, т.е. у него не указан E-mail.</p>';
            }
          }
          //Пользователь не найден
          else {

            //Письмо
            $email = Array();
            $email['to'] = 'info@goyug.com';
            $email['subject'] = 'GOYUG.COM | Снятие выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ - владелец не найден';
            $email['body'] = '
            <h2>Ахтунг!</h2>
            <p>Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!</p>';

            //Лог
            $errors[] = 'Снятие выделения объекта ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ. Владелец с ID '.$rows['obj_u_id'].' не был найден в базе сайта. Срочно все проверьте!';
          }

          //Отправка
          myMail($email['to'], $email['subject'], $email['body']);
        */
        /////////////////////////
        ///Send E-mail end///////
        /////////////////////////
      }
    }//while
  } else {
    $log[] = 'Нет объектов для снятия выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ.';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  /*
  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт снятия выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ объектов</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }

  //Отправка лога
  myMail('marselos@gmail.com', 'Скрипт снятия выделения', $bodymail);
  */

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ объектов</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка в снятии выделения ВЫДЕЛИТЬ ОБЪЕКТ РАМКОЙ', $bodymail);
  }
?>
