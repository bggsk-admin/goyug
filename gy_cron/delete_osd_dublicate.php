<?php //Удаление дублей в таблице ежедневных просмотров

  //Библиотека
  include('cron_lib.php');

  $time_start = time();

  //Объекты к рассмотрению
  $objects = Array();

  //Выбираем все уникальные значения
  $query_all_objects = "SELECT DISTINCT osd_obj_id as obj_id FROM osd_objstat_daily WHERE 1 ORDER BY osd_obj_id";
  $result_all_objects = giveTable($query_all_objects);
  if(mysql_num_rows($result_all_objects)){
    while($object = mysql_fetch_assoc($result_all_objects)){
      $objects[] = $object;
    }//while
  }

  //Выбираем даты и просмотры по убыванию
  foreach ($objects as $object_key => $object) {
    $query = "SELECT osd_id, osd_date, osd_count FROM osd_objstat_daily WHERE osd_obj_id = $object[obj_id] GROUP BY osd_date ORDER BY osd_date DESC, osd_count DESC";
    $result = giveTable($query);
    if(mysql_num_rows($result)){
      $row = 0;
      while($osd_row = mysql_fetch_assoc($result)){
        $objects[$object_key]['dates_count'][$row]['osd_id'] = $osd_row['osd_id'];
        $objects[$object_key]['dates_count'][$row]['osd_date'] = $osd_row['osd_date'];
        $objects[$object_key]['dates_count'][$row]['osd_count'] = $osd_row['osd_count'];
        $row++;
      }
    }

  }//foreach

  //Удаляем лишние записи
  foreach ($objects as $object_key => $object) {
    //Проход по каждой дате
    foreach ($object['dates_count'] as $dates_count_key => $dates_count) {
      $query = "DELETE FROM osd_objstat_daily WHERE osd_obj_id = $object[obj_id] AND osd_date = '$dates_count[osd_date]' AND osd_id != $dates_count[osd_id]";
      if(!goToTable($query)){
        die("Ошибка <br />".mysql_error());
      }
    }
  }

  $time_end = time();
?>

<h2>
  <?="Уникализировано: ".count($objects)." объектов за ".($time_end - $time_start)." секунд"?>
</h2>

<pre>
  <?=print_r($objects)?>
</pre>