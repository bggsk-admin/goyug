<?php //Рассылка письма

  /*
    /////////////////////////////////////////////////////////////////////////////////////////////////

    ~ Для запуска акции по новому городу необходимо:

    * Ключи для поиска и замены:
    * astrakhan (UID города)
    * action_rub_1000_ast (Тип акции, action_rub_1000_ХХХ, где ХХХ - сокращение города)
    * 17 ноября (дата окончания акции)
    * 2016-11-17 (дата окончания акции)
    * Астрахань (название города)

    * Изменить дату окончания акции в двух методах модели User - checkValidLogin, authToken
    * Удалить блок-файл email_set_task_actions.txt

    /////////////////////////////////////////////////////////////////////////////////////////////////
  */

  //Библиотека
  include('cron_lib.php');

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт рассылки письма';
  $log[] = 'Начало работы скрипта: '.date("Y-m-d H:i:s");

  //uid писем
  $uids = Array();

  //Регистрация рассылки
  $eso = Array();

  //Путь к папке с файлами карт сайта
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $path = "../gy_cron";
  }
  //На сервере
  else {
    $path = "/var/www/goyug/gy_cron";
  }

  //Проверяем создавалась ли уже рассылка
  if(!file_exists($path."/email_set_task_actions.txt")){

    //////////////////////////////////////////
    ///Рассылка для всех владельцев///////////
    //////////////////////////////////////////

      //////////////////////
      //Находим владельцев//
      //////////////////////

        //Список рассылки
        $users = Array();

        //Лист отправки - все владельцы
        $query = "SELECT DISTINCT(obj_u_id), obj_object.obj_addr_city_uid as city_uid, u_user.u_id as id, LCASE(u_user.u_email) as email, trim(concat(upper(mid(u_user.u_firstname,1,1)), mid(u_user.u_firstname,2))) as name, u_user.u_authtoken as authtoken, u_user.u_money
                  FROM `obj_object`
                  LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
                  WHERE
                    obj_object.obj_enable AND
                    u_user.u_email != '' AND
                    (
                      obj_object.obj_addr_city_uid = 'astrakhan'
                    ) AND
                    u_user.u_money = 0 AND
                    obj_object.obj_u_id NOT IN (
                      SELECT cf_u_id FROM cf_cashflow WHERE cf_transaction != 'ontop_dummy' AND cf_mode = 'balanceoff' GROUP BY cf_u_id ORDER BY cf_u_id
                    )
                  ORDER BY obj_object.obj_addr_city_uid";
        //echo $query;
        $result = giveTable($query);

        if(mysql_num_rows($result)){
          while($rows = mysql_fetch_assoc($result)){
            unset($rows['obj_u_id']);
            $users[] = $rows;
          }//while
        }

        $log[] = 'Найдено '.count($users). ' адресатов';

        echo '<pre>';
        print_r($users);
        echo '</pre>';

      //////////////////////////////////
      //Находим владельцев - окончание//
      //////////////////////////////////

      ///////////////////////////////////
      //Формируем индивидуальные письма//
      ///////////////////////////////////

        //Исключенные адреса
        $emails_restric = Array();

        //Идентификатор рассылки
        $eso_uid = md5('action_rub_1000_ast'.count($users).date("Y-m-d"));

        foreach ($users as $key => $user) {

          //Пропускаем исключенные из рассылки E-mail
          if(in_array($user['email'], $emails_restric)){
            $log[] = 'Исключение из рассылки: '.$user['email'];
            continue;
          }

          //Одноразовый токен
          $authtoken = md5($user['email'].$user['id'].rand(1000, 9999));
          //Создаем, если он еще не назначен
          if($user['authtoken'] == ''){
            goToTable("UPDATE u_user SET `u_authtoken` = '$authtoken' WHERE u_id = $user[id]");
            $user['authtoken'] = $authtoken;
          }

          //Таблица celt_cron_email_list_tasks
          $celt = Array();

          //Идентификатор письма
          $celt['celt_uid'] = md5(microtime().rand(0, 1000000));
          $uids[] = $celt['celt_uid'];

          //Время создания
          $celt['celt_date_create'] = date("Y-m-d H:i:s");

          //Письмо
          $celt['celt_email_subject'] = 'Внимание! Вам активированы 1000 рублей.';
          $celt['celt_email_body'] = '
          <!DOCTYPE html>
          <html lang="ru">
          <head>
              <meta charset="UTF-8">
              <title>'.$celt['celt_email_subject'].'</title>
          </head>
          <body>

          <p>Уважаемый, Владелец!</p>

          <p>Мы активировали Вам 1000 рублей в личном кабинете. Вы можете провести свой личный "Тест Драйв" сайта по посуточной аренде Goyug.com и воспользоваться любыми услугами ресурса.</p>

          <p>Потратить бонусные средства необходимо до 17 ноября. Для их активации просто перейдите по ссылке:</p>
          <a href="http://goyug.com/user/authtoken/token/'.$user['authtoken'].'/src_type/email/src_mod/action_rub_1000_ast/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Активировать бонус</a>

          <p>
            --<br />
            С уважением,<br />
            команда Goyug.com
          </p>
          <img src="http://goyug.com/stat/emailimg/uid/'.$eso_uid.'/u_id/'.$user['id'].'/">
          </body>
          </html>';

          //echo '<pre>';
          //print_r($celt);
          //echo '</pre>';

          //Запись в базу
          $result_celt = insertToTable('celt_cron_email_list_tasks', $celt);

          //Адресаты
          $cel = Array();
          $cel['cel_celt_uid'] = $celt['celt_uid'];
          $cel['cel_email'] = $user['email'];
          $cel['cel_date_stage'] = date("Y-m-d H:i:s");

          //Запись в базу
          $result_cel = insertToTable('cel_cron_email_list', $cel);

          //echo '<pre>';
          //print_r($cel);
          //echo '</pre>';

          //Останов в случае ошибки
          if(!$result_celt || !$result_cel){
            die(mysql_error());
          }

          //Прописываем акцию пользователю
          $action_stage = date("Y-m-d H:i:s");
          //$action_actual_to = date("Y-m-d H:i:s", time() + 3*(60*60*24));
          $action_actual_to = date("2016-11-17 23:59:59");
          goToTable("UPDATE u_user SET `u_action_enable` = 1, `u_action_type` = 'action_rub_1000_ast', `u_action_stage` = '$action_stage', `u_action_actual_to` = '$action_actual_to' WHERE u_id = $user[id]");

        }//foreach

        //Для избежания повторной генерации писем создаем файл
        $text = "Задача поставлена для ".count($users)." адресатов.\n\r";
        $text .= "Из них исключено из рассылки ".count($emails_restric)." адресатов.\n\r";
        foreach ($uids as $key => $uid) {
          $text .= $uid."\n\r";
        }
        file_put_contents($path."/email_set_task_actions.txt", $text);

      ///////////////////////////////////////////////
      //Формируем индивидуальные письма - окончание//
      ///////////////////////////////////////////////

      ////////////////////////
      //Регистрация рассылки//
      ////////////////////////
        $eso['eso_uid'] = $eso_uid;
        $eso['eso_mod'] = 'action_rub_1000_ast';
        $eso['eso_count'] = count($users);
        $eso['eso_date'] = date("Y-m-d");
        $eso['eso_desc'] = 'ПОЛУЧИ 1000 РУБЛЕЙ - город Астрахань';
        if(!insertToTable('eso_email_stat_opens', $eso)){
          myMail('marselos@gmail.com', 'Рассылка не зарегистрирована', 'Рассылка '.$eso['eso_mod'].'<br />Ошибка: '.mysql_error());
        }
      ////////////////////////////////////
      //Регистрация рассылки - окончание//
      ////////////////////////////////////

    //////////////////////////////////////////////////////
    ///Рассылка для всех владельцев - окончание///////////
    //////////////////////////////////////////////////////

  } else {
    $log[] = 'Рассылка уже назначена';
  }

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт рассылки письма</h2>";
  foreach ($log as $value) {
    $bodymail .= '<p>'.$value.'</p>';
  }
  //Отправка лога
  usleep(500000);
  myMail('marselos@gmail.com', 'Скрипт рассылки письма', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт рассылки письма</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }
    //Отправка лога
    usleep(500000);
    myMail('marselos@gmail.com', 'Ошибка в рассылке письма', $bodymail);
  }
?>
