<?php //Генерация JSON-объектов городов

  //Библиотека
  include('cron_lib.php');

  //Текущая дата
  $date_current = date("Y-m-d H:i:s");

  //Лог
  $log = Array();
  $errors = Array();
  $log[] = 'Вас приветствует скрипт снятия генерации GeoJSON';
  $log[] = 'Начало работы скрипта: '.$date_current;

  // Города с АБОНПЛАТОЙ
  $rental_cities = array(
    "kislovodsk", 
    "nnovgorod", 
    "sochi", "sevastopol", "spb", 
    "krasnodar", "saratov", "tyumen", 
    "novosibirsk", "ekaterinburg", "perm", 
    "kazan", "ufa", "voronezh", 
    "moscow", "omsk", "rostovnadonu", "volgograd",
    "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
    "zheleznovodsk",
    "pyatigorsk", "yalta", "yaroslavl", "tomsk",
    "bryansk", "tula", "magnitogorsk", "ivanovo",
    "khabarovsk", "vladimir", "penza", "kirov", "vologda",
    "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
    "astrakhan", "tolyatty", "orel", "orenburg", "tver",
    "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
  );

  /////////////////
  //Бизнес-логика//
  /////////////////

  ////Путь к папке с файлами
  //Локально
  if($_SERVER['HTTP_HOST'] == 'goyug-cron'){
    $venue_path = '../public_html/data/venue/';
  }
  //На сервере
  else {
    $venue_path = '/var/www/goyug/public_html/data/venue/';
  }

  //Выбираем оъекты
  $query = "SELECT *, u.u_rental
            FROM obj_object AS obj

            LEFT JOIN u_user AS u
            ON u.u_id = obj.obj_u_id

            LEFT JOIN per_period AS per
            ON per.per_id = obj.obj_price_period

            LEFT JOIN v_value AS v
            ON v.obj_id = obj.obj_id

            LEFT JOIN ot_object_type AS ot
            ON ot.ot_id = v.v_key

            LEFT JOIN acc_accommodation AS acc
            ON acc.acc_id = obj.obj_acc_type

            WHERE obj.obj_enable = 1
            AND v.v_table = 'ot_object_type'
            AND obj.obj_enable = 1

            ORDER BY obj_ontop DESC, obj_price";

  $result = giveTable($query);

  $geojson_arr = Array();
  $venues_arr = Array();

  //Формирование массива данных
  if(mysql_num_rows($result) > 0){

    while($object = mysql_fetch_assoc($result)){
      //print_r($object);

      //Абонимент
      if( in_array($object['obj_addr_city_uid'], $rental_cities) && !$object['u_rental'] ) continue;
      else unset($object['u_rental']);

      $images = getObjectImages($object['obj_id']);

      $venues_arr[$object['obj_addr_city_uid']][] = Array(
        "type" => "Feature",
        "geometry" => Array(
          "type" => "Point",
          "coordinates" => Array(
            round($object['obj_addr_lon'], 6),
            round($object['obj_addr_lat'], 6)
          )
        ),
        "properties" => Array(
          "index" => "",
          "id" => $object['obj_id'],
          "uid" => $object['obj_uid'],
          "title" => $object['obj_name_ru'],
          "ontop" => $object['obj_ontop'],
          "ontop_mark" => $object['obj_ontop_mark'],
          "isempty" => $object['obj_isempty'],
          "photomark" => $object['obj_photomark'],
          "price" => $object['obj_price'],
          "rooms" => $object['obj_rooms'],
          "guests" => $object['obj_guests'],
          "sqr" => $object['obj_sqr'],
          "rating" => $object['obj_rating'],
          "checkin" => $object['obj_checkin'],
          "checkout" => $object['obj_checkout'],
          "addr_street" => $object['obj_addr_street'],
          "addr_number" => $object['obj_addr_number'],
          "addr_floor" => ($object['obj_addr_floor'] > 0) ? $object['obj_addr_floor'] : "",
          "acc_name" => $object['acc_name_ru'],
          "minstay" => $object['obj_price_minstay'],
          "ot_name" => $object['ot_name_ru'],
          "ot_type" => $object['ot_uid'],
          "link" => 'http://' . $object['obj_addr_city_uid'] . '.' . $_SERVER['HOSTNAME'] . '/' . $object['obj_id'],
          "sleepers" => $object['obj_sleepers'],
          "images" => $images,
          "icon" => Array(
            "className" => Array(
              "marker-pin pin-".(($object['obj_instantbook']) ? "selected" : "regular")
            )
          )
        )
      );
    }//while
  }

  //Запись файлов
  foreach ($venues_arr as $city_uid => $city_val) {

    $geojson_arr = Array("type" => "FeatureCollection", "features" => $city_val);
    $geojson = json_encode($geojson_arr);
    $geojson_file = fopen($venue_path.$city_uid.".geojson", "w");
    fwrite($geojson_file, $geojson);
    fclose($geojson_file);
  }
  /////////////////////////////
  //Бизнес-логика - окончание//
  /////////////////////////////

  //Окончание работы
  $log[] = 'Окончание работы скрипта: '.date("Y-m-d H:i:s");

  ////Оповещение каждого выполнения
  //Сериализация лога
  $bodymail = "<h2>Скрипт снятия генерации GeoJSON</h2>";
  foreach ($log as $value) {
    echo '<p>'.$value.'</p>';
  }

  //Отправка лога
  //myMail('marselos@gmail.com', 'Скрипт генерации GeoJSON', $bodymail);

  ////Оповещение только в случае ошибки
  if(count($errors)){
    //Сериализация лога
    $bodymail = "<h2>Скрипт снятия генерации GeoJSON</h2>";
    foreach ($errors as $error) {
      $bodymail .= '<p>'.$error.'</p>';
    }

    //Отправка лога
    myMail('marselos@gmail.com', 'Ошибка генерации GeoJSON', $bodymail);
  }
?>