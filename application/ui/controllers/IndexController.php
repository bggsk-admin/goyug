<?php
/**
 * Class and Function List:
 * Function list:
 * - __call()
 * - init()
 * - introAction()
 * - indexAction()
 * - ajaxAction()
 * - ajaxUpdateCounter()
 * - domainsAction()
 * - combinations()
 * Classes list:
 * - IndexController extends Zend_Controller_Action
 */
require_once ("Util.php");

//#########################################################################################
class IndexController extends Zend_Controller_Action
{

    //#########################################################################################
    public function __call($method, $args)
    {
        if ('Action' == substr($method, -6)) {
            if (method_exists($this, $method)) {
                $this->render();
            } else {

                $params['type'] = $this->getRequest()->getParam('action');

                $params['code'] = $this
                    ->getRequest()
                    ->getParam('type');

                $this->_forward('index', 'subscribe', 'default', $params);
            }
        }
    }

    //#########################################################################################
    public function init()
    {
        $this->log = new Ui_Model_Log();
        $this->model = new Ui_Model_Index();
        $this->_helper->layout->setLayout('main');
        $this->auth = Zend_Auth::getInstance();

        $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : "";

        $this->sessFilter = new Zend_Session_Namespace('filter');

        $ajaxContext = $this->_helper->getHelper('AjaxContext');

        $ajaxContext
            ->addActionContext('ajax', 'json')
            ->addActionContext('ajaxUpdateCounter', 'json')
            ->initContext('json');

        // $contextSwitch = $this->_helper->getHelper('contextSwitch');    
        // $contextSwitch
            // ->addActionContext('sitemap', 'xml')
            // ->initContext('xml');            
    }

    //#########################################################################################
    public function introAction()
    {
        // var_dump($_SERVER);
        // print_r($this->_request->getParams());die;

        $city_uid = $this->_request->getParam("cityuid");
        $this->view->cities = $cities = $this->model->getCities();

        if(!empty($city_uid) && array_key_exists($city_uid, $cities['list']))
        {
            $cur_city = $cities['list'][$city_uid];
        }
        
        $this->_helper->layout->setLayout('intro');
        $form = new Ui_Form_UserLogin();

        $this->view->form = $form;
        $this->view->objects_total = $this->model->getTotalObjectsAmount();
        $this->view->topdest = $this->model->getTopDestinations();

        // $this->view->curcity = array($this->sessFilter->data['cityuid'] => $this->sessFilter->data['cityname']);
        $this->view->city_uid = $city_uid;
        $this->view->identity = $this->identity;

        // $this->log->write( array('status' => 'success', 'result' => 'intro') );
    }

    //#########################################################################################
    public function indexAction()
    {
        $smartspanel = $this->model->getSmartsPanel();

        $this
            ->view
            ->smartspanel = $smartspanel;

        // $this->_forward('intro');

    }

    //#########################################################################################
    public function ajaxAction()
    {

        $this
            ->_helper
            ->viewRenderer
            ->setNoRender();

        $action = $_REQUEST['action'];

        switch ($action) {
            case 'updatecounter':
                $this->ajaxUpdateCounter($_REQUEST);
                break;
        }
    }

    //#########################################################################################
    public function sitemapAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        $city_uid = $this->_request->getParam('cityuid');

        $filename = (!empty($city_uid)) ? $city_uid . ".sitemap.xml" : "sitemap.xml";

        $content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/sitemaps/" . $filename);

        // Set up headers and body
        $this->_response->setHeader('Content-Type', 'text/xml; charset=utf-8')
            ->setBody($content);
    }

    //#########################################################################################
    public function ajaxUpdateCounter($query)
    {
        $this
            ->view
            ->value = $this
            ->model
            ->getFakeCounter();
    }

    //#########################################################################################
    public function testmailAction()
    {
        $db = Zend_Registry::get( 'db' );
        require 'PHPMailer/PHPMailerAutoload.php';

        $this->_helper->layout->setLayout('null-bootstrap');

        $email_to = $this->_request->getParam('email_to');
        $email_transport = $this->_request->getParam('email_transport');

        // Mail Parameters
        $newline = "\n";
        $boundary = '----=_NextPart_' . md5(time());
        $sender = "GOYUG";
        $from = "info@goyug.com";
        $replyto = "info@goyug.com";
        $to = $email_to;

        //Mail Subject
        $subject = "MAIL TEST ";
        $subject .= "(" . $email_transport . ")";

        //Mail Body
        // $body = '<!DOCTYPE html><html lang="ru"><head><meta charset="UTF-8"><title>TEST</title></head><body><h3>==TEST==</h3></body></html>';

        //SQL query
        //*****************************************************************
        $query = 'SELECT celt_email_body FROM celt_cron_email_list_tasks LIMIT 1';
        $data = $db->query($query)->fetchAll();
        $body = $data[0]['celt_email_body'];


        if(!empty($email_to) && !empty($email_transport))
        {

            if($email_transport == 'func')
            {
                //Headers
                $header = 'MIME-Version: 1.0' . $newline;
                $header .= 'From: =?UTF-8?B?' . base64_encode($sender) . '?=' . ' <' . $from . '>' . $newline;
                $header .= 'Reply-To: =?UTF-8?B?' . base64_encode($replyto) . '?=' . ' <' . $from . '>' . $newline;
                $header .= 'Return-Path: ' . $from . $newline;
                $header .= 'Content-Type: multipart/related; boundary="' . $boundary . '"' . $newline . $newline;

                $message = 'Content-Type: text/html; charset="utf-8"' . $newline;
                $message .= 'Content-Transfer-Encoding: 8bit' . $newline . $newline;
                $message .= $body . $newline;

                ini_set('sendmail_from', $from);
                mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $message, $header, "-f".$from);
            
            } // email_transport = func

            else if ($email_transport == 'zend')
            {

                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHTML($body);
                $mail->setFrom($from, $sender);
                $mail->setReplyTo($replyto, '');
                $mail->addTo($to, '');
                $mail->setSubject($subject);
                $mail->send();                

            } // email_transport = zend

            else if ($email_transport == 'phpmailer')
            {


                $mail = new PHPMailer;
                // $mail->CharSet = 'UTF-8';
                $mail->setFrom($from, $sender);
                $mail->addReplyTo($replyto, $sender);
                $mail->addAddress($to, '');
                $mail->Subject = $subject;
                $mail->msgHTML($body);

                if (!$mail->send()) {
                    echo "<h2 class='text-center'>Mailer Error: " . $mail->ErrorInfo . "</h2>";
                } else {
                    echo "<h2 class='text-center'>Message sent!</h2>";
                }

            } // email_transport = phpmailer

        }

    }

    //#########################################################################################
    public function combinations($words)
    {
        if (count($words) == 1) {
            return Array($words);
        };

        $combinations = Array();
        $i = 0;

        for($i = 0; $i < count($words); $i++)
        {
            $first_word = array_shift($words);

            foreach ($this->combinations($words) as $cmb) {
                $combinations[] = array_merge(Array($first_word), $cmb);
            };

            array_push($words, $first_word);
        }

        return $combinations;
    }

    //#########################################################################################
    public function BackTrace(&$srcarr, $cnt = 0, $arr = array(), $mask = array())
    {
        if (count($srcarr) == $cnt)
        {
            echo implode('', $arr) . '<br />';
            return;
        }

        if(count($srcarr)>0)
        {
        for ($i = 0; $i < count($srcarr); $i++)
        {
            if (!$mask[$i])
            {
                $mask[$i] = true;
                $arr[$cnt] = $srcarr[$i];
                $this->BackTrace($srcarr, $cnt + 1, $arr, $mask);
                $mask[$i] = false;
            }
        }

        }
    }

    // {{ END }} #########################################################################################

}
