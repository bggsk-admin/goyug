<?php
    require_once("Util.php");

    class AjaxController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->_helper->layout->setLayout('objects');

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext
            ->addActionContext('get', 'json')
            ->addActionContext('getObjects', 'json')
            ->initContext('json');
        }

        ##########################################################################################
        public function ajaxGetObjects()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->view->data = array(
                "title" => "Group A",
                "type" => "marker",
                "points" => array(
                    array("lat" => 45.066953, "lon" => 38.978399, "title" => "Title A1", "html" => "<h3>Content A1</h3>"),
                    array("lat" => 44.8,"lon" => 1.7,"title" => "Title B1","html" => "<h3>Content B1</h3>"),
                    array("lat" => 51.5,"lon" => -1.1,"title" => "Title C1","html" => "<h3>Content C1</h3>"),
                )
            );
        }

        ##########################################################################################
        public function getAction ()
        {
            $action = $_REQUEST['action'];

            switch ($action) {
                case 'getobjs':
                    $this->ajaxGetObjects($_REQUEST);
                    break;
            }

        }


    }


