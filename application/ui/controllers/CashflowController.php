<?php

require_once("Util.php");

class CashflowController extends Zend_Controller_Action {

  //#########################################################################################
  public function init() {

    $this->model = new Ui_Model_Cashflow();

    $this->log = new Ui_Model_Log();

    $this->auth = Zend_Auth::getInstance();

    $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : "";

    $ajaxContext = $this->_helper->getHelper('AjaxContext');

    $this->request = $_POST;

    $this->model->log('startCashflow', 'Started');
    $this->model->log('post', serialize($_POST));

    //print_r($_POST);
    /*
    $ajaxContext
    ->addActionContext('ajax', 'json')
    ->initContext('json');
    */

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'cashflow'));

    //$this->_helper->viewRenderer->setNoRender();

  }
  public function indexAction() {

    $this->_helper->viewRenderer->setNoRender();
  }

  ////////
  //ДЕМО//
  ////////
  //Проверка платежа
  public function checkorderdemoAction() {
    $this->model->log('startActionCheckOrderDemo', 'Started');
    $this->model->DEMO = true;
    $this->processRequest();
    exit;
  }

  //Подтверждение платежа
  public function paymentavisodemoAction() {
    $this->model->log('startActionPaymentAvisoDemo', 'Started');
    $this->model->DEMO = true;
    $this->processRequest();
    exit;
  }
  ////////////////////
  //ДЕМО - окончание//
  ////////////////////

  //Проверка платежа
  public function checkorderAction() {
    $this->model->log('startActionCheckOrder', 'Started');
    $this->processRequest();
    exit;
  }

  //Подтверждение платежа
  public function paymentavisoAction() {
    $this->model->log('startActionPaymentAviso', 'Started');
    $this->processRequest();
    exit;
  }

  //Обработка запроса
  public function processRequest() {

    // print_r($_REQUEST);

    // print_r($this->request);die;

    if(!isset($this->request['action']) || !in_array($this->request['action'], Array("checkOrder", "paymentAviso"))){
      $this->model->log('unknownAction', $this->request['action']);
      $this->model->logClose();
      exit;
    }

    $this->model->log('startProcess', $this->request['action']);
    $this->model->log('securityType', $this->model->SECURITY_TYPE);

    //Режим безопасности MD5
    if ($this->model->SECURITY_TYPE == "MD5") {

      // Проверка MD5 завершилась неудачей, ответ с кодом ошибки "1"
      if (!$this->model->checkMD5($this->request)) {
        $response = $this->model->buildResponse($this->request['action'], $this->request['invoiceId'], 1);
        $this->model->log('failedCheckMD5', 'Send back response: '.$response);
        $this->model->sendRequest($response);
        $this->model->logClose();
      } else {
        $this->model->log('successCheckMD5', 'Check MD5 success, go ahead...');
      }
    }
    //Режим безопасности PKCS7
    /*
    else if ($this->model->SECURITY_TYPE == "PKCS7") {
      // Проверка сертификата завершилась неудачей, ответ с кодом "200"
      if (($this->request = $this->model->verifySign()) == null) {
        $response = $this->model->buildResponse($this->request['action'], null, 200);
        $this->model->sendRequest($response);
      }
    }*/

    $response = null;

    $this->model->log('startMethod'.$this->request['action'], 'Started');

    if ($this->request['action'] == 'checkOrder') {
      $response = $this->model->checkOrder($this->request);
    } else {
      $response = $this->model->paymentAviso($this->request);
    }
    $this->model->log('sendRequestBack', 'Send back response: '.$response);
    $this->model->sendRequest($response);
    $this->model->logClose();
  }
}

