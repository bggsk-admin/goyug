<?php

require_once ('CustomUploadHandler.php');

class FilesController extends Zend_Controller_Action {

    //#########################################################################################
    public function init() {

        $this->db = Zend_Registry::get('db');
        $this->tbl = new Zend_Db_Table(array('name' => 'f_files', 'primary' => 'f_id'));

        $this->log = new Ui_Model_Log();
        $this->auth = Zend_Auth::getInstance()->getIdentity();

        $this->pref = "f_";
        $this->ucid = $this->view->ucid = "files";

        $this->view->name = "Файлы";

        $this->backurl = $this->view->backurl = $this->view->url(array('action' => 'index'));

        $ajaxContext = $this->_helper->getHelper('AjaxContext');

        $ajaxContext->addActionContext('index', 'json')->initContext('json');
    }

    //#########################################################################################
    public function indexAction() {

        $upload_id = $this->_request->getParam('upload_id');
        $upload_ucid = $this->_request->getParam('upload_ucid');

        //Перехват запросов
        switch ($_SERVER['REQUEST_METHOD']) {

            case 'GET':

                $files_upload_handler = Array();

                $rows = $this->db->query("
                    SELECT *
                    FROM f_files
                    WHERE f_uid = $upload_id
                    ORDER BY f_order
                ")->fetchAll();

                //Приводим к формату UploadHandler
                foreach ($rows as $value) {

                    $files_upload_handler[] = Array(

                        "id" => $value['f_id'],
                        "deleteType" => "DELETE",
                        "deleteUrl" => "/files/index/upload_ucid/".$upload_ucid."/upload_id/".$upload_id."/?file=".$value['f_name'],
                        "name" => $value['f_name'],
                        "res" => "800 x 600",
                        "size" => $value['f_size'],
                        "thumbnailUrl" => "/upload/".$upload_ucid."/".$upload_id."/thumbnail/".$value['f_name'],
                        "thumbnail_res" => "200 x 150",
                        "url" => "/upload/".$upload_ucid."/".$upload_id."/".$value['f_name'],
                        "x100Url" => "/upload/".$upload_ucid."/".$upload_id."/x100/".$value['f_name'],
                        "x100_res" => "100 x 75",
                        "x300Url" => "/upload/".$upload_ucid."/".$upload_id."/x300/".$value['f_name'],
                        "x300_res" => "300 x 225",
                        "x400Url" => "/upload/".$upload_ucid."/".$upload_id."/x400/".$value['f_name'],
                        "x400_res" => "400 x 300",
                        "x500Url" => "/upload/".$upload_ucid."/".$upload_id."/x500/".$value['f_name'],
                        "x500_res" => "500 x 375",
                        "x600Url" => "/upload/".$upload_ucid."/".$upload_id."/x600/".$value['f_name'],
                        "x600_res" => "600 x 450",
                        "x800Url" => "/upload/".$upload_ucid."/".$upload_id."/x800/".$value['f_name'],
                        "x800_res" => "800 x 600",
                        "x1200Url" => "/upload/".$upload_ucid."/".$upload_id."/x1200/".$value['f_name'],
                        "x1200_res" => "800 x 600"
                    );
                }

                $files = Array("files" => $files_upload_handler);

                echo json_encode($files);

                // return $upload_handler->get_server_var('SCRIPT_PATH');

                break;

            case 'DELETE':

                //Название файла
                $file = $_GET['file'];

                //Т.к. у файлов не указан владелец, придется немного извратиться ))
                //и узнать сначала объект, в котором есть эта фотка, а через объект узнать владельца )

                //Получаем файл
                $f_file_row = $this->db->query("
                    SELECT f_uid, f_name
                    FROM f_files
                    WHERE f_name = '$file'
                    AND f_uid = '$upload_id'
                ")->fetch();

                //print_r($f_file_row);

                //Получаем объект
                $obj_object_row = $this->db->query("
                    SELECT obj_u_id
                    FROM obj_object
                    WHERE obj_id = '$f_file_row[f_uid]'
                ")->fetch();

                //print_r($obj_object_row);

                //Теперь владелец известен - сравниваем его с текущим, и если не совпадает генерируем 403
                //или же производим удаление файла
                if($obj_object_row['obj_u_id'] != $this->auth->u_id){

                    //Отвечаем
                    header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
                    header("HTTP/1.1 403 Forbidden");
                    die("Access denied. You are not own this file.");

                } else {

                    //Удаляем
                    $upload_handler = new CustomUploadHandler(
                        array(
                            'upload_dir' => $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                            'upload_url' => '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                            'script_url' => 'http://'.$_SERVER['HTTP_HOST'].'/files/index/upload_ucid/' . $upload_ucid . '/upload_id/' . $upload_id . '/',
                            'db_table' => 'f_files',
                            'obj_id' => $upload_id,
                        )
                    );
                }

                break;

            default:

                $upload_handler = new CustomUploadHandler(
                    array(
                        'upload_dir' => $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                        'upload_url' => '/upload/' . $upload_ucid . '/' . $upload_id . '/',
                        'script_url' => 'http://'.$_SERVER['HTTP_HOST'].'/files/index/upload_ucid/' . $upload_ucid . '/upload_id/' . $upload_id . '/',
                        'db_table' => 'f_files'
                    )
                );

                // return $upload_handler->get_server_var('SCRIPT_PATH');

                break;
        }

        //Дабы избежать лишнего вывода
        die();
    }

    ##########################################################################################
    //Упорядочивание картинок
    ##########################################################################################
    public function orderAction() {

        //Количество фотографий
        $order = json_decode($_GET['order'], true);
        $count = count($order);

        //Обновляем порядок
        for ($i = 0; $i < $count; $i++) {

            //Получаем файл
            $f_file_row = $this->tbl->find(intval($order[$i]))->current();

            //Получаем объект
            $obj_object_row = $this->db->query("
                SELECT obj_u_id
                FROM obj_object
                WHERE obj_id = '$f_file_row[f_uid]'
            ")->fetch();

            //Проверяем владелца файла
            //если все ОК, то переупорядочиваем
            if($obj_object_row['obj_u_id'] != $this->auth->u_id){

                //Отвечаем
                header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
                header("HTTP/1.1 403 Forbidden");
                die("Access denied. You are not own this file.");

            } else {

                //Новое упорядочивание
                $f_file_row->f_order = $i;
                $f_file_row->save();
            }

        }

        //Предотвращаем вывод ненужной информации от зенда
        die();
    }
}
