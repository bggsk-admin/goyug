<?php
require_once("Util.php");

class UserController extends Zend_Controller_Action
{

  //#########################################################################################
  public function init() {

    $this->model = new Ui_Model_User();
    $this->modelObjects = new Ui_Model_Objects();
    $this->modelCashflow = new Ui_Model_Cashflow();

    $this->log = new Ui_Model_Log();

    $this->auth = Zend_Auth::getInstance();
    $this->db = Zend_Registry::get('db');

    $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : "";

    //Обновим бабки
    if($this->identity){
      $u_user = $this->model->find($this->identity->u_id)->current();
      $this->identity->u_money = $u_user['u_money'];
      $this->identity->u_action_enabled = $u_user['u_action_enabled'];
    }

    $this->backurl = $this->view->url(array('action' => 'index'));


    /////////////
    //Абонплата//
    /////////////

      //Шаг 1! Важно! Указать города!
      //Города кроме этого скрипта еще должны указываться в скриптах деактиваций СВОБОДНА СЕГОДНЯ и РАСПРОДАЖА (isempty_demark, sale_demark), а так же в письме рассылки по обновлению работы
      //Запуск активации услуги необходимо сделать с помощью кроновского скрипта set_rental, там тоже нужно указать город! И не забыть указать нужную дату старта
      //Еще нужно указать город в модели Objects в методе getObjectsByCityUID
      //И еще в автоматизации, где пробуждение владельцев - в запросе - ПРОБУЖДЕНИЕ (Шаг 1) MailAutomate/Action.class.php
      //И еще указать город в cron/rebuild_geo_json, чтобы исключить из выдачи объекты

      //Шаг 2.
      //Сперва делается активация услуги (кроновский скрипт set_rental), затем уже рассылка уведомлений владельцам.
      //Для обработки продлений необходимо запустить отдельные крон-скрипты isempty_rental_demark и sale_rental_demark
      //Далее все по идее должно работать - автопродление, отключение за неуплату и т.д. в штатном режиме.

      //Режим абонплаты
      //Внимание! В будущем лучше перенести определение переменной сюда - так будет удобней управлять городами, чем в каждом отдельном экшне
      $this->rentalCities = array(
        "kislovodsk", 
        "nnovgorod", 
        "sochi", "sevastopol", "spb", 
        "krasnodar", "saratov", "tyumen", 
        "novosibirsk", "ekaterinburg", "perm", 
        "kazan", "ufa", "voronezh", 
        "moscow", "omsk", "rostovnadonu", "volgograd",
        "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
        "zheleznovodsk",
        "pyatigorsk", "yalta", "yaroslavl", "tomsk",
        "bryansk", "tula", "magnitogorsk", "ivanovo",
        "khabarovsk", "vladimir", "penza", "kirov", "vologda",
        "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
        "astrakhan", "tolyatty", "orel", "orenburg", "tver",
        "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
      );
      $this->view->is_rental = false;

      //Исключенные из показа предупреждения о абонплаты
      $this->view->part_rental_exclude = Array("profile");

    /////////////////////////
    //Абонплата - окончание//
    /////////////////////////


    $ajaxContext = $this->_helper->getHelper('AjaxContext');
    $ajaxContext
    ->addActionContext('ajax', 'json')
    ->addActionContext('ajaxDoLogin', 'json')
    ->addActionContext('ajaxDoReg', 'json')
    ->addActionContext('ajaxSaveProfile', 'json')
    ->addActionContext('ajaxFeedback', 'json')
    ->addActionContext('ajaxIsEmpty', 'json')
    ->addActionContext('ajaxForgot', 'json')
    ->addActionContext('markOutObjects', 'json')
    ->addActionContext('ajaxOntop', 'json')
    ->addActionContext('ajaxUserAwakeOK', 'json')
    ->addActionContext('ajaxHideNotification', 'json')
    ->addActionContext('ajaxObjEnable', 'json')
    ->addActionContext('ajaxObjDisable', 'json')
    ->initContext('json')
    ;

    if(!isset($_SESSION['shownotification']['rental']))
      $_SESSION['shownotification']['rental'] = true;

    if( $this->_request->getParam('id') && !$this->model->isObjectIsOwn($this->_request->getParam('id')) )
      die("<p>Вам не принадлежит выбранный объект<br /><a href=\"http://goyug.com/user/dashboard\">Перейти в Личный Кабинет</a></p>");
  }

  //#########################################################################################
  public function indexAction() {
    header("Location: /user/dashboard");
  }

  //#########################################################################################
  public function loginAction() {

    $this->_helper->layout->setLayout('login');
    $form = new Ui_Form_UserLogin();

    if ($this->_request->isPost() && $form->isValid($_POST)) {
      $formValues = $form->getValues();

      $db = Zend_Db_Table::getDefaultAdapter();

      //create the auth adapter
      $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'u_user', 'u_username', 'u_password', 'MD5(?) AND u_active = "1"');

      //set the username and password
      $authAdapter->setIdentity($formValues['u_username']);
      $authAdapter->setCredential($formValues['u_password']);

      //authenticate
      $result = $authAdapter->authenticate();

      if ($result->isValid()) {

        // store the username, first and last names of the user
        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage();
        $storage->write($authAdapter->getResultRowObject(array('u_id', 'u_username', 'u_firstname', 'u_lastname', 'u_email', 'u_phone_1', 'u_phone_2', 'u_phone_3', 'u_role')));
        $this->model->updateUserLoginTime($auth->getIdentity()->u_id);

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'success', 'result' => 'login'));

        return $this->_redirect("/index/intro", array('prependBase' => false));
      }
      else {

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $formValues['u_username']));

        $this->view->alertErrorMessage = "Sorry, your username or password was incorrect";
        $this->view->errorClass = "error";
      }
    }

    $this->view->form = $form;
    $this->view->title = Util::getConfig(array("project_title"));
  }

  //#########################################################################################
  public function ajaxAction() {

    $action = $_REQUEST['action'];

    switch ($action) {
      case 'dologin':
        $this->ajaxDoLogin($_REQUEST);
        break;

      case 'doreg':
        $this->ajaxDoReg($_REQUEST);
        break;

      case 'signup':
        $this->ajaxSignup($_REQUEST);
        break;

      case 'addObject':
        $this->ajaxSaveObject($_REQUEST);
        break;

      case 'saveObject':
        $this->ajaxSaveObject($_REQUEST);
        break;

      case 'saveProfile':
        $this->ajaxSaveProfile($_REQUEST);
        break;

      case 'loadCal':
        $this->ajaxLoadCal($_REQUEST);
        break;

      case 'saveCal':
        $this->ajaxSaveCal($_REQUEST);
        break;

      case 'markOutObjects':
        $this->ajaxMarkOutObjects($_REQUEST);
        break;

      case 'ontop':
        $this->ajaxOntop($_REQUEST);
        break;

      case 'objEnable':
        $this->ajaxObjEnable($_REQUEST);
        break;

      case 'objDisable':
        $this->ajaxObjDisable($_REQUEST);
        break;

      case 'saveUserEmail':
        $this->ajaxSaveUserEmail($_REQUEST);
        break;

      case 'userawakeok':
        $this->ajaxUserAwakeOK($_REQUEST);
        break;

      case 'hidenotification':
        $this->ajaxHideNotification($_REQUEST);
        break;

      case 'feedback':
        $this->ajaxFeedback($_REQUEST);
        break;

      case 'isempty':
        $this->ajaxIsEmpty($_REQUEST);
        break;

      case 'forgot':
        $this->ajaxForgot($_REQUEST);
        break;
    }

    $this->log->write( array('status' => 'success', 'result' => 'ajax'), $_REQUEST);
  }

  //#########################################################################################
  public function ajaxSignup($params) {
    $this->_helper->viewRenderer->setNoRender();

    // $this->view->redirect = $params["redirect"];
    $this->view->error = "Sorry, your username or password was incorrect";

    // $this->view->errorClass = "error";
  }

  //#########################################################################################
  public function ajaxDoLogin($params) {
    $this->_helper->viewRenderer->setNoRender();

    $username = $params['u_username'];
    $password = $params['u_password'];

    if (!empty($username) && !empty($password)) {

      $if_phone = Util::stripTel($username);

      $auth_arr = $this->model->checkValidLogin($username, $password);

      if ($auth_arr) {

        $auth = Zend_Auth::getInstance();
        $storage = $auth->getStorage();
        $storage->write($auth_arr);

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'success', 'result' => 'login'), $auth_arr);

        $this->view->redirect = "/user/dashboard";
      }
      else {

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'error', 'result' => 'username or password was incorrect', 'username' => $params['u_username']));

        $this->view->error = array('text' => $this->view->translate('Incorrect username or password'), 'elements' => "#u_username, #u_password");
      }
    }
    else {
      $this->view->error = $this->view->translate('Incorrect username or password');
    }
  }

  //#########################################################################################
  public function ajaxDoReg($params) {
    $this->_helper->viewRenderer->setNoRender();

    $name = $params['msf_name'];
    $email = $params['msf_email'];
    $phone = $params['msf_mobile'];
    $phone = Util::stripTel($phone);

    $password = $params['msf_password'];

    if (!empty($name) && !empty($email) && !empty($phone) && !empty($password)) {

      $check_phone = $this->model->checkPhoneExist($phone);
      $check_email = $this->model->checkEmailExist($email);
      $valid_email = $this->model->checkEmailValid($email);

      if($check_phone) {

        $this->view->error = array('text' => 'Такой номер уже зарегистрирован', 'elements' => "#msf_mobile");

      } else if($check_email) {

        $this->view->error = array('text' => 'Такой Email зарегистрирован', 'elements' => "#msf_email");

      } else if(!$valid_email) {

        $this->view->error = array('text' => 'E-mail некорректный', 'elements' => "#msf_email");

      } else {

        $auth_arr = $this->model->createUser($name, $email, $phone, $password, 'owner');

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'success', 'result' => 'reguser'));

        if($auth_arr) {

          $auth = Zend_Auth::getInstance();
          $storage = $auth->getStorage();
          $storage->write($auth_arr);

          //------------------- Logger ---------------------
          $this->log->write(array('status' => 'success', 'result' => 'login'), $auth_arr);
          $this->view->redirect = "/user/rooms";
        }
        else {

          //------------------- Logger ---------------------
          $this->log->write(array('status' => 'error', 'result' => 'Ошибка при создании пользователя', 'username' => $email));
          $this->view->error = array('text' => 'Ошибка при создании пользователя', 'elements' => "#msf_email");
        }
      }
    } else {
      $this->view->error = array('text' => $this->view->translate('Incorrect username or password'), 'elements' => "#msf_mobile");
    }
  }

  //#########################################################################################
  public function ajaxLoadCal($params) {
    $this->_helper->viewRenderer->setNoRender();

    $obj_id = isset($params['id']) ? $params['id'] : 0;
    $response = array();

    $items = $this->model->loadObjectCal($this->identity->u_id, $obj_id);

    if (!empty($obj_id) && $obj_id > 0) {
      if (count($items) > 0) {
        foreach ($items as $id => $val) {
          if (!empty($val['cal_startdate'])) {
            $className = (!empty($val['cal_status'])) ? "event-closed" : "";

            $response[$id] = array(
              "id" => date("Ymd", strtotime($val['cal_startdate'])),
              "title" => $val['cal_title'],
              "start" => date("c", strtotime($val['cal_startdate'])),
              "end" => date("c", strtotime($val['cal_enddate'])),
              "className" => $className,
              "status" => $val['cal_status'],
              "type" => "event",
              );

          }
        }
      }
    }

    $this->view->data = $response;
  }

  //#########################################################################################
  public function ajaxSaveCal($params) {
    $this->_helper->viewRenderer->setNoRender();

    $obj_id = $params['id'];
    $events_json = $params['json'];

    $response = "";

    $response = $this->model->saveObjectCal($this->identity->u_id, $obj_id, $events_json);

    $this->view->data = $response;
  }

  //#########################################################################################
  public function ajaxMarkOutObjects($params) {
    $response = $this->model->markOutObjects($params);
    echo json_encode($response);
    die();
  }

  //#########################################################################################
  public function ajaxOntop($params) {
    $response = $this->modelCashflow->ontop($params);
    $this->view->ontop = $response;
  }

  //#########################################################################################
  public function ajaxObjEnable($params) {
    $response = $this->model->objEnable($params);
    $this->view->obj_enable = $response;

    $this->log->write(array('status' => ($response['status'] ? 'success' : 'error'), 'result' => 'obj_enable'));
  }

  //#########################################################################################
  public function ajaxObjDisable($params) {
    $response = $this->model->objDisable($params);
    $this->view->obj_disable = $response;

    $this->log->write(array('status' => ($response['status'] ? 'success' : 'error'), 'result' => 'obj_disable'));
  }

  //#########################################################################################
  public function ajaxSaveUserEmail($params) {
    $response = $this->model->saveUserEmail($params);
    $this->view->response = $response;
  }

  //#########################################################################################
  public function ajaxUserAwakeOK($params) {
    $response = $this->model->userAwakeOK($params);
    $this->view->response = $response;
  }

  //#########################################################################################
  public function ajaxHideNotification($params) {
    $response = $this->model->userHideNotification($params);
    $this->view->response = $response;
  }

  //#########################################################################################
  public function ajaxForgot($params) {
    $response = $this->model->forgot($params);
    $this->view->forgot = $response;

    $this->log->write(array('status' => ($response['status'] ? 'success' : 'error'), 'result' => 'forgot'));
  }

  //#########################################################################################
  public function ajaxSaveObject($params) {
    $response = $this->model->updateObject($params);
    $this->view->form = $response;
  }

  //#########################################################################################
  public function ajaxSaveProfile($params) {
    //print_r($params);
    $response = $this->model->updateProfile($params);
    $this->view->response = $response;
  }

  //#########################################################################################
  public function ajaxFeedback($params) {
    //print_r($params);
    $response = $this->model->feedback($params);
    $this->view->response = $response;
  }

  //#########################################################################################
  public function ajaxIsEmpty($params) {

    $response = $this->model->isEmpty($params);
    $this->view->isempty = $response;

    $result = $this->view->isempty['isempty'] ? 'isempty_on' : 'isempty_off';

    $this->log->write(array('status' => ($response['status'] ? 'success' : 'error'), 'result' => $result));
  }

  //#########################################################################################
  public function logoutAction() {
    $auth = Zend_Auth::getInstance();

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'logout'));

    $auth->clearIdentity();

    return $this->_forward('intro', 'index');
  }


  ///////////////////////////////////////////////////////////////////////////////////////////
  // Разделы Личного Кабинета ///////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function dashboardAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "dashboard";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->billing = $this->modelCashflow->getBilling();
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'dashboard'));
  }

  //#########################################################################################
  public function roomsAction() {

    // $this->_helper->layout->setLayout('user2colfix');

    if (!$this->identity->u_tutorial) {
      // return $this->_forward('tutorial', 'user');
    }

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "rooms";
    $this->view->objects = $objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();

    // print_r($objects);die;

    $todayViews = array();
    $weekViews = array();
    $totalViews = array();

    foreach($objects as $ind => $val) {
      //$todayViews[$val['obj_id']] = $this->model->getTodayViewsAmount($val['obj_id']);
      //$weekViews[$val['obj_id']] = $this->model->getWeekViewsAmount($val['obj_id'], 0);
      //$totalViews[$val['obj_id']] = $this->model->getTotalViewsAmount($val['obj_id']);
      $todayViews[$val['obj_id']] = $this->model->getTodayViewsAmountAuto($val['obj_id']);
      $totalViews[$val['obj_id']] = $this->model->getTotalViewsAmountAuto($val['obj_id']);
    }

    $this->view->todayViews = $todayViews;
    $this->view->weekViews = $weekViews;
    $this->view->totalViews = $totalViews;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'rooms'));
  }

  //#########################################################################################
  public function paymentAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action_ctrl = "payment";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getCities();
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->ontop_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'payment-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'payment'));
  }

  //#########################################################################################
  public function profileAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "profile";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));
  }

  //#########################################################################################
  public function feedbackAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "feedback";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->feedbacks = $this->model->getFeedbacks();
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //--------------- Logger--------------------
    $this->log->write( array('status' => 'success', 'result' => 'feedback'), array());
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // Разделы Личного Кабинета - окончание ///////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  //#########################################################################################
  public function authtokenAction() {

    $this->_helper->viewRenderer->setNoRender();

    $token = $this->_request->getParam('token');

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'authtoken_start'));

    //Попытка авторизации с токеном
    $result = $this->model->authToken($token);

    if($result){

      $auth = Zend_Auth::getInstance();
      $storage = $auth->getStorage();
      $storage->write($result);

      //------------------- Logger ---------------------
      $this->log->write(array('status' => 'success', 'result' => 'authtoken_success'), $result);

      header("Location: /user/dashboard");

    } else {

      //------------------- Logger ---------------------
      $this->log->write(array('status' => 'success', 'result' => 'authtoken_fail'));

      $_SESSION['authtoken_expired'] = true;

      header("Location: /user/index");

    }

    exit;
  }

  //#########################################################################################
  public function subscribeAction() {

    $this->_helper->layout->setLayout('login');
    $this->view->action = "subscribe";

    //////////////////////
    //Входящие параметры//
    //////////////////////

      //Токен отмены подписки
      //по нему находится пользователь
      //md5()
      $token = $this->_request->getParam('token');

      //Режим получения - управляет, какую рассылочную опцию включить/выключить
      //news, email, sms
      $mode = $this->_request->getParam('mode');

      //Действие - отписка или подписка
      //subscribe, unsubscribe
      $direction = $this->_request->getParam('direction');

    //////////////////////////////////
    //Входящие параметры - окончание//
    //////////////////////////////////

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => $direction.'_'.$mode.'_start'));

    //Манипуляция с рассылками
    //Параметр считывается в шаблоне
    //Возврат: $this->model->subscribe()
    //Array("status" => boolean, "reason" => string, "mode" => string, "direction" => string);
    $this->view->result = $this->model->subscribe($direction, $mode, $token);

    //Манипуляция завершилась успешно
    if($this->view->result['status']){

      //------------------- Logger ---------------------
      $this->log->write(array('status' => 'success', 'result' => $direction.'_'.$mode.'_success'));

    }
    //Ошибка в процессе управления рассылками
    else {

      //------------------- Logger ---------------------
      $this->log->write(array('status' => 'success', 'result' => $direction.'_'.$mode.'_fail'));

    }
  }

  //#########################################################################################
  public function calendarAction() {

    $this->_helper->layout->setLayout('user-lk');

    $this->view->action = "calendar";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->object = $this->model->getUserObject($this->identity->u_id, $this->_request->id);
    $this->view->obj_id = $this->view->object['obj_id'];


      //------------------- Logger ---------------------
      $this->log->write(array('status' => 'success', 'result' => 'calendar'));
  }

  //#########################################################################################
  public function tutorialAction() {
    $this->_helper->layout->setLayout('user-lk');
  }

  //#########################################################################################
  public function updateAction() {

    $form = new Ui_Form_Objects();

    $id = $this->_request->getParam('id');

    $prm_parameter = $this->modelObjects->getParamsFields('prm_parameter', 'prm');
    $per_period = $this->modelObjects->getMultiselect('per_period', 'per');

    if ($this->_request->isPost()) {
      $post = $this->_request->getPost();

      if ($form->isValid($post)) {
        $this->modelObjects->updateItem($form->getValues());
        $form_arr = $form->getValues();

        //------------------- Logger ---------------------
        $this->log->write( array('status' => 'success', 'result' => 'update'), $form->getValues() );

        return $this->_redirect($this->backurl, array('prependBase' => false));
      }
    }
    else {

      $id = $this->_request->getParam('id');

      $item = $this->modelObjects->find($id)->current();

      $form_arr = $item->toArray();

      //Подгружаем свойства объекта
      $form_arr+= $this->modelObjects->getValues(array('ot_object_type', 'rt_rent_type', 'srv_service', 'at_amenity', 'eq_equipment', 'rst_restriction'), $id);

      //Подгружаем смартфильтры
      //$form_arr +=  $this->modelObjects->getSmartfiltersValues($id);

      //Подгружаем параметры объекта
      $form_arr+= $this->modelObjects->getParamValues(array('prm_parameter'), $id);

      $form->populate($form_arr);

      //------------------- Logger ---------------------
      $this->log->write( array('status' => 'success', 'result' => 'update-view'), $form_arr );
    }

    //print_r($form);
    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "update";
    $this->view->upload_id = $id;
    $this->view->upload_ucid = "object";
    $this->view->prm_parameter = $prm_parameter;
    $this->view->per_period = $per_period;
    $this->view->form = $form;
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->object = $this->model->getUserObject($this->identity->u_id, $this->_request->id);
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getAllCities();
  }

  //#########################################################################################
  public function addAction() {

    $form = new Ui_Form_Objects();

    $this->_helper->layout->setLayout('user-lk');
    $this->view->objects = $objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->action = "add";
    $this->view->form = $form;
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getAllCities();

      //------------------- Logger ---------------------
      $this->log->write( array('status' => 'success', 'result' => 'add'));
  }


  ///////////////////////////////////////////////////////////////////////////////////////////
  // ontop //////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function ontopAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "ontop";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->ontop_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'ontop-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function ontopbalanceAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "ontopbalance";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->ontop_price;

    //Итоговая стоимость с учетом скидки, в рублях
    $this->view->sumTotal = $this->modelCashflow->ontop_price;

    switch ($_POST['ontopAutoUpdateCount']) {
      case '1':
        $this->view->sumTotal = 25;
        break;
      case '2':
        $this->view->sumTotal = 45;
        break;
      
      case '3':
        $this->view->sumTotal = 65;
        break;
      
      case '5':
        $this->view->sumTotal = 105;
        break;
      
      case '10':
        $this->view->sumTotal = 220;
        break;
      
      case '20':
        $this->view->sumTotal = 420;
        break;
      
      case '30':
        $this->view->sumTotal = 620;//620
        break;
      
      default:
        # code...
        break;
    }

    //Номер заказа
    $this->view->orderNumber = 'ontop-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function ontopbalanceprocessAction() {

    $this->view->action = "ontopbalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;

    //Кол-во поднятий
    $ontopAutoUpdateCount = $form['ontopAutoUpdateCount'];

    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);

    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];

    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    //Итоговая стоимость с учетом скидки, в рублях
    $this->sumTotal = $this->modelCashflow->ontop_price;

    switch ($ontopAutoUpdateCount) {

      case '1':
        $this->sumTotal = 25;
        break;

      case '2':
        $this->sumTotal = 45;
        break;

      case '3':
        $this->sumTotal = 65;
        break;

      case '5':
        $this->sumTotal = 105;
        break;

      case '10':
        $this->sumTotal = 220;
        break;

      case '20':
        $this->sumTotal = 420;
        break;
      
      case '30':
        $this->sumTotal = 620;//620
        break;

      default:
        # code...
        break;
    }

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->sumTotal){

      mail("marselos@gmail.com", "Не удалось поднять объект", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат поднятия
      $ontop_result = $ontopAutoUpdateCount == 1 ? $this->modelCashflow->ontop(Array("obj_id" => $obj_id)) : $this->modelCashflow->ontopAuto(Array("obj_id" => $obj_id), $ontopAutoUpdateCount);

      //Списание средств
      if($ontop_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать за услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => $obj_id,
          "transaction_name" => $ontopAutoUpdateCount == 1 ? "ontop" : "ontop_auto_update_".$ontopAutoUpdateCount,
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => ''
          );

        // В следствии ошибки во время транзакции по пользователю из Оренбурга (ID 8626, OBJ_ID 36897, TRANSACTION ontop_auto_update_30)
        // Ошибка случилась во время транзакции Jan 22 10:36 log_2018_01_22_10_36_49_1516606609.log
        // Временно отключаем оплату по нему 23.01.2018 09:54
        // 
        if($u_id != '8626') {
          $this->modelCashflow->pay($pay);
        }

        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось поднять объект", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // ontop end //////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////////
  // isempty ////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function isemptyAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "isempty";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->isempty_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'isempty-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function isemptybalanceprocessAction() {

    $this->view->action = "isemptybalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->isempty_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось активировать услугу СВОБОДНА СЕГОДНЯ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат активации услуги
      if($this->view->is_rental){

        $isempty_result = $this->modelCashflow->isemptyRental(Array("obj_id" => $obj_id));
        //Списание средств
        if($isempty_result['status']){
          //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
          //$pay             - массив операции списания
          //u_id             - пользователь, с которого необходимо списать за услугу
          //obj_id           - объект, где произошло списание за услугу
          //transaction_name - название транзакции, см. шапку
          //sum              - сумма операции
          //table            - таблица, где можно посмотреть статистику
          //table_id         - ID записи статистики
          $pay = Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => "isempty_rental",
            "sum" => 0,
            "table" => "u_user",
            "table_id" => $u_id
            );
          $this->modelCashflow->pay($pay);
          header("Location: /user/paymentsuccess");
          exit;
        } else {
          mail("marselos@gmail.com", "Не удалось активировать услугу СВОБОДНА СЕГОДНЯ - бесплатно", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
          header("Location: /user/paymentfail");
          exit;
        }

      } else {

        $isempty_result = $this->modelCashflow->isempty(Array("obj_id" => $obj_id));
        //Списание средств
        if($isempty_result['status']){
          //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
          //$pay             - массив операции списания
          //u_id             - пользователь, с которого необходимо списать за услугу
          //obj_id           - объект, где произошло списание за услугу
          //transaction_name - название транзакции, см. шапку
          //sum              - сумма операции
          //table            - таблица, где можно посмотреть статистику
          //table_id         - ID записи статистики
          $pay = Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => "isempty",
            "sum" => $form['sum'],
            "table" => "u_user",
            "table_id" => $u_id
            );
          $this->modelCashflow->pay($pay);
          header("Location: /user/paymentsuccess");
          exit;
        } else {
          mail("marselos@gmail.com", "Не удалось активировать услугу СВОБОДНА СЕГОДНЯ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
          header("Location: /user/paymentfail");
          exit;
        }

      }

    }

    exit;
  }

  //#########################################################################################
  public function isemptydeactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "isemptydeactivate";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->isempty_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'isemptydeactivate-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function isemptydeactivatebalanceprocessAction() {

    $this->view->action = "isemptydeactivatebalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу СВОБОДНА СЕГОДНЯ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $isemptydeactivate_result = $this->modelCashflow->isemptydeactivate(Array("obj_id" => $obj_id));

      //Списание средств
      if($isemptydeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу СВОБОДНА СЕГОДНЯ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").".");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function isemptydeactivatebalanceprocessnowAction() {

    $this->view->action = "isemptydeactivatebalanceprocessnow";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу СВОБОДНА СЕГОДНЯ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $isemptydeactivate_result = $this->modelCashflow->isemptydeactivatenow(Array("obj_id" => $obj_id));

      //Списание средств
      if($isemptydeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу СВОБОДНА СЕГОДНЯ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").".");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // isempty end ////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////////
  // photomark //////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function photomarkAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "photomark";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость выделения, в рублях
    $this->view->price = $this->modelCashflow->photomark_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'photomark-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function photomarkbalanceAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "photomarkbalance";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->photomark_price;

    //Номер заказа
    $this->view->orderNumber = 'photomark-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function photomarkbalanceprocessAction() {

    $this->view->action = "photomarkbalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->photomark_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось выделить фото объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат поднятия
      $photomark_result = $this->modelCashflow->photomark(Array("obj_id" => $obj_id));

      //Списание средств
      if($photomark_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => $obj_id,
          "transaction_name" => "photomark",
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => ''
          );
        $this->modelCashflow->pay($pay);
        header("Location: /user/paymentsuccess");
        exit;
      } else {

        mail("marselos@gmail.com", "Не удалось выделить фото объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // photomark end //////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////////
  // sale ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function saleAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "sale";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->sale_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'sale-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function salebalanceprocessAction() {

    $this->view->action = "salebalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->sale_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось активировать услугу РАСПРОДАЖА", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      if($this->view->is_rental){

        //Результат активации услуги
        $sale_result = $this->modelCashflow->saleRental(Array("obj_id" => $obj_id));

        //Списание средств
        if($sale_result['status']){
          //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
          //$pay             - массив операции списания
          //u_id             - пользователь, с которого необходимо списать за услугу
          //obj_id           - объект, где произошло списание за услугу
          //transaction_name - название транзакции, см. шапку
          //sum              - сумма операции
          //table            - таблица, где можно посмотреть статистику
          //table_id         - ID записи статистики
          $pay = Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => "sale_rental",
            "sum" => 0,
            "table" => "u_user",
            "table_id" => $u_id
            );
          $this->modelCashflow->pay($pay);
          header("Location: /user/paymentsuccess");
          exit;
        } else {
          mail("marselos@gmail.com", "Не удалось активировать услугу РАСПРОДАЖА - бесплатно", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: ошибка метода.");
          header("Location: /user/paymentfail");
          exit;
        }

      } else {

        //Результат активации услуги
        $sale_result = $this->modelCashflow->sale(Array("obj_id" => $obj_id));

        //Списание средств
        if($sale_result['status']){
          //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
          //$pay             - массив операции списания
          //u_id             - пользователь, с которого необходимо списать за услугу
          //obj_id           - объект, где произошло списание за услугу
          //transaction_name - название транзакции, см. шапку
          //sum              - сумма операции
          //table            - таблица, где можно посмотреть статистику
          //table_id         - ID записи статистики
          $pay = Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => "sale",
            "sum" => $form['sum'],
            "table" => "u_user",
            "table_id" => $u_id
            );
          $this->modelCashflow->pay($pay);
          header("Location: /user/paymentsuccess");
          exit;
        } else {
          mail("marselos@gmail.com", "Не удалось активировать услугу РАСПРОДАЖА", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: ошибка метода.");
          header("Location: /user/paymentfail");
          exit;
        }

      }

    }

    exit;
  }

  //#########################################################################################
  public function saledeactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "saledeactivate";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Режим абонплаты
    $this->view->is_rental = (count($this->view->objects) && in_array($this->view->objects[0]['obj_addr_city_uid'], $this->rentalCities));

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->sale_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'saledeactivate-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function saledeactivatebalanceprocessAction() {

    $this->view->action = "saledeactivatebalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу РАСПРОДАЖА", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $saledeactivate_result = $this->modelCashflow->saledeactivate(Array("obj_id" => $obj_id));

      //Списание средств
      if($saledeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу РАСПРОДАЖА", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function saledeactivatebalanceprocessnowAction() {

    $this->view->action = "saledeactivatebalanceprocessnow";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу РАСПРОДАЖА прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $saledeactivate_result = $this->modelCashflow->saledeactivatenow(Array("obj_id" => $obj_id));

      //Списание средств
      if($saledeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу РАСПРОДАЖА прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // sale end ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////////
  // hotorder ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  public function hotorderAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "hotorder";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->hotorder_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'hotorder-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function hotorderbalanceprocessAction() {

    $this->view->action = "hotorderbalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->hotorder_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось активировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат активации услуги
      $hotorder_result = $this->modelCashflow->hotorder(Array("obj_id" => $obj_id));

      //Списание средств
      if($hotorder_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать за услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => $obj_id,
          "transaction_name" => "hotorder",
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => ''
          );
        $this->modelCashflow->pay($pay);
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось активировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function hotorderdeactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "hotorderdeactivate";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->hotorder_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'hotorderdeactivate-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function hotorderdeactivatebalanceprocessAction() {

    $this->view->action = "hotorderdeactivatebalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $hotorderdeactivate_result = $this->modelCashflow->hotorderdeactivate(Array("obj_id" => $obj_id));

      //Списание средств
      if($hotorderdeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function hotorderdeactivatebalanceprocessnowAction() {

    $this->view->action = "hotorderdeactivatebalanceprocessnow";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $hotorderdeactivate_result = $this->modelCashflow->hotorderdeactivatenow(Array("obj_id" => $obj_id));

      //Списание средств
      if($hotorderdeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу СРОЧНОЕ ЗАСЕЛЕНИЕ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
  // hotorder end ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////////////////////
  // paramsorder ///////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  public function paramsorderAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "paramsorder";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->paramsorder_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'paramsorder-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function paramsorderbalanceprocessAction() {

    $this->view->action = "paramsorderbalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->paramsorder_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось активировать услугу ЗАПРОС ПО ПАРАМЕТРАМ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат активации услуги
      $paramsorder_result = $this->modelCashflow->paramsorder(Array("obj_id" => $obj_id));

      //Списание средств
      if($paramsorder_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать за услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => $obj_id,
          "transaction_name" => "paramsorder",
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => ''
          );
        $this->modelCashflow->pay($pay);
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось активировать услугу ЗАПРОС ПО ПАРАМЕТРАМ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function paramsorderdeactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "paramsorderdeactivate";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    $id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($id)->current();

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->paramsorder_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'paramsorderdeactivate-'.$this->view->object->obj_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function paramsorderdeactivatebalanceprocessAction() {

    $this->view->action = "paramsorderdeactivatebalanceprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу ЗАПРОС ПО ПАРАМЕТРАМ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $paramsorderdeactivate_result = $this->modelCashflow->paramsorderdeactivate(Array("obj_id" => $obj_id));

      //Списание средств
      if($paramsorderdeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу ЗАПРОС ПО ПАРАМЕТРАМ", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function paramsorderdeactivatebalanceprocessnowAction() {

    $this->view->action = "paramsorderdeactivatebalanceprocessnow";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор объекта
    $obj_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$obj_id || !$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу ЗАПРОС ПО ПАРАМЕТРАМ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID объекта или пользователя. obj_id: ".$obj_id.", u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $paramsorderdeactivate_result = $this->modelCashflow->paramsorderdeactivatenow(Array("obj_id" => $obj_id));

      //Списание средств
      if($paramsorderdeactivate_result['status']){
        header("Location: /user/paymentsuccess");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу ЗАПРОС ПО ПАРАМЕТРАМ прямо сейчас", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////
  // paramsorder end ///////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////////////////////////
  // delete object /////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  public function deleteAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "delete";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->identity = $this->identity;

    $obj_id = $this->_request->getParam('id');

    $this->view->object = $this->modelObjects->find($obj_id)->current();

    $_SESSION['delete_token'] = md5(microtime().$obj_id);

    //--------------- Logger--------------------
    $this->log->write( array('status' => 'success', 'result' => 'delete_start'), array());
  }

  //#########################################################################################
  public function deleteprocessAction() {

    $this->view->action = "deleteprocess";
    $this->view->u_user = $this->model->find($this->identity->u_id)->current();
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Идентификатор объекта
    $obj_id = $this->_request->getParam('id');
    //Идентификатор пользователя
    $u_id = $form['u_id'];
    //Токен удаления
    $delete_token = $_SESSION['delete_token'];//$form['delete_token'];

    ////Если токен удаления не совпадает
    if($delete_token != $_SESSION['delete_token']){

      //--------------- Logger--------------------
      $this->log->write( array('status' => 'error', 'result' => 'delete_process'), array());

      $_SESSION['delete_error'] = 'Возникла ошибка при удалении объекта. Попытайтесь сделать это позже или напишите в службу поддержки.';
      mail("marselos@gmail.com", "Ошибка удаления объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: токен удаления неверный. Переданная форма: ".print_r($form, true)." Сессия: ".print_r($_SESSION, true));
      header("Location: /user/paymentfail");
      exit;

    }
    //Пользователь не тот
    else if($u_id != $this->view->u_user->u_id) {

      //--------------- Logger--------------------
      $this->log->write( array('status' => 'error', 'result' => 'delete_process'), array());

      $_SESSION['delete_error'] = 'Возникла ошибка при удалении объекта. Попытайтесь сделать это позже или напишите в службу поддержки.';
      mail("marselos@gmail.com", "Ошибка удаления объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: пользователь не тот. Переданная форма: ".print_r($form, true)." Сессия: ".print_r($_SESSION, true));
      header("Location: /user/paymentfail");
      exit;

    } else {

      if($this->model->deleteItems(Array($obj_id))){

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'delete_process'), array());

        //mail("marselos@gmail.com", "Успех удаления объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Переданная форма: ".print_r($form, true)." Сессия: ".print_r($_SESSION, true));
        header("Location: /user/paymentsuccess");
        exit;

      } else {

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'error', 'result' => 'delete_process'), array());

        mail("marselos@gmail.com", "Ошибка удаления объекта", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: ошибка метода. Переданная форма: ".print_r($form, true)." Сессия: ".print_r($_SESSION, true));
        header("Location: /user/paymentfail");
        exit;
      }

    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////
  // delete object end /////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////
  // rental ///////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  //#########################################################################################
  public function rentalAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "rental";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->rental_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'rental-'.$this->identity->u_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function rentalreactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "rentalreactivate";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->rental_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'rentalreactivate-'.$this->identity->u_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function rentalbalanceprocessAction() {

    $this->view->action = "rentalbalanceprocess";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->rental_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор владельца
    $u_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось активировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат активации услуги
      $rental_result = $this->modelCashflow->rental(Array("u_id" => $u_id));

      //Списание средств
      if($rental_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать за услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => "",
          "transaction_name" => "rental",
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => $u_id
          );
        $this->modelCashflow->pay($pay);
        header("Location: /user/profile");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось активировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function rentalreactivatebalanceprocessAction() {

    $this->view->action = "rentalreactivatebalanceprocess";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->price = $this->modelCashflow->rental_price;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор владельца
    $u_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если отличаются суммы оплаты
    if($form['sum'] != $this->price){

      mail("marselos@gmail.com", "Не удалось реактивировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК. Причина: не сходятся суммы оплаты.");
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат активации услуги
      $rental_result = $this->modelCashflow->rentalreactivate(Array("u_id" => $u_id));

      //Списание средств
      if($rental_result['status']){
        //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
        //$pay             - массив операции списания
        //u_id             - пользователь, с которого необходимо списать за услугу
        //obj_id           - объект, где произошло списание за услугу
        //transaction_name - название транзакции, см. шапку
        //sum              - сумма операции
        //table            - таблица, где можно посмотреть статистику
        //table_id         - ID записи статистики
        $pay = Array(
          "u_id" => $u_id,
          "obj_id" => "",
          "transaction_name" => "rentalreactivate",
          "sum" => $form['sum'],
          "table" => "u_user",
          "table_id" => $u_id
          );
        $this->modelCashflow->pay($pay);
        header("Location: /user/profile");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось реактивировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s")." метод оплаты: списание с баланса ЛК.");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function rentaldeactivateAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "rentaldeactivate";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Стоимость услуги, в рублях
    $this->view->price = $this->modelCashflow->rental_price;

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'rentaldeactivate-'.$this->identity->u_id.'-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);
  }

  //#########################################################################################
  public function rentaldeactivatebalanceprocessAction() {

    $this->view->action = "rentaldeactivatebalanceprocess";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор владельца
    $u_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID владельца. u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $rental_deactivate_result = $this->modelCashflow->rentaldeactivate(Array("u_id" => $u_id));

      //Списание средств
      if($rental_deactivate_result['status']){
        header("Location: /user/profile");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу АБОНПЛАТА", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").".");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }

  //#########################################################################################
  public function rentaldeactivatebalanceprocessnowAction() {

    $this->view->action = "rentaldeactivatebalanceprocessnow";
    $this->view->user = $this->model->find($this->identity->u_id)->current();
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;

    //Данные
    $form = $_REQUEST;
    //Номер заказа
    $orderNumberArray = explode('-', $form['orderNumber']);
    //Идентификатор владельца
    $u_id = $orderNumberArray[1];
    //Идентификатор пользователя
    $u_id = $form['customerNumber'];

    ////Если данные верны
    if(!$u_id){

      mail("marselos@gmail.com", "Не удалось деактивировать услугу АБОНПЛАТА прямо сейчас", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").". Причина: не указан ID владельца. u_id: ".$u_id);
      header("Location: /user/paymentfail");
      exit;

    } else {

      //Результат деактивации услуги
      $rental_deactivate_result = $this->modelCashflow->rentaldeactivatenow(Array("u_id" => $u_id));

      //Списание средств
      if($rental_deactivate_result['status']){
        header("Location: /user/profile");
        exit;
      } else {
        mail("marselos@gmail.com", "Не удалось деактивировать услугу АБОНПЛАТА прямо сейчас", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").".");
        header("Location: /user/paymentfail");
        exit;
      }

    }

    exit;
  }
  /////////////////////////////////////////////////////////////////////////////////////////
  // rental end ///////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////


  //#########################################################################################
  public function thanksAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action_ctrl = "thanks";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getCities();

    //Shop ID
    $this->view->shopId = $this->modelCashflow->SHOP_ID;

    //Sc ID
    $this->view->scId = $this->modelCashflow->SCID;

    //POST Action
    $this->view->action = $this->modelCashflow->ACTION;

    //Номер заказа
    $this->view->orderNumber = 'thanks-'.md5(mktime().rand(1000, 9999).$this->identity->u_id);

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'thanks'));
  }

  //#########################################################################################
  public function paymentsuccessAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "payment_success";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getCities();

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'paymentsuccess'));
  }

  //#########################################################################################
  public function paymentfailAction() {

    $this->_helper->layout->setLayout('user-lk');
    $this->view->action = "payment_fail";
    $this->view->objects = $this->model->getUserObjects($this->identity->u_id);
    $this->view->identity = $this->identity;
    $this->view->cities = $this->modelObjects->getCities();

    //------------------- Logger ---------------------
    $this->log->write(array('status' => 'success', 'result' => 'paymentfail'));
  }
}

