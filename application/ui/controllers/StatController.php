<?php

require_once ("Util.php");

class StatController extends Zend_Controller_Action {

    //#########################################################################################
    public function init() {

        $this->model = new Ui_Model_Stat();
        $this->modelObject = new Ui_Model_Objects();
        $this->modelUser = new Ui_Model_User();
        $this->log = new Ui_Model_Log();
        $this->auth = Zend_Auth::getInstance();

        $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : false;

        $locale = new Zend_Session_Namespace('locale');
        $this->lang = $locale->curlocale['lang'];

        $ajaxContext = $this
            ->_helper
            ->getHelper('AjaxContext');

        $ajaxContext
            ->addActionContext('ajax', 'json')
            ->addActionContext('ajaxMethod', 'json')
            ->initContext('json');

        //------------------- Logger ---------------------
        $this->log->write(array('status' => 'success', 'result' => 'stat'));

    }

    //#########################################################################################
    public function indexAction() {

        $this->_helper->viewRenderer->setNoRender();
    }

    //#########################################################################################
    public function ajaxAction() {

        $this->_helper->viewRenderer->setNoRender();

        $action = $_REQUEST['action'];

        switch ($action) {
            case 'ajaxMethod':
                $this->ajaxMethod($_REQUEST);
                break;
            default:
                break;
        }

        $this->log->write( array('status' => 'success', 'result' => 'ajax'), $_REQUEST);
    }

    //#########################################################################################
    public function emailimgAction() {

        $uid = $this->_request->getParam('uid');
        $u_id = $this->_request->getParam('u_id');

        if(!empty($uid)) {
            $this->model->registerEmailStatOpen($uid, $u_id);
        }

        //Отдаем картинку
        $image = @imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].'/images/1.png');
        header('Content-type: image/png');
        imagepng($image);
        imagedestroy($image);

        exit;
    }

    // AJAX ###################################################################################
    public function ajaxMethod($request) {

    }
}

