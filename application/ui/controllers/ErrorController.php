<?php

class ErrorController extends Zend_Controller_Action
{

    //#########################################################################################
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('error');
        $this->log = new Ui_Model_Log();
    }

    //#########################################################################################
    public function indexAction()
    {
    }

    //#########################################################################################
    public function errorAction()
    {
        $objectsModel = new Ui_Model_Objects();

        // $this->view->cities = $cities = $objectsModel->getCities();

        $errors = $this->_getParam('error_handler');
 
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $exception = $errors->exception;
                
                $this->view->exception = $exception->getMessage();
                $this->view->trace = $exception->getTraceAsString();
                $this->view->request = $errors->request;                

                //Log action
                // $this->log->write( array('status' => 'error', 'result' => $exception->getMessage()) );
                $logger = Zend_Registry::get('logger');
                $logger->err($this->view->exception);
                
                break;
            default:
            {
                $exception = $errors->exception;
                
                $this->view->exception = $exception->getMessage();
                $this->view->trace = $exception->getTraceAsString();
                $this->view->request = $errors->request;                

                //Log action
                // $this->log->write( array('status' => 'error', 'result' => $exception->getMessage()) );
                $logger = Zend_Registry::get('logger');
                $logger->err($this->view->exception);
                
            break;
            }
                    
                
        }
    }

    //#########################################################################################
    public function noauthAction()
    {
        //Log action
        $this->log->write( array('status' => 'error', 'result' => 'not authorized action') );
    }

    //#########################################################################################
    public function notfoundAction()
    {
        $objectsModel = new Ui_Model_Objects();

        $this->view->cities = $cities = $objectsModel->getCities();
        // $this->view->city = (!empty($city_uid)) ? $cities['list'][$city_uid] : array();

    }

}



