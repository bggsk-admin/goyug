<?php
require_once("Util.php");

class PageController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->auth = Zend_Auth::getInstance();
        $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : false;

        $this->_helper->layout->setLayout('page');
    }

    ##########################################################################################
    public function indexAction()
    {

    }

    ##########################################################################################
    public function aboutAction()
    {

    }

    ##########################################################################################
    public function contactsAction()
    {

    }

    ##########################################################################################
    public function securepayAction()
    {

    }

    ##########################################################################################
    public function privacyAction()
    {
        $this->_helper->layout->setLayout('page');

        $city_uid = $this->_request->getParam('cityuid');

        $this->view->form = new Ui_Form_UserLogin();

        $model = new Ui_Model_Objects();
        $this->view->cities = $cities = $model->getCities();
        $this->view->city = (!empty($city_uid)) ? $cities['list'][$city_uid] : array();

        $this->view->identity = $this->identity;
        $this->view->hostname = $_SERVER['HOSTNAME'];
    }

    ##########################################################################################
    public function legalAction()
    {
        $this->_helper->layout->setLayout('page');

        $city_uid = $this->_request->getParam('cityuid');

        $this->view->form = new Ui_Form_UserLogin();

        $model = new Ui_Model_Objects();
        $this->view->cities = $cities = $model->getCities();
        $this->view->city = (!empty($city_uid)) ? $cities['list'][$city_uid] : array();

        $this->view->identity = $this->identity;
        $this->view->hostname = $_SERVER['HOSTNAME'];
    }

    ##########################################################################################
    public function ownersAction()
    {

    }

    ##########################################################################################
    public function guestsAction()
    {

    }

}


