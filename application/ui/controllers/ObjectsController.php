<?php
/**
 * Class and Function List:
 * Function list:
 * - init()
 * - indexAction()
 * - testmapAction()
 * - ajaxAction()
 * - ajaxGetObjects()
 * Classes list:
 * - ObjectsController extends Zend_Controller_Action
 */
require_once ("Util.php");

class ObjectsController extends Zend_Controller_Action {

    //#########################################################################################
    public function init() {

        $this->model = new Ui_Model_Objects();
        $this->modelStat = new Ui_Model_Stat();
        $this->log = new Ui_Model_Log();
        $this->auth = Zend_Auth::getInstance();

        $this->_helper->layout->setLayout('objects');

        $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : false;

        $locale = new Zend_Session_Namespace('locale');
        $this->lang = $locale->curlocale['lang'];

        $this->sessFilter = new Zend_Session_Namespace('filter');
        $this->sessBooking = new Zend_Session_Namespace('booking');
        $this->sessSignup = new Zend_Session_Namespace('signup');
        $this->sessSignin = new Zend_Session_Namespace('signin');

        $ajaxContext = $this
            ->_helper
            ->getHelper('AjaxContext');

        $ajaxContext
            ->addActionContext('ajax', 'json')
            ->addActionContext('ajaxGetObjects', 'json')
            ->addActionContext('ajaxGetCities', 'json')
            ->addActionContext('ajaxSetCity', 'json')
            ->addActionContext('ajaxGetSmarts', 'json')
            ->addActionContext('ajaxGetSmartsForSelect', 'json')
            ->addActionContext('ajaxGetObjTypesForSelect', 'json')
            ->addActionContext('ajaxSendObjCardView', 'json')
            ->addActionContext('ajaxShowTel', 'json')
            ->addActionContext('ajaxShowTelExt', 'json')
            ->addActionContext('ajaxShowTelExtView', 'json')
            ->addActionContext('ajaxViewHotOrder', 'json')
            ->addActionContext('ajaxSendRingQuery', 'json')
            ->addActionContext('ajaxSendOrder', 'json')
            ->addActionContext('ajaxShowSendOrder', 'json')
            ->addActionContext('ajaxSendHotOrder', 'json')
            ->addActionContext('ajaxCallback', 'json')
            ->addActionContext('ajaxStatOrders', 'json')
            ->initContext('json');

    }

    //#########################################################################################
    public function indexAction() {

        $this->view->cities = $this->model->getCities();
        $this->view->sessFilter = $this->sessFilter->data;
        $this->view->identity = $this->identity;
    }

    //#########################################################################################
    public function mapAction() {
        $city_uid = $this->_request->getParam('cityuid');

        $this->view->action = 'map';

        //Проверка десктопа и мобилы
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $isMobile = false;
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            $isMobile = $this->view->isMobile = true;
        } else {
            $isMobile = $this->view->isMobile = false;
        }

        //Мобильный и обычный лейаут
        if($isMobile) $this->_helper->layout->setLayout('objects-map-mobile');
        else          $this->_helper->layout->setLayout('objects-map');

        // $this->_helper->layout->setLayout('objects-map-mobile');

        $form = new Ui_Form_UserLogin();

        // Get all objects from DB by City UID
        $this->view->objects = $objects = $this->model->getObjectsByCityUID($city_uid);

        if(count($objects) > 0) {

            // Prepare Static GeoJSON to MapBox
            $this->view->geojson = $this->model->prepareGeoJSONfromArray($objects);

            // print_r($this->view->geojson); die;

            $this->view->form = $form;
            $this->view->cities = $cities = $this->model->getCities();

            $this->view->city = (!empty($city_uid)) ? $cities['list'][$city_uid] : array();
            $this->view->identity = $this->identity;
            $this->view->hostname = $city_uid. "." . $_SERVER['HOSTNAME'];

            // Log
            $this->log->write( array('status' => 'success', 'result' => 'map'), array("city" => $city_uid));

            //Заявки по объектам
            $this->view->statOrders = $this->modelStat->getOntop($city_uid);

            //ТОП дня
            $this->view->statOntop = $this->modelStat->getOntop($city_uid);

            //СВОБОДНА СЕГОДНЯ
            $this->view->statIsempty = $this->modelStat->getIsempty($city_uid);

            //РАСПРОДАЖА
            $this->view->statSale = $this->modelStat->getSale($city_uid);

            //Мобильный и обычный вью
            if($isMobile) $this->render('map-mobile');
            else          $this->render();


        } else {

            $this->log->write( array('status' => 'error', 'result' => 'notfound'), array("city" => $city_uid));

            header('HTTP/ 404 This Object Not Found');
            $this->_forward('notfound', 'error');

        }
    }

    //#########################################################################################
    public function viewAction() {

        $city_uid = $this->_request->getParam('cityuid');
        $obj_uid = $this->_request->getParam('objectuid');

        $this->view->action = 'view';

        //Проверка десктопа и мобилы
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $isMobile = false;
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            $isMobile = $this->view->isMobile = true;
        } else {
            $isMobile = $this->view->isMobile = false;
        }

        if(!empty($obj_uid))
        {
            $item = $this->model->getObjectByUID($obj_uid);

            if($item)
            {

                $this->_helper->layout->setLayout('object-view');

                $form = new Ui_Form_UserLogin();

                $this->view->form = $form;
                $this->view->cities = $cities = $this->model->getCities();
                $this->view->city = (!empty($city_uid)) ? $cities['list'][$city_uid] : array();

                // Stat
                $this->log->writeStat($item['obj_id'], "view");
                $this->log->write( array('status' => 'success', 'result' => 'view'), array("obj_id" => $item['obj_id'], "city" => $city_uid));

                $this->view->item = $item;

                //Пока модель пользователя подключим здесь, чтобы не дергать каждый раз в ините
                $this->modelUser = new Ui_Model_User();
                $this->view->user = $this->modelUser->find($item['obj_u_id'])->current();

            }
            else
            {
                $this->log->write( array('status' => 'error', 'result' => 'notfound'), array("obj_id" => $obj_uid, "city" => $city_uid));

                header('HTTP/ 404 This Object Not Found');
                $this->_forward('notfound', 'error');
            }

        } else {

                $this->log->write( array('status' => 'error', 'result' => 'notfound'), array("city" => $city_uid));

                header('HTTP/ 404 This Object Not Found');
                $this->_forward('notfound', 'error');

        }

    }

    //#########################################################################################
    public function notFoundAction() {

        // $this->_helper->viewRenderer->setNoRender();

        throw new Zend_Controller_Action_Exception('This page does not exist', 404);

        $this->getResponse()->clearBody();
        $this->getResponse()->clearHeaders();
        $this->getResponse()->setHttpResponseCode(404);
        $this->getResponse()->setStatusCode(404);

        return;

    }

    //#########################################################################################
    public function ajaxAction() {

        $this->_helper->viewRenderer->setNoRender();

        $action = $_REQUEST['action'];

        switch ($action) {
            case 'getobjects':
                $this->ajaxGetObjects($_REQUEST);
                break;
            case 'getstreets':
                $this->ajaxGetStreets($_REQUEST["query"]);
                break;
            case 'getsmarts':
                $this->ajaxGetSmarts();
                break;
            case 'getobjtypes':
                $this->ajaxGetObjTypes();
                break;
            case 'getsmartsselect':
                $this->ajaxGetSmartsForSelect($_REQUEST);
                break;
            case 'getobjtypesselect':
                $this->ajaxGetObjTypesForSelect($_REQUEST);
                break;
            case 'getoverlaycontent':
                $this->ajaxGetOverlayContent($_REQUEST);
                break;
            case 'getcities':
                $this->ajaxGetCities();
                break;
            case 'setcity':
                $this->ajaxSetCity($_REQUEST);
                break;
            case 'sendobjcardview':
                $this->ajaxSendObjCardView($_REQUEST);
                break;
            case 'showtel':
                $this->ajaxShowTel($_REQUEST);
                break;
            case 'showtelext':
                $this->ajaxShowTelExt($_REQUEST);
                break;
            case 'showtelextmobile':
                $this->ajaxShowTelExtMobile($_REQUEST);
                break;
            case 'showtelextview':
                $this->ajaxShowTelExtView($_REQUEST);
                break;
            case 'showtelextviewmobile':
                $this->ajaxShowTelExtViewMobile($_REQUEST);
                break;
            case 'sendringquery':
                $this->ajaxSendRingQuery($_REQUEST);
                break;
            case 'sendorder':
                $this->ajaxSendOrder($_REQUEST);
                break;
            case 'showsendorder':
                $this->ajaxShowSendOrder($_REQUEST);
                break;
            case 'viewhotorder':
                $this->ajaxViewHotOrder($_REQUEST);
                break;
            case 'hotorder':
                $this->ajaxSendHotOrder($_REQUEST);
                break;
            case 'viewparamsorder':
                $this->ajaxViewParamsOrder($_REQUEST);
                break;
            case 'paramsorder':
                $this->ajaxSendParamsOrder($_REQUEST);
                break;
            case 'callback':
                $this->ajaxCallback($_REQUEST);
                break;
            case 'statorders':
                $this->ajaxStatOrders($_REQUEST);
                break;
        }

        $this->log->write( array('status' => 'success', 'result' => 'ajax'), $_REQUEST);
    }

    //#########################################################################################
    function ajaxGetOverlayContent($params) {
        $response = array();

        $obj_id = (isset($params["obj_id"]) && !empty($params["obj_id"])) ? $params["obj_id"] : "";

        // GET Object Info
        //*****************************************************************
        $where[] = "obj.obj_id = '" . $obj_id . "'";
        $item = $this->model->getItems($where);

        if(count($item) > 0 && isset($item[0]))
        {
            $response = array(
                'name' => $item[0]['obj_name_ru'],
                'guests' => $item[0]['obj_guests'],
                'sqr' => $item[0]['obj_sqr'],
                'rooms' => $item[0]['obj_rooms'],
                'price' => $item[0]['obj_price'],
                'stayperiod' => $item[0]['per_pricename_ru'],
                'uid' => $item[0]['obj_uid'],
                'lat' => $item[0]['obj_addr_lat'],
                'lng' => $item[0]['obj_addr_lon'],
            );

        }

        // GET Object Title Image
        //*****************************************************************
        $images = $this->model->getImages($obj_id);
        if(count($images) > 0 && isset($images[0]))
        {
            $response['img'] = "/upload/object/" . $obj_id . "/x400/" . $images[0]['f_name'];
        }

        $this->view->response = $response;
    }

    //#########################################################################################
    function ajaxGetSmarts() {

        $response = array();
        $items = $this->model->getSmartfilters();

        foreach($items as $i_id => $i_val)
        {
            $response[ $i_val["sf_uid"] ]  = $i_val;
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetObjTypes() {

        $response = array();
        $items = $this->model->getObjectTypes();

        foreach($items as $i_id => $i_val)
        {
            $response[ $i_val["ot_uid"] ]  = $i_val;
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetCities() {

        $items = $this->model->getCities();
        $this->view->items = $items;
    }

    //#########################################################################################
    function ajaxGetStreets($query) {

        $response = array();
        $items = $this->model->getStreets($query);

        foreach($items as $i_id => $i_val)
        {
            $objects = (!empty($i_val["kl_objects"])) ? $i_val["kl_objects"] : 0;

            $response[]  = array(
                "name" => $i_val["kl_name"],
                "gis_name" => $i_val["kl_2gis"],
                "district" => $i_val["kl_district"],
                "objects" => $objects,
                "type_short" => $i_val["kl_type_short"]
            );
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetSmartsForSelect($params) {

        $response = array();

        $ot_code = (isset($params["ot_code"]) && !empty($params["ot_code"])) ? $params["ot_code"] : "";

        $items = $this->model->getSmartfilters($ot_code);

        foreach($items as $i_id => $i_val)
        {
            $response[]  = array(
                "value" => $i_val["sf_uid"] ,
                "text" => $i_val["sf_name_ru"] ,
            );
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxGetObjTypesForSelect($params) {

        $response = array();

        $items = $this->model->getObjectTypes();

        foreach($items as $i_id => $i_val)
        {
            $response[]  = array(
                "value" => $i_val["ot_uid"] ,
                "text" => $i_val["ot_name_ru"] ,
            );
        }

        $this->view->items = $response;
    }

    //#########################################################################################
    function ajaxSetCity($params) {
        $city_uid = (isset($params['uid']) && !empty($params['uid'])) ? $params['uid'] : "";
        $city_name = (isset($params['name']) && !empty($params['name'])) ? $params['name'] : "";

        $this->sessFilter->data['cityuid'] = $city_uid;
        $this->sessFilter->data['cityname'] = $city_name;

        $this->view->data = $city_uid;
    }

    //#########################################################################################
    function ajaxSendObjCardView($params) {

        $response = "";
        $obj_id = (isset($params['obj_id']) && !empty($params['obj_id'])) ? $params['obj_id'] : "";

        if(!empty($obj_id)) {
            $this->log->writeStat($obj_id, "view");
        }

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'preview'), array("obj_id" => $obj_id));

        $this->view->response = $response;
    }

    //#########################################################################################
    function ajaxGetObjects($params) {

        $clearsess = (isset($params['clearsess']) && !empty($params['clearsess'])) ? $params['clearsess'] : false;
        $only_locations = (isset($params['only_locations']) && intval($params['only_locations']) > 0) ? true : false;
        $no_limit = (isset($params['no_limit']) && intval($params['no_limit']) > 0) ? true : false;

        $f_page = (isset($params['f_page']) && intval($params['f_page']) > 0) ? $params['f_page'] : 1;

        //Store in session
        $this->sessFilter->data['f_page'] = $f_page;

        $limit_items = 20;



        if($clearsess) {
            // unset($this->sessFilter->data);
            // echo "erase";
        }

        $this->_helper->viewRenderer->setNoRender();

        //Generate WHERE clause
        //*****************************************************************
        $where = array();

        // Filter by City
        //*****************************************************************
        $f_city = $this->_request->getParam('f_city');
        if(!empty($f_city))
        {
            //Store in session
            $this->sessFilter->data['f_city'] = $f_city;

            $where[] = 'obj.obj_addr_city_uid = "' . $f_city . '"';
        }

        // Filter by Price Min
        //*****************************************************************
        $f_minprice = $this->_request->getParam('f_minprice');
        if(!empty($f_minprice))
        {
            //Store in session
            $this->sessFilter->data['f_minprice'] = $f_minprice;

            $where[] = "obj.obj_price >= " . $f_minprice;
        }

        // Filter by Price Max
        //*****************************************************************
        $f_maxprice = $this->_request->getParam('f_maxprice');
        if(!empty($f_maxprice))
        {
            //Store in session
            $this->sessFilter->data['f_maxprice'] = $f_maxprice;

            $where[] = "obj.obj_price <= " . $f_maxprice;
        }

        // Filter by Guests
        //*****************************************************************
        $f_guests = $this->_request->getParam('f_guests');
        if(!empty($f_guests))
        {
            //Store in session
            $this->sessFilter->data['f_guests'] = $f_guests;

            $where[] = "obj.obj_guests >= " . $f_guests;
        }

        // Filter by Street
        //*****************************************************************
        /*        $f_street = urldecode($this->_request->getParam('f_street'));
        if(!empty($f_street))
        {
            //Store in session
            $this->sessFilter->data['f_street'] = $f_street;

            $where[] = "obj.obj_addr_street LIKE '%" . $f_street . "%' ";
        }
        */

        // Filter by Object Type
        //*****************************************************************
        $f_objtype = urldecode($this->_request->getParam('f_objtype'));
        if(!empty($f_objtype))
        {
            //Store in session
            $this->sessFilter->data['f_objtype'] = $f_objtype;

            if($f_objtype != "all")
            {
                $where[] = "ot.ot_uid = '" . $f_objtype . "' ";
            }

        }

        // Filter by Arrive Date
        //*****************************************************************
        $f_arrdate = urldecode($this->_request->getParam('f_arrdate'));

        if(!empty($f_arrdate) && $f_arrdate != "Invalid date")
        {
            //Store in session
            $this->sessFilter->data['f_arrdate'] = $f_arrdate;
        } else {
            unset($this->sessFilter->data['f_arrdate']);
        }

        // Filter by Departure Date
        //*****************************************************************
        $f_depdate = urldecode($this->_request->getParam('f_depdate'));

        if(!empty($f_depdate) && $f_depdate != "Invalid date")
        {
            //Store in session
            $this->sessFilter->data['f_depdate'] = $f_depdate;
        } else {
            unset($this->sessFilter->data['f_depdate']);
        }

        // Filter by UID
        //*****************************************************************
        $f_uid = urldecode($this->_request->getParam('f_uid'));
        if(!empty($f_uid))
        {
            $where[] = "obj.obj_uid = '" . $f_uid . "' ";
        }

        //Generate ORDER clause
        //*****************************************************************
        $order = array();

        // Sort
        //*****************************************************************
        $f_sort_type = urldecode($this->_request->getParam('f_sort_type'));
        $f_sort_dest = urldecode($this->_request->getParam('f_sort_dest'));

        // Order by price
        //*****************************************************************
        if(!empty($f_sort_type) && !empty($f_sort_dest))
        {

            //Store in session
            $this->sessFilter->data['f_sort_type'] = $f_sort_type;
            $this->sessFilter->data['f_sort_dest'] = $f_sort_dest;

            if($f_sort_type == "price")
            {
                if($f_sort_dest == "asc" || $f_sort_dest == "desc")
                    $order[] = "obj.obj_price " . $f_sort_dest;

            } elseif($f_sort_type == "rate")
            {
                if($f_sort_dest == "asc" || $f_sort_dest == "desc")
                    $order[] = "obj.obj_rating " . $f_sort_dest;
            }


        } else {
            $order[] = "obj.obj_price";
        }

        // Limit Offset
        //*****************************************************************
        $limit = (!$no_limit) ? "LIMIT " . ( ($f_page - 1) * $limit_items ) . ", " . $limit_items : "";


        // GET All Filtered Objects
        //*****************************************************************
        $items = $this->model->getItems($where, $order, $limit);

        // print_r($items);die;

        //Prepare JSON Locations
        //*****************************************************************
        $locations = array();
        $index = 0;
        $html_objlist = "";

        foreach ($items as $item)
        {

            $images_arr = $this->model->getImages($item['obj_id']);
            $images = array();

            foreach ($images_arr as $i_id => $i_val)
            {
                $images[] = $i_val["f_name"];
            }

            // Get Location & Streetview Coordinates
            //*****************************************************************
            $lat = $item['obj_addr_lat'];
            $lon = $item['obj_addr_lon'];
            $lat_strview = (!empty($item['obj_addr_lat_view'])) ? $item['obj_addr_lat_view'] : $item['obj_addr_lat'];
            $lon_strview = (!empty($item['obj_addr_lon_view'])) ? $item['obj_addr_lon_view'] : $item['obj_addr_lon'];


            // Map Pin Color
            //*****************************************************************
                if ($item['ot_code'] == "AFL") $icon = '/images/ui/markers/m3.png'; //Апартамент - Квартира
                elseif ($item['ot_code'] == "MHT") $icon = '/images/ui/markers/m3-blue.png'; //Отель
                elseif ($item['ot_code'] == "HSL") $icon = '/images/ui/markers/m3.png'; //Хостел
                else { $icon = '/images/ui/markers/m3.png'; }


            // Get Rating
            //*****************************************************************
            $obj_rating = (!empty($item['obj_rating'])) ? $item['obj_rating'] : 0;

            $obj_details = array(

                    "id" => $item['obj_id'],
                    "uid" => $item['obj_uid'],

                    "name" => $item['obj_name_ru'],

                    // Object Type
                    "ot_name" => $item['ot_name_ru'],
                    "ot_code" => $item['ot_code'],

                    // Accommodation Type
                    "acc_type" => $item['acc_name_ru'],

                    // Parameters
                    "rating"         => $obj_rating,
                    "guests"        => $item['obj_guests'],
                    "sqr"             => $item['obj_sqr'],
                    "rooms"        => $item['obj_rooms'],

                    // Price
                    "price" => $item['obj_price'],
                    "stayperiod" => $item['per_pricename_ru'],

                    // Images
                    "images" => $images,

                );



            // $locations[] = array(
            $locations[$item['obj_uid']] = array(
                "index" => $index,
                "id" => $item['obj_id'],

                "name" => $item['obj_name_ru'],

                "lat" => round($lat, 6) ,
                "lng" => round($lon, 6) ,
                "lat_strview" => round($lat_strview, 6),
                "lng_strview" => round($lon_strview, 6),

                "icon" => $icon,
            );

            // if(!$only_locations) $locations[$index]["details"] = $obj_details;
            if(!$only_locations) $locations[$item['obj_uid']]["details"] = $obj_details;

            $index++;
        }

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'search') );

        $this->view->locations = $locations;
        $this->view->html_objlist = $html_objlist;
    }

    //#########################################################################################
    public function ajaxShowTel($params) {

        // print_r($params);die;

        //Отправка E-mail - только с сервера
        $this->view->showtel_result = $this->model->showTel($params);
        $status = $this->view->showtel_result['status'] ? 'success' : 'error';

        //Log
        $this->log->write( array('status' => $status, 'result' => 'showtel'), $params);
    }

    //#########################################################################################
    public function ajaxSendOrder($params) {

        // print_r($params);die;

        //Отправка E-mail
        $this->view->sendorder_result = $this->model->sendOrder($params);
        $status = $this->view->sendorder_result['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'sendorder'), $params);
    }

    //#########################################################################################
    public function ajaxShowSendOrder($params) {

        //Log
        $this->log->write( array('status' => 'success', 'result' => 'showsendorder'), $params);

    }

    //#########################################################################################
    public function ajaxStatOrders($params) {

        //Log
        //$this->log->write( array('status' => 'success', 'result' => 'statorders'), $params);

        $this->view->statOrders = $this->modelStat->getOrders($params['city_uid']);
        //$this->view->statOrders = rand(0, 10000);
    }

    //#########################################################################################
    public function ajaxShowTelExt($params) {

        //print_r($params); die;

        //Прием информации о пользователе
        $this->view->showtelext_result = $this->model->sendOrder($params);
        $status = $this->view->showtelext_result['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'showtelext'), $params);
    }

    //#########################################################################################
    public function ajaxShowTelExtMobile($params) {

        $this->view->showtelextmobile_result = $this->model->showTelExtMobile($params);
        $this->view->showtel_result = $this->model->showTel($params);
        $status = $this->view->showtelextmobile_result['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'showtelextmobile'), $params);
    }

    //#########################################################################################
    public function ajaxSendRingQuery($params) {

        //print_r($params); die;

        //Результат соединения
        $this->view->sendringquery = $this->model->sendRingQuery($params);
        $status = $this->view->sendringquery['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'sendringquery'), $params);
    }

    //#########################################################################################
    public function ajaxShowTelExtView($params) {

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'showtelextview'), $params);
    }

    //#########################################################################################
    public function ajaxShowTelExtViewMobile($params) {

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'showtelextviewmobile'), $params);
    }

    //#########################################################################################
    public function ajaxViewHotOrder($params) {

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'viewhotorder'), $params);
    }

    //#########################################################################################
    public function ajaxSendHotOrder($params) {

        // print_r($params);die;

        //Отправка E-mail
        $this->view->hotorder_result = $this->model->sendHotOrder($params);
        $status = $this->view->hotorder_result['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'sendhotorder'), $params);
    }

    //#########################################################################################
    public function ajaxViewParamsOrder($params) {

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'viewparamsorder'), $params);
    }

    //#########################################################################################
    public function ajaxSendParamsOrder($params) {

        // print_r($params);die;

        //Отправка E-mail
        $this->view->paramsorder_result = $this->model->sendParamsOrder($params);
        $status = $this->view->paramsorder_result['status'] ? 'success' : 'error';

        // Log
        $this->log->write( array('status' => $status, 'result' => 'sendparamsorder'), $params);
    }

    //#########################################################################################
    public function ajaxCallback($params) {

        //Логирование реультата звонка
        $this->view->callback_result = $this->model->logCallback($params);

        // Log
        $this->log->write( array('status' => 'success', 'result' => 'callback'), $params);
    }
}

