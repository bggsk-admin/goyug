<?
    class Ui_Form_Restriction extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ui_Form_Decorator', 'Ui/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'rst_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'rst_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rst_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rst_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'rst_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'rst_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'rst_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Restriction name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'rst_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'rst_description_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Restriction description'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'rst_description_' . $lang, 'placeholder' => '', 'rows' => 3 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
