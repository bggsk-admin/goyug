<?
    class Ui_Form_Amenity extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ui_Form_Decorator', 'Ui/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'at_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'at_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'at_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'at_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'at_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'at_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'at_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Amenity name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'at_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'at_description_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Amenity description'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'at_description_' . $lang, 'placeholder' => '', 'rows' => 3 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
