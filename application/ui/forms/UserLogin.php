<?
class Ui_Form_UserLogin extends ZendX_JQuery_Form
{

    ##########################################################################################
    public function init()
    {

        $this->addPrefixPath( 'Ui_Form_Decorator', 'Ui/Form/Decorator', 'decorator' );
        $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

        $this->setMethod("post");
        $this->setAction("/user/ajax");

        $this->addElement('text',       'u_username',       array('label' => $this->getView()->translate('Username'), 'required' => true, 'filters' => array('StripTags'), 'attribs' => array('class' => 'form-control', 'placeholder' => $this->getView()->translate('Email or mobile') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));
        $this->addElement('password',   'u_password',       array('label' => $this->getView()->translate('Password'), 'required' => false, 'attribs' => array('class' => 'form-control', 'placeholder' => $this->getView()->translate('Password') ), 'errorMessages' => array(''), 'decorators' => array('HorizontalForm') ));

	}

} #class