<?php
    class Ui_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract
    {

        public function preDispatch(Zend_Controller_Request_Abstract $request)
        {
            // set up acl
            $acl = new Zend_Acl();

            # ROLES
            $acl->addRole(new Zend_Acl_Role('guest'));
            $acl->addRole(new Zend_Acl_Role('customer'), 'guest');
            $acl->addRole(new Zend_Acl_Role('editor'), 'guest');
            $acl->addRole(new Zend_Acl_Role('owner'), 'guest');
            $acl->addRole(new Zend_Acl_Role('administrator'), 'customer');

            # RESOURCES
            $acl->add(new Zend_Acl_Resource('mvc:customers'))
            ->add(new Zend_Acl_Resource('mvc:administrators'))
            ->add(new Zend_Acl_Resource('mvc:editors'))
            ->add(new Zend_Acl_Resource('mvc:owners'));

            $acl
            ->add(new Zend_Acl_Resource('error'))
            ->add(new Zend_Acl_Resource('index'))
            ->add(new Zend_Acl_Resource('user'))
            ->add(new Zend_Acl_Resource('files'))
            ->add(new Zend_Acl_Resource('objects'))
            ->add(new Zend_Acl_Resource('help'))
            ->add(new Zend_Acl_Resource('page'))
            ->add(new Zend_Acl_Resource('cashflow'))
            ->add(new Zend_Acl_Resource('stat'))
            ;

            // -- GUEST --
            // $acl->allow('guest', null);
            $acl->allow('guest', 'error', array('index','notfound'));
            $acl->allow('guest', 'index', array('index', 'intro', 'ajax', 'sitemap', 'testmail'));
            $acl->allow('guest', 'user', array('login', 'logout', 'ajax', 'authtoken', 'subscribe'));
            $acl->allow('guest', 'objects', array('index', 'map', 'view', 'checkout', 'pay', 'ajax'));
            $acl->allow('guest', 'cashflow', array('index', 'checkorderdemo', 'paymentavisodemo', 'checkorder', 'paymentaviso'));
            $acl->allow('guest', 'stat', array('index', 'emailimg'));
            // $acl->allow('guest', 'files', array('index', 'order'));
            // $acl->allow('guest', 'help', array('index', 'uc'));
            $acl->allow('guest', 'page', array('index', 'about', 'contacts', 'securepay', 'privacy', 'guests', 'owners', 'legal'));

            // -- CUSTOMER --
            $acl->allow('customer', 'user', array('rooms', 'calendar'));
            // $acl->allow('customer', null);
            // $acl->allow('customer', 'objects',  array('index', 'ajax'));
            // $acl->allow('customer', 'index', array('index', 'ajax'));
            // $acl->allow('customer', 'help', array('index'));
            // $acl->allow('customer', 'mvc:customers');

            // -- OWNER --
            $acl->allow('owner', 'user', array('index', 'rooms', 'calendar', 'add', 'update', 'dashboard', 'profile', 'feedback', 'thanks',
                                               'payment', 'paymentsuccess', 'paymentfail',
                                               'ontop', 'ontopbalance', 'ontopbalanceprocess',
                                               'isempty', 'isemptydeactivate', 'isemptybalanceprocess', 'isemptydeactivatebalanceprocess', 'isemptydeactivatebalanceprocessnow',
                                               'rental', 'rentaldeactivate', 'rentalbalanceprocess', 'rentaldeactivatebalanceprocess', 'rentaldeactivatebalanceprocessnow', 'rentalreactivate', 'rentalreactivatebalanceprocess',
                                               /*'photomark', 'photomarkbalance', 'photomarkbalanceprocess',*/
                                               'sale', 'saledeactivate', 'salebalanceprocess', 'saledeactivatebalanceprocess', 'saledeactivatebalanceprocessnow',
                                               'hotorder', 'hotorderdeactivate', 'hotorderbalanceprocess', 'hotorderdeactivatebalanceprocess',
                                               'paramsorder', 'paramsorderdeactivate', 'paramsorderbalanceprocess', 'paramsorderdeactivatebalanceprocess',
                                               'delete', 'deleteprocess')
            );
            $acl->allow('owner', 'files', array('index', 'order'));

            // -- EDITOR --
            $acl->allow('editor', null);

            // -- ADMINISTRATOR --
            $acl->allow('administrator', null);

            // fetch the current user
            $auth = Zend_Auth::getInstance();

            if($auth->hasIdentity()) {
                $identity = $auth->getIdentity();
                $role = strtolower($identity->u_role);
            }else{
                $role = 'guest';
            }

            $controller = $request->controller;
            $action = $request->action;

            if (!$acl->isAllowed($role, $controller, $action)) {
                if ($role == 'guest') {
                    $request->setControllerName('user');
                    $request->setActionName('login');
                } else if($role == 'owner') {
                    $request->setControllerName('user');
                    $request->setActionName('index');
                } else {
                    $request->setControllerName('error');
                    $request->setActionName('noauth');
                }
            }

            # Link navigation with Acl
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);

        }

    } #class
