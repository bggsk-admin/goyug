<?php
    class Ui_Plugin_LangSelector extends Zend_Controller_Plugin_Abstract
    {

        public function preDispatch(Zend_Controller_Request_Abstract $request)
        {
            $registry = Zend_Registry::getInstance();

            // Create Session block and save the locale
            $sessLocale  = new Zend_Session_Namespace('locale');
            $paramLang = $request->getParam('lang', '');

            switch ($paramLang) {
                case "ru":
                    $currLocale = 'ru_RU';
                    break;
                case "en":
                    $currLocale = 'en_US';
                    break;
                default:
                    $currLocale = !empty($sessLocale->curlocale['locale']) ? $sessLocale->curlocale['locale'] : 'ru_RU';
            }

            $locale = new Zend_Locale($currLocale);

            // Store in session current locale
            $sessLocale->curlocale = array('region' => strtolower($locale->getRegion()), 'locale' => $currLocale, 'lang' => strtolower($locale->getLanguage()) );


            $translate  = $registry->get('Zend_Translate');
            $translate->setLocale($currLocale);

            $registry->set('Zend_Locale', $locale);
            $registry->set('Zend_Translate', $translate);


            // Store all availabel locales in session
            $locales = Zend_Registry::get('locales');
            foreach($locales as $id => $val)
            {
                $locale = new Zend_Locale($val);
                $sessLocale->locales[ strtolower($locale->getLanguage()) ] = array('region' => strtolower($locale->getRegion()), 'locale' => $val, 'lang' => strtolower($locale->getLanguage()) );
            }

        }

    } #class
