<?php

/* Модель биллинга */

/*

Названия транзакций:
add - пополнение баланса
showtel - просмотр номера телефона
showtelext - просмотр номера телефона с формой данных
sendorder - отправка заявки по объекту

*/

require_once("Util.php");

class Ui_Model_Cashflow extends Zend_Db_Table_Abstract {

  ##########################################################################################
  public $_name = 'cf_cashflow';
  public $_primary = 'cf_id';

  //Пароль Яндекс Касса
  public $SHOP_PASSWORD = 'gYti72359hibfc6Fytf8';

  //Режим безопасности
  public $SECURITY_TYPE = 'MD5'; //Или PKCS7;

  //Идентификатор магазина
  // public $SHOP_ID = '104217'; // goyugkassa
  // public $SHOP_ID = '51462'; // goyugkassa-2
  public $SHOP_ID = '67324'; // goyugkassa-3

  //Идентификатор витрины
  // public $SCID = '37184'; // goyugkassa
  // public $SCID = '48261'; // goyugkassa-2
  public $SCID = '61987'; // goyugkassa-3

  //Обработчик запросов
  //public $ACTION = 'https://demomoney.yandex.ru/eshop.xml';
  // public $ACTION = 'https://money.yandex.ru/eshop.xml'; Яндекс.Касса стала Юкассой 16 мая 2022
  public $ACTION = 'https://yoomoney.ru/eshop.xml'; // Хост ЮКассы

  //ДЕМО-режим
  public $DEMO = false;

  //Стоимость поднятия объекта в рублях
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $ontop_price = 25;

  //Стоимость СВОБОДНА СЕГОДНЯ в рублях
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $isempty_price = 5;

  //Стоимость выделения фото объекта в рублях
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $photomark_price = 0;

  //Стоимость распродажи
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $sale_price = 3;

  //Стоимость срочного заселения
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $hotorder_price = 0;

  //Стоимость запроса по параметрам
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $paramsorder_price = 0;

  //Стоимость АБОНПЛАТА в рублях
  //!!!Внимание!!! Также необходимо обновить в скрипте деактивации функции и на дашборде!!!
  public $rental_price = 100;

  //Лог транзакций
  private $transaction_log = Array();


  ##########################################################################################
  public function init() {

    //Данные входа
    $this->auth = Zend_Auth::getInstance();
    $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : false;

    //База данных
    $this->db = Zend_Registry::get('db');

    //Локаль
    $locale = new Zend_Session_Namespace('locale');

    //Язык
    $this->lang = $locale->curlocale['lang'];

    //Логи
    $this->log = new Ui_Model_Log();
  }

  //Пополнение баланса
  public function add($sum, $u_id) {

    //Сумма пополнения
    $sum = intval($sum);

    //Результат операции
    $result = false;

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $u_id,
      "cf_transaction" => "add",
      "cf_table" => "u_user",
      "cf_table_id" => "",
      "cf_sum" => $sum,
      "cf_mode" => "plus",
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);

    //Если транзакция зарегистрирована
    if($resultTransaction) {

      ////Обновление баланса пользователя
      $tbl_u_user = new Zend_Db_Table(Array(
          'name' => 'u_user',
          'primary' => 'u_id'
      ));

      //Пользователь
      $row_u_user = $tbl_u_user->find($u_id)->current();
      $row_u_user->u_money += $sum;

      //Обновление в сессии
      ////$this->identity->u_money = $row_u_user->u_money;

      //Сохранение пользователя
      $resultUserUpdate = $row_u_user->save();
      ////Обновление баланса пользователя - окончание

      //Успех транзакции
      if($resultUserUpdate) {
          $cf_success = true;
      } else {
          $cf_success = false;
      }

      //Обновляем транзакцию
      $row_cf_cashflow = $this->find($resultTransaction)->current();
      $row_cf_cashflow->cf_success = $cf_success;
      $row_cf_cashflow->save();

      //Возвращаем результат
      $result = $cf_success;
    }

    return $result;
  }

  //Платеж за услугу
  public function pay($pay) {

    //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
    //$pay             - массив операции списания
    //u_id             - пользователь, с которого необходимо списать услугу
    //obj_id           - объект, где произошло списание за услугу
    //transaction_name - название транзакции, см. шапку
    //sum              - сумма операции
    //table            - таблица, где можно посмотреть статистику
    //table_id         - ID записи статистики

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $pay['u_id'],
      "cf_obj_id" => $pay['obj_id'],
      "cf_transaction" => $pay['transaction_name'],
      "cf_table" => $pay['table'],
      "cf_table_id" => $pay['table_id'],
      "cf_sum" => $pay['sum'],
      "cf_mode" => "minus",
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);

    //Если транзакция зарегистрирована
    if($resultTransaction) {

      ////Обновление баланса пользователя
      $tbl_u_user = new Zend_Db_Table(Array(
          'name' => 'u_user',
          'primary' => 'u_id'
      ));

      //Пользователь
      $row_u_user = $tbl_u_user->find($pay['u_id'])->current();

      //Вычитаем денежку со счета
      $row_u_user->u_money -= $pay['sum'];

      //Если баланс ушел в минус - обнуляем его
      if($row_u_user->u_money < 0) $row_u_user->u_money = 0;

      //Обновление в сессии
      //$this->identity->u_money = $row_u_user->u_money;

      //Сохранение пользователя
      $resultUserUpdate = $row_u_user->save();
      ////Обновление баланса пользователя - окончание

      //Успех транзакции
      if($resultUserUpdate) {
          $cf_success = true;
      } else {
          $cf_success = false;
      }

      //Обновляем транзакцию
      $row_cf_cashflow = $this->find($resultTransaction)->current();
      $row_cf_cashflow->cf_success = $cf_success;
      $row_cf_cashflow->save();

      //Возвращаем результат
      $result = $cf_success;
    }
  }

  //Регистрация транзакции
  public function register($info) {
    //mail('marselos@gmail.com', 'Начало регистрации транзакции...', '');
    //$info = Array("u_id", "obj_id", "transaction_name", "sum");
    //$info            - массив операции
    //u_id             - пользователь, с которого необходимо списать услугу
    //obj_id           - объект, где произошло списание за услугу
    //transaction_name - название транзакции, см. шапку
    //sum              - сумма операции
    //success          - статус транзакции

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $info['u_id'],
      "cf_obj_id" => $info['obj_id'],
      "cf_transaction" => $info['transaction_name'],
      "cf_table" => "",
      "cf_table_id" => "",
      "cf_sum" => $info['sum'],
      "cf_mode" => "balanceoff",
      "cf_success" => $info['success'],
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);

    ////////////////////////////////////
    //Информация о владельце и объекте//
    ////////////////////////////////////
    $tbl_u_user = new Zend_Db_Table(Array(
        'name' => 'u_user',
        'primary' => 'u_id'
    ));

    //Пользователь
    $row_u_user = $tbl_u_user->find($info['u_id'])->current();

    //Объект
    $row_obj_object = $this->db->query("
      SELECT *
      FROM obj_object
      WHERE obj_u_id = '".$info['u_id']."'
      LIMIT 1
    ")->fetch();
    ////////////////////////////////////////////////
    //Информация о владельце и объекте - окончание//
    ////////////////////////////////////////////////


    /////////////////////////
    ///Send E-mail///////////
    /////////////////////////

    //Письмо
    $email = Array();
    $email['to'] = 'info@goyug.com';
    $email['subject'] = 'GOYUG.COM | Упали бабки в размере '.$info['sum'].' рексов';
    $email['body'] = '
    <h2>Дорогой друг!</h2>
    <p>Поздравляем!</p>
    <p>Поступил новый платеж по транзакции: '.$info['transaction_name'].' на сумму: '.$info['sum'].'</p>
    <p>ID пользователя: '.$info['u_id'].'</p>
    <p>Имя пользователя: '.$row_u_user->u_firstname.'</p>';
    if($row_obj_object) $email['body'] .= '<p>Город: '.$row_obj_object['obj_addr_city'].' ('.$row_obj_object['obj_addr_city_uid'].')</p>';
    else                $email['body'] .= '<p>Город: не определен, т.к. у владельца нет ни одного объекта</p>';

    //Процедура отправки
    try {
      $mail = new Zend_Mail('UTF-8');
      $mail->setBodyHTML($email['body']);
      $mail->setFrom('info@goyug.com', 'GoYug.com');
      $mail->setReplyTo('info@goyug.com', 'GoYug.com');
      $mail->addTo('marselos@gmail.com', '');
      $mail->addTo('160961@mail.ru', '');
      $mail->addBcc(array('info@goyug.com'));
      $mail->setSubject($email['subject']);
      $mail->send();
    } catch(Exception $ex) {

    }
    //mail('marselos@gmail.com, 160961@mail.ru', $email['subject'], $email['body']);

    /////////////////////////
    ///Send E-mail end///////
    /////////////////////////

    // DEBUG
    if(
      $info['u_id'] == 8626 && 
      $info['obj_id'] == 36897 && 
      $info['transaction_name'] == 'ontop_auto_update_30'
    ) {

      $mail = new Zend_Mail('UTF-8');
      $mail->setBodyHTML(print_r($info, true));
      // $mail->setBodyHTML(print_r(debug_backtrace(), true));
      // $mail->setBodyHTML("TEST");
      $mail->setFrom('info@goyug.com', 'GoYug.com');
      $mail->setReplyTo('info@goyug.com', 'GoYug.com');
      $mail->addTo('marselos@gmail.com', '');
      $mail->setSubject("Окончание регистрации транзакции");
      $mail->send();
    }

  }

  //Поднятие наверх
  public function ontop($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Срок выделения в днях
    $days = 3;

    //Выделено до
    $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_ontop = mktime();
      $row->obj_ontop_mark = 1;
      $row->obj_photomark = 1;
      $row->obj_ontop_deactivate = $markedTo;
      $row->save();
    }

    //Ответ
    $response = Array( "status" => true, "obj_id" => $obj_id, "markedTo" => Util::getHumanDate($markedTo, "-", true, true) );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'ontop'));

    return $response;
  }

  //Поднятие наверх автоматическое
  public function ontopAuto($params, $ontopAutoUpdateCount) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Срок выделения в днях
    $days = 3;

    //Выделено до
    $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_ontop = mktime();
      $row->obj_ontop_mark = 1;
      $row->obj_ontop_auto_update_count = $ontopAutoUpdateCount - 1;
      $row->obj_photomark = 1;
      $row->obj_ontop_deactivate = $markedTo;
      $row->save();
    }

    //Ответ
    $response = Array( "status" => true, "obj_id" => $obj_id, "markedTo" => Util::getHumanDate($markedTo, "-", true, true) );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'ontop_auto_update_'.$ontopAutoUpdateCount));

    return $response;
  }

  //Выделение фотографии
  public function photomark($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Срок выделения в днях
    $days = 3;

    //Выделено до
    $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_photomark = 1;
      $row->obj_photomark_activate = date("Y-m-d H:i:s");
      $row->obj_photomark_deactivate = $markedTo;
      $row->save();
    }

    //Ответ
    $response = Array( "status" => true, "obj_id" => $obj_id, "markedTo" => Util::getHumanDate($markedTo, "-", true, true) );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'photomark'));

    return $response;
  }


  ################################################################
  //Активация услуги СВОБОДНА СЕГОДНЯ
  public function isempty($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_isempty = 1;
      $row->obj_isempty_activate = $date_activate;
      $row->obj_isempty_nextpay = $date_nextpay;
      $row->obj_isempty_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'isempty'));

    return $response;
  }

  //Активация услуги СВОБОДНА СЕГОДНЯ при абонплате
  public function isemptyRental($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_isempty = 1;
      $row->obj_isempty_activate = $date_activate;
      $row->obj_isempty_nextpay = $date_nextpay;
      $row->obj_isempty_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'isempty'));

    return $response;
  }

  //Деактивация услуги СВОБОДНА СЕГОДНЯ
  public function isemptydeactivate($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_isempty_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'isemptydeactivate'));

    return $response;
  }

  //Деактивация услуги СВОБОДНА СЕГОДНЯ прямо сейчас
  public function isemptydeactivatenow($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_isempty = 0;
      $row->obj_isempty_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'isemptydeactivatenow'));

    return $response;
  }
  ################################################################


  ################################################################
  //Активация услуги РАСПРОДАЖА
  public function sale($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_sale = 1;
      $row->obj_sale_activate = $date_activate;
      $row->obj_sale_nextpay = $date_nextpay;
      $row->obj_sale_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'sale'));

    return $response;
  }

  //Активация услуги РАСПРОДАЖА при абонплате
  public function saleRental($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_sale = 1;
      $row->obj_sale_activate = $date_activate;
      $row->obj_sale_nextpay = $date_nextpay;
      $row->obj_sale_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'sale'));

    return $response;
  }

  //Деактивация услуги РАСПРОДАЖА
  public function saledeactivate($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_sale_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'saledeactivate'));

    return $response;
  }

  //Деактивация услуги РАСПРОДАЖА прямо сейчас
  public function saledeactivatenow($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_sale = 0;
      $row->obj_sale_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'saledeactivatenow'));

    return $response;
  }
  ################################################################


  ################################################################
  //Активация АБОНПЛАТА
  public function rental($params) {

    $u_id = intval($params['u_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'u_user',
      'primary' => 'u_id'
    ));

    //Включение
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Обновляем объекты
    if($u_id){
      $row = $tbl->find($u_id)->current();
      $row->u_rental = 1;
      $row->u_rental_activate = $date_activate;
      $row->u_rental_nextpay = $date_nextpay;
      $row->u_rental_deactivate = $date_deactivate;
      $row->u_rental_warning_7_day = 0;
      $row->u_rental_warning_3_day = 0;
      $row->u_rental_warning_1_day = 0;
      $row->u_rental_expire_3_day = 0;
      $row->u_rental_expire_7_day = 0;
      $row->u_rental_expire_14_day = 0;
      $row->u_rental_expire_21_day = 0;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "u_id" => $u_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'rental'));

    return $response;
  }

  //Реактивация АБОНПЛАТА
  public function rentalreactivate($params) {

    $u_id = intval($params['u_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'u_user',
      'primary' => 'u_id'
    ));

    //Обновляем объекты
    if($u_id){
      $row = $tbl->find($u_id)->current();
      $row->u_rental_nextpay = $row->u_rental_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "u_id" => $u_id,
      "date_nextpay" => Util::getHumanDate($row->u_rental_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($row->u_rental_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'rentalreactivate'));

    return $response;
  }

  //Деактивация АБОНПЛАТА
  public function rentaldeactivate($params) {

    $u_id = intval($params['u_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'u_user',
      'primary' => 'u_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($u_id){
      $row = $tbl->find($u_id)->current();
      $row->u_rental_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "u_id" => $u_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'rentaldeactivate'));

    return $response;
  }

  //Деактивация АБОНПЛАТА прямо сейчас
  public function rentaldeactivatenow($params) {

    $u_id = intval($params['u_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'u_user',
      'primary' => 'u_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($u_id){
      $row = $tbl->find($u_id)->current();
      $row->u_rental = 0;
      $row->u_rental_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "u_id" => $u_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'rentaldeactivatenow'));

    return $response;
  }
  ################################################################


  /////////////////////////////////////
  // СРОЧНОЕ ЗАСЕЛЕНИЕ ////////////////
  /////////////////////////////////////
  //Активация услуги СРОЧНОЕ ЗАСЕЛЕНИЕ
  public function hotorder($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_hotorder = 1;
      $row->obj_hotorder_activate = $date_activate;
      $row->obj_hotorder_nextpay = $date_nextpay;
      $row->obj_hotorder_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'hotorder'));

    return $response;
  }

  //Деактивация услуги СРОЧНОЕ ЗАСЕЛЕНИЕ
  public function hotorderdeactivate($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_hotorder_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'hotorderdeactivate'));

    return $response;
  }

  //Деактивация услуги СРОЧНОЕ ЗАСЕЛЕНИЕ прямо сейчас
  public function hotorderdeactivatenow($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_hotorder = 0;
      $row->obj_hotorder_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'hotorderdeactivatenow'));

    return $response;
  }
  ////////////////////////////////////////////////
  // СРОЧНОЕ ЗАСЕЛЕНИЕ - окончание////////////////
  ////////////////////////////////////////////////

  ///////////////////////////////////////
  //ЗАПРОС ПО ПАРАМЕТРАМ ////////////////
  ///////////////////////////////////////
  //Активация услуги ЗАПРОС ПО ПАРАМЕТРАМ
  public function paramsorder($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Выделено
    $date_activate = date("Y-m-d H:i:s");

    //Списание следующего платежа
    $date_nextpay = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Деактивация услуги
    $date_deactivate = date("Y-m-d H:i:s", mktime() + (60*60*24)*30);

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_paramsorder = 1;
      $row->obj_paramsorder_activate = $date_activate;
      $row->obj_paramsorder_nextpay = $date_nextpay;
      $row->obj_paramsorder_deactivate = $date_deactivate;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_activate" => Util::getHumanDate($date_activate, "-", true, true),
      "date_nextpay" => Util::getHumanDate($date_nextpay, "-", true, true),
      "date_deactivate" => Util::getHumanDate($date_deactivate, "-", true, true)
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'paramsorder'));

    return $response;
  }

  //Деактивация услуги ЗАПРОС ПО ПАРАМЕТРАМ
  public function paramsorderdeactivate($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_paramsorder_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'paramsorderdeactivate'));

    return $response;
  }

  //Деактивация услуги ЗАПРОС ПО ПАРАМЕТРАМ прямо сейчас
  public function paramsorderdeactivatenow($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Списание следующего платежа
    $date_nextpay = "0000-00-00 00:00:00";

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_paramsorder = 0;
      $row->obj_paramsorder_nextpay = $date_nextpay;
      $row->save();
    }

    //Ответ
    $response = Array(
      "status" => true,
      "obj_id" => $obj_id,
      "date_nextpay" => $date_nextpay
      );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'paramsorderdeactivatenow'));

    return $response;
  }
  //////////////////////////////////////////////////
  //ЗАПРОС ПО ПАРАМЕТРАМ - окончание////////////////
  //////////////////////////////////////////////////

  //Статистика расчетов
  public function getBilling($from = 0, $limit = 5, $u_id = null) {

    //Текущий пользователь
    if($u_id == null) {
      $u_id = $this->identity->u_id;
    }
    //Произвольный пользователь
    else {
      $u_id = intval($u_id);
    }

    //Статистика заявок
    $rows = $this->db->query("SELECT cf_cashflow.*
                              FROM cf_cashflow
                              /*WHERE cf_cashflow.cf_u_id = '$u_id'*/
                              ORDER BY cf_date DESC
                              LIMIT $from, $limit")->fetchAll();

    return $rows;
  }

  //Проверка контрольной суммы
  public function checkMD5($request) {

    //Формируем строку для проверки
    $str =
      $request['action'] . ";" .
      $request['orderSumAmount'] . ";" .
      $request['orderSumCurrencyPaycash'] . ";" .
      $request['orderSumBankPaycash'] . ";" .
      $request['shopId'] . ";" .
      $request['invoiceId'] . ";" .
      $request['customerNumber'] . ";" .
      $this->SHOP_PASSWORD;

    //md5 - хеш строки
    $md5 = strtoupper(md5($str));

    //Строки не равны
    if ($md5 != strtoupper($request['md5'])) {
      $this->log('wrongMD5', "Wait for md5:" . $md5 . ", recieved md5: " . $request['md5']);
      return false;
    }

    return true;
  }

  //Проверка подписи PKCS7
  public function verifySign() {

    $descriptorspec = array(0 => array("pipe", "r"), 1 => array("pipe", "w"), 2 => array("pipe", "w"));
    $certificate = 'yamoney.pem';
    $process = proc_open('openssl smime -verify -inform PEM -nointern -certfile ' . $certificate . ' -CAfile ' . $certificate,
      $descriptorspec, $pipes);
    if (is_resource($process)) {
      // Getting data from request body.
      $data = file_get_contents($this->settings->request_source); // "php://input"
      fwrite($pipes[0], $data);
      fclose($pipes[0]);
      $content = stream_get_contents($pipes[1]);
      fclose($pipes[1]);
      $resCode = proc_close($process);
      if ($resCode != 0) {
        return null;
      } else {
        //$this->logInfo("Row xml: " . $content);
        $xml = simplexml_load_string($content);
        $array = json_decode(json_encode($xml), TRUE);
        return $array["@attributes"];
      }
    }
    return null;
  }

  //Возвращает сформированный ответ для запроса checkOrder
  //Суммы, меньше $ontop_price рублей не обрабатываются
  public function checkOrder($request) {

    $response = null;

    /* Блок отключен, т.к. в нашем случае операции возможны на любые суммы 2015-10-31
    if ($request['orderSumAmount'] < $ontop_price) {
      $response = $this->buildResponse($request['action'], $request['invoiceId'], $ontop_price, "The amount should be more than $ontop_price rubles.");
    } else {
      $response = $this->buildResponse($request['action'], $request['invoiceId'], 0);
    }*/

    $response = $this->buildResponse($request['action'], $request['invoiceId'], 0);

    $this->log('resultMethodCheckOrder', $response);

    return $response;
  }

  //Возвращает сформированный ответ для запроса paymentAviso
  public function paymentAviso($request) {

    $response = $this->buildResponse($request['action'], $request['invoiceId'], 0);
    $this->log('resultMethodPaymentAviso', $response);

    //Бизнес-логика
    if(!$this->DEMO){

      //Номер заказа
      //составляется из названия транзакции, номера объекта и хеша md5()
      //например, ontop-2455-hygg6gryg6543fgrstrhje6trgfnvht62
      $orderNumber = $request['orderNumber'];
      $orderNumberArray = explode('-', $orderNumber);

      //Кол-во поднятий
      if(isset($request['ontopAutoUpdateCount'])) $ontopAutoUpdateCount = $request['ontopAutoUpdateCount'];
      else                                        $ontopAutoUpdateCount = 1;

      //Сумма платежа
      $orderSum = $request['orderSumAmount'];

      //Идентификатор плательщика - предстваляет из себя u_id
      $u_id = $request['customerNumber'];

      //Название транзакции
      $transaction_name = $orderNumberArray[0];

      //id объекта
      $obj_id = $orderNumberArray[1];

      //Действуем
      switch ($transaction_name) {

        //Поднятие объекта
        case 'ontop':

          //Выполняем транзакцию
          // АХТУНГ! Поменять комменты в случае некорректной работы транзакции!
          //$result = $this->ontop(Array("obj_id" => $obj_id));
          $result = $ontopAutoUpdateCount == 1 ? $this->ontop(Array("obj_id" => $obj_id)) : $this->ontopAuto(Array("obj_id" => $obj_id), $ontopAutoUpdateCount);

          ////Регистрация транзакции
          //Ошибка поднятия
          if(!$result['status']){
            $success = false;
            mail("marselos@gmail.com", "Не удалось поднять объект", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s"));
          }
          //Успех поднятия
          else {

            $success = true;

            /////////////////////////
            ///Send E-mail///////////
            /////////////////////////

            //Дата отправки письма
            $email_date_send = date("Y-m-d H:i:s");

            //Достаем пользователя
            $tbl_u_user = new Zend_Db_Table(Array(
                'name' => 'u_user',
                'primary' => 'u_id'
            ));

            //Пользователь
            $row_u_user = $tbl_u_user->find($u_id)->current();

            //Если указан E-mail - отправляем уведомление
            if($row_u_user->u_email){
              switch ($ontopAutoUpdateCount) {

                case 1:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 3 дня в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Выделен до: '.$result['markedTo'].'</p>';

                  break;

                case 2:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 6 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                case 3:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 9 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                case 5:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 15 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                case 10:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 30 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                case 20:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 60 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                case 30:
                  //Письмо
                  $email = Array();
                  $email['to'] = $row_u_user->u_email;
                  $email['subject'] = 'GOYUG.COM | Поднятие объекта';
                  $email['body'] = '
                  <h2>Поздравляем!</h2>
                  <p>Ваш объект успешно поднят и будет выделен на 90 дней в списке и на карте.</p>
                  <p>Дата поднятия: '.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).'</p>
                  <p>Следующее автоподнятие: '.$result['markedTo'].'</p>
                  <p>Осталось автоподнятий: '.($ontopAutoUpdateCount - 1).'</p>';

                  break;

                default:
                  # code...
                  break;
              }
            }
            //Если не указан E-mail - оптравляем оповещение
            else {
              //Письмо
              $email = Array();
              $email['to'] = 'info@goyug.com';
              $email['subject'] = 'GOYUG.COM | Поднятие объекта - E-mail владельца не указан';
              $email['body'] = '
              <h2>Внимание!</h2>
              <p>Владельцу с ID '.$u_id.' не отправлено уведомление о поднятии объекта, т.е. у него не указан E-mail.</p>';
            }

            //Процедура отправки
            try {
              $mail = new Zend_Mail('UTF-8');
              $mail->setBodyHTML($email['body']);
              $mail->setFrom('info@goyug.com', 'GoYug.com');
              $mail->setReplyTo('info@goyug.com', 'GoYug.com');
              $mail->addTo($email['to'], '');
              $mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
              $mail->setSubject($email['subject']);
              $mail->send();
            } catch(Exception $ex) {

            }
            //mail($email['to'], $email['subject'], $email['body']);
            //mail('marselos@gmail.com, 160961@mail.ru', $email['subject'], $email['body']);

            ////Сохраняем письмо
            //Идентификатор письма
            // $email_uid = md5(mktime().rand(0, 1000));
            // //Путь к корню писем
            // $email_path = $_SERVER['DOCUMENT_ROOT'].'/data/email';
            // //Дата отправка
            // $email_date_send_for_path = explode("-", substr($email_date_send, 0, strpos($email_date_send, ' ')));
            // //Корневая папка
            // if(!file_exists($email_path))
            //     mkdir($email_path);
            // //Папка заявок
            // if(!file_exists($email_path.'/sendorder'))
            //     mkdir($email_path.'/sendorder');
            // //Папка заявок - по датам ГОД
            // if(!file_exists($email_path.'/sendorder/'.$email_date_send_for_path[0]))
            //     mkdir($email_path.'/sendorder/'.$email_date_send_for_path[0]);
            // //Папка заявок - по датам МЕСЯЦ
            // if(!file_exists($email_path.'/sendorder/'.$email_date_send_for_path[0].'/'.$email_date_send_for_path[1]))
            //     mkdir($email_path.'/sendorder/'.$email_date_send_for_path[0].'/'.$email_date_send_for_path[1]);
            // //Папка заявок - по датам ДЕНЬ
            // if(!file_exists($email_path.'/sendorder/'.$email_date_send_for_path[0].'/'.$email_date_send_for_path[1].'/'.$email_date_send_for_path[2]))
            //     mkdir($email_path.'/sendorder/'.$email_date_send_for_path[0].'/'.$email_date_send_for_path[1].'/'.$email_date_send_for_path[2]);
            // //Сохраняем письмо
            // file_put_contents($email_path.'/sendorder/'.$email_date_send_for_path[0].'/'.$email_date_send_for_path[1].'/'.$email_date_send_for_path[2].'/'.$email_uid.'.html', $email_body);
            ////Сохраняем письмо - окончание
            /////////////////////////
            ///Send E-mail end///////
            /////////////////////////
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => $ontopAutoUpdateCount == 1 ? 'ontop' : 'ontop_auto_update_'.$ontopAutoUpdateCount,
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        //Пополнение баланса
        case 'payment':

          //Выполняем транзакцию
          $result = $this->add($orderSum, $u_id);

          ////Регистрация транзакции
          //Ошибка пополнения баланса
          if(!$result){
            $success = false;
            mail("marselos@gmail.com", "Не удалось зафиксировать пополнение баланса", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").", сумма пополнения: ".$orderSum);
          }
          //Успех пополнения баланса
          else {
            $success = true;
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => '',
            "transaction_name" => 'payment',
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        //Благодарочка
        case 'thanks':

          //Выполняем транзакцию
          $result = true;

          ////Регистрация транзакции
          //Ошибка пополнения баланса
          if(!$result){
            $success = false;
            mail("marselos@gmail.com", "Не удалось зафиксировать благодарность", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").", сумма оплаты: ".$orderSum);
          }
          //Успех пополнения баланса
          else {
            $success = true;
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => '',
            "transaction_name" => 'thanks',
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        default:
          # code...
          break;
      }
    }

    return $response;
  }

  //Формирует XML-ответ
  // * @param  string $functionName - запросы "checkOrder" или "paymentAviso"
  // * @param  string $invoiceId - номер транзакции, генерируемой Яндекс Кассой
  // * @param  string $result_code - код ответа
  // * @param  string $message - сообщение об ошибке - может быть null
  // * @return string - сформированный XML-ответ
  public function buildResponse($functionName, $invoiceId, $result_code, $message = null) {

    try {

      //$performedDatetime = Utils::formatDate(new DateTime());
      //$performedDatetime = new DateTime();
      //$performedDatetime = $date->format("c");
      $performedDatetime = date("c");
      $response = '<?xml version="1.0" encoding="UTF-8"?><' . $functionName . 'Response performedDatetime="' . $performedDatetime .
        '" code="' . $result_code . '" ' . ($message != null ? 'message="' . $message . '"' : "") . ' invoiceId="' . $invoiceId . '" shopId="' . $this->SHOP_ID . '"/>';

      $this->log('buildResponse', $response);

      return $response;

    } catch (\Exception $e) {

      $this->log('buildResponseError', $e);
    }
    return null;
  }

  //Вывод ответа
  public function sendRequest($response) {
    echo $response;
  }

  //Логирование
  public function log($type, $info){

    //Запись файла
    $this->transaction_log[] = $type.' | '.$info.' | '.microtime();
  }

  //Закрытие лога
  public function logClose(){

    ////Путь к папке с логами Яндекс.Касса
    //Локально
    if($_SERVER['HTTP_HOST'] == 'goyug'){

      $path = "../cashflow/";
      if(!file_exists($path)) mkdir($path);

      $path = "../cashflow/yandex/";
      if(!file_exists($path)) mkdir($path);
    }
    //На сервере
    else {

      $path = "/var/www/goyug/cashflow/";
      if(!file_exists($path)) mkdir($path);

      $path = "/var/www/goyug/cashflow/yandex/";
      if(!file_exists($path)) mkdir($path);

    }
    ////Путь к папке с логами Яндекс.Касса - окончание

    //Сшиваем лог в файл
    $string = '';
    foreach ($this->transaction_log as $key => $value) {
      $string .= $value."\n";
    }

    //Сохраняем
    $microtime = microtime();
    $filename = 'log_'.date("Y_m_d_H_i_s_").substr($microtime, strpos($microtime, ' ') + 1).'.log';
    file_put_contents($path.$filename, $string);
  }
}
