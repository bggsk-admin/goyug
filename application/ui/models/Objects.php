<?php
/**
* Class and Function List:
* Function list:
* - init()
* - getItems()
* - getPrices()
* - getImages()
* - getValues()
* - getStayPeriods()
* Classes list:
* - Ui_Model_Objects extends Zend_Db_Table_Abstract
*/
class Ui_Model_Objects extends Zend_Db_Table_Abstract {

	public $_name = 'obj_object';
	public $_primary = 'obj_id';


	##########################################################################################
	public function init()
	{

		//Из ai Object.php
		$this->auth = Zend_Auth::getInstance();

		$this->db = Zend_Registry::get('db');
		$locale = new Zend_Session_Namespace('locale');

		$this->lang = $locale->curlocale['lang'];

		$this->pref = "obj_";
		$this->id = $this->pref . 'id';
		$this->name = $this->pref . 'name_' . $this->lang;

		//Смс-шлюз
		$this->sms_gateway = 'mainsms';
		//$this->sms_gateway = 'iqsms';

		$this->log = new Ui_Model_Log();
		$this->cf_cashflow = new Ui_Model_Cashflow();

	      // Города с АБОНПЛАТОЙ
	      $this->rental_cities = array(
			"kislovodsk", 
			"nnovgorod", 
			"sochi", "sevastopol", "spb", 
			"krasnodar", "saratov", "tyumen", 
			"novosibirsk", "ekaterinburg", "perm", 
			"kazan", "ufa", "voronezh", 
			"moscow", "omsk", "rostovnadonu", "volgograd",
      "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
      "zheleznovodsk",
      "pyatigorsk", "yalta", "yaroslavl", "tomsk",
      "bryansk", "tula", "magnitogorsk", "ivanovo",
      "khabarovsk", "vladimir", "penza", "kirov", "vologda",
      "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
      "astrakhan", "tolyatty", "orel", "orenburg", "tver",
      "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
		);

	}

	##########################################################################################
	public function getSmartfilters($ot_code = "")
	{
		$db = Zend_Registry::get( 'db' );

		$where_ot_code = (!empty($ot_code)) ? "AND ot.ot_code = '".$ot_code."' " : "";

		//SQL query
		//*****************************************************************
		$query = '
			SELECT sf.sf_uid, sf.sf_name_ru, sf.sf_name_en, sf.sf_minprice, sf.sf_maxprice
			FROM sf_smartfilters AS sf
			LEFT JOIN ot_object_type AS ot
			ON sf.ot_id = ot.ot_id
			WHERE sf.sf_enable=1
			' . $where_ot_code . '
			ORDER BY ot.ot_order, sf.sf_order_navbar
		';

		$data = $db->query($query)->fetchAll();

		// Log
		$this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

		return $data;
	}

	//#########################################################################################
	public function getCities()
	{
		$result = array();

		$data = $this->db->query("
			SELECT *
			FROM ct_city
			WHERE ct_enable = 1
			AND ct_objects > 0
			ORDER BY ct_name_ru
		")->fetchAll();

		foreach($data as $d_id => $d_val)
		{

			$center_lat = (!empty($d_val["ct_center_lat"])) ? $d_val["ct_center_lat"] : $d_val["ct_lat"];
			$center_lng = (!empty($d_val["ct_center_lng"])) ? $d_val["ct_center_lng"] : $d_val["ct_lng"];
			$zoom = (!empty($d_val["ct_zoom"])) ? $d_val["ct_zoom"] : 14;

			$result["list"][ $d_val["ct_uid"] ] = array(
				"id" => $d_val["ct_id"],
				"uid" => $d_val["ct_uid"],
				"name" => $d_val["ct_name_ru"],
				"name_pr" => $d_val["ct_name_ru_pr"],
				"objects" => $d_val["ct_objects"],
				"lat" => $center_lat,
				"lng" => $center_lng,
				"zoom" => $zoom,
				"minprice" => $d_val["ct_minprice"],
				"maxprice" => $d_val["ct_maxprice"],
				"maplink" => "http://" . $d_val["ct_uid"] . "." . $_SERVER['HOSTNAME'],
				"yandex_web" => $d_val["ct_yandex_webmaster"],
			);

			$result["alphabet"][ mb_substr($d_val["ct_name_ru"], 0, 1, 'utf-8') ][] = $d_val["ct_uid"];
		}

		ksort($result["alphabet"]);

		// print_r($result);die;

		return $result;
		// return "OK";
	}

	//#########################################################################################
	public function getAllCities()
	{
		$result = array();

		$data = $this->db->query("
			SELECT *
			FROM ct_city
			WHERE ct_enable = 1
			ORDER BY ct_name_ru
		")->fetchAll();

			// AND ct_objects > 0

		foreach($data as $d_id => $d_val)
		{

			$center_lat = (!empty($d_val["ct_center_lat"])) ? $d_val["ct_center_lat"] : $d_val["ct_lat"];
			$center_lng = (!empty($d_val["ct_center_lng"])) ? $d_val["ct_center_lng"] : $d_val["ct_lng"];
			$zoom = (!empty($d_val["ct_zoom"])) ? $d_val["ct_zoom"] : 14;

			$result["list"][ $d_val["ct_uid"] ] = array(
				"id" => $d_val["ct_id"],
				"uid" => $d_val["ct_uid"],
				"name" => $d_val["ct_name_ru"],
				"name_pr" => $d_val["ct_name_ru_pr"],
				"objects" => $d_val["ct_objects"],
				"lat" => $center_lat,
				"lng" => $center_lng,
				"zoom" => $zoom,
				"minprice" => $d_val["ct_minprice"],
				"maxprice" => $d_val["ct_maxprice"],
				"maplink" => "http://" . $d_val["ct_uid"] . "." . $_SERVER['HOSTNAME'],
				"yandex_web" => $d_val["ct_yandex_webmaster"],
			);

			$result["alphabet"][ mb_substr($d_val["ct_name_ru"], 0, 1, 'utf-8') ][] = $d_val["ct_uid"];
		}

		ksort($result["alphabet"]);

		// print_r($result);die;

		return $result;
		// return "OK";
	}

	##########################################################################################
	public function getObjectTypes()
	{
		$db = Zend_Registry::get( 'db' );

		// $where_ot_code = (!empty($ot_code)) ? "AND ot.ot_code = '".$ot_code."' " : "";

		//SQL query (All items)
		//*****************************************************************
		$query = '
			SELECT MIN(ot.ot_minprice) AS minprice, MAX(ot.ot_maxprice) AS maxprice
			FROM ot_object_type AS ot
			WHERE ot.ot_enable=1
		';

		$all_items = $db->query($query)->fetchAll();

		$data_all[] = array(
			"ot_uid" => "all",
			"ot_name_ru" => "Все объекты",
			"ot_minprice" => $all_items[0]["minprice"],
			"ot_maxprice" => $all_items[0]["maxprice"]
		);


		//SQL query
		//*****************************************************************
		$query = '
			SELECT *
			FROM ot_object_type AS ot
			WHERE ot.ot_enable=1
			ORDER BY ot.ot_order
		';

		$data = $db->query($query)->fetchAll();

		$res_data = array_merge($data_all, $data);

		// Log
		$this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

		// print_r($res_data);

		return $res_data;
	}

	##########################################################################################
	public function getStreets($query)
	{
		$db = Zend_Registry::get( 'db' );

		//SQL query
		//*****************************************************************
		$query = '
			SELECT *
			FROM kl_kladr
			WHERE kl_name LIKE "' . $query . '%"
			ORDER BY kl_objects DESC, kl_name
		';

		$data = $db->query($query)->fetchAll();

		// Log
		$this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

		return $data;
	}

	##########################################################################################
	public function getItems($where = array(), $order = array(), $limit = "")
	{

		$db = Zend_Registry::get( 'db' );

		//Add default WHERE conditions
		//*****************************************************************
		$where[] = "obj.obj_enable = 1";
		$where[] = "v.v_table = 'ot_object_type'";

		//Generate WHERE clause
		//*****************************************************************
		if (!empty($where) && count($where) > 0)
		{
			$where = " WHERE " . implode(" AND ", $where);
		}

		//Generate ORDER clause
		//*****************************************************************
		if (!empty($order) && count($order) > 0)
		{
			$order = " ORDER BY " . implode(", ", $order);
		} else {
			$order = "";
		}

		//SQL query
		//*****************************************************************
		$query = '
			SELECT *

			FROM obj_object AS obj

			LEFT JOIN per_period AS per
			ON per.per_id = obj.obj_price_period

			LEFT JOIN v_value AS v
			ON v.obj_id = obj.obj_id

			LEFT JOIN ot_object_type AS ot
			ON ot.ot_id = v.v_key

			LEFT JOIN acc_accommodation AS acc
			ON acc.acc_id = obj.obj_acc_type

			LEFT JOIN u_user AS u
			ON u.u_id = obj.obj_u_id

			' . $where . '
			' . $order . '
			' . $limit . '

		';
		// LIMIT 0, 20


		// Log
		$this->log->write( array('status' => 'success', 'result' => 'sql'), $query );

		$rows = $db->query($query)->fetchAll();

		return $rows;
	}

	##########################################################################################
	public function getObjectsByCityUID($city_uid) {

		$db = Zend_Registry::get( 'db' );

		$rows = array();

		if(!empty($city_uid)) {

			//Абонплата
			if(in_array($city_uid, $this->rental_cities)) $rental = 'AND u.u_rental';
			else 						  			  	 $rental = '';

			$query = $this->db->query("
				SELECT obj.obj_id, obj.obj_uid,
				obj.obj_name_ru,
				obj.obj_price, obj.obj_rooms, obj.obj_guests, obj.obj_sqr, obj.obj_rating,
				obj.obj_checkin, obj.obj_checkout,
				obj.obj_addr_city, obj.obj_addr_street, obj.obj_addr_number, obj.obj_addr_floor, obj.obj_addr_lat, obj.obj_addr_lon,
				obj.obj_price_minstay, obj.obj_sleepers,
				obj.obj_ontop, obj.obj_ontop_mark,

				obj_isempty, obj_sale, obj_photomark,
				obj_sale, obj_hotorder,
				obj_phantom,

				acc.acc_name_ru, acc.acc_uid,
				ot.ot_name_ru, ot.ot_code, ot.ot_uid,

				IFNULL(osp.osp_count, 0) as views_total,
				IFNULL(osd.osd_count, 0) as views_today

				FROM obj_object AS obj

				LEFT JOIN per_period AS per
				ON per.per_id = obj.obj_price_period

				LEFT JOIN acc_accommodation AS acc
				ON acc.acc_id = obj.obj_acc_type

				LEFT JOIN u_user AS u
				ON u.u_id = obj.obj_u_id

				LEFT JOIN v_value AS v
				ON v.obj_id = obj.obj_id

				LEFT JOIN ot_object_type AS ot
				ON v.v_key = ot.ot_id

				LEFT JOIN osp_objstat_permanent AS osp
				ON osp.osp_obj_id = obj.obj_id

				LEFT JOIN osd_objstat_daily AS osd
				ON (osd.osd_obj_id = obj.obj_id AND osd.osd_date = CURDATE())

				WHERE obj.obj_addr_city_uid = '".$city_uid."'
				AND obj.obj_enable = 1
				AND v.v_table = 'ot_object_type'
				".$rental."

				ORDER BY obj_ontop_mark DESC, obj_ontop DESC, obj_price");

			$rows = $query->fetchAll();

			//Картинки
			$images = $this->getObjectImagesByCityUID($city_uid);

			//Просмотры
			//$views = $this->getObjectViewsByCityUID($city_uid);

			foreach($rows as $key => $val) {

				//Разбор картинок
				$rows[$key]['obj_title_image'] = (isset($images[$val['obj_id']][0])) ? $images[$val['obj_id']][0] : "";
				$rows[$key]['obj_images'] = (isset($images[$val['obj_id']])) ? $images[$val['obj_id']] : "";

				//Календарь
				// 27/12 - Закоментил. Подозреваю что это нагружает сильно базу.
				// $calendar = $this->getObjectCalendar($val['obj_id']);
				$calendar = "";

				$rows[$key]['calendar'] = $calendar;

				//Ссылка на объект
				$rows[$key]['link'] = 'http://' . $city_uid . '.' . $_SERVER['HOSTNAME'] . '/' . $val['obj_id'];

				//Просмотры
				//$rows[$key]['views'] = (isset($views[$val['obj_id']])) ? $views[$val['obj_id']] : Array("total" => 0, "today" => 0);
			}

			return $rows;
		}
	}

	##########################################################################################
	public function getObjectViewsByCityUID($city_uid) {

		/* depricated 2016 02 17
		$db = Zend_Registry::get( 'db' );
		$objects = Array();
		$views = Array();

		if(!empty($city_uid)) {

			//Просмотры
			$query = $this->db->query("
				SELECT os_obj_id as obj_id,
					SUM(os_val) as total,
					SUM(IF(os_date LIKE '%".date("Y-m-d")."%', os_val, 0)) as today
				FROM
					os_objstat
				WHERE
					os_action = 'view'
				GROUP BY
					os_obj_id
				ORDER BY obj_id");

			$objects = $query->fetchAll();

			foreach($objects as $key => $object) {

				$views[$object['obj_id']]['total'] = $object['total'];
				$views[$object['obj_id']]['today'] = $object['today'];
			}
		}

		return $views;
		*/

		$db = Zend_Registry::get( 'db' );
		$objects = Array();
		$views = Array();

		if(!empty($city_uid)) {

			//Просмотры
			$query = $this->db->query("
			SELECT osp_id, osp_obj_id, osp_count as total, SUM(IF(osd_date LIKE '%".date("Y-m-d")."%', osd_count, 0)) as today
			FROM osp_objstat_permanent
			LEFT JOIN osd_objstat_daily ON osd_obj_id = osp_obj_id
			GROUP BY osp_obj_id
			ORDER BY today DESC");

			$objects = $query->fetchAll();

			foreach($objects as $key => $object) {

				$views[$object['osp_obj_id']]['total'] = $object['total'];
				$views[$object['osp_obj_id']]['today'] = $object['today'];
			}
		}

		return $views;
	}

	##########################################################################################
	public function getObjectImagesByCityUID($city_uid) {

		$db = Zend_Registry::get( 'db' );
		$rows = array();
		$result = array();

		if(!empty($city_uid)) {

			$query = $this->db->query("
				SELECT DISTINCT obj.obj_id, f.f_name, f.f_order
				FROM f_files AS f

				LEFT JOIN obj_object AS obj
				ON obj.obj_id = f.f_uid

				WHERE f.f_ucid = 'object'
				AND obj.obj_addr_city_uid = '".$city_uid."'

				ORDER BY f.f_order, f.f_datetime");

			$rows = $query->fetchAll();

			foreach($rows as $key => $val) {
				$result[$val['obj_id']][] = $val['f_name'];
			}

			return $result;
		}
	}

	##########################################################################################
	public function prepareGeoJSONfromArray($objects) {
		$result = array();
		$result_tops = array();
		$item = array();

		if(!empty($objects) && count($objects) > 0) {
			foreach($objects as $key => $val) {

				$pin_style = ($val['obj_ontop_mark']) ? "top" : "regular";
				// $pin_style .= ($val['obj_isempty']) ? "-empty" : "";

				if($val['obj_ontop_mark'])
				{
					if($val['obj_isempty'])
					{
						$icon = array(
							"className" => array("marker-pin pin-top-isempty"),
							"iconSize" => array(59, 59),
							"iconAnchor" => array(25, 25),
							"popupAnchor" => array(-6, -25),
							"html" => $val['obj_price'],
						);
					} else {
						$icon = array(
							"className" => array("marker-pin pin-top"),
							"iconSize" => array(59, 59),
							"iconAnchor" => array(25, 25),
							"popupAnchor" => array(-6, -25),
							"html" => $val['obj_price'],
						);

					}

				}
				elseif($val['obj_sale'])
				{
					if($val['obj_isempty'])
					{
						$icon = array(
							"className" => array("marker-pin pin-sale-isempty"),
							"iconSize" => array(59, 59),
							"iconAnchor" => array(25, 25),
							"popupAnchor" => array(-6, -25),
							"html" => $val['obj_price'],
						);
					} else {
						$icon = array(
							"className" => array("marker-pin pin-sale"),
							"iconSize" => array(59, 59),
							"iconAnchor" => array(25, 25),
							"popupAnchor" => array(-6, -25),
							"html" => $val['obj_price'],
						);
					}
				}
				elseif($val['obj_isempty'])
				{
					$icon = array(
						"className" => array("marker-pin pin-isempty"),
						"iconSize" => array(40, 46),
						"iconAnchor" => array(25, 25),
						"popupAnchor" => array(-6, -25),
						"html" => $val['obj_price'],
					);
				}
				else {
					$icon = array(
						"className" => array("marker-pin pin-regular"),
						"iconSize" => array(40, 46),
						"iconAnchor" => array(25, 25),
						"popupAnchor" => array(-6, -25),
						"html" => $val['obj_price'],
					);
				}



				$item = array(
					"type" => "Feature",
					"geometry" => array(
						"type" => "Point",
						"coordinates" => array(round($val['obj_addr_lon'], 6), round($val['obj_addr_lat'], 6)),
					), // geometry
					"properties" => array(
						"id" => $val['obj_id'],
						"top" => $val['obj_ontop_mark'],
						"uid" => $val['obj_uid'],
						"title" => $val['obj_name_ru'],
						"price" => $val['obj_price'],
						"rooms" => $val['obj_rooms'],
						"guests" => $val['obj_guests'],
						"sqr" => $val['obj_sqr'],
						"rating" => $val['obj_rating'],
						"checkin" => $val['obj_checkin'],
						"checkout" => $val['obj_checkout'],
						"addr_city" => $val['obj_addr_city'],
						"addr_street" => $val['obj_addr_street'],
						"addr_number" => $val['obj_addr_number'],
						"addr_floor" => ($val['obj_addr_floor'] > 0) ? $val['obj_addr_floor'] : "",
						"acc_name" => $val['acc_name_ru'],
						"minstay" => $val['obj_price_minstay'],
						"ot_name" => $val['ot_name_ru'],
						"ot_uid" => $val['ot_uid'],
						"isempty" => $val['obj_isempty'],
						"sleepers" => $val['obj_sleepers'],
						"images" => $val['obj_images'],
						"calendar" => $val['calendar'],
						"link" => $val['link'],
						"sale" => $val['obj_sale'],
						"phantom" => $val['obj_phantom'],

						/* "marker-color" => "#FB4151",
						"marker-size" => "large",
						"marker-symbol" => "",
						*/
						"icon" => $icon
						// icon


					) // properties
				); // item

					/*
					if($val['obj_ontop_mark'])
					$result_tops[] = $item;
					else
					$result[] = $item;
					*/
				  $result_total[] = $item;

			} // foreach

			// $result_total = array_merge($result_tops, $result);

			$result_total = json_encode($result_total);
		}

		return $result_total;
	}

	##########################################################################################
	public function getObjectByUID($obj_uid) {
		$db = Zend_Registry::get( 'db' );

		if(!empty($obj_uid))
		{
			$query = $this->db->query("
				SELECT *

				FROM obj_object AS obj

				LEFT JOIN per_period AS per
				ON per.per_id = obj.obj_price_period

				LEFT JOIN acc_accommodation AS acc
				ON acc.acc_id = obj.obj_acc_type

				LEFT JOIN u_user AS u
				ON u.u_id = obj.obj_u_id

				LEFT JOIN v_value AS v
				ON v.obj_id = obj.obj_id

				LEFT JOIN ot_object_type AS ot
				ON v.v_key = ot.ot_id

				WHERE obj.obj_uid = '".$obj_uid."' OR  obj.obj_id = '".$obj_uid."'
				AND obj.obj_enable = 1
				AND v.v_table = 'ot_object_type'
			");

			$rows = $query->fetchAll();

			if(!empty($rows) && isset($rows[0]))
			{
				$item = $rows[0];
				$obj_id = $item['obj_id'];

				$images = $this->getImages($obj_id);

				foreach ($images as $key => $value)
				{
					$item['images']['thmb'][] = "/upload/object/" . $obj_id . "/x100/" . $value;
					$item['images']['full'][] = "/upload/object/" . $obj_id . "/x800/" . $value;
				}

				$item['equipment'] = $this->getValuesName("eq_equipment", "eq", $obj_id);
				$item['amenity'] = $this->getValuesName("at_amenity", "at", $obj_id);
				$item['restriction'] = $this->getValuesName("rst_restriction", "rst", $obj_id);
				$item['calendar'] = $this->getObjectCalendar($obj_id);
				$item['calendarJSON'] = json_encode( $item['calendar'] );

				return $item;
			} else {
				return false;
			}

		}

	}

	##########################################################################################
	public function getObjectCalendar($object_id = 0) {

		$rows = array();
		$result = array();

		$query = $this->db->query("
			SELECT cal_startdate
			FROM cal_calendar
			WHERE obj_id = '" . $object_id . "'
			AND cal_status = 'closed'
			AND cal_enable = 1
		");

		$rows = $query->fetchAll();

		foreach ($rows as $key => $value) {

			// $date = $value['cal_startdate'];
			$date = date_create($value['cal_startdate']);
			$result[] = date_format($date, 'Y-n-j');
		}

		return $result;

	}

	##########################################################################################
	public function getImages($object_id = 0) {

		$rows = array();
		$result = array();

		$query = $this->db->query("
			SELECT f_name
			FROM f_files
			WHERE f_ucid = 'object'
			AND f_uid = '" . $object_id . "'
			ORDER BY f_order
		");

		$rows = $query->fetchAll();

		foreach($rows as $id => $val)
		{
			$result[] = $val['f_name'];
		}

		return $result;
	}

	##########################################################################################
	public function sendSms($params) {

		//Ответ метода
		$response = Array();

		///////////////////
		//Даные iqsms//////
		///////////////////
		//Логин СМС-дисконт
		$sms_login = "z1440672789976";
		//Пароль СМС-дисконт
		$sms_password = "968994";
		//Псевдоним отправителя (должен быть задан в ЛК СМС-дискаунта)
		$sms_sender = "GOYUG.COM";
		///////////////////////////////
		//Даные iqsms - окончание//////
		///////////////////////////////

		/////////////////////
		//Даные mainsms//////
		/////////////////////
		//Логин СМС-дисконт
		$mainsms_project = 'goyug';
		//API Key
		$mainsms_key = '76e238b2b1f1a';
		//Номер отправки
		$mainsms_recipients = ''; //определен будет ниже
		//Отправитель
		$mainsms_sender = 'GOYUG.COM';
		//Подпись
		$mainsms_sign = '';
		//Тестовая отправка
		$mainsms_test = 0;
		/////////////////////////////////
		//Даные mainsms - окончание//////
		/////////////////////////////////

		//Находим объект
		$obj_id = intval($params['obj_id']);
		$query = "SELECT * FROM obj_object WHERE obj_id = '$obj_id'";
		$obj_row = $this->db->query($query)->fetch();
		//print_r($obj_row).'<br />';

		//Находим владельца
		$query = "SELECT * FROM u_user WHERE u_id = '$obj_row[obj_u_id]'";
		$u_row = $this->db->query($query)->fetch();
		//print_r($u_row).'<br />';

		//Телефон отправления
		$sms_phone = $u_row['u_phone_1'];
		// $sms_phone = "79615322225"; // Марсель
		// $sms_phone = "79181610720"; // Алексей
		$sms_phone = str_replace("(","",$sms_phone);
		$sms_phone = str_replace(")","",$sms_phone);
		$sms_phone = str_replace("-","",$sms_phone);
		$sms_phone = str_replace(" ","",$sms_phone);
		//Номер отправки
		$mainsms_recipients = $sms_phone;
		$sms_phone = "+".$sms_phone;

		//Шаблоны текста смс
		$sms_templates = Array();

		// $sms_templates['showtel'] = 'Только что просмотрели ваши контакты на объекте, по адресу: г. '.$obj_row['obj_addr_city'].', ул. '.ltrim($obj_row['obj_addr_street'], 'ул.').' д. '.$obj_row['obj_addr_number'].'. Вам возможен звонок.';
		$sms_templates['showtel'] = 'На сайте Goyug.com запрошены контакты вашего объекта '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].'. Ожидайте звонка.';
		// $sms_templates['sendorder'] = 'Вашу квартиру по адресу: г. '.$obj_row['obj_addr_city'].', ул. '.ltrim($obj_row['obj_addr_street'], 'ул.').' д. '.$obj_row['obj_addr_number'].', хотят забронировать. Подробности отправлены на ваш E-mail.';

		if($params['action'] == 'sendorder') {

			//Данные по заявке
			$order = $params['order'];
			$sms_templates['sendorder'] = 'Заявка от Goyug.com '. $order['name'] .' '. $order['phone'] .' См. ваш email';
		}

		if($params['action'] == 'showtelext') {

			//Данные по заявке
			$order = $params['order'];
			$sms_templates['showtelext'] = 'Заявка от Goyug.com '. $order['name'] .' '. $order['phone'] .' См. ваш email';
		}

		// Вашу кв. '. $obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].' хотят арендовать, проверьте Email.';

		//Если не задан телефон отправки - ничего не делаем
		if( !$sms_phone || !isset($sms_templates[$params['action']]) ){

			mail("marselos@gmail.com", "Пустой номер или шаблон", "Попытка отправить пустую заявку. Смс не отправляется. Операция прерывается.");
			$response['status'] = false;

		} else {

			//////////////////////
			///Send SMS///////////
			//////////////////////

			//Текст смс
			$sms_text = $mainsms_text = $sms_templates[$params['action']];
			//$mainsms_text = $sms_templates[$params['action']];
			$sms_length = mb_strlen($sms_text, 'UTF-8');

			//Режим отправки Fake Live
			$sms_mod = 'live';

			//Live
			if($sms_mod == 'live'){

				if($this->sms_gateway == 'iqsms') {

					////////////////////////
					//Отправка СМС-дисконт//
					////////////////////////
					//Адрес API
					$ch = curl_init('http://api.iqsms.ru/messages/v2/send/');

					//Заголовки
					$headers = Array(
						'Accept: plain/text',
						'Accept-Language: ru, en-us',
						'Accept-Charset: windows-1251, utf-8'
					);

					//Установка адреса
					curl_setopt($ch, CURLOPT_POST, 1);

					//Установка заголовков
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

					//Запрос
					curl_setopt($ch, CURLOPT_POSTFIELDS, "login=$sms_login&password=$sms_password&phone=$sms_phone&text=$sms_text&sender=$sms_sender");

					//Отключение вывода в браузер + возврат в переменную
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

					//Выполняем запрос curl
					$result = curl_exec($ch);

					//Разбор ответа
					if( !isset($result) || $result == '' ){
						$sms_answer = "empty answer";
						$sms_uid = "no id";
					} else {
						$status = explode(";", $result);
						$sms_answer = $status[0];
						$sms_uid = $status[1];
					}

					//Время отправки смс
					$sms_date_send = date("Y-m-d H:i:s");

					//Закрываем соединение
					curl_close($ch);
					////////////////////////////////////
					//Отправка СМС-дисконт - окончание//
					////////////////////////////////////

				} else if($this->sms_gateway == 'mainsms') {

					//Подписание
					$mainsms_params = Array($mainsms_project, $mainsms_sender, $mainsms_text, $mainsms_recipients, $mainsms_test);
					ksort($mainsms_params);
					$mainsms_params[] = $mainsms_key;
					$mainsms_sign = md5(sha1(implode(';', $mainsms_params)));

					////////////////////////
					//Отправка MainSms//////
					////////////////////////
					//Адрес API
					$ch = curl_init('http://mainsms.ru/api/mainsms/message/send');

					//Установка адреса
					curl_setopt($ch, CURLOPT_POST, 1);

					//Запрос
					curl_setopt($ch, CURLOPT_POSTFIELDS, "project=$mainsms_project&sender=$mainsms_sender&message=$mainsms_text&recipients=$mainsms_recipients&sign=$mainsms_sign&test=$mainsms_test");

					//Отключение вывода в браузер + возврат в переменную
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

					//Выполняем запрос curl
					$result = curl_exec($ch);

					//Разбор ответа
					if( !isset($result) || $result == '' ){
						$sms_uid = "no id";
					} else {
						$result = json_decode($result, true);
						$sms_uid = implode(',', $result['messages_id']);
					}

					//Время отправки смс
					$sms_date_send = date("Y-m-d H:i:s");

					//Телефон для MainSms
					$sms_phone = $mainsms_recipients;

					//Закрываем соединение
					curl_close($ch);
					////////////////////////////////////
					//Отправка MainSms - окончание//////
					////////////////////////////////////

				} else {
					mail("marselos@gmail.com", "Не указан смс-шлюз", "Необходимо проверить значение смс-шлюза в инициализации модели Ui Objects");
				}

			} else {

				//Fake
				$sms_uid = "fake mod";
				$sms_date_send = date("Y-m-d H:i:s");
			}

			//////////////////////
			///Send SMS end///////
			//////////////////////

			//Заносим ID сообщений в базу
			$query = "INSERT INTO ss_sms_stat
					  (`ss_obj_id`, `ss_u_id`, `ss_phone_send`, `ss_text`, `ss_length`, `ss_ip`, `ss_sms_uid`, `ss_date_send`)
					  VALUES
					  ('$obj_id', '$u_row[u_id]', '$sms_phone', '$sms_text', '$sms_length', '$_SERVER[REMOTE_ADDR]', '$sms_uid', '$sms_date_send')";
			$this->db->query($query);

			//print_r($query);
			$response['status'] = true;
		}

		return $response;
	}

	##########################################################################################
	public function sendSmsHotOrder($hotorder) {

		//Ответ метода
		$response = Array();

		/////////////////////
		//Даные mainsms//////
		/////////////////////
		//Логин СМС-дисконт
		$mainsms_project = 'goyug';
		//API Key
		$mainsms_key = '76e238b2b1f1a';
		//Номер отправки
		$mainsms_recipients = ''; //определен будет ниже
		//Отправитель
		$mainsms_sender = 'GOYUG.COM';
		//Подпись
		$mainsms_sign = '';
		//Тестовая отправка
		$mainsms_test = 0;
		/////////////////////////////////
		//Даные mainsms - окончание//////
		/////////////////////////////////

		//Телефон отправления
		$sms_phone = Util::stripTel($hotorder['phone']);
		// $sms_phone = "79615322225"; // Марсель
		// $sms_phone = "79181610720"; // Алексей
		// $sms_phone = "79182881855"; // Сергей
		//Номер отправки
		$mainsms_recipients = $sms_phone;
		$sms_phone = "+".$sms_phone;

		//Если не задан телефон отправки - ничего не делаем
		if(!$sms_phone){

			mail("marselos@gmail.com", "Пустой номер в отправке кода подтверждения hotorder", "Попытка отправить на пустой номер. Смс не отправляется. Операция прерывается.<br />".print_r($hotorder, true));
			$response['status'] = false;

		} else {

			//////////////////////
			///Send SMS///////////
			//////////////////////

			//Текст смс
			$sms_text = $mainsms_text = 'Код подтверждения срочного заселения: '.$hotorder['code'];
			//$mainsms_text = $sms_templates[$params['action']];
			$sms_length = mb_strlen($sms_text, 'UTF-8');

			//Режим отправки Fake Live
			$sms_mod = 'live';

			//Live
			if($sms_mod == 'live'){

				//Подписание
				$mainsms_params = Array($mainsms_project, $mainsms_sender, $mainsms_text, $mainsms_recipients, $mainsms_test);
				ksort($mainsms_params);
				$mainsms_params[] = $mainsms_key;
				$mainsms_sign = md5(sha1(implode(';', $mainsms_params)));

				////////////////////////
				//Отправка MainSms//////
				////////////////////////
				//Адрес API
				$ch = curl_init('http://mainsms.ru/api/mainsms/message/send');

				//Установка адреса
				curl_setopt($ch, CURLOPT_POST, 1);

				//Запрос
				curl_setopt($ch, CURLOPT_POSTFIELDS, "project=$mainsms_project&sender=$mainsms_sender&message=$mainsms_text&recipients=$mainsms_recipients&sign=$mainsms_sign&test=$mainsms_test");

				//Отключение вывода в браузер + возврат в переменную
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				//Выполняем запрос curl
				$result = curl_exec($ch);

				//Разбор ответа
				if( !isset($result) || $result == '' ){
					$sms_uid = "no id";
				} else {
					$result = json_decode($result, true);
					$sms_uid = implode(',', $result['messages_id']);
				}

				//Время отправки смс
				$sms_date_send = date("Y-m-d H:i:s");

				//Телефон для MainSms
				$sms_phone = $mainsms_recipients;

				//Закрываем соединение
				curl_close($ch);
				////////////////////////////////////
				//Отправка MainSms - окончание//////
				////////////////////////////////////

			} else {

				//Fake
				$sms_uid = "fake mod";
				$sms_date_send = date("Y-m-d H:i:s");
			}

			//////////////////////
			///Send SMS end///////
			//////////////////////

			//Заносим ID сообщений в базу
			$query = "INSERT INTO ss_sms_stat
					  (`ss_phone_send`, `ss_text`, `ss_length`, `ss_ip`, `ss_sms_uid`, `ss_date_send`)
					  VALUES
					  ('$sms_phone', '$sms_text', '$sms_length', '$_SERVER[REMOTE_ADDR]', '$sms_uid', '$sms_date_send')";
			$this->db->query($query);

			//print_r($query);
			$response['status'] = true;
		}

		return $response;
	}

	##########################################################################################
	public function sendSmsParamsOrder($hotorder) {

		//Ответ метода
		$response = Array();

		/////////////////////
		//Даные mainsms//////
		/////////////////////
		//Логин СМС-дисконт
		$mainsms_project = 'goyug';
		//API Key
		$mainsms_key = '76e238b2b1f1a';
		//Номер отправки
		$mainsms_recipients = ''; //определен будет ниже
		//Отправитель
		$mainsms_sender = 'GOYUG.COM';
		//Подпись
		$mainsms_sign = '';
		//Тестовая отправка
		$mainsms_test = 0;
		/////////////////////////////////
		//Даные mainsms - окончание//////
		/////////////////////////////////

		//Телефон отправления
		$sms_phone = Util::stripTel($hotorder['phone']);
		// $sms_phone = "79615322225"; // Марсель
		// $sms_phone = "79181610720"; // Алексей
		// $sms_phone = "79182881855"; // Сергей
		//Номер отправки
		$mainsms_recipients = $sms_phone;
		$sms_phone = "+".$sms_phone;

		//Если не задан телефон отправки - ничего не делаем
		if(!$sms_phone){

			mail("marselos@gmail.com", "Пустой номер в отправке кода подтверждения hotorder", "Попытка отправить на пустой номер. Смс не отправляется. Операция прерывается.<br />".print_r($hotorder, true));
			$response['status'] = false;

		} else {

			//////////////////////
			///Send SMS///////////
			//////////////////////

			//Текст смс
			$sms_text = $mainsms_text = 'Код подтверждения запроса по параметрам: '.$hotorder['code'];
			//$mainsms_text = $sms_templates[$params['action']];
			$sms_length = mb_strlen($sms_text, 'UTF-8');

			//Режим отправки Fake Live
			$sms_mod = 'live';

			//Live
			if($sms_mod == 'live'){

				//Подписание
				$mainsms_params = Array($mainsms_project, $mainsms_sender, $mainsms_text, $mainsms_recipients, $mainsms_test);
				ksort($mainsms_params);
				$mainsms_params[] = $mainsms_key;
				$mainsms_sign = md5(sha1(implode(';', $mainsms_params)));

				////////////////////////
				//Отправка MainSms//////
				////////////////////////
				//Адрес API
				$ch = curl_init('http://mainsms.ru/api/mainsms/message/send');

				//Установка адреса
				curl_setopt($ch, CURLOPT_POST, 1);

				//Запрос
				curl_setopt($ch, CURLOPT_POSTFIELDS, "project=$mainsms_project&sender=$mainsms_sender&message=$mainsms_text&recipients=$mainsms_recipients&sign=$mainsms_sign&test=$mainsms_test");

				//Отключение вывода в браузер + возврат в переменную
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				//Выполняем запрос curl
				$result = curl_exec($ch);

				//Разбор ответа
				if( !isset($result) || $result == '' ){
					$sms_uid = "no id";
				} else {
					$result = json_decode($result, true);
					$sms_uid = implode(',', $result['messages_id']);
				}

				//Время отправки смс
				$sms_date_send = date("Y-m-d H:i:s");

				//Телефон для MainSms
				$sms_phone = $mainsms_recipients;

				//Закрываем соединение
				curl_close($ch);
				////////////////////////////////////
				//Отправка MainSms - окончание//////
				////////////////////////////////////

			} else {

				//Fake
				$sms_uid = "fake mod";
				$sms_date_send = date("Y-m-d H:i:s");
			}

			//////////////////////
			///Send SMS end///////
			//////////////////////

			//Заносим ID сообщений в базу
			$query = "INSERT INTO ss_sms_stat
					  (`ss_phone_send`, `ss_text`, `ss_length`, `ss_ip`, `ss_sms_uid`, `ss_date_send`)
					  VALUES
					  ('$sms_phone', '$sms_text', '$sms_length', '$_SERVER[REMOTE_ADDR]', '$sms_uid', '$sms_date_send')";
			$this->db->query($query);

			//print_r($query);
			$response['status'] = true;
		}

		return $response;
	}

	##########################################################################################
	public function showTel($params) {

		//Ответ метода
		$response = Array();
		$response['status'] = false;
		$response['reason'] = 'Неизвестная ошибка - попробуйте позже';

		//Ошибки
		$errors = false;

		//ID объекта
		$obj_id = intval($params['obj_id']);

		////////////////////////////////
		//Проверка данных пользователя//
		////////////////////////////////

			//Идентификатор
			if(!$obj_id){
				$errors = true;
				$response['reason'] = "Не передан идентификатор";
			}

		////////////////////////////////////////////
		//Проверка данных пользователя - окончание//
		////////////////////////////////////////////

		/////////////////////////////
		//Форма заполнена корректно//
		/////////////////////////////

			if(!$errors){

				//Находим объект
				$query = "SELECT * FROM obj_object WHERE obj_id = ".$obj_id;
				$obj_row = $this->db->query($query)->fetch();

				//Находим владельца
				$query = "SELECT * FROM u_user WHERE u_id = ".$obj_row['obj_u_id'];
				$u_row = $this->db->query($query)->fetch();

				//Генерируем токен управления рассылками, если его еще нет
				if(!$u_row['u_subscribe_token']){
					$subscribe_token = md5($u_row['u_id'].rand(1000, 999999).time());
					$query_subscribe_token = "UPDATE u_user SET `u_subscribe_token` = '".$subscribe_token."' WHERE u_id = ".$u_row['u_id'];
					$this->db->query($query_subscribe_token);
					$u_row['u_subscribe_token'] = $subscribe_token;
				}

				////////////////////
				//Для пользователя//
				////////////////////

					################################
					# Возвращаем контактные данные #
					################################

						//Статус
						$response['status'] = true;
						$response['reason'] = 'Благодарим!';
						//Телефон
						$response['phone'] = Util::getHumanTel($u_row['u_phone_1']);
						if(!$response['phone'])	$response['phone'] = Util::getHumanTel($u_row['u_phone_2']);
						if(!$response['phone'])	$response['phone'] = Util::getHumanTel($u_row['u_phone_3']);
						if(!$response['phone'])	$response['phone'] = 'Телефон недоступен';
						//E-mail
						$response['email'] = ($u_row['u_show_email'] && $u_row['u_email']) ? $u_row['u_email'] : '';

				////////////////////////////////
				//Для пользователя - окончание//
				////////////////////////////////


				/////////////////
				//Для владельца//
				/////////////////

					###############################
					# Отправляем письмо владельцу #
					###############################

						////Если указан E-mail владельца, пользователь заполняет первый раз форму, владелец не отписан от получения заявки
						if( $u_row['u_email'] /*&& $u_row['u_subscribe_email']*/ ){

							////Подготовка письма

								//Идентификатор письма
								$email_uid = md5(microtime().$u_row['u_email']);
								//Дата отправки письма
								$email_date_send = date("Y-m-d H:i:s");
								//E-mail для отправки
								$email_send_to = $u_row['u_email'];
								//Заголовок письма
								$email_subject = addslashes('Показ контактов по ул. '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number']);
								//Текст письма
								$email_body = '
								<!DOCTYPE html>
								<html lang="ru">
								<head>
									<meta charset="UTF-8">
									<title>Показ контактов по ул. '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].'</title>
								</head>
								<body>
								<h3>Просмотр контактов</h3>

								<p>
									'.date("d").' '.Util::giveRealMounth(date("m")).' в '.date("H").':'.date('i').' были просмотрены контакты вашего объекта по адресу: г. '.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].'<br />
									Это означает, что гость заинтересовался вашей квартирой, и вероятно свяжется с вами в ближайшее время.
								</p>

								<p>Мы были рады Вам помочь.</p>

								<p>* письмо сформировано автоматически, ответ на него не требуется</p>
								</body>
								</html>';

							////Процедура отправки E-mail

								try {

									//Отправка
									$mail = new Zend_Mail('UTF-8');
									$mail->setBodyHTML($email_body);
									$mail->setFrom('info@goyug.com', 'GoYug.com');
									$mail->setReplyTo('info@goyug.com', '');
									$mail->addTo($email_send_to, '');
									$mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
									// $mail->addBcc(array('marselos@gmail.com'));
									$mail->setSubject($email_subject);
									$mail->send();

					            } catch(Exception $ex) {

					            }

							////Сохраняем ID писем в базу

								//Готовим таблицу
								$tbl_es_email_stat = new Zend_Db_Table(array(
									'name' => 'es_email_stat',
									'primary' => 'es_id'
								));

								//Пишем в базу
								$es_email_stat_id = $tbl_es_email_stat->insert(Array(
									'es_obj_id' => $obj_id,
									'es_u_id' => $u_row['u_id'],
									'es_email_send' => $email_send_to,
									'es_ip' => $_SERVER['REMOTE_ADDR'],
									'es_email_uid' => $email_uid,
									'es_date_send' => $email_date_send
								));

						}

				/////////////////////////////
				//Для владельца - окончание//
				/////////////////////////////

			}

		/////////////////////////////////////////
		//Форма заполнена корректно - окончание//
		/////////////////////////////////////////

		return $response;

	}

	##########################################################################################
	public function sendOrder($params) {

		//Ответ метода
		$response = Array();
		$response['status'] = false;
		$response['reason'] = 'Неизвестная ошибка - попробуйте позже';

		//Ошибки формы
		$errors = false;

		//Данные по заявке
		$order = $params['order'];

		//Преобразование дат
		$order_checkin = explode('.', $order['checkin']);
		$order_checkout = explode('.', $order['checkout']);

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя
		/////////////////////////////////////////////////////////////////////////////////////////////

		//Имя
		if(!$order['name']){
			$errors = true;
			$response['reason'] = "Укажите свое имя";
		}
		//Телефон
		/*
		else if(!$order['phone']){
			$errors = true;
			$response['reason'] = "Укажите свой телефон";
		}
		*/
		// else if( $order['phone'] && Util::testTel($order['phone']) == false ) {
		// 	$errors = true;
		// 	$response['reason'] = 'Телефон указан неверно';
		// }

		//Email
		else if(isset($order['email']) && !$order['email']){
			$errors = true;
			$response['reason'] = "Укажите свой E-mail";
		}
		else if(isset($order['email']) && !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $order['email'])){
			$errors = true;
			$response['reason'] = 'E-mail указан неверно';
		}

		//Дата заезда
		else if(!$order['checkin']){
			$errors = true;
			$response['reason'] = "Дата заезда не указана";
		}

		//Дата выезда
		else if(!$order['checkout']){
			$errors = true;
			$response['reason'] = "Дата выезда не указана";
		}
		else if( mktime(0, 0, 0, $order_checkin[1], $order_checkin[0], $order_checkin[2]) >= mktime(0, 0, 0, $order_checkout[1], $order_checkout[0], $order_checkout[2]) ){
			$errors = true;
			$response['reason'] = "Дата выезда должна быть больше даты заезда";
		}

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно
		/////////////////////////////////////////////////////////////////////////////////////////////
		if(!$errors)
		{

			//Находим объект
			$obj_id = intval($params['obj_id']);
			$query = "SELECT * FROM obj_object WHERE obj_id = ".$obj_id;
			$obj_row = $this->db->query($query)->fetch();

			//Находим владельца
			$query = "SELECT * FROM u_user WHERE u_id = ".$obj_row['obj_u_id'];
			$u_row = $this->db->query($query)->fetch();

			//Генерируем токен управления рассылками, если его еще нет
			if(!$u_row['u_subscribe_token'])
			{
				$subscribe_token = md5($u_row['u_id'].rand(1000, 999999).time());
				$query_subscribe_token = "UPDATE u_user SET `u_subscribe_token` = '".$subscribe_token."' WHERE u_id = ".$u_row['u_id'];
				$this->db->query($query_subscribe_token);
				$u_row['u_subscribe_token'] = $subscribe_token;
			}

			/////////////////////////////////////////////////////////////////////////////////////////////
			/// Отправка письма владельцу
			/////////////////////////////////////////////////////////////////////////////////////////////

			////Если указан E-mail владельца, владелец не отписан от получения заявки
			if( $u_row['u_email'] /*&& $u_row['u_subscribe_email']*/ )
			{

				////Подготовка письма

				//Идентификатор письма
				$email_uid = md5(microtime().$order['email']);

				//Дата отправки письма
				$email_date_send = date("Y-m-d H:i:s");

				//Настоящий или проверочный E-mail
				$email_send_to = $u_row['u_email'];

				//Заголовок письма
				$email_subject = addslashes('Запрос на бронь вашей квартиры по ул. '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number']);

				//Текст письма

				// 14/12/2017 - Исключили телефон из письма запроса на бронь
				// <tr>
				// 	<td style="font-weight: bold;">Телефон:</td>
				// 	<td>'.((isset($order['phone']) && $order['phone']) ? Util::getHumanTel($order['phone']) : "Не указан").'</td>
				// </tr>

				$email_body = '
				<!DOCTYPE html>
				<html lang="ru">
				<head>
					<meta charset="UTF-8">
					<title>Запрос на бронь вашей квартиры по ул. '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].'</title>
				</head>
				<body>
				<h3>Новый запрос на бронь вашей квартиры</h3>

				<p>
					На сайте <a href="http://'.$obj_row['obj_addr_city_uid'].'.goyug.com">GoYug.com</a> гость заполнил форму запроса на бронирование вашей квартиры по адресу: г. '.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].' д. '.$obj_row['obj_addr_number'].'<br />
					Указав в заявке следующие данные.
				</p>

				<h3>Информация о госте</h3>
				<table cellpadding="5" border="1" style="border-collapse: collapse;">
					<tbody>
					<tr>
						<td style="font-weight: bold;">Имя:</td>
						<td>'.$order['name'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">E-mail:</td>
						<td>'.((isset($order['email']) && $order['email']) ? $order['email'] : "Не указан").'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Дата заезда:</td>
						<td>'.$order['checkin'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Дата выезда:</td>
						<td>'.$order['checkout'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Примечание:</td>
						<td>'.((isset($order['comment']) && $order['comment']) ? $order['comment'] : "Не указано").'</td>
					</tr>
					</tbody>
				</table>

				<h3>Информация об объекте</h3>
				<table cellpadding="5" border="1" style="border-collapse: collapse;">
					<tbody>
					<tr>
						<td style="font-weight: bold;">Количество комнат:</td>
						<td>'.$obj_row['obj_rooms'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Количество гостей:</td>
						<td>'.$obj_row['obj_guests'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Район:</td>
						<td>'.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Стоимость:</td>
						<td>До '.$obj_row['obj_price'].' руб.</td>
					</tr>
					</tbody>
				</table>

				</body>
				</html>';

				////Процедура отправки E-mail
				try {

					//Отправка
					$mail = new Zend_Mail('UTF-8');
					$mail->setBodyHTML($email_body);
					$mail->setFrom('info@goyug.com', 'GoYug.com');
					// $mail->setReplyTo($order['email'], '');
					$mail->setReplyTo('info@goyug.com', '');
					$mail->addTo($email_send_to, '');
					$mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
					// $mail->addBcc(array('marselos@gmail.com'));
					$mail->setSubject($email_subject);
					$mail->send();

		            } catch(Exception $ex) {

		            }

				////Сохраняем ID писем в базу

				//Готовим таблицу
				$tbl_es_email_stat = new Zend_Db_Table(array(
					'name' => 'es_email_stat',
					'primary' => 'es_id'
				));

				//Пишем в базу
				$es_email_stat_id = $tbl_es_email_stat->insert(Array(
					'es_obj_id' => $obj_id,
					'es_u_id' => $u_row['u_id'],
					'es_email_send' => $email_send_to,
					'es_ip' => $_SERVER['REMOTE_ADDR'],
					'es_email_uid' => $email_uid,
					'es_date_send' => $email_date_send
				));

			} ////Если указан E-mail владельца

			/////////////////////////////////////////////////////////////////////////////////////////////
			/// Отправка письма владельцу - КОНЕЦ
			/////////////////////////////////////////////////////////////////////////////////////////////

			###############################
			# Тестируем временной сегмент #
			###############################

			// //Отключено 2015 11 07 - не убивать, т.к. нетривиальная тема
			// //Маркер разрешения отправки
			// $timeSegmentPermit = false;

			// //Путь к папке с тайм-сегментами
			// //На сервере
			// if( strpos($_SERVER['HTTP_HOST'], 'goyug.com') !== false ){
			// 	$path = "/home/c/c74542/goyug.com/time_segment/";
			// }
			// //Локально
			// else {
			// 	$path = "../time_segment/";
			// }
			// if(!file_exists($path)) mkdir($path);

			// //Текущий час
			// $current_hour = date("G");

			// //Проверка отправлений в текущем сегменте
			// $segments[] = Array();
			// $segments[$obj_row['obj_addr_city_uid'].'_from_0_to_8'] = file_exists($path.$obj_row['obj_addr_city_uid'].'_from_0_to_8');
			// $segments[$obj_row['obj_addr_city_uid'].'_from_8_to_16'] = file_exists($path.$obj_row['obj_addr_city_uid'].'_from_8_to_16');
			// $segments[$obj_row['obj_addr_city_uid'].'_from_16_to_0'] = file_exists($path.$obj_row['obj_addr_city_uid'].'_from_16_to_0');

			// //Интервал 1
			// if($current_hour >= 0 && $current_hour < 8){
			// 	if(!$segments[$obj_row['obj_addr_city_uid'].'_from_0_to_8']){
			// 		file_put_contents($path.$obj_row['obj_addr_city_uid'].'_from_0_to_8', $current_hour);
			// 		$timeSegmentPermit = true;
			// 	}
			// }
			// //Интервал 2
			// else if ($current_hour >= 8 && $current_hour < 16){
			// 	if(!$segments[$obj_row['obj_addr_city_uid'].'_from_8_to_16']){
			// 		file_put_contents($path.$obj_row['obj_addr_city_uid'].'_from_8_to_16', $current_hour);
			// 		$timeSegmentPermit = true;
			// 	}
			// }
			// //Интервал 3
			// else if ($current_hour >= 16 && $current_hour <=23){
			// 	if(!$segments[$obj_row['obj_addr_city_uid'].'_from_16_to_0']){
			// 		file_put_contents($path.$obj_row['obj_addr_city_uid'].'_from_16_to_0', $current_hour);
			// 		$timeSegmentPermit = true;
			// 	}
			// }
			// //Если не найдено интервала
			// else {
			// 	file_put_contents($path.$obj_row['obj_addr_city_uid'].'_bad_time', $current_hour);
			// }

			// ////Тестируем временной сегмент - окончание

			##############################
			# Проверка на жирность заяки #
			##############################

			//Маркер жирности
			$isBold = false;

			//Срок заявки
			$delta = mktime(0, 0, 0, $order_checkout[1], $order_checkout[0], $order_checkout[2]) - mktime(0, 0, 0, $order_checkin[1], $order_checkin[0], $order_checkin[2]);
			$order_days = ceil($delta / (60 * 60 * 24));

			//Определяем жирность
			if($order_days >= 5) $isBold = true;


			/////////////////////////////////////////////////////////////////////////////////////////////
			/// Веерная Рассылка по всем владельцам города
			/////////////////////////////////////////////////////////////////////////////////////////////
			// if( !isset($_SESSION['order']) )
			// {

				//Объект рассылки
				//$email['list'] - массив адресов отправки
				//$email['uid'] - уникальный идентификатор письма
				//$email['date_create'] - время создания рассылки
				//$email['subject'] - тема письма
				//$email['body'] - текст письма
				$email = Array();

				////Данные для рассылки

				//Идентификатор рассылки
				$email['uid'] = md5(time().rand(0, 1000));

				//Время создания
				$email['date_create'] = date("Y-m-d H:i:s");

				//Ответ заявителю
				$email['reply'] = (isset($order['email']) && $order['email']) ? $order['email'] : "";

				////Список владельцев для отправки

				//Исключения из отправки
				$email_restrict = Array();

				//Владельцу объекта уже ушло сообщение - добавляем в исключения
				$email_restrict[] = $u_row['u_email'];
				$email_restrict[] = "sosnickaya888@yandex.ru";

				/////////////////////////////////////////////////////////////////////////////////////////////
				/// Веерная Рассылка по всем владельцам города
				/////////////////////////////////////////////////////////////////////////////////////////////

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Веер 1 ШАГ: Все владельцы или только абоненты города с абонплатой
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//Заголовок письма
				$email['subject'] = 'Запрос бронирования квартиры на '.$order_days.' '.Util::pluralForm($order_days, Array("день", "дня", "дней"));

				// Если город с абонплатой, выбираем лишь абонентов
				$query_ext_rental = (in_array($obj_row['obj_addr_city_uid'], $this->rental_cities)) ? ' AND u.u_rental = 1' : '';

				//Лист отправки - все владельцы города, кроме отписавшихся
				$query = "
					SELECT obj_u_id, LCASE(u.u_email) as u_email
					FROM u_user AS u

					LEFT JOIN (
						SELECT *
						FROM obj_object
						GROUP BY obj_u_id
					)
					AS obj ON obj.obj_u_id = u.u_id

					WHERE u.u_role = 'owner'
					AND obj_addr_city_uid = '".$obj_row['obj_addr_city_uid']."'
					AND u.u_email != ''
					AND u.u_email LIKE '%@%.%'
					"
					. $query_ext_rental;

				$u_rows = $this->db->query($query)->fetchAll();

				foreach ($u_rows as $key => $value)
				{
					$email['list'][] = $value['u_email'];
				}

				$email['list'] = array_diff($email['list'], $email_restrict);

				////Текст письма

				// 14/12/2017 - Исключили телефон из письма запроса на бронь
				// <tr>
				// 	<td style="font-weight: bold;">Телефон гостя:</td>
				// 	<td>'.((isset($order['phone']) && $order['phone']) ? Util::getHumanTel($order['phone']) : "Не указан").'</td>
				// </tr>

				$email['body'] = '
				<!DOCTYPE html>
				<html lang="ru">
				<head>
					<meta charset="UTF-8">
					<title>'.$email['subject'].'</title>
				</head>
				<body>
				<h3>Запрос бронирования квартиры в вашем городе</h3>

				<p>На сайте <a href="http://'.$obj_row['obj_addr_city_uid'].'.goyug.com">GoYug.com</a> заполнен запрос на бронирование квартиры по адресу: г. '.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].'. Если у вас есть подходящий вариант - можете предложить его гостю.</p>

					<p style="padding: 12px 18px; background: #77bc3f; color: #fff; display: inline-block;">
						Уважаемый владелец!<br>
						Данный запрос был отправлен гостем на все подходящие квартиры в вашем городе.<br>
						Если вы считаете что ваша квартира подходит по всем параметрам, свяжитесь напрямую с владельцем, написав ему
						на Email <a href="mailto: ">'.((isset($order['email']) && $order['email']) ? $order['email'] : "Не указан").'</a>
					</p>

				<h3>Информация о заявке</h3>
				<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
					<tbody>
					<tr>
						<td style="font-weight: bold;">Имя:</td>
						<td>'.$order['name'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Email гостя:</td>
						<td>'.((isset($order['email']) && $order['email']) ? $order['email'] : "Не указан").'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Дата заезда:</td>
						<td>'.$order['checkin'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Дата выезда:</td>
						<td>'.$order['checkout'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Примечание:</td>
						<td>'.((isset($order['comment']) && $order['comment']) ? $order['comment'] : "Не указано").'</td>
					</tr>
					</tbody>
				</table>

				<h3>Информация об объекте</h3>
				<table cellpadding="5" border="1" style="border-collapse: collapse;">
					<tbody>
					<tr>
						<td style="font-weight: bold;">Количество комнат:</td>
						<td>'.$obj_row['obj_rooms'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Количество гостей:</td>
						<td>'.$obj_row['obj_guests'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Район:</td>
						<td>'.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].'</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">Стоимость:</td>
						<td>До '.$obj_row['obj_price'].' руб.</td>
					</tr>
					</tbody>
				</table>

				</body>
				</html>';

				////Создаем письмо рассылки

				//Готовим таблицу для письма рассылки
				$tbl = new Zend_Db_Table(array(
					'name' => 'celt_cron_email_list_tasks',
					'primary' => 'celt_id'
				));

				//Пишем в базу письмо рассылки
				$tbl->insert(Array(
					'celt_uid' => $email['uid'],
					'celt_email_subject' => $email['subject'],
					'celt_email_body' => $email['body'],
					'celt_date_create' => $email['date_create']
				));

				////Создаем рассылку

				//Назначаем письмо владельцам
				foreach ($email['list'] as $key => $value) {
					$query = "INSERT INTO cel_cron_email_list (`cel_celt_uid`, `cel_email`, `cel_reply`, `cel_date_stage`) VALUES ('".$email['uid']."', '".$value."', 'info@goyug.com', '".$email['date_create']."')";
					$this->db->query($query);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Веер 1 ШАГ: Все владельцы или только абоненты города с абонплатой - КОНЕЦ
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Веер 2 ШАГ: НЕ абоненты города с абонплатой
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if(in_array($obj_row['obj_addr_city_uid'], $this->rental_cities))
				{
					//Идентификатор рассылки
					$email['uid'] = md5(time().rand(0, 1000));

					//Время создания
					$email['date_create'] = date("Y-m-d H:i:s");

					$email['subject'] = '* Запрос бронирования квартиры на '.$order_days.' '.Util::pluralForm($order_days, Array("день", "дня", "дней"));

					// Обнуляем список рассылки
					$email['list'] = array();

					//Лист отправки - все НЕ абоненты города
					$query = "
						SELECT obj_u_id, LCASE(u.u_email) as u_email
						FROM u_user AS u

						LEFT JOIN (
							SELECT *
							FROM obj_object
							GROUP BY obj_u_id
						)
						AS obj ON obj.obj_u_id = u.u_id

						WHERE u.u_role = 'owner'
						AND obj_addr_city_uid = '" . $obj_row['obj_addr_city_uid'] . "'
						AND u.u_email != ''
						AND u.u_email LIKE '%@%.%'
						AND u.u_rental = 0
					";

					$u_rows = $this->db->query($query)->fetchAll();

					foreach ($u_rows as $key => $value)
					{
						$email['list'][] = $value['u_email'];
					}

					$email['list'] = array_diff($email['list'], $email_restrict);

					////Текст письма

					// 14/12/2017 - Исключили телефон из письма запроса на бронь
					// <tr>
					// 	<td style="font-weight: bold;">Телефон гостя:</td>
					// 	<td>*************************</td>
					// </tr>

					$email['body'] = '
					<!DOCTYPE html>
					<html lang="ru">
					<head>
						<meta charset="UTF-8">
						<title>'.$email['subject'].'</title>
					</head>
					<body>
					<h3>Новый запрос на бронь квартиры в вашем городе</h3>

					<p>На сайте <a href="http://'.$obj_row['obj_addr_city_uid'].'.goyug.com">GoYug.com</a> заполнен запрос на бронирование квартиры по адресу: г. '.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].'. Если у вас есть подходящий вариант - можете предложить его гостю.</p>

					<p style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block;">
						Уважаемый владелец!<br>
            				Вы не активировали услугу "АБОНЕМЕНТ" на нашем ресурсе.<br>
            				Для того чтобы иметь возможность видеть контакты гостя отправившего заявку, необходимо чтобы ваш месячный абонемент был оплачен на момент поступления заявки.<br>
						Стоимость месячного абонемента - 100 рублей</p>

            			<p><a href="http://lk.goyug.com/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>

					<h3>Информация о заявке</h3>
					<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
						<tbody>
						<tr>
							<td style="font-weight: bold;">Имя:</td>
							<td>'.$order['name'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Email гостя:</td>
							<td>*************************</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Дата заезда:</td>
							<td>'.$order['checkin'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Дата выезда:</td>
							<td>'.$order['checkout'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Примечание:</td>
							<td>'.((isset($order['comment']) && $order['comment']) ? $order['comment'] : "Не указано").'</td>
						</tr>
						</tbody>
					</table>

					<h3>Информация об объекте</h3>
					<table cellpadding="5" border="1" style="border-collapse: collapse;">
						<tbody>
						<tr>
							<td style="font-weight: bold;">Количество комнат:</td>
							<td>'.$obj_row['obj_rooms'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Количество гостей:</td>
							<td>'.$obj_row['obj_guests'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Район:</td>
							<td>'.$obj_row['obj_addr_city'].', '.$obj_row['obj_addr_street'].'</td>
						</tr>
						<tr>
							<td style="font-weight: bold;">Стоимость:</td>
							<td>До '.$obj_row['obj_price'].' руб.</td>
						</tr>
						</tbody>
					</table>

					</body>
					</html>';

					////Создаем письмо рассылки

					//Готовим таблицу для письма рассылки
					$tbl = new Zend_Db_Table(array(
						'name' => 'celt_cron_email_list_tasks',
						'primary' => 'celt_id'
					));

					//Пишем в базу письмо рассылки
					$tbl->insert(Array(
						'celt_uid' => $email['uid'],
						'celt_email_subject' => $email['subject'],
						'celt_email_body' => $email['body'],
						'celt_date_create' => $email['date_create']
					));

					////Создаем рассылку

					//Назначаем письмо владельцам
					foreach ($email['list'] as $key => $value) {
						$query = "INSERT INTO cel_cron_email_list (`cel_celt_uid`, `cel_email`, `cel_reply`, `cel_date_stage`) VALUES ('".$email['uid']."', '".$value."', 'info@goyug.com', '".$email['date_create']."')";
						$this->db->query($query);
					}

				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Веер 2 ШАГ: НЕ абоненты города с абонплатой - КОНЕЦ
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			// }
			/////////////////////////////////////////////////////////////////////////////////////////////////////
			/// Веерная Рассылка по всем владельцам города - КОНЕЦ
			/////////////////////////////////////////////////////////////////////////////////////////////////////

			//Сохраняем данные формы пользователя
			// $_SESSION['order'] = $order;

			$response['status'] = true;

		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		return $response;

	}

	##########################################################################################
	public function showTelExtMobile($params) {

		//Ответ метода
		$response = Array();
		$response['status'] = false;
		$response['reason'] = 'Неизвестная ошибка - попробуйте позже';

		//Ошибки формы
		$errors = false;


		/////////////////////////////
		//Форма заполнена корректно//
		/////////////////////////////

			if(!$errors){

				//Находим объект
				$obj_id = intval($params['obj_id']);
				$query = "SELECT * FROM obj_object WHERE obj_id = '$obj_id'";
				$obj_row = $this->db->query($query)->fetch();

				//Находим владельца
				$query = "SELECT * FROM u_user WHERE u_id = '".$obj_row['obj_u_id']."'";
				$u_row = $this->db->query($query)->fetch();

				////////////////////
				//Для пользователя//
				////////////////////

					################################
					# Возвращаем контактные данные #
					################################

						//Статус
						$response['status'] = true;
						$response['reason'] = 'Благодарим!';
						//Телефон
						$response['phone'] = Util::getHumanTel($u_row['u_phone_1']);
						if(!$response['phone'])	$response['phone'] = Util::getHumanTel($u_row['u_phone_2']);
						if(!$response['phone'])	$response['phone'] = Util::getHumanTel($u_row['u_phone_3']);
						if(!$response['phone'])	$response['phone'] = 'Телефон недоступен';
						//E-mail
						//$response['email'] = ($u_row['u_show_email'] && $u_row['u_email']) ? $u_row['u_email'] : '';

				////////////////////////////////
				//Для пользователя - окончание//
				////////////////////////////////

			}

		/////////////////////////////////////////
		//Форма заполнена корректно - окончание//
		/////////////////////////////////////////

		return $response;
	}

	##########################################################################################
	public function sendHotOrder($params) {

		//Ответ метода
		$response = Array();
		$response['status'] = false;
		$response['step'] = 'info';
		$response['reason'] = 'Неизвестная ошибка - попробуйте позже';

		//Ошибки формы
		$errors = false;

		//Данные по заявке
		$hotorder = $params['hotorder'];

		//Преобразование дат
		$hotorder_checkin = explode('.', $hotorder['checkin']);
		$hotorder_checkout = explode('.', $hotorder['checkout']);

		$hotorder['guests'] = intval($hotorder['guests']);
		$hotorder['price'] = intval($hotorder['price']);

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя
		/////////////////////////////////////////////////////////////////////////////////////////////

		//Имя
		if(!$hotorder['name']){
			$errors = true;
			$response['reason'] = "Укажите свое имя";
		}

		//Телефон
		else if(!$hotorder['phone']){
			$errors = true;
			$response['reason'] = "Укажите свой телефон";
		}
		else if( $hotorder['phone'] && Util::testTel($hotorder['phone']) == false ) {
			$errors = true;
			$response['reason'] = 'Телефон указан неверно';
		}

		//Email
		/*
		else if(isset($hotorder['email']) && !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $hotorder['email'])){
			$errors = true;
			$response['reason'] = 'E-mail указан неверно';
		}
		else if(isset($hotorder['email']) && ($hotorder['email'] && !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $hotorder['email'])) ){
			$errors = true;
			$response['reason'] = 'E-mail указан неверно';
		}
		*/

		//Дата заезда
		else if(!$hotorder['checkin']){
			$errors = true;
			$response['reason'] = "Дата заезда не указана";
		}

		//Дата выезда
		else if(!$hotorder['checkout']){
			$errors = true;
			$response['reason'] = "Дата выезда не указана";
		}
		else if( mktime(0, 0, 0, $hotorder_checkin[1], $hotorder_checkin[0], $hotorder_checkin[2]) >= mktime(0, 0, 0, $hotorder_checkout[1], $hotorder_checkout[0], $hotorder_checkout[2]) ){
			$errors = true;
			$response['reason'] = "Дата выезда должна быть больше даты заезда";
		}

		//Кол-во гостей
		else if(!$hotorder['guests']){
			$errors = true;
			$response['reason'] = "Укажите количество гостей";
		}

		//Стоимость
		else if(!$hotorder['price']){
			$errors = true;
			$response['reason'] = "Укажите желаемую стоимость";
		}

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно
		/////////////////////////////////////////////////////////////////////////////////////////////
		if(!$errors)
		{

			//Переход на второй шаг
			$response['step'] = 'code';

			//Код уже сгенерирован и телефонный номер не изменен
			if( isset($_SESSION['hotorder']['code']) && $hotorder['phone'] == $_SESSION['hotorder']['phone'] && isset($_SESSION['hotorder']['phone']) )
			{

				//Код указан верно
				if( intval($hotorder['code']) == $_SESSION['hotorder']['code'] )
				{

					/////////////////////////////////////////////////////////////////////////////////////////////
					/// Веерная отправка для всех владельцев города
					/////////////////////////////////////////////////////////////////////////////////////////////

					##############################
					# Проверка на жирность заяки
					##############################

					//Маркер жирности
					$isBold = false;
					//Срок заявки
					$delta = mktime(0, 0, 0, $hotorder_checkout[1], $hotorder_checkout[0], $hotorder_checkout[2]) - mktime(0, 0, 0, $hotorder_checkin[1], $hotorder_checkin[0], $hotorder_checkin[2]);
					$hotorder_days = ceil($delta / (60 * 60 * 24));
					//Определяем жирность
					if($hotorder_days >= 5) $isBold = true;

					//Надо ли отправлять
					if( true && !isset($_SESSION['hotorder']['sended']) )
					{

						$response['status'] = true;
						$response['reason'] = "Ваша заявка успешно отправлена!";

						//Объект рассылки
						//$email['list'] - массив адресов отправки
						//$email['uid'] - уникальный идентификатор письма
						//$email['date_create'] - время создания рассылки
						//$email['subject'] - тема письма
						//$email['body'] - текст письма
						$email = Array();

						////Данные для рассылки

						//Идентификатор рассылки
						$email['uid'] = md5(time().rand(0, 1000));

						//Время создания
						$email['date_create'] = date("Y-m-d H:i:s");

						//Ответ заявителю
						$email['reply'] = (isset($hotorder['email']) && $hotorder['email']) ? $hotorder['email'] : "";

						//Список адресатов
						$email['list'] = Array();

						// Список владельцев для отправки

						//Исключения из отправки
						$email_restrict = Array();

						//Лист отправки - все владельцы города, кроме отписавшихся
						$query = "SELECT DISTINCT(obj_u_id), LCASE(u_user.u_email) as u_email, obj_object.obj_price
								  FROM `obj_object`
								  LEFT JOIN u_user ON u_user.u_id = obj_object.obj_u_id
								  WHERE
								  	obj_object.obj_addr_city_uid = '".$params['addr_city_uid']."' AND
								  	obj_object.obj_enable AND
								  	obj_object.obj_hotorder AND
								  	u_user.u_email != '' AND
								  	u_user.u_subscribe_email AND
								  	obj_object.obj_price <= ".($hotorder['price'] + 500)." AND obj_object.obj_price >= ".($hotorder['price'] - 500)."
								  	GROUP BY u_user.u_email
								  	ORDER BY RAND()";

						$u_rows = $this->db->query($query)->fetchAll();

						foreach ($u_rows as $key => $value)
						{
							$email['list'][] = $value['u_email'];
						}

						////Есть адресаты
						if(count($email['list']))
						{

							$email['list'] = array_diff($email['list'], $email_restrict);

							////Текст письма

							//Заголовок
							$email['subject'] = 'Заявка на срочное заселение на '.$hotorder_days.' '.Util::pluralForm($hotorder_days, Array("день", "дня", "дней"));

							$email['body'] = '
							<!DOCTYPE html>
							<html lang="ru">
							<head>
								<meta charset="UTF-8">
								<title>Заявка на срочное заселение</title>
							</head>
							<body>

							<h2>Goyug.com - квартиры посуточно</h2>

							<h3>Новая заявка на <span style="color:red;">срочное</span> заселение</h3>
							<p>На сайте <a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a> заполнена заявка на срочное* заселение. Если у вас есть подходящий вариант, можете связаться с заказчиком.</p>

							<h3>Информация о заказчике</h3>
							<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
								<tbody>
								<tr>
									<td style="font-weight: bold;">Имя:</td>
									<td>'.$hotorder['name'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Телефон:</td>
									<td>'.((isset($hotorder['phone']) && $hotorder['phone']) ? Util::getHumanTel(Util::stripTel($hotorder['phone']))." (Подтвержден смс-кодом)" : "Не указан").'</td>
								</tr>
								</tbody>
							</table>

							<h3>Информация по заявке</h3>
							<table cellpadding="5" border="1" style="border-collapse: collapse;">
								<tbody>
								<tr>
									<td style="font-weight: bold;">Город:</td>
									<td>'.$params['addr_city'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Дата заезда:</td>
									<td>'.$hotorder['checkin'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Дата выезда:</td>
									<td>'.$hotorder['checkout'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Количество гостей:</td>
									<td>'.$hotorder['guests'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Желаемая стоимость:</td>
									<td>до '.$hotorder['price'].' руб.</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Предпочтительный район:</td>
									<td>'.($hotorder['district'] ? $hotorder['district'] : "Не указан").'</td>
								</tr>
								</tbody>
							</table>
							<p>* заявка на срочное заселение означает, что пользователь на сайте GoYug.com заполнил специальную форму «Срочное заселение» с целью подобрать жилье максимально быстро, получив предложения от собственников. Номер телефона заказчика в обязательном порядке проверяется смс-кодом.</p>
							<p><a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a></p>
							<p style="color: gray;">Заявка отправлена: '.$email['date_create'].'</p>
							%unsubscribe%
							</body>
							</html>';

							////Создаем письмо рассылки

							//Готовим таблицу для письма рассылки
							$tbl = new Zend_Db_Table(array(
								'name' => 'celt_cron_email_list_tasks',
								'primary' => 'celt_id'
							));

							//Пишем в базу письмо рассылки
							$tbl->insert(Array(
								'celt_uid' => $email['uid'],
								'celt_email_subject' => $email['subject'],
								'celt_email_body' => $email['body'],
								'celt_date_create' => $email['date_create']
								));

							////Создаем рассылку

							//Назначаем письмо владельцам
							foreach ($email['list'] as $key => $value) {
								$query = "INSERT INTO cel_cron_email_list (`cel_celt_uid`, `cel_email`, `cel_reply`, `cel_date_stage`) VALUES ('$email[uid]', '$value', 'info@goyug.com', '$email[date_create]')";
								$this->db->query($query);
							}

							////Оповещение об отправке

							//Отправка
							$email['body'] = $email['body'].'
							<hr />
							<p>Заявка сформировалась для '.count($email['list']).' владельцев</p>';

							try {
								$mail = new Zend_Mail('UTF-8');
								$mail->setBodyHTML($email['body']);
								$mail->setFrom('info@goyug.com', 'GoYug.com');
								$mail->setReplyTo('info@goyug.com', '');
								$mail->addTo('info@goyug.com', '');
								$mail->addBcc(Array('marselos@gmail.com', '160961@mail.ru'));
								// $mail->addBcc(Array('marselos@gmail.com'));
								$mail->setSubject($email['subject']);
								$mail->send();
							}
							catch(Exception $ex)
							{

							}

						}
						///// Если Есть адресаты - КОНЕЦ
						/////////////////////////////////////////////////////////////////////////////////////////////////////

						//Можно указать любое значение, главное чтобы поле существовало
						$_SESSION['hotorder']['sended'] = true;

					}
					//// Заявку уже отправляли
					/////////////////////////////////////////////////////////////////////////////////////////////////////
					else
					{
						$response['status'] = false;
						$response['reason'] = "Вы уже отправляли заявку";
					}

					/////////////////////////////////////////////////////////////////////////////////////////////////////
					/// Веерная отправка для всех владельцев города - КОНЕЦ
					/////////////////////////////////////////////////////////////////////////////////////////////////////

				}
				//Код указан неверно
				/////////////////////////////////////////////////////////////////////////////////////////////////////
				else
				{
					$response['phone'] = Util::stripTel($hotorder['phone']);
					$response['phone'] = Util::getHumanTel($hotorder['phone']);
					//$response['code'] = $_SESSION['hotorder']['code'];
					$response['reason'] = 'Проверочный код указан неверно';
				}

			}
			//Генерируем код и предлагаем ввести его
			/////////////////////////////////////////////////////////////////////////////////////////////////////
			else {
				$_SESSION['hotorder']['code'] = rand(1000, 9999);

				//Отправка смс-сообщения
				$hotorder['code'] = $_SESSION['hotorder']['code'];
				$this->sendSmsHotOrder($hotorder);

				//$response['code'] = $_SESSION['hotorder']['code'];
				$response['phone'] = Util::stripTel($hotorder['phone']);
				$response['phone'] = Util::getHumanTel($hotorder['phone']);
				$response['reason'] = 'Введите проверочный код, полученный в смс-сообщении';
			}

			//Сохраняем данные формы пользователя
			$_SESSION['hotorder']['name'] = $hotorder['name'];
			$_SESSION['hotorder']['phone'] = $hotorder['phone'];
			$_SESSION['hotorder']['checkin'] = $hotorder['checkin'];
			$_SESSION['hotorder']['checkout'] = $hotorder['checkout'];
			$_SESSION['hotorder']['guests'] = $hotorder['guests'];
			$_SESSION['hotorder']['price'] = $hotorder['price'];

		} else {
			$response['step'] = 'info';
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		return $response;
	}

	##########################################################################################
	public function sendParamsOrder($params)
	{

		//Ответ метода
		$response = Array();
		$response['status'] = false;
		$response['step'] = 'info';
		$response['reason'] = 'Неизвестная ошибка - попробуйте позже';

		//Ошибки формы
		$errors = false;

		//Данные по заявке
		$paramsorder = $params['paramsorder'];

		//Преобразование дат
		$paramsorder_checkin = explode('.', $paramsorder['checkin']);
		$paramsorder_checkout = explode('.', $paramsorder['checkout']);

		$paramsorder['guests'] = intval($paramsorder['guests']);
		$paramsorder['price'] = intval($paramsorder['price']);

		$paramsorder['comment'] = htmlspecialchars($paramsorder['comment']);

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя
		/////////////////////////////////////////////////////////////////////////////////////////////
		//Имя
		if(!$paramsorder['name']){
			$errors = true;
			$response['reason'] = "Укажите свое имя";
		}
		//Телефон (необязательно)
		// else if(!$paramsorder['phone']){
		// 	$errors = true;
		// 	$response['reason'] = "Укажите свой телефон";
		// }
		else if( $paramsorder['phone'] && Util::testTel($paramsorder['phone']) == false ) {
			$errors = true;
			$response['reason'] = 'Телефон указан неверно';
		}

		//Email

		else if(isset($paramsorder['email']) && !$paramsorder['email']){
			$errors = true;
			$response['reason'] = "Укажите свой E-mail";
		}

		else if( isset($paramsorder['email']) && $paramsorder['email'] && !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $paramsorder['email']) ){
			$errors = true;
			$response['reason'] = 'E-mail указан неверно';
		}
		//Дата заезда
		else if(!$paramsorder['checkin']){
			$errors = true;
			$response['reason'] = "Дата заезда не указана";
		}
		//Дата выезда
		else if(!$paramsorder['checkout']){
			$errors = true;
			$response['reason'] = "Дата выезда не указана";
		}
		else if( mktime(0, 0, 0, $paramsorder_checkin[1], $paramsorder_checkin[0], $paramsorder_checkin[2]) >= mktime(0, 0, 0, $paramsorder_checkout[1], $paramsorder_checkout[0], $paramsorder_checkout[2]) ){
			$errors = true;
			$response['reason'] = "Дата выезда должна быть больше даты заезда";
		}
		//Кол-во гостей
		else if(!$paramsorder['guests']){
			$errors = true;
			$response['reason'] = "Укажите количество гостей";
		}
		//Стоимость
		else if(!$paramsorder['price']){
			$errors = true;
			$response['reason'] = "Укажите желаемую стоимость";
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Проверка данных пользователя - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно
		/////////////////////////////////////////////////////////////////////////////////////////////
		if(!$errors)
		{

			//Переход на второй шаг
			$response['step'] = 'code';

			//Код уже сгенерирован и телефонный номер не изменен
			// if( isset($_SESSION['paramsorder']['code']) && $paramsorder['phone'] == $_SESSION['paramsorder']['phone'] && isset($_SESSION['paramsorder']['phone']) ){
			// if(true)
			// {

				//Код указан верно
				// if( intval($paramsorder['code']) == $_SESSION['paramsorder']['code'] ){
				// if(true)
				// {

					##############################
					# Проверка на жирность заяки #
					##############################

					//Маркер жирности
					$isBold = false;
					//Срок заявки
					$delta = mktime(0, 0, 0, $paramsorder_checkout[1], $paramsorder_checkout[0], $paramsorder_checkout[2]) - mktime(0, 0, 0, $paramsorder_checkin[1], $paramsorder_checkin[0], $paramsorder_checkin[2]);
					$paramsorder_days = ceil($delta / (60 * 60 * 24));
					//Определяем жирность
					if($paramsorder_days >= 5) $isBold = true;

					/////////////////////////////////////////////////////////////////////////////////////////////
					/// Веерная Рассылка по всем владельцам города
					/////////////////////////////////////////////////////////////////////////////////////////////


					//Надо ли отправлять
					// if( true && !isset($_SESSION['paramsorder']['sended']) )
					// {

						$response['status'] = true;
						$response['reason'] = "Ваша заявка успешно отправлена!";

						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/// Веер 1 ШАГ: Все владельцы или только абоненты города с абонплатой
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						//Объект рассылки
						//$email['list'] - массив адресов отправки
						//$email['uid'] - уникальный идентификатор письма
						//$email['date_create'] - время создания рассылки
						//$email['subject'] - тема письма
						//$email['body'] - текст письма
						$email = Array();

						////Данные для рассылки

						//Идентификатор рассылки
						$email['uid'] = md5(time().rand(0, 1000));

						//Время создания
						$email['date_create'] = date("Y-m-d H:i:s");

						//Ответ заявителю
						$email['reply'] = (isset($paramsorder['email']) && $paramsorder['email']) ? $paramsorder['email'] : "";
						//Список адресатов
						$email['list'] = Array();

						////Список владельцев для отправки

						//Исключения из отправки
						$email_restrict = Array();

						// Если город с абонплатой, выбираем лишь абонентов
						$query_ext_rental = (in_array($params['addr_city_uid'], $this->rental_cities)) ? ' AND u.u_rental = 1 ' : '';

						//Лист отправки - все владельцы города, кроме отписавшихся
						// 18.12.2017 - @marselos - Убрали фильтр по цене 
						// 								AND obj_price <= ".($paramsorder['price'] + 1500)."

						$query = "
							SELECT obj_u_id, LCASE(u.u_email) as u_email, obj_price
							FROM u_user AS u

							LEFT JOIN (
								SELECT *
								FROM obj_object
								WHERE obj_paramsorder
								AND obj_price >= 0
								GROUP BY obj_u_id
							)
							AS obj ON obj.obj_u_id = u.u_id

							WHERE u.u_role = 'owner'
							AND obj_addr_city_uid = '" . $params['addr_city_uid'] . "'
							AND u.u_email != ''
							AND u.u_email LIKE '%@%.%'
							AND u.u_subscribe_email
							" . $query_ext_rental . "
							ORDER BY RAND()
					  	";

						$u_rows = $this->db->query($query)->fetchAll();

						foreach ($u_rows as $key => $value) {
							$email['list'][] = $value['u_email'];
						}

						////Есть ли адресаты
						///
						///	17.12.2017 - Отменили отправку запроса по параметрам для абонентов
						///
						if(count($email['list']) && !in_array($params['addr_city_uid'], $this->rental_cities))
						{

							$email['list'] = array_diff($email['list'], $email_restrict);

							////Текст письма

							//Заголовок
							//$email['subject'] = 'Запрос по параметрам на '.$paramsorder_days.' '.Util::pluralForm($paramsorder_days, Array("день", "дня", "дней"));
							// $seed = rand(1, 2);
							// if($seed == 1){
							// 	$email['subject'] = $paramsorder['name'].' ищет квартиру на '.$paramsorder_days.' '.Util::pluralForm($paramsorder_days, Array("день", "дня", "дней")).' до '.$paramsorder['price'].' р.';
							// } else if($seed == 2) {
							// 	$email['subject'] = $paramsorder['name'].' подыскивает квартиру на '.$paramsorder_days.' '.Util::pluralForm($paramsorder_days, Array("день", "дня", "дней")).' до '.$paramsorder['price'].' р.';
							// }

							$email['subject'] = $paramsorder['name'].' ищет квартиру на '.$paramsorder_days.' '.Util::pluralForm($paramsorder_days, Array("день", "дня", "дней")).' до '.$paramsorder['price'].' р.';

							$email['body'] = '
							<!DOCTYPE html>
							<html lang="ru">
							<head>
								<meta charset="UTF-8">
								<title>Запрос по параметрам</title>
							</head>
							<body>

							<h2>Goyug.com - квартиры посуточно</h2>

							<h3 style="border:solid #df3341 2px; padding: 8px 12px; text-transform: uppercase; display: inline-block;">Новый запрос по параметрам</h3>
							<p>На сайте <a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a> гость заполнил специальную форму <strong>«Предложите мне вариант»</strong>, с целью подобрать жилье, отвечающее параметрам, получив предложения от собственников. Если у вас есть подходящий вариант &mdash; можете связаться с заказчиком.</p>

							<h3>Информация о госте</h3>
							<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
								<tbody>
								<tr>
									<td style="font-weight: bold;">Имя:</td>
									<td>'.$paramsorder['name'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Телефон:</td>
									<td>'.((isset($paramsorder['phone']) && $paramsorder['phone']) ? Util::getHumanTel(Util::stripTel($paramsorder['phone']))." (Подтвержден смс-кодом)" : "Не указан").'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">E-mail:</td>
									<td>'.((isset($paramsorder['email']) && $paramsorder['email']) ? $paramsorder['email'] : "Не указан").'</td>
								</tr>
								</tbody>
							</table>

							<h3>Информация об объекте</h3>
							<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
								<tbody>
								<tr>
									<td style="font-weight: bold;">Город:</td>
									<td>'.$params['addr_city'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Дата заезда:</td>
									<td>'.$paramsorder['checkin'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Дата выезда:</td>
									<td>'.$paramsorder['checkout'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Количество гостей:</td>
									<td>'.$paramsorder['guests'].'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Желаемая стоимость:</td>
									<td>до '.$paramsorder['price'].' руб.</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Предпочтительный район:</td>
									<td>'.($paramsorder['district'] ? $paramsorder['district'] : "Не указан").'</td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Комментарий к заявке:</td>
									<td>'.($paramsorder['comment'] ? $paramsorder['comment'] : "Не указан").'</td>
								</tr>
								</tbody>
							</table>
							<p>* письмо сформировано автоматически, ответ на него не требуется</p>
							<p><a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a></p>
							<p style="color: gray;">Заявка отправлена: '.$email['date_create'].'</p>
							%unsubscribe%
							</body>
							</html>';

							////Создаем письмо рассылки

							//Готовим таблицу для письма рассылки
							$tbl = new Zend_Db_Table(array(
								'name' => 'celt_cron_email_list_tasks',
								'primary' => 'celt_id'
							));

							//Пишем в базу письмо рассылки
							$tbl->insert(Array(
								'celt_uid' => $email['uid'],
								'celt_email_subject' => $email['subject'],
								'celt_email_body' => $email['body'],
								'celt_date_create' => $email['date_create']
							));

							////Создаем рассылку

							//Назначаем письмо владельцам
							foreach ($email['list'] as $key => $value)
							{
								$query = "INSERT INTO cel_cron_email_list (`cel_celt_uid`, `cel_email`, `cel_reply`, `cel_date_stage`) VALUES ('$email[uid]', '$value', 'info@goyug.com', '$email[date_create]')";
								$this->db->query($query);
							}

						}
						////Есть ли адресаты

						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/// Веер 1 ШАГ: Все владельцы или только абоненты города с абонплатой - КОНЕЦ
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/// Веер 2 ШАГ: НЕ абоненты города с абонплатой
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						if(in_array($params['addr_city_uid'], $this->rental_cities))
						{
							////Данные для рассылки

							//Идентификатор рассылки
							$email['uid'] = md5(time().rand(0, 1000));

							//Время создания
							$email['date_create'] = date("Y-m-d H:i:s");

							//Ответ заявителю
							$email['reply'] = (isset($paramsorder['email']) && $paramsorder['email']) ? $paramsorder['email'] : "";
							//Список адресатов
							$email['list'] = Array();

							////Список владельцев для отправки

							//Исключения из отправки
							$email_restrict = Array();

							//Лист отправки - все владельцы города, кроме отписавшихся
							// 18.12.2017 - @marselos - Убрали фильтр по цене 
							// 								AND obj_price <= ".($paramsorder['price'] + 1500)."
							$query = "
								SELECT obj_u_id, LCASE(u.u_email) as u_email, obj_price
								FROM u_user AS u

								LEFT JOIN (
									SELECT *
									FROM obj_object
									WHERE obj_price >= 0
									GROUP BY obj_u_id
								)
								AS obj ON obj.obj_u_id = u.u_id

								WHERE u.u_role = 'owner'
								AND obj_addr_city_uid = '" . $params['addr_city_uid'] . "'
								AND u.u_email != ''
								AND u.u_email LIKE '%@%.%'
								AND u.u_rental = 0
								ORDER BY RAND()
						  	";

							$u_rows = $this->db->query($query)->fetchAll();

							foreach ($u_rows as $key => $value) {
								$email['list'][] = $value['u_email'];
							}

							////Есть ли адресаты
							if(count($email['list']))
							{

								$email['list'] = array_diff($email['list'], $email_restrict);

								$email['subject'] = '* ' . $paramsorder['name'].' ищет квартиру на '.$paramsorder_days.' '.Util::pluralForm($paramsorder_days, Array("день", "дня", "дней")).' до '.$paramsorder['price'].' р.';

								$email['body'] = '
								<!DOCTYPE html>
								<html lang="ru">
								<head>
									<meta charset="UTF-8">
									<title>Запрос по параметрам</title>
								</head>
								<body>

								<h2>Goyug.com - квартиры посуточно</h2>

								<h3 style="border:solid #df3341 2px; padding: 8px 12px; text-transform: uppercase; display: inline-block;">*** Новый запрос по параметрам</h3>
								<p>На сайте <a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a> гость заполнил специальную форму <strong>«Предложите мне вариант»</strong>, с целью подобрать жилье, отвечающее параметрам, получив предложения от собственников. Если у вас есть подходящий вариант &mdash; можете связаться с заказчиком.</p>

								<p style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block;">
									Уважаемый владелец!<br>
			            				Вы не активировали услугу "АБОНЕМЕНТ" на нашем ресурсе.<br>
            				Для того чтобы иметь возможность видеть контакты гостя отправившего заявку, необходимо чтобы ваш месячный абонемент был оплачен на момент поступления заявки.<br>
									Стоимость месячного абонемента - 100 рублей</p>

			            			<p><a href="http://lk.goyug.com/" style="padding: 12px 18px; background: #d24b4b; color: #fff; display: inline-block; text-transform: uppercase;">Войти в личный кабинет GOYUG.COM</a></p>

								<h3>Информация о госте</h3>
								<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
									<tbody>
									<tr>
										<td style="font-weight: bold;">Имя:</td>
										<td>'.$paramsorder['name'].'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Телефон гостя:</td>
										<td>*************************</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">E-mail гостя:</td>
										<td>>*************************</</td>
									</tr>
									</tbody>
								</table>

								<h3>Информация об объекте</h3>
								<table cellpadding="5" border="1" style="border-collapse: collapse; margin-bottom: 15px;">
									<tbody>
									<tr>
										<td style="font-weight: bold;">Город:</td>
										<td>'.$params['addr_city'].'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Дата заезда:</td>
										<td>'.$paramsorder['checkin'].'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Дата выезда:</td>
										<td>'.$paramsorder['checkout'].'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Количество гостей:</td>
										<td>'.$paramsorder['guests'].'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Желаемая стоимость:</td>
										<td>до '.$paramsorder['price'].' руб.</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Предпочтительный район:</td>
										<td>'.($paramsorder['district'] ? $paramsorder['district'] : "Не указан").'</td>
									</tr>
									<tr>
										<td style="font-weight: bold;">Комментарий к заявке:</td>
										<td>'.($paramsorder['comment'] ? $paramsorder['comment'] : "Не указан").'</td>
									</tr>
									</tbody>
								</table>
								<p>* письмо сформировано автоматически, ответ на него не требуется</p>
								<p><a href="http://'.$params['addr_city_uid'].'.goyug.com">GoYug.com</a></p>
								<p style="color: gray;">Заявка отправлена: '.$email['date_create'].'</p>
								%unsubscribe%
								</body>
								</html>';

								////Создаем письмо рассылки

								//Готовим таблицу для письма рассылки
								$tbl = new Zend_Db_Table(array(
									'name' => 'celt_cron_email_list_tasks',
									'primary' => 'celt_id'
								));

								//Пишем в базу письмо рассылки
								$tbl->insert(Array(
									'celt_uid' => $email['uid'],
									'celt_email_subject' => $email['subject'],
									'celt_email_body' => $email['body'],
									'celt_date_create' => $email['date_create']
								));

								////Создаем рассылку

								//Назначаем письмо владельцам
								foreach ($email['list'] as $key => $value)
								{
									$query = "INSERT INTO cel_cron_email_list (`cel_celt_uid`, `cel_email`, `cel_reply`, `cel_date_stage`) VALUES ('$email[uid]', '$value', 'info@goyug.com', '$email[date_create]')";
									$this->db->query($query);
								}

							} /// Если есть адресаты

						} /// Если город в списке городов с абонплатой
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/// Веер 2 ШАГ: НЕ абоненты города с абонплатой - КОНЕЦ
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						////Оповещение об отправке

						//Отправка
						$email['body'] = $email['body'].'
						<hr />
						<p>Запрос по параметрам. Сформирован для для '.count($email['list']).' владельцев</p>
						<p>Город: '.$params['addr_city'].'</p>
						<p>Желаемая цена: '.$paramsorder['price'].'</p>
						<p>Имя: '.$paramsorder['name'].'</p>';

						try {
							$mail = new Zend_Mail('UTF-8');
							$mail->setBodyHTML($email['body']);
							$mail->setFrom('info@goyug.com', 'GoYug.com');
							$mail->setReplyTo('info@goyug.com', '');
							$mail->addTo('info@goyug.com', '');
							$mail->addBcc(Array('marselos@gmail.com', '160961@mail.ru'));
							// $mail->addBcc(Array('marselos@gmail.com'));
							$mail->setSubject($email['subject']);
							$mail->send();
						} catch(Exception $ex)
						{

						}

						//Можно указать любое значение, главное чтобы поле существовало
						$_SESSION['paramsorder']['sended'] = true;

					// }

					//// Уже отправляли заявку
					// else
					// {
					// 	$response['status'] = false;
					// 	$response['reason'] = "Вы уже отправляли заявку";
					// }

					/////////////////////////////////////////////////////////////////////////////////////////////////////
					/// Веерная Рассылка по всем владельцам города - КОНЕЦ
					/////////////////////////////////////////////////////////////////////////////////////////////////////

				// }

				//Код указан неверно
				// else
				// {
				// 	$response['phone'] = Util::stripTel($paramsorder['phone']);
				// 	$response['phone'] = Util::getHumanTel($paramsorder['phone']);
				// 	//$response['code'] = $_SESSION['paramsorder']['code'];
				// 	$response['reason'] = 'Проверочный код указан неверно';
				// }

			// }
			//Генерируем код и предлагаем ввести его
			// else
			// {
			// 	$_SESSION['paramsorder']['code'] = rand(1000, 9999);

			// 	//Отправка смс-сообщения
			// 	$paramsorder['code'] = $_SESSION['paramsorder']['code'];
			// 	$this->sendSmsParamsOrder($paramsorder);

			// 	//$response['code'] = $_SESSION['paramsorder']['code'];
			// 	$response['phone'] = Util::stripTel($paramsorder['phone']);
			// 	$response['phone'] = Util::getHumanTel($paramsorder['phone']);
			// 	$response['reason'] = 'Введите проверочный код, полученный в смс-сообщении';
			// }

			//Сохраняем данные формы пользователя
			$_SESSION['paramsorder']['name'] = $paramsorder['name'];
			$_SESSION['paramsorder']['phone'] = $paramsorder['phone'];
			$_SESSION['paramsorder']['email'] = $paramsorder['email'];
			$_SESSION['paramsorder']['checkin'] = $paramsorder['checkin'];
			$_SESSION['paramsorder']['checkout'] = $paramsorder['checkout'];
			$_SESSION['paramsorder']['guests'] = $paramsorder['guests'];
			$_SESSION['paramsorder']['price'] = $paramsorder['price'];
			$_SESSION['paramsorder']['comment'] = $paramsorder['comment'];

		}
		else
		{
			$response['step'] = 'info';
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		/// Форма заполнена корректно - КОНЕЦ
		/////////////////////////////////////////////////////////////////////////////////////////////

		return $response;
	}

	##########################################################################################
	public function sendRingQuery($params) {

		//Ответ метода
		$response = Array();
		$response['status'] = false;

		//Dummy
		usleep(2000000);
		usleep(rand(500000, 1000000));

		return $response;
	}

	##########################################################################################
	public function logCallback($params) {

		//Ответ метода
		$response = Array();

		//Доступные варианты результата
		$results = Array("yes", "no", "none");

		//Находим объект
		$obj_id = intval($params['obj_id']);
		$query = "SELECT * FROM obj_object WHERE obj_id = '$obj_id'";
		$obj_row = $this->db->query($query)->fetch();

		//Находим владельца
		$query = "SELECT * FROM u_user WHERE u_id = '$obj_row[obj_u_id]'";
		$u_row = $this->db->query($query)->fetch();

		//Проверка корректности результата
		if( in_array($params['result'], $results) ){

			//Результат звонка
			$clb_result = $params['result'];
			//Дата
			$clb_date = date("Y-m-d H:i:s");

			//Запись в базу
			$query = "INSERT INTO clb_callback (`clb_obj_id`, `clb_u_id`, `clb_phone`, `clb_result`, `clb_ip`, `clb_date`) VALUES ('$obj_id', '$u_row[u_id]', '$u_row[u_phone_1]', '$clb_result', '$_SERVER[REMOTE_ADDR]', '$clb_date')";
			$this->db->query($query);

			$response['status'] = true;

		} else {

			mail("marselos@gmail.com", "Возможная атака сервера", "Непредусмотренный результат звонка: ".$params['result']);
			$response['status'] = false;
		}

		return $response;
	}

	##########################################################################################
	/* отключено 10 июля 2015 г.
	public function getValues($table, $obj_id) {

		$items = array();
		$tbl = new Zend_Db_Table(array(
			'name' => 'v_value',
			'primary' => 'v_id'
		));

		$rows = $tbl
			->fetchAll($tbl
			->select()
			->from(array(
			'v_value'
		) , array(
			'v_key'
		))
			->where('obj_id = ?', $obj_id)
			->where('v_table = ?', $table))->toArray();

		foreach ($rows as $id => $val) {
			$items[] = $val['v_key'];
		}

		return $items;
	}*/
	##########################################################################################
	/* скопировано 10 июля 2015 г. */
	public function getValues($tables = array() , $obj_id) {

		$items = array();
		$tbl = new Zend_Db_Table(array(
			'name' => 'v_value',
			'primary' => 'v_id'
		));

		foreach ($tables as $table) {
			$rows = $tbl
				->fetchAll($tbl
				->select()
				->from(array('v_value') , array('v_key'))
				->where('obj_id = ?', $obj_id)
				->where('v_table = ?', $table))->toArray();

			foreach ($rows as $id => $val) {
				$items[$table][] = $val['v_key'];
			}
		}

		return $items;
	}

	##########################################################################################
	public function getValuesName($table, $prefix, $obj_id) {

		$items = array();

		$db = Zend_Registry::get( 'db' );

		//SQL query
		//*****************************************************************
		$query = '
			SELECT  *

			FROM v_value AS val

			LEFT JOIN ' . $table . ' AS tab_names
			ON val.v_key = tab_names.' . $prefix . '_id

			WHERE obj_id = ' . $obj_id . '
			AND v_table = "' . $table . '"
		';

		$rows= $db->query($query)->fetchAll();

		foreach ($rows as $id => $val) {
			$items[] = $val[$prefix . '_name_ru'];
		}

		return $items;
	}

	##########################################################################################
	public function getStayPeriods() {
		$items = array();
		$tbl = new Zend_Db_Table(array(
			'name' => 'per_period',
			'primary' => 'per_id'
		));

		$rows = $tbl
			->fetchAll($tbl
			->select()
			->from(array(
			'per_period'
		) , array(
			'*'
		))
			->where('per_enable = ?', "1"))
			->toArray();

		foreach ($rows as $id => $val) {
			$items[$val['per_id']] = array(
				"name" => $val['per_pricename_' . $this->lang]
			);
		}

		return $items;
	}

	##########################################################################################
	//Далее взято из модели ai Object.php
	##########################################################################################

	##########################################################################################
	public function getParamValues($tables = array() , $obj_id) {

		$items = array();
		$tbl = new Zend_Db_Table(array(
			'name' => 'v_value',
			'primary' => 'v_id'
		));

		foreach ($tables as $table) {
			$rows = $tbl
				->fetchAll($tbl
				->select()
				->from(array('v_value') , array('v_key','v_val'))
				->where('obj_id = ?', $obj_id)
				->where('v_table = ?', $table))->toArray();

			foreach ($rows as $id => $val) {
				$items[$table . '_' . $val['v_key']] = $val['v_val'];
			}
		}

		return $items;
	}

	##########################################################################################
	public function getMultiselect($table, $prefix, $order = array()) {

		$items = array();

		$tbl = new Zend_Db_Table(array(
			'name' => $table,
			'primary' => $prefix . '_id'
		));

		$select = $tbl->select()->from(array($table) , array($prefix . '_id', $prefix . '_name_' . $this->lang))->where($prefix . '_enable = ?', 1);

		if (!empty($order)) $select->order($order);
		else {

			$select
				->order($prefix . '_order ASC')
				->order($prefix . '_name_' . $this->lang . ' ASC');
		}

		$rows = $tbl
			->fetchAll($select)->toArray();

		foreach ($rows as $id => $val) {
			$items[$val[$prefix . '_id']] = $val[$prefix . '_name_' . $this->lang];
		}

		return $items;
	}

	##########################################################################################
	public function getSmartfiltersMultiselect() {

		$items = array();

		$db = Zend_Registry::get( 'db' );

		$rows = $db->query("
			SELECT sf.sf_id, sf.sf_name_" . $this->lang . ", ot.ot_name_" . $this->lang . "
			FROM sf_smartfilters AS sf
			LEFT JOIN ot_object_type AS ot
			ON sf.ot_id = ot.ot_id
			WHERE sf.sf_enable = 1
			ORDER BY sf.sf_level ASC, sf.sf_order ASC"
		)->fetchAll();

		foreach ($rows as $id => $val) {
			$items[$val['sf_id']] = $val["ot_name_" . $this->lang] . " - " . $val['sf_name_' . $this->lang];
		}

		return $items;
	}

	##########################################################################################
	public function getOwners() {

		$sql_where = "";

		if($this->auth->getIdentity()->u_role == "editor")
		{
			$sql_where = " AND u.u_creater = '".$this->auth->getIdentity()->u_username."' ";
		}


		$items = array();

		$db = Zend_Registry::get( 'db' );

		$rows = $db->query("
			SELECT *
			FROM u_user AS u
			WHERE u.u_active = 1
			AND u.u_role = 'owner'
			" . $sql_where . "
			ORDER BY u.u_lastname"
		)->fetchAll();

		$items[0] = "";
		foreach ($rows as $id => $val) {
			$items[$val['u_id']] = $val['u_lastname'] . " " . $val['u_firstname']  . " - " . $val['u_phone_1']  . " - " . $val['u_email'];
		}

		return $items;
	}

	##########################################################################################
	public function getParamsFields($table, $prefix) {

		$items = array();

		$tbl = new Zend_Db_Table(array(
			'name' => $table,
			'primary' => $prefix . '_id'
		));

		$rows = $tbl
			->fetchAll($tbl
			->select()
			->from(array(
			$table
		) , array(
			$prefix . '_id',
			$prefix . '_name_' . $this
				->lang,
			$prefix . '_unit_' . $this
				->lang,
			$prefix . '_type'
		))
			->where($prefix . '_enable = ?', 1)
			->order($prefix . '_order ASC')
			->order($prefix . '_name_' . $this
			->lang . ' ASC'))
			->toArray();

		foreach ($rows as $id => $val) {
			$items[$val[$prefix . '_id']] = array(
				'name' => $val[$prefix . '_name_' . $this
					->lang],
				'type' => $val[$prefix . '_type'],
				'unit' => $val[$prefix . '_unit_' . $this->lang],
			);
		}

		return $items;
	}
}
?>
