<?
/**
 * Class and Function List:
 * Function list:
 * - getItems()
 * - updateUserLoginTime()
 * - getMultiselect()
 * Classes list:
 * - Ui_Model_User extends Zend_Db_Table_Abstract
 */
require_once("Util.php");

class Ui_Model_User extends Zend_Db_Table_Abstract {

	##########################################################################################
	public $_name = 'u_user';
	public $_primary = 'u_id';

    ##########################################################################################
    public function init() {

    	$this->auth = Zend_Auth::getInstance();
        $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : "";

        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->log = new Ui_Model_Log();

        $this->object = new Ui_Model_Objects;
    }

    ##########################################################################################
    public function isObjectIsOwn($obj_id) {
        $result = "";

        if($this->identity){
            //Объект
            $result = $this->db->query("
                SELECT obj_id
                FROM obj_object
                WHERE obj_u_id = '".$this->identity->u_id."' AND obj_id = '".$obj_id."'
                LIMIT 1
            ")->fetch();
        }

        if($result){
            return true;
        }

        return false;
    }

	##########################################################################################
	public function getItems() {
		$select = $this->select();
		$select->order(array(
			'u_username',
			'u_lastname',
			'u_firstname'
		));
		return $this->fetchAll($select);
	}

	##########################################################################################
	public function updateUserLoginTime($userId) {

		// fetch the user's row
		$row = $this
			->find($userId)->current();
		$row->u_lastlogin = date('Y-m-d H:i:s');
		$row->save();

		return $row;
	}

	##########################################################################################
	public function getMultiselect($u_role) {
		$res = array();

		$select = $this->select('u_id', 'u_firstname');
		$select->where('u_role = ?', ucfirst($u_role));
		$items = $this
			->fetchAll($select)->toArray();

		foreach ($items as $id => $val) $res[$val['u_id']] = $val['u_firstname'];

		return $res;
	}

	##########################################################################################
	public function getUserObject($u_id, $obj_id) {

		//Айди не пустые
		if( (!empty($u_id) && $u_id > 0) && (!empty($obj_id) && $obj_id) ) {

			//Объект
			$result = $this->db->query("
			    SELECT *
			    FROM obj_object
			    WHERE /*obj_enable = 1
			    AND*/ obj_u_id = '".$u_id."' AND obj_id = '".$obj_id."'
			    LIMIT 1
			")->fetch();

			//Картинки объекта
			$images_arr = $this->getObjectImages($obj_id);
			//print_r($images_arr);

			foreach ($images_arr as $i_id => $i_val) {
			    $result['obj_image'][] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
			}

			return $result;
		}
	}

	##########################################################################################
	public function getUserObjects($u_id) {

		if(!empty($u_id) && $u_id > 0) {

			$result = $this->db->query("
			    SELECT *
			    FROM obj_object
			    WHERE obj_u_id = '".$u_id."'
			    ORDER BY obj_id DESC
			")->fetchAll();

			foreach ($result as $key => $value) {

				$obj_id = $value['obj_id'];

                ##############
                ////Картиночки
                //Картинки объекта
                $images_arr = $this->getObjectImages($obj_id);

                /* Здесь крутит все изображения и оставляется последнее - зачем?
		        foreach ($images_arr as $i_id => $i_val) {
		            $result[$key]['obj_image'] = "/upload/object/" . $obj_id . "/thumbnail/" . $i_val["f_name"];
		        }*/

                //Берем первую (главную) картинку для превью
                if (count($images_arr)) {
                    $result[$key]['obj_image'] = "/upload/object/" . $obj_id . "/x500/" . $images_arr[0]["f_name"];
                } else {
                    $result[$key]['obj_image'] = "/images/default.jpg";
                }

                //Изображение по умолчанию
                if(!file_exists(".".$result[$key]['obj_image'])) $result[$key]['obj_image'] = "/images/default.jpg";
                ////Картиночки окончание
                ########################

                ##############
                ////Типы жилья
                //Доступные типы жилья
                $object_types = $this->object->getMultiselect('ot_object_type', 'ot');
                //Выбранные типы жилья
                $object_type = $this->object->getValues(Array('ot_object_type'), $obj_id);
                //Выбранные типы жилья словами
                $ot_object_type = Array();
                foreach ($object_types as $object_type_key => $object_type_value) {
                    if( isset($object_type['ot_object_type']) && in_array($object_type_key, $object_type['ot_object_type']) ) $ot_object_type[] = $object_type_value;
                }
                //Выбранные типы жилья в строку
                $result[$key]['ot_object_type'] = implode(", ", $ot_object_type);
                ////Типы жилья окончание
                ########################
			}//foreach

			return $result;
		}
	}

    ##########################################################################################
    public function getObjectImages($object_id = 0) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));

        $items = array();

        if ($object_id > 0) {
            $items = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array(
                'f_files'
            ) , array(
                'f_name'
            ))
                ->where('f_ucid = ?', 'object')
                ->where('f_uid = ?', $object_id)
                ->order('f_order'))->toArray();
        }

        return $items;
    }

    ##########################################################################################
    public function updateObject($formValues) {

        //Ошибка заполнения
        $isError = false;

        //Ответ
        $response = Array();

        //Не выбран город
        if(!$formValues['obj_addr_city_uid']){
            $response['status'] = false;
            $response['reason'] = 'Необходимо выбрать город';
            return $response;
        }
        //Не указан адрес
        else if(!$formValues['obj_addr_street'] || !$formValues['obj_addr_number']){
            $response['status'] = false;
            $response['reason'] = 'Необходимо указать адрес оъекта с номером дома';
            return $response;
        }
        //Не указан этаж и этажность дома
        else if(!$formValues['obj_addr_floor'] || !$formValues['obj_addr_elevation']){
            $response['status'] = false;
            $response['reason'] = 'Укажите этаж и этажность дома';
            return $response;
        }
        //Не указана метка на карте
        else if(!$formValues['obj_addr_lon'] || !$formValues['obj_addr_lat']){
            $response['status'] = false;
            $response['reason'] = 'Вы не указали объект на карте';
            return $response;
        }
        //Не указана цена аренды
        else if(!$formValues['obj_price'] || $formValues['obj_price'] == 0){
            $response['status'] = false;
            $response['reason'] = 'Необходимо указать стоимость аренды объекта за сутки';
            return $response;
        }
        //Не указано название объекта
        else if(!$formValues['obj_name_ru']){
            $response['status'] = false;
            $response['reason'] = 'Необходимо указать название объекта. В качестве названия допускается указывать адрес.';
            return $response;
        }

        if(!$isError){

            //Вбитый адрес
            $formValues['obj_search_address'] = $formValues['search_address'];
            $formValues['status'] = true;

        	//Обновление данных
            if (isset($formValues['obj_id']) && !empty($formValues['obj_id'])) {

                $row = $this->object->find($formValues['obj_id'])->current();
                $row->setFromArray($formValues);

                //--------------- Logger--------------------
                $this->log->write( array('status' => 'success', 'result' => 'update'), $formValues );
            }
            //Создание нового объекта
            else {

                //Тип жилья по умолчанию
                if( !isset($formValues['ot_object_type']) || !$formValues['ot_object_type'] ) $formValues['ot_object_type'][] = 10;

                //По умолчанию, объект учавствует в запросе по параметрам
                $formValues['obj_paramsorder'] = 1;

                //Координаты по умолчанию - по-хорошему надо заполнять координатами города владельца - но пока Краснодар
                if( !isset($formValues['obj_addr_lon']) || !$formValues['obj_addr_lon'] )           $formValues['obj_addr_lon'] = "38.96222000";
                if( !isset($formValues['obj_addr_lon_view']) || !$formValues['obj_addr_lon_view'] ) $formValues['obj_addr_lon_view'] = "38.96222000";
                if( !isset($formValues['obj_addr_lat']) || !$formValues['obj_addr_lat'] )           $formValues['obj_addr_lat'] = "45.06148367";
                if( !isset($formValues['obj_addr_lat_view']) || !$formValues['obj_addr_lat_view'] ) $formValues['obj_addr_lat_view'] = "45.06148367";

                // Помечаем объект как "на модерации"
                $formValues['obj_moderated'] = '0';

                //Создаем новую строку
                $row = $this->object->createRow($formValues);
                $row->obj_create = date('Y-m-d H:i:s');
                $formValues['obj_create'] = date('d.m.Y H:i:s');

                //Информация о владельце
                $row->obj_creater = $this->auth->getIdentity()->u_username;
                $row->obj_u_id = $this->auth->getIdentity()->u_id;

                //Уникальный идентификатор
                $row->obj_uid = Util::generateUID();

                //--------------- Logger--------------------
                $this->log->write( array('status' => 'success', 'result' => 'create'), $formValues );
            }

            if ($row) {

                $row->obj_update = $formValues['obj_update'] = date('Y-m-d H:i:s');
                $formValues['obj_update'] = date('d.m.Y H:i:s');

                $row->obj_updater = $this->auth->getIdentity()->u_username;

                //Проверки
                $null_fields_check = array(
                    "obj_rooms",
                    "obj_bedrooms",
                    "obj_bathrooms",
                    "obj_beds_sngl",
                    "obj_beds_dbl",
                    "obj_guests",
                    "obj_order",
                    "obj_sqr",
                    "obj_addr_floor",
                    "obj_addr_lon",
                    "obj_addr_lon_view",
                    "obj_addr_lat",
                    "obj_addr_lat_view",
                    "obj_price",
                    "obj_price_minstay",
                    "obj_rating",
                    "obj_deposit",
                );

                foreach ($null_fields_check as $check_field) {
                    $row->{$check_field} = (!empty($row->{$check_field})) ? $row->{$check_field} : 0;
                }//foreach

                //Выдавал ошибку при сохранении
                // добавил эту строку 21 апреля 2016 г.
                // блок, что ниже был закомментирован
                $ct_uid = $formValues['obj_addr_city_uid'];

                // // Transliterate City Name
                // $ct_uid = Util::Rus2Lat( $formValues['obj_addr_city'], true );

                // $city_bad = array(
                //     "jeleznovodsk",
                //     "astrahan",
                //     "bolshoysochi",
                //     "voronej",
                //     "ustjlabinsk",
                // );
                // $city_good   = array(
                //     "zheleznovodsk",
                //     "astrakhan",
                //     "sochi",
                //     "voronezh",
                //     "ustlabinsk",
                // );

                // $formValues['obj_addr_city_uid'] = str_replace($city_bad, $city_good, $formValues['obj_addr_city_uid']);


                // Помечаем объект как "на модерации"
                $row->obj_moderated = '0';

                $formValues['obj_id'] = $row->save();

                //13.11.2013 Исключили справочник - rt_rent_type
                $this->updateSelected(array(
                    'ot_object_type',
                    'srv_service',
                    'at_amenity',
                    'eq_equipment',
                    'rst_restriction',
                    // 'ot_smartfilters'
                ), $formValues);

                $this->updateParameters(array(
                    'prm_parameter'
                ), $formValues);


                $obj_pos = array(
                    'lat' => $formValues['obj_addr_lat'],
                    'lng' => $formValues['obj_addr_lon'],
                );

                // $this->updateObjtypesStat($formValues["ot_object_type"]);
                // $this->updateCityStat(
                //     $ct_uid,
                //     $formValues['obj_addr_city'],
                //     $formValues['obj_price'],
                //     $obj_pos
                // );

                return $formValues;
            } else {
                throw new Zend_Exception("Item update failed. Item not found!");
            }
        }
    }

    ##########################################################################################
    public function updateProfile($formValues) {

        //Ответ метода
        $response = Array();
        $response['status'] = true;

        //Ошибки
        $isError = false;

        //Чекбоксы
        if(!isset($formValues['u_subscribe_news'])) $formValues['u_subscribe_news'] = 0;
        if(!isset($formValues['u_subscribe_email'])) $formValues['u_subscribe_email'] = 0;
        if(!isset($formValues['u_subscribe_sms'])) $formValues['u_subscribe_sms'] = 0;
        if(!isset($formValues['u_show_email'])) $formValues['u_show_email'] = 0;

        //Обновление данных
        $row = $this->find($formValues['u_id'])->current();
        $row->setFromArray($formValues);

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'update'), $formValues );

        if ($row) {

            //Дата обновления
            $row->u_update = date('Y-m-d H:i:s');
            $formValues['u_update'] = $row->u_update;

            //Редактор
            $row->u_updater = $this->auth->getIdentity()->u_username;

            ////Проверки
            //Имя
            if(!$row->u_firstname){
                $isError = true;
                $response['reason'] = 'Имя не должно быть пустым';
                $response['class'] = 'u_firstname';
            }
            //Телефоны
            else if( !$row->u_phone_1 || Util::testTel($row->u_phone_1) == false ) {
                $isError = true;
                $response['reason'] = 'Телефон указан неверно';
                $response['class'] = 'u_phone_1';
                $response['phone'] = $row->u_phone_1;
            }
            else if( $row->u_phone_2 && Util::testTel($row->u_phone_2) == false ) {
                $isError = true;
                $response['reason'] = 'Телефон указан неверно';
                $response['class'] = 'u_phone_2';
                $response['phone'] = $row->u_phone_2;
            }
            else if( $row->u_phone_3 && Util::testTel($row->u_phone_3) == false ) {
                $isError = true;
                $response['reason'] = 'Телефон указан неверно';
                $response['class'] = 'u_phone_3';
                $response['phone'] = $row->u_phone_3;
            }
            //Телефоны - проверка существования
            else if( $this->checkPhoneExist($row->u_phone_1) ) {
                $isError = true;
                $response['reason'] = 'Такой телефон уже занят';
                $response['class'] = 'u_phone_1';
                $response['phone'] = $row->u_phone_1;
            }
            else if( $this->checkPhoneExist($row->u_phone_2) ) {
                $isError = true;
                $response['reason'] = 'Такой телефон уже занят';
                $response['class'] = 'u_phone_2';
                $response['phone'] = $row->u_phone_2;
            }
            else if( $this->checkPhoneExist($row->u_phone_3) ) {
                $isError = true;
                $response['reason'] = 'Такой телефон уже занят';
                $response['class'] = 'u_phone_3';
                $response['phone'] = $row->u_phone_3;
            }
            //E-mail
            else if( !$row->u_email || !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $row->u_email) ){
                $isError = true;
                $response['reason'] = 'E-mail указан неверно';
                $response['class'] = 'u_email';
            }
            //Пароль
            else if( $formValues['password'] && ($formValues['password'] != $formValues['password_confirm']) ){
                $isError = true;
                $response['reason'] = 'Пароли не совпадают';
                $response['class'] = 'password';
            }
            else if( $formValues['password'] && (!preg_match("/^(.{6,})$/", $formValues['password'])) ){
                $isError = true;
                $response['reason'] = 'Пароль должен быть не менее 6-ти символов';
                $response['class'] = 'password';
            }
            ////Проверки - окончание

            //Если все ОК - сохраняем
            if(!$isError){

                //Телефоны
                $row->u_phone_1 = Util::stripTel($row->u_phone_1);
                $row->u_phone_2 = Util::stripTel($row->u_phone_2);
                $row->u_phone_3 = Util::stripTel($row->u_phone_3);

                //Пароль
                if($formValues['password']){
                    $row->u_password = md5($formValues['password']);
                }

                $row->save();
            }
            else {
                $response['status'] = false;
            }

        } else {
            //throw new Zend_Exception("User update failed. User not found!");
            $response['status'] = false;
            $response['reason'] = 'Пользователь не обнаружен';
        }

        return $response;
    }

    ##########################################################################################
    public function feedback($formValues) {

        //Ответ метода
        $response = Array();
        $response['status'] = true;

        //Ошибки
        $isError = false;

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'feedback'), $formValues );

        //Проверка
        if(!$formValues['text']){
            $isError = true;
            $response['status'] = false;
            $response['class'] = 'text';
            $response['reason'] = 'Напишите сообщение';
        }
        //E-mail
        else if( !$formValues['u_email'] || !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $formValues['u_email']) ){
            $isError = true;
            $response['status'] = false;
            $response['reason'] = 'E-mail указан неверно';
            $response['class'] = 'u_email';
        }

        //Запись в базу и отправка письма
        if(!$isError){

            //Коннект к таблице fb_feedback
            $tbl_fb = new Zend_Db_Table(array(
                'name' => 'fb_feedback',
                'primary' => 'fb_id'
            ));

            //Коннект к таблице u_user
            $tbl_u = new Zend_Db_Table(array(
                'name' => 'u_user',
                'primary' => 'u_id'
            ));

            //Создаем строку обратной связи
            $row_fb = $tbl_fb->createRow();
            $row_fb->fb_u_id = $formValues['u_id'];
            $row_fb->fb_email = $formValues['u_email'];
            $row_fb->fb_text = htmlspecialchars($formValues['text']);
            $row_fb->fb_date = date("Y-m-d H:i:s");

            //Ищем владельца
            $row_u = $tbl_u->find($formValues['u_id'])->current();

            //Письмо
            $email_subject = 'Обратная связь личного кабинета';
            $email_body = '
            <h2>Обратная связь</h2>
            <h3>Владелец</h3>
            <table border="1" cellpadding="7" style="border-collapse: collapse;">
                <thead>
                    <tr style="text-align: left;">
                        <th>Имя</th>
                        <th>Основной телефон</th>
                        <th>E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.$row_u->u_firstname.'</td>
                        <td>'.Util::getHumanTel($row_u->u_phone_1).'</td>
                        <td>'.$row_fb->fb_email.'</td>
                    </tr>
                </tbody>
            </table>
            <h3>Дата отправки</h3>
            <p>'.Util::getHumanDate($row_fb->fb_date, "-", true, true).'</p>
            <h3>Обращение</h3>
            <p>'.$row_fb->fb_text.'</p>';

            try {
                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHTML($email_body);
                $mail->setFrom('info@goyug.com', 'GoYug.com');
                $mail->setReplyTo('info@goyug.com', 'GoYug.com');
                $mail->addTo('info@goyug.com', '');
                $mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
                // $mail->addBcc(array('marselos@gmail.com'));
                $mail->setSubject($email_subject);
                $mail->send();
            } catch(Exception $ex) {

            }

            //Сохранение в базу
            $row_fb->save();

            //Ошибка
            if(!$row_fb){
                $response['status'] = false;
                $response['class'] = 'text';
                $response['reason'] = 'Ошибка отправки - попробуйте позже';
            }

            //Добавляем E-mail ко владельцу, если он не был указан
            if(!$row_u->u_email){
                $row_u->u_email = $formValues['u_email'];
                $row_u->save();
            }
        }

        return $response;
    }

    ##########################################################################################
    public function isEmpty($params) {

        //Ответ метода
        $response = Array();
        $response['status'] = true;

        //Защита
        $obj_id = intval($params['obj_id']);

        //Срок выделения в днях
        $days = 3;

        //Выделено до
        $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

        //Объект
        $obj = $this->object->find($obj_id)->current();

        //Обновление объекта
        $obj->obj_isempty = !$obj->obj_isempty;
        $obj->obj_isempty_deactivate = $markedTo;
        $obj->save();

        //Маркер
        $response['isempty'] = $obj->obj_isempty ? true : false;
        $response['uid'] = $obj->obj_uid;
        $response['isempty_deactivate'] = 'Дата деактивации статуса:<br /> '.Util::getHumanDate($markedTo, "-", true, true).'.';
        $response['deactivate'] = Util::getHumanDate($markedTo, "-", true, true);

        return $response;
    }

    ##########################################################################################
    public function objEnable($params) {

        //Ответ метода
        $response = Array();
        $response['status'] = true;

        //Защита
        $obj_id = intval($params['obj_id']);

        //Объект
        $obj = $this->object->find($obj_id)->current();

        //Обновление объекта
        $obj->obj_enable = 1;
        $obj->save();

        return $response;
    }

    ##########################################################################################
    public function objDisable($params) {

        //Ответ метода
        $response = Array();
        $response['status'] = true;

        //Защита
        $obj_id = intval($params['obj_id']);

        //Объект
        $obj = $this->object->find($obj_id)->current();

        //Обновление объекта
        $obj->obj_enable = 0;
        $obj->save();

        return $response;
    }

    ##########################################################################################
    public function updateSelected($tables = array(), $values = array()) {

        $item_id = $values['obj_id'];

        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));

                if (isset($values[$table]) && count($values[$table])) {
                    foreach ($values[$table] as $id => $key) {
                        $row = $tbl->createRow();
                        if ($row) {

                            $row->v_table = $table;
                            $row->obj_id = $item_id;
                            $row->v_key = $key;
                            $row->save();
                        } else {
                            throw new Zend_Exception("Could not create prv_prm_values row!");
                        }
                    }
                    //foreach - $values
                }
                // if - $values
            }
            //foreach - $tables
        }
        //if - $tables
    }

    ##########################################################################################
    public function updateParameters($tables = array() , $values = array()) {

        $item_id = $values['obj_id'];

        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));

                foreach (array_keys($values) as $key) {
                    if (preg_match('/^' . $table . '_\d+/', $key, $matches)) {
                        $v_key = str_replace($table . '_', "", $key);

                        if (!empty($values[$key])) {
                            $row = $tbl->createRow();
                            if ($row) {

                                $row->v_table = $table;
                                $row->obj_id = $item_id;
                                $row->v_key = $v_key;
                                $row->v_val = $values[$key];
                                $row->save();
                            } else {
                                throw new Zend_Exception("Could not create prv_prm_values row!");
                            }
                        }
                        //if
                    }
                    //if - preg_match
                }
                //foreach
            }
            //foreach - $tables
        }
        //if - $tables
    }

    ##########################################################################################
    public function updateObjtypesStat($ot_objtypes) {

        $tbl = new Zend_Db_Table(array('name' => 'ot_object_type', 'primary' => 'ot_id'));

        if (count($ot_objtypes)) {

            foreach ($ot_objtypes as $id => $key) {

                $ot_id = $key;
                $ot_price_range = $this->getObjtypePriceRange($ot_id);
                $row = $tbl->find($ot_id)->current();

                if ($row) {
                    $row->ot_minprice = $ot_price_range["minprice"];
                    $row->ot_maxprice = $ot_price_range["maxprice"];
                }
                $row->save();
            }
        }
    }

    ##########################################################################################
    public function getObjtypePriceRange($ot_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ot_id) && $ot_id > 0) {

            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice
                FROM v_value AS v
                LEFT JOIN obj_object AS obj
                ON v.obj_id = obj.obj_id
                WHERE v.v_table = 'ot_object_type'
                AND v.v_key = " . $ot_id ."
                "
            )->fetchAll();
        }

        return $data[0];
    }

    ##########################################################################################
    public function updateCityStat($obj_addr_city_uid, $obj_addr_city = "", $obj_price = 0, $obj_pos = array()) {

        $tbl = new Zend_Db_Table(array('name' => 'ct_city', 'primary' => 'ct_id'));
        $venue_path = $_SERVER['DOCUMENT_ROOT'] . '/data/venue/';

        if(!empty($obj_addr_city_uid)) {

            $row = $tbl->fetchRow($tbl->select()->where('ct_uid = ?', $obj_addr_city_uid));

            // If City exist
            if ($row) {

                $ct_price_range = $this->getCityPriceRange($obj_addr_city_uid);
                $ct_objects = $this->getCityObjCount($obj_addr_city_uid);

                $row->ct_minprice = $ct_price_range["minprice"];
                $row->ct_maxprice = $ct_price_range["maxprice"];
                $row->ct_objects = $ct_objects;

                if($ct_objects > 0)
                    $row->save();
                //else
                    //$row->delete();

            // If City NOT exist
            } else {

                $newRow = $tbl->createRow();
                $newRow->ct_pid = 0;
                $newRow->ct_enable = 1;
                $newRow->ct_uid = $obj_addr_city_uid;
                $newRow->ct_name_ru = $obj_addr_city;
                $newRow->ct_name_ru_pr = Util::wordInflection($obj_addr_city);
                $newRow->ct_name_en = $obj_addr_city_uid;

                $newRow->ct_lat = $obj_pos['lat'];
                $newRow->ct_lng = $obj_pos['lng'];

                $newRow->ct_minprice = $obj_price;
                $newRow->ct_maxprice = $obj_price;

                $newRow->ct_objects = 1;

                $newRow->ct_create = date('Y-m-d H:i:s');
                $newRow->ct_update = date('Y-m-d H:i:s');

                $newRow->ct_creater = $this->auth->getIdentity()->u_username;
                $newRow->ct_updater = $this->auth->getIdentity()->u_username;

                $newRow->save();
            }

            // Update Cities JSON file
            $this->rebuildGeoJson($obj_addr_city_uid);


        }
    }

    //#########################################################################################
    public function rebuildGeoJson($city_uid) {

        $db = Zend_Registry::get('db');

        $venue_path = $_SERVER['DOCUMENT_ROOT'] . '/data/venue/';

        $response = "";

        // Select All Objects
        $obj_query = $this->db->query("
                SELECT *
                FROM obj_object AS obj

                LEFT JOIN per_period AS per
                ON per.per_id = obj.obj_price_period

                LEFT JOIN v_value AS v
                ON v.obj_id = obj.obj_id

                LEFT JOIN ot_object_type AS ot
                ON ot.ot_id = v.v_key

                LEFT JOIN acc_accommodation AS acc
                ON acc.acc_id = obj.obj_acc_type

                WHERE obj.obj_enable = 1
                AND v.v_table = 'ot_object_type'
                AND obj.obj_enable = 1
                AND obj.obj_addr_city_uid = '". $city_uid . "'

                ORDER BY obj_price
            ");

        $obj_query->setFetchMode(Zend_Db::FETCH_OBJ);
        $objects = $obj_query->fetchAll();

        $geojson_arr = array();
        $venues_arr = array();

        if (count($objects) > 0) {
          foreach ($objects as $id => $val) {

            $obj_id = $val->obj_id;
            $images = $this->getObjectImagesForGeojson($obj_id);

            $pin_style = "regular";

            $venues_arr[] = array(
              "type" => "Feature",
              "geometry" => array(
                  "type" => "Point",
                  "coordinates" => array(round($val->obj_addr_lon, 6), round($val->obj_addr_lat, 6))),
                  "properties" => array(
                    "index" => "",
                    "id" => $val->obj_id,
                    "uid" => $val->obj_uid,
                    "title" => $val->obj_name_ru,
                    "price" => $val->obj_price,
                    "rooms" => $val->obj_rooms,
                    "guests" => $val->obj_guests,
                    "sqr" => $val->obj_sqr,
                    "rating" => $val->obj_rating,
                    "checkin" => $val->obj_checkin,
                    "checkout" => $val->obj_checkout,
                    "addr_street" => $val->obj_addr_street,
                    "addr_number" => $val->obj_addr_number,
                    "addr_floor" => ($val->obj_addr_floor > 0) ? $val->obj_addr_floor : "",
                    "acc_name" => $val->acc_name_ru,
                    "minstay" => $val->obj_price_minstay,
                    "ot_name" => $val->ot_name_ru,
                    "sleepers" => $val->obj_sleepers,
                    "images" => $images,
                    "icon" => array("className" => array("marker-pin pin-" . $pin_style),
                    ) //properties
                ) //geometry
            );

          }
           // foreach

        }
         // if

          $geojson_arr = array("type" => "FeatureCollection", "features" => $venues_arr,);

          $geojson = json_encode($geojson_arr);
          $geojson_file = fopen($venue_path . $city_uid . ".geojson", "w");
          fwrite($geojson_file, $geojson);
          fclose($geojson_file);
    }

    //#########################################################################################
    public function getObjectImagesForGeojson($object_id = 0) {
        $rows = array();
        $res = array();

        if ($object_id > 0) {

          // Select All Objects
          $query = $this->db->query("
                    SELECT f_name
                    FROM f_files
                    WHERE f_ucid = 'object'
                    AND f_uid = '" . $object_id . "'
                    ORDER BY f_order
                ");

          $query->setFetchMode(Zend_Db::FETCH_OBJ);
          $rows = $query->fetchAll();

          foreach ($rows as $id => $val) {
            $res[] = $val->f_name;
          }
        }

        return $res;
    }

    ##########################################################################################
    public function getCityPriceRange($ct_uid) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ct_uid)) {

            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice
                FROM obj_object AS obj
                WHERE obj.obj_addr_city_uid = '" . $ct_uid . "'
                "
            )->fetchAll();

            return $data[0];

        } else {
            return;
        }
    }

    ##########################################################################################
    public function getCityObjCount($ct_uid) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ct_uid)){

            $data = $db->query("
                SELECT COUNT(obj.obj_id) AS obj_count
                FROM obj_object AS obj
                WHERE obj.obj_addr_city_uid = '" . $ct_uid ."'
                AND obj.obj_enable = 1"
            )->fetchAll();

            return $data[0]["obj_count"];
        } else {
            return;
        }
    }

    ##########################################################################################
    public function checkValidLogin($username, $password) {

        if(!empty($username) && !empty($password)){

            $if_phone = Util::stripTel($username);

            //Если телефон имеет верную длину
            if(mb_strlen($if_phone, 'utf-8') == 11){
                $or_phone = "
                OR u.u_phone_1 = '" . $if_phone ."'
                OR u.u_phone_2 = '" . $if_phone ."'";
            } else {
                $or_phone = '';
            }

            $data = $this->db->query("
                SELECT u.u_id, u.u_username, u.u_firstname, u.u_lastname, u.u_password, u.u_money, u.u_email, u.u_phone_1, u.u_phone_2, u.u_phone_3, u.u_role, u.u_tutorial, u.u_next_email_task, u.u_action_type, u.u_action_enable, u.u_action_enabled, u.u_action_actual_to, u.u_action_expired, u.u_action_activate, u.u_action_deactivate
                FROM u_user AS u
                WHERE u.u_username = '" . $username ." '
                OR u.u_email = '" . $username ." '
                $or_phone
                AND u.u_active = 1"
            )->fetchObject();

            //Проверка пароля
            if(md5($password) == $data->u_password){

                //Последний вход
                $this->db->query("
                    UPDATE u_user
                    SET u_lastlogin = NOW()
                    WHERE u_id = '". $data->u_id ."'
                ");

                //Убитие фантома
                $this->db->query("
                    UPDATE obj_object
                    SET obj_phantom = 0
                    WHERE obj_u_id = '". $data->u_id ."'
                ");

                //Снятие меток пробуждения по E-mail
                if(in_array($data->u_next_email_task, Array("awake2", "awake3", "objects_disabled", "awake_fail"))){
                    $this->db->query("
                        UPDATE u_user
                        SET u_next_email_task = 'awake_success', u_next_email_date = NOW()
                        WHERE u_id = '". $data->u_id ."'
                    ");
                    $data->u_next_email_task = 'awake_success';
                }

                //Снятие меток пробуждения по СМС
                if(in_array($data->u_next_email_task, Array("awake_fail_sms"))){
                    $this->db->query("
                        UPDATE u_user
                        SET u_next_email_task = 'awake_success_sms', u_next_email_date = NOW()
                        WHERE u_id = '". $data->u_id ."'
                    ");
                    $data->u_next_email_task = 'awake_success_sms';
                }

                //Снятие меток активации
                if(in_array($data->u_next_email_task, Array("activate_awake1", "activate_awake2", "activate_awake3", "activate_awake_fail"))){
                    $this->db->query("
                        UPDATE u_user
                        SET u_next_email_task = 'activate_awake_success', u_next_email_date = NOW()
                        WHERE u_id = '". $data->u_id ."'
                    ");
                    $data->u_next_email_task = 'activate_awake_success';
                }

                ///////////////////
                //Активация акции//
                ///////////////////

                    //Пользователь - акционник
                    if($data->u_action_enable){

                        //Срок активации еще не вышел
                        if($data->u_action_actual_to >= date("Y-m-d H:i:s")){

                            $data->u_action_activate = date("Y-m-d H:i:s");
                            $data->u_action_deactivate = date("2016-05-26 23:59:59");
                                if($data->u_action_type == 'action_rub_1000_nnov_1' || $data->u_action_type == 'action_rub_1000_nnov_2')
                                    $data->u_action_deactivate = date("2016-06-02 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_nnov')
                                    $data->u_action_deactivate = date("2016-06-02 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_spb')
                                    $data->u_action_deactivate = date("2016-06-09 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_ekb')
                                    $data->u_action_deactivate = date("2016-06-16 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_nsk')
                                    $data->u_action_deactivate = date("2016-06-22 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_tmn')
                                    $data->u_action_deactivate = date("2016-06-30 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_krd')
                                    $data->u_action_deactivate = date("2016-07-06 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_kzn')
                                    $data->u_action_deactivate = date("2016-07-15 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_sch')
                                    $data->u_action_deactivate = date("2016-07-21 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_svst')
                                    $data->u_action_deactivate = date("2016-07-28 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_rnd')
                                    $data->u_action_deactivate = date("2016-08-04 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_vrn')
                                    $data->u_action_deactivate = date("2016-08-10 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_krsk')
                                    $data->u_action_deactivate = date("2016-08-18 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_msk')
                                    $data->u_action_deactivate = date("2016-08-25 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_irk')
                                    $data->u_action_deactivate = date("2016-09-01 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_chl')
                                    $data->u_action_deactivate = date("2016-09-09 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_prm')
                                    $data->u_action_deactivate = date("2016-09-14 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_srt')
                                    $data->u_action_deactivate = date("2016-09-22 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_ksl')
                                    $data->u_action_deactivate = date("2016-09-29 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_smr')
                                    $data->u_action_deactivate = date("2016-10-05 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_vlg')
                                    $data->u_action_deactivate = date("2016-10-12 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_yrs')
                                    $data->u_action_deactivate = date("2016-10-20 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_stv')
                                    $data->u_action_deactivate = date("2016-10-26 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_srg')
                                    $data->u_action_deactivate = date("2016-11-02 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_khb')
                                    $data->u_action_deactivate = date("2016-11-11 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_ast')
                                    $data->u_action_deactivate = date("2016-11-17 23:59:59");

                            $data->u_action_enabled = 1;
                            $data->u_money += 1000;

                            $query = "
                                UPDATE u_user
                                SET
                                    u_money = ".$data->u_money.",
                                    u_action_enable = 0,
                                    u_action_enabled = 1,
                                    u_action_activate = '".$data->u_action_activate."',
                                    u_action_deactivate = '".$data->u_action_deactivate."'
                                WHERE u_id = '". $data->u_id ."'";
                                //echo $query;
                            $this->db->query($query);
                        }
                        //Срок активации вышел
                        else {
                            $query = "
                                UPDATE u_user
                                SET
                                    u_action_enable = 0,
                                    u_action_enabled = 0,
                                    u_action_expired = 1,
                                    u_action_expire = '".date("Y-m-d H:i:s")."'
                                WHERE u_id = '". $data->u_id ."'";
                                //echo $query;
                            $this->db->query($query);
                            $data->u_action_enabled = 0;
                        }
                    }

                ///////////////////////////////
                //Активация акции - окончание//
                ///////////////////////////////

                if (isset($_SESSION['authtoken_expired'])) {
                    unset($_SESSION['authtoken_expired']);
                }

                return $data;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    ##########################################################################################
    public function authToken($token) {

        if(!empty($token)){

            $data = $this->db->query("
                SELECT u.u_id, u.u_username, u.u_firstname, u.u_lastname, u.u_password, u.u_money, u.u_email, u.u_phone_1, u.u_phone_2, u.u_phone_3, u.u_role, u.u_tutorial, u.u_next_email_task, u.u_action_type, u.u_action_enable, u.u_action_enabled, u.u_action_actual_to, u.u_action_expired, u.u_action_activate, u.u_action_deactivate
                FROM u_user AS u
                WHERE u.u_authtoken = '" . $token ." '
                LIMIT 1"
            )->fetchObject();

            if($data){

                //Владелец
                $user = $this->find($data->u_id)->current();

                //Стираем токен
                $user->u_authtoken = '';

                //Последний логин
                $user->u_lastlogin = date("Y-m-d H:i:s");

                //Убитие фантома
                $this->db->query("
                    UPDATE obj_object
                    SET obj_phantom = 0
                    WHERE obj_u_id = '". $data->u_id ."'
                ");

                //Снятие меток пробуждения по E-mail
                if(in_array($user->u_next_email_task, Array("awake2", "awake3", "objects_disabled", "awake_fail"))){
                    $user->u_next_email_task = 'awake_success';
                    $user->u_next_email_date = date("Y-m-d H:i:s");
                    $data->u_next_email_task = 'awake_success';
                }

                //Снятие меток пробуждения по СМС
                if(in_array($user->u_next_email_task, Array("awake_fail_sms"))){
                    $user->u_next_email_task = 'awake_success_sms';
                    $user->u_next_email_date = date("Y-m-d H:i:s");
                    $data->u_next_email_task = 'awake_success_sms';
                }

                //Снятие меток активации
                if(in_array($user->u_next_email_task, Array("activate_awake1", "activate_awake2", "activate_awake3", "activate_awake_fail"))){
                    $user->u_next_email_task = 'activate_awake_success';
                    $user->u_next_email_date = date("Y-m-d H:i:s");
                    $data->u_next_email_task = 'activate_awake_success';
                }

                ///////////////////
                //Активация акции//
                ///////////////////

                    //Пользователь - акционник
                    if($data->u_action_enable){

                        //Срок активации еще не вышел
                        if($data->u_action_actual_to >= date("Y-m-d H:i:s")){

                            $user->u_action_activate = date("Y-m-d H:i:s");
                            $user->u_action_deactivate = date("2016-05-26 23:59:59");
                                if($user->u_action_type == 'action_rub_1000_nnov_1' || $user->u_action_type == 'action_rub_1000_nnov_2')
                                    $user->u_action_deactivate = date("2016-06-02 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_nnov')
                                    $user->u_action_deactivate = date("2016-06-02 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_spb')
                                    $user->u_action_deactivate = date("2016-06-09 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_ekb')
                                    $user->u_action_deactivate = date("2016-06-16 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_nsk')
                                    $user->u_action_deactivate = date("2016-06-22 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_tmn')
                                    $user->u_action_deactivate = date("2016-06-30 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_krd')
                                    $user->u_action_deactivate = date("2016-07-06 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_kzn')
                                    $user->u_action_deactivate = date("2016-07-15 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_sch')
                                    $user->u_action_deactivate = date("2016-07-21 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_svst')
                                    $user->u_action_deactivate = date("2016-07-28 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_rnd')
                                    $user->u_action_deactivate = date("2016-08-04 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_vrn')
                                    $user->u_action_deactivate = date("2016-08-10 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_krsk')
                                    $user->u_action_deactivate = date("2016-08-18 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_msk')
                                    $user->u_action_deactivate = date("2016-08-25 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_irk')
                                    $user->u_action_deactivate = date("2016-09-01 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_chl')
                                    $user->u_action_deactivate = date("2016-09-09 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_prm')
                                    $user->u_action_deactivate = date("2016-09-14 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_srt')
                                    $user->u_action_deactivate = date("2016-09-22 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_ksl')
                                    $user->u_action_deactivate = date("2016-09-29 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_smr')
                                    $user->u_action_deactivate = date("2016-10-05 23:59:59");
                                else if($user->u_action_type == 'action_rub_1000_vlg')
                                    $user->u_action_deactivate = date("2016-10-12 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_yrs')
                                    $data->u_action_deactivate = date("2016-10-20 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_stv')
                                    $data->u_action_deactivate = date("2016-10-26 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_srg')
                                    $data->u_action_deactivate = date("2016-11-02 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_khb')
                                    $data->u_action_deactivate = date("2016-11-11 23:59:59");
                                else if($data->u_action_type == 'action_rub_1000_ast')
                                    $data->u_action_deactivate = date("2016-11-17 23:59:59");

                            $user->u_action_enable = 0;
                            $user->u_action_enabled = 1;
                            $user->u_money += 1000;

                            $data->u_action_enabled = 1;
                            $data->u_money += 1000;
                        }
                        //Срок активации вышел
                        else {

                            $user->u_action_expire = date("Y-m-d H:i:s");
                            $user->u_action_enable = 0;
                            $user->u_action_enabled = 0;
                            $user->u_action_expired = 1;

                            $data->u_action_enabled = 0;
                        }
                    }

                ///////////////////////////////
                //Активация акции - окончание//
                ///////////////////////////////

                $user->save();

                //Письмо
                /*
                $email = Array();
                $email['to'] = $user->u_email;
                $email['subject'] = 'GoYug.com | Восстановление доступа';
                $email['body'] = '
                <h2>Добрый день!</h2>
                <h3>Вы запросили ссылку для входа в личный кабинет</h3>
                <p>После входа поменяйте пароль на новый в раделе «Профиль».</p>
                <p>Внимание! Ссылка одноразовая, повторно зайти в свой личный кабинет не получится.</p>
                <p>Для входа в ваш личный кабинет перейдите по ссылке: <a href="http://'.$_SERVER['HTTP_HOST'].'/user/authtoken/token/'.$user->u_authtoken.'">http://'.$_SERVER['HTTP_HOST'].'/user/authtoken/token/'.$user->u_authtoken.'</a></p>';

                //Отправляем письмо
                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHTML($email['body']);
                $mail->setFrom('info@goyug.com', 'GoYug.com');
                $mail->setReplyTo('info@goyug.com', 'GoYug.com');
                $mail->addTo($email['to'], '');
                //$mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
                $mail->setSubject($email['subject']);
                $mail->send();
                */

                return $data;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    ##########################################################################################
    public function subscribe($direction, $mode, $token) {

        //Ответ метода
        $response  = Array();
        $response['status'] = false;
        $response['mode'] = $mode;
        $response['direction'] = $direction;
        $response['reason'] = "Неизвестная ошибка, попробуйте произвести операцию позднее.";

        //Проверка на верность параметров
        if( !in_array($direction, Array("subscribe", "unsubscribe")) ||
            !in_array($mode, Array("news", "email", "sms")) ||
            !$token ) {
            $response['reason'] = "Указаны не все параметры.";
            return $response;
        }

        //Находим пользователя по токену
        $user = $this->db->query("
            SELECT u.u_id, u.u_subscribe_news, u.u_subscribe_email, u.u_subscribe_sms
            FROM u_user AS u
            WHERE u.u_subscribe_token = '" . $token ." '
            LIMIT 1"
        )->fetchObject();

        //Пользователь найден
        if($user){

            //Маркер
            $marker = 0;
            if($direction == 'subscribe') $marker = 1;

            //Опция
            $field = 'u_subscribe_'.$mode;

            //Обновляем данные по подписке
            $query = "UPDATE u_user SET `".$field."` = '".$marker."' WHERE u_id = ".$user->u_id;
            $result = $this->db->query($query);

            if($result){

                $response['status'] = true;
                $response['reason'] = "Изменения вступили в силу.";

            } else {
                $response['reason'] = "Ошибка при обновлении данных. Пожалуйста, обратитесь в службу поддержки из вашего личного кабинета или по адресу: info@goyug.com";
            }

        }
        //Пользователь не найден
        else {
            $response['reason'] = "Токен не найден в базе. Пожалуйста, обратитесь в службу поддержки из вашего личного кабинета или по адресу: info@goyug.com";
        }

        return $response;
    }

    ##########################################################################################
    public function userAwakeOK($params) {

        //Обновляем пользователя
        $row = $this->find($this->auth->getIdentity()->u_id)->current();
        $row->u_next_email_task = 'awake_end';
        $row->u_next_email_date = date("Y-m-d H:i:s");
        $row->save();

        //Меняем статус в айдентити
        $this->auth->getIdentity()->u_next_email_task = 'awake_end';

        //Ответ
        $response = Array("status" => true);

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'userawakeok'), array('u_next_email_task' => 'awake_end'));

        return $response;
    }

    ##########################################################################################
    public function userHideNotification($params) {

        //Ответ
        $response = Array("status" => true);

        $_SESSION['shownotification']['rental'] = false;

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'userhidenotification'), array());

        return $response;
    }

    ##########################################################################################
    public function checkPhoneExist($phone) {

        if(!empty($phone)){

            $if_phone = Util::stripTel($phone);

            //Исключение текущего пользователя
            if($this->auth->getIdentity() && $this->auth->getIdentity()->u_id) $user_disable = " AND u.u_id != '".$this->auth->getIdentity()->u_id."' ";
            else                                                               $user_disable = "";

            //Запрос
            $query = "SELECT *
                      FROM u_user AS u
                      WHERE (u.u_phone_1 = '" . $if_phone ."'
                      OR u.u_phone_2 = '" . $if_phone ."'
                      OR u.u_phone_3 = '" . $if_phone ."')
                      AND u.u_active = 1
                      AND u.u_role = 'owner'
                      $user_disable";

            //echo $query;

            $data = $this->db->query($query)->fetchObject();

            return $data;

        } else {
            return false;
        }
    }

    ##########################################################################################
    public function checkEmailExist($email) {

        if(!empty($email)){

            $email = Util::strtolower_utf8(trim($email));

            $data = $this->db->query("
                SELECT *
                FROM u_user AS u
                WHERE u.u_email = '" . $email ."'
                AND u.u_active = 1
                AND u.u_role = 'owner'"
            )->fetchObject();

            return $data;

        } else {
            return false;
        }
    }

    ##########################################################################################
    public function checkEmailValid($email) {

        if(!empty($email)){

            $email = Util::strtolower_utf8(trim($email));

            return preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $email);

        } else {
            return false;
        }
    }

    ##########################################################################################
    public function createUser($name, $email, $phone, $password, $role) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'u_user',
            'primary' => 'u_id'
        ));

        if(!empty($name) && !empty($email) && !empty($phone) && !empty($password) && !empty($role)) {

            $email = Util::strtolower_utf8(trim($email));
            $phone = Util::stripTel($phone);

            $row = $tbl->createRow();
            $row->u_lf_id = 1;
            $row->u_active = 1;
            $row->u_tutorial = 0;
            $row->u_username = $phone;
            $row->u_password = md5($password);

            $row->u_money = 0;
            $row->u_authtoken = "";

            $row->u_subscribe_news = 1;
            $row->u_subscribe_email = 1;
            $row->u_subscribe_sms = 1;

            $row->u_firstname = $name;

            $row->u_email = $email;
            $row->u_phone_1 = $phone;
            $row->u_role = $role;
            $row->u_is_phoned = 0;

            $row->u_isnew = 1;

            $row->u_create = date('Y-m-d H:i:s');
            $row->u_update = date('Y-m-d H:i:s');

            $row->u_creater = $email;
            $row->u_updater = $email;

            $row->u_lastlogin = date('Y-m-d H:i:s');

            $row->save();

            $data = $this->db->query("
                SELECT u.u_id, u.u_username, u.u_firstname, u.u_lastname, u.u_email, u.u_phone_1, u.u_phone_2, u.u_phone_3, u.u_role, u.u_tutorial, u.u_next_email_task
                FROM u_user AS u
                WHERE u.u_email = '" . $email ."'
                AND u.u_active = 1"
            )->fetchObject();

            return $data;
        }
    }

    ##########################################################################################
    public function loadObjectCal($u_id, $obj_id) {

        $db = Zend_Registry::get( 'db' );
        $data = "";

        if(!empty($obj_id) && $obj_id > 0)
        {
            $data = $db->query("
                SELECT *, DATEDIFF(cal.cal_enddate, cal.cal_startdate) AS cal_days
                FROM cal_calendar AS cal
                LEFT JOIN obj_object AS obj
                ON cal.obj_id = obj.obj_id
                WHERE cal.obj_id = " . $obj_id ."
                AND obj.obj_u_id = " . $u_id ."
                AND cal.cal_enable = 1
                ORDER BY cal_startdate DESC"
            )->fetchAll();
        }

        // return $u_id;
        return $data;
    }

    ##########################################################################################
    public function saveObjectCal($u_id, $obj_id, $events_json) {

        $tbl = new Zend_Db_Table(array('name' => 'cal_calendar', 'primary' => 'cal_id'));
        $auth = Zend_Auth::getInstance();

        $data = "";
        $events_json = json_decode($events_json);

        $tbl->delete(array(
            'obj_id = ?' => $obj_id,
        ));

        if(count($events_json))
            {
                foreach($events_json as $jid => $event)
                {

                    if(isset($event->type) && $event->type == "event")
                    {

                        $row = $tbl->fetchRow(
                            $tbl->select()
                                ->where('obj_id = ?', $obj_id)
                                ->where('cal_startdate = ?', date("Y-m-d H:i", strtotime($event->start)))
                        );

                        if(!$row)
                        {

                            $row = $tbl->createRow();
                            if ($row)
                            {
                                $row->cal_enable = 1;
                                $row->obj_id = $obj_id;
                                $row->cal_startdate = date("Y-m-d H:i", strtotime($event->start));
                                $row->cal_enddate = date("Y-m-d H:i", strtotime($event->end));
                                $row->cal_msec = strtotime($event->start);
                                $row->cal_status = $event->status;
                                $row->cal_title = $event->title;
                                $row->cal_owner = ($auth->hasIdentity()) ? $auth->getIdentity()->u_username : "";
                                $row->save();
                            } else {
                                throw new Zend_Exception("Could not create cal_calendar row!");
                            }
                        }
                    }
                }
            }

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'addcalevent'), array('obj_id' => $obj_id));

        return $data;
    }

    ##########################################################################################
    public function saveUserEmail($params) {

        //Все E-mail'ы в базе, кроме текущего юзера
        $result = $this->db->query("
            SELECT u_email
            FROM u_user
            WHERE u_id != " . $this->auth->getIdentity()->u_id ."
            ORDER BY u_id"
        )->fetchAll();

        //Формируем массив для последующей проверки на вхождение
        $emails = Array();
        foreach ($result as $value) {
            $emails[] = $value['u_email'];
        }

        //E-mail указан неверно
        if (!$params['email'] || !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $params['email'])) {

            //Ответ
            $response = Array("status" => false, "reason" => "E-mail введен неверно");
        }
        //E-mail уже занят
        else if (in_array($params['email'], $emails)) {

            //Ответ
            $response = Array("status" => false, "reason" => "На такой E-mail уже зарегистрирован пользователь");
        }
        //E-mail указан верно
        else {

            //Обновляем пользователя
            $row = $this->find($this->auth->getIdentity()->u_id)->current();
            $row->u_email = $params['email'];
            $row->save();

            //Добавляем E-mail в айдентити
            $this->auth->getIdentity()->u_email = $params['email'];

            //Ответ
            $response = Array("status" => true);
        }

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'savedemail'), array('email' => $params['email']));

        return $response;
    }

    ##########################################################################################
    public function forgot($params) {

        $response = Array();
        $response['status'] = false;

        //E-mail указан неверно
        if (!$params['u_email'] || !preg_match("/^[._a-zA-Z0-9-]+@[.a-zA-Z0-9-]+\.[a-z]{2,6}$/", $params['u_email'])) {

            //Ответ
            $response = Array("status" => false, "reason" => "E-mail введен неверно");
        }
        //E-mail указан верно
        else {

            //Пользователь, связанный с E-mail
            $result = $this->db->query("
                SELECT u_id, u_email
                FROM u_user
                WHERE u_email = '" . $params['u_email'] ."'
                LIMIT 1"
            )->fetchAll();

            //Пользователь найден
            if(count($result)){

                $u_row = $result[0];

                //print_r($u_row);

                //Обновляем пользователя
                $user = $this->find($u_row['u_id'])->current();
                $user->u_authtoken = md5($user->u_email.$user->u_id.rand(1000, 9999));
                $user->save();

                //Ответ
                $response = Array("status" => true);

                //Письмо
                $email = Array();
                $email['to'] = $user->u_email;
                $email['subject'] = 'GoYug.com | Восстановление доступа';
                $email['body'] = '
                <h2>Добрый день!</h2>
                <h3>Вы запросили ссылку для входа в личный кабинет</h3>
                <p>После входа поменяйте пароль на новый в разделе «Профиль».</p>
                <p>Внимание! Ссылка одноразовая, повторно зайти в свой личный кабинет по ней не получится.</p>
                <p>Для входа в ваш личный кабинет перейдите по ссылке: <a href="http://'.$_SERVER['HTTP_HOST'].'/user/authtoken/token/'.$user->u_authtoken.'">http://'.$_SERVER['HTTP_HOST'].'/user/authtoken/token/'.$user->u_authtoken.'</a></p>';

                //Отправляем письмо
                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHTML($email['body']);
                $mail->setFrom('info@goyug.com', 'GoYug.com');
                $mail->setReplyTo('info@goyug.com', 'GoYug.com');
                $mail->addTo($email['to'], '');
                $mail->addBcc(array('marselos@gmail.com', '160961@mail.ru'));
                $mail->setSubject($email['subject']);
                $mail->send();

                //--------------- Logger--------------------
                $this->log->write( array('status' => 'success', 'result' => 'authtoken_forgot_send'));

                if (isset($_SESSION['authtoken_expired'])) {
                    unset($_SESSION['authtoken_expired']);
                }

            } else {

                //Ответ
                $response = Array("status" => false, "reason" => "Пользователь с таким E-mail не найден");

                //--------------- Logger--------------------
                $this->log->write( array('status' => 'success', 'result' => 'authtoken_forgot_baduser'));

            }
        }

        return $response;
    }

    ##########################################################################################
    public function markOutObjects($params) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'obj_object',
            'primary' => 'obj_id'
        ));

        //Массив выделенных
        $marked = json_decode($params['marked'], false);
        //print_r($marked);

        //Срок выделения в днях
        $days = 3;

        //Выделено до
        $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

        //Обновляем объекты
        foreach ($marked as $key => $obj_id) {

            $row = $tbl->find($obj_id)->current();
            $row->obj_mark_out = 1;
            $row->obj_mark_out_deactivate = $markedTo;
            $row->save();
        }//foreach

        //Ответ
        $response = Array("status" => true, "marked" => $marked, "markedTo" => Util::getHumanDate($markedTo, "-", true, true));

        //--------------- Logger--------------------
        $this->log->write( array('status' => 'success', 'result' => 'markedoutobjects'), array('email' => $marked));

        return $response;
    }

    ##########################################################################################
    public function getTotalViewsAmount($obj_id) {

        $items = $this->db->query("
            SELECT IFNULL(SUM(os_val), 0) AS amount
            FROM os_objstat
            WHERE os_action = 'view'
            AND os_obj_id = '".$obj_id."'
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getTodayViewsAmount($obj_id) {
        $items = $this->db->query("
            SELECT IFNULL(SUM(os_val), 0) AS amount
            FROM os_objstat
            WHERE os_action = 'view'
            AND DATE(os_date) = CURDATE()
            AND os_obj_id = '".$obj_id."'
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getWeekViewsAmount($obj_id, $shift = 0) {
        $items = $this->db->query("
            SELECT IFNULL(SUM(os_val), 0) AS amount
            FROM os_objstat
            WHERE os_action = 'view'
            AND WEEKOFYEAR(os_date) = WEEKOFYEAR(NOW()) - " . $shift . "
            AND os_obj_id = '".$obj_id."'
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getTotalViewsAmountAuto($obj_id) {

        $items = $this->db->query("
            SELECT IFNULL(SUM(osp_count), 0) AS amount
            FROM osp_objstat_permanent
            WHERE osp_obj_id = '".$obj_id."'
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getTodayViewsAmountAuto($obj_id) {
        $items = $this->db->query("
            SELECT IFNULL(SUM(osd_count), 0) AS amount
            FROM osd_objstat_daily
            WHERE
            DATE(osd_date) = CURDATE() AND
            osd_obj_id = '".$obj_id."'
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getFeedbacks() {

        //Письма
        $result = $this->db->query("
            SELECT *
            FROM fb_feedback
            WHERE fb_u_id = " . $this->auth->getIdentity()->u_id ."
            ORDER BY fb_date DESC"
        )->fetchAll();

        return $result;
    }

    ##########################################################################################
    public function deleteItems($item_ids) {

        $isError = false;

        if (count($item_ids) > 0) {

            foreach ($item_ids as $id => $item_id) {

                $row = $this->object->find($item_id)->current();

                if ($row) {

                    $obj_addr_city_uid = $row->obj_addr_city_uid;

                    //------------------------------- Удаляем данные из cal_calendar
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'cal_calendar',
                        'primary' => 'cal_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));

                    //------------------------------- Удаляем данные из v_value
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'v_value',
                        'primary' => 'v_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));

                    //------------------------------- Удаляем данные из f_files
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'f_files',
                        'primary' => 'f_id'
                    ));
                    $tbl->delete(array('f_uid = ?' => $item_id, 'f_ucid = ?' => "object"));

                    //------------------------------- Удаляем объект
                    $row->delete();

                    //------------------------------- Удаляем папку с фотками
                    $remove_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/object/' . $item_id;
                    Util::rmdir_recursive($remove_dir);

                    //------------------------------- Пересчитываем статистику городов
                    //$this->updateCityStat($obj_addr_city_uid);

                }
                else {
                    $isError = true;
                    throw new Zend_Exception("Could not delete item. Item not found!");
                }
            }
        }

        return !$isError;
    }
}
