<?php
/**
 * Class and Function List:
 * Function list:
 * - init()
 * - getSmartfiltersNavbar()
 * - getSmartFilters()
 * - getSmartFilterID()
 * - getSmartfilters()
 * - countObjectsInSmartfilter()
 * - getObjectsInSmartFilter()
 * - getSmartfilterTypes()
 * - getObjectTypes()
 * Classes list:
 * - Ui_Model_Index extends Zend_Db_Table_Abstract
 */
class Ui_Model_Index extends Zend_Db_Table_Abstract
{
    
    public $_name = 'sf_smartfilters';
    public $_primary = 'sf_id';
    
    //#########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');
        
        $this->lang = $locale->curlocale['lang'];
    }
    
    //#########################################################################################
    public function getSmartsNavbar()
    {
        $result = array();

        $db = Zend_Registry::get( 'db' );

        $data = $db->query("
            SELECT * 
            FROM sf_smartfilters AS sf
            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id
            WHERE sf.sf_enable=1
            ORDER BY ot.ot_order, sf.sf_order_navbar
        ")->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $name = ( !empty($d_val["sf_name_navbar_" . $this->lang]) ) ? $d_val["sf_name_navbar_" . $this->lang] : $d_val["sf_name_" . $this->lang]; 

            $result[ $d_val["ot_name_" . $this->lang] ][] = array(
                "uid" => $d_val["sf_uid"],
                "name" => $name,
                "objects" => $d_val["sf_objects"],
                "minprice" => $d_val["sf_minprice"],
                "maxprice" => $d_val["sf_maxprice"],
            );   
        }

        return $result;
    }

    //#########################################################################################
    public function getTotalObjectsAmount()
    {
        $items = $this->db->query("
            SELECT COUNT(*) AS amount
            FROM obj_object
            WHERE obj_enable = 1
        ")->fetchAll();

        return $items[0]["amount"];
    }


    //#########################################################################################
    public function getSmartsPanel()
    {
        $result = array();

        $db = Zend_Registry::get( 'db' );

        $data = $db->query('
            SELECT  sf.sf_id, sf.sf_uid, sf.sf_level, sf.sf_order, sf.sf_name_' . $this->lang . ', sf.sf_objects, 
                    sf.sf_minprice, sf.sf_maxprice, sf.per_id, ot.ot_order, ot.ot_name_ru, sft.sft_cssclass,
                    f.f_name, per.per_pricename_' . $this->lang . '

            FROM sf_smartfilters AS sf

            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id

            LEFT JOIN sft_smartfilters_type AS sft
            ON sf.sft_id = sft.sft_id

            LEFT JOIN f_files AS f
            ON sf.sf_id = f.f_uid

            LEFT JOIN per_period AS per
            ON sf.per_id = per.per_id

            WHERE sf.sf_enable=1
            AND f.f_ucid = "smartfilters"

            ORDER BY ot.ot_order, sf.sf_level, sf.sf_order
        ')->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $result[ $d_val["ot_name_" . $this->lang] ][ $d_val["sf_level"] ][ $d_val["sf_order"] ] = array(
                "uid" => $d_val["sf_uid"],
                "name" => $d_val["sf_name_" . $this->lang],
                "objects" => $d_val["sf_objects"],
                "minprice" => $d_val["sf_minprice"],
                "maxprice" => $d_val["sf_maxprice"],
                "cssclass" => $d_val["sft_cssclass"],
                "image" => "/upload/smartfilters/" . $d_val["sf_id"] ."/" . $d_val["f_name"],
                "period" => $d_val["per_pricename_" . $this->lang],
            );   
        }

        return $result;
    }

    //#########################################################################################
    public function getFakeCounter()
    {
        $tbl = new Zend_Db_Table(array(
            'name' => 'fc_fake_counter',
            'primary' => 'fc_id'
        ));

        $item = $tbl->fetchAll(
            $tbl->select()->from(array('fc_fake_counter') , array("fc_id", "fc_hour", "fc_val"))
        )->toArray();

        // $currDay = date("j");
        $currHour = date("G");
        $currHourDB = $item[0]["fc_hour"];
        $id = $item[0]["fc_id"];

        // $newCounterValue = (mt_rand(0, 1)) ? $item[0]["fc_val"] + mt_rand(0, 3) : $item[0]["fc_val"];
        $newCounterValue = $item[0]["fc_val"] + mt_rand(0, 3);

        $row = $tbl->find($id)->current();

        if($currHour == $currHourDB)
        {
            $row->setFromArray(array("fc_val" => $newCounterValue));
            $row->save();

            return $newCounterValue;

        } else {

            $row->setFromArray(array("fc_val" => 0, "fc_hour" => $currHour));
            $row->save();

            return 0;
        }

    }   

    //#########################################################################################
    public function getTopDestinations()
    {
        $result = array();

        $data = $this->db->query("
            SELECT * 
            FROM ct_city AS ct
            WHERE ct.ct_enable = 1
            AND ct.ct_top = 1
            ORDER BY ct.ct_order, ct.ct_name_ru
        ")->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $result[] = array(
                "uid" => $d_val["ct_uid"],
                "name" => $d_val["ct_name_ru"],
                "objects" => $d_val["ct_objects"],
                "minprice" => $d_val["ct_minprice"],
                "maxprice" => $d_val["ct_maxprice"],
            );   
        }

        return $result;

    }

    //#########################################################################################
    public function getCities()
    {
        $result = array();

        $data = $this->db->query("
            SELECT * 
            FROM ct_city AS ct
            WHERE ct.ct_enable = 1
            AND ct.ct_objects > 0
            ORDER BY ct.ct_name_ru
        ")->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $result["list"][ $d_val["ct_uid"] ] = array(
                "id" => $d_val["ct_id"],
                "uid" => $d_val["ct_uid"],
                "name" => $d_val["ct_name_ru"],
                "objects" => $d_val["ct_objects"],
                "minprice" => $d_val["ct_minprice"],
                "maxprice" => $d_val["ct_maxprice"],
            );   

            $result["alphabet"][ mb_substr($d_val["ct_name_ru"], 0, 1, 'utf-8') ][] = $d_val["ct_uid"];
        }

        ksort($result["alphabet"]);

        return $result;

    }



}
?>
