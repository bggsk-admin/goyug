<?php

class Ui_Model_Stat extends Zend_Db_Table_Abstract {

    public $_name = '';
    public $_primary = '';

    //#########################################################################################
    public function init() {

        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        // Города с АБОНПЛАТОЙ
        $this->rental_cities = array(
        "kislovodsk", 
        "nnovgorod", 
        "sochi", "sevastopol", "spb", 
        "krasnodar", "saratov", "tyumen", 
        "novosibirsk", "ekaterinburg", "perm", 
        "kazan", "ufa", "voronezh", 
        "moscow", "omsk", "rostovnadonu", "volgograd",
        "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
        "zheleznovodsk",
        "pyatigorsk", "yalta", "yaroslavl", "tomsk",
        "khabarovsk", "vladimir", "penza", "kirov", "vologda",
        "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
        "astrakhan", "tolyatty", "orel", "orenburg", "tver",
        "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
      );
      
    }

    //#########################################################################################
    //Кол-во заявок по городу
    public function getOrders($city_uid) {

        $db = Zend_Registry::get('db');

        $count = $db->query("
            SELECT IF(COUNT( * ) = 0, 2, COUNT( * ) * 2) as count
            FROM es_email_stat
            WHERE es_date_send LIKE '%".date("Y-m-d")."%'
        ")->fetch();

        return $count['count'];
    }

    //#########################################################################################
    //ТОП дня
    public function getOntop($city_uid) {

        $db = Zend_Registry::get('db');

        $rental_query = in_array($city_uid, $this->rental_cities) ? "AND u_rental = 1" : "";

        $count = $db->query("
            SELECT COUNT(*) as count
            FROM obj_object

            LEFT JOIN u_user AS u
            ON obj_u_id = u.u_id

            WHERE obj_addr_city_uid = '$city_uid' 
            AND obj_enable = 1
            " . $rental_query . "
            AND obj_ontop_mark = 1
        ")->fetch();

        return $count['count'];
    }

    //СВОБОДНА СЕГОДНЯ
    public function getIsempty($city_uid) {

        $db = Zend_Registry::get('db');

        $rental_query = in_array($city_uid, $this->rental_cities) ? "AND u_rental = 1" : "";

        $count = $db->query("
            SELECT COUNT(*) as count
            FROM obj_object

            LEFT JOIN u_user AS u
            ON obj_u_id = u.u_id

            WHERE obj_addr_city_uid = '$city_uid' 
            AND obj_enable = 1
            " . $rental_query . "
            AND obj_isempty = 1
        ")->fetch();

        return $count['count'];
    }

    //РАСПРОДАЖА
    public function getSale($city_uid) {

        $db = Zend_Registry::get('db');
        $rental_query = in_array($city_uid, $this->rental_cities) ? "AND u_rental = 1" : "";

        $count = $db->query("
            SELECT COUNT(*) as count
            FROM obj_object

            LEFT JOIN u_user AS u
            ON obj_u_id = u.u_id

            WHERE obj_addr_city_uid = '$city_uid' 
            AND obj_enable = 1
            " . $rental_query . "
            AND obj_sale = 1
        ")->fetch();

        return $count['count'];
    }

    //Регистрация рассылки
    public function registerEmailStatOpen($uid, $u_id) {

        //Проверяем, открывал ли пользователь уже письмо
        $query_isopened = "SELECT * FROM eso_email_stat_opens WHERE `eso_uid` = '$uid' AND (eso_open_u_ids LIKE '$u_id,%' OR eso_open_u_ids LIKE '%,$u_id' OR eso_open_u_ids LIKE '%,$u_id,%' OR eso_open_u_ids LIKE '$u_id')";
        //echo $query_isopened;
        $isopened = $this->db->query($query_isopened)->fetchAll();
        //print_r($isopened);
        //exit;

        //Если еще не открывал
        if(!count($isopened)){

            //Добавляем просмотр
            $query_newcount = "UPDATE `eso_email_stat_opens` SET `eso_open_count` = (eso_open_count + 1), `eso_open_u_ids` = TRIM(LEADING ',' FROM CONCAT(eso_open_u_ids, ',$u_id')), `eso_open_last_access` = '".date("Y-m-d H:i:s")."' WHERE `eso_uid` = '$uid'";
            echo $query_newcount;
            $this->db->query($query_newcount);
        } else {
            //echo 'already exists';
        }

    }

}
?>
