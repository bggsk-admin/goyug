<?php
require_once("Util.php");

class Ui_Model_Log extends Zend_Db_Table_Abstract {

    public $_name       = 'l_log';
    public $_primary    = 'l_id';

    //#########################################################################################
    public function init()
    {

    }

    //#########################################################################################
    public function write( $params = array(), $raw = array() )
    {
        if(!empty($params))
        {
            $row = $this->createRow();
            if($row)
            {
                foreach($params as $p_id => $p_val)
                {
                    $row->{"l_".$p_id} = $p_val;
                }

                list($usec, $sec) = explode(" ", microtime());

                $request = Zend_Controller_Front::getInstance()->getRequest();
                $auth = Zend_Auth::getInstance();

                $raw = json_encode($raw);

                $row->l_date = date ( 'Y-m-d H:i:s' );
                $row->l_msec = (float)$usec + (float)$sec;
                $row->l_ip = Util::getIp();
                $row->l_useragent = Util::getUserAgent();
                $row->l_url = $_SERVER["REQUEST_URI"];
                $row->l_module = MODULE_NAME;
                $row->l_action = $request->getActionName();
                $row->l_controller = $request->getControllerName();
                $row->l_username = ($auth->hasIdentity()) ? $auth->getIdentity()->u_username : "";
                $row->l_u_id = ($auth->hasIdentity()) ? $auth->getIdentity()->u_id : 0;
                $row->l_object = json_encode($request->getParams());
                $row->l_raw = $raw;

                $row->save();

                return $row;

            } else {
                throw new Zend_Exception("Could not create item!");
            }
        }

    }

    //#########################################################################################
    public function writeStat( $obj_id, $action ) {

        if(!empty($obj_id)) {

            $current_date = date("Y-m-d");

            //////////////////
            //Старая таблица//
            //////////////////
            $tbl = new Zend_Db_Table(array( 'name' => 'os_objstat', 'primary' => 'os_id'));

            $row = $tbl->createRow();
            if($row) {

                //Объект
                $modelObject = new Ui_Model_Objects();
                $obj = $modelObject->find($obj_id)->current();
                
                //Коэффициент для поднятых
                // if($obj->obj_ontop_mark == 1) $row->os_val = rand(2, 4);
                // else                          $row->os_val = 1;
                $row->os_val = 1;

                list($usec, $sec) = explode(" ", microtime());

                $row->os_action = $action;
                $row->os_obj_id = $obj_id;
                $row->os_date = date ( 'Y-m-d H:i:s' );
                $row->os_msec = (float)$usec + (float)$sec;
                $row->os_ip = Util::getIp();
                $row->os_useragent = Util::getUserAgent();

                $row->save();

            } else {
                throw new Zend_Exception("Could not create item!");
            }
            //////////////////

            /////////////////
            //Новая таблица//
            /////////////////
            $db = Zend_Registry::get( 'db' );

            #part 1
                $query = "SELECT * FROM osp_objstat_permanent WHERE osp_obj_id = ".$obj_id;
                $result = $db->query($query)->fetch();

                //Запись есть
                if($result){
                    $query = "UPDATE osp_objstat_permanent SET `osp_count` = (osp_count + 1) WHERE osp_obj_id = ".$obj_id;
                } else {
                    $query = "INSERT INTO osp_objstat_permanent (`osp_obj_id`, `osp_count`) VALUES ('$obj_id', 1)";
                }

                $record = $db->query($query);

            #part 2
                $query = "SELECT * FROM osd_objstat_daily WHERE osd_obj_id = ".$obj_id." AND osd_date = '".$current_date."'";
                $result = $db->query($query)->fetch();

                //Запись есть
                if($result){
                    $query = "UPDATE osd_objstat_daily SET `osd_count` = (osd_count + 1) WHERE osd_obj_id = ".$obj_id." AND osd_date = '".$current_date."'";
                } else {
                    $query = "INSERT INTO osd_objstat_daily (`osd_obj_id`, `osd_count`, `osd_date`) VALUES ('$obj_id', 1, '".$current_date."')";
                }

                $record = $db->query($query);

            /////////////////
        }
    }

}
?>
