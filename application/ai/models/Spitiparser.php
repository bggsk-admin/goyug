<?php
require_once("Util.php");
require_once("nokogiri.php");

class Ai_Model_Spitiparser extends Zend_Db_Table_Abstract {

    //#########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        $this->domain = "http://%%%.spiti.ru";
        $this->img_path = $_SERVER['DOCUMENT_ROOT'] . "/upload/spiti";
        $this->watermark_path = $_SERVER['DOCUMENT_ROOT'] . "/images/watermark-1.jpg";
    }

    //#########################################################################################
    public function countParseItems($pars_city = "")
    {
        $result = array();

        if(!empty($pars_city))
        {
            $url = preg_replace("/%%%/", $pars_city, $this->domain);

            $html = $this->get_data($url);

            $saw = new nokogiri($html);

            $node = $saw->get('.place__counter')->toArray();

            // print_r($node);die;

            if(isset($node[0]['#text'][0]))
            {
                $result['objects_num'] = intval(preg_replace("/[^0-9]/", "", $node[0]['#text'][0]));
            } else {
                $result['objects_num'] = 0;    
            }

            // print_r($result);die;

            $node = $saw->get('.page-item')->toArray();
            if(isset($node[sizeof($node) - 2]))
            {
                $result['pages_num'] = intval($node[sizeof($node) - 2]['#text'][0]);   
            } else {
                $result['pages_num'] = 0;        
            }

            // print_r($result);die;

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    public function countDownloadItems($pars_city = "")
    {
        $result = array();

        if(!empty($pars_city))
        {
            //Запрос
            $query = "
                SELECT pso_uid, pso_images
                FROM pso_spiti_object AS pso
                WHERE pso.pso_addr_city_uid = '" . $pars_city ."'
                AND pso_is_goyug = 0
            ";

            $data = $this->db->query($query)->fetchAll();

            $result['images'] = 0;
            if(count($data) > 0)
            {
                foreach($data as $id => $val)
                {
                    $result[$val['pso_uid']] = explode(";", $val['pso_images']);
                    $result['images'] = $result['images'] + count($result[$val['pso_uid']]);
                }
            }

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    public function download($pars_city = "", $items, $task_token)
    {
        set_time_limit(0);
        $result = array();

        if($pars_city != "" && count($items) > 0)
        {
            foreach($items as $id => $val)
            {
                if($id != "images")
                {
                    $path = $this->img_path . "/" . $pars_city . "/" . $id;
                    
                    if(!file_exists($path)) mkdir($path, 0777, true);

                    foreach($val as $img_id => $img)
                    {
                        $img_path = $path . "/" . $img_id . ".jpg";
                        if(!file_exists($img_path))
                        {
                            if(!empty($img) && !empty($img_path))
                            {
                                copy( $img , $img_path);

                                // Обновляем статус задачи
                                //  ============================================================
                                if(!empty($task_token))
                                {
                                    $query = "
                                        UPDATE pt_parse_tasks
                                        SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                        WHERE pt_token = '" . $task_token ."'
                                    ";
                                    $this->db->query($query);         
                                }

                            }

                        }
                    }

                }
            }
        }

        return $result;
    }


    //#########################################################################################
    public function parseObjList($pars_city = "", $pars_page = 1, $task_token = "")
    {
        $result = "";
        $url = "";

        // Составляем URL старнички
        $url = preg_replace("/%%%/", $pars_city, $this->domain . "/?p=" . $pars_page);

        // Получаем HTML и инициализиурем парсер
        $html = $this->get_data($url);
        $saw = new nokogiri($html);

        $item_links = $saw->get('.jsCardItem .jsPromoHref')->toArray();

        if(count($item_links) > 0)
        {
            foreach($item_links as $id => $val)
            {
                //  Парсим ссылку на карточку объекта
                $result[$id]['link'] = (isset($val['href'])) ? "http:" . $val['href'] : "";

                //  Парсим UID
                $result[$id]['uid'] = preg_replace("/[^0-9]/", "", $result[$id]['link']);

                //  Проверяем есть ли такой владелец в нашей базе
                // $result[$id]['avito_owner'] = $this->checkAvitoPhoneExist($result[$id]['phone']);

                // $result[$id]['goyug_owner'] = $this->checkGoyugPhoneExist($result[$id]['phone']);
                // $result[$id]['parsed_owner'] = $this->checkSpitiPhoneExist($result[$id]['phone']);

                // Проверка: парсился ли уже этот объект
                $result[$id]['parsed_object'] = $this->checkObjectExist($result[$id]['uid'], $pars_city);

            } // foreach

            // print_r($result); die;

            // Обновляем статус задачи
            if(!empty($task_token))
            {
                $query = "
                    UPDATE pt_parse_tasks
                    SET pt_cur_page = pt_cur_page +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                    WHERE pt_token = '" . $task_token ."'
                ";
                $this->db->query($query);         
            }

            return $result;
        
        } else {
            return 0;
        }
    }


    //#########################################################################################
    public function parseObjDetails($objects = array(), $city_uid, $task_token = "")
    {
        $auth = Zend_Auth::getInstance();       
        $result = "";

        if(count($objects) > 0 && $objects != 0)
        {
            foreach($objects as $id => $val)
            {

                // Если такой объект еще не парсился никогда
                if(!$val['parsed_object'])
                {
                    
                    // Парсим карточку об-та
                    //  ============================================================
                    $html = file_get_contents($val['link']);
                    $saw = new nokogiri($html);                

                    // Имя владельца и ссылка на его профайл
                    //  ============================================================
                    $owner_name = $saw->get('.object-content__describe__landlord__contact__name a')->toArray();
                    $objects[$id]['owner'] = $owner_name[0]['title'];
                    $objects[$id]['owner_link'] = "http:" . $owner_name[0]['href'];

                    // Название об-та
                    //  ============================================================
                    $name = $saw->get('.object-header__more h1')->toArray();
                    $objects[$id]['name'] = $name[0]['#text'][0];

                    // Описание об-та
                    //  ============================================================
                    $desc = $saw->get('.jsRealtyFullDescription p')->toArray();
                    $desc_clr = isset($desc[0]['#text'][0]) ? trim($desc[0]['#text'][0]) : "";
                    $objects[$id]['desc'] = $desc_clr;       

                    // Адрес об-та
                    //  ============================================================
                    $addr = $saw->get('.object-header__more p')->toArray();
                    $addr = explode(",", trim($addr[0]['#text'][0]));

                    $objects[$id]['addr_street'] = (isset($addr[0])) ? trim($addr[0]) : "";
                    $objects[$id]['addr_number'] =  (isset($addr[1])) ? trim($addr[1]) : "";
                    $objects[$id]['addr_district'] =  (isset($addr[2])) ? trim($addr[2]) : "";

                    // Фотки
                    //  ============================================================
                    $imgs = $saw->get('.royalSlidesContainer li')->toArray();
                    if(count($imgs) > 0)
                    {
                        foreach($imgs as $i_id => $i_val)
                        {
                            $objects[$id]['imgs'][] = (isset($i_val['data-src'])) ? "http:" . $i_val['data-src'] : "";
                        }
                        $objects[$id]['imgs'] = trim(implode(";", $objects[$id]['imgs']));
                    } else {
                        $objects[$id]['imgs'] = "";
                    }

                    // Удобства
                    //  ============================================================
                    $amnts = $saw->get('.object-content__container__comfort__list__item')->toArray();
                    if(count($amnts) > 0)
                    {
                        foreach($amnts as $a_id => $a_val)
                        {
                            if($a_val['class'] != 'object-content__container__comfort__list__item disabled')
                            {
                                $objects[$id]['amenities'][] = (isset($a_val['span'][0]['#text'][0])) ? trim($a_val['span'][0]['#text'][0]) : "";
                            }
                        }
                        
                        $objects[$id]['amenities'] = implode(";", $objects[$id]['amenities']);

                    } else {
                        $objects[$id]['amenities'] = "";
                    }

                    //  Парсим параметры квартиры
                    //  ============================================================
                    $pref = $saw->get('.object-content__container__params li')->toArray();

                    // Количество комнат
                    //  ============================================================
                    $objects[$id]['rooms'] = (isset($pref[0]['#text'][0])) ? preg_replace("/[^0-9]/", "", $pref[0]['#text'][0]) : 0;

                    // Площадь
                    //  ============================================================
                    $objects[$id]['sqr'] = (isset($pref[3]['#text'][0])) ? preg_replace("/[^0-9]/", "", $pref[3]['#text'][0]) : 0;
                    
                    // Спальные места
                    //  ============================================================
                    $objects[$id]['sleepers'] = (isset($pref[1]['#text'][0])) ? preg_replace("/[^0-9]/", "", $pref[1]['#text'][0]) : 0;
                    
                    // Этаж
                    //  ============================================================
                    preg_match_all('/\d+/i', $pref[2]['#text'][0], $matches);
                    $objects[$id]['floor'] = isset($matches[0][0]) ? $matches[0][0] : rand(1, 9);
                    $objects[$id]['elevation'] = isset($matches[0][1]) ? $matches[0][1] : rand(1, 9);
                    
                    // Цена
                    //  ============================================================
                    $price = $saw->get('.object-content__container__pricelist strong')->toArray();
                    $objects[$id]['price'] = preg_replace("/[^0-9]/", "", $price[0]['#text'][0]);

                    // Координаты
                    //  ============================================================
                    preg_match('/MAP_CENTER_COORD_X = ([\d]+[\.][\d]+)/i', $html, $matches);
                    $objects[$id]['lat'] = (isset($matches[1])) ? round(floatval($matches[1]), 6) : 0;

                    preg_match('/MAP_CENTER_COORD_Y = ([\d]+[\.][\d]+)/i', $html, $matches);
                    $objects[$id]['lng'] = (isset($matches[1])) ? round(floatval($matches[1]), 6) : 0;


                    // Парсим карточку владельца
                    //  ============================================================
                    $html = file_get_contents($objects[$id]['owner_link']);
                    $saw = new nokogiri($html);                

                    // Телефон владельца
                    //  ============================================================
                    $phones = $saw->get('.object-content__describe__landlord__contact__list__phone')->toArray();
                    $objects[$id]['phone_1'] =  (isset($phones[0]['#text'][0])) ? Util::stripTel($phones[0]['#text'][0]) : "";
                    $objects[$id]['phone_1_orig']   =  (isset($phones[0]['#text'][0])) ? trim($phones[0]['#text'][0]) : "";
                    $objects[$id]['phone_2'] =  (isset($phones[1]['#text'][0])) ? Util::stripTel($phones[1]['#text'][0]) : "";
                    $objects[$id]['phone_2_orig'] =  (isset($phones[1]['#text'][0])) ? trim($phones[1]['#text'][0]) : "";


                    //  Проверяем есть ли такой владелец в нашей базе
                    $objects[$id]['goyug_owner'] = $this->checkGoyugPhoneExist($objects[$id]['phone_1']);
                    $objects[$id]['parsed_owner'] = $this->checkSpitiPhoneExist($objects[$id]['phone_1']);

                    if(!$objects[$id]['goyug_owner'])
                    {

                        // Пишем а базу объект
                        //  ============================================================
                        $tbl = new Zend_Db_Table(array(
                            'name' => 'pso_spiti_object',
                            'primary' => 'pso_id'
                        ));

                        $row = $tbl->createRow(
                            array(
                              'pso_psu_id' => $objects[$id]['phone_1'],
                              'pso_is_goyug' => $objects[$id]['goyug_owner'],
                              'pso_link' => $objects[$id]['link'],
                              'pso_uid' => $objects[$id]['uid'],
                              'pso_sqr' => $objects[$id]['sqr'],
                              'pso_rooms' => $objects[$id]['rooms'],
                              'pso_name' => $objects[$id]['name'],
                              'pso_desc' => $objects[$id]['desc'],
                              'pso_sleepers' => $objects[$id]['sleepers'],
                              'pso_amenities' => $objects[$id]['amenities'],
                              'pso_images' => $objects[$id]['imgs'],
                              'pso_addr_city_uid' => $city_uid,
                              'pso_addr_street' => $objects[$id]['addr_street'],
                              'pso_addr_number' => $objects[$id]['addr_number'],
                              'pso_addr_district' => $objects[$id]['addr_district'],
                              'pso_addr_floor' => $objects[$id]['floor'],
                              'pso_addr_elevation' => $objects[$id]['elevation'],
                              'pso_addr_lat' => $objects[$id]['lat'],
                              'pso_addr_lng' => $objects[$id]['lng'],
                              'pso_price' => $objects[$id]['price'],
                              'pso_create' => date ( 'Y-m-d H:i:s' ),
                              'pso_creater' => $auth->getIdentity()->u_username
                            )
                        );
                        $row->save();

                        // Обновляем статус задачи
                        //  ============================================================
                        if(!empty($task_token))
                        {
                            $query = "
                                UPDATE pt_parse_tasks
                                SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                WHERE pt_token = '" . $task_token ."'
                            ";
                            $this->db->query($query);         
                        }

                        if(!$objects[$id]['parsed_owner'])
                        {
                            // Пишем а базу владельца если его нет ни в нашей базе ни в спарсенной
                            //  ============================================================
                            $tbl = new Zend_Db_Table(array(
                                'name' => 'psu_spiti_user',
                                'primary' => 'psu_id'
                            ));

                            $row = $tbl->createRow(
                                array(
                                  'psu_is_goyug' => $objects[$id]['goyug_owner'],
                                  'psu_is_avito' => '',
                                  'psu_link' => $objects[$id]['owner_link'],
                                  'psu_phone_1' => $objects[$id]['phone_1'],
                                  'psu_phone_2' => $objects[$id]['phone_2'],
                                  'psu_phone_1_orig' => $objects[$id]['phone_1_orig'],
                                  'psu_phone_2_orig' => $objects[$id]['phone_2_orig'],
                                  'psu_email' => '',
                                  'psu_name' => $objects[$id]['owner'],
                                  'psu_city' => $city_uid,
                                  'psu_create' => date ( 'Y-m-d H:i:s' ),
                                  'psu_creater' => $auth->getIdentity()->u_username
                                )
                            );
                            $row->save();
                        
                        } // if NOT Parsed Spiti Owner
                    
                    } // if NOT GoYug Owner

                } // if NOT parsed

            } // foreach
        } // if count objects > 0
    
        // print_r($objects);die;

        return count($objects);

    }

    //#########################################################################################
function get($url, $ajax=false){
        $ch = curl_init ();
       curl_setopt ($ch , CURLOPT_URL , $url);

        if($ajax) {
           $headers = array(
               'x-requested-with: XMLHttpRequest',
               'accept: application/json, text/javascript, */*; q=0.01',
               'accept-language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,uk;q=0.2',
               'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36',
               'referer: ' . str_replace($this->mobDomain,'',$url),
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
       curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1 );
       $content = curl_exec($ch);
       curl_close($ch);
       return $content;
    }

    //#########################################################################################
    function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($status != '404')
            return $data;
        else
            return 0;
    }
    ##########################################################################################
    public function checkGoyugPhoneExist($phone) {

        if(!empty($phone)){

            $phone = Util::stripTel($phone);

            //Запрос
            $query = "
                SELECT *
                FROM u_user AS u
                WHERE (u.u_phone_1 = '" . $phone ."'
                OR u.u_phone_2 = '" . $phone ."'
                OR u.u_phone_3 = '" . $phone ."')
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function existParsedObject($url) {

        if(!empty($url)){

            //Запрос
            $query = "
                SELECT *
                FROM obj_object
                WHERE obj_url = '" . $url ."'
                AND obj_creater = 'parser_spiti'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function checkSpitiPhoneExist($phone) {

        if(!empty($phone)){

            $phone = Util::stripTel($phone);

            // echo $phone;die;

            //Запрос
            $query = "
                SELECT *
                FROM psu_spiti_user AS psu
                WHERE psu.psu_phone_1 = '" . $phone ."'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function checkAvitoPhoneExist($phone) {

        if(!empty($phone)){

            $phone = Util::stripTel($phone);

            // echo $phone;die;

            //Запрос
            $query = "
                SELECT *
                FROM pau_avito_user AS pau
                WHERE pau.pau_phone = '" . $phone ."'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function checkObjectExist($obj_uid = "", $pars_city) {

        if(!empty($obj_uid)){

            //Запрос
            $query = "
                SELECT *
                FROM pso_spiti_object AS pso
                WHERE pso.pso_uid = '" . $obj_uid ."'
                AND pso.pso_addr_city_uid = '" . $pars_city ."'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function createParseJob($pars_city, $objects_num, $pages_num, $pars_page, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'spiti',
                  'pt_action' => 'parse',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => intval($pages_num),
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => intval($pars_page),
                  'pt_cur_page' => intval($pars_page-1),
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function createDownloadJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'spiti',
                  'pt_action' => 'download',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => '',
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function createClearingJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'spiti',
                  'pt_action' => 'clearingimgs',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => 0,
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }
    ##########################################################################################
    public function createWriteJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'spiti',
                  'pt_action' => 'writetodb',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => 0,
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function updateParseJob($token = "", $value) {

        if(!empty($token)){

            $query = "
                UPDATE pt_parse_tasks
                SET pt_progress = '" . $value ."', pt_end = '".date ( 'Y-m-d H:i:s' )."'
                WHERE pt_token = '" . $token ."'
            ";
            $this->db->query($query);

        }
    }

    ##########################################################################################
    public function getParseJob($token = "") {

        if(!empty($token)){

            $query = "
                SELECT *
                FROM pt_parse_tasks
                WHERE pt_token = '" . $token ."'
            ";
            $row = $this->db->query($query)->fetchObject();

            return $row;

        } else return "empty token";
    }

    ##########################################################################################
    public function generateToken() {
        return Util::generateHash();
    }

    ##########################################################################################
    public function getParsedImages($pars_city) {

        if(!empty($pars_city))
        {
            $path = $this->img_path . "/" . $pars_city;

            if(is_dir($path))
            {
                $arr = Util::dirToArray($path);

                return $arr;

            } else {
                return "folder dosent exist";
            }

        } else {
            return "empty city name";
        }
    }

    ##########################################################################################
    public function countParsedImages($items) {

        $result = 0;

        if(!empty($items))
        {

            foreach($items as $id => $val)
            {
                $result += count($val);
            }

            return $result;

        } else {

            return "items array is empty";

        }

    }


    ##########################################################################################
    public function clearImages($pars_city, $items, $task_token) 
    {
        ini_set("gd.jpeg_ignore_warning", 1);

        if(!empty($items) && count($items) > 0 && !empty($pars_city))
        {

            $path = $this->img_path . "/" . $pars_city;
            $clear_path = $this->img_path . "/_clear_/" . $pars_city;

            $stamp = imagecreatefromjpeg($this->watermark_path);

            foreach($items as $uid => $images)
            {

                foreach($images as $key => $filename)
                {
                    $origin_filename = $path . "/" . $uid . "/" . $filename;

                    $xorigin_path = $clear_path . "/" . $uid;
                    $x800_path = $clear_path . "/" . $uid . "/x800";
                    $x500_path = $clear_path . "/" . $uid . "/x500";
                    $x100_path = $clear_path . "/" . $uid . "/x100";
                    $thumb_path = $clear_path . "/" . $uid . "/thumbnail";

                    list($image_w, $image_h) = getimagesize($origin_filename);

                    if($image_w > $image_h && intval($image_w) >= 500)
                    {
                        if(!file_exists($x800_path)) mkdir($x800_path, 0777, true);
                        if(!file_exists($x500_path)) mkdir($x500_path, 0777, true);
                        if(!file_exists($x100_path)) mkdir($x100_path, 0777, true);
                        if(!file_exists($thumb_path)) mkdir($thumb_path, 0777, true);

                        // $items[$uid][$key] = array("name" => $origin_filename, "w" => $image_w, "h" => $image_h);

                        // Загрузка фото, для которого применяется водяной знак
                        if($im = @imagecreatefromjpeg($origin_filename))
                        {
                            // $true_filename = $this->generateFilename($filename);
                            $true_filename = $filename;

                            imagecopy($im, $stamp, 0, 40, 0, 0, imagesx($stamp), imagesy($stamp));
                            
                            imagejpeg($im, $x800_path . "/" . $true_filename, 85);
                            imagejpeg($im, $xorigin_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize x500
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 500, 375);
                            imagejpeg($im, $x500_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize x100
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 100, 75);
                            imagejpeg($im, $x100_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize Thumbnail (200 x 150)
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 200, 150);
                            imagejpeg($im, $thumb_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Update Job Status
                            $query = "
                                UPDATE pt_parse_tasks
                                SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                WHERE pt_token = '" . $task_token ."'
                            ";
                            $this->db->query($query);         

                        } else {
                            // unlink($origin_filename);
                        }
                        // break;
                    
                    } else {
                        // unset($items[$uid][$key]);
                    }
                
                } // foreach $images
                
            // break;
            }  // foreach $items

            return $items;

        } else {
            return "items array is empty";
        }

    }   

    ##########################################################################################
    public function resizeImage($filename, $width, $height) 
    {
        $image = imagecreatefromjpeg($filename);
        list($image_w, $image_h) = getimagesize($filename);

        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, $image_w, $image_h);

        return $new_image;
  }

    ##########################################################################################
    public function generateFilename($filename) 
    {
        if(!empty($filename))
        {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            list($usec, $sec) = explode(" ", microtime());
            $usec = $usec  * 1000000;
            $microtime = $sec . "-" . $usec;

            $name = date("Y-m-d-H-i") . "-" . $microtime . "." . $ext;        

            return $name;
        } else {
            return 0;
        }

    }   

    ##########################################################################################
    public function addOwner($params) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'u_user',
            'primary' => 'u_id'
        ));

        if(!empty($params['psu_phone_1'])) {

                $email = Util::strtolower_utf8(trim($params['psu_email']));
                $phone = Util::stripTel($params['psu_phone_1']);

                $row = $tbl->createRow();
                $row->u_lf_id = 1;
                $row->u_active = 1;
                $row->u_tutorial = 0;
                $row->u_username = $phone;
                $row->u_password = md5($phone);

                $row->u_money = 0;
                $row->u_authtoken = "";

                $row->u_subscribe_news = 1;
                $row->u_subscribe_email = 1;
                $row->u_subscribe_sms = 1;

                $row->u_firstname = $params['psu_name'];
                $row->u_url = $params['psu_link'];

                $row->u_email = $email;
                $row->u_phone_1 = $phone;
                $row->u_phone_2 = $params['psu_phone_2'];
                $row->u_role = 'owner';
                $row->u_is_phoned = 0;

                $row->u_isnew = 0;

                $row->u_create = date('Y-m-d H:i:s');
                $row->u_update = date('Y-m-d H:i:s');

                $row->u_creater = 'parser_spiti';
                $row->u_updater = 'parser_spiti';

                $owner_id = $row->save();

                return $owner_id;

        }
    }
    ##########################################################################################
    public function addObject($params) {

        $cities = array(
                                // 0 wave
                                "astrakhan" => "Астрахань", 
                                "kislovodsk" => "Кисловодск", 
                                "krasnodar" => "Краснодар", 
                                "pyatigorsk" => "Пятигорск", 
                                "rostovnadonu" => "Ростов-на-Дону", 
                                "sochi" => "Сочи", 
                                "stavropol" => "Ставрополь",

                                // 1 wave
                                "anapa" => "Анапа",
                                "gelendzhik" => "Геленджик",
                                "zheleznovodsk" => "Железноводск",
                                "novorossisk" => "Новороссийск",
                                "pyatigorsk" => "Пятигорск",
                                "sevastopol" => "Севастополь",
                                "simferopol" => "Симферополь",
                                "yalta" => "Ялта",

                                // 2 wave
                                "astrakhan" => "Астрахань",
                                "volgograd" => "Волгоград",
                                "volgodonsk" => "Волгодонск",
                                "volzhsky" => "Волжский",
                                "voronezh" => "Воронеж",
                                "novocherkassk" => "Новочеркасск",
                                "taganrog" => "Таганрог",

                                // 3 wave
                                "alushta" => "Алушта",
                                "evpatoria" => "Евпатория",
                                "feodosia" => "Феодосия",
                                "ufa" => "Уфа",
                                "samara" => "Самара",
                                "kazan" => "Казань",
                                "nnovgorod" => "Нижний Новгород",
                                "ekaterinburg" => "Екатеринбург",
                                "saratov" => "Саратов",
                                "samara" => "Самара",
                    
                                // 4 wave
                                "omsk" => "Омск",
                                "irkutsk" => "Иркутск",
                                "yaroslavl" => "Ярославль",
                                "chelyabinsk" => "Челябинск",
                                "tyumen" => "Тюмень",
                                "spb" => "Санкт-Петербург",
                                "moscow" => "Москва",

                                // 5 wave
                                "krasnoyarsk" => "Красноярск",
                                "novosibirsk" => "Новосибирск",
                                "perm" => "Пермь",
                                "surgut" => "Сургут",
                                "tomsk" => "Томск",
                                "kemerovo" => "Кемерово",
                                "smolensk" => "Смоленск",
                                "khabarovsk" => "Хабаровск",
                                "barnaul" => "Барнаул",
                                "novokuznetsk" => "Новокузнецк",
                                "nvartovsk" => "Нижневартовск",
                                "orenburg" => "Оренбург",
                                "belgorod" => "Белгород",
                                "tula" => "Тула",
                                "kirov" => "Киров",
                                "izhevsk" => "Ижевск",
                                "kursk" => "Курск",

                                // 6 wave
                                "vladivostok"   =>"Владивосток",
                                "vladimir"  =>"Владимир",
                                "kaliningrad"   =>"Калининград",
                                "magnitogorsk"  =>"Магнитогорск",
                                "penza" =>"Пенза",
                                "salekhard" =>"Салехард",
                                "saransk"   =>"Саранск",
                                "syktyvkar" =>"Сыктывкар",
                                "tolyatty"  =>"Тольятти",
                                "ulanude"   =>"Улан-Удэ",
                                "cheboksary"    =>"Чебоксары",
                                "chita" =>"Чита",

                                // 7 wave
                                "arkhangelsk"=>"Архангельск",
                                "bryansk"=>"Брянск",
                                "vologda"=>"Вологда",
                                "ivanovo"=>"Иваново",
                                "kaluga"=>"Калуга",
                                "kurgan"=>"Курган",
                                "lipetsk"=>"Липецк",
                                "orel"=>"Орел",
                                "pskov"=>"Псков",
                                "ryazan"=>"Рязань",
                                "tver"=>"Тверь",
                                "ulyanovsk"=>"Ульяновск",

                                // 8 wave
                                "alushta"=>"Алушта",
                                "blagoveschensk"=>"Благовещенск",
                                "vnovgorod"=>"Великий Новгород",
                                "volzhsky"=>"Волжский",
                                "evpatoria"=>"Евпатория",
                                "yoshkarola"=>"Йошкар-Ола",
                                "kemerovo"=>"Кемерово",
                                "kostroma"=>"Кострома",
                                "nchelni"=>"Набережные Челны",
                                "nefteyugansk"=>"Нефтеюганск",
                                "nizhnekamsk"=>"Нижнекамск",
                                "ntagil"=>"Нижний Тагил",
                                "novocherkassk"=>"Новочеркасск",
                                "novyurengoy"=>"Новый Уренгой",
                                "petrozavodsk"=>"Петрозаводск",
                                "rybinsk"=>"Рыбинск",
                                "staryoskol"=>"Старый Оскол",
                                "taganrog"=>"Таганрог",
                                "tambov"=>"Тамбов",
                                "tobolsk"=>"Тобольск",
                                "ukhta"=>"Ухта",
                                "khantymansiysk"=>"Ханты-Мансийск",
                                "cherepovets"=>"Череповец",
                                
                                
        );

        $elevation = array(
            "5", "9", "11", "16"
            );

        $tbl = new Zend_Db_Table(array(
            'name' => 'obj_object',
            'primary' => 'obj_id'
        ));

        if(!empty($params['u_id'])) 
        {
                // $obj_addr_floor = (intval($params['pso_addr_floor']) > 0) ? $params['pso_addr_floor'] : 3;
                
                // $obj_addr_elevation = $elevation[array_rand($elevation, 1)];
                // while ($obj_addr_floor > $obj_addr_elevation) 
                // {
                //     $obj_addr_elevation = $elevation[array_rand($elevation, 1)];
                // }

                $row = $tbl->createRow();
                
                $row->obj_enable = 1;
                $row->obj_phantom = 1;
                
                $row->obj_ontop = 0;
                $row->obj_ontop_mark = 0;
                $row->obj_ontop_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_photomark = 0;
                $row->obj_photomark_activate = '0000-00-00 00:00:00';
                $row->obj_photomark_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_mark_out = 0;
                $row->obj_mark_out_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_isempty = 0;
                $row->obj_isempty_activate = '0000-00-00 00:00:00';
                $row->obj_isempty_nextpay = '0000-00-00 00:00:00';
                $row->obj_isempty_deactivate = '0000-00-00 00:00:00';

                $row->obj_sale = 0;
                $row->obj_sale_activate = '0000-00-00 00:00:00';
                $row->obj_sale_nextpay = '0000-00-00 00:00:00';
                $row->obj_sale_deactivate = '0000-00-00 00:00:00';



                $row->obj_hotorder = 1;
                $row->obj_hotorder_activate = date('Y-m-d H:i:s');
                $row->obj_hotorder_nextpay = date('Y-m-d H:i:s', strtotime(' + 30 days'));
                $row->obj_hotorder_deactivate = date('Y-m-d H:i:s', strtotime(' + 30 days'));

                $row->obj_paramsorder = 1;
                $row->obj_paramsorder_activate = date('Y-m-d H:i:s');
                $row->obj_paramsorder_nextpay = date('Y-m-d H:i:s', strtotime(' + 30 days'));
                $row->obj_paramsorder_deactivate = date('Y-m-d H:i:s', strtotime(' + 30 days'));

                $row->obj_virtual = 0;
                $row->obj_moderated = 1;
                
                $row->obj_uid = Util::generateUID();
                $row->obj_u_id = $params['u_id'];

                $row->obj_sqr = $params['pso_sqr'];
                $row->obj_rooms = $params['pso_rooms'];
                $row->obj_sleepers = $params['pso_sleepers'];
                $row->obj_checkin = "12:00";
                $row->obj_checkout = "12:00";
                $row->obj_url = $params['pso_link'];
                $row->obj_name_ru = $params['pso_name'];
                $row->obj_fulldesc_ru = $params['pso_desc'];
                $row->obj_search_address = $cities[$params['pso_addr_city_uid']];
                $row->obj_addr_country_code = "RU";
                $row->obj_addr_country = "Россия";
                $row->obJ_addr_area_2 = $params['pso_addr_district'];

                $row->obj_addr_city_uid = $params['pso_addr_city_uid'];
                $row->obj_addr_city = $cities[$params['pso_addr_city_uid']];
                $row->obj_addr_street = $params['pso_addr_street'];
                $row->obj_addr_number = $params['pso_addr_number'];
                
                $row->obj_addr_floor = $params['pso_addr_floor'];
                $row->obj_addr_elevation = $params['pso_addr_elevation'];

                $row->obj_addr_lat = $params['pso_addr_lat'];
                $row->obj_addr_lon = $params['pso_addr_lng'];
                $row->obj_addr_lat_view = $params['pso_addr_lat'];
                $row->obj_addr_lon_view = $params['pso_addr_lng'];
                
                $row->obj_price = $params['pso_price'];
                $row->obj_price_period = 2;
                $row->obj_acc_type = 1;
                $row->obj_price_minstay = 1;
                $row->obj_deposit = 0;

                $row->obj_create = date('Y-m-d H:i:s');
                $row->obj_update = date('Y-m-d H:i:s');

                $row->obj_creater = 'parser_spiti';
                $row->obj_updater = 'parser_spiti';

                $object_id = $row->save();

                // Добавляем данные в таблицу v_val
                $tbl = new Zend_Db_Table(array(
                    'name' => 'v_value',
                    'primary' => 'v_id'
                ));

                $row_2 = $tbl->createRow(
                    array(
                      'v_table' => 'ot_object_type',
                      'obj_id' => $object_id,
                      'v_key' => '10',
                      'v_val' => '',
                    )
                );
                $row_2->save();

                return $object_id;

        }
    }

    ##########################################################################################
    public function addImages($src_id, $dst_id, $pars_city) 
    {
        $src_path = $this->img_path . "/_clear_/" . $pars_city . "/" . $src_id;
        // $dst_path = $this->img_path . "/_clear_/" . $pars_city . "/" . $dst_id;
        $dst_path = $this->img_path . "/_write_/" . $dst_id;

        // Util::recurse_copy($src_path, $dst_path);
        rename($src_path, $dst_path);

        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));

        foreach (scandir($dst_path) as $node) {
            if ($node != '.' && $node != '..' && !is_dir($src_path . '/' . $node))
            {
                $row = $tbl->createRow();

                $row->f_datetime = date('Y-m-d H:i:s');
                $row->f_ucid = 'object';
                $row->f_uid = $dst_id;
                $row->f_name = $node;
                $row->f_title = $pars_city;
                $row->f_description = "parser_spiti";
                $row->f_size = filesize($dst_path . '/' . $node);
                $row->f_type = "image/jpeg";
                $row->f_order = 999;

                $row->save();
            }
        }

    }


    ##########################################################################################
    public function getParsedOwnerID($phone) {

        if(!empty($phone)){

            $query = "
                SELECT *
                FROM u_user
                WHERE u_phone_1 = '" . $phone ."'
                AND u_role = 'owner'
                AND u_creater = 'parser_spiti'
            ";
            $row = $this->db->query($query)->fetchObject();

            return isset($row->u_id) ? $row->u_id : 0;

        } else return "empty phone";
    }

    //#########################################################################################
    public function writeToDB($pars_city, $task_token)
    {
        set_time_limit(0);
        $result = array();
        $obj_ids = array();

        $imgdir = $this->img_path . '/_clear_/' . $pars_city;

        $pso_items = $this->spitiGetParsedObjects($pars_city);

        // print_r($pso_items); die;

        // Получаем список спарсенных об-тов с привязкой к владельцу
        if(count($pso_items) > 0)
        {
            foreach($pso_items as $pso_ind => $pso_val)
            {
                // Проверяем есть ли у объекта "чистые" фотки
                // Если фотки есть: начинаем процедуру копирования объекта в "живую" базу
                if(is_dir($imgdir . '/' . $pso_val['pso_uid']))
                {

                    // Проверка:
                    // Если владельца с таким телефоном нет - добавляем и берем его ID
                    // Если есть - берем его ID, если он ранее спарсен нами
                    if(!$this->checkGoyugPhoneExist($pso_val['psu_phone_1']))
                    {
                        $owner_id = $this->addOwner($pso_val);
                    } else {
                        $owner_id = $this->getParsedOwnerID($pso_val['psu_phone_1']);
                    }

                    $pso_val['u_id'] = $owner_id;

                    // Если получили ID владельца начинаем копировать объекты
                    if($owner_id > 0) 
                    {
                        if(!$this->existParsedObject($pso_val['pso_link']))
                        {
                            $object_id = $this->addObject($pso_val);
                            $this->addImages($pso_val['pso_uid'], $object_id, $pars_city);

                            // Update Job Status
                            $query = "
                                UPDATE pt_parse_tasks
                                SET pt_progress = 0, pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                WHERE pt_token = '" . $task_token ."'
                            ";
                            $this->db->query($query);         

                        }

                    }

                } // IF exist img_dir
            } // foreach
        } // if array not empty

    }

    //#########################################################################################
    public function spitiGetParsedObjects($pars_city)
    {
        if(!empty($pars_city))
        {
            if($pars_city == "spb" || $pars_city == "moscow")
            {
                $query = "
                    SELECT *
                    FROM psu_spiti_user AS psu

                    LEFT JOIN pso_spiti_object AS pso
                    ON psu.psu_phone_1 = pso.pso_psu_id


                    WHERE psu.psu_is_goyug = 0
                    AND psu.psu_city =  '" . $pars_city ."'

                    ORDER BY RAND()
                    LIMIT 956
                ";

            } else {

                $query = "
                    SELECT *
                    FROM psu_spiti_user AS psu

                    LEFT JOIN pso_spiti_object AS pso
                    ON psu.psu_phone_1 = pso.pso_psu_id

                    WHERE psu.psu_is_goyug = 0
                    AND psu.psu_city =  '" . $pars_city ."'
                ";
            }

            $data = $this->db->query($query)->fetchAll();

            if(!empty($data) && count($data) > 0)
            {
                return $data;
            
            } else {
                return 0;
            }

        } else {
            return 0;
        }

    }

}  
?>
