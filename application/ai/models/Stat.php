<?php
require_once("Util.php");

/**
 */
class Ai_Model_Stat extends Zend_Db_Table_Abstract {
    
    public $_name = 'obj_object';
    public $_primary = 'obj_id';
    
    ##########################################################################################
    public function init() 
    {
        $this->auth = Zend_Auth::getInstance();
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');
        
        $this->lang = $locale->curlocale['lang'];
        
        $this->pref = "obj_";
        $this->id = $this->pref . 'id';
        $this->name = $this->pref . 'name_' . $this->lang;

        $this->ucid = "object";

        $this->log = new Ai_Model_Log();

        $this->rentalCities = array(
            "kislovodsk", 
            "nnovgorod", 
            "sochi", "sevastopol", "spb", 
            "krasnodar", "saratov", "tyumen", 
            "novosibirsk", "ekaterinburg", "perm", 
            "kazan", "ufa", "voronezh", 
            "moscow", "omsk", "rostovnadonu", "volgograd",
            "krasnoyarsk", "chelyabinsk", "irkutsk", "stavropol", "samara",
            "zheleznovodsk",
            "pyatigorsk", "yalta", "yaroslavl", "tomsk",
            "bryansk", "tula", "magnitogorsk", "ivanovo",
            "khabarovsk", "vladimir", "penza", "kirov", "vologda",
            "surgut", "lipetsk", "ulyanovsk", "kemerovo", "kursk",
            "astrakhan", "tolyatty", "orel", "orenburg", "tver",
            "syktyvkar", "saransk", "ryazan", "pskov", "novocherkassk", "adler", "anapa"
    
        );

        $this->rentalCitiesSQL = "('" . implode("','", $this->rentalCities) . "')";

    }
    
    ##########################################################################################
    public function getNewObjects($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (obj_create BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, count(obj_id) AS total
            FROM obj_object

            WHERE obj_creater NOT IN ('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru', 'parser_spiti')
            ". $period_sql ."
            ". $rental_cities_sql ."
            AND obj_u_id > 0
            GROUP BY obj_addr_city_uid
            ORDER BY total DESC
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getObjects($params) 
    {
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, count(obj_id) AS total
            FROM obj_object AS obj

            LEFT JOIN u_user AS u
            ON obj.obj_u_id = u.u_id

            WHERE obj_u_id > 0
            AND obj_enable = 1
            AND u.u_rental = 1

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        
        
        return $rows;
    }

    ##########################################################################################
    public function getNewUsers($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (u_create BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, COUNT(*) as total
            FROM u_user AS u

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE u.u_role = 'owner'
            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY obj_addr_city_uid
            ORDER BY total DESC
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getUsersOntop($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cf_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, count(*) AS total
            FROM (
                SELECT u_id, obj_addr_city
                FROM cf_cashflow AS cf

                LEFT JOIN u_user AS u
                ON cf.cf_u_id = u.u_id

                LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
                ON obj.obj_u_id = u.u_id

                WHERE cf_transaction LIKE '%ontop%'
                AND cf_success = 1
                AND u.u_email NOT LIKE '%@tryred.ru'

                ". $period_sql ."
                ". $rental_cities_sql ."

                GROUP BY cf_u_id
                ORDER BY obj_addr_city
            ) AS t1
            GROUP BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getMoney($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cf_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, round((SUM(cf_sum) - SUM(cf_sum)*0.055), 2) AS total

            FROM cf_cashflow

            LEFT JOIN u_user AS u
            ON u.u_id = cf_u_id

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE cf_mode = 'balanceoff'
            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getMoneyOntop($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cf_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj.obj_addr_city AS city, round((SUM(cf_sum) - SUM(cf_sum)*0.055), 2) AS total
            FROM cf_cashflow AS cf

            LEFT JOIN u_user AS u
            ON cf.cf_u_id = u.u_id

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid, obj_enable FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE cf_transaction LIKE 'ontop%'
            AND cf_mode = 'balanceoff'
            AND cf_success = 1
            AND u.u_username NOT IN ('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru')
            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getAbonements($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cf_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, COUNT(*) AS total
            FROM u_user

            LEFT JOIN cf_cashflow
            ON cf_u_id = u_id

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid, obj_enable FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u_id

            WHERE cf_transaction = 'rental'
            AND u_email NOT LIKE '%@tryred.ru'
            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY obj_addr_city
            ORDER BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getAbonementsNow($params) 
    {
        $query = "
            SELECT obj_addr_city AS city, COUNT(*) AS total
            FROM u_user AS u

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid, obj_enable FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE u.u_role = 'owner'
            AND u.u_email NOT LIKE '%@tryred.ru'
            AND u.u_rental = 1
            AND (u.u_notes <> 'virtrental' OR ISNULL(u.u_notes) OR u.u_notes = '')

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city;
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getAbonementsAllInCity($params) 
    {
        $rental_cities_sql = 
            ($params['city'] == 'rental') ? 
            "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : 
            "AND obj_addr_city_uid = '".$params['city']."'";

        $query = "
            SELECT u_id, LCASE(u.u_email) as u_email, u_notes, DATE(u_rental_activate), DATE(u_rental_deactivate), u_money
            FROM u_user AS u

            LEFT JOIN (SELECT * FROM obj_object GROUP BY obj_u_id)
            AS obj ON obj.obj_u_id = u.u_id

            WHERE u.u_role = 'owner'
            
            " . $rental_cities_sql . "

            AND u.u_email != ''
            AND u.u_email LIKE '%@%.%'
            AND u.u_rental = 1
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getEmails($params) 
    {
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, COUNT(*) as total
            FROM u_user AS u

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid, obj_enable FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE u.u_role = 'owner'
            AND u.u_email != ''
            AND u.u_email LIKE '%@%.%'
            AND u.u_email NOT LIKE '%@tryred.ru'
            AND NOT ISNULL(u.u_email)

            ". $rental_cities_sql ."

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getLogins($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (l.l_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "WHERE obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj.obj_addr_city AS city, COUNT(*) AS total
            FROM (

                SELECT l.l_username AS user
                FROM l_log AS l

                WHERE l_status = 'success' 
                AND l_result IN ('login', 'authtoken_success')
                AND l.l_username NOT IN ('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru')
                AND l.l_username NOT LIKE '799900000%'
                ". $period_sql ."

                GROUP BY l.l_username
            ) AS t1

            LEFT JOIN u_user AS u
            ON u.u_username = t1.user

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            ". $rental_cities_sql ."

            GROUP BY obj.obj_addr_city
    ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getOntopReal($params) 
    {
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";

        $query = "
            SELECT obj_addr_city AS city, COUNT(*) AS total, GROUP_CONCAT(DISTINCT u.u_id), CONCAT('<a href=\"http://', obj_addr_city_uid ,'.goyug.com\" target=\"_blank\">', obj_addr_city, '</a>')
            FROM u_user AS u

            LEFT JOIN (SELECT * FROM obj_object) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE obj.obj_enable = 1
            AND u.u_username NOT IN ('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru')
            AND u.u_username NOT LIKE '799900000%'
            AND obj.obj_ontop_mark = 1
            AND obj.obj_phantom = 0
            AND obj.obj_virtual = 0

            ". $rental_cities_sql ."

            GROUP BY obj_addr_city_uid
            ORDER BY obj_addr_city;
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getPayments($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cf_date BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";


        $query = "
            SELECT cf_date, cf_sum, cf_transaction, cf_u_id, cf_obj_id, obj.obj_addr_city, u.u_email, u.u_create, u.u_creater
            FROM cf_cashflow

            LEFT JOIN u_user AS u
            ON u.u_id = cf_u_id

            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj
            ON obj.obj_u_id = u.u_id

            WHERE cf_mode = 'balanceoff' 
            AND cf_success = 1
            
            ". $period_sql ."
            ". $rental_cities_sql ."

            ORDER BY cf_date DESC            
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getOrders($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cel_date_stage BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";


        $query = "
            SELECT cel_date_stage, celt_email_subject, obj.obj_addr_city, COUNT(*)
            FROM celt_cron_email_list_tasks

            LEFT JOIN cel_cron_email_list ON celt_uid = cel_celt_uid
            LEFT JOIN u_user AS u ON cel_email = u.u_email
            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj ON obj.obj_u_id = u.u_id

            WHERE celt_email_subject LIKE '%Запрос бронирования%'

            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY celt_uid
            ORDER BY celt_date_create DESC
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getParamOrders($params) 
    {
        $period_sql = (!empty($params['date_from']) && !empty($params['date_to'])) ? "AND (cel_date_stage BETWEEN '".$params['date_from']." 00:00:00' AND '".$params['date_to']." 23:59:59')" : "";
        $rental_cities_sql = ($params['city'] == 'rental') ? "AND obj.obj_addr_city_uid IN " . $this->rentalCitiesSQL : "";


        $query = "
            SELECT cel_date_stage, celt_email_subject, obj.obj_addr_city, COUNT(*)
            FROM celt_cron_email_list_tasks

            LEFT JOIN cel_cron_email_list ON celt_uid = cel_celt_uid
            LEFT JOIN u_user AS u ON cel_email = u.u_email
            LEFT JOIN (SELECT obj_u_id, obj_addr_city, obj_addr_city_uid FROM obj_object GROUP BY obj_u_id) AS obj ON obj.obj_u_id = u.u_id

            WHERE celt_email_subject LIKE '%ищет квартиру на%'

            ". $period_sql ."
            ". $rental_cities_sql ."

            GROUP BY celt_uid
            ORDER BY celt_date_create DESC
        ";

        $query = $this->db->query($query);
        $query->setFetchMode(Zend_Db::FETCH_NUM);
        $rows = $query->fetchAll();

        return $rows;
    }






























    ##########################################################################################
    public function getTotalObjectsAmount() {
        
        $items = $this->db->query("
            SELECT COUNT(*) AS amount
            FROM obj_object
        ")->fetchAll();

        return $items[0]["amount"];
    }
    
    ##########################################################################################
    public function getTodayObjectsAmount() 
    {
        $items = $this->db->query("
            SELECT count(*) AS amount 
            FROM obj_object 
            WHERE DATE(obj_create) = CURDATE()
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getWeekObjectsAmount($shift = 0) 
    {
        $items = $this->db->query("
            SELECT count(*) AS amount 
            FROM obj_object 
            WHERE WEEKOFYEAR(obj_create) = WEEKOFYEAR(NOW()) - " . $shift . "
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getTotalViewsAmount() {
        
        $items = $this->db->query("
            SELECT COUNT(*) AS amount
            FROM os_objstat
            WHERE os_action = 'view'
        ")->fetchAll();

        return $items[0]["amount"];
    }
    
    ##########################################################################################
    public function getTodayViewsAmount() 
    {
        $items = $this->db->query("
            SELECT count(*) AS amount 
            FROM os_objstat
            WHERE os_action = 'view'
            AND DATE(os_date) = CURDATE()
        ")->fetchAll();

        return $items[0]["amount"];
    }
    
    ##########################################################################################
    public function getWeekViewsAmount($shift = 0) 
    {
        $items = $this->db->query("
            SELECT count(*) AS amount 
            FROM os_objstat
            WHERE os_action = 'view'
            AND WEEKOFYEAR(os_date) = WEEKOFYEAR(NOW()) - " . $shift . "
        ")->fetchAll();

        return $items[0]["amount"];
    }

    ##########################################################################################
    public function getCitiesStat() 
    {
        $items = $this->db->query("
            SELECT *
            FROM ct_city
            ORDER BY ct_objects DESC
        ")->fetchAll();

        return $items;
    }

    ##########################################################################################
    public function getDBName() 
    {

        $dbname = $this->db->query("SELECT DATABASE()")->fetchAll();
        $dbname = $dbname[0]["DATABASE()"];

        return $dbname;
    }

    ##########################################################################################
    public function getDBSize($dbname) 
    {

        $dbsize = $this->db->query("
            SELECT table_schema, 
            ROUND(sum( data_length + index_length ) / 1024 / 
            1024, 2) 'size', 
            ROUND(sum( data_free )/ 1024 / 1024, 2) 'free' 
            FROM information_schema.TABLES 
            WHERE table_schema = '".$dbname."'
            GROUP BY table_schema 
        ")->fetchAll();
        
        $dbsize = $dbsize[0]["size"];

        return $dbsize;
    }

    ##########################################################################################
    public function getDBStat($dbname) 
    {
        $result = array();

        $rows = $this->db->query("
            SELECT table_name,Engine,Version,Row_format,table_rows,Avg_row_length,
            Data_length,Max_data_length,Index_length,Data_free,Auto_increment,
            Create_time,Update_time,Check_time,table_collation,Checksum,
            Create_options,table_comment FROM information_schema.tables
            WHERE table_schema = '".$dbname."'
            ORDER BY Data_length DESC;
        ")->fetchAll();
        
        // print_r($rows);die;

        foreach($rows as $key => $val)
        {
            $size = ($val['Data_length'] + $val['Index_length']) / 1024 / 1024;

            $result[] = array(
                "table" => $val['table_name'],
                "rows" => $val['table_rows'],
                "size" => $size,
            );
        }

        return $result;
    }

    ##########################################################################################
    public function getOrdersStat($period = array()) 
    {
        $objectModel = new Ai_Model_Object();
        $result = array();

        $period_query = (count($period) > 0) ? "AND (l_date BETWEEN '".$period[0]." 00:00:00' AND '".$period[1]." 23:59:59')" : "";

        // print_r( $period_query );die;

        $rows = $this->db->query("
            SELECT l_date, l_ip, l_object, l_result
            FROM l_log 
            WHERE l_status = 'success' 
            AND (l_result = 'sendorder' OR l_result = 'showtelext')
            ". $period_query ."
            ORDER BY l_date DESC
        ")->fetchAll();

        foreach($rows as $key => $value)
        {
            $order_info = json_decode($value['l_object']);
            $object_info = $objectModel->getOwnerObjectByObjID( $order_info->obj_id );

            $result[] = array(
                "date" => $value['l_date'],
                "ip" => $value['l_ip'],
                "object" => array(
                    "id" => $order_info->obj_id,
                    "name" => $object_info['obj_name_ru'],
                    "city" => $object_info['obj_addr_city'],
                    "city_uid" => $order_info->cityuid,
                ),
                "owner" => array(
                    "name" => $object_info['u_lastname'] . " " . $object_info['u_firstname'], 
                    "phone" => $object_info['u_phone_1'], 
                    "email" => $object_info['u_email'], 
                ),
                "order" => array(
                    "type" => $value['l_result'],
                    "name" => $order_info->order->name,
                    // "phone" => Util::stripTel( $order_info->order->phone ),
                    "checkin" => $order_info->order->checkin,
                    "checkout" => $order_info->order->checkout,
                ),
            );
        }

        return $result;
    }

    ##########################################################################################
    public function getLogStatByCity($actions = array(), $period = array()) 
    {
        $objectModel = new Ai_Model_Object();
        
        $result = array();
        $actions_sql = array();
        
        $period_sql = (count($period) > 0) ? "AND (l_date BETWEEN '".$period[0]." 00:00:00' AND '".$period[1]." 23:59:59')" : "";

        if(count($actions) > 0)
        {
            $actions_sql = "'" . implode("','", $actions) . "'";
        }

        $query = "
            SELECT l_date, l_ip, l_object, l_result, l_raw
            FROM l_log 
            WHERE l_status = 'success' 
            AND l_result IN (".$actions_sql.")
            AND LOWER(l_useragent) NOT LIKE '%bot%'
            ". $period_sql ."
            ORDER BY l_date DESC
        ";

        $rows = $this->db->query($query)->fetchAll();

        // print_r($query);die;
        // print_r($rows);die;

        foreach($rows as $key => $value)
        {
            $l_object = json_decode($value['l_object']);
            $result[$l_object->cityuid][$value['l_result']][] = 1;
        }

        return $result;
    }
    
    ##########################################################################################
    public function getLoginsStat($period = array()) 
    {
        $objectModel = new Ai_Model_Object();
        
        $result = array();
        $usernames = array();
        
        $period_sql = (count($period) > 0) ? "AND (l_date BETWEEN '".$period[0]." 00:00:00' AND '".$period[1]." 23:59:59')" : "";

        $query = "
            SELECT l_date, l_ip, l_object, l_result, l_raw
            FROM l_log 
            WHERE l_status = 'success' 
            AND l_result IN ('login')
            AND LOWER(l_useragent) NOT LIKE '%bot%'
            ". $period_sql ."
            ORDER BY l_date DESC
        ";

        $rows = $this->db->query($query)->fetchAll();

        // print_r($query);die;
        // print_r($rows);die;

        foreach($rows as $key => $value)
        {
            $l_raw = json_decode($value['l_raw']);

            if(isset($l_raw->u_username) && !empty($l_raw->u_username)
                && !in_array($l_raw->u_username, array('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru') )
                )
            $usernames[] = $l_raw->u_username;
        }

        $usernames_sql = "'" . implode("','", $usernames) . "'";

        $query = "
            SELECT u.u_username, obj.obj_addr_city_uid AS city, count(obj.obj_id) as objects
            FROM u_user as u
            LEFT JOIN obj_object AS obj
            ON u.u_id = obj.obj_u_id
            WHERE u.u_role = 'owner'
            AND u.u_username IN (".$usernames_sql.")
            GROUP BY u.u_id
            ORDER BY city, objects        
        ";

        $rows = $this->db->query($query)->fetchAll();

        // print_r($rows);die;

        foreach($rows as $id => $val)
        {
            if(!empty($val['city']))    
            {
                $result[$val['city']][] = 1;    
            }
        }

        return $result;
    }

    ##########################################################################################
    public function getNewUsersStat($period = array()) 
    {
        $objectModel = new Ai_Model_Object();
        
        $result = array();
        $usernames = array();
        
        $period_sql = (count($period) > 0) ? "AND (u_create BETWEEN '".$period[0]." 00:00:00' AND '".$period[1]." 23:59:59')" : "";

        $query = "
            SELECT obj_addr_city_uid AS city
            FROM u_user AS u
            LEFT JOIN obj_object AS obj
            ON obj.obj_u_id = u.u_id
            WHERE
            u_creater NOT IN ('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru')
            ". $period_sql ."
            AND u_role = 'owner'
        ";

        // print_r($query);die;

        $rows = $this->db->query($query)->fetchAll();

        foreach($rows as $id => $val)
        {
            if(!empty($val['city']))    
            {
                $result[$val['city']][] = 1;    
            }
        }

        return $result;
    }







}
?>
