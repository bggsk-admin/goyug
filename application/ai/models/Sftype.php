<?php
class Ai_Model_Sftype extends Zend_Db_Table_Abstract {

    public $_name = 'sft_smartfilters_type';
    public $_primary = 'sft_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $locale  = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref     = "sft_";
        $this->id       = $this->pref . 'id';
        $this->name     = $this->pref . 'name';
    }
    ##########################################################################################
    public function getItems($where = array(), $order = array())
    {
            $select = $this->select()          
                    ->from(array('sft_smartfilters_type', array('*') ));        
    
            if(!empty($where)) foreach ( $where as $condition ) { $select->where( $condition ); }
            if(!empty($order)) $select->order( $order );
            
            return $this->fetchAll($select);
    }

    ##########################################################################################
    public function getItemsForSelect($where = array(), $order = array())
    {
            $items = array();
            $res = array();

            $select = $this->select()          
                    ->from(array('sft_smartfilters_type', array('*') ));        
    
            if(!empty($where)) foreach ( $where as $condition ) { $select->where( $condition ); }
            if(!empty($order)) $select->order( $order );
            
            $items = $this->fetchAll($select)->toArray();

            foreach($items as $i_id => $i_val)
            {
                $res[$i_val['sft_id']] = $i_val['sft_name'];
            }

            return $res;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }


    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues['sft_id'])->current();
        $row->setFromArray($formValues);

        if($row)
        {
            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();

                if($row)
                {
                    $row->delete();
                }
                else        throw new Zend_Exception("Could not delete item. Item not found!");
            }

        }
    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }



}
?>
