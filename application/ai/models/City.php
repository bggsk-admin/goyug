<?php
require_once ("Util.php");

class Ai_Model_City extends Zend_Db_Table_Abstract {

    public $_name       = 'ct_city';
    public $_primary    = 'ct_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $this->locale  = new Zend_Session_Namespace('locale');

        $this->lang = $this->locale->curlocale['lang'];

        $this->categories   = 'ct_city';
        $this->items        = 'obj_object';

        //AptTypes
        $this->pref     = 'ct_';
        $this->id       = $this->pref . 'id';
        $this->pid      = $this->pref . 'pid';

        $this->name    = $this->pref . 'name_' . $this->lang;

        //Items
        $this->item_pref = 'obj_';
        $this->item_id   = $this->item_pref . 'id';
        $this->item_pid  = $this->item_pref . $this->pref . 'id';
    }

    ##########################################################################################
    public function getItems( $where = array(), $order = array() )
    {

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(
                    array($this->categories),
                    array(
                        'id'        => $this->id,
                        'pid'       => $this->pid,
                        'enable'    => $this->pref . 'enable',
                        'top'    => $this->pref . 'top',
                        'mainmap'    => $this->pref . 'mainmap',
                        'name'      => $this->name,
                        'order'      => $this->pref . "order",
                        'uid'      => $this->pref . "uid",
                        'lat'      => $this->pref . "lat",
                        'lng'      => $this->pref . "lng",
                        'minprice'      => $this->pref . "minprice",
                        'maxprice'      => $this->pref . "maxprice",
                        'objects'      => $this->pref . "objects",
                        //'items'     => "COUNT(".$this->item_id.")",
                    )
                )
                //->joinLeft(array($this->items), $this->id . ' = ' . $this->item_pid )
                //->group( $this->id )
                ;

        if(!empty($order)) $select->order( $order );
        if(!empty($where)) foreach ( $where AS $condition ) { $select->where( $condition ); }

        $rows = $this->fetchAll($select)->toArray();

        $res = array();
        foreach($rows as $row_id => $row_val)    $res[$row_val["id"]] = $row_val;

        return $res;
    }

    ##########################################################################################
    public function arrToTree( $rows )
    {
        $tree = array();        //stores the tree
        $tree_index = array();  //an array used to quickly find nodes in the tree

        while(count($rows) > 0)
        {
            foreach($rows as $row_id => $row)
            {
                if( $row["pid"] )
                {

                    if((!array_key_exists($row["pid"], $rows)) && (!array_key_exists($row["pid"], $tree_index)))
                    {
                        unset($rows[$row_id]);
                    }
                    else
                    {
                        if(array_key_exists($row["pid"], $tree_index))
                        {
                            $parent = & $tree_index[$row["pid"]];
                            $parent['child'][$row_id] = array("node" => $row, "child" => array());
                            $tree_index[$row_id] = & $parent['child'][$row_id];
                            unset($rows[$row_id]);
                        }//if

                    }//else
                }
                else
                {
                    $tree[$row_id] = array("node" => $row, "child" => array());
                    $tree_index[$row_id] = & $tree[$row_id];
                    unset($rows[$row_id]);
                }//else
            }//foreach
        }//while

        unset($tree_index);

        return $tree;
    }

    ##########################################################################################
    public function printTree($node, $level)
    {
        //print the current node
        $html = str_repeat(".", $indent) . "<li>". $level ." ". $node['node'][$this->name];

        if($node['child'])
        {
            $html .= "\n". str_repeat(" ", $indent + 10) . "<ul>\n";

            //then print it's child nodes
            foreach($node['child'] as $child)
            {
                $html .= $this->printTree($child, $level + 1);
            }

            $html .= str_repeat(" ", $indent + 10) . "</ul>\n". str_repeat(" ", $indent);
        }

        $html .= "</li>\n";
        return $html;
    }

    ##########################################################################################
    public function getSelectCtgTree($show_root = TRUE)
    {
        $res = "";
        $items = array();

        if($show_root) $items[0] = html_entity_decode("&mdash; Корневой &mdash;", ENT_COMPAT, "UTF-8");

        $rows = $this->getItems();
        $tree = $this->arrToTree( $rows );

        foreach($tree as $node)
        {
            $res .= $this->_getSelectCtgTree($node, 0);
        }

        $res = preg_replace('/\|$/', '', $res);
        $res = explode("|", $res);

        foreach($res as $val)
        {
            list($id, $name, $level) = explode("^", $val);
            $name = str_repeat("&nbsp;", ($level*5)).$name;
            $items[$id] = html_entity_decode($name, ENT_COMPAT, "UTF-8");
        }


        return $items;
    }

    ##########################################################################################
    function _getSelectCtgTree($node, $level)
    {
        $html = "";

        //$html .= $node['node']['id']."^".$node['node']['name']." (".$node['node']['items'].")^".$level."|";
        $html .= $node['node']['id']."^".$node['node']['name']."^".$level."|";

        if($node['child'])
        {
            foreach($node['child'] as $child)
            {
                $html .= $this->_getSelectCtgTree($child, $level + 1);
            }
        }

        return $html;
    }

    ##########################################################################################
    public function getSelectCtgTreeDisabled( $apttyp_id_arr)
    {
        $disabled = array();
        $childs = $this->getChildsArr();
        unset($childs[0]);

        foreach( $apttyp_id_arr as $apttyp_id => $apttyp_name)
        {
            if(is_array($childs[$apttyp_id])) $disabled[] = $apttyp_id;
        }

        return $disabled;
    }

    ##########################################################################################
    public function getChildsArr()
    {
        $select = $this->db->query("SELECT ".$this->pid.", GROUP_CONCAT(".$this->id.") AS childs FROM ".$this->categories." GROUP BY ".$this->pid);
        $rows = $select->fetchAll();

        $res = array();
        foreach($rows as $id => $val)
        {
            $res[$val[$this->pid]] = explode(",", $val['childs']);
        }

        return $res;
    }

    ##########################################################################################
    public function getChildsByID($apttyp_id, $childs_arr)
    {
        $res .= $apttyp_id.",";

        if($childs_arr[$apttyp_id])
        {
            foreach($childs_arr[$apttyp_id] as $child)
            {
                $res .= $this->getChildsByID($child, $childs_arr);
            }
        }

        return $res;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        // print_r($formValues);die;

        $formValues['ct_name_en'] = $formValues['ct_uid'];

        $formValues['ct_order'] = (!empty($formValues['ct_order'])) ? $formValues['ct_order'] : 0;       
        $formValues['ct_lat'] = (!empty($formValues['ct_lat'])) ? $formValues['ct_lat'] : 0;       
        $formValues['ct_lng'] = (!empty($formValues['ct_lng'])) ? $formValues['ct_lng'] : 0;       
        $formValues['ct_minprice'] = (!empty($formValues['ct_minprice'])) ? $formValues['ct_minprice'] : 0;       
        $formValues['ct_maxprice'] = (!empty($formValues['ct_maxprice'])) ? $formValues['ct_maxprice'] : 0;       
        $formValues['ct_objects'] = (!empty($formValues['ct_objects'])) ? $formValues['ct_objects'] : 0;       

        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }

    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues[$this->id])->current();
        $row->setFromArray($formValues);

        if($row)
        {
            $row->ct_lat = (!empty($row->ct_lat)) ? $row->ct_lat : 0;            
            $row->ct_lng = (!empty($row->ct_lng)) ? $row->ct_lng : 0;            
            $row->ct_minprice = (!empty($row->ct_minprice)) ? $row->ct_minprice : 0;            
            $row->ct_maxprice = (!empty($row->ct_maxprice)) ? $row->ct_maxprice : 0;            
            $row->ct_objects = (!empty($row->ct_objects)) ? $row->ct_objects : 0;            

            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        $db = Zend_Registry::get( 'db' );

        if(count($items_id) > 0)
        {
          
            $items_ids_str = implode(",", $items_id );

            $query = 'DELETE FROM ct_city WHERE ct_id IN ('.$items_ids_str.')';
            $data = $db->query($query);
        }
    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    if($field == "mdl_br_id")
                    {
                        $mdl_br_ids[] = $row->$field;
                        $mdl_br_ids[] = $value;
                    }

                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }


    ##########################################################################################
    public function getAptTypesMultiselect()
    {
        $res = "";
        $items = array();

        $select =  $this->select()
                    ->where( "apttyp_enable", 1 )
                    ;

        $rows = $this->fetchAll($select)->toArray();
        foreach($rows as $id => $val)
        {
            $items[$val['apttyp_id']] = $val['apttyp_name_' . $this->lang];
        }

        return $items;


    }

}
?>
