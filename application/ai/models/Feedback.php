<?php

	class Ai_Model_Feedback extends Zend_Db_Table_Abstract {

	    ##########################################################################################
		public $_name       = 'fb_feedback';
		public $_primary    = 'fb_id';

	    ##########################################################################################
	    public function init() {

	        $this->db = Zend_Registry::get('db');
	        $locale = new Zend_Session_Namespace('locale');
	        $this->lang = $locale->curlocale['lang'];
	        $this->log = new Ai_Model_Log();
	    }

	    ##########################################################################################
		public function getItems() {

			$select = $this->select();
			$select->order(array('fb_date DESC'));
			return $this->fetchAll($select);
		}

	    ##########################################################################################
		public function getItemsById($u_id) {

			$select = $this->select();
			$select->where('fb_u_id = ?', $u_id);
			$select->order(array('fb_date DESC'));
			return $this->fetchAll($select);
		}

	    ##########################################################################################
		public function reply($params) {

			$this->insert(Array(
				"fb_u_id" => $params['u_id'],
				"fb_text" => $params['text'],
				"fb_direction" => 'answer',
				"fb_date" => date("Y-m-d H:i:s")
				));

			//E-mail пользователя
			$select = $this->select();
			$select->where('fb_u_id = ?', $params['u_id']);
			$select->where('fb_direction = ?', '');
			$select->order(array('fb_date DESC'));
			$select->limit(1);
			$messages = $this->fetchAll($select);
			//print_r($messages);

			//Письмо
			$email = Array();
			$email['to'] = $messages[0]['fb_email'];
			$email['subject'] = 'GOYUG.COM | Ответ службы поддержки';
			$email['body'] = '
			<h2>Ответ службы поддержки GOYUG.COM</h2>
			<h3>Вы писали</h3>
			<p>'.Util::getHumanDate($messages[0]['fb_date'], "-", true, true).' - '.$messages[0]['fb_text'].'</p>
			<h3>Ответ</h3>
			<p>'.Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true).' - '.$params['text'].'</p>';

			//Отправка письма
            $mail = new Zend_Mail('UTF-8');
            $mail->setBodyHTML($email['body']);
            $mail->setFrom('info@goyug.com', 'GoYug.com');
            $mail->setReplyTo('info@goyug.com', 'GoYug.com');
            $mail->addTo($email['to'], '');
            $mail->addBcc(Array('marselos@gmail.com', '160961@mail.ru'));
            $mail->setSubject($email['subject']);
            $mail->send();

            //Ответ метода
			return Array(
				"status" => true,
				"fb_date" => Util::getHumanDate(date("Y-m-d H:i:s"), "-", true, true),
				"fb_text" => $params['text']
				);
		}

	    ##########################################################################################
		public function delete($id) {

			// fetch the user's row
			$row = $this->find($id)->current();

			if($row) {
				$row->delete();
			}else{
				throw new Zend_Exception("Could not delete user. User not found!");
			}
		}

}
