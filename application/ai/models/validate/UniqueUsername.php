<?
class Ai_Validate_UniqueUsername extends Zend_Validate_Abstract
{
    const USERNAME_EXISTS = 'usernameExists';
    
    protected $_messageTemplates = array(
        self::USERNAME_EXISTS =>
        'Username "%value%" already exists in our system',
    );

    public function __construct(Ai_Model_Player $model)
    {
        $this->_model = $model;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        $currentPlayer = isset($context['p_id']) ? $this->_model->getPlayerById($context['p_id']) : null;

        $user = $this->_model->getPlayerByUsername($value, $currentPlayer);
        if (null === $user) {
            return true;
        }

        $this->_error(self::USERNAME_EXISTS);

        return false;

    }
}