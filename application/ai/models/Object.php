<?php
require_once("Util.php");

/**
 * Class and Function List:
 * Function list:
 * - init()
 * - getItems()
 * - getImages()
 * - createItem()
 * - updateItem()
 * - deleteItems()
 * - updateSelected()
 * - updateParameters()
 * - cloneItem()
 * - getValues()
 * - getParamValues()
 * - getMultiselect()
 * - getParamsFields()
 * - setValues()
 * - getObjecttypeCode()
 * - getObjecttypeNames()
 * - getNextID()
 * - ajaxGetObjectPrices()
 * Classes list:
 * - Ai_Model_Object extends Zend_Db_Table_Abstract
 */
class Ai_Model_Object extends Zend_Db_Table_Abstract {

    public $_name = 'obj_object';
    public $_primary = 'obj_id';

    ##########################################################################################
    public function init() {
        $this->auth = Zend_Auth::getInstance();
        $this->db = Zend_Registry::get('db');
        $locale = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref = "obj_";
        $this->id = $this->pref . 'id';
        $this->name = $this->pref . 'name_' . $this->lang;

        $this->ucid = "object";

        $this->log = new Ai_Model_Log();
    }

    ##########################################################################################
    public function getItems() {

        $select = $this->select();
        $items = $this->fetchAll($select)->toArray();

        $sql_where = "";

        if($this->auth->getIdentity()->u_role == "editor")
        {
            // $sql_where = "WHERE obj_creater = '".$this->auth->getIdentity()->u_username."'";
        }

        $items = $this->db->query("
            SELECT *
            FROM obj_object AS obj
            LEFT JOIN u_user AS u
            ON obj.obj_u_id = u.u_id
            ORDER BY obj_create DESC
            LIMIT 50
            " . $sql_where . ""
        )->fetchAll();

        foreach ($items as $id => $val) {

            $obj_typenames_arr = $this->getValues(array(
                'ot_object_type'
            ) , $val['obj_id']);

            if (count($obj_typenames_arr) > 0) {
                $obj_typenames_arr = $obj_typenames_arr['ot_object_type'];
                $items[$id]['obj_objtypename'] = implode(", ", $this->getObjecttypeNames($obj_typenames_arr));
            } else {
                $items[$id]['obj_objtypename'] = "";
            }
        }

        return $items;
    }

    ##########################################################################################
    public function getImages($object_id = 0) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));

        $items = array();

        if ($object_id > 0) {
            $items = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array(
                'f_files'
            ) , array(
                'f_name'
            ))
                ->where('f_ucid = ?', 'object')
                ->where('f_uid = ?', $object_id))->toArray();
        }

        return $items;
    }

    ##########################################################################################
    public function createItem($formValues) {
        $rowItem = $this->createRow($formValues);
        if ($rowItem) {

            $rowItem->obj_create = date('Y-m-d H:i:s');
            $rowItem->obj_update = date('Y-m-d H:i:s');

            $row->obj_creater = $this->auth->getIdentity()->u_username;
            $row->obj_updater = $this->auth->getIdentity()->u_username;
            $row->obj_uid = Util::generateUID();

            $rowItem->save();

            $formValues['obj_id'] = $rowItem->obj_id;

            $this->updateSelected(array(
                'ot_object_type',
                'rt_rent_type',
                'srv_service',
                'at_amenity',
                'eq_equipment',
                'rst_restriction'
            ) , $formValues);

            $this->updateParameters(array(
                'prm_parameter'
            ) , $formValues);


            $this->log->write( array('status' => 'success', 'result' => 'create'), $formValues );

            return $rowItem;
        } else {
            throw new Zend_Exception("Could not create item!");
        }
    }

    ##########################################################################################
    public function updateItem($formValues) {

        if (isset($formValues['obj_id']) && !empty($formValues['obj_id'])) {

            $row = $this->find($formValues['obj_id'])->current();
            $row->setFromArray($formValues);

            //--------------- Logger--------------------
            $this->log->write( array('status' => 'success', 'result' => 'update'), $formValues );

        } else {

            $row = $this->createRow($formValues);
            $row->obj_create = date('Y-m-d H:i:s');
            $formValues['obj_create'] = date('d.m.Y H:i:s');

            $row->obj_creater = $this->auth->getIdentity()->u_username;
            $row->obj_uid = Util::generateUID();

            //--------------- Logger--------------------
            $this->log->write( array('status' => 'success', 'result' => 'create'), $formValues );

        }

        if ($row) {

            $row->obj_update = $formValues['obj_update'] = date('Y-m-d H:i:s');
            $formValues['obj_update'] = date('d.m.Y H:i:s');

            $row->obj_updater = $this->auth->getIdentity()->u_username;

            //Проверки
            $null_fields_check = array(
                "obj_rooms",
                "obj_bedrooms",
                "obj_bathrooms",
                "obj_beds_sngl",
                "obj_beds_dbl",
                "obj_guests",
                "obj_order",
                "obj_sqr",
                "obj_addr_floor",
                "obj_addr_lon",
                "obj_addr_lon_view",
                "obj_addr_lat",
                "obj_addr_lat_view",
                "obj_price",
                "obj_price_minstay",
                "obj_rating",
                "obj_deposit",
            );

            foreach($null_fields_check as $check_field)
            {
                $row->{$check_field} = (!empty($row->{$check_field})) ? $row->{$check_field} : 0;
            }

            // Transliterate City Name
            // $row->obj_addr_city_uid = $ct_uid = Util::Rus2Lat( $formValues['obj_addr_city'] );

            // $city_bad = array(
            //     "jeleznovodsk",
            //     "astrahan",
            //     "bolshoysochi",
            //     "voronej",
            //     "ustjlabinsk",
            // );
            // $city_good   = array(
            //     "zheleznovodsk",
            //     "astrakhan",
            //     "sochi",
            //     "voronezh",
            //     "ustlabinsk",
            // );

            // $row->obj_addr_city_uid = str_replace($city_bad, $city_good, $row->obj_addr_city_uid);
            // $formValues['obj_addr_city_uid'] = $row->obj_addr_city_uid;


            $formValues['obj_id'] = $row->save();

            //13.11.2013 Исключили справочник - rt_rent_type
            $this->updateSelected(array(
                'ot_object_type',
                'srv_service',
                'at_amenity',
                'eq_equipment',
                'rst_restriction',
                // 'ot_smartfilters'
            ) , $formValues);

            $this->updateParameters(array(
                'prm_parameter'
            ) , $formValues);


            $obj_pos = array(
                'lat' => $formValues['obj_addr_lat'],
                'lng' => $formValues['obj_addr_lon'],
            );

            // Update Stats
            /*
            if(isset($formValues["sf_smartfilters"])) {
                $this->updateSmartfiltersObjects(
                    $formValues["obj_id"],
                    $formValues["obj_enable"],
                    $formValues["sf_smartfilters"]
                );
                $this->updateSmartfiltersStat($formValues["sf_smartfilters"]);
            }
            */
            // Update ObjectType Statistic & City Statistic
            $this->updateObjtypesStat($formValues["ot_object_type"]);

            if(isset($ct_uid)){
                $this->updateCityStat(
                    $ct_uid,
                    $formValues['obj_addr_city'],
                    $formValues['obj_price'],
                    $obj_pos
                );
            }

            return $formValues;
        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }
    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if (count($items_id) > 0)
        {
            foreach ($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();

                if ($row)
                {
                    $obj_addr_city_uid =  $row->obj_addr_city_uid;

                    //------------------------------- Удаляем данные из cal_calendar
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'cal_calendar',
                        'primary' => 'cal_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));

                    //------------------------------- Удаляем данные из v_value
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'v_value',
                        'primary' => 'v_id'
                    ));
                    $tbl->delete(array('obj_id = ?' => $item_id));

                    //------------------------------- Удаляем данные из f_files
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'f_files',
                        'primary' => 'f_id'
                    ));
                    $tbl->delete(array('f_uid = ?' => $item_id, 'f_ucid = ?' => "object"));

                    //------------------------------- Удаляем объект
                    $row->delete();

                    //------------------------------- Удаляем папку с фотками
                    $remove_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $this->ucid . '/' . $item_id;
                    Util::rmdir_recursive($remove_dir);

                    //------------------------------- Пересчитываем статистику городов
                    $this->updateCityStat($obj_addr_city_uid);

                } else throw new Zend_Exception("Could not delete item. Item not found!");
            }
        }
    }

    ##########################################################################################
    public function updateSmartfiltersStat($sf_smartfilters)
    {

        $tbl = new Zend_Db_Table(array('name' => 'sf_smartfilters', 'primary' => 'sf_id'));

        if(count($sf_smartfilters))
        {
            foreach ($sf_smartfilters as $id => $key)
            {

                $sf_id = $key;
                $sf_price_range = $this->getSmarfilterPriceRange($sf_id);
                $sf_objects = $this->getSmarfilterObjCount($sf_id);

                $row = $tbl->find($sf_id)->current();
                if ($row)
                {
                    $row->sf_minprice = $sf_price_range["minprice"];
                    $row->sf_maxprice = $sf_price_range["maxprice"];
                    $row->per_id = $sf_price_range["per_id"];
                    $row->sf_objects = $sf_objects;
                }
                $row->save();

            }
        }
    }

    ##########################################################################################
    public function updateObjtypesStat($ot_objtypes) {

        $tbl = new Zend_Db_Table(array('name' => 'ot_object_type', 'primary' => 'ot_id'));

        if (count($ot_objtypes))
        {
            foreach ($ot_objtypes as $id => $key)
            {

                $ot_id = $key;
                $ot_price_range = $this->getObjtypePriceRange($ot_id);

                $row = $tbl->find($ot_id)->current();
                if ($row)
                {
                    $row->ot_minprice = $ot_price_range["minprice"];
                    $row->ot_maxprice = $ot_price_range["maxprice"];
                }
                $row->save();

            }
        }
    }

    ##########################################################################################
    public function updateCityStat($obj_addr_city_uid, $obj_addr_city = "", $obj_price = 0, $obj_pos = array())
    {

        $tbl = new Zend_Db_Table(array('name' => 'ct_city', 'primary' => 'ct_id'));
        $venue_path = $_SERVER['DOCUMENT_ROOT'] . '/data/venue/';

        if(!empty($obj_addr_city_uid))
        {
            $row = $tbl->fetchRow($tbl->select()->where('ct_uid = ?', $obj_addr_city_uid));

            // If City exist
            if ($row)
            {
                $ct_price_range = $this->getCityPriceRange($obj_addr_city_uid);
                $ct_objects = $this->getCityObjCount($obj_addr_city_uid);

                $row->ct_minprice = $ct_price_range["minprice"];
                $row->ct_maxprice = $ct_price_range["maxprice"];
                $row->ct_objects = $ct_objects;

                if($ct_objects > 0)
                    $row->save();
                else
                    $row->delete();

            // If City NOT exist
            } else {

                $newRow = $tbl->createRow();
                $newRow->ct_pid = 0;
                $newRow->ct_enable = 1;
                $newRow->ct_uid = $obj_addr_city_uid;
                $newRow->ct_name_ru = $obj_addr_city;
                $newRow->ct_name_en = $obj_addr_city_uid;

                $newRow->ct_lat = $obj_pos['lat'];
                $newRow->ct_lng = $obj_pos['lng'];

                $newRow->ct_minprice = $obj_price;
                $newRow->ct_maxprice = $obj_price;

                $newRow->ct_objects = 1;

                $newRow->ct_create = date('Y-m-d H:i:s');
                $newRow->ct_update = date('Y-m-d H:i:s');

                $newRow->ct_creater = $this->auth->getIdentity()->u_username;
                $newRow->ct_updater = $this->auth->getIdentity()->u_username;

                $newRow->save();

            }

            // Update Cities JSON file
            $query = $this->db->query("
                SELECT ct_uid, ct_name_ru, ct_minprice, ct_maxprice, ct_objects
                FROM ct_city AS ct
                WHERE ct_enable = 1
            ");

            $query->setFetchMode(Zend_Db::FETCH_OBJ);
            $rows = $query->fetchAll();

            $venues_data = array();

            foreach ($rows as $id => $val)
            {

                // Generate Obj Data Files
                $obj_data = array(
                    'uid' => $val->ct_uid,
                    'name' => $val->ct_name_ru,

                );
                $obj_json = json_encode($obj_data);
            }




        }
    }

    ##########################################################################################
    public function updateSmartfiltersObjects($obj_id, $obj_enable, $sf_smartfilters) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'sfo_smartfilters_objects',
            'primary' => 'sfo_id'
        ));

        $tbl->delete(array(
            'obj_id = ?' => $obj_id
        ));

        if (count($sf_smartfilters))
        {
            foreach ($sf_smartfilters as $id => $key) {

                //Update sfo_smartfilters_objects
                $row = $tbl->createRow();
                if ($row) {
                    $row->obj_id = $obj_id;
                    $row->obj_enable = $obj_enable;
                    $row->sf_id = $key;
                    $row->save();
                } else {
                    throw new Zend_Exception("Could not create prv_prm_values row!");
                }

            }
            //foreach - $values
        }
        // if - $values
    }

    ##########################################################################################
    public function getSmarfilterObjCount($sf_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($sf_id) && $sf_id > 0)
        {
            $data = $db->query("
                SELECT COUNT(obj.obj_id) AS obj_count
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.sf_id = " . $sf_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();
        }

        return $data[0]["obj_count"];
    }

    ##########################################################################################
    public function getCityObjCount($ct_uid) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ct_uid))
        {
            $data = $db->query("
                SELECT COUNT(obj.obj_id) AS obj_count
                FROM obj_object AS obj
                WHERE obj.obj_addr_city_uid = '" . $ct_uid ."'
                AND obj.obj_enable = 1"
            )->fetchAll();

            return $data[0]["obj_count"];
        } else {
            return;
        }

    }

    ##########################################################################################
    public function getSmarfilterPriceRange($ot_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ot_id) && $ot_id > 0)
        {
            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice, obj.obj_price_period AS per_id
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.ot_id = " . $ot_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();
        }

        return $data[0];
    }

    ##########################################################################################
    public function getObjtypePriceRange($ot_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ot_id) && $ot_id > 0)
        {
            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice
                FROM v_value AS v
                LEFT JOIN obj_object AS obj
                ON v.obj_id = obj.obj_id
                WHERE v.v_table = 'ot_object_type'
                AND v.v_key = " . $ot_id ."
                "
            )->fetchAll();
        }

        return $data[0];
    }

    ##########################################################################################
    public function getCityPriceRange($ct_uid) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ct_uid))
        {
            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice
                FROM obj_object AS obj
                WHERE obj.obj_addr_city_uid = '" . $ct_uid . "'
                "
            )->fetchAll();

            return $data[0];

        } else {
            return;
        }

    }

    ##########################################################################################
    public function getObjectCalendar($obj_id) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($obj_id) && $obj_id > 0)
        {
            $data = $db->query("
                SELECT *, DATEDIFF(cal.cal_enddate, cal.cal_startdate) AS cal_days
                FROM cal_calendar AS cal
                LEFT JOIN obj_object AS obj
                ON cal.obj_id = obj.obj_id
                WHERE cal.obj_id = " . $obj_id ."
                AND cal.cal_enable = 1
                ORDER BY cal_startdate DESC"
            )->fetchAll();
        }

        return $data;
    }

    ##########################################################################################
    public function updateSelected($tables = array() , $values = array()) {
        $item_id = $values['obj_id'];

        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));

                if (isset($values[$table]) && count($values[$table])) {
                    foreach ($values[$table] as $id => $key) {
                        $row = $tbl->createRow();
                        if ($row) {

                            $row->v_table = $table;
                            $row->obj_id = $item_id;
                            $row->v_key = $key;
                            $row->save();
                        } else {
                            throw new Zend_Exception("Could not create prv_prm_values row!");
                        }
                    }
                    //foreach - $values
                }
                // if - $values
            }
            //foreach - $tables
        }
        //if - $tables
    }

    ##########################################################################################
    public function updateParameters($tables = array() , $values = array()) {
        $item_id = $values['obj_id'];

        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        if (count($tables)) {
            foreach ($tables as $table) {
                $tbl->delete(array(
                    'obj_id = ?' => $item_id,
                    'v_table = ?' => $table
                ));

                foreach (array_keys($values) as $key) {
                    if (preg_match('/^' . $table . '_\d+/', $key, $matches)) {
                        $v_key = str_replace($table . '_', "", $key);

                        if (!empty($values[$key])) {
                            $row = $tbl->createRow();
                            if ($row) {

                                $row->v_table = $table;
                                $row->obj_id = $item_id;
                                $row->v_key = $v_key;
                                $row->v_val = $values[$key];
                                $row->save();
                            } else {
                                throw new Zend_Exception("Could not create prv_prm_values row!");
                            }
                        }

                        //if


                    }

                    //if - preg_match


                }

                //foreach


            }

            //foreach - $tables


        }

        //if - $tables


    }

    ##########################################################################################
    public function cloneItem($id) {
        if ($id > 0) {
            $row = $this
                ->find($id)
                ->current()
                ->toArray();
            unset($row[$this->id]);
            $row[$this->name].= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function getValues($tables = array() , $obj_id) {

        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        foreach ($tables as $table) {
            $rows = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array('v_value') , array('v_key'))
                ->where('obj_id = ?', $obj_id)
                ->where('v_table = ?', $table))->toArray();

            foreach ($rows as $id => $val) {
                $items[$table][] = $val['v_key'];
            }
        }

        return $items;
    }

    ##########################################################################################
    public function getSmartfiltersValues($obj_id) {

        $items = array();

        $tbl = new Zend_Db_Table(array( 'name' => 'sfo_smartfilters_objects', 'primary' => 'sfo_id' ));

        if(!empty($obj_id))
        {
            $rows = $tbl
                ->fetchAll(
                    $tbl->select()
                    ->from(array('sfo_smartfilters_objects') , array('sf_id'))
                    ->where('obj_id = ?', $obj_id)
                )->toArray();

                foreach ($rows as $id => $val) {
                    $items["sf_smartfilters"][] = $val['sf_id'];
                }
        }

        return $items;

    }

    ##########################################################################################
    public function getParamValues($tables = array() , $obj_id) {

        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'v_value',
            'primary' => 'v_id'
        ));

        foreach ($tables as $table) {
            $rows = $tbl
                ->fetchAll($tbl
                ->select()
                ->from(array('v_value') , array('v_key','v_val'))
                ->where('obj_id = ?', $obj_id)
                ->where('v_table = ?', $table))->toArray();

            foreach ($rows as $id => $val) {
                $items[$table . '_' . $val['v_key']] = $val['v_val'];
            }
        }

        return $items;
    }

    ##########################################################################################
    public function getMultiselect($table, $prefix, $order = array()) {

        $items = array();

        $tbl = new Zend_Db_Table(array(
            'name' => $table,
            'primary' => $prefix . '_id'
        ));

        $select = $tbl->select()->from(array($table) , array($prefix . '_id', $prefix . '_name_' . $this->lang))->where($prefix . '_enable = ?', 1);

        if (!empty($order)) $select->order($order);
        else {

            $select
                ->order($prefix . '_order ASC')
                ->order($prefix . '_name_' . $this->lang . ' ASC');
        }

        $rows = $tbl
            ->fetchAll($select)->toArray();

        foreach ($rows as $id => $val) {
            $items[$val[$prefix . '_id']] = $val[$prefix . '_name_' . $this->lang];
        }

        return $items;
    }

    ##########################################################################################
    public function getOwners() {

        $sql_where = "";

        if($this->auth->getIdentity()->u_role == "editor")
        {
            // $sql_where = " AND u.u_creater = '".$this->auth->getIdentity()->u_username."' ";
        }


        $items = array();

        $db = Zend_Registry::get( 'db' );

        $rows = $db->query("
            SELECT *
            FROM u_user AS u
            WHERE u.u_active = 1
            AND u.u_role = 'owner'
            " . $sql_where . "
            ORDER BY u.u_lastname"
        )->fetchAll();

        $items[0] = "";
        foreach ($rows as $id => $val) {
            $items[$val['u_id']] = $val['u_lastname'] . " " . $val['u_firstname']  . " - " . $val['u_phone_1']  . " - " . $val['u_email'];
        }

        return $items;
    }

    ##########################################################################################
    public function getSmartfiltersMultiselect() {

        $items = array();

        $db = Zend_Registry::get( 'db' );

        $rows = $db->query("
            SELECT sf.sf_id, sf.sf_name_" . $this->lang . ", ot.ot_name_" . $this->lang . "
            FROM sf_smartfilters AS sf
            LEFT JOIN ot_object_type AS ot
            ON sf.ot_id = ot.ot_id
            WHERE sf.sf_enable = 1
            ORDER BY sf.sf_level ASC, sf.sf_order ASC"
        )->fetchAll();

        foreach ($rows as $id => $val) {
            $items[$val['sf_id']] = $val["ot_name_" . $this->lang] . " - " . $val['sf_name_' . $this->lang];
        }

        return $items;
    }

    ##########################################################################################
    public function getParamsFields($table, $prefix) {
        $items = array();

        $tbl = new Zend_Db_Table(array(
            'name' => $table,
            'primary' => $prefix . '_id'
        ));

        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            $table
        ) , array(
            $prefix . '_id',
            $prefix . '_name_' . $this
                ->lang,
            $prefix . '_unit_' . $this
                ->lang,
            $prefix . '_type'
        ))
            ->where($prefix . '_enable = ?', 1)
            ->order($prefix . '_order ASC')
            ->order($prefix . '_name_' . $this
            ->lang . ' ASC'))
            ->toArray();

        foreach ($rows as $id => $val) {
            $items[$val[$prefix . '_id']] = array(
                'name' => $val[$prefix . '_name_' . $this
                    ->lang],
                'type' => $val[$prefix . '_type'],
                'unit' => $val[$prefix . '_unit_' . $this->lang],
            );
        }

        return $items;
    }

    ##########################################################################################
    public function setValues($items, $values) {
        $items = (count($items) == 1 && count($items) > 0) ? array(
            $items
        ) : $items;

        $mdl_br_ids = array();

        foreach ($items as $id => $item_id) {
            $row = $this->find($item_id)->current();
            if (count($values) > 0) {
                foreach ($values as $field => $value) {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if (count($mdl_br_ids) > 0) $this->updateBrands($mdl_br_ids);
    }

    ##########################################################################################
    public function getObjecttypeCode($values) {
        $items = "";
        $tbl = new Zend_Db_Table(array(
            'name' => 'ot_object_type',
            'primary' => 'ot_id'
        ));

        $rows = $tbl
            ->fetchAll(
                $tbl->select()
                ->from(array('ot_object_type') , array('ot_code'))
                ->where('ot_id IN (?)', array($values))
            )->toArray();

        foreach ($rows as $id => $val) {
            $items.= $val['ot_code'];
        }

        return $items;
    }

    ##########################################################################################
    public function getSmartsByObjecttype($ot_ids = array()) {

        $items = array();

        $db = Zend_Registry::get( 'db' );

        // print_r($values); die;

        if(count($ot_ids) > 0 && !empty($ot_ids))
        {
            $rows = $db->query("
                SELECT sf.sf_id, sf.sf_name_" . $this->lang . ", ot.ot_name_" . $this->lang . "
                FROM sf_smartfilters AS sf
                LEFT JOIN ot_object_type AS ot
                ON sf.ot_id = ot.ot_id
                WHERE sf.sf_enable = 1
                AND sf.ot_id IN (" . implode(",", $ot_ids) . ")
                ORDER BY sf.sf_level ASC, sf.sf_order ASC"
            )->fetchAll();

            foreach ($rows as $id => $val) {
                $items[$val['sf_id']] = $val["ot_name_" . $this->lang] . " - " . $val['sf_name_' . $this->lang];
            }
        }

        return $items;
    }

    ##########################################################################################
    public function getObjecttypeNames($values) {
        $items = array();
        $tbl = new Zend_Db_Table(array(
            'name' => 'ot_object_type',
            'primary' => 'ot_id'
        ));

        $rows = $tbl
            ->fetchAll($tbl
            ->select()
            ->from(array(
            'ot_object_type'
        ) , array(
            'ot_name_ru'
        ))
            ->where('ot_id IN (?)', $values))->toArray();

        foreach ($rows as $id => $val) {
            $items[] = $val['ot_name_ru'];
        }

        return $items;
    }

    ##########################################################################################
    public function getNextID() {
        $res = $this
            ->getAdapter()
            ->query("SHOW TABLE STATUS LIKE 'obj_object';");
        $item = $res->fetchObject();

        return $item->Auto_increment;
    }

    ##########################################################################################
    public function ajaxGetObjectPrices($params) {
        $obj_id = $params['obj_id'];
        $prc_tbl = new Zend_Db_Table(array(
            'name' => 'prc_price',
            'primary' => 'prc_id'
        ));

        $rows = $prc_tbl
            ->fetchAll($prc_tbl
            ->select()
            ->from(array(
            'prc_price'
        ) , array(
            'per_id',
            'prc_min_period',
            'prc_price'
        ))
            ->where('obj_id = ?', $obj_id))->toArray();

        return $rows;
    }

    ##########################################################################################
    public function rebuildItems()
    {
        $db = Zend_Registry::get( 'db' );

        $venue_path = $_SERVER['DOCUMENT_ROOT'] . '/data/venue/';

/*        $query = $db->query("
            SELECT *
            FROM obj_object AS obj

            LEFT JOIN per_period AS per
            ON per.per_id = obj.obj_price_period

            LEFT JOIN v_value AS v
            ON v.obj_id = obj.obj_id

            LEFT JOIN ot_object_type AS ot
            ON ot.ot_id = v.v_key

            LEFT JOIN acc_accommodation AS acc
            ON acc.acc_id = obj.obj_acc_type

            WHERE obj.obj_enable = 1
            AND v.v_table = 'ot_object_type'

        ");

        $query->setFetchMode(Zend_Db::FETCH_OBJ);
        $rows = $query->fetchAll();

        $venues_data = array();

        foreach ($rows as $id => $val)
        {
            $obj = $this->find($val['obj_id'])->current();

            // Generate random UIDs
            $obj->obj_uid = Util::generateUID();

            // Generate translit Citynames
            $obj->obj_addr_city_uid = Util::Rus2Lat( $obj->obj_addr_city );

            $obj->save();

            // Generate Obj Data Files
            $obj_data = array(
                'id' => $val->obj_id,
                'uid' => $val->obj_uid,
                'name' => $val->obj_name_ru,
                'params' => array(
                    'guests' => $val->obj_guests,
                    'sqr' => $val->obj_sqr,
                    'rooms' => $val->obj_rooms,
                    'price' => $val->obj_price,
                    'stayperiod' => $val->per_pricename_ru,
                ),
                'loc' => array(
                    'lan' => $val->obj_addr_lon,
                    'lng' => $val->obj_addr_lat,
                ),
                'address' => array(
                    'city_uid' => $val->obj_addr_city_uid,
                    'city' => $val->obj_addr_city,
                ),
            );
            $obj_json = json_encode($obj_data);

            $obj_file = fopen($venue_path . $val->obj_uid, "w");
            fwrite($obj_file, $obj_json);
            fclose($obj_file);

            $venues_arr[$val->obj_addr_city_uid][] = array(
                $val->obj_uid,
                round($val->obj_addr_lat, 6),
                round($val->obj_addr_lon, 6),
                "3000",
                "1"
            );

            $venues_arr_all[] = array(
                $val->obj_uid,
                round($val->obj_addr_lat, 6),
                round($val->obj_addr_lon, 6),
                "3000",
                "1"
            );

            $this->updateCityStat(
                $obj->obj_addr_city,
                $obj->obj_addr_city_uid,
                $obj->obj_price
            );

        }

        foreach ($venues_arr as $city_uid => $val)
        {
            $venues_json = "venues = " . json_encode($val);
            $venues_file = fopen($venue_path . "_" . $city_uid . ".js", "w");
            fwrite($venues_file, $venues_json);
            fclose($venues_file);
        }


        $venues_json = "venues = " . json_encode($venues_arr_all);
        $venues_file = fopen($venue_path . "_all.js", "w");
        fwrite($venues_file, $venues_json);
        fclose($venues_file);*/

        $query = $db->query("
            SELECT *
            FROM ct_city AS ct
            WHERE ct_enable = 1
        ");

        $query->setFetchMode(Zend_Db::FETCH_OBJ);
        $rows = $query->fetchAll();

        $cities_arr = array();

        foreach ($rows as $id => $val)
        {
            $query_obj = $db->query("
                SELECT obj_addr_lat, obj_addr_lon
                FROM obj_object
                WHERE obj_addr_city_uid = '".$val->ct_uid."'
                AND obj_enable = 1
                LIMIT 1
            ");

            $query_obj->setFetchMode(Zend_Db::FETCH_OBJ);
            $obj = $query_obj->fetchAll();

            // print_r($obj[0]->obj_addr_lon);die;

            $ct_lat = round($obj[0]->obj_addr_lat, 6);
            $ct_lng = round($obj[0]->obj_addr_lon, 6);

            $db->query("
                UPDATE ct_city
                SET ct_lat = ".$ct_lat.", ct_lng = ".$ct_lng."
                WHERE ct_uid = '".$val->ct_uid."'
            ");


            // Generate Obj Data Files
            $cities_arr[] = array(
                'uid' => $val->ct_uid,
                'name' => $val->ct_name_ru,
                'lat' => $ct_lat,
                'lng' => $ct_lng,
                'minprice' => $val->ct_minprice,
                'maxprice' => $val->ct_maxprice,
                'objects' => $val->ct_objects,

            );
            // $cities_json = json_encode($cities_arr);

            // $cities_file = fopen($venue_path . "__cities.js", "w");
            // fwrite($cities_file, $cities_json);
            // fclose($cities_file);


        }



        return "OK";
    }


    ##########################################################################################
    public function getNotModeratedObjects() {

        $query = $this->db->query("
            SELECT *
            FROM obj_object AS obj
            LEFT JOIN u_user AS u
            ON obj.obj_u_id = u.u_id
            WHERE obj_moderated = 0
        ");

        $rows = $query->fetchAll();

        return $rows;
    }

    ##########################################################################################
    public function getOwnerObjectByObjID($obj_id) {

        $result = array();

        if(!empty($obj_id))
        {
            $rows = $this->db->query("
                SELECT obj.obj_name_ru, u.u_firstname, u.u_lastname, u.u_phone_1, u.u_email, obj.obj_addr_city
                FROM obj_object AS obj
                LEFT JOIN u_user AS u
                ON obj.obj_u_id = u.u_id
                WHERE obj.obj_id = '".$obj_id."'
            ")->fetchAll();

        }

        $result = $rows[0];

        return $result;
    }

//#########################################################################################
    public function getCities()
    {
        $result = array();

        $data = $this->db->query("
            SELECT *
            FROM ct_city
            WHERE ct_enable = 1
            ORDER BY ct_name_ru
        ")->fetchAll();

        foreach($data as $d_id => $d_val)
        {
            $result["list"][ $d_val["ct_uid"] ] = array(
                "id" => $d_val["ct_id"],
                "uid" => $d_val["ct_uid"],
                "name" => $d_val["ct_name_ru"],
                "name_pr" => $d_val["ct_name_ru_pr"],
                "objects" => $d_val["ct_objects"],
                "lat" => $d_val["ct_lat"],
                "lng" => $d_val["ct_lng"],
                "minprice" => $d_val["ct_minprice"],
                "maxprice" => $d_val["ct_maxprice"],
                "maplink" => "http://" . $d_val["ct_uid"] . "." . $_SERVER['HOSTNAME'],
                "yandex_web" => $d_val["ct_yandex_webmaster"],
            );

            $result["alphabet"][ mb_substr($d_val["ct_name_ru"], 0, 1, 'utf-8') ][] = $d_val["ct_uid"];
        }

        ksort($result["alphabet"]);

        return $result;
    }
}
?>
