<?php
require_once("Util.php");
require_once("nokogiri.php");

class Ai_Model_Parser extends Zend_Db_Table_Abstract {

    //#########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        $this->mobDomain = "https://m.avito.ru";
        $this->domain = "https://www.avito.ru";
        $this->workURL = "/kvartiry/sdam/posutochno";
        $this->obj_perpage = 50;
        $this->obj_mob_perpage = 20;
        $this->img_path = $_SERVER['DOCUMENT_ROOT'] . "/upload/avito";
    }

    //#########################################################################################
    public function getParseParams($pars_city = "")
    {
        $result = array();
        $url = $this->domain . "/" . $pars_city . $this->workURL;
        
        $html = $this->get_data($url);
        $saw = new nokogiri($html);
        $data = $saw->get('.breadcrumbs-link-count')->toArray();

        if(isset($data[0]['#text'][0]))
        {
            $result['objects'] = trim(preg_replace("/[^0-9]/", "", $data[0]['#text'][0]));
            $result['pages'] =  ceil($result['objects'] / $this->obj_perpage);
        } else {
            $data = $saw->get('title')->toArray();
            print_r($data[0]['#text'][0]);
        }

        return $result;
    }

    //#########################################################################################
    public function getPayers($pars_city = "")
    {
        if(!empty($pars_city))
        {
            //Запрос
            $query = "
                SELECT pao_id, pao_link, pao_uid
                FROM pao_avito_object AS pao
                WHERE pao.pao_addr_city_uid = '" . $pars_city ."' 
                AND (pao_is_vip = 1 OR pao_is_highlight = 1 OR pao_is_premium = 1 OR pao_is_up = 1)
            ";

            $data = $this->db->query($query)->fetchAll();

            if(count($data) > 0)
            {
                foreach($data as $id => $val)
                {
                    $result[] = array(
                        "id" => $val['pao_id'],
                        "uid" => $val['pao_uid'],
                        "link" => $val['pao_link'],
                    );
                }
            }

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    public function parsePayers($pars_city, $payers, $task_token)
    {
        if(!empty($pars_city) && count($payers) > 0)
        {
            $result = array();

            foreach($payers as $i => $val)
            {

                $pao_id = $val['id'];
                $pao_uid = $val['uid'];
                $pao_link = $val['link'];

                // Парсим карточку об-та (моб. версия)
                //  ============================================================
                $url = preg_replace("/www/", "m", $pao_link);
                $html = $this->get_data($url);
                $saw = new nokogiri($html);        

                if($html == '403')
                {
                    $result = array("status" => "error", "message" => "403 Forbidden");
                    return $result;
                }

                //  Парсим токен для телефона
                //  ============================================================
                $phone_token = $saw->get('.action-show-number')->toArray();
                $phone_token = explode("/", $phone_token[0]['href']);
                $phone_token = $phone_token[count($phone_token)-1];

                //  Парсим телефон
                //  ============================================================
                $phone = $this->getAvitoPhone($pao_link, $phone_token);
                $result[$pao_id]['phone'] = (!empty($phone)) ? Util::stripTel($phone) : 0;


                //  Проверяем есть ли такой владелец в бвзе GOYUG
                //  ============================================================
                $result[$pao_id]['is_goyug_owner'] = $this->checkGoyugPhoneExist($result[$pao_id]['phone']);

                //  Парсим имя владельца
                //  ============================================================
                $owner = $saw->get('.person-name')->toArray();
                $result[$pao_id]['owner'] = trim($owner[0]['#text'][0]);

                //  Парсим фотки
                //  ============================================================
                $obj_imgs = $saw->get('meta[property="og:image"]')->toArray();
                
                if(count($obj_imgs) > 0)
                {
                    foreach($obj_imgs as $oi_id => $oi_val)
                    {
                        if(isset($oi_val['content']))
                        {
                            $result[$pao_id]['imgs'][] = preg_replace("/https/", "http", $oi_val['content']);
                        } 
                    }

                    $result[$pao_id]['imgs'] = implode(";", $result[$pao_id]['imgs']);
                } else {
                    $result[$pao_id]['imgs'] = "";
                }

                //  Парсим описание
                //  ============================================================
                $obj_desc = $saw->get('.description-preview-wrapper p')->toArray();
                $result[$pao_id]['desc'] = (isset($obj_desc[0]['#text'][0])) ? $obj_desc[0]['#text'][0] : "";

                //  Парсим координаты
                //  ============================================================
                $obj_loc = $saw->get('.item-map')->toArray();
            
                $result[$pao_id]['lat'] = (isset($obj_loc[0])) ? round(floatval($obj_loc[0]['data-coords-lat']), 6) : 0;
                $result[$pao_id]['lng'] = (isset($obj_loc[0])) ? round(floatval($obj_loc[0]['data-coords-lng']), 6) : 0;


                // Обновляем статус задачи
                //  ============================================================
                if(!empty($task_token))
                {
                    $query = "
                        UPDATE pt_parse_tasks
                        SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                        WHERE pt_token = '" . $task_token ."'
                    ";
                    $this->db->query($query);         
                }

                // Дополняем инфу объекта
                //  ============================================================
                $tbl = new Zend_Db_Table(array(
                    'name' => 'pao_avito_object',
                    'primary' => 'pao_id'
                ));
                $row = $tbl->find($pao_id)->current();
                if ($row)
                {
                    $row->pao_pau_id = $result[$pao_id]['phone'];
                    $row->pao_desc = $result[$pao_id]['desc'];
                    $row->pao_images = $result[$pao_id]['imgs'];
                    $row->pao_addr_lat = $result[$pao_id]['lat'];
                    $row->pao_addr_lng = $result[$pao_id]['lng'];
                }
                $row->save();



                // Пишем а базу владельца если его нет в базе спарсенных
                //  ============================================================
                if(!$this->checkAvitoPhoneExist($result[$pao_id]['phone']))
                {
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'pau_avito_user',
                        'primary' => 'pau_id'
                    ));

                    $row = $tbl->createRow(
                        array(
                          'pau_is_goyug' => $result[$pao_id]['is_goyug_owner'],
                          'pau_is_payer' => 1,
                          'pau_phone' => $result[$pao_id]['phone'],
                          'pau_name' => $result[$pao_id]['owner'],
                          'pau_city' => $pars_city,
                          'pau_create' => date ( 'Y-m-d H:i:s' ),
                          'pau_creater' => 'parser_avito'
                        )
                    );
                    $row->save();
                }

                // Тормозим парсинг на случайное кол-во секунд
                //  ============================================================
                $sleep_sec = mt_rand(1, 10) * 1000000;
                usleep($sleep_sec);

            } // foreach payers array

            return $result;

        } // if payers array NOT empty
        else {
            return 0;
        }
    }

    //#########################################################################################
    public function getObjectsList($pars_city = "", $pars_page = 1, $task_token = "")
    {
        $result = array();

        // Составляем URL старнички
        $url = $this->domain . "/" . $pars_city . $this->workURL . "/?p=" . $pars_page;

        // Получаем HTML и инициализиурем парсер
        $html = $this->get_data($url);

        $saw = new nokogiri($html);
        $item_links = $saw->get('.item')->toArray();
        
        if(count($item_links) > 0)
        {
            foreach($item_links as $id => $val)
            {

                // ID об-та
                //  ============================================================
                $result[$id]['uid'] = $val['id'];

                // Ссылка на об-т
                //  ============================================================
                $o_link = $saw->get("#" . $result[$id]['uid'] . " .item-description-title-link")->toArray();
                $result[$id]['link'] = $this->domain . $o_link[0]['href'];

                // Название об-та
                //  ============================================================
                $result[$id]['name'] = trim( $o_link[0]['#text'][0] );

                // Количество комнат
                //  ============================================================
                $o_name_parts = explode(",", $result[$id]['name']);
                $result[$id]['rooms'] = (isset($o_name_parts[0])) ? preg_replace("/[^0-9]/", "", $o_name_parts[0]) : 0;
                $result[$id]['rooms'] = (!empty($result[$id]['rooms'])) ? $result[$id]['rooms'] : 0;

                // Площадь
                //  ============================================================
                $result[$id]['sqr'] = (isset($o_name_parts[1])) ? preg_replace("/[^0-9]/", "", $o_name_parts[1]) : 0;

                // Этаж & Этажность
                //  ============================================================
                if(isset($o_name_parts[2]))
                {
                    $o_floor = explode("/", $o_name_parts[2]);
                    $result[$id]['floor'] = (isset($o_floor[0])) ? preg_replace("/[^0-9]/", "", $o_floor[0]) : 0;
                    $result[$id]['floor'] = (!empty($result[$id]['floor'])) ? $result[$id]['floor'] : 0;
                    
                    $result[$id]['elevation'] = (isset($o_floor[1])) ? preg_replace("/[^0-9]/", "", $o_floor[1]) : 0;
                    $result[$id]['elevation'] = (!empty($result[$id]['elevation'])) ? $result[$id]['elevation'] : 0;
                } else {
                    $result[$id]['floor'] = 0;
                    $result[$id]['elevation'] = 0;   
                }

                // Цена
                //  ============================================================
                $o_about = $saw->get("#" . $result[$id]['uid'] . " .about")->toArray();
                $result[$id]['price'] = (isset($o_about[0]['#text'][0])) ? preg_replace("/[^0-9]/", "", $o_about[0]['#text'][0]) : 0;
                $result[$id]['price'] = (!empty($result[$id]['price'])) ? $result[$id]['price'] : 0;

                // Адрес
                //  ============================================================
                $o_address = $saw->get("#" . $result[$id]['uid'] . " .address")->toArray();
                $result[$id]['addr_street'] = (isset($o_address[0]['#text'][0])) ? trim($o_address[0]['#text'][0]) : 0;

                // Признаки продвижения
                //  ============================================================
                $o_adv = $saw->get("#" . $result[$id]['uid'] . " .vas-applied a i")->toArray();

                $result[$id]['is_vip'] = 0;
                $result[$id]['is_highlight'] = 0;
                $result[$id]['is_premium'] = 0;
                $result[$id]['is_up'] = 0;

                if(count($o_adv) > 0)
                {
                    foreach ($o_adv as $o_adv_key => $o_adv_val) 
                    {
                        if($o_adv_val['class'] == 'i-vas-s-gray-vip i-vas-s-gray')
                        {
                            $result[$id]['is_vip'] = 1;
                        } 
                        elseif($o_adv_val['class'] == 'i-vas-s-gray-highlight i-vas-s-gray')
                        {
                            $result[$id]['is_highlight'] = 1;
                        } 
                        elseif($o_adv_val['class'] == 'i-vas-s-gray-premium i-vas-s-gray')
                        {
                            $result[$id]['is_premium'] = 1;
                        } 
                        elseif($o_adv_val['class'] == 'i-vas-s-gray-up i-vas-s-gray')
                        {
                            $result[$id]['is_up'] = 1;
                        }
                    }

                }

                // Пишем а базу объект, если он не парсился
                // ============================================================
                if(!$this->checkObjectExist($result[$id]['uid'], $pars_city))
                {
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'pao_avito_object',
                        'primary' => 'pao_id'
                    ));

                    $row = $tbl->createRow(
                        array(
                          'pao_pau_id' => '',
                          'pao_is_vip' => $result[$id]['is_vip'],
                          'pao_is_highlight' => $result[$id]['is_highlight'],
                          'pao_is_premium' => $result[$id]['is_premium'],
                          'pao_is_up' => $result[$id]['is_up'],
                          'pao_link' => $result[$id]['link'],
                          'pao_uid' => $result[$id]['uid'],
                          'pao_sqr' => $result[$id]['sqr'],
                          'pao_rooms' => $result[$id]['rooms'],
                          'pao_name' => $result[$id]['name'],
                          'pao_desc' => '',
                          'pao_images' => '',
                          'pao_addr_city_uid' => $pars_city,
                          'pao_addr_street' => $result[$id]['addr_street'],
                          'pao_addr_number' => "",
                          'pao_addr_floor' => $result[$id]['floor'],
                          'pao_addr_elevation' => $result[$id]['elevation'],
                          'pao_addr_lat' => '',
                          'pao_addr_lng' => '',
                          'pao_price' => $result[$id]['price'],
                          'pao_create' => date ( 'Y-m-d H:i:s' ),
                          'pao_creater' => 'parser_avito'
                        )
                    );
                    $row->save();

                // Если объект парсился, обновляем статусы премиальности (вдруг его подняли)
                // ============================================================
                } else {
                    $query = "
                        UPDATE pao_avito_object
                        SET 
                            pao_is_vip = " . $result[$id]['is_vip'] . ",
                            pao_is_highlight = " . $result[$id]['is_highlight'] . ",
                            pao_is_premium = " . $result[$id]['is_premium'] . ",
                            pao_is_up = " . $result[$id]['is_up'] . ",
                            pao_create = '".date ( 'Y-m-d H:i:s' )."'

                        WHERE pao_uid = '" . $result[$id]['uid'] ."'
                    ";
                    $this->db->query($query);         
                }

                // Обновляем статус задачи
                //  ============================================================
                if(!empty($task_token))
                {
                    $query = "
                        UPDATE pt_parse_tasks
                        SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                        WHERE pt_token = '" . $task_token ."'
                    ";
                    $this->db->query($query);         
                }
                

            } // foreach

            // Обновляем статус задачи
            if(!empty($task_token))
            {
                $query = "
                    UPDATE pt_parse_tasks
                    SET pt_cur_page = pt_cur_page +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                    WHERE pt_token = '" . $task_token ."'
                ";
                $this->db->query($query);         
            }

            // Тормозим парсинг на случайное кол-во секунд
            //  ============================================================
            $sleep_sec = mt_rand(1, 5) * 1000000;
            usleep($sleep_sec);

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    public function getObjectsDetails($objects = array(), $city_uid, $task_token = "")
    {
        $auth = Zend_Auth::getInstance();       
        $result = "";

        if(count($objects) > 0 && $objects != 0)
        {
            foreach($objects as $id => $val)
            {

                // $objects[$id]['is_parsed'] = $this->checkObjectExist($val['uid'], $city_uid);

                // Если такого объекта нет в базе - парсим карточку объекта
                // if(!$objects[$id]['is_parsed'])
                // {

                    // Парсим карточку об-та (моб. версия)
                    //  ============================================================
                    $url = preg_replace("/www/", "m", $val['link']);
                    $html = file_get_contents($url);
                    $saw = new nokogiri($html);                

                    //  Парсим токен для телефона
                    //  ============================================================
                    $phone_token = $saw->get('.action-show-number')->toArray();
                    $phone_token = explode("/", $phone_token[0]['href']);
                    $phone_token = $phone_token[count($phone_token)-1];

                    //  Парсим телефон
                    //  ============================================================
                    $phone = $this->getAvitoPhone($val['link'], $phone_token);
                    $objects[$id]['phone'] = (!empty($phone)) ? Util::stripTel($phone) : 0;


                    //  Проверяем есть ли такой владелец в бвзе GOYUG
                    $objects[$id]['is_goyug_owner'] = $this->checkGoyugPhoneExist($objects[$id]['phone']);

                    //  Парсим имя владельца
                    $owner = $saw->get('.person-name')->toArray();
                    $objects[$id]['owner'] = trim($owner[0]['#text'][0]);

                    //  Парсим фотки
                    //  ============================================================
                    $obj_imgs = $saw->get('meta[property="og:image"]')->toArray();
                    
                    if(count($obj_imgs) > 0)
                    {
                        foreach($obj_imgs as $oi_id => $oi_val)
                        {
                            if(isset($oi_val['content']))
                            {
                                $objects[$id]['imgs'][] = preg_replace("/https/", "http", $oi_val['content']);
                            } 
                        }

                        $objects[$id]['imgs'] = implode(";", $objects[$id]['imgs']);
                    } else {
                        $objects[$id]['imgs'] = "";
                    }

                    //  Парсим описание
                    //  ============================================================
                    $obj_desc = $saw->get('.description-preview-wrapper')->toArray();

                    // print_r($obj_desc);die;

                    $objects[$id]['desc'] = (isset($obj_desc[0]['#text'][0])) ? $obj_desc[0]['#text'][0] : "";

                    //  Парсим координаты
                    //  ============================================================
                    $obj_loc = $saw->get('.item-map')->toArray();
                
                    $objects[$id]['lat'] = (isset($obj_loc[0])) ? round(floatval($obj_loc[0]['data-coords-lat']), 6) : 0;
                    $objects[$id]['lng'] = (isset($obj_loc[0])) ? round(floatval($obj_loc[0]['data-coords-lng']), 6) : 0;

                    // На сервере надо сделать перекодировку
                    //  ============================================================
                    // if($_SERVER['HOSTNAME'] == 'goyug.com')
                    // {
                    //     $objects[$id]['owner'] = iconv("utf-8", "iso8859-1", $owner[0]['#text'][0]);
                    //     $objects[$id]['desc'] = iconv("utf-8", "iso8859-1", $obj_desc[0]['#text'][0]);
                    // }

                    // Пишем а базу объект, если он не парсился
                    // ============================================================
                    $tbl = new Zend_Db_Table(array(
                        'name' => 'pao_avito_object',
                        'primary' => 'pao_id'
                    ));

                    $row = $tbl->createRow(
                        array(
                          'pao_pau_id' => $objects[$id]['phone'],
                          'pao_is_vip' => $objects[$id]['is_vip'],
                          'pao_is_highlight' => $objects[$id]['is_highlight'],
                          'pao_is_premium' => $objects[$id]['is_premium'],
                          'pao_is_up' => $objects[$id]['is_up'],
                          'pao_link' => $objects[$id]['link'],
                          'pao_uid' => $objects[$id]['uid'],
                          'pao_sqr' => $objects[$id]['sqr'],
                          'pao_rooms' => $objects[$id]['rooms'],
                          'pao_name' => $objects[$id]['name'],
                          'pao_desc' => $objects[$id]['desc'],
                          'pao_images' => $objects[$id]['imgs'],
                          'pao_addr_city_uid' => $city_uid,
                          'pao_addr_street' => $objects[$id]['addr_street'],
                          'pao_addr_number' => "",
                          'pao_addr_floor' => $objects[$id]['floor'],
                          'pao_addr_elevation' => $objects[$id]['elevation'],
                          'pao_addr_lat' => $objects[$id]['lat'],
                          'pao_addr_lng' => $objects[$id]['lng'],
                          'pao_price' => $objects[$id]['price'],
                          'pao_create' => date ( 'Y-m-d H:i:s' ),
                          'pao_creater' => 'parser_avito'
                        )
                    );
                    $row->save();

                    // Обновляем статус задачи
                    //  ============================================================
                    if(!empty($task_token))
                    {
                        $query = "
                            UPDATE pt_parse_tasks
                            SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                            WHERE pt_token = '" . $task_token ."'
                        ";
                        $this->db->query($query);         
                    }

                    // Пишем а базу владельца если его нет в базе спарсенных
                    //  ============================================================
                    if(!$this->checkAvitoPhoneExist($objects[$id]['phone']))
                    {
                        $tbl = new Zend_Db_Table(array(
                            'name' => 'pau_avito_user',
                            'primary' => 'pau_id'
                        ));

                        $row = $tbl->createRow(
                            array(
                              'pau_is_goyug' => $objects[$id]['is_goyug_owner'],
                              'pau_phone' => $objects[$id]['phone'],
                              'pau_name' => $objects[$id]['owner'],
                              'pau_city' => $city_uid,
                              'pau_create' => date ( 'Y-m-d H:i:s' ),
                              'pau_creater' => 'parser_avito'
                            )
                        );
                        $row->save();
                    }
                    
                // if object not parsed
                // } else {
                    // unset($objects[$id]);
                // }

            } // foreach

            return $objects;
        
        } // If ObjectsLit not empty
        else {
            return 0;
        }

    }    

    //#########################################################################################
    public function countDownloadItems($pars_city = "")
    {
        $result = array();

        if(!empty($pars_city))
        {
            //Запрос
            $query = "
                SELECT pao_uid, pao_images
                FROM pao_avito_object AS pao
                WHERE pao.pao_addr_city_uid = '" . $pars_city ."'
            ";

            $data = $this->db->query($query)->fetchAll();

            $result['images'] = 0;
            if(count($data) > 0)
            {
                foreach($data as $id => $val)
                {
                    $result[$val['pao_uid']] = explode(";", $val['pao_images']);
                    $result['images'] = $result['images'] + count($result[$val['pao_uid']]);
                }
            }

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    function getAvitoPhone($url, $phone_token = "")
    {
        if(!empty($phone_token) && !empty($url))
        {
            // Используем мобильную версию карточки об-та
            $url = preg_replace("/www/", "m", $url);

            $url = $url . "/phone/" . $phone_token . "?async";
            $phone = $this->get($url, true);

            return $phone;
        } else {
            return;
        }
    }

    //#########################################################################################
    function makeURLMobile($url = "")
    {
        if(!empty($url))
        {
            $url = str_replace("www.", "m.", $url);

            return $url;
        } else {
            return;
        }
    }

    //#########################################################################################
    function get($url, $ajax=false)
    {
        $ch = curl_init ();
        curl_setopt ($ch , CURLOPT_URL , $url);

        if($ajax) 
        {
           $headers = array(
               'x-requested-with: XMLHttpRequest',
               'accept: application/json, text/javascript, */*; q=0.01',
               'accept-language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,uk;q=0.2',
               'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36',
               'referer: ' . str_replace($this->mobDomain,'',$url),
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1 );
        $content = curl_exec($ch);
        curl_close($ch);
        
        return $content;
    }

    //#########################################################################################
    function get_data($url) 
    {
        $ch = curl_init();
        $timeout = 5;
    
        $agents = array(
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (iPad; CPU OS 9_3_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13E238 Safari/601.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/49.0.2623.108 Chrome/49.0.2623.108 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  Trident/5.0)",
            "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0;  Trident/5.0)",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 5.1; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/537.86.5",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12",
            "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36 OPR/36.0.2130.65",
            "Mozilla/5.0 (X11; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy[array_rand($proxy)]);
    
        $data = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($status == '200')
            return $data;
        else
        {
            return $status;
        }

    }
    ##########################################################################################
    public function checkGoyugPhoneExist($phone) {

        if(!empty($phone)){

            $phone = Util::stripTel($phone);

            //Запрос
            $query = "SELECT *
                      FROM u_user AS u
                      WHERE (u.u_phone_1 = '" . $phone ."'
                      OR u.u_phone_2 = '" . $phone ."'
                      OR u.u_phone_3 = '" . $phone ."')
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function checkAvitoPhoneExist($phone) {

        if(!empty($phone)){

            $phone = Util::stripTel($phone);

            // echo $phone;die;

            //Запрос
            $query = "SELECT *
                      FROM pau_avito_user AS pau
                      WHERE pau.pau_phone = '" . $phone ."'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function checkObjectExist($obj_uid = "", $city_uid = "") {

        if(!empty($obj_uid)){

            //Запрос
            $query = "
                SELECT *
                FROM pao_avito_object AS pao
                WHERE pao.pao_uid = '" . $obj_uid ."'
                AND pao.pao_addr_city_uid = '" . $city_uid ."'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function createParseJob($pars_city, $objects_num, $pages_num, $pars_page, $task_token, $action = "") {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'avito',
                  'pt_action' => $action,
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => intval($pages_num),
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => intval($pars_page),
                  'pt_cur_page' => intval($pars_page-1),
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function createDownloadJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'avito',
                  'pt_action' => 'download',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => '',
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    //#########################################################################################
    public function download($pars_city = "", $items, $task_token)
    {
        set_time_limit(0);
        $result = array();

        if($pars_city != "" && count($items) > 0)
        {
            foreach($items as $id => $val)
            {
                if($id != "images")
                {
                    $path = $this->img_path . "/" . $pars_city . "/" . $id;
                    
                    if(!file_exists($path)) mkdir($path, 0777, true);

                    foreach($val as $img_id => $img)
                    {
                        $img_path = $path . "/" . $img_id . ".jpg";
                        if(!file_exists($img_path) && $this->url_exists($img))
                        {
                            if(!empty($img) && !empty($img_path))
                            {
                                copy( $img , $img_path);

                                // Обновляем статус задачи
                                //  ============================================================
                                if(!empty($task_token))
                                {
                                    $query = "
                                        UPDATE pt_parse_tasks
                                        SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                        WHERE pt_token = '" . $task_token ."'
                                    ";
                                    $this->db->query($query);         
                                }

                            }

                        }
                    }

                }
            }
        }

        return $result;
    }

   ##########################################################################################
    function url_exists($url) {
        if (!$fp = curl_init($url)) return false;
        return true;
    }

    ##########################################################################################
    public function updateParseJob($token = "", $value) {

        if(!empty($token)){

            $query = "
                UPDATE pt_parse_tasks
                SET pt_progress = '" . $value ."', pt_end = '".date ( 'Y-m-d H:i:s' )."'
                WHERE pt_token = '" . $token ."'
            ";
            $this->db->query($query);

        }
    }

    ##########################################################################################
    public function checkParseJob($token = "") {

        if(!empty($token)){

            $query = "
                SELECT pt_progress
                FROM pt_parse_tasks
                WHERE pt_token = '" . $token ."'
            ";
            $row = $this->db->query($query)->fetchObject();

            return $row->pt_value;

        }
    }

    ##########################################################################################
    public function getParseJob($token = "") {

        if(!empty($token)){

            $query = "
                SELECT *
                FROM pt_parse_tasks
                WHERE pt_token = '" . $token ."'
            ";
            $row = $this->db->query($query)->fetchObject();

            return $row;

        } else return "empty token";
    }

    ##########################################################################################
    public function generateToken() {
        return Util::generateHash();
    }
}  
?>
