<?php
require_once("Util.php");

class Ai_Model_Log extends Zend_Db_Table_Abstract {

    public $_name       = 'l_log';
    public $_primary    = 'l_id';

    public function init()
    {
        $this->db = Zend_Registry::get('db');
    }
    
    public function getItems( $where )
    {
        $select = $this->select()          
                ->from(array('l_log', array('*') ))
                ->order(array('l_msec DESC'));        

        if(is_array($where)) foreach ( $where AS $condition ) { $select->where( $condition ); }
        
        return $this->fetchAll($select);
    }

    public function write( $params = array(), $raw = array() )
    {
        if(!empty($params))
        {
            $row = $this->createRow();
            if($row) 
            {
                foreach($params as $p_id => $p_val)
                {
                    #$row_name = "l_". 
                    // if($p_id == "result") print_r($p_val);die;
                    // if($p_id != "result") 
                        $row->{"l_".$p_id} = $p_val;    
                }
                list($usec, $sec) = explode(" ", microtime()); 
                
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $auth = Zend_Auth::getInstance();                
                
                $raw = json_encode($raw);

                if($params["result"] == "sql")
                {
                    $raw = trim($raw);
                }
                
                $row->l_date = date ( 'Y-m-d H:i:s' );
                $row->l_msec = (float)$usec + (float)$sec; 
                $row->l_ip = Util::getIp();
                $row->l_useragent = Util::getUserAgent();
                $row->l_url = $_SERVER["REQUEST_URI"];
                $row->l_module = MODULE_NAME;
                $row->l_action = $request->getActionName();
                $row->l_controller = $request->getControllerName();
                $row->l_username = ($auth->hasIdentity()) ? $auth->getIdentity()->u_username : "";
                $row->l_object = json_encode($request->getParams());
                // $row->l_raw = json_encode($raw);
                
                $row->save();
                
                return $row;
                
            } else {
                throw new Zend_Exception("Could not create item!");
            }
        }
        
    }
        
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();
                
                if($row) {
                    $row->delete();
                }else{
                    throw new Zend_Exception("Could not delete item. Item not found!");
                }
            }

        }
    }        
        
    public function setValues($items, $values)
    {
        if(count($items) > 0)
        {
            foreach($items as $id => $item_id)
            {
                $row = $this->find($item_id)->current();
                if(count($values) > 0)
                {
                    foreach($values as $field => $value)
                    {
                        $row->$field = $value;
                    }
                }
                $row->save();
            }        
        }
        
    }

    public function getTop( $count = 10, $actions = array() )
    {
        $result = array();
        $actions_string  = "'" . implode("','", $actions) . "'";

        $query = $this->db->query("
            SELECT *
            FROM l_log
            WHERE l_status = 'success'
            AND l_result IN (".$actions_string.")
            ORDER BY l_date DESC
            LIMIT ".$count."
        ");

        $rows = $query->fetchAll();

        return $rows;
    }


}  
?>
