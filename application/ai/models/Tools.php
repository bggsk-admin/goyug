<?php
require_once ("Util.php");

class Ai_Model_Tools extends Zend_Db_Table_Abstract
{

  public $_name = '';
  public $_primary = '';

  //#########################################################################################
  public function init() {
    $this->auth = Zend_Auth::getInstance();
    $this->db = Zend_Registry::get('db');
    $locale = new Zend_Session_Namespace('locale');

    $this->lang = $locale->curlocale['lang'];

    $this->log = new Ai_Model_Log();
    $this->cashflow = new Ai_Model_Cashflow();
  }

  //#########################################################################################
  public function getObjectImages($object_id = 0) {
    $rows = array();
    $res = array();

    if ($object_id > 0) {

      // Select All Objects
      $query = $this->db->query("
                SELECT f_name
                FROM f_files
                WHERE f_ucid = 'object'
                AND f_uid = '" . $object_id . "'
                ORDER BY f_order
            ");

      $query->setFetchMode(Zend_Db::FETCH_OBJ);
      $rows = $query->fetchAll();

      foreach ($rows as $id => $val) {
        $res[] = $val->f_name;
      }
    }

    return $res;
  }
  //#########################################################################################
  public function defOwnersPasswords() {
    $tbl = new Zend_Db_Table(array('name' => 'u_user', 'primary' => 'u_id'));

    $rows = array();

    // Select All Objects
    $query = $this->db->query("
            SELECT *
            FROM u_user
            WHERE u_role = 'owner'
        ");

    $query->setFetchMode(Zend_Db::FETCH_OBJ);
    $rows = $query->fetchAll();

    foreach ($rows as $id => $val) {
      $phone_pass = Util::stripTel($val->u_phone_1);
      $username = (!empty($val->u_email)) ? $val->u_email : Util::stripTel($val->u_phone_1);

      $row = $tbl->find($val->u_id)->current();
      $row->u_tutorial = 0;
      $row->u_username = $username;
      $row->u_phone_1 = Util::stripTel($val->u_phone_1);
      $row->u_phone_2 = Util::stripTel($val->u_phone_2);
      $row->u_phone_3 = Util::stripTel($val->u_phone_3);
      $row->u_password = md5($phone_pass);
      $row->save();
    }

    $response = "<h4>Владельцам назначены дефолтные пароли (Номера телефонов вида: 79181234567)</h4>";
    return $response;
  }

  //#########################################################################################
  public function rebuildGeoJson() {
    $db = Zend_Registry::get('db');

    $venue_path = $_SERVER['DOCUMENT_ROOT'] . '/data/venue/';

    $response = "";

    // Select All Objects
    $obj_query = $this->db->query("
            SELECT *
            FROM obj_object AS obj

            LEFT JOIN per_period AS per
            ON per.per_id = obj.obj_price_period

            LEFT JOIN v_value AS v
            ON v.obj_id = obj.obj_id

            LEFT JOIN ot_object_type AS ot
            ON ot.ot_id = v.v_key

            LEFT JOIN acc_accommodation AS acc
            ON acc.acc_id = obj.obj_acc_type

            WHERE obj.obj_enable = 1
            AND v.v_table = 'ot_object_type'
            AND obj.obj_enable = 1

            ORDER BY obj_ontop DESC, obj_price
        ");

    $obj_query->setFetchMode(Zend_Db::FETCH_OBJ);
    $objects = $obj_query->fetchAll();

    $geojson_arr = array();
    $venues_arr = array();

    if (count($objects) > 0) {
      foreach ($objects as $id => $val) {
        $obj_id = $val->obj_id;
        $images = $this->getObjectImages($obj_id);

        // $marker_price = $val->obj_price;
        // $marker_price_class = floor($val->obj_price / 1000);

        $pin_style = ($val->obj_instantbook) ? "selected" : "regular";
        // $pin_style = "regular";

        $venues_arr[$val->obj_addr_city_uid][] = array(
          "type" => "Feature",
          "geometry" => array(
              "type" => "Point",
              "coordinates" => array(round($val->obj_addr_lon, 6), round($val->obj_addr_lat, 6),),),
              "properties" => array(
                  "index" => "",
                  "id" => $val->obj_id,
                  "uid" => $val->obj_uid,
                  "title" => $val->obj_name_ru,
                  "ontop" => $val->obj_ontop,
                  "ontop_mark" => $val->obj_ontop_mark,
                  "isempty" => $val->obj_isempty,

        // "description" => $val->obj_name_ru,
        // "marker-color" => "#FC4353",
        // "marker-size" => "medium",
        // "marker-symbol" => "bicycle",

        "price" => $val->obj_price,
        "rooms" => $val->obj_rooms,
        "guests" => $val->obj_guests,
        "sqr" => $val->obj_sqr,
        "rating" => $val->obj_rating,
        "checkin" => $val->obj_checkin,
        "checkout" => $val->obj_checkout,
        "addr_street" => $val->obj_addr_street,
        "addr_number" => $val->obj_addr_number,
        "addr_floor" => ($val->obj_addr_floor > 0) ? $val->obj_addr_floor : "",
        "acc_name" => $val->acc_name_ru,
        "minstay" => $val->obj_price_minstay,
        "ot_name" => $val->ot_name_ru,
        "ot_type" => $val->ot_uid,
        "link" => 'http://' . $val->obj_addr_city_uid . '.' . $_SERVER['HOSTNAME'] . '/' . $val->obj_id,

        /*
        "bathrooms" => $val->obj_bathrooms,
        "bedrooms" => $val->obj_bedrooms,
        "beds" => intval($val->obj_beds_dbl + $val->obj_beds_sngl),
        */
        "sleepers" => $val->obj_sleepers,
        "images" => $images,
        "icon" => array("className" => array("marker-pin pin-" . $pin_style),),),);
      }
      // foreach

    }
    // if

    foreach ($venues_arr as $city_uid => $city_val) {
      $geojson_arr = array("type" => "FeatureCollection", "features" => $city_val,);

      $geojson = json_encode($geojson_arr);
      $geojson_file = fopen($venue_path . $city_uid . ".geojson", "w");
      fwrite($geojson_file, $geojson);
      fclose($geojson_file);

      $response.= "<tr><td>" . $city_uid . ".geojson</td><td>" . count($city_val) . "</td></tr>";
    }
     // foreach

    $response = "<h4>Пересозданы файлы:</h4><table class='table table-striped'>" . $response . "</table>";

    return $response;
  }

  //#########################################################################################
  public function ontopDummy() {

    $db = Zend_Registry::get('db');

    //Города фиктивного поднятия
    $city_uids = Array();

    //Ограничение кол-ва объектов
    $limit = 3;

    $response = "";

    //Объекты для фиктивного поднятия
    /*
    $query = $this->db->query("SELECT obj_id
                               FROM obj_object
                               WHERE obj_enable AND obj_addr_city_uid = 'krasnodar'
                               ORDER BY RAND()
                               LIMIT $limit");

    $query->setFetchMode(Zend_Db::FETCH_OBJ);
    $objects = $query->fetchAll();
    */
    $objects = Array();

    //Краснодар
    // $objects[] = Array("u_id" => 30, "obj_id" => 198);
    // $objects[] = Array("u_id" => 31, "obj_id" => 200);
    // $objects[] = Array("u_id" => 36, "obj_id" => 228);
    // $objects[] = Array("u_id" => 40, "obj_id" => 249);
    // $objects[] = Array("u_id" => 68, "obj_id" => 371);

    //Кисловодск
    // $objects[] = Array("u_id" => 790 , "obj_id" => 3119);
    // $objects[] = Array("u_id" => 791 , "obj_id" => 3121);
    // $objects[] = Array("u_id" => 792 , "obj_id" => 3123);
    // $objects[] = Array("u_id" => 794 , "obj_id" => 3127);
    // $objects[] = Array("u_id" => 796 , "obj_id" => 3130);

    //Ростов
    // $objects[] = Array("u_id" => 1007, "obj_id" => 3654);
    // $objects[] = Array("u_id" => 1008, "obj_id" => 3662);
    // $objects[] = Array("u_id" => 1009, "obj_id" => 3665);
    // $objects[] = Array("u_id" => 1010, "obj_id" => 3666);
    // $objects[] = Array("u_id" => 1011, "obj_id" => 3677);

    //Ставрополь
    // $objects[] = Array("u_id" => 1188, "obj_id" => 4070);
    // $objects[] = Array("u_id" => 1190, "obj_id" => 4075);
    // $objects[] = Array("u_id" => 1183, "obj_id" => 4055);
    // $objects[] = Array("u_id" => 1184, "obj_id" => 4062);
    // $objects[] = Array("u_id" => 1186, "obj_id" => 4065);

    //Сочи
    // $objects[] = Array("u_id" => 620 , "obj_id" => 2527);
    // $objects[] = Array("u_id" => 621 , "obj_id" => 2534);
    // $objects[] = Array("u_id" => 654 , "obj_id" => 2619);
    // $objects[] = Array("u_id" => 354, "obj_id" => 1470);
    // $objects[] = Array("u_id" => 354, "obj_id" => 1470);

    //Пятигорск
    // $objects[] = Array("u_id" => 302, "obj_id" => 1215);
    // $objects[] = Array("u_id" => 303, "obj_id" => 1216);
    // $objects[] = Array("u_id" => 305, "obj_id" => 1235);
    // $objects[] = Array("u_id" => 307, "obj_id" => 1249);
    // $objects[] = Array("u_id" => 308, "obj_id" => 1252);
    // $objects[] = Array("u_id" => 309 , "obj_id" => 1257);
    // $objects[] = Array("u_id" => 693 , "obj_id" => 2771);
    // $objects[] = Array("u_id" => 694 , "obj_id" => 2775);
    // $objects[] = Array("u_id" => 710 , "obj_id" => 2859);
    // $objects[] = Array("u_id" => 715 , "obj_id" => 2871);

    //Анапа
    // $objects[] = Array("u_id" => 206 , "obj_id" => 934 );
    // $objects[] = Array("u_id" => 1297, "obj_id" => 4544);
    // $objects[] = Array("u_id" => 1298, "obj_id" => 4545);
    // $objects[] = Array("u_id" => 1299, "obj_id" => 4546);
    // $objects[] = Array("u_id" => 1305, "obj_id" => 4557);

    //Железноводск
    // $objects[] = Array("u_id" => 1332, "obj_id" => 4600);
    // $objects[] = Array("u_id" => 1333, "obj_id" => 4603);
    // $objects[] = Array("u_id" => 1334, "obj_id" => 4604);
    // $objects[] = Array("u_id" => 1335, "obj_id" => 4607);
    // $objects[] = Array("u_id" => 1336, "obj_id" => 4608);

    //Севастополь
    // $objects[] = Array("u_id" => 1408, "obj_id" => 4749);
    // $objects[] = Array("u_id" => 1409, "obj_id" => 4750);
    // $objects[] = Array("u_id" => 1410, "obj_id" => 4751);
    // $objects[] = Array("u_id" => 1411, "obj_id" => 4753);
    // $objects[] = Array("u_id" => 1413, "obj_id" => 4757);

    //Ялта
    // $objects[] = Array("u_id" => 1471, "obj_id" => 4865);
    // $objects[] = Array("u_id" => 1473, "obj_id" => 4868);
    // $objects[] = Array("u_id" => 1475, "obj_id" => 4870);
    // $objects[] = Array("u_id" => 1477, "obj_id" => 4872);
    // $objects[] = Array("u_id" => 1478, "obj_id" => 4873);

    //Астрахань
    // $objects[] = Array("u_id" => 1503, "obj_id" => 5006);
    // $objects[] = Array("u_id" => 1504, "obj_id" => 5010);
    // $objects[] = Array("u_id" => 1505, "obj_id" => 5011);
    // $objects[] = Array("u_id" => 1506, "obj_id" => 5012);
    // $objects[] = Array("u_id" => 1507, "obj_id" => 5017);

    //Волгоград
    // $objects[] = Array("u_id" => 1551, "obj_id" => 5156);
    // $objects[] = Array("u_id" => 1553, "obj_id" => 5184);
    // $objects[] = Array("u_id" => 1554, "obj_id" => 5187);
    // $objects[] = Array("u_id" => 1555, "obj_id" => 5188);
    // $objects[] = Array("u_id" => 1558, "obj_id" => 5195);

    //Воронеж
    // $objects[] = Array("u_id" => 1671, "obj_id" => 5617);
    // $objects[] = Array("u_id" => 1673, "obj_id" => 5625);
    // $objects[] = Array("u_id" => 1674, "obj_id" => 5627);
    // $objects[] = Array("u_id" => 1675, "obj_id" => 5628);
    // $objects[] = Array("u_id" => 1670, "obj_id" => 5612);

    //Новороссийск
    // $objects[] = Array("u_id" => 225 , "obj_id" => 1024);
    // $objects[] = Array("u_id" => 226 , "obj_id" => 1025);
    // $objects[] = Array("u_id" => 228 , "obj_id" => 1036);
    // $objects[] = Array("u_id" => 229 , "obj_id" => 1041);
    // $objects[] = Array("u_id" => 234 , "obj_id" => 1051);

    if (count($objects) > 0) {
      foreach ($objects as $key => $object) {
        //print_r($object);
        $this->cashflow->ontop(Array("obj_id" => $object['obj_id']));
        $this->cashflow->register(Array(
          "u_id" => $object['u_id'],
          "obj_id" => $object['obj_id'],
          "transaction_name" => 'ontop_dummy',
          "sum" => 0,
          "success" => true
          ));
      }
    }

    $response = "<h4>Объекты подняты</h4>";

    return $response;
  }

  //#########################################################################################
  public function findUnlinkedObjDirs() {
    $db = Zend_Registry::get('db');
    $response = array();

    $obj_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/object/';

    $dirs = scandir($obj_dir, SCANDIR_SORT_ASCENDING);
    $count_dirs = count($dirs) - 2;

    foreach ($dirs as $id => $dir) {
      if ($dir != "." && $dir != ".." && $dir != ".DS_Store") {

        $obj_id = intval($dir);

        $query = $this->db->query("
                SELECT obj_id
                FROM obj_object AS obj
                WHERE obj.obj_id = '" . $obj_id . "'
            ");

        $objs = $query->fetchAll();

        if (count($objs) == 0) {
          $response[] = $obj_id;
        }
      }
    }

    return $response;
  }

  //#########################################################################################
  public function findObjectsNoImgDir() {

    $db = Zend_Registry::get('db');
    $response = array();

    $obj_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/object/';

    $query = $this->db->query("
        SELECT obj_id
        FROM obj_object
    ");

    $objs = $query->fetchAll();

    foreach ($objs as $id => $obj) {

        $obj_id = $obj['obj_id'];

        if(!file_exists($obj_dir . $obj_id)) {
          $response[] = $obj_id;
          }

    }

    return $response;
  }

  //#########################################################################################
  public function findUnlinkedImgFiles() {

    $db = Zend_Registry::get('db');
    $response = array();

    $obj_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/object/';

    $query = $this->db->query("
        SELECT f_name, f_uid, f_id
        FROM f_files
    ");

    $files = $query->fetchAll();

    foreach ($files as $id => $file) {

        $f_name = $file['f_name'];

        if(!file_exists($obj_dir . $file['f_uid'] . "/" . $f_name)) {
          $response[] = array("name" => $f_name, "id" => $file['f_id'], "uid" => $file['f_uid']);
          }

    }

    return $response;
  }

  //#########################################################################################
  public function findObjectsWithoutOwners() {

    $db = Zend_Registry::get('db');
    $response = array();

    // $obj_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/object/';

    $query = $this->db->query("
      SELECT *
      FROM obj_object
      WHERE obj_u_id = '' or obj_u_id = 0 or isnull(obj_u_id)
    ");

    $rows = $query->fetchAll();

    return $rows;
  }

  //#########################################################################################
  public function refreshCitiesStat() {

    $db = Zend_Registry::get('db');
    $response = array();

    $query = $this->db->query("
      SELECT obj.obj_addr_city_uid AS city, obj.obj_addr_city, count(obj.obj_id) as objects, obj_addr_lat, obj_addr_lon
      FROM obj_object as obj
      WHERE obj.obj_addr_city_uid <> ''
      AND obj.obj_enable = 1
      GROUP BY obj.obj_addr_city_uid
      ORDER BY objects DESC
    ");

    $cities = $query->fetchAll();

    foreach($cities as $city_id =>$city_val)
    {

      $city_uid = $city_val['city'];
      $city_objects = $city_val['objects'];

      $city_prices = $this->getCityPriceRange($city_uid);

      $city_minprice = (isset($city_prices['minprice'])) ? $city_prices['minprice'] : 0;
      $city_maxprice = (isset($city_prices['maxprice'])) ? $city_prices['maxprice'] : 0;

      if(!empty($city_uid) && $city_uid != "")
      {

        $this->db->query("
          UPDATE ct_city
          SET ct_objects = ".$city_objects.",
          ct_minprice = '".$city_minprice."',
          ct_maxprice = '".$city_maxprice."',
          ct_lat = '".round($city_val['obj_addr_lat'], 6)."',
          ct_lng = '".round($city_val['obj_addr_lon'], 6)."'
          WHERE ct_uid = '".$city_uid."'
        ");

        $cities[$city_id]['minprice'] = $city_minprice;
        $cities[$city_id]['maxprice'] = $city_maxprice;

      }

    }

    //----------------------------------------------------------------
    // Исправляем названия городов
    //----------------------------------------------------------------

    // Железноводск
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'jeleznovodsk', 'zheleznovodsk')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'jeleznovodsk'");

    // Астрахань
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'astrahan', 'astrakhan')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'astrahan'");

    // Сочи
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'bolshoysochi', 'sochi')");
    $this->db->query("UPDATE obj_object SET obj_addr_city = REPLACE(obj_addr_city, 'Большой Сочи', 'Сочи')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'bolshoysochi'");
    
    // Воронеж
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'voronej', 'voronezh')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'voronej'");

    // Усть-Лабинск
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'ustjlabinsk', 'ustlabinsk')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'ustjlabinsk'");

    // Нижний Новгород
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'nijniynovgorod', 'nnovgorod')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'nijniynovgorod'");

    // Санкт-Петербург
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'sanktpeterburg', 'spb')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'sanktpeterburg'");

    // Москва
    $this->db->query("UPDATE obj_object SET obj_addr_city_uid = REPLACE(obj_addr_city_uid, 'moskva', 'moscow')");
    $this->db->query("DELETE FROM ct_city WHERE ct_uid = 'moskva'");

    // $this->rebuildGeoJson();

    return $cities;

  }

    ##########################################################################################
    public function getCityPriceRange($ct_uid) {

        $db = Zend_Registry::get( 'db' );

        if(!empty($ct_uid))
        {
            $data = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice
                FROM obj_object AS obj
                WHERE obj.obj_addr_city_uid = '" . $ct_uid . "'
                AND obj.obj_enable = 1
                "
            )->fetchAll();

            return $data[0];

        } else {
            return array(0,0);
        }

    }

    ##########################################################################################
    public function markRandomObjects() {

        $db = Zend_Registry::get( 'db' );

        $days = 1;
        $result = array();

        $data = $db->query("
            SELECT obj_id
            FROM obj_object
            "
        )->fetchAll();

        foreach($data as $key => $val) {


          $obj_id = $val['obj_id'];
          $markedTo = date("Y-m-d H:i:s", mktime() + $days*(60*60*24));

          if(rand(1,10) == 1) {
            $db->query("UPDATE obj_object SET obj_mark_out = '1', obj_mark_out_deactivate = '".$markedTo."' WHERE obj_id = '".$obj_id."'");
            $result[] = $obj_id;
          } else {
            $db->query("UPDATE obj_object SET obj_mark_out = '0' WHERE obj_id = '".$obj_id."'");
          }

        }

        return $result;

    }

      //#########################################################################################
      public function refreshStat() {

        $db = Zend_Registry::get('db');

        $response = array();
        $result = array();

        // $start_date = '2015-09-01';
        // $start_day = date('z', strtotime($start_date));
        // $days_num = date('z', strtotime(date("Y-m-d")));
        // $number_of_days = ($days_num - $start_day) +1 ;

      /*
      for ($i = 0; $i < $number_of_days; $i++)
      {
          $date = strtotime(date("Y-m-d", strtotime($start_date)) . " +$i day");
          $curdate = date('Y-m-d', $date);
      }
      */

      // ********************************************************************
      // showtelext, showtelextview, showtel, sendorder
      // ********************************************************************
      $result = array();
      $query = "
        SELECT DATE(l_date) AS date, l_object, l_result
        FROM l_log
        WHERE l_status = 'success'
        AND l_result IN ('showtelext', 'showtelextview', 'showtel', 'sendorder')
        AND LOWER(l_useragent) NOT LIKE '%bot%'";

      $rows = $this->db->query($query)->fetchAll();

      foreach($rows as $key => $value)
      {
          $l_object = json_decode($value['l_object']);
          $date = $value['date'];

          if(isset($result[$date][$l_object->cityuid][$value['l_result']]))
          {
            $result[$date][$l_object->cityuid][$value['l_result']] = $result[$date][$l_object->cityuid][$value['l_result']] + 1;
          } else {
            $result[$date][$l_object->cityuid][$value['l_result']] = 1;
          }
      }

      foreach($result as $stat_date => $cities)
      {
        foreach($cities as $city => $actions)
        {
          foreach($actions as $action => $count)
          {

          $query = "DELETE FROM cs_citystat WHERE cs_date = '".$stat_date."' AND cs_city_uid = '".$city."' AND cs_action = '".$action."' ";
          $this->db->query($query);

          $tbl = new Zend_Db_Table(array(
              'name' => 'cs_citystat',
              'primary' => 'cs_id'
          ));

          $row = $tbl->createRow(
            array(
              'cs_date' => $stat_date,
              'cs_city_uid' => $city,
              'cs_action' => $action,
              'cs_count' => $count,
            )
          );

          $row->save();

          }
        }
      }

      // ********************************************************************
      // login
      // ********************************************************************
      $result = array();
      $usernames = array();

        $query = "
            SELECT DATE(l_date) AS date, l_raw
            FROM l_log
            WHERE l_status = 'success'
            AND l_result IN ('login')
            AND LOWER(l_useragent) NOT LIKE '%bot%'
            AND l_date >= '2015-09-01'
        ";
        $rows = $this->db->query($query)->fetchAll();

        foreach($rows as $key => $value)
        {
          // echo $value['l_raw'];

          if($value['l_raw'] != '"{}"')
          {
            $l_raw = json_decode($value['l_raw'], true);

            if(
                isset($l_raw['u_username'])
                && !empty($l_raw['u_username'])
                && !in_array($l_raw['u_username'], array('marselos@gmail.com', 'jobguess@mail.ru', '79615322225', '79181610720', '79182963704', 'content02', 'content01', 'lvika', 'nikita', '160961@mail.ru', 'i@bggsk.ru') )
                )

            $date = $value['date'];
            $user = $l_raw['u_username'];

            if($user != "")
            {
              if(isset($logins[$date][$user]))
              {
                $logins[$date][$user] = $logins[$date][$user] + 1;
              } else {
                $logins[$date][$user] = 1;
              }

            }

          }
        }

        print_r($logins);die;

        $usernames_sql = "'" . implode("','", $usernames) . "'";

        $query = "
            SELECT obj.obj_addr_city_uid AS city, DATE(obj.obj_create) AS date
            FROM u_user as u
            LEFT JOIN obj_object AS obj
            ON u.u_id = obj.obj_u_id
            WHERE u.u_role = 'owner'
            AND u_username IN (".$usernames_sql.")
            AND obj.obj_addr_city_uid <> ''
            AND obj.obj_create >= '2015-09-01'
        ";
        $rows = $this->db->query($query)->fetchAll();

        foreach($rows as $id => $value)
        {
          $date = $value['date'];
          $city = $value['city'];

          if(isset($result[$date][$city]))
          {
            $result[$date][$city] = $result[$date][$city] + 1;
          } else {
            $result[$date][$city] = 1;
          }
        }



        print_r($result);die;

        return $response;
      }

    //#########################################################################################
    public function clearImages() {

      $result = 0;
      $imgdir = $_SERVER['DOCUMENT_ROOT'] . "/upload/object";

      foreach (scandir($imgdir) as $objdir)
      {
            if ($objdir == '.' || $objdir == '..') continue;

            $obj_path = $imgdir . '/' . $objdir;
            $obj_1200_path = $imgdir . '/' . $objdir . '/x1200';
            $obj_800_path = $imgdir . '/' . $objdir . '/x800';
            $obj_600_path = $imgdir . '/' . $objdir . '/x600';
            $obj_400_path = $imgdir . '/' . $objdir . '/x400';
            $obj_300_path = $imgdir . '/' . $objdir . '/x300';


            if (is_dir($obj_path))
            {
              foreach (scandir($obj_path) as $objimg)
              {
                if ($objimg != '.' && $objimg != '..' && !is_dir($obj_path . '/' . $objimg))
                {

                  if(file_exists($obj_1200_path . '/' . $objimg))
                  {
                    unlink($obj_1200_path . '/' . $objimg);
                    $result ++;
                  }

                  if(file_exists($obj_600_path . '/' . $objimg))
                  {
                    unlink($obj_600_path . '/' . $objimg);
                    $result ++;
                  }

                  if(file_exists($obj_400_path . '/' . $objimg))
                  {
                    unlink($obj_400_path . '/' . $objimg);
                    $result ++;
                  }

                  if(file_exists($obj_300_path . '/' . $objimg))
                  {
                    unlink($obj_300_path . '/' . $objimg);
                    $result ++;
                  }

                  if(file_exists($obj_800_path . '/' . $objimg))
                  {
                    copy($obj_800_path . '/' . $objimg, $obj_path . '/' . $objimg);
                    $result ++;
                  }
                }
              }
            }

            rmdir($obj_1200_path);
            rmdir($obj_800_path);
            rmdir($obj_600_path);
            rmdir($obj_400_path);
            rmdir($obj_300_path);

        }

        return "Удалено " . $result . " файлов";
    }

    ##########################################################################################
    public function setFloors() {

        $db = Zend_Registry::get( 'db' );

        $tbl = new Zend_Db_Table(array(
            'name' => 'obj_object',
            'primary' => 'obj_id'
        ));

        $elevation = array("5", "9", "11", "16");

        $result = array();

        $data = $db->query("
            SELECT obj_id
            FROM obj_object
            "
        )->fetchAll();

        foreach($data as $key => $val)
        {
          $obj_id = $val['obj_id'];

          $row = $tbl->find($obj_id)->current();

          $obj_floor = $row->obj_addr_floor;
          $obj_elevation = $row->obj_addr_elevation;

          $obj_elevation = $elevation[array_rand($elevation, 1)];
          $obj_floor = rand(1, $obj_elevation);

          $row->obj_addr_floor = $obj_floor;
          $row->obj_addr_elevation = $obj_elevation;

          $row->save();

          $result[] = $obj_id."<br>";

        }

        return $result;

    }



    //#########################################################################################
    public function renameImages() {

      $result = 0;
      $upload_path = $_SERVER['DOCUMENT_ROOT'] . "/upload/object";

      foreach (scandir($upload_path) as $obj_id_dir)
      {
            if ($obj_id_dir == '.' || $obj_id_dir == '..') continue;

            $obj_id = intval($obj_id_dir);
            $obj_path = $upload_path . "/" . $obj_id_dir;
            $thumb_path = $obj_path . "/thumbnail";
            $x100_path = $obj_path . "/x100";
            $x500_path = $obj_path . "/x500";
            $x800_path = $obj_path . "/x800";

            $new_filename = 0;

            foreach(scandir($obj_path) as $image_file)
            {
              if ($image_file != '.' && $image_file != '..' && !is_dir($obj_path . '/' . $image_file))
              {

                rename($obj_path . '/' . $image_file, $obj_path . '/' . $new_filename . '.jpg');
                rename($thumb_path . '/' . $image_file, $thumb_path . '/' . $new_filename . '.jpg');
                rename($x100_path . '/' . $image_file, $x100_path . '/' . $new_filename . '.jpg');
                rename($x500_path . '/' . $image_file, $x500_path . '/' . $new_filename . '.jpg');
                rename($x800_path . '/' . $image_file, $x800_path . '/' . $new_filename . '.jpg');

                // Update Files Table
                $query = "
                    UPDATE f_files
                    SET f_name = '" . $new_filename . ".jpg'
                    WHERE f_ucid = 'object' AND f_uid = '".$obj_id."' AND f_name = '" . $image_file . "'
                ";
                $this->db->query($query);


                $result++;
                $new_filename++;

              }
            }


      }
      return $result;

    }

    //#########################################################################################
    public function addThumbsImages() {

      $result = 0;
      $upload_path = $_SERVER['DOCUMENT_ROOT'] . "/upload/object";

      foreach (scandir($upload_path) as $obj_id_dir)
      {
            if ($obj_id_dir == '.' || $obj_id_dir == '..') continue;

            $obj_path = $upload_path . "/" . $obj_id_dir;
            $thumb_path = $obj_path . "/thumbnail";

            if(is_dir($obj_path))
            {
              if(!file_exists($thumb_path) && !is_dir($thumb_path))
              {
                mkdir($thumb_path, 0777, true);

              }

              foreach(scandir($obj_path) as $image_file)
              {
                if ($image_file != '.' && $image_file != '..' && !is_dir($obj_path . '/' . $image_file) && !file_exists($thumb_path . '/' . $image_file))
                {

                        $image_path = $obj_path . '/' . $image_file;
                        $thumb_file = $thumb_path . '/' . $image_file;

                        $im = $this->resizeImage($image_path, 200, 150);
                        imagejpeg($im, $thumb_file, 100);
                        imagedestroy($im);

                        $result++;


                }
              }
            }






      }
      return $result;

    }

    ##########################################################################################
    public function resizeImage($filename, $width, $height)
    {
        $image = imagecreatefromjpeg($filename);
        list($image_w, $image_h) = getimagesize($filename);

        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, $image_w, $image_h);

        return $new_image;
  }


    ##########################################################################################
    public function importEmails($data)
    {
      $items = 0;

      if(!empty($data))
      {
        $city = trim($data['pars_city']);
        $content = $data['import_data'];

        if(!empty($city) && !empty($content))
        {
          $content = explode("\r\n", $content);

          if(count($content) > 0)
          {
            foreach($content as $val)
            {
              $row = explode("\t", $val);
              if(count($row) == 2)
              {
                $phone = trim($row[0]);
                $email = trim($row[1]);

                // echo $phone . "-" .$email."\n";

                // Update DB
                $query = "
                    UPDATE u_user
                    SET u_email = '" . $email . "'
                    WHERE u_phone_1 = '" . $phone . "' AND u_url LIKE '%" . $city . "%'
                ";
                $this->db->query($query);

                $items++;

              }

            }
          }

        }
      }
      // die;
      return $items;
    }

  }
?>
