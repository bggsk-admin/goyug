<?php
class Ai_Model_ObjectType extends Zend_Db_Table_Abstract {

    public $_name       = 'ot_object_type';
    public $_primary    = 'ot_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $this->locale  = new Zend_Session_Namespace('locale');

        $this->lang = $this->locale->curlocale['lang'];

        $this->categories   = 'ot_object_type';
        $this->items        = 'obj_object';

        //AptTypes
        $this->pref     = 'ot_';
        $this->id       = $this->pref . 'id';
        $this->pid      = $this->pref . 'pid';

        $this->name    = $this->pref . 'name_' . $this->lang;

        //Items
        $this->item_pref = 'obj_';
        $this->item_id   = $this->item_pref . 'id';
        $this->item_pid  = $this->item_pref . $this->pref . 'id';
    }

    ##########################################################################################
    public function getItems( $where = array(), $order = array() )
    {

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(
                    array($this->categories),
                    array(
                        'id'        => $this->id,
                        'pid'       => $this->pid,
                        'enable'    => $this->pref."enable",
                        'name'      => $this->name,
                        'order'      => $this->pref."order",
                        'uid'      => $this->pref."uid",
                        'code'      => $this->pref."code",
                        'ot_bookfee_pct'      => 'ot_bookfee_pct',
                        'ot_bookfee_fix'      => 'ot_bookfee_fix',
                        //'items'     => "COUNT(".$this->item_id.")",
                    )
                )
                //->joinLeft(array($this->items), $this->id . ' = ' . $this->item_pid )
                //->group( $this->id )
                ;

        if(!empty($order)) $select->order( $order );
        if(!empty($where)) foreach ( $where AS $condition ) { $select->where( $condition ); }

        $rows = $this->fetchAll($select)->toArray();

        $res = array();
        foreach($rows as $row_id => $row_val)    $res[$row_val["id"]] = $row_val;

        return $res;
    }

    ##########################################################################################
    public function arrToTree( $rows )
    {
        $tree = array();        //stores the tree
        $tree_index = array();  //an array used to quickly find nodes in the tree

        while(count($rows) > 0)
        {
            foreach($rows as $row_id => $row)
            {
                if( $row["pid"] )
                {

                    if((!array_key_exists($row["pid"], $rows)) && (!array_key_exists($row["pid"], $tree_index)))
                    {
                        unset($rows[$row_id]);
                    }
                    else
                    {
                        if(array_key_exists($row["pid"], $tree_index))
                        {
                            $parent = & $tree_index[$row["pid"]];
                            $parent['child'][$row_id] = array("node" => $row, "child" => array());
                            $tree_index[$row_id] = & $parent['child'][$row_id];
                            unset($rows[$row_id]);
                        }//if

                    }//else
                }
                else
                {
                    $tree[$row_id] = array("node" => $row, "child" => array());
                    $tree_index[$row_id] = & $tree[$row_id];
                    unset($rows[$row_id]);
                }//else
            }//foreach
        }//while

        unset($tree_index);

        return $tree;
    }

    ##########################################################################################
    public function printTree($node, $level)
    {
        //print the current node
        $html = str_repeat(".", $indent) . "<li>". $level ." ". $node['node'][$this->name];

        if($node['child'])
        {
            $html .= "\n". str_repeat(" ", $indent + 10) . "<ul>\n";

            //then print it's child nodes
            foreach($node['child'] as $child)
            {
                $html .= $this->printTree($child, $level + 1);
            }

            $html .= str_repeat(" ", $indent + 10) . "</ul>\n". str_repeat(" ", $indent);
        }

        $html .= "</li>\n";
        return $html;
    }

    ##########################################################################################
    public function getSelectCtgTree($show_root = TRUE)
    {
        $res = "";
        $items = array();

        if($show_root) $items[0] = html_entity_decode("&mdash; Корневой &mdash;", ENT_COMPAT, "UTF-8");

        $rows = $this->getItems();
        $tree = $this->arrToTree( $rows );

        foreach($tree as $node)
        {
            $res .= $this->_getSelectCtgTree($node, 0);
        }

        $res = preg_replace('/\|$/', '', $res);
        $res = explode("|", $res);

        foreach($res as $val)
        {
            list($id, $name, $level) = explode("^", $val);
            $name = str_repeat("&nbsp;", ($level*5)).$name;
            $items[$id] = html_entity_decode($name, ENT_COMPAT, "UTF-8");
        }


        return $items;
    }

    ##########################################################################################
    function _getSelectCtgTree($node, $level)
    {
        $html = "";

        //$html .= $node['node']['id']."^".$node['node']['name']." (".$node['node']['items'].")^".$level."|";
        $html .= $node['node']['id']."^".$node['node']['name']."^".$level."|";

        if($node['child'])
        {
            foreach($node['child'] as $child)
            {
                $html .= $this->_getSelectCtgTree($child, $level + 1);
            }
        }

        return $html;
    }

    ##########################################################################################
    public function getSelectCtgTreeDisabled( $apttyp_id_arr)
    {
        $disabled = array();
        $childs = $this->getChildsArr();
        unset($childs[0]);

        foreach( $apttyp_id_arr as $apttyp_id => $apttyp_name)
        {
            if(is_array($childs[$apttyp_id])) $disabled[] = $apttyp_id;
        }

        return $disabled;
    }

    ##########################################################################################
    public function getChildsArr()
    {
        $select = $this->db->query("SELECT ".$this->pid.", GROUP_CONCAT(".$this->id.") AS childs FROM ".$this->categories." GROUP BY ".$this->pid);
        $rows = $select->fetchAll();

        $res = array();
        foreach($rows as $id => $val)
        {
            $res[$val[$this->pid]] = explode(",", $val['childs']);
        }

        return $res;
    }

    ##########################################################################################
    public function getChildsByID($apttyp_id, $childs_arr)
    {
        $res .= $apttyp_id.",";

        if($childs_arr[$apttyp_id])
        {
            foreach($childs_arr[$apttyp_id] as $child)
            {
                $res .= $this->getChildsByID($child, $childs_arr);
            }
        }

        return $res;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }

    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues[$this->id])->current();
        $row->setFromArray($formValues);

        if($row)
        {
            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        $items_table = new Zend_Db_Table(array('name' => $this->items, 'primary' => $this->item_id));

        $childs_arr = $this->getChildsArr();
        $childs_id = "";

        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $apttyp_id)
            {
                $childs_id .= $this->getChildsByID($apttyp_id, $childs_arr);
            }

            $childs_id = preg_replace('/,$/', '', $childs_id);
            $this->delete( array( $this->id . ' in (?)' => array( explode(",", $childs_id ))) );

            $items_table->update(
                array ( $this->item_pid => '-1' ),
                array( $this->item_pid . ' in (?)' => array( $childs_id ) )
            );

        }
    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    if($field == "mdl_br_id")
                    {
                        $mdl_br_ids[] = $row->$field;
                        $mdl_br_ids[] = $value;
                    }

                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }


    ##########################################################################################
    public function getAptTypesMultiselect()
    {
        $res = "";
        $items = array();

        $select =  $this->select()
                    ->where( "apttyp_enable", 1 )
                    ;

        $rows = $this->fetchAll($select)->toArray();
        foreach($rows as $id => $val)
        {
            $items[$val['apttyp_id']] = $val['apttyp_name_' . $this->lang];
        }

        return $items;


    }

    ##########################################################################################
    public function makeTemplate( $rowID )
    {
        $tplTable = new Zend_Db_Table(array('name'=>'apttpl_apartment_templates', 'primary'=>'apttpl_id'));

        if(!empty($rowID))
        {
            $item = $tplTable->createRow();
            $row = $this->find($rowID)->current();

            if($item) {

                $item->tpl_enable = 1;

                foreach($this->locale->locales as $lang => $val)
                {
                    $item->{"apttpl_name_" . $lang} = $row->{"apttyp_name_" . $lang};
                }

                $item->save();

                $row->apttyp_tpl_id = $item->tpl_id;
                $row->save();

                return $item;

            } else {
                throw new Zend_Exception("Could not create item!");
            }

        }
    }

}
?>
