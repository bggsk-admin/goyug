<?php
require_once("Util.php");

class Ai_Model_Owner extends Zend_Db_Table_Abstract {

    public $_name = 'u_user';
    public $_primary = 'u_id';

    ##########################################################################################
    public function init()
    {
        $this->auth = Zend_Auth::getInstance();
        $this->db = Zend_Registry::get( 'db' );
        $locale  = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref     = "u_";
        $this->id       = $this->pref . 'id';
        $this->name     = $this->pref . 'name_' . $this->lang;
    }
    ##########################################################################################
    public function getItems()
    {
        $sql_where = "";

        if($this->auth->getIdentity()->u_role == "editor")
        {
            // $sql_where = " AND u.u_creater = '".$this->auth->getIdentity()->u_username."' ";
        }

        $data = $this->db->query(
            "SELECT u.u_id, u.u_active, u.u_phone_1, u.u_phone_2, u.u_email, u.u_firstname, u.u_lastname, obj.obj_addr_city AS city, count(obj.obj_id) as objects
            FROM u_user as u
            LEFT JOIN obj_object AS obj
            ON u.u_id = obj.obj_u_id
            WHERE u.u_role = 'owner'
            " . $sql_where . "
            GROUP BY u.u_id
            ORDER BY city, objects
            "            
        )->fetchAll();

        return $data;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $row = $this->createRow($formValues);
        if($row) {

            $row->u_role = "owner";

            $row->u_phone_1 = (!empty($row->u_phone_1)) ? Util::stripTel($row->u_phone_1): "";
            $row->u_phone_2 = (!empty($row->u_phone_2)) ? Util::stripTel($row->u_phone_2): "";
            $row->u_phone_3 = (!empty($row->u_phone_3)) ? Util::stripTel($row->u_phone_3): "";

            $row->u_username = (!empty($row->u_phone_1)) ? $row->u_phone_1: "";
            $row->u_password = (!empty($row->u_username)) ? md5($row->u_username) : "";

            $row->u_create = date ( 'Y-m-d H:i:s' );
            $row->u_creater = $this->auth->getIdentity()->u_username;
            
            $row->u_update = date ( 'Y-m-d H:i:s' );
            $row->u_updater = $this->auth->getIdentity()->u_username;
                        
            $row->save();
            return $row;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }


    ##########################################################################################
    public function updateItem($formValues)
    {
        $row = $this->find($formValues['u_id'])->current();
        $row->setFromArray($formValues);

        if($row)
        {

            $row->u_phone_1 = (!empty($row->u_phone_1)) ? Util::stripTel($row->u_phone_1): "";
            $row->u_phone_2 = (!empty($row->u_phone_2)) ? Util::stripTel($row->u_phone_2): "";
            $row->u_phone_3 = (!empty($row->u_phone_3)) ? Util::stripTel($row->u_phone_3): "";

            $row->u_update = date ( 'Y-m-d H:i:s' );
            $row->u_updater = $this->auth->getIdentity()->u_username;

            $row->save();
            return $row;

        } else {
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {

            $objModel = new Ai_Model_Object;

            foreach($items_id as $id => $item_id)
            {
                $obj_items = $this->db->query("
                    SELECT obj_id
                    FROM obj_object AS obj
                    WHERE obj_u_id = '" . $item_id . "'"
                )->fetchAll();


                foreach ($obj_items as $arr_id => $obj_val) {

                    $obj_id = $obj_val['obj_id'];
                    $objModel->deleteItems(array($obj_id));

                }


                $row = $this->find($item_id)->current();

                if($row)
                {
                    $row->delete();
                }

                else        throw new Zend_Exception("Could not delete item. Item not found!");
            }

        }
    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function getSelectList()
    {
        $res = "";
        $items = array();

        $select =  $this->select()
                    ->where( "u_enable", 1 )
                    ;

        $rows = $this->fetchAll($select)->toArray();
        foreach($rows as $id => $val)
        {
            $items[$val['u_id']] = $val['u_name_' . $this->lang];
        }

        return $items;

    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }


    ##########################################################################################
    public function checkPhone($phone, $u_id = 0)
    {
        $phone = Util::stripTel($phone);

        $sql_u_id = ($u_id > 0) ? " AND u.u_id <> " . $u_id : "";

        $data = $this->db->query("
            SELECT u_firstname, u_lastname, u_patronomyc, u_phone_1, u_phone_2, u_phone_3
            FROM u_user as u
            WHERE (u.u_phone_1 = '".$phone."' OR u.u_phone_2 = '".$phone."' OR u.u_phone_3 = '".$phone."')" .
            $sql_u_id
        )->fetchAll();

        return $data;
    }
}
?>
