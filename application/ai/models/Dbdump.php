<?php
require_once("Util.php");

class Ai_Model_Dbdump extends Zend_Db_Table_Abstract {

    public function init()
    {
        $this->dump_dir = APPLICATION_PATH . '/data/dumps/';
        $this->prefix =  "goyug";
        $this->bkp_ext = '.backup';
        $this->zip_ext = '.zip';
        
        $this->server = (getenv('APPLICATION_ENV') == 'development') ? "local" : "server";
        $this->utils = Util::getConfig( array($this->server."_mysql", $this->server."_mysqldump", $this->server."_zip", $this->server."_unzip") );
    }

    public function getItems()
    {
        $except = array(".", "..");
        
        if(is_dir($this->dump_dir))
        {
            $res = array();
            $dirlist = scandir($this->dump_dir, 1);
            foreach($dirlist as $file)
            {
                if(!in_array($file, $except))
                {
                    $stat = stat($this->dump_dir . $file);
                    $res[] = array("name" => $file, "size" => $stat['size'], "date" => $stat['ctime']);
                }
            }
            return $res;

        }
        
    }
    
    public function createDump()
    {
        $db = Zend_Registry::get('db');
        $config = $db->getConfig();

        $sign = date("Y_m_d_H_i_s");
        $bkp_file = $this->dump_dir . $this->prefix ."_". $sign . $this->bkp_ext;
        $zip_file = $this->dump_dir . $this->prefix ."_". $sign . $this->zip_ext;
        
        $cmd = $this->utils[$this->server.'_mysqldump']." -u ".$config['username']." -p".$config['password']." -f --quick ".$config['dbname']." > ".$bkp_file. " 2>&1";
        exec($cmd, $out, $code); 
        
        $cmd = $this->utils[$this->server.'_zip']." -Djrm ".$zip_file." ".$bkp_file;
        exec($cmd, $out, $code); 
    }

    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                if(file_exists($this->dump_dir . $item_id))
                {
                    unlink($this->dump_dir . $item_id);                    
                }
            }

        }
    }        

    public function restoreDump($dumpfile)
    {
        $db = Zend_Registry::get('db');
        $config = $db->getConfig();

        $file = $this->dump_dir . $dumpfile;
        if(file_exists($file))
        {
            $cmd = $this->utils[$this->server.'_unzip']." -o ".$file." -d " . $this->dump_dir;
            exec($cmd, $out, $code); 
            ob_clean();
            
            $dump_filename = basename( preg_replace("/inflating: /", "", $out[1]) );
            
            $cmd = $this->utils[$this->server.'_mysql']." --user=".$config['username']." --password=".$config['password']." ".$config['dbname']." < " . $this->dump_dir . $dump_filename;
            exec($cmd, $out, $code); 
            
            unlink($this->dump_dir . $dump_filename);
        }
    }
        
    
    
}  
?>
