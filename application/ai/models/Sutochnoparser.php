<?php
require_once("Util.php");
require_once("nokogiri.php");

class Ai_Model_Sutochnoparser extends Zend_Db_Table_Abstract {

    //#########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        $this->domain = "http://%%%.spiti.ru";
        $this->img_path = $_SERVER['DOCUMENT_ROOT'] . "/upload/sutochno";
        $this->watermark_path = $_SERVER['DOCUMENT_ROOT'] . "/images/watermark-2.jpg";
    }

    //#########################################################################################
    public function parse($pars_city = "", $owners_list, $task_token)
    {
        if(!empty($pars_city) && !empty($owners_list))
        {
            $result = array();

            foreach($owners_list as $id => $val)
            {
                $row = explode("\t", $val);

                if(count($row) == 5)
                {
                    $obj = array();

                    list($phone_1, $phone_2, $email, $link, $name) = $row;

                    $phone_1 = ($phone_1 != 0) ? trim($phone_1) : "";
                    $phone_2 = ($phone_2 != 0) ? trim($phone_2) : "";

                    $is_goyug = $this->isGoyugOwner($phone_1, $email);
                    $is_parsed = $this->isParsedOwner($phone_1, $email);

                    if(!$is_parsed && !$is_goyug)
                    {

                        // Парсим карточку об-та
                        //  ============================================================
                        $html = file_get_contents($link);
                        $saw = new nokogiri($html);                


                        // Проверка на доступность объявления
                        //  ============================================================
                        $halt = $saw->get('.halted-gag-inner')->toArray();
                        if(sizeof($halt) == 0)
                        {

                            // Адрес объекта
                            //  ============================================================
                            $address = $saw->get('.bb .full-view')->toArray();
                            $address = explode(",", trim($address[0]['#text'][2]));

                            // Название
                            //  ============================================================
                            $obj['name'] = (isset($address[0])) ? trim($address[0]) : "";

                            // Количество комнат
                            //  ============================================================
                            $obj['rooms'] = (isset($address[0])) ? preg_replace("/[^0-9]/", "", $address[0]) : 0;

                            // Адрес - улица
                            //  ============================================================
                            $obj['addr_street'] = (isset($address[1])) ? trim($address[1]) : "";

                            // Адрес - номер дома
                            //  ============================================================
                            $obj['addr_number'] = (isset($address[2])) ? preg_replace("/[^0-9]/", "", $address[2]) : 0;
                            $addr_add = (isset($address[3])) ? trim($address[3]) : "";
                            $obj['addr_number'] .= " " . $addr_add;

                            // Адрес - район
                            //  ============================================================
                            $fulldom = $saw->get('.xt')->toArray();

                            if(!isset($fulldom[5]))
                            {
                                echo $link; die;
                            }

                            $obj['addr_district'] = trim($fulldom[5]['span'][0]['#text'][0]);
                            $obj['addr_district'] = explode(",", $obj['addr_district']);
                            $obj['addr_district'] = $obj['addr_district'][1];

                            // Площадь
                            //  ============================================================
                            $obj['sqr'] = $fulldom[1]['ul'][0]['li'][1]['#text'][1];
                            $obj['sqr'] = preg_replace("/[^0-9]/", "", $obj['sqr']);

                            // Описание
                            //  ============================================================
                            $obj['desc'] = $fulldom[3]['p'];

                            $part = array();
                            $part[] = isset($obj['desc'][0]['span'][0]['#text'][0]) ? trim($obj['desc'][0]['span'][0]['#text'][0]) : "";
                            $part[] = isset($obj['desc'][0]['#text'][2]) ? trim($obj['desc'][0]['#text'][2]) : "";
                            $part[] = isset($obj['desc'][1]['#text'][2]) ? trim($obj['desc'][1]['#text'][2]) : "";
                            $part[] = isset($obj['desc'][2]['#text'][2]) ? trim($obj['desc'][2]['#text'][2]) : "";
                            $part[] = isset($obj['desc'][3]['#text'][2]) ? trim($obj['desc'][3]['#text'][2]) : "";

                            $obj['desc'] = implode("\n\n", $part);

                            // Этажность
                            //  ============================================================
                            $floor = $fulldom[1]['ul'][0]['li'][3]['#text'][1];
                            $floor = explode (",", $floor);
                            $floor = $floor[0];
                            list($obj['addr_floor'], $obj['addr_elevation']) = explode("/", $floor);

                            // Кол-во гостей
                            //  ============================================================
                            $obj['guests'] = $fulldom[1]['ul'][0]['li'][6]['#text'][1];

                            // Кол-во спальных мест
                            //  ============================================================
                            $obj['sleepers'] = $fulldom[1]['ul'][0]['li'][2]['#text'][1];

                            // Цена
                            //  ============================================================
                            $obj['price'] = $fulldom[7]['div'][0]['div'][1]['p'][0]['span'][0]['b'][0]['#text'][0];
                            $obj['price'] = preg_replace("/[^0-9]/", "", $obj['price']);

                            // UID
                            //  ============================================================
                            $obj['uid'] = $fulldom[10]['b'][0]['#text'][0];
                            $obj['uid'] = preg_replace("/[^0-9]/", "", $obj['uid']);

                            // Ссылка на владельца
                            //  ============================================================
                            $obj['owner_link'] = "http://" . $pars_city . ".sutochno.ru" . $fulldom[10]['a'][0]['href'];

                            // Координаты
                            //  ============================================================
                            preg_match('/"point"\W+([\d]+[\.][\d]+)\W+([\d]+[\.][\d]+)\W+([\d]+[\.][\d]+)\W+([\d]+[\.][\d]+)/i', $html, $matches);
                            $obj['lat'] = (isset($matches[3])) ? round(floatval($matches[3]), 6) : 0;
                            $obj['lng'] = (isset($matches[4])) ? round(floatval($matches[4]), 6) : 0;

                            // Фотки
                            //  ============================================================
                            $imgs = $saw->get('.galery-item')->toArray();

                            if(count($imgs) > 0)
                            {
                                foreach($imgs as $i_id => $i_val)
                                {
                                    $matches = "";
                                    preg_match('/\((.+)\)/', $i_val['style'], $matches);
                                    $url = $matches[1];
                                    $url = explode("/", $url);

                                    if(sizeof($url == 7) && $url[2] == 'photo')
                                    {
                                        $url[5] = $url[5] . "/big";
                                        $url[6] = str_replace(".", "_big.", $url[6]);
                                        $url = "http://www.sutochno.ru" . implode("/", $url);
                                        $obj['imgs'][] = $url;
                                    }

                                }

                                $obj['imgs'] = trim(implode(";", $obj['imgs']));
                            
                            } else {
                                $obj['imgs'] = "";
                            }
                            

                            $result[] = $obj;

                            // Пишем а базу владельца если его нет ни в нашей базе ни в спарсенной
                            //  ============================================================
                            $tbl = new Zend_Db_Table(array(
                                'name' => 'ptu_sutochno_user',
                                'primary' => 'ptu_id'
                            ));

                            $row = $tbl->createRow(
                                array(
                                  'ptu_is_goyug' => $is_goyug,
                                  'ptu_is_avito' => '',
                                  'ptu_link' => $obj['owner_link'],
                                  'ptu_phone_1' => $phone_1,
                                  'ptu_phone_2' => $phone_2,
                                  'ptu_email' => $email,
                                  'ptu_name' => $name,
                                  'ptu_city' => $pars_city,
                                  'ptu_create' => date ( 'Y-m-d H:i:s' ),
                                  'ptu_creater' => 'parser_sutochno'
                                )
                            );
                            $row->save();

                            // Пишем а базу объект
                            //  ============================================================
                            $tbl = new Zend_Db_Table(array(
                                'name' => 'pto_sutochno_object',
                                'primary' => 'pto_id'
                            ));

                            $row = $tbl->createRow(
                                array(
                                  'pto_ptu_id' => $phone_1,
                                  'pto_link' => $link,
                                  'pto_uid' => $obj['uid'],
                                  'pto_sqr' => $obj['sqr'],
                                  'pto_rooms' => $obj['rooms'],
                                  'pto_name' => $obj['name'],
                                  'pto_desc' => $obj['desc'],
                                  'pto_sleepers' => $obj['sleepers'],
                                  'pto_guests' => $obj['guests'],
                                  'pto_images' => $obj['imgs'],
                                  'pto_addr_city_uid' => $pars_city,
                                  'pto_addr_street' => $obj['addr_street'],
                                  'pto_addr_number' => $obj['addr_number'],
                                  'pto_addr_district' => $obj['addr_district'],
                                  'pto_addr_floor' => $obj['addr_floor'],
                                  'pto_addr_elevation' => $obj['addr_elevation'],
                                  'pto_addr_lat' => $obj['lat'],
                                  'pto_addr_lng' => $obj['lng'],
                                  'pto_price' => $obj['price'],
                                  'pto_create' => date ( 'Y-m-d H:i:s' ),
                                  'pto_creater' => 'parser_sutochno'
                                )
                            );
                            $row->save();

                            // Обновляем статус задачи
                            //  ============================================================
                            if(!empty($task_token))
                            {
                                $query = "
                                    UPDATE pt_parse_tasks
                                    SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                    WHERE pt_token = '" . $task_token ."'
                                ";
                                $this->db->query($query);         
                            }
                        }
                    }
                }
            }
        }

        // print_r($result);
        // die;

    }

    //#########################################################################################
    public function download($pars_city = "", $items, $task_token)
    {
        set_time_limit(0);
        $result = array();

        if($pars_city != "" && count($items) > 0)
        {
            foreach($items as $id => $val)
            {
                if($id != "images")
                {
                    $path = $this->img_path . "/" . $pars_city . "/" . $id;
                    
                    if(!file_exists($path)) mkdir($path, 0777, true);

                    foreach($val as $img_id => $img)
                    {
                        $img_path = $path . "/" . $img_id . ".jpg";
                        if(!file_exists($img_path))
                        {
                            if(!empty($img) && !empty($img_path))
                            {
                                copy( $img , $img_path);

                                // Обновляем статус задачи
                                //  ============================================================
                                if(!empty($task_token))
                                {
                                    $query = "
                                        UPDATE pt_parse_tasks
                                        SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                        WHERE pt_token = '" . $task_token ."'
                                    ";
                                    $this->db->query($query);         
                                }

                            }

                        }
                    }

                }
            }
        }

        return $result;
    }

    //#########################################################################################
    public function countDownloadItems($pars_city = "")
    {
        $result = array();

        if(!empty($pars_city))
        {
            //Запрос
            $query = "
                SELECT pto_uid, pto_images
                FROM pto_sutochno_object AS pto
                WHERE pto.pto_addr_city_uid = '" . $pars_city ."'
            ";

            $data = $this->db->query($query)->fetchAll();

            $result['images'] = 0;
            if(count($data) > 0)
            {
                foreach($data as $id => $val)
                {
                    $result[$val['pto_uid']] = explode(";", $val['pto_images']);
                    $result['images'] = $result['images'] + count($result[$val['pto_uid']]);
                }
            }

            return $result;
        
        } else {
            return 0;
        }
    }

    //#########################################################################################
    function get($url, $ajax=false)
    {
        $ch = curl_init ();
        curl_setopt ($ch , CURLOPT_URL , $url);

        if($ajax) 
        {
           $headers = array(
               'x-requested-with: XMLHttpRequest',
               'accept: application/json, text/javascript, */*; q=0.01',
               'accept-language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,uk;q=0.2',
               'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36',
               'referer: ' . str_replace($this->mobDomain,'',$url),
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1 );
        $content = curl_exec($ch);
        curl_close($ch);
        
        return $content;
    }

    //#########################################################################################
    function get_data($url) 
    {
        $ch = curl_init();
        $timeout = 5;
    
        $agents = array(
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (iPad; CPU OS 9_3_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13E238 Safari/601.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/49.0.2623.108 Chrome/49.0.2623.108 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  Trident/5.0)",
            "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0;  Trident/5.0)",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 5.1; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/537.86.5",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
            "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12",
            "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36 OPR/36.0.2130.65",
            "Mozilla/5.0 (X11; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        // curl_setopt($ch, CURLOPT_PROXY, $proxy[array_rand($proxy)]);
    
        $data = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($status == '200')
            return $data;
        else
        {
            return $status;
        }

    }

    ##########################################################################################
    public function isGoyugOwner($phone_1, $email) {

        if(!empty($phone_1))
        {

            $phone_1 = Util::stripTel($phone_1);

            //Запрос
            $query = "
                SELECT *
                FROM u_user AS u
                WHERE u.u_phone_1 = '" . $phone_1 ."'
                OR u.u_phone_2 = '" . $phone_1 ."'
                OR u.u_phone_3 = '" . $phone_1 ."'
            ";

            if(!empty($email))
            {
                $query .= " OR u.u_email = '" . $email ."'";
            }

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function isParsedOwner($phone_1, $email) {

        if(!empty($phone_1))
        {

            $phone_1 = Util::stripTel($phone_1);

            //Запрос
            $query = "
                SELECT *
                FROM ptu_sutochno_user AS ptu
                WHERE ptu.ptu_phone_1 = '" . $phone_1 ."'
            ";

            if(!empty($email))
            {
                $query .= " OR ptu.ptu_email = '" . $email ."'";
            }

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }

    ##########################################################################################
    public function createParseJob($pars_city, $objects_num, $pages_num, $pars_page, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'sutochno',
                  'pt_action' => 'parse',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => intval($pages_num),
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => intval($pars_page),
                  'pt_cur_page' => intval($pars_page-1),
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function createDownloadJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'sutochno',
                  'pt_action' => 'download',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => '',
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function createClearingJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'sutochno',
                  'pt_action' => 'clearingimgs',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => 0,
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }
    ##########################################################################################
    public function createWriteJob($pars_city, $objects_num, $task_token) {

        if(!empty($task_token)){

            // Создаем задачу для парсинга в базе
            $tbl = new Zend_Db_Table(array(
                'name' => 'pt_parse_tasks',
                'primary' => 'pt_id'
            ));

            $row = $tbl->createRow(
                array(
                  'pt_source' => 'sutochno',
                  'pt_action' => 'writetodb',
                  'pt_token' => $task_token,
                  'pt_progress' => 0,
                  'pt_city' => $pars_city,
                  'pt_all_pages' => 0,
                  'pt_all_obj' => intval($objects_num),
                  'pt_start_page' => '',
                  'pt_cur_page' => '',
                  'pt_cur_obj' => 0,
                  'pt_start' => date ( 'Y-m-d H:i:s' ),
                  'pt_end' => date ( 'Y-m-d H:i:s' ),
                )
            );
            
            if($row->save()) return $task_token;
            else return "save error";
        } else {
            return "empty token";
        }
    }

    ##########################################################################################
    public function updateParseJob($token = "", $value) {

        if(!empty($token)){

            $query = "
                UPDATE pt_parse_tasks
                SET pt_progress = '" . $value ."', pt_end = '".date ( 'Y-m-d H:i:s' )."'
                WHERE pt_token = '" . $token ."'
            ";
            $this->db->query($query);

        }
    }

    ##########################################################################################
    public function getParseJob($token = "") {

        if(!empty($token)){

            $query = "
                SELECT *
                FROM pt_parse_tasks
                WHERE pt_token = '" . $token ."'
            ";
            $row = $this->db->query($query)->fetchObject();

            return $row;

        } else return "empty token";
    }

    ##########################################################################################
    public function generateToken() {
        return Util::generateHash();
    }

    ##########################################################################################
    public function getParsedImages($pars_city) {

        if(!empty($pars_city))
        {
            $path = $this->img_path . "/" . $pars_city;

            if(is_dir($path))
            {
                $arr = Util::dirToArray($path);

                return $arr;

            } else {
                return "folder dosent exist";
            }

        } else {
            return "empty city name";
        }
    }

    ##########################################################################################
    public function countParsedImages($items) {

        $result = 0;

        if(!empty($items))
        {

            foreach($items as $id => $val)
            {
                $result += count($val);
            }

            return $result;

        } else {

            return "items array is empty";

        }

    }


    ##########################################################################################
    public function clearImages($pars_city, $items, $task_token) 
    {
        ini_set("gd.jpeg_ignore_warning", 1);

        if(!empty($items) && count($items) > 0 && !empty($pars_city))
        {

            $path = $this->img_path . "/" . $pars_city;
            $clear_path = $this->img_path . "/_clear_/" . $pars_city;

            $stamp = imagecreatefromjpeg($this->watermark_path);

            foreach($items as $uid => $images)
            {

                foreach($images as $key => $filename)
                {
                    $origin_filename = $path . "/" . $uid . "/" . $filename;

                    $xorigin_path = $clear_path . "/" . $uid;
                    $x800_path = $clear_path . "/" . $uid . "/x800";
                    $x500_path = $clear_path . "/" . $uid . "/x500";
                    $x100_path = $clear_path . "/" . $uid . "/x100";
                    $thumb_path = $clear_path . "/" . $uid . "/thumbnail";

                    list($image_w, $image_h) = getimagesize($origin_filename);

                    if($image_w > $image_h && intval($image_w) >= 500)
                    {
                        if(!file_exists($x800_path)) mkdir($x800_path, 0777, true);
                        if(!file_exists($x500_path)) mkdir($x500_path, 0777, true);
                        if(!file_exists($x100_path)) mkdir($x100_path, 0777, true);
                        if(!file_exists($thumb_path)) mkdir($thumb_path, 0777, true);

                        // $items[$uid][$key] = array("name" => $origin_filename, "w" => $image_w, "h" => $image_h);

                        // Загрузка фото, для которого применяется водяной знак
                        if($im = @imagecreatefromjpeg($origin_filename))
                        {
                            // $true_filename = $this->generateFilename($filename);
                            $true_filename = $filename;

                            imagecopy($im, $stamp, imagesx($im) - 126, 0, 0, 0, imagesx($stamp), imagesy($stamp));
                            
                            imagejpeg($im, $x800_path . "/" . $true_filename, 85);
                            imagejpeg($im, $xorigin_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize x500
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 500, 375);
                            imagejpeg($im, $x500_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize x100
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 100, 75);
                            imagejpeg($im, $x100_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Resize Thumbnail (200 x 150)
                            $im = $this->resizeImage($x800_path . "/" . $true_filename, 200, 150);
                            imagejpeg($im, $thumb_path . "/" . $true_filename, 85);
                            imagedestroy($im);
                            
                            // Update Job Status
                            $query = "
                                UPDATE pt_parse_tasks
                                SET pt_progress = ROUND((pt_cur_obj / pt_all_obj) * 100), pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                WHERE pt_token = '" . $task_token ."'
                            ";
                            $this->db->query($query);         

                        } else {
                            // unlink($origin_filename);
                        }
                        // break;
                    
                    } else {
                        // unset($items[$uid][$key]);
                    }
                
                } // foreach $images
                
            // break;
            }  // foreach $items

            return $items;

        } else {
            return "items array is empty";
        }

    }   

    ##########################################################################################
    public function resizeImage($filename, $width, $height) 
    {
        $image = imagecreatefromjpeg($filename);
        list($image_w, $image_h) = getimagesize($filename);

        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, $image_w, $image_h);

        return $new_image;
  }

    ##########################################################################################
    public function generateFilename($filename) 
    {
        if(!empty($filename))
        {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            list($usec, $sec) = explode(" ", microtime());
            $usec = $usec  * 1000000;
            $microtime = $sec . "-" . $usec;

            $name = date("Y-m-d-H-i") . "-" . $microtime . "." . $ext;        

            return $name;
        } else {
            return 0;
        }

    }   

    ##########################################################################################
    public function addOwner($params) {

        $tbl = new Zend_Db_Table(array(
            'name' => 'u_user',
            'primary' => 'u_id'
        ));

        if(!empty($params['ptu_phone_1'])) {

                $email = Util::strtolower_utf8(trim($params['ptu_email']));
                $phone = Util::stripTel($params['ptu_phone_1']);

                $row = $tbl->createRow();
                $row->u_lf_id = 1;
                $row->u_active = 1;
                $row->u_tutorial = 0;
                $row->u_username = $phone;
                $row->u_password = md5($phone);

                $row->u_authtoken = "";
                $row->u_money = 0;

                // $row->u_action_enable = 0;
                // $row->u_action_enabled = 0;
                // $row->u_action_expire = "";
                // $row->u_action_expired = 0;
                // $row->u_action_type = "";
                // $row->u_action_stage = "";
                // $row->u_action_actual_to = "";
                // $row->u_action_activate = "";
                // $row->u_action_deactivate = "";
                // $row->u_action_deactivated = "";

                $row->u_subscribe_news = 1;
                $row->u_subscribe_email = 1;
                $row->u_subscribe_sms = 1;

                $row->u_show_email = 1;

                $row->u_firstname = $params['ptu_name'];
                $row->u_url = $params['ptu_link'];

                $row->u_email = $email;
                $row->u_phone_1 = $phone;
                $row->u_phone_2 = $params['ptu_phone_2'];
                $row->u_role = 'owner';
                $row->u_is_phoned = 0;

                $row->u_isnew = 0;

                $row->u_create = date('Y-m-d H:i:s');
                $row->u_update = date('Y-m-d H:i:s');

                $row->u_creater = 'parser_sutochno';
                $row->u_updater = 'parser_sutochno';

                $owner_id = $row->save();

                return $owner_id;

        }
    }
    ##########################################################################################
    public function addObject($params) {

        $cities = array(
                                // 0 wave
                                "astrakhan" => "Астрахань", 
                                "kislovodsk" => "Кисловодск", 
                                "krasnodar" => "Краснодар", 
                                "pyatigorsk" => "Пятигорск", 
                                "rostovnadonu" => "Ростов-на-Дону", 
                                "sochi" => "Сочи", 
                                "stavropol" => "Ставрополь",

                                // 1 wave
                                "anapa" => "Анапа",
                                "gelendzhik" => "Геленджик",
                                "zheleznovodsk" => "Железноводск",
                                "novorossisk" => "Новороссийск",
                                "pyatigorsk" => "Пятигорск",
                                "sevastopol" => "Севастополь",
                                "simferopol" => "Симферополь",
                                "yalta" => "Ялта",

                                // 2 wave
                                "astrakhan" => "Астрахань",
                                "volgograd" => "Волгоград",
                                "volgodonsk" => "Волгодонск",
                                "volzhsky" => "Волжский",
                                "voronezh" => "Воронеж",
                                "novocherkassk" => "Новочеркасск",
                                "taganrog" => "Таганрог",

                                // 3 wave
                                "alushta" => "Алушта",
                                "evpatoria" => "Евпатория",
                                "feodosia" => "Феодосия",
                                "ufa" => "Уфа",
                                "samara" => "Самара",
                                "kazan" => "Казань",
                                "nnovgorod" => "Нижний Новгород",
                                "ekaterinburg" => "Екатеринбург",
                                "saratov" => "Саратов",
                                "samara" => "Самара",
                    
                                // 4 wave
                                "omsk" => "Омск",
                                "irkutsk" => "Иркутск",
                                "yaroslavl" => "Ярославль",
                                "chelyabinsk" => "Челябинск",
                                "tyumen" => "Тюмень",
                                "spb" => "Санкт-Петербург",
                                "moscow" => "Москва",

                                // 5 wave
                                "krasnoyarsk" => "Красноярск",
                                "novosibirsk" => "Новосибирск",
                                "perm" => "Пермь",
                                "surgut" => "Сургут",
                                "tomsk" => "Томск",
                                "kemerovo" => "Кемерово",
                                "smolensk" => "Смоленск",
                                "khabarovsk" => "Хабаровск",
                                "barnaul" => "Барнаул",
                                "novokuznetsk" => "Новокузнецк",
                                "nvartovsk" => "Нижневартовск",
                                "orenburg" => "Оренбург",
                                "belgorod" => "Белгород",
                                "tula" => "Тула",
                                "kirov" => "Киров",
                                "izhevsk" => "Ижевск",
                                "kursk" => "Курск",

                                // 6 wave
                                "vladivostok"   =>"Владивосток",
                                "vladimir"  =>"Владимир",
                                "kaliningrad"   =>"Калининград",
                                "magnitogorsk"  =>"Магнитогорск",
                                "penza" =>"Пенза",
                                "salekhard" =>"Салехард",
                                "saransk"   =>"Саранск",
                                "syktyvkar" =>"Сыктывкар",
                                "tolyatty"  =>"Тольятти",
                                "ulanude"   =>"Улан-Удэ",
                                "cheboksary"    =>"Чебоксары",
                                "chita" =>"Чита",

                                // 7 wave
                                "arkhangelsk"=>"Архангельск",
                                "bryansk"=>"Брянск",
                                "vologda"=>"Вологда",
                                "ivanovo"=>"Иваново",
                                "kaluga"=>"Калуга",
                                "kurgan"=>"Курган",
                                "lipetsk"=>"Липецк",
                                "orel"=>"Орел",
                                "pskov"=>"Псков",
                                "ryazan"=>"Рязань",
                                "tver"=>"Тверь",
                                "ulyanovsk"=>"Ульяновск",

                                // 8 wave
                                "alushta"=>"Алушта",
                                "blagoveschensk"=>"Благовещенск",
                                "vnovgorod"=>"Великий Новгород",
                                "volzhsky"=>"Волжский",
                                "evpatoria"=>"Евпатория",
                                "yoshkarola"=>"Йошкар-Ола",
                                "kemerovo"=>"Кемерово",
                                "kostroma"=>"Кострома",
                                "nchelni"=>"Набережные Челны",
                                "nefteyugansk"=>"Нефтеюганск",
                                "nizhnekamsk"=>"Нижнекамск",
                                "ntagil"=>"Нижний Тагил",
                                "novocherkassk"=>"Новочеркасск",
                                "novyurengoy"=>"Новый Уренгой",
                                "petrozavodsk"=>"Петрозаводск",
                                "rybinsk"=>"Рыбинск",
                                "staryoskol"=>"Старый Оскол",
                                "taganrog"=>"Таганрог",
                                "tambov"=>"Тамбов",
                                "tobolsk"=>"Тобольск",
                                "ukhta"=>"Ухта",
                                "khantymansiysk"=>"Ханты-Мансийск",
                                "cherepovets"=>"Череповец",
                                
                                
        );

        $elevation = array(
            "5", "9", "11", "16"
            );

        $tbl = new Zend_Db_Table(array(
            'name' => 'obj_object',
            'primary' => 'obj_id'
        ));

        if(!empty($params['u_id'])) 
        {
                // $obj_addr_floor = (intval($params['pto_addr_floor']) > 0) ? $params['pto_addr_floor'] : 3;
                
                // $obj_addr_elevation = $elevation[array_rand($elevation, 1)];
                // while ($obj_addr_floor > $obj_addr_elevation) 
                // {
                //     $obj_addr_elevation = $elevation[array_rand($elevation, 1)];
                // }

                $row = $tbl->createRow();
                
                $row->obj_enable = 1;
                $row->obj_phantom = 0;
                
                $row->obj_ontop = 0;
                $row->obj_ontop_mark = 0;
                $row->obj_ontop_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_photomark = 0;
                $row->obj_photomark_activate = '0000-00-00 00:00:00';
                $row->obj_photomark_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_mark_out = 0;
                $row->obj_mark_out_deactivate = '0000-00-00 00:00:00';
                
                $row->obj_isempty = 0;
                $row->obj_isempty_activate = '0000-00-00 00:00:00';
                $row->obj_isempty_nextpay = '0000-00-00 00:00:00';
                $row->obj_isempty_deactivate = '0000-00-00 00:00:00';

                $row->obj_sale = 0;
                $row->obj_sale_activate = '0000-00-00 00:00:00';
                $row->obj_sale_nextpay = '0000-00-00 00:00:00';
                $row->obj_sale_deactivate = '0000-00-00 00:00:00';



                $row->obj_hotorder = 1;
                $row->obj_hotorder_activate = date('Y-m-d H:i:s');
                $row->obj_hotorder_nextpay = date('Y-m-d H:i:s', strtotime(' + 30 days'));
                $row->obj_hotorder_deactivate = date('Y-m-d H:i:s', strtotime(' + 30 days'));

                $row->obj_paramsorder = 1;
                $row->obj_paramsorder_activate = date('Y-m-d H:i:s');
                $row->obj_paramsorder_nextpay = date('Y-m-d H:i:s', strtotime(' + 30 days'));
                $row->obj_paramsorder_deactivate = date('Y-m-d H:i:s', strtotime(' + 30 days'));

                $row->obj_virtual = 0;
                $row->obj_moderated = 1;
                
                $row->obj_uid = Util::generateUID();
                $row->obj_u_id = $params['u_id'];

                $row->obj_sqr = $params['pto_sqr'];
                $row->obj_rooms = $params['pto_rooms'];
                $row->obj_sleepers = $params['pto_sleepers'];
                $row->obj_guests = $params['pto_guests'];
                $row->obj_checkin = "12:00";
                $row->obj_checkout = "12:00";
                $row->obj_url = $params['pto_link'];
                $row->obj_name_ru = $params['pto_name'];
                $row->obj_fulldesc_ru = $params['pto_desc'];
                $row->obj_search_address = $cities[$params['pto_addr_city_uid']];
                $row->obj_addr_country_code = "RU";
                $row->obj_addr_country = "Россия";
                $row->obJ_addr_area_2 = $params['pto_addr_district'];

                $row->obj_addr_city_uid = $params['pto_addr_city_uid'];
                $row->obj_addr_city = $cities[$params['pto_addr_city_uid']];
                $row->obj_addr_street = $params['pto_addr_street'];
                $row->obj_addr_number = $params['pto_addr_number'];
                
                $row->obj_addr_floor = $params['pto_addr_floor'];
                $row->obj_addr_elevation = $params['pto_addr_elevation'];

                $row->obj_addr_lat = $params['pto_addr_lat'];
                $row->obj_addr_lon = $params['pto_addr_lng'];
                $row->obj_addr_lat_view = $params['pto_addr_lat'];
                $row->obj_addr_lon_view = $params['pto_addr_lng'];
                
                $row->obj_price = $params['pto_price'];
                $row->obj_price_period = 2;
                $row->obj_acc_type = 1;
                $row->obj_price_minstay = 1;
                $row->obj_deposit = 0;

                $row->obj_create = date('Y-m-d H:i:s');
                $row->obj_update = date('Y-m-d H:i:s');

                $row->obj_creater = 'parser_sutochno';
                $row->obj_updater = 'parser_sutochno';

                $object_id = $row->save();

                // Добавляем данные в таблицу v_val
                $tbl = new Zend_Db_Table(array(
                    'name' => 'v_value',
                    'primary' => 'v_id'
                ));

                $row_2 = $tbl->createRow(
                    array(
                      'v_table' => 'ot_object_type',
                      'obj_id' => $object_id,
                      'v_key' => '10',
                      'v_val' => '',
                    )
                );
                $row_2->save();

                return $object_id;

        }
    }

    ##########################################################################################
    public function addImages($src_id, $dst_id, $pars_city) 
    {
        $src_path = $this->img_path . "/_clear_/" . $pars_city . "/" . $src_id;
        // $dst_path = $this->img_path . "/_clear_/" . $pars_city . "/" . $dst_id;
        $dst_path = $this->img_path . "/_write_/" . $dst_id;

        // Util::recurse_copy($src_path, $dst_path);
        rename($src_path, $dst_path);

        $tbl = new Zend_Db_Table(array(
            'name' => 'f_files',
            'primary' => 'f_id'
        ));

        foreach (scandir($dst_path) as $node) {
            if ($node != '.' && $node != '..' && !is_dir($src_path . '/' . $node))
            {
                $row = $tbl->createRow();

                $row->f_datetime = date('Y-m-d H:i:s');
                $row->f_ucid = 'object';
                $row->f_uid = $dst_id;
                $row->f_name = $node;
                $row->f_title = $pars_city;
                $row->f_description = "parser_sutochno";
                $row->f_size = filesize($dst_path . '/' . $node);
                $row->f_type = "image/jpeg";
                $row->f_order = 999;

                $row->save();
            }
        }

    }


    //#########################################################################################
    public function writeToDB($pars_city, $task_token)
    {
        set_time_limit(0);
        $result = array();
        $obj_ids = array();

        $imgdir = $this->img_path . '/_clear_/' . $pars_city;

        // Получаем список спарсенных об-тов с привязкой к владельцу
        $pto_items = $this->getParsedObjects($pars_city);

        if(count($pto_items) > 0)
        {
            foreach($pto_items as $pto_ind => $pto_val)
            {
                // Проверяем есть ли у объекта "чистые" фотки
                // Если фотки есть: начинаем процедуру копирования объекта в "живую" базу
                if(is_dir($imgdir . '/' . $pto_val['pto_uid']))
                {

                    $owner_id = $this->addOwner($pto_val);

                    $pto_val['u_id'] = $owner_id;

                    // Если получили ID владельца начинаем копировать объекты
                    if($owner_id > 0) 
                    {
                        if(!$this->existParsedObject($pto_val['pto_link']))
                        {
                            $object_id = $this->addObject($pto_val);
                            $this->addImages($pto_val['pto_uid'], $object_id, $pars_city);

                            // Update Job Status
                            $query = "
                                UPDATE pt_parse_tasks
                                SET pt_progress = 0, pt_cur_obj = pt_cur_obj +1, pt_end = '".date ( 'Y-m-d H:i:s' )."'
                                WHERE pt_token = '" . $task_token ."'
                            ";
                            $this->db->query($query);         

                        }

                    }

                } // IF exist img_dir
            } // foreach
        } // if array not empty

    }

    ##########################################################################################
    public function existParsedObject($url) {

        if(!empty($url)){

            //Запрос
            $query = "
                SELECT *
                FROM obj_object
                WHERE obj_url = '" . $url ."'
                AND obj_creater = 'parser_sutochno'
            ";

            $data = $this->db->query($query)->fetchObject();

            if($data) return 1;
            else return 0;

        } else {
            return 0;
        }
    }


    //#########################################################################################
    public function getParsedObjects($pars_city)
    {
        if(!empty($pars_city))
        {
            $query = "
                SELECT *
                FROM ptu_sutochno_user AS ptu

                LEFT JOIN pto_sutochno_object AS pto
                ON ptu.ptu_phone_1 = pto.pto_ptu_id

                WHERE ptu.ptu_city =  '" . $pars_city ."'
            ";

            $data = $this->db->query($query)->fetchAll();

            if(!empty($data) && count($data) > 0)
            {
                return $data;
            
            } else {
                return 0;
            }

        } else {
            return 0;
        }

    }

}  
?>
