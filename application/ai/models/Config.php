<?php
class Ai_Model_Config extends Zend_Db_Table_Abstract {

    public $_name = 'c_config';
    public $_primary = 'c_id';

    ##########################################################################################
    public function getItems()
    {
        $select = $this->select();
        $select->order(array('c_name', 'c_desc', 'c_value'));
        return $this->fetchAll($select);
    }
    
    ##########################################################################################
    public function createItem($formValues)
    {
        $auth = Zend_Auth::getInstance();
         
        $row = $this->createRow($formValues);
        if($row) {
            
            $row->save();
            
            return $row;
            
        } else {
            
            throw new Zend_Exception("Could not create item!");
        
        }
    }

    ##########################################################################################
    public function updateItem($formValues)
    {
         $auth = Zend_Auth::getInstance();

        // fetch the user's row
        $row = $this->find($formValues['c_id'])->current();
        
        if($row) {
            // update the row values
            $row->c_name = $formValues['c_name'];
            $row->c_desc = $formValues['c_desc'];
            $row->c_value = $formValues['c_value'];
            $row->save();

            //return the updated user
            return $rowUser;

        }else{
            throw new Zend_Exception("Item update failed. Item not found!");
        }

    }

    ##########################################################################################
    public function deleteItem($id)
    {
        // fetch the user's row
        $row = $this->find($id)->current();
        
        if($row) {
            $row->delete();
        }else{
            throw new Zend_Exception("Could not delete item. Item not found!");
        }
    }    

    ##########################################################################################
    public function cloneItem($id)
    {
        if(!empty($id))
        {
            $row = $this->find($id)->current()->toArray();
            unset($row['c_id']);
            $row['c_name'] .= " -COPY-";
            $row['c_desc'] .= " -COPY-";
            $this->createItem($row);
        }
    }
    


}  
?>
