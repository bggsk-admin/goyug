<?php
class Ai_Model_Smartfilters extends Zend_Db_Table_Abstract {

    public $_name = 'sf_smartfilters';
    public $_primary = 'sf_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $locale  = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref     = "sf_";
        $this->id       = $this->pref . 'id';
        $this->name     = $this->pref . 'name';
        $this->ucid     = "smartfilters";
    }
    ##########################################################################################
    public function getItems($where = array(), $order = array())
    {
            $select = $this->select()          
                    ->from(array('sf_smartfilters', array('*') ));        

            if(!empty($where)) foreach ( $where as $condition ) { $select->where( $condition ); }
            if(!empty($order)) $select->order( $order );
            
            $items = $this->fetchAll($select)->toArray();

            foreach($items as $id => $val)
            {
                $items[$id]['sf_objtypename'] = (!empty($val['ot_id'])) ? implode(",", $this->getObjecttypeNames($val['ot_id']) ) : "";
                $items[$id]['sf_sftypename'] = (!empty($val['sft_id'])) ? implode(",", $this->getSftypeNames($val['sft_id']) ) : "";
            }


            return $items;
    }

        ##########################################################################################
        public function getObjecttypeNames($values)
        {
            $items = array();
            $tbl = new Zend_Db_Table(array('name' => 'ot_object_type', 'primary'=> 'ot_id'));

            $rows = $tbl->fetchAll(
                $tbl->select()
                ->from( array( 'ot_object_type' ), array( 'ot_name_ru' ) )
                ->where( 'ot_id IN (?)', $values )
            )->toArray();

            foreach($rows as $id => $val)
            {
                $items[] = $val['ot_name_ru'];
            }

            return $items;
        }

        ##########################################################################################
        public function getSftypeNames($values)
        {
            $items = array();
            $tbl = new Zend_Db_Table(array('name' => 'sft_smartfilters_type', 'primary'=> 'sft_id'));

            $rows = $tbl->fetchAll(
                $tbl->select()
                ->from( array( 'sft_smartfilters_type' ), array( 'sft_name' ) )
                ->where( 'sft_id IN (?)', $values )
            )->toArray();

            foreach($rows as $id => $val)
            {
                $items[] = $val['sft_name'];
            }

            return $items;
        }

    ##########################################################################################
    public function getItemsForSelect($where = array(), $order = array())
    {
            $items = array();
            $res = array();

            $select = $this->select()          
                    ->from(array('sf_smartfilters', array('*') ));        
    
            if(!empty($where)) foreach ( $where as $condition ) { $select->where( $condition ); }
            if(!empty($order)) $select->order( $order );
            
            $items = $this->fetchAll($select)->toArray();

            foreach($items as $i_id => $i_val)
            {
                $res[$i_val['sf_id']] = $i_val['sf_name'];
            }

            return $res;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }


    ##########################################################################################
    public function updateItem($formValues)
    {
            if(isset($formValues['sf_id']) && !empty($formValues['sf_id']))
            {
                $row = $this->find($formValues['sf_id'])->current();
                $row->setFromArray($formValues);
            }
            else
            {
                $row = $this->createRow($formValues);
                $row->sf_create = date('Y-m-d H:i:s');
                $formValues['sf_create'] = date('d.m.Y H:i:s');
            }

            if($row)
            {
                $row->sf_update = $formValues['sf_update'] = date('Y-m-d H:i:s');
                $formValues['sf_update'] = date('d.m.Y H:i:s');

                $formValues['sf_id'] = $row->save();

                return $formValues;

            } else {
                throw new Zend_Exception("Item update failed. Item not found!");
            }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();

                if($row)
                {
                    $row->delete();
                }
                else        throw new Zend_Exception("Could not delete item. Item not found!");
            }

        }
    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }

    ##########################################################################################
    public function rebuildItems()
    {
        $db = Zend_Registry::get( 'db' );

        $sf = $db->query("
            SELECT sf.sf_id 
            FROM sf_smartfilters AS sf
        "
        )->fetchAll();

        foreach($sf as $sf_ind => $sf_val)
        {
            $sf_id = $sf_val["sf_id"];

            $price_range = $db->query("
                SELECT min(obj.obj_price) AS minprice, max(obj.obj_price) AS maxprice, obj.obj_price_period AS per_id
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.sf_id = " . $sf_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();

            $price_range = (count($price_range) > 0) ? $price_range[0] : 0;

            $obj_count = $db->query("
                SELECT COUNT(obj.obj_id) AS obj_count
                FROM sfo_smartfilters_objects AS sfo
                LEFT JOIN obj_object AS obj
                ON sfo.obj_id = obj.obj_id
                WHERE sfo.sf_id = " . $sf_id ."
                AND obj.obj_enable = 1"
            )->fetchAll();

            $obj_count = (count($obj_count) > 0) ? $obj_count[0]["obj_count"] : 0;

            //UPDATE sf_smartfilters
            $tbl = new Zend_Db_Table(array('name' => 'sf_smartfilters', 'primary' => 'sf_id'));
            
            $row = $tbl->find($sf_id)->current();
            if ($row) 
            {
                $row->sf_minprice = $price_range["minprice"];
                $row->sf_maxprice = $price_range["maxprice"];
                $row->per_id = $price_range["per_id"];
                $row->sf_objects = $obj_count;
            }
            $row->save();

        }

        return "OK";
    }

    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }

        ##########################################################################################
        public function getImages($object_id = 0)
        {
            $tbl = new Zend_Db_Table(array('name' => 'f_files', 'primary' => 'f_id'));

            $items = array();
            
            if($object_id > 0)
            {
                $items = $tbl->fetchAll(
                    $tbl->select()->from( array('f_files'), array('f_name') )
                    ->where('f_ucid = ?', $this->ucid)
                    ->where('f_uid = ?', $object_id)
                )->toArray();
                
            }
            
            return $items;
        }

        ##########################################################################################
        public function getNextID()
        {
            $res = $this->getAdapter()->query("SHOW TABLE STATUS LIKE 'sf_smartfilters';");
            $item = $res->fetchObject();

            return $item->Auto_increment;
        }



}
?>
