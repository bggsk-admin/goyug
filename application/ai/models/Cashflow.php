<?php

/* Модель биллинга */

/*

Названия транзакций:
add - пополнение баланса
showtel - просмотр номера телефона
showtelext - просмотр номера телефона с формой данных
sendorder - отправка заявки по объекту

*/

require_once("Util.php");

class Ai_Model_Cashflow extends Zend_Db_Table_Abstract {

  ##########################################################################################
  public $_name = 'cf_cashflow';
  public $_primary = 'cf_id';

  //Пароль Яндекс Касса
  public $SHOP_PASSWORD = 'gYti72359hibfc6Fytf8';

  //Режим безопасности
  public $SECURITY_TYPE = 'MD5'; //Или PKCS7;

  //Идентификатор магазина
  public $SHOP_ID = '104217';

  //Идентификатор витрины
  //public $SCID = '528198';
  public $SCID = '37184'; // '528198';

  //Обработчик запросов
  //public $ACTION = 'https://demomoney.yandex.ru/eshop.xml';
  public $ACTION = 'https://money.yandex.ru/eshop.xml';

  //ДЕМО-режим
  public $DEMO = false;

  //Стоимость поднятия объекта в рублях
  public $ontop_price = 25;

  //Лог транзакций
  private $transaction_log = Array();


  ##########################################################################################
  public function init() {

    //Данные входа
    $this->auth = Zend_Auth::getInstance();
    $this->identity = ($this->auth->hasIdentity()) ? $this->auth->getIdentity() : false;

    //База данных
    $this->db = Zend_Registry::get('db');

    //Локаль
    $locale = new Zend_Session_Namespace('locale');

    //Язык
    $this->lang = $locale->curlocale['lang'];

    //Логи
    $this->log = new Ai_Model_Log();
  }

  //Пополнение баланса
  public function add($sum, $u_id) {

    //Сумма пополнения
    $sum = intval($sum);

    //Результат операции
    $result = false;

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $u_id,
      "cf_transaction" => "add",
      "cf_table" => "u_user",
      "cf_table_id" => "",
      "cf_sum" => $sum,
      "cf_mode" => "plus",
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);

    //Если транзакция зарегистрирована
    if($resultTransaction) {

      ////Обновление баланса пользователя
      $tbl_u_user = new Zend_Db_Table(Array(
          'name' => 'u_user',
          'primary' => 'u_id'
      ));

      //Пользователь
      $row_u_user = $tbl_u_user->find($u_id)->current();
      $row_u_user->u_money += $sum;

      //Обновление в сессии
      $this->identity->u_money = $row_u_user->u_money;

      //Сохранение пользователя
      $resultUserUpdate = $row_u_user->save();
      ////Обновление баланса пользователя - окончание

      //Успех транзакции
      if($resultUserUpdate) {
          $cf_success = true;
      } else {
          $cf_success = false;
      }

      //Обновляем транзакцию
      $row_cf_cashflow = $this->find($resultTransaction)->current();
      $row_cf_cashflow->cf_success = $cf_success;
      $row_cf_cashflow->save();

      //Возвращаем результат
      $result = $cf_success;
    }

    return $result;
  }

  //Платеж за услугу
  public function pay($pay) {

    //$pay = Array("u_id", "obj_id", "transaction_name", "sum", "table", "table_id");
    //$pay             - массив операции списания
    //u_id             - пользователь, с которого необходимо списать услугу
    //obj_id           - объект, где произошло списание за услугу
    //transaction_name - название транзакции, см. шапку
    //sum              - сумма операции
    //table            - таблица, где можно посмотреть статистику
    //table_id         - ID записи статистики

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $pay['u_id'],
      "cf_obj_id" => $pay['obj_id'],
      "cf_transaction" => $pay['transaction_name'],
      "cf_table" => $pay['table'],
      "cf_table_id" => $pay['table_id'],
      "cf_sum" => $pay['sum'],
      "cf_mode" => "minus",
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);

    //Если транзакция зарегистрирована
    if($resultTransaction) {

      ////Обновление баланса пользователя
      $tbl_u_user = new Zend_Db_Table(Array(
          'name' => 'u_user',
          'primary' => 'u_id'
      ));

      //Пользователь
      $row_u_user = $tbl_u_user->find($pay['u_id'])->current();

      //Вычитаем денежку со счета
      $row_u_user->u_money -= $pay['sum'];

      //Если баланс ушел в минус - обнуляем его
      if($row_u_user->u_money < 0) $row_u_user->u_money = 0;

      //Обновление в сессии
      $this->identity->u_money = $row_u_user->u_money;

      //Сохранение пользователя
      $resultUserUpdate = $row_u_user->save();
      ////Обновление баланса пользователя - окончание

      //Успех транзакции
      if($resultUserUpdate) {
          $cf_success = true;
      } else {
          $cf_success = false;
      }

      //Обновляем транзакцию
      $row_cf_cashflow = $this->find($resultTransaction)->current();
      $row_cf_cashflow->cf_success = $cf_success;
      $row_cf_cashflow->save();

      //Возвращаем результат
      $result = $cf_success;
    }
  }

  //Регистрация транзакции
  public function register($info) {

    //$info = Array("u_id", "obj_id", "transaction_name", "sum");
    //$info            - массив операции
    //u_id             - пользователь, с которого необходимо списать услугу
    //obj_id           - объект, где произошло списание за услугу
    //transaction_name - название транзакции, см. шапку
    //sum              - сумма операции
    //success          - статус транзакции

    //Транзакция
    $transaction = Array(
      "cf_u_id" => $info['u_id'],
      "cf_obj_id" => $info['obj_id'],
      "cf_transaction" => $info['transaction_name'],
      "cf_table" => "",
      "cf_table_id" => "",
      "cf_sum" => $info['sum'],
      "cf_mode" => "balanceoff",
      "cf_success" => $info['success'],
      "cf_date" => date("Y-m-d H:i:s")
      );

    //Выполнение транзакции
    $resultTransaction = $this->insert($transaction);
  }

  //Поднятие наверх
  public function ontop($params) {

    $obj_id = intval($params['obj_id']);

    $tbl = new Zend_Db_Table(array(
      'name' => 'obj_object',
      'primary' => 'obj_id'
    ));

    //Срок выделения в днях
    $days = 3;

    //Метка поднятия с рандомным разбросом от 1-й до 10 минут
    $timeStamp = time();
    $timeStamp = $timeStamp - rand(60, 600);

    //Выделено до
    $markedTo = date("Y-m-d H:i:s", $timeStamp + $days*(60*60*24));

    //Обновляем объекты
    if($obj_id){

      $row = $tbl->find($obj_id)->current();
      $row->obj_ontop = $timeStamp;
      $row->obj_ontop_mark = 1;
      $row->obj_ontop_deactivate = $markedTo;
      $row->save();
    }

    //Ответ
    $response = Array( "status" => true, "obj_id" => $obj_id, "markedTo" => Util::getHumanDate($markedTo, "-", true, true) );

    //--------------- Logger--------------------
    $this->log->write(array('status' => 'success', 'result' => 'ontop_dummy'));

    return $response;
  }

  //Статистика расчетов
  public function getBilling($from = 0, $limit = 5, $u_id = null) {

    //Текущий пользователь
    if($u_id == null) {
      $u_id = $this->identity->u_id;
    }
    //Произвольный пользователь
    else {
      $u_id = intval($u_id);
    }

    //Статистика заявок
    $rows = $this->db->query("SELECT cf_cashflow.*
                              FROM cf_cashflow
                              /*WHERE cf_cashflow.cf_u_id = '$u_id'*/
                              ORDER BY cf_date DESC
                              LIMIT $from, $limit")->fetchAll();

    return $rows;
  }

  //Возвращает сформированный ответ для запроса paymentAviso
  public function paymentAviso($request) {

    $response = $this->buildResponse($request['action'], $request['invoiceId'], 0);
    $this->log('resultMethodPaymentAviso', $response);

    //Бизнес-логика
    if(!$this->DEMO){

      //Номер заказа
      //составляется из названия транзакции, номера объекта и хеша md5()
      //например, ontop-2455-hygg6gryg6543fgrstrhje6trgfnvht62
      $orderNumber = $request['orderNumber'];
      $orderNumberArray = explode('-', $orderNumber);

      //Сумма платежа
      $orderSum = $request['orderSumAmount'];

      //Идентификатор плательщика - предстваляет из себя u_id
      $u_id = $request['customerNumber'];

      //Название транзакции
      $transaction_name = $orderNumberArray[0];

      //id объекта
      $obj_id = $orderNumberArray[1];

      //Действуем
      switch ($transaction_name) {

        //Поднятие объекта
        case 'ontop':

          //Выполняем транзакцию
          $result = $this->ontop(Array("obj_id" => $obj_id));

          ////Регистрация транзакции
          //Ошибка поднятия
          if(!$result['status']){
            $success = false;
            mail("marselos@gmail.com", "Не удалось поднять объект", "Объект: ".$obj_id." время выполнения: ".date("Y-m-d H:i:s"));
          }
          //Успех поднятия
          else {
            $success = true;
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => $obj_id,
            "transaction_name" => 'ontop',
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        //Пополнение баланса
        case 'payment':

          //Выполняем транзакцию
          $result = $this->add($orderSum, $u_id);

          ////Регистрация транзакции
          //Ошибка пополнения баланса
          if(!$result){
            $success = false;
            mail("marselos@gmail.com", "Не удалось зафиксировать пополнение баланса", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").", сумма пополнения: ".$orderSum);
          }
          //Успех пополнения баланса
          else {
            $success = true;
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => '',
            "transaction_name" => 'payment',
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        //Благодарочка
        case 'thanks':

          //Выполняем транзакцию
          $result = true;

          ////Регистрация транзакции
          //Ошибка пополнения баланса
          if(!$result){
            $success = false;
            mail("marselos@gmail.com", "Не удалось зафиксировать благодарность", "Владелец: ".$u_id." время выполнения: ".date("Y-m-d H:i:s").", сумма оплаты: ".$orderSum);
          }
          //Успех пополнения баланса
          else {
            $success = true;
          }
          $this->register(Array(
            "u_id" => $u_id,
            "obj_id" => '',
            "transaction_name" => 'thanks',
            "sum" => $orderSum,
            "success" => $success
            ));

          break;

        default:
          # code...
          break;
      }
    }

    return $response;
  }
}
