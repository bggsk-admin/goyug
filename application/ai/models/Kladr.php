<?php
class Ai_Model_Kladr extends Zend_Db_Table_Abstract {

    public $_name = 'kl_kladr';
    public $_primary = 'kl_id';

    ##########################################################################################
    public function init()
    {
        $this->db = Zend_Registry::get( 'db' );
        $locale  = new Zend_Session_Namespace('locale');

        $this->lang = $locale->curlocale['lang'];

        $this->pref     = "kl_";
        $this->id       = $this->pref . 'id';
        $this->name     = $this->pref . 'name';
        $this->ucid     = "kladr";
    }
    ##########################################################################################
    public function getItems($where = array(), $order = array())
    {
            $select = $this->select()          
                    ->from(array('kl_kladr', array('*') ));        

            if(!empty($where)) foreach ( $where as $condition ) { $select->where( $condition ); }
            if(!empty($order)) $select->order( $order );
            
            $items = $this->fetchAll($select)->toArray();

            return $items;
    }

    ##########################################################################################
    public function createItem($formValues)
    {
        $rowItem = $this->createRow($formValues);
        if($rowItem) {

            $rowItem->save();
            return $rowItem;

        } else {

            throw new Zend_Exception("Could not create item!");

        }
    }


    ##########################################################################################
    public function updateItem($formValues)
    {
            if(isset($formValues['kl_id']) && !empty($formValues['kl_id']))
            {
                $row = $this->find($formValues['kl_id'])->current();
                $row->setFromArray($formValues);
            }

            if($row)
            {
                $formValues['kl_id'] = $row->save();
                return $formValues;
            } else {
                throw new Zend_Exception("Item update failed. Item not found!");
            }

    }

    ##########################################################################################
    public function deleteItems($items_id)
    {
        if(count($items_id) > 0)
        {
            foreach($items_id as $id => $item_id)
            {
                $row = $this->find($item_id)->current();

                if($row)
                {
                    $row->delete();
                }
                else        throw new Zend_Exception("Could not delete item. Item not found!");
            }

        }
    }

    ##########################################################################################
    public function cloneItem($id)
    {
        if($id > 0)
        {
            $row = $this->find($id)->current()->toArray();
            unset($row[$this->id]);
            $row[$this->name] .= " -COPY-";
            $this->createItem($row);
        }
    }


    ##########################################################################################
    public function setValues($items, $values)
    {
        $items = (count($items) == 1 && count($items) > 0) ? array($items) : $items;

        $mdl_br_ids = array();

        foreach($items as $id => $item_id)
        {
            $row = $this->find($item_id)->current();
            if(count($values) > 0)
            {
                foreach($values as $field => $value)
                {
                    $row->$field = $value;
                }
            }
            $row->save();
        }

        if(count($mdl_br_ids) > 0) $this->updateBrands( $mdl_br_ids );

    }

        ##########################################################################################
        public function getNextID()
        {
            $res = $this->getAdapter()->query("SHOW TABLE STATUS LIKE 'kl_kladr';");
            $item = $res->fetchObject();

            return $item->Auto_increment;
        }

    ##########################################################################################
    public function rebuildItems()
    {
        $db = Zend_Registry::get( 'db' );

        // Соответствие: Код ОКАТО - Название Района
        $districts = array(
            "03401364" => "Западный",
            "03401369" => "Центральный",
            "03401370" => "Прикубанский",
            "03401372" => "Карасунский",
            "03401962" => "Березовский",
            "03401964" => "Елизаветинский",
            "03401967" => "Калининский",
            "03401973" => "Пашковский",
            "03401975" => "Старокорсунский",
        );


        // Модификация КЛАДРа:
        // - если улица имееет вид "им Название", преобразуем в "Название"
        //********************************************************************
        $items = $db->query("SELECT * FROM kl_kladr")->fetchAll();

        if(count($items) > 0)
        {
            foreach($items as $ind => $val)
            {
                $kl_id = $val["kl_id"];

                $row = $this->find($kl_id)->current();
                if ($row) 
                {
                    $okato = (!empty($val["kl_okato"])) ? substr($val["kl_okato"], 0, 8) : 0;
                    $district_name =  (isset($districts[$okato])) ? $districts[$okato] : "";

                    $kl_name = $val["kl_name"];

                    $pattern = array(
                        // Убираем из названий инициалы людей вида "А.Б."
                        "/([A-ZА-Я]\.)([A-ZА-Я]\.)/su",
                        
                        // Меняем названия вида "70-летия Октября" на "70 лет Октября"
                        "/(\d{2})-(летия)/su",
                    );

                    $replacement = array(
                        "",
                        "$1 лет",
                    );

                    $kl_name_mod = preg_replace($pattern, $replacement, $kl_name);

                    // Убираем из названий улиц "им "
                    $kl_name_mod = str_replace("им ", "", $kl_name_mod);

                    // Добавляем к названиям расширения и записываем в колонку kl_2gis для синхро с ГИСом
                    if($val["kl_type_short"] == "проезд")
                        $row->kl_2gis = $kl_name_mod . " проезд";

                    elseif($val["kl_type_short"] == "пр-кт")
                        $row->kl_2gis = $kl_name_mod . " проспект";

                    elseif($val["kl_type_short"] == "пер")
                        $row->kl_2gis = $kl_name_mod . " переулок";

                    elseif($val["kl_type_short"] == "ул")
                        $row->kl_2gis = $kl_name_mod;


                    $row->kl_district = $district_name;
                    $row->kl_name = $kl_name_mod;
                    $row->save();
                }
            }            
            // die;
        }

    }

    ##########################################################################################
    public function recalcItems()
    {
        $db = Zend_Registry::get( 'db' );

        $data = array();

        //Сверяемся с адресами объектов и высчитываем количество объектов по улицам
        //********************************************************************************
        $items = $db->query("
            SELECT 
                obj_addr_street AS street, 
                COUNT(*) AS objects, 
                obj_addr_district AS district,
                obj_addr_zip AS zip

            FROM obj_object       
            GROUP BY obj_addr_street
        ")->fetchAll();

        if(count($items) > 0)
        {
            foreach($items as $ind => $val)
            {
                
                //Ищеем соответствия улицам на которых стоят объекты, улицы в таблице КЛАДР
                //********************************************************************************
                $kladr_matches = $db->query("
                    SELECT *
                    FROM kl_kladr AS kl
                    WHERE kl.kl_2gis = '".$val['street']."'
                "
                )->fetchAll();

                $data[$ind]["gis"] = $val;  
                // $data[$ind]["kladr"] = $kladr_matches;  

        
                // Если соответствие одно, то все в порядке        
                //********************************************************************************
                if(count($kladr_matches) == 1)
                {
                    $kl_id = $kladr_matches[0]["kl_id"];
                    $row = $this->find($kl_id)->current();
                    
                    if ($row) 
                    {
                        $row->kl_objects = $val["objects"];
                        $row->save();
                    }
                    $data[$ind]["kladr"][] = $kladr_matches[0];  

                }
                // if(count($kladr_matches)

                // Если соответствий несколько, то ищем среди них совпадения по району города        
                //********************************************************************************
                elseif(count($kladr_matches) > 1)
                {
                
                    foreach($kladr_matches as $km_id => $km_val)
                    {
                        
                        if($km_val["kl_district"] == $val["district"])
                        {
                            $kl_id = $km_val["kl_id"];
                            $row = $this->find($kl_id)->current();
                            
                            if ($row) 
                            {
                                $row->kl_objects = $val["objects"];
                                $row->save();
                            }

                            unset($data[$ind]["kladr"]);
                            $data[$ind]["kladr"][] = $kladr_matches[$km_id];  

                            break;

                        } else {
                            $data[$ind]["kladr"][] = $kladr_matches[$km_id];  
                        }
                    
                    }
                    //foreach
                
                }
                // elseif

            }
            // foreach($items as $ind => $val)
        }
        // if(count($items)

        return $data;

    }    

    ##########################################################################################
    public function installKladr()
    {
        $db = Zend_Registry::get( 'db' );

        // Оригинал КЛАДРа (скачанный с сайта)
        $kladr_orig_tbl = new Zend_Db_Table(array( 'name' => "kl_kladr_orig", 'primary' => "kl_id"));

        // Модифицированный КЛАДР
        $kladr_tbl = new Zend_Db_Table(array( 'name' => "kl_kladr", 'primary' => "kl_id"));

        // Чистим кастом КЛАДР
        $kladr_tbl->delete("1=1");

       // Копируем все записи из оригнального в кастомный КЛАДР
       $rows = $kladr_orig_tbl
            ->fetchAll( $kladr_orig_tbl->select()->from(array('kl_kladr_orig')) )
            ->toArray();        

        foreach($rows as $row)
        {
            $rowItem = $kladr_tbl->createRow($row);
            if ($rowItem) {            
                $rowItem->save();
            }
        }

       // Вносим изменения в кастом КЛАДР
       // ************************************

        //1. Удаляем дубликаты улиц (Имеющих одинаковый ТИП и KeyID)
        //*****************************************
        $db->query("   DELETE kl1
                                FROM kl_kladr kl1, kl_kladr kl2 
                                WHERE kl1.kl_id < kl2.kl_id 
                                AND kl1.kl_name = kl2.kl_name
                                AND kl1.kl_type = kl2.kl_type
                                AND kl1.kl_keyid = kl2.kl_keyid
        ");



    }

}
?>
