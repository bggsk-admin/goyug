<?php
    $elem = $this->element;
    $elemName = $elem->getName();
    $values = $elem->getValue();
    $attribs = $elem->getAttribs();
?>

<?php foreach($elem->getMultiOptions() as $option=>$value){ ?>

    <div class="col-md-4">
        <input type="checkbox" data-label="<?=$value;?>" name="<?php echo $elemName; ?>[]" id="<?php echo $elemName; ?>-<?php echo $option; ?>" value="<?php echo $option; ?>" <?php if($values && in_array($option, $values)){ echo ' checked="checked"'; }?> <? foreach($attribs as $id=>$val) if($id!='options') echo $id."='".$val."' ";?> />
    </div>

    <?php } ?>
