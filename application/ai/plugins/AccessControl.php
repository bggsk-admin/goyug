<?php
    class Ai_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract
    {

        public function preDispatch(Zend_Controller_Request_Abstract $request)
        {
            // set up acl
            $acl = new Zend_Acl();

            # ROLES
            $acl->addRole(new Zend_Acl_Role('guest'));
            $acl->addRole(new Zend_Acl_Role('user'), 'guest');
            $acl->addRole(new Zend_Acl_Role('editor'), 'guest');
            $acl->addRole(new Zend_Acl_Role('manager'), 'guest');
            $acl->addRole(new Zend_Acl_Role('administrator'), 'user');

            # RESOURCES
            $acl
            ->add(new Zend_Acl_Resource('mvc:administrators'))
            ->add(new Zend_Acl_Resource('mvc:editors'))
            ->add(new Zend_Acl_Resource('mvc:managers'))
            ->add(new Zend_Acl_Resource('mvc:users'));

            $acl
            ->add(new Zend_Acl_Resource('error'))
            ->add(new Zend_Acl_Resource('index'))
            ->add(new Zend_Acl_Resource('config'))
            ->add(new Zend_Acl_Resource('user'))
            ->add(new Zend_Acl_Resource('dbdump'))
            ->add(new Zend_Acl_Resource('help'))
            ->add(new Zend_Acl_Resource('reset'))
            ->add(new Zend_Acl_Resource('log'))

            ->add(new Zend_Acl_Resource('owner'))
            ->add(new Zend_Acl_Resource('datadict'))
            ->add(new Zend_Acl_Resource('acctype'))
            ->add(new Zend_Acl_Resource('legalform'))
            ->add(new Zend_Acl_Resource('objecttype'))
            ->add(new Zend_Acl_Resource('renttype'))
            ->add(new Zend_Acl_Resource('amenity'))
            ->add(new Zend_Acl_Resource('restriction'))
            ->add(new Zend_Acl_Resource('manageobjects'))
            ->add(new Zend_Acl_Resource('parameter'))
            ->add(new Zend_Acl_Resource('service'))
            ->add(new Zend_Acl_Resource('equipment'))
            ->add(new Zend_Acl_Resource('object'))
            ->add(new Zend_Acl_Resource('feedback'))
            ->add(new Zend_Acl_Resource('language'))
            ->add(new Zend_Acl_Resource('period'))
            ->add(new Zend_Acl_Resource('files'))
            ->add(new Zend_Acl_Resource('customer'))
            ->add(new Zend_Acl_Resource('smartfilters'))
            ->add(new Zend_Acl_Resource('sftype'))
            ->add(new Zend_Acl_Resource('kladr'))
            ->add(new Zend_Acl_Resource('city'))
            ->add(new Zend_Acl_Resource('tools'))
            ->add(new Zend_Acl_Resource('parser'))
            ->add(new Zend_Acl_Resource('spitiparser'))
            ->add(new Zend_Acl_Resource('sutochnoparser'))
            ->add(new Zend_Acl_Resource('stat'))
            ;

            // -- GUEST --
            $acl->allow('guest', 'user', array('login', 'logout', 'ajax'));

            // -- EDITOR --
            $acl->allow('editor', 'mvc:editors');
            $acl->allow('editor', 'index',      array('index'));
            $acl->allow('editor', 'object');
            $acl->allow('editor', 'owner');
            $acl->allow('editor', 'files');
            $acl->allow('editor', 'feedback');

            // -- MANAGERS --
            $acl->allow('manager', 'mvc:editors');
            $acl->allow('manager', 'index',      array('index'));
            $acl->allow('manager', 'object');
            $acl->allow('manager', 'owner');
            $acl->allow('manager', 'files');

            // -- ADMINISTRATOR --
            $acl->allow('administrator', null);

            // fetch the current user
            $auth = Zend_Auth::getInstance();

            if($auth->hasIdentity()) {
                $identity = $auth->getIdentity();
                $role = strtolower($identity->u_role);
            }else{
                $role = 'guest';
            }

            $controller = $request->controller;
            $action = $request->action;

            if (!$acl->isAllowed($role, $controller, $action)) {
                if ($role == 'guest') {
                    $request->setControllerName('user');
                    $request->setActionName('login');
                } else {
                    $request->setControllerName('error');
                    $request->setActionName('noauth');
                }
            }

            # Link navigation with Acl
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
            Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);

        }

    } #class
