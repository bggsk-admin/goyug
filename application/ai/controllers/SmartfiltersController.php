<?php
    class SmartfiltersController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ai_Model_Smartfilters();
            $this->log = new Ai_Model_Log();

            $locale         = new Zend_Session_Namespace('locale');
            $this->curlang  = $this->view->curlang  =  $locale->curlocale;
            $this->langs    = $this->view->langs    =  $locale->locales;
            $this->ucid     = "smartfilters";

            //--------------------- SETUP AJAX ----------------------------------------
            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
            ->addActionContext('upload', 'json')
            ->initContext('json');

            $this->backurl = $this->view->url( array('controller' => $this->ucid, 'action' => 'index'), NULL, true );
        }

        ##########################################################################################
        public function indexAction()
        {
            $where      = array();

            $order      = array();
            // $order[]            = "ot_id ASC";
            $order[]            = "sf_level ASC";
            $order[]            = "sf_order ASC";

            $items = $this->model->getItems($where, $order);

            if(!empty($items))
            {
                foreach($items as $i_id => $i_val)
                {
                    $images = $this->model->getImages($i_val['sf_id']);
                    $items[$i_id]['sf_images'] = array();

                    foreach($images as $im_id => $im_val)
                    {
                        $items[$i_id]['sf_images'][] = "/upload/" . $this->ucid  . "/" . $i_val['sf_id'] . "/thumbnail/" . $im_val["f_name"];
                    }

                }

                $this->view->items = $items;
                $this->view->qty = count($items);

            } else {
                $this->view->items = null;
            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
        }

        ##########################################################################################
        public function createAction()
        {
            $next_id = $this->model->getNextID();

            $form = new Ai_Form_Smartfilters();

            if($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            }

            $this->view->form = $form;

            $this->view->sf_id = $next_id;
            $this->view->upload_id = $next_id;
            $this->view->upload_ucid = $this->ucid;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
            $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml');
        }

        ##########################################################################################
        public function updateAction()
        {
            $form = new Ai_Form_Smartfilters();

            $id = $this->_request->getParam('id');

            if ($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    $form_arr = $form->getValues();

                    $this->log->write( array('status' => 'success', 'result' => 'update') );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }

            } else {

                $id = $this->_request->getParam('id');
                $item = $this->model->find($id)->current();

                $form_arr = $item->toArray();
                $form->populate( $form_arr );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'edit') );
            }

            $this->view->form = $form;

            $this->view->upload_id = $id;
            $this->view->upload_ucid = $this->ucid;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
            $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml', $form_arr);
        }

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->deleteItems(array($id));

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }


        ##########################################################################################
        public function cloneAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->cloneItem($id);

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function multiupdateAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->_request->isPost() || $this->_request->isGet())
            {

                $mode = $this->_request->getParam('mode');
                $items = $this->_request->getParam('customize_items');

                switch ( $mode )
                {
                    case 'enable':
                    {
                        $this->model->setValues($items, array("sf_enable"=>1));
                        break;
                    }

                    case 'disable':
                    {
                        $this->model->setValues($items, array("sf_enable" => 0));
                        break;
                    }

                    case 'delete':
                    {
                        $this->model->deleteItems($items);
                        break;
                    }

                }

            }

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $method = $this->_request->getParam('method');
            $params = $_REQUEST;

            switch ($method) {
                case 'save':
                    $this->ajaxSave($params);
                    break;
                
                case 'rebuild':
                    $this->ajaxRebuild();
                    break;
            }

        }

        ##########################################################################################
        function ajaxSave($params)
        {
            $response = $this->model->updateItem( $params );
            $this->view->form = $response;
        }

        ##########################################################################################
        function ajaxRebuild()
        {
            $response = $this->model->rebuildItems();
            $this->view->response = $response;
        }





}