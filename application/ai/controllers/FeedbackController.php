<?php

    class FeedbackController extends Zend_Controller_Action {

        ##########################################################################################
        public function init() {

            $this->model = new Ai_Model_Feedback();
            $this->modelUser = new Ai_Model_User();
            $this->log = new Ai_Model_Log();

            $locale           = new Zend_Session_Namespace('locale');
            $this->curlang    = $this->view->curlang  =  $locale->curlocale;
            $this->langs      = $this->view->langs    =  $locale->locales;
            $this->folder = "feedback";
            $this->controller = "feedback";

            $this->backurl = $this->view->url( array('controller' => $this->controller, 'action' => 'index'), NULL, true );

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext
                ->addActionContext('ajax', 'json')
                ->addActionContext('ajaxReply', 'json')
                ->initContext('json');
        }

        ##########################################################################################
        public function indexAction() {

            $this->view->feedbacks = $this->model->getItems();

            $this->view->layout()->breadcrumb = $this->view->partial( $this->folder. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->folder. '/subnavbar.phtml');
        }

        ##########################################################################################
        public function ajaxAction() {

            switch ($_POST['action']) {

                case 'reply':
                    $this->ajaxReply($_POST);
                    break;

                default:

                    break;
            }
            exit;
        }

        ##########################################################################################
        public function replyAction() {

            $u_id = $this->_request->getParam('id');

            $this->view->user = $this->modelUser->find($u_id)->current();
            $this->view->feedbacks = $this->model->getItemsById($u_id);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->folder. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->folder. '/subnavbar-edit.phtml');
        }

        ##########################################################################################
        public function ajaxReply($params) {
            $this->view->reply = $this->model->reply($params);
            echo json_encode($this->view);
            exit;
        }

        ##########################################################################################
        public function deleteAction() {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->delete($id);

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

    }