<?php

class ErrorController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('error');
        $this->log = new Ai_Model_Log();
        $this->ucid     = "error";
    }

    public function indexAction()
    {
    }

    public function errorAction()
    {

        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $exception = $errors->exception;

                $this->view->exception = $exception->getMessage();
                $this->view->trace = $exception->getTraceAsString();
                $this->view->request = $errors->request;

                //Log action
                $this->log->write( array('status' => 'error', 'result' => $exception->getMessage()) );

                break;
            default:
            {
                $exception = $errors->exception;

                $this->view->exception = $exception->getMessage();
                $this->view->trace = $exception->getTraceAsString();
                $this->view->request = $errors->request;

                //Log action
                $this->log->write( array('status' => 'error', 'result' => $exception->getMessage()) );
                $logger = Zend_Registry::get('logger');
                $logger->err($this->view->exception);

            break;
            }


        }

        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

    }

    public function noauthAction()
    {
        //Log action
        $this->log->write( array('status' => 'error', 'result' => 'not authorized action') );
    }

}



