<?php
    class KladrController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ai_Model_Kladr();
            $this->log = new Ai_Model_Log();

            $locale         = new Zend_Session_Namespace('locale');
            $this->curlang  = $this->view->curlang  =  $locale->curlocale;
            $this->langs    = $this->view->langs    =  $locale->locales;
            $this->ucid     = "kladr";

            //--------------------- SETUP AJAX ----------------------------------------
            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
            ->initContext('json');

            $this->backurl = $this->view->url( array('controller' => $this->ucid, 'action' => 'index'), NULL, true );
        }

        ##########################################################################################
        public function indexAction()
        {
            $where      = array();

            $order      = array();
            // $order[]            = "ot_id ASC";
            // $order[]            = "sf_level ASC";
            // $order[]            = "sf_order ASC";

            $items = $this->model->getItems($where, $order);

            if(!empty($items))
            {

                $this->view->items = $items;
                $this->view->qty = count($items);

            } else {
                $this->view->items = null;
            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
        }

        ##########################################################################################
        public function createAction()
        {
            $next_id = $this->model->getNextID();

            $form = new Ai_Form_Kladr();

            if($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            }

            $this->view->form = $form;

            $this->view->sf_id = $next_id;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
            $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml');
        }

        ##########################################################################################
        public function updateAction()
        {
            $form = new Ai_Form_Kladr();

            $id = $this->_request->getParam('id');

            if ($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->updateItem( $form->getValues() );
                    $form_arr = $form->getValues();

                    $this->log->write( array('status' => 'success', 'result' => 'update') );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }

            } else {

                $id = $this->_request->getParam('id');
                $item = $this->model->find($id)->current();

                $form_arr = $item->toArray();
                $form->populate( $form_arr );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'edit') );
            }

            $this->view->form = $form;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
            // $this->view->layout()->infobar = $this->view->partial( $this->ucid. '/infobar-edit.phtml', $form_arr);
        }

        ##########################################################################################
        public function recalcAction()
        {
            $data = $this->model->recalcItems();

            $this->view->data = $data;
            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-recalc.phtml');
        }        

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->deleteItems(array($id));

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function multiupdateAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->_request->isPost() || $this->_request->isGet())
            {

                $mode = $this->_request->getParam('mode');
                $items = $this->_request->getParam('customize_items');

                switch ( $mode )
                {
                    case 'enable':
                    {
                        $this->model->setValues($items, array("sf_enable"=>1));
                        break;
                    }

                    case 'disable':
                    {
                        $this->model->setValues($items, array("sf_enable" => 0));
                        break;
                    }

                    case 'delete':
                    {
                        $this->model->deleteItems($items);
                        break;
                    }

                }

            }

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $method = $this->_request->getParam('method');
            $params = $_REQUEST;

            switch ($method) {
                case 'save':
                    $this->ajaxSave($params);
                    break;                
            }

        }

        ##########################################################################################
        function ajaxSave($params)
        {
            $response = $this->model->updateItem( $params );
            $this->view->form = $response;
        }

        ##########################################################################################
        function rebuildAction()
        {
            $response = $this->model->rebuildItems();

            //Log action
            $this->log->write( array('status' => 'success', 'result'=>'rebuild') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

    //#########################################################################################
    public function getkladrAction() {

        // $this->_helper->layout->setLayout('null');

        $tbl = new Zend_Db_Table(array('name' => 'kl_kladr_orig', 'primary' => 'kl_id'));
        $db = Zend_Registry::get( 'db' );

        $parse_arr = array(
            "а",
            "б", 
            "в",
            "г",
            "д", 
            "е", "ё", "ж", "з", "и", 
            "й", "к", "л", "м", "н", 
            "о", "п", "р", "с", "т", 
            "у", "ф", "х", "ц", "ч", 
            "ш", "щ", "э", "ю", "я", 
        );

        $forbidden_types = array(
            "Садовое неком-е товарищество",
            "Сквер",
            "Площадь",
            "Квартал",
            "Парк",
            "Бульвар",
            "Гаражно-строительный кооперат",
            "Железнодорожная будка",
            "Почтовое отделение",
            "Станция",
            "Тупик",
            "Участок",
        );

        $client = new Zend_Http_Client();

        $client->setUri("http://kladr-api.ru/api.php");
        $client->setParameterGet('token', '53c581e1fca916514f48bb38');
        
        //В новой версии КЛАДР-Облака ключ уже не нужен
        // $client->setParameterGet('key', 'bf9c7dd5b4fc43a8264c94183232b0164807cb91');        
        
        $client->setParameterGet('cityId', '2300000100000');        
        $client->setParameterGet('contentType', 'street');        
        
        //В новой версии КЛАДР-Облака limit < 400
        $client->setParameterGet('limit', '399');        

        $client->setMethod(Zend_Http_Client::GET);

        if(count($parse_arr) > 0)
        {
            $this->view->response = array();

            foreach($parse_arr as $parse_val)
            {
                $tbl->delete("1=1");
        
                $client->setParameterGet('query', $parse_val);        
                
                $response = $client->request(); 
                $request = $client->getLastRequest(); 

                $this->view->request = $request;  

                $data = json_decode($response->getBody()); 
                
                if(isset($data->result))
                {
                    $data = $data->result;
                    
                    foreach($data as $id => $val)
                    {
                        if( !in_array($val->type, $forbidden_types) )
                        {
                            $this->view->response[] = $val;   
                        }
                    }

                }
            }


            // Пишем в базу
            foreach($this->view->response as $val)
            {
                // $okato = (!empty($val->okato)) ? substr($val->okato, 0, 8) : 0;
                // $district_name =  (isset($districts[$okato])) ? $districts[$okato] : "";

                $row = $tbl->createRow();

                $row->kl_city = "2300000100000";
                $row->kl_keyid = $val->id;
                $row->kl_name = $val->name;
                $row->kl_zip = $val->zip;
                $row->kl_type = $val->type;
                $row->kl_type_short = $val->typeShort;
                $row->kl_okato = $val->okato;

                $row->save();
            }

            $this->view->count = count($this->view->response);  
        }

        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

    }

    ##########################################################################################
    public function installAction()
    {
        $this->model->installKladr();

        //Log action
        $this->log->write( array('status' => 'success', 'result'=>'install') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }



}