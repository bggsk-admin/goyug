<?php
    class LegalformController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->model = new Ai_Model_Legalform();
            $this->log = new Ai_Model_Log();

            $locale         = new Zend_Session_Namespace('locale');
            $this->curlang  = $this->view->curlang  =  $locale->curlocale;
            $this->langs    = $this->view->langs    =  $locale->locales;
            $this->ucid     = "legalform";
            $this->folder     = "legalform";
            $this->controller     = "legalform";

            $this->backurl = $this->view->url( array('controller' => $this->controller, 'action' => 'index'), NULL, true );
        }

        ##########################################################################################
        public function indexAction()
        {
            $items = $this->model->getItems();

            if(!empty($items))
            {
                $this->view->items = $items;
                $this->view->qty = count($items);
            } else {
                $this->view->items = null;
            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->folder. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->folder. '/subnavbar.phtml');
        }

        ##########################################################################################
        public function createAction()
        {
            $form = new Ai_Form_Legalform();

            if($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if($form->isValid($post))
                {
                    $this->model->createItem( $form->getValues() );
                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            }

            $this->view->form = $form;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->folder. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->folder. '/subnavbar-edit.phtml');
        }

        ##########################################################################################
        public function updateAction()
        {
            $form = new Ai_Form_Legalform();

            if ($this->_request->isPost())
            {
                $post = $this->_request->getPost();

                if ($form->isValid($post))
                {

                    $this->model->updateItem( $form->getValues() );

                    //Log action
                    $this->log->write( array('status' => 'success', 'result' => 'update') );

                    return $this->_redirect( $this->backurl, array('prependBase' => false) );
                }
            } else {
                $id = $this->_request->getParam('id');
                $currentItem = $this->model->find($id)->current();
                $form->populate($currentItem->toArray());

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'edit') );
            }
            $this->view->form = $form;

            $this->view->layout()->breadcrumb = $this->view->partial( $this->folder. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->folder. '/subnavbar-edit.phtml');
        }

        ##########################################################################################
        public function deleteAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->deleteItems(array($id));

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }


        ##########################################################################################
        public function cloneAction()
        {
            $id = $this->_request->getParam('id');

            if(!empty($id)) $this->model->cloneItem($id);

            //Log action
            $this->log->write( array('status' => 'success') );

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }

        ##########################################################################################
        public function multiupdateAction()
        {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->_request->isPost() || $this->_request->isGet())
            {

                $mode = $this->_request->getParam('mode');
                $items = $this->_request->getParam('customize_items');

                switch ( $mode )
                {
                    case 'enable':
                    {
                        $this->model->setValues($items, array("lf_enable"=>1));
                        break;
                    }

                    case 'disable':
                    {
                        $this->model->setValues($items, array("lf_enable" => 0));
                        break;
                    }

                    case 'delete':
                    {
                        $this->model->deleteItems($items);
                        break;
                    }

                }

            }

            return $this->_redirect( $this->backurl, array('prependBase' => false) );
        }
}