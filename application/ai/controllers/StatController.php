<?php

require_once("Util.php");

class StatController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model = new Ai_Model_Stat();
        $this->objectModel = new Ai_Model_Object();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');

        $ajaxContext->addActionContext('ajax', 'json')
        ->addActionContext('ajaxStatGenerate', 'json')
        ->initContext('json');
    }

    ##########################################################################################
    public function indexAction()
    {
        $this->view->layout()->subnavbar = $this->view->partial( 'index/subnavbar.phtml');
    }

    ##########################################################################################
    public function ajaxAction ()
    {
        $method = $this->_request->getParam('method');
        $params = $_REQUEST;

        switch ($method) {
            case 'statgen':
                $this->ajaxStatGenerate($params);
                break;
        }
    }

    ##########################################################################################
    public function ajaxStatGenerate($params)
    {
        set_time_limit(0);

        $stat_action = $params['stat_params']['stat_action'];
        $stat_city = $params['stat_params']['stat_city'];
        
        $stat_date_from = $params['stat_params']['stat_date_from'];
        $stat_date_from = (!empty($stat_date_from)) ? date_format(date_create_from_format('d.m.Y', $stat_date_from), 'Y-m-d') : "";

        $stat_date_to = $params['stat_params']['stat_date_to'];
        $stat_date_to = (!empty($stat_date_to)) ? date_format(date_create_from_format('d.m.Y', $stat_date_to), 'Y-m-d') : "";


        $statParams = array(
            "city" => $stat_city,
            "date_from" => $stat_date_from,
            "date_to" => $stat_date_to
        );

        if(!empty($stat_action) && !empty($stat_city))
        {
            switch ($stat_action) {
                
                case 'new_objects':
                    $stat_result = $this->viewNewObjectsStat($statParams);
                    break;
                
                case 'objects':
                    $stat_result = $this->viewObjectsStat($statParams);
                    break;
                
                case 'new_users':
                    $stat_result = $this->viewNewUsersStat($statParams);
                    break;

                case 'money':
                    $stat_result = $this->viewMoneyStat($statParams);
                    break;

                case 'money_ontop':
                    $stat_result = $this->viewMoneyOntopStat($statParams);
                    break;

                case 'abonements':
                    $stat_result = $this->viewAbonementsStat($statParams);
                    break;

                case 'emails':
                    $stat_result = $this->viewEmailsStat($statParams);
                    break;

                case 'logins':
                    $stat_result = $this->viewLoginsStat($statParams);
                    break;

                case 'abonements_now':
                    $stat_result = $this->viewAbonementsNowStat($statParams);
                    break;

                case 'abonements_all_in_city':
                    $stat_result = $this->viewAbonementsAllInCityStat($statParams);
                    break;

                case 'users_ontop':
                    $stat_result = $this->viewUsersOntopStat($statParams);
                    break;

                case 'ontop_real':
                    $stat_result = $this->viewOntopRealStat($statParams);
                    break;

                case 'payments':
                    $stat_result = $this->viewPaymentsStat($statParams);
                    break;

                case 'orders':
                    $stat_result = $this->viewOrdersStat($statParams);
                    break;

                case 'paramorders':
                    $stat_result = $this->viewParamOrdersStat($statParams);
                    break;

            }           
            $this->view->response = $stat_result;

        } else {
            $this->view->response = "<strong>По вашему запросу ничего не найдено</strong>";
        }
    }

    ##########################################################################################
    public function viewNewObjectsStat($params)
    {
        $db_data = $this->model->getNewObjects($params);
        $stat_result = $this->generateTable(array('Город', 'Объекты'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewObjectsStat($params)
    {
        $db_data = $this->model->getObjects($params);
        $stat_result = $this->generateTable(array('Город', 'Объекты', 'Вирт', 'Реал'), $db_data, array(1,2,3));

        return $stat_result;
    }

    ##########################################################################################
    public function viewNewUsersStat($params)
    {
        $db_data = $this->model->getNewUsers($params);
        $stat_result = $this->generateTable(array('Город', 'Пользователи'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewMoneyStat($params)
    {
        $db_data = $this->model->getMoney($params);
        $stat_result = $this->generateTable(array('Город', 'Сумма'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewMoneyOntopStat($params)
    {
        $db_data = $this->model->getMoneyOntop($params);
        $stat_result = $this->generateTable(array('Город', 'Сумма'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewAbonementsStat($params)
    {
        $db_data = $this->model->getAbonements($params);
        $stat_result = $this->generateTable(array('Город', 'Количество'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewAbonementsNowStat($params)
    {
        $db_data = $this->model->getAbonementsNow($params);
        $stat_result = $this->generateTable(array('Город', 'Количество'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewAbonementsAllInCityStat($params)
    {
        $db_data = $this->model->getAbonementsAllInCity($params);
        $stat_result = $this->generateTable(array('ID', 'Email', 'Метка', 'Начало аб-та', 'Конец аб-та', 'Деньги на счету'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewEmailsStat($params)
    {
        $db_data = $this->model->getEmails($params);
        $stat_result = $this->generateTable(array('Город', 'Количество'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewLoginsStat($params)
    {
        $db_data = $this->model->getLogins($params);
        $stat_result = $this->generateTable(array('Город', 'Входов в ЛК'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewUsersOntopStat($params)
    {
        $db_data = $this->model->getUsersOntop($params);
        $stat_result = $this->generateTable(array('Город', 'Поднимальщики'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewOntopRealStat($params)
    {
        $db_data = $this->model->getOntopReal($params);
        $stat_result = $this->generateTable(array('Город', 'Поднято об-тов', 'ID Владельцев', 'Ссылка на карту'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewPaymentsStat($params)
    {
        $db_data = $this->model->getPayments($params);
        $stat_result = $this->generateTable(array('Дата', 'Сумма', 'Операция', 'ID Вл-ца', 'ID Об-та', 'Город', 'Email', 'Вл-ц Создан', 'Кем создан'), $db_data, array(1));

        return $stat_result;
    }

    ##########################################################################################
    public function viewOrdersStat($params)
    {
        $db_data = $this->model->getOrders($params);
        $stat_result = $this->generateTable(array('Дата', 'Заголовок', 'Город', 'Кол-во писем'), $db_data, array(3));

        return $stat_result;
    }

    ##########################################################################################
    public function viewParamOrdersStat($params)
    {
        $db_data = $this->model->getParamOrders($params);
        $stat_result = $this->generateTable(array('Дата', 'Заголовок', 'Город', 'Кол-во писем'), $db_data, array(3));

        return $stat_result;
    }

    ##########################################################################################
    public function generateTable($headers, $db_data, $totals_to_show = array()) {

        $totals = array();
        $result_html = "";

        $result_html = '
            <h4>Найдено ' . count($db_data) . ' записей</h4>
            <table class="table table-striped" id="stat-table">
            <thead>
                <tr>';

        foreach($headers as $header_name) {
           $result_html .= '<th><a href="#">'.$header_name.'</a></th>'; 
        }

        $result_html .= '        
                </tr>
            </thead>
            <tbody>
        ';        

        $totals = array_fill(0, count($headers), 0);

        if(count($db_data) > 0) {
            foreach ($db_data as $key => $value) {
                
                $result_html .= '<tr><td>'.$value[0].'</td>';

                for($i = 1; $i<count($value); $i++) {
                    

                    $totals[$i] += (in_array($i, $totals_to_show)) ? $value[$i] : 0;
                    
                    $result_html .= '<td>'.$value[$i].'</td>';
                }

                $result_html .= '</tr>';
            }
        }


        $result_html .= '
            </tbody>
            <tr>
                <td><strong>ВСЕГО</strong></td>';

        for($i = 1; $i<count($totals); $i++) {
            $result_html .= (in_array($i, $totals_to_show)) ? '<td><strong>'.$totals[$i].'</strong></td>' : '<td>&nbsp;</td>';

        }
        
        $result_html .= '    
            </tr>
        </table>
        '; 

        return $result_html;
    }


}


