<?php
class ObjecttypeController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model    = new Ai_Model_ObjectType();
        $this->log      = new Ai_Model_Log();
        $this->auth     = Zend_Auth::getInstance()->getIdentity();

        $locale         = new Zend_Session_Namespace('locale');
        $this->curlang  = $this->view->curlang  =  $locale->curlocale;
        $this->langs    = $this->view->langs    =  $locale->locales;

        $this->pref     = "ot_";
        $this->ucid     = "objecttype";
        $this->pid      = $this->pref . 'pid';

        $this->is_filter = FALSE;

        $this->prm      = $this->view->prm      = $this->_getAllParams();
        $this->f_prm    = $this->view->f_prm    = Util::filterArrayByKey("f_", $this->prm);

        $this->backurl = $this->view->url(  array_merge($this->f_prm, array('controller' => $this->ucid, 'action' => 'index')), NULL, true );
    }

    ##########################################################################################
    public function indexAction()
    {

        //WHERE & ORDER elements
        $sql_where = array();
        $sql_order = array();

        //Filter Form
        if($this->is_filter)
        {
            $f_prm      = $this->f_prm;
            $filter     = new Ai_Form_ObjecttypeFilter();
            $filter->populate($f_prm);

            if(!empty($f_prm['f_p_uid']))     $sql_where[] = "p_uid LIKE '%".$f_prm['f_p_uid']."%'";

            //Generate ORDER element
            if(!empty($f_prm['f_sort_field']) && !empty($f_prm['f_sort_type'])) $sql_order[] = $f_prm['f_sort_field']." ".$f_prm['f_sort_type'];
            else $sql_order[] =  $this->pref . "name_".$this->curlang['lang']." ASC";

        }

        $items = $this->model->getItems( $sql_where, $sql_order );

        if(!empty($items))
        {
            //$this->view->items = $items;
            $this->view->items = $this->model->arrToTree( $items );
            $this->view->qty = count($items);
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'list items') );

        } else {

            $this->view->items = null;
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'no items') );

        }

        $this->view->layout()->filter = ($this->is_filter) ? $filter : NULL;
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

    }

    ##########################################################################################
    public function createAction()
    {
        $pid = $this->_request->getParam('pid');

        $form = new Ai_Form_Objecttype();
        $form->populate( array($this->pid => $pid) );

        if($this->_request->isPost())
        {
            if($form->isValid($_POST))
            {

                $row = $this->model->createItem( $form->getValues() );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'save') );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );

            }
        }

        //Log action
        $this->log->write( array('status' => 'success', 'result' => 'create') );

        $this->view->form = $form;
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
        //////
    }

    ##########################################################################################
    public function updateAction()
    {
        $form = new Ai_Form_Objecttype();
        $form->setAction("/ai/".$this->ucid."/update");

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {

                $this->model->updateItem( $form->getValues() );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'update') );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );
            }
        } else {
            $id = $this->_request->getParam('id');
            $currentItem = $this->model->find($id)->current();
            $form->populate($currentItem->toArray());

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'edit') );
        }
        $this->view->form = $form;
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar-edit.phtml');
    }

    ##########################################################################################
    public function deleteAction()
    {
        $id = $this->_request->getParam('id');

        if(!empty($id)) $this->model->deleteItems(array($id));

        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function cloneAction()
    {
        $id = $this->_request->getParam('id');

        if(!empty($id)) $this->model->cloneItem($id);

        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function multiupdateAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost() || $this->_request->isGet())
        {

            $mode = $this->_request->getParam('mode');
            $items = $this->_request->getParam('customize_items');
            $pid = $this->_request->getParam('pid');

            switch ( $mode )
            {
                case 'enable':
                {
                    $this->model->setValues($items, array("ot_enable"=>1));
                    break;
                }

                case 'disable':
                {
                    $this->model->setValues($items, array("ot_enable"=>0));
                    break;
                }

                case 'change-parent':
                {
                    $this->model->setValues($items, array("ot_pid" => $pid));
                    break;
                }

                case 'delete':
                {
                    $this->model->deleteItems($items);
                    break;
                }

            }

        }

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function maketplsAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $items = $this->model->getItems();

        foreach($items as $id => $val)
        {
            $this->model->makeTemplate($val['id']);
        }

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }
}