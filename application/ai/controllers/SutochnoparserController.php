<?
    class SutochnoparserController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->ucid = $this->view->ucid = "sutochnoparser";
            $this->view->name = "Парсер Sutochno";
            $this->model = new Ai_Model_Sutochnoparser();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
            ->addActionContext('ajaxStartParse', 'json')
            ->addActionContext('ajaxStartDownload', 'json')
            ->addActionContext('ajaxStartClearing', 'json')
            ->addActionContext('ajaxCheckParse', 'json')
            ->addActionContext('ajaxGetToken', 'json')
            ->initContext('json');


        }

        
        ##########################################################################################
        public function fixdbAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            set_time_limit(0);

            $this->model->fixDB();
            
        }


        ##########################################################################################
        public function writedbAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function downloadimgsAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function clearimgsAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function indexAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $method = $this->_request->getParam('method');
            $params = $_REQUEST;

            switch ($method) {
                
                case 'startparse':
                    $this->ajaxStartParse($params);
                    break;

                case 'parseowners':
                    $this->ajaxParse($params);
                    break;

                case 'startdownload':
                    $this->ajaxStartDownload($params);
                    break;

                case 'startclearing':
                    $this->ajaxStartClearing($params);
                    break;

                case 'startwritingdb':
                    $this->ajaxStartWriteDB($params);
                    break;

                case 'checkparse':
                    $this->ajaxCheckParse($params);
                    break;

                case 'gettoken':
                    $this->ajaxGetToken();
                    break;
            }

        }

        ##########################################################################################
        public function ajaxParse($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];
            $owners_list = $params['owners_list'];

            if(!empty($pars_city) && !empty($owners_list))
            {
                $owners_list = explode("\n", $owners_list);

                $job = $this->model->createParseJob($pars_city, count($owners_list), 0, 0, $task_token);
                $result = $this->model->parse($pars_city, $owners_list, $task_token);

                $job_stat = $this->model->getParseJob($task_token);
                $this->view->response = $job_stat;
            
            }   else {
                $this->view->response = "empty query";
            }         

        }

        ##########################################################################################
        public function ajaxStartDownload($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $items = $this->model->countDownloadItems($pars_city);
                $objects_num = $items['images'];

                // print_r($items);die;

                $job = $this->model->createDownloadJob($pars_city, $objects_num, $task_token);
                $job_stat = $this->model->download($pars_city, $items, $task_token);

                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxStartClearing($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();
            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $items = $this->model->getParsedImages($pars_city);
                $items_num = $this->model->countParsedImages($items);
                $job = $this->model->createClearingJob($pars_city, $items_num, $task_token);
                $items = $this->model->clearImages($pars_city, $items, $task_token);
                
                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxStartWriteDB($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();
            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $job = $this->model->createWriteJob($pars_city, 0, $task_token);
                $items = $this->model->writeToDB($pars_city, $task_token);
                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxCheckParse($params)
        {
            $this->view->status = $this->model->checkParseJob($params['token']);
        }

}