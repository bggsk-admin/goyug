<?php
class PagesController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model    = new Ai_Model_Pages();
        $this->log      = new Ai_Model_Log();
        $this->auth     = Zend_Auth::getInstance()->getIdentity();
           
        $this->pref     = "p_";
        $this->ucid     = $this->view->ucid = "pages";

        $this->view->name = "Страницы";
        $this->perpage = Util::getConfig(array("ai_pages_perpage"));

        // All url params
        $this->prm      = $this->view->prm      = $this->_getAllParams();
        // Filter url params
        $this->f_prm    = $this->view->f_prm    = Util::filterArrayByKey("f_", $this->prm);
        
        $this->backurl = $this->view->backurl = $this->view->url( array_merge($this->f_prm, array('action' => 'index')) );
    }

    ##########################################################################################
    public function indexAction()
    {
        $this->view->headTitle()->append(": Список");

        //Filter Form
        $f_prm = $this->f_prm;
        $filter = new Ai_Form_PagesFilter();
        $filter->populate($f_prm);
        
        //Generate WHERE element
        $where = array();
        if(!empty($f_prm['f_date_from']))   $where[] = "p_date >= '".$f_prm['f_date_from']." 00:00:00'";
        if(!empty($f_prm['f_date_to']))     $where[] = "p_date <= '".$f_prm['f_date_to']." 23:59:59'";
        if(!empty($f_prm['f_p_title']))     $where[] = "p_title LIKE '%".$f_prm['f_p_title']."%'";
        if(!empty($f_prm['f_p_uid']))     $where[] = "p_uid LIKE '%".$f_prm['f_p_uid']."%'";
        
        //Generate ORDER element
        if(!empty($f_prm['f_sort_field']) && !empty($f_prm['f_sort_type'])) $order[] = $f_prm['f_sort_field']." ".$f_prm['f_sort_type'];
        else $order[] = "p_date DESC";

        $items = $this->model->getItems( $where, $order );
        
        if(!empty($items)) 
        {
            //Paginate data
            Zend_View_Helper_PaginationControl::setDefaultViewPartial('/paginators/general_paginator.phtml');
            $pitems = Zend_Paginator::factory($items);        
            $pitems->setItemCountPerPage($this->perpage);
            $pitems->setCurrentPageNumber($f_prm['f_page']);

            $this->view->paginator = Zend_View_Helper_PaginationControl::paginationControl($pitems);
            $this->view->items = $pitems;
            $this->view->qty = count($items);
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'list items') );

        } else {
            $this->view->items = null;
            $this->view->filterActive = (!empty($f_prm)) ? TRUE : FALSE;

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'no items') );
        
        }
        $this->view->form = $filter;

    }

    ##########################################################################################
    public function createAction()
    {
        // Enable CKeditor
        $this->view->layout()->ckeditor = true;

        // Enable Uploadify
        $this->view->layout()->uploadify = true;

        //Generate Form
        $form = new Ai_Form_Pages();
        $form->setAction("/ai/".$this->ucid."/create");
        
        //Write Filter Hidden Params
        foreach($this->f_prm as $f_name => $f_val)
        {
            $form->addElement('hidden', $f_name, array( 'value' => $f_val, 'decorators' => array('ViewHelper')) );
        }

        
        if($this->_request->isPost()) {
            if($form->isValid($_POST)) {

                $row = $this->model->createItem( $form->getValues() );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'save') );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );
            }
        }

        //Log action
        $this->log->write( array('status' => 'success', 'result' => 'create') );
        
        $this->view->form = $form;

    }

    ##########################################################################################
    public function updateAction()
    {
        // Enable CKeditor
        $this->view->layout()->ckeditor = true;

        // Enable Uploadify
        $this->view->layout()->uploadify = true;
        
        $form = new Ai_Form_Pages();
        $form->setAction("/ai/".$this->ucid."/update");

        //Write Filter Hidden Params
        foreach($this->f_prm as $f_name => $f_val)
        {
            $form->addElement('hidden', $f_name, array( 'value' => $f_val, 'decorators' => array('ViewHelper')) );
        }

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                
                $this->model->updateItem( $form->getValues() );

                //Log action
                $this->log->write( array('status' => 'success', 'result' => 'update') );

                return $this->_redirect( $this->backurl, array('prependBase' => false) );
            }
        } else {
            
            $id = $this->_request->getParam('id');
            $item = $this->model->find($id)->current();
            $form->populate($item->toArray());

            //Log action
            $this->log->write( array('status' => 'success', 'result' => 'edit') );
        }
        $this->view->form = $form;
    }    

    ##########################################################################################
    public function deleteAction()
    {
        $id = $this->_request->getParam('id');
        
        if(!empty($id)) $this->model->deleteItem($id);
        
        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function cloneAction()
    {
        $id = $this->_request->getParam('id');
        
        if(!empty($id)) $this->model->cloneItem($id);
        
        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }

    ##########################################################################################
    public function multiupdateAction()
    {
        $params = $this->_request->getParams();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_request->isPost()) 
        {

            $mode = $this->_request->getParam('mode');
            $ctg_new = $this->_request->getParam('ctg_new');
            $items = $this->_request->getParam('customize_items');
            $date = $this->_request->getParam('date');
            
            if(!empty($params['ctg_new']) && $params['ctg_new'] != "none")
            {
                $this->model->setValues($items, array("ab_ag_id" => $ctg_new));
            }
            
            switch ( $mode )  
            {

                case 'setpublic-on':    
                {
                    $this->model->setValues($items, array("p_public" => 1));
                    break;
                }            

                case 'setpublic-off':    
                {
                    $this->model->setValues($items, array("p_public" => 0));
                    break;
                }            

                case 'delete':    
                {
                    $this->model->deleteItems($items);
                    break;
                }            
                
            }
        
        }    

        //Log action
        $this->log->write( array('status' => 'success') );

        return $this->_redirect( $this->backurl, array('prependBase' => false) );
    }    

}
