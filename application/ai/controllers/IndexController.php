<?php

require_once("Util.php");

class IndexController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model = new Ai_Model_Stat();
        $this->modelLog = new Ai_Model_Log();
        $this->objectModel = new Ai_Model_Object();
    }

    ##########################################################################################
    public function indexAction()
    {

        $auth = Zend_Auth::getInstance();

        if($auth->hasIdentity()) {
            $this->view->identity = $auth->getIdentity();
        }

        $this->view->dbname =  $dbname = $this->model->getDBName();
        $this->view->dbsize =  $this->model->getDBSize($dbname);
        // $this->view->dbstat =  $this->model->getDBStat($dbname);

        $this->view->objects_total = $this->model->getTotalObjectsAmount();
        $this->view->objects_today = $this->model->getTodayObjectsAmount();
        $this->view->objects_week = $this->model->getWeekObjectsAmount();
        $this->view->objects_last_week = $this->model->getWeekObjectsAmount(1);

        // $this->view->views_total = $this->model->getTotalViewsAmount();
        // $this->view->views_today = $this->model->getTodayViewsAmount();
        // $this->view->views_week = $this->model->getWeekViewsAmount();
        // $this->view->views_last_week = $this->model->getWeekViewsAmount(1);

        $this->view->cities_stat = $this->model->getCitiesStat();
        // $this->view->not_moderated = $this->objectModel->getNotModeratedObjects();
        
        // $this->view->orders = $this->model->getOrdersStat(array(date('Y-m-d', strtotime('-1 week')), date("Y-m-d")));
        $this->view->cities = $this->objectModel->getCities();

        // Get Log Top
        $this->view->logLegend = array(
            'map' => 'Карта',    
            'preview' => 'Сладер об-та',    
            'view' => 'Просмотр об-та',    
            'showtel' => 'Показ контактов',    
            'sendorder' => 'Отправка заявки',    
            'add' => 'Новый об-т',    
            'update' => 'Редакт-ие об-та',    
            'dashboard' => 'ЛК. Главная',    
            'rooms' => 'ЛК. Список об-тов',    
        );

        /*
        -- map, preview, view, showtel, callback, sendorder, login, reguser, dashboard, rooms, update-view, update, add, calendar, addcalevent, markedoutobjects
        -- showtelext, showtelextview, sendringquery, savedemail
         */

        // $log = $this->modelLog->getTop(50, array('map', 'preview', 'view', 'showtel', 'sendorder', 'add', 'update', 'dashboard', 'rooms' ));
        // $this->view->logTop = $log;

        // $this->view->logCustomStat = $this->model->getLogStatByCity(array('login'), array('2015-10-01', '2015-10-07'));
        // $this->view->loginsStat = $this->model->getLoginsStat(array('2015-10-29', '2015-11-05'));
        // $this->view->newobjsStat = $this->model->getNewObjectsStat(array('2015-10-29', '2015-11-05'));
        // $this->view->newusersStat = $this->model->getNewUsersStat(array('2015-10-29', '2015-11-05'));

        //$this->view->layout()->breadcrumb = $this->view->partial('index/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( 'index/subnavbar.phtml');
        //$this->view->headScript()->prependFile('/js/test.js');

   }

}


