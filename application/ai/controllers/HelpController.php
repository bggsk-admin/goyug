<?php
    require_once("Util.php");

    class HelpController extends Zend_Controller_Action
    {

        public function init()
        {
            $this->ucid = $this->view->ucid = "help";
            $this->view->name = "Справка";  

            $this->apiauth  = Util::getConfig(array('apiauth'));
            $this->apikey   = Util::getConfig(array('apikey'));
        }

        public function indexAction()
        {
            if( $this->apiauth )
            {
                $tmstmp = time();
                $rnd = rand();

                $this->view->authurl = "/tmstmp/".$tmstmp."/rnd/".$rnd."/sig/" . md5( $tmstmp . $rnd . $this->apikey );
            } else {
                $this->view->authurl = "";    
            }

        }

}