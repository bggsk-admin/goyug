<?php
class ResetController extends Zend_Controller_Action
{

    public function init()
    {
        $this->ucid = $this->view->ucid = "reset";
        $this->model = new Ai_Model_Reset();
    }

    public function indexAction()
    {
    }

    public function resetallAction()
    {
        $tables = array(
            array("name" => _('Log'),           "uid" => "l_log",           "pid" => "l_id", "items" => 0),
        );

        foreach($tables as $id => $table)
        {
            $tables[$id]['items'] = $this->model->dropTable( $table['uid'], $table['pid'] );
        }
        $this->view->tables = $tables;
    }
}