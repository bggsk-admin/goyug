<?php
class ToolsController extends Zend_Controller_Action
{

    ##########################################################################################
    public function init()
    {
        $this->model = new Ai_Model_Tools();
        $this->log = new Ai_Model_Log();
        $this->ucid     = "tools";

        $ajaxContext = $this->_helper->getHelper('AjaxContext');

        $ajaxContext->addActionContext('ajax', 'json')
        ->addActionContext('ajaxRebuild', 'json')
        ->addActionContext('ajaxOntopDummy', 'json')
        ->initContext('json');

        $this->backurl = $this->view->url( array('action' => 'index') );
    }

    ##########################################################################################
    public function indexAction()
    {
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
    }

    ##########################################################################################
    public function importemailsAction()
    {
        $items = 0;

        if($this->_request->isPost())
        {
            $post = $this->_request->getPost();
            $items = $this->model->importEmails($post);
        }
        $this->view->items = $items;

        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
    }

    ##########################################################################################
    public function delobjectsAction()
    {
        $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
        $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');
    }

    ##########################################################################################
    public function ajaxAction ()
    {
        $method = $this->_request->getParam('method');
        $params = $_REQUEST;

        switch ($method) {

            case 'set_floors':
                $this->ajaxSetFloors();
                break;

            case 'clear_images':
                $this->ajaxClearImages();
                break;

            case 'rebuild_geo_json':
                $this->ajaxRebuildGeoJson();
                break;

            case 'default_owners_passwords':
                $this->ajaxDefOwnersPasswords();
                break;

            case 'find_unlinked_objdirs':
                $this->ajaxFindUnlinkedObjDirs();
                break;

            case 'find_obj_no_imgdir':
                $this->ajaxFindObjectsNoImgDir();
                break;

            case 'find_unlinked_imgfiles':
                $this->ajaxFindUnlinkedImgFiles();
                break;

            case 'refresh_cities_stat':
                $this->ajaxRefreshCitiesStat();
                break;

            case 'refresh_stat':
                $this->ajaxRefreshStat();
                break;

            case 'mark_random_objects':
                $this->ajaxMarkRandomObjects();
                break;

            case 'ontop_dummy':
                $this->ajaxOntopDummy();
                break;

            case 'rename_images':
                $this->ajaxRenameImages();
                break;

            case 'find_obj_wo_owners':
                $this->ajaxFindObjectsWithoutOwners();
                break;




        }

    }


    ##########################################################################################
    function ajaxSetFloors()
    {
        $response = $this->model->setFloors();
        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxClearImages()
    {
        $response = $this->model->clearImages();
        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxRebuildGeoJson()
    {
        $response = $this->model->rebuildGeoJson();
        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxRefreshCitiesStat()
    {
        $rows = $this->model->refreshCitiesStat();

        $total_rows = count($rows);
        $response = "Всего городов обработано: " . $total_rows . "<br>-------------------<br>";

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxRefreshStat()
    {
        $res = $this->model->refreshStat();

        $response = $res;

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxDefOwnersPasswords()
    {
        $response = $this->model->defOwnersPasswords();
        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxFindUnlinkedObjDirs()
    {
        $response = "";
        $dirs = $this->model->findUnlinkedObjDirs();

        $total_dirs = count($dirs);
        $response = "Всего директорий к удалению: " . count($dirs) . "<br>-------------------<br>";

        foreach($dirs as $id => $dir)
        {
            $response .= $dir . "<br>";
        }

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxFindObjectsNoImgDir()
    {
        $response = "";
        $objs = $this->model->findObjectsNoImgDir();

        $total_objs = count($objs);
        $response = "Всего объектов без папок: " . count($objs) . "<br>-------------------<br>";

        $response .=  implode(",", $objs);

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxFindUnlinkedImgFiles()
    {
        $response = "";
        $files = $this->model->findUnlinkedImgFiles();

        $total_files = count($files);
        $response = "Всего записей без файлов: " . count($files) . "<br>-------------------<br>";

        $ids = array();
        foreach($files as $id => $val)
        {
            $response .= "/upload/object/" . $val['uid'] . "/" . $val['name'] . "<br>";
            $ids[] = $val['id'];
        }

        $ids = implode(",", $ids);

        $response .= "<br>-------------------<br>IDs for DELETE: " . $ids;

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxFindObjectsWithoutOwners()
    {
        $response = "";
        $rows = $this->model->findObjectsWithoutOwners();

        $total_rows = count($rows);
        $response = "Всего записей без владельцев: " . count($rows) . "<br>-------------------<br>";

        $ids = array();
        foreach($rows as $id => $val)
        {
            $ids[] = $val['obj_id'];
        }

        $ids = implode(",", $ids);

        // print_r($ids); die;

        $response .= "<br>-------------------<br>IDs: " . $ids;

        $this->view->response = $response;
    }

    ##########################################################################################
    function ajaxMarkRandomObjects()
    {
        $response = "";
        $rows = $this->model->markRandomObjects();

        $total_rows = count($rows);
        $response = "Всего объектов помечено: " . count($rows) . "<br>-------------------<br>";

        $this->view->response = $response;

    }

    ##########################################################################################
    function ajaxOntopDummy()
    {
        $result = $this->model->ontopDummy();

        $this->view->response = $result;

    }

    ##########################################################################################
    function ajaxRenameImages()
    {
        $response = "";
        $rows = $this->model->renameImages();

        $total_rows = count($rows);
        $response = "Всего фоток обработано: " . $rows . "<br>-------------------<br>";

        $this->view->response = $response;

    }


}