<?
    class ParserController extends Zend_Controller_Action
    {

        ##########################################################################################
        public function init()
        {
            $this->ucid = $this->view->ucid = "parser";
            $this->view->name = "Парсер Avito";
            $this->model = new Ai_Model_Parser();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');

            $ajaxContext->addActionContext('ajax', 'json')
            ->addActionContext('ajaxStartParse', 'json')
            ->addActionContext('ajaxStartDownload', 'json')
            ->addActionContext('ajaxCheckParse', 'json')
            ->addActionContext('ajaxGetToken', 'json')
            ->initContext('json');


        }

        ##########################################################################################
        public function indexAction()
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            if($this->_request->isPost())
            {
                $pars_city = $this->view->city = $this->_request->getParam('pars_city');
                $pars_page = $this->view->page = $this->_request->getParam('pars_page');

                if(!empty($pars_city) && !empty($pars_page))
                {
                    $objects_num = $this->model->countObjects($pars_city);
                    $pages_num = $this->model->countPages($objects_num);

                    $job = $this->model->createParseJob($pars_city, $objects_num, $pages_num, $pars_page, $task_token);

                    //******************** LIMIT PAGSES for test (1 current page)
                    $pages_num = $pars_page;

                    for($p = $pars_page; $p <= $pages_num; $p++)
                    {
                        $objects_list = $this->model->getObjList($pars_city, $p, $task_token);    
                        if($objects_list > 0)
                        {
                            $this->model->getObjDetails($objects_list, $pars_city, $task_token);
                        }
                    }

                    $this->view->pages_num = $pages_num;
                    $this->view->objects_num = $objects_num;

                    $this->view->job_stat = $this->model->getParseJob($task_token);
                }

            }

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function downloadimgsAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function parseobjectsAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function parsepayersAction()
        {
            set_time_limit(0);

            $this->view->layout()->breadcrumb = $this->view->partial( $this->ucid. '/breadcrumb.phtml');
            $this->view->layout()->subnavbar = $this->view->partial( $this->ucid. '/subnavbar.phtml');

        }

        ##########################################################################################
        public function ajaxAction ()
        {
            $method = $this->_request->getParam('method');
            $params = $_REQUEST;

            switch ($method) {
                case 'startparse':
                    $this->ajaxStartParse($params);
                    break;

                case 'parseobjects':
                    $this->ajaxParseObjects($params);
                    break;

                case 'parsepayers':
                    $this->ajaxParsePayers($params);
                    break;

                case 'startdownload':
                    $this->ajaxStartDownload($params);
                    break;

                case 'checkparse':
                    $this->ajaxCheckParse($params);
                    break;

                case 'gettoken':
                    $this->ajaxGetToken();
                    break;
            }

        }



        ##########################################################################################
        public function ajaxStartParse($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];
            $pars_page = $params['pars_page'];

            if(!empty($pars_city) && !empty($pars_page))
            {
                $parse_params = $this->model->getParseParams($pars_city);
                $objects_num = $parse_params['objects'];
                $pages_num = $parse_params['pages'];

                $job = $this->model->createParseJob($pars_city, $objects_num, $pages_num, $pars_page, $task_token, "parse");

                for($p = $pars_page; $p <= $pages_num; $p++)
                {
                    $objects_list = $this->model->getObjectsList($pars_city, $p, $task_token);    

                    if($objects_list > 0)
                    {
                        $objects = $this->model->getObjectsDetails($objects_list, $pars_city, $task_token);

                    }

                    // print_r($objects);
                    // die;
                }

                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxParseObjects($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $parse_params = $this->model->getParseParams($pars_city);
                $objects_num = $parse_params['objects'];
                $pages_num = $parse_params['pages'];

                $job = $this->model->createParseJob($pars_city, $objects_num, $pages_num, 1, $task_token, "parseobjects");

                for($p = 1; $p <= $pages_num; $p++)
                {
                    $objects_list = $this->model->getObjectsList($pars_city, $p, $task_token);    
                }

                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxParsePayers($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $payers = $this->model->getPayers($pars_city);
                $job = $this->model->createParseJob($pars_city, count($payers), 1, 1, $task_token, "parsepayers");

                $result = $this->model->parsePayers($pars_city, $payers, $task_token);    

                $job_stat = $this->model->getParseJob($task_token);
            }

            if(isset($result['status']) && $result['status'] == "error")
                $this->view->response = $result;
            else
            {
                $this->view->response = $job_stat;
            }
        }

        ##########################################################################################
        public function ajaxStartDownload($params)
        {
            set_time_limit(0);

            $task_token = $this->model->generateToken();

            $pars_city = $params['pars_city'];

            if(!empty($pars_city))
            {
                $items = $this->model->countDownloadItems($pars_city);

                // print_r($items);die;
                
                $objects_num = $items['images'];

                $job = $this->model->createDownloadJob($pars_city, $objects_num, $task_token);
                $job_stat = $this->model->download($pars_city, $items, $task_token);

                $job_stat = $this->model->getParseJob($task_token);
            }

            $this->view->response = $job_stat;
        }

        ##########################################################################################
        public function ajaxCheckParse($params)
        {
            $this->view->status = $this->model->checkParseJob($params['token']);
        }

        ##########################################################################################
        public function ajaxGetToken()
        {
            $this->view->token = $this->model->generateToken();
        }


}