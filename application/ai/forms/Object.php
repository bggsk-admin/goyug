<?
    class Ai_Form_Object extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $model = new Ai_Model_Object();

            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'obj_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'obj_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('checkbox',   'obj_virtual',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('checkbox',   'obj_moderated',
                array(
                    'label' => 'Отмодерирован',
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'obj_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'obj_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_uid', 'placeholder' => $this->getView()->translate('UID'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'obj_rating',
                array(
                    'label' => $this->getView()->translate('Rating'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_rating', 'placeholder' => $this->getView()->translate('Rating')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_sqr',
                array(
                    'label' => $this->getView()->translate('Square'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_sqr', 'placeholder' => $this->getView()->translate('Square')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_checkin',
                array(
                    'label' => $this->getView()->translate('Checkin'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_checkin', 'placeholder' => $this->getView()->translate('Checkin')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_checkout',
                array(
                    'label' => $this->getView()->translate('Checkout'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_checkout', 'placeholder' => $this->getView()->translate('Checkout')),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $ot_object_type = $model->getMultiselect('ot_object_type', 'ot' );
            $this->addElement(
                'multiselect',
                'ot_object_type',
                array(
                    'label' => $this->getView()->translate('Object type'),
                    'required' => false,
                    'multiOptions' => $ot_object_type,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one or more'), 'multiple' => 'multiple'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );

            $sf_smartfilters = $model->getSmartfiltersMultiselect();

            $this->addElement(
                'multiselect',
                'sf_smartfilters',
                array(
                    'label' => $this->getView()->translate('Smartfilter'),
                    'required' => false,
                    'multiOptions' => $sf_smartfilters,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one or more'), 'multiple' => 'multiple'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );

            /*
            $rt_rent_type = $model->getMultiselect('rt_rent_type', 'rt');
            $this->addElement(
            'multiselect',
            'rt_rent_type',
            array(
            'label' => $this->getView()->translate('Rent type'),
            'required' => false,
            'multiOptions' => $rt_rent_type,
            'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one or more'), 'multiple' => 'multiple'),
            'errorMessages' => array(''),
            'decorators' => array('HorizontalForm')
            )
            );
            */
           

            $owners = $model->getOwners();
            $this->addElement(
                'select',
                'obj_u_id',
                array(
                    'label' => $this->getView()->translate('Owner'),
                    'required' => false,
                    'multiOptions' => $owners,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => $this->getView()->translate('Select one or more'), 'data-allowClear' => 'true'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                )
            );


            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'obj_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'obj_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'obj_shortdesc_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object short description'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'obj_shortdesc_' . $lang, 'placeholder' => '', 'rows' => 3 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'obj_fulldesc_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object full description'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'obj_fulldesc_' . $lang, 'placeholder' => '', 'rows' => 8 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }

            //------------------------------------------------------------------ Цена
            $per_period = $model->getMultiselect('per_period', 'per');
            $acc_accommodation = $model->getMultiselect('acc_accommodation', 'acc');

            $this->addElement('select', 'obj_price_period',
                array(
                    'label' => $this->getView()->translate('Period type'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control select2', 'id' => 'obj_price_period', 'data-placeholder' => 'Select period type' ),
                    'multiOptions' => $per_period,
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('select', 'obj_acc_type',
                array(
                    'label' => $this->getView()->translate('Accommodation type'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control select2', 'id' => 'obj_acc_type', 'data-placeholder' => 'Select accommodation type' ),
                    'multiOptions' => $acc_accommodation,
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('text', 'obj_price_minstay',
                array(
                    'label' => $this->getView()->translate('Minimal period'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_price_minstay', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('text', 'obj_price',
                array(
                    'label' => $this->getView()->translate('Price'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_price', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('text', 'obj_deposit',
                array(
                    'label' => $this->getView()->translate('Deposit'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_deposit', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));



/*            $per_period = $model->getMultiselect('per_period', 'per');

            $this->addElement('select', 'per_id',
                array(
                    'label' => $this->getView()->translate('Period type'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control select2', 'id' => 'per_id', 'data-placeholder' => 'Select period type' ),
                    'multiOptions' => $per_period,
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('text', 'prc_min_period',
                array(
                    'label' => $this->getView()->translate('Minimal period'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'prc_min_period', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            $this->addElement('text', 'prc_price',
                array(
                    'label' => $this->getView()->translate('Price'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'prc_price', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

*/

            //------------------------------------------------------------------ Количество гостей
            $this->addElement('text', 'obj_guests',
                array(
                    'label' => $this->getView()->translate('Guests'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_guests', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Количество комнат
            $this->addElement('text', 'obj_rooms',
                array(
                    'label' => $this->getView()->translate('Rooms'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_rooms', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Количество спален
            $this->addElement('text', 'obj_bedrooms',
                array(
                    'label' => $this->getView()->translate('Bedrooms'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_bedrooms', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Количество одинарных кроватей
            $this->addElement('text', 'obj_beds_sngl',
                array(
                    'label' => $this->getView()->translate('Beds single'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_beds_sngl', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Количество двойных кроватей
            $this->addElement('text', 'obj_beds_dbl',
                array(
                    'label' => $this->getView()->translate('Beds double'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_beds_dbl', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Количество спальных мест
            $this->addElement('text', 'obj_sleepers',
                array(
                    'label' => $this->getView()->translate('Sleepers'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_sleepers', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));


            //------------------------------------------------------------------ Количество ванных комнат
            $this->addElement('text', 'obj_bathrooms',
                array(
                    'label' => $this->getView()->translate('Bathrooms'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'obj_bathrooms', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
                    'isArray' => true,
            ));

            //------------------------------------------------------------------ Дополнительные параметры
            $prm_parameter = $model->getParamsFields('prm_parameter', 'prm');

            foreach($prm_parameter as $prm_id => $prm_val)
            {
                $this->addElement('text', "prm_parameter_" . $prm_id,
                    array(
                        'label' => $prm_val['name'],
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'prm_parameter_' . $prm_id, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }

            //------------------------------------------------------------------ Услуги
            $srv_service = $model->getMultiselect('srv_service', 'srv');
            $this->addElement('multiCheckbox',   'srv_service',
                array(
                    'required' => false,
                    'attribs' => array('class' => 'pretty'),
                    'decorators' => array(
                        'Errors',
                        array('ViewScript', array('viewScript'=>'form/multicheckbox.php'))
                    ),
                    'multiOptions' => $srv_service
            ));

            ######################################################### Оборудование
            $eq_equipment = $model->getMultiselect('eq_equipment', 'eq');
            $this->addElement('multiCheckbox',   'eq_equipment',
                array(
                    'required' => false,
                    'attribs' => array('class' => 'pretty'),
                    'decorators' => array(
                        'Errors',
                        array('ViewScript', array('viewScript'=>'form/multicheckbox.php'))
                    ),
                    'multiOptions' => $eq_equipment
            ));

            ######################################################### Оборудование
            $at_amenity = $model->getMultiselect('at_amenity', 'at');
            $this->addElement('multiCheckbox',   'at_amenity',
                array(
                    'required' => false,
                    'attribs' => array('class' => 'pretty'),
                    'decorators' => array(
                        'Errors',
                        array('ViewScript', array('viewScript'=>'form/multicheckbox.php'))
                    ),
                    'multiOptions' => $at_amenity
            ));

            ######################################################### Ограничения
            $rst_restriction = $model->getMultiselect('rst_restriction', 'rst');
            $this->addElement('multiCheckbox',   'rst_restriction',
                array(
                    'required' => false,
                    'attribs' => array('class' => 'pretty'),
                    'decorators' => array(
                        'Errors',
                        array('ViewScript', array('viewScript'=>'form/multicheckbox.php'))
                    ),
                    'multiOptions' => $rst_restriction
            ));

            ######################################################### Адрес
            $this->addElement('text', 'obj_addr_country_code',
                array(
                    'label' => $this->getView()->translate('Code'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_country_code', 'placeholder' => $this->getView()->translate('Country code'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_country',
                array(
                    'label' => $this->getView()->translate('Country'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_country', 'placeholder' => $this->getView()->translate('Country'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_zip',
                array(
                    'label' => $this->getView()->translate('ZIP code'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_zip', 'placeholder' => $this->getView()->translate('ZIP code'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_city',
                array(
                    'label' => $this->getView()->translate('City'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_city', 'placeholder' => $this->getView()->translate('City'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));


            $this->addElement('text', 'obj_addr_area_1',
                array(
                    'label' => $this->getView()->translate('Area 1'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_area_1', 'placeholder' => $this->getView()->translate('Area 1'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_area_2',
                array(
                    'label' => $this->getView()->translate('Area 2'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_area_2', 'placeholder' => $this->getView()->translate('Area 2'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));




            $this->addElement('text', 'obj_addr_street',
                array(
                    'label' => $this->getView()->translate('Street'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_street', 'placeholder' => $this->getView()->translate('Street') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_number',
                array(
                    'label' => $this->getView()->translate('№'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_number', 'placeholder' => $this->getView()->translate('№') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_entrance',
                array(
                    'label' => $this->getView()->translate('Entrance'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_entrance', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_floor',
                array(
                    'label' => $this->getView()->translate('Floor'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_floor', 'placeholder' => $this->getView()->translate('Floor') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_room',
                array(
                    'label' => $this->getView()->translate('Room'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_room', 'placeholder' => $this->getView()->translate('Room') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_lon',
                array(
                    'label' => $this->getView()->translate('Longitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_lon', 'placeholder' => $this->getView()->translate('Longitude'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_lat',
                array(
                    'label' => $this->getView()->translate('Latitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_lat', 'placeholder' => $this->getView()->translate('Latitude'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_lon_view',
                array(
                    'label' => $this->getView()->translate('Longitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_lon_view', 'placeholder' => $this->getView()->translate('Longitude') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));

            $this->addElement('text', 'obj_addr_lat_view',
                array(
                    'label' => $this->getView()->translate('Latitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'obj_addr_lat_view', 'placeholder' => $this->getView()->translate('Latitude') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm'),
            ));



        } #func

    } #class
