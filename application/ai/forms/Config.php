<?
class Ai_Form_Config extends ZendX_JQuery_Form
{


    ##########################################################################################
    public function init()
    {
        
        $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
        $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

        $this->setMethod("post");
        
        // Item ID
        $this->addElement('hidden', 'c_id',     array( 'decorators' => array('ViewHelper')));

        $this->addElement('text',   'c_name',   array( 'label' => $this->getView()->translate('Key'), 'required' => true, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'span3', 'placeholder' => 'Key'), 'errorMessages' => array('Укажите имя настройки'), 'decorators' => array('HorizontalForm')  ));
        
        $this->addElement('text', 'c_value', array('label' => $this->getView()->translate('Value'), 'required' => true, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'span3', 'placeholder' => 'Value'), 'errorMessages' => array('Укажите значение настройки'), 'decorators' => array('HorizontalForm')  ));

        $this->addElement('textarea', 'c_desc', array( 'label' => $this->getView()->translate('Description'), 'required' => false, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'span6', 'placeholder' => 'Description', 'rows' => '3'), 'errorMessages' => array('Укажите описание настройки'), 'decorators' => array('HorizontalForm')  ));


    }
    
} #class