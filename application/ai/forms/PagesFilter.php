<?
class Ai_Form_PagesFilter extends Zend_Form
{

    ##########################################################################################
    public $buttonDeco = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public $tableDeco = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
    );

    ##########################################################################################
    public function init()
    {
        $this->setMethod("post");

        //Даты
        $this->addElement('text', 'f_date_from', array('label' => 'c: ', 'required' => false, 'attribs' => array('class' => ' datepicker'), 'decorators' => $this->tableDeco  ));
        $this->addElement('text', 'f_date_to', array('label' => 'по:', 'required' => false, 'attribs' => array('class' => 'datepicker'), 'decorators' => $this->tableDeco  ));
        $this->addDisplayGroup( array('f_date_from', 'f_date_to'), 'group1', array( 'legend' => 'Дата публикации' ) );

        //Остальное
        $this->addElement('text', 'f_p_title', array('label' => 'Заголовок', 'required' => false, 'attribs' => array('class' => 'normal')  ));
        $this->addElement('text', 'f_p_uid', array('label' => 'Идентификатор', 'required' => false, 'attribs' => array('class' => 'short')  ));
        $this->addDisplayGroup( array('f_p_uid', 'f_p_title'), 'group2', array( 'legend' => 'Остальное' ) );
        
        //Сортировка
        $sort_field = array( 
            ''          =>  '', 
            'p_date'    =>  'Дата публикации', 
            'p_title'   =>  'Заголовок',
            'p_uid'     =>  'Идентификатор',
        );
        $this->addElement('select', 'f_sort_field', array('label' => 'Поле: ', 'multiOptions' => $sort_field, 'value' => '0', 'required' => false, 'attribs' => array('class' => 'short'), 'decorators' => $this->tableDeco  ));
        $sort_type = array( '' => '', 'asc' => 'по возрастсанию', 'desc' => 'по убыванию');
        $this->addElement('select', 'f_sort_type', array('label' => 'направление: ', 'multiOptions' => $sort_type, 'value' => '0', 'required' => false, 'attribs' => array('class' => 'short'), 'decorators' => $this->tableDeco  ));
        $this->addDisplayGroup( array('f_sort_field', 'f_sort_type'), 'group3', array( 'legend' => 'Сортировка' ) );
        
        $submit = $this->addElement('submit', 'submit', array('label' => 'Применить', 'decorators' => $this->buttonDeco ));
    }
    
} #class
