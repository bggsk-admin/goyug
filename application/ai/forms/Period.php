<?
    class Ai_Form_Period extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'per_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('text', 'per_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'per_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'per_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'per_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'per_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Period name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'per_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
                
                $this->addElement('text', 'per_pricename_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Period pricename'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'per_pricename_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }

            $this->addElement('text', 'per_format',
                array(
                    'label' => $this->getView()->translate('Period format'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'per_format', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


        }

    } #class
