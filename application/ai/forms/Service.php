<?
    class Ai_Form_Service extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'srv_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'srv_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'srv_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'srv_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'srv_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'srv_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'srv_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'srv_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'srv_description_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Amenity type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'srv_description_' . $lang, 'placeholder' => '', 'rows' => 3 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
