<?
    class Ai_Form_City extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $ct_model = new Ai_Model_City();

            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'ct_id',       array('decorators' => array('ViewHelper')) );

            $ct_pid = $ct_model->getSelectCtgTree();
            $this->addElement(
                'select',
                'ct_pid',
                array(
                    'label' => $this->getView()->translate('Parent'),
                    'required' => false,
                    'multiOptions' => $ct_pid,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );

            $this->addElement('checkbox',   'ct_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('checkbox',   'ct_top',
                array(
                    'label' => $this->getView()->translate('Top'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('checkbox',   'ct_mainmap',
                array(
                    'label' => $this->getView()->translate('On main map'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'ct_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('City'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'ct_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

            }

            $this->addElement('text', 'ct_lat',
                array(
                    'label' => $this->getView()->translate('Latitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_lat', 'placeholder' => $this->getView()->translate('Latitude') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_lng',
                array(
                    'label' => $this->getView()->translate('Longitude'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_lng', 'placeholder' => $this->getView()->translate('Longitude') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_minprice',
                array(
                    'label' => $this->getView()->translate('Min price'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_minprice', 'placeholder' => $this->getView()->translate('Min price'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_maxprice',
                array(
                    'label' => $this->getView()->translate('Max price'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_maxprice', 'placeholder' => $this->getView()->translate('Max price'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'ct_objects',
                array(
                    'label' => $this->getView()->translate('Objects'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'ct_objects', 'placeholder' => $this->getView()->translate('Objects'), 'readonly' => 'readonly' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


        }

    } #class
