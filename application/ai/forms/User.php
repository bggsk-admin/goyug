<?
    class Ai_Form_User extends Zend_Form
    {
        
        ##########################################################################################
        public function init()
        {
            $model = new Ai_Model_User();
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'u_id',       array('decorators' => array('ViewHelper')) );


            /***************************************************
            Main Info
            ****************************************************/
            $roles = array(
                'customer'             => $this->getView()->translate('Customer'),
                'owner'                 => $this->getView()->translate('Owner'),
                'editor'                => $this->getView()->translate('Editor'),
                'manager'                => $this->getView()->translate('Manager'),
                'administrator'     => $this->getView()->translate('Administrator')
            );

            $this->addElement(
                'select',
                'u_role',
                array(
                    'label' => $this->getView()->translate('Role'),
                    'required' => false,
                    'multiOptions' => $roles,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );

            $this->addElement('checkbox',   'u_active',
                array(
                    'label' => $this->getView()->translate('Enable'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));


            /***************************************************
            User Info
            ****************************************************/
            $this->addElement('text', 'u_firstname',
                array(
                    'label' => $this->getView()->translate('Firstname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_firstname', 'placeholder' => $this->getView()->translate('Firstname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_lastname',
                array(
                    'label' => $this->getView()->translate('Lastname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_lastname', 'placeholder' => $this->getView()->translate('Lastname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_patronomyc',
                array(
                    'label' => $this->getView()->translate('Patronomyc'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_patronomyc', 'placeholder' => $this->getView()->translate('Patronomyc') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


            /***************************************************
            Legal Info
            ****************************************************/
            $this->addElement('text', 'u_inn',
                array(
                    'label' => $this->getView()->translate('INN'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_inn', 'placeholder' => $this->getView()->translate('INN') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $legalforms = $model->getMultiselect('lf_legalform', 'lf');

            $this->addElement(
                'select',
                'u_lf_id',
                array(
                    'label' => $this->getView()->translate('Legal form'),
                    'required' => false,
                    'multiOptions' => $legalforms,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );



            /***************************************************
            Account Info
            ****************************************************/

            $this->addElement('text', 'u_username',
                array(
                    'label' => $this->getView()->translate('Username'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_username', 'placeholder' => $this->getView()->translate('Username') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('password', 'u_password',
                array(
                    'label' => $this->getView()->translate('Password'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_password', 'placeholder' => $this->getView()->translate('Password') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_password_open',
                array(
                    'label' => $this->getView()->translate('Password'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_password_open', 'placeholder' => $this->getView()->translate('Password') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_email',
                array(
                    'label' => $this->getView()->translate('Email'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_email', 'placeholder' => $this->getView()->translate('Email') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_inn',
                array(
                    'label' => $this->getView()->translate('INN'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_inn', 'placeholder' => $this->getView()->translate('INN') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_company',
                array(
                    'label' => $this->getView()->translate('Company name'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_company', 'placeholder' => $this->getView()->translate('Company name') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_1',
                array(
                    'label' => $this->getView()->translate('Phone') . "-1",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_1', 'placeholder' => $this->getView()->translate('Phone') . "-1" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_2',
                array(
                    'label' => $this->getView()->translate('Phone') . "-2",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_2', 'placeholder' => $this->getView()->translate('Phone') . "-2" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_3',
                array(
                    'label' => $this->getView()->translate('Phone') . "-3",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_3', 'placeholder' => $this->getView()->translate('Phone') . "-3" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


    	}
    	
    } #class
?>