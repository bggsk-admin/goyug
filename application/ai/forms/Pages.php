<?
class Ai_Form_Pages extends Zend_Form
{
    
    public $checkboxDecorator = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'li', 'class' => '')),
        array('Label', array('tag' => 'li', 'placement' => 'append')),
        array(array('row' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'checkbox'))
    );

    public $shorttextDecorator = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'li', 'class' => 'data')),
        array('Label', array('tag' => 'li', 'class' => '', 'placement' => 'append')),
        array(array('row' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'shorttext'))
    );

    ##########################################################################################
    public function init()
    {
        $this->setMethod("post");
        
        $this->addElement('hidden', 'p_id', array( 'decorators' => array('ViewHelper')));
        
        $this->addElement('checkbox',   'p_public', array('label' => 'Опубликовано', 'required' => false, 'attribs' => array(), 'decorators' => $this->checkboxDecorator  ));
        $this->addElement('text',       'p_date',   array('label' => 'Дата публикации: ', 'required' => false, 'attribs' => array('class' => 'short datepicker')  ));
        $this->addElement('text',       'p_uid',    array('label' => 'Идентификатор: ', 'required' => true, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'short'), 'errorMessages' => array('Укажите идентификатор')  ));
        $this->addElement('text',       'p_title',  array('label' => 'Заголовок: ', 'required' => true, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'xlong'), 'errorMessages' => array('Укажите заголовок')  ));
        $this->addElement('textarea',   'p_short',  array( 'label' => 'Краткое содержимое: ', 'required' => false, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'xlong h100')  ));
        $this->addElement('textarea',   'p_full',   array( 'label' => 'Полное содержимое: ', 'required' => false, 'filters' => array('StringTrim'), 'attribs' => array('class' => 'xlong h100')  ));
        $this->addElement('file',       'p_titlepic', array( 'label' => 'Заглавное изображение: ', 'validators' => array( array('Size', false, 5242880) ), 'required' => false, 'attribs' => array('class' => 'xlong')  ));

        $submit = $this->addElement('submit', 'submit', array('label' => 'Сохранить запись', 'attribs' => array('class' => 'btn btn-large')) );

    }
    
} #class