<?
    class Ai_Form_Owner extends Zend_Form
    {
        
        ##########################################################################################
        public function init()
        {
            $model = new Ai_Model_User();
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'u_id',       array('decorators' => array('ViewHelper')) );


            /***************************************************
            Main Info
            ****************************************************/
            $this->addElement('checkbox',   'u_active',
                array(
                    'label' => $this->getView()->translate('Enable'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));


            /***************************************************
            User Info
            ****************************************************/
            $this->addElement('text', 'u_firstname',
                array(
                    'label' => $this->getView()->translate('Firstname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_firstname', 'placeholder' => $this->getView()->translate('Firstname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_lastname',
                array(
                    'label' => $this->getView()->translate('Lastname'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_lastname', 'placeholder' => $this->getView()->translate('Lastname') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_patronomyc',
                array(
                    'label' => $this->getView()->translate('Patronomyc'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_patronomyc', 'placeholder' => $this->getView()->translate('Patronomyc') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


            /***************************************************
            Legal Info
            ****************************************************/
            $this->addElement('text', 'u_inn',
                array(
                    'label' => $this->getView()->translate('INN'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_inn', 'placeholder' => $this->getView()->translate('INN') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $legalforms = $model->getMultiselect('lf_legalform', 'lf');

            $this->addElement(
                'select',
                'u_lf_id',
                array(
                    'label' => $this->getView()->translate('Legal form'),
                    'required' => false,
                    'multiOptions' => $legalforms,
                    'attribs' => array('class' => 'form-control select2', 'data-placeholder' => '...'),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
                )
            );



            /***************************************************
            Account Info
            ****************************************************/

            $this->addElement('text', 'u_email',
                array(
                    'label' => $this->getView()->translate('Email'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_email', 'placeholder' => $this->getView()->translate('Email') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_url',
                array(
                    'label' => $this->getView()->translate('Website link'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_url', 'placeholder' => $this->getView()->translate('Website link') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_inn',
                array(
                    'label' => $this->getView()->translate('INN'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_inn', 'placeholder' => $this->getView()->translate('INN') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_company',
                array(
                    'label' => $this->getView()->translate('Company name'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_company', 'placeholder' => $this->getView()->translate('Company name') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_1',
                array(
                    'label' => $this->getView()->translate('Phone') . "-1",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_1', 'placeholder' => $this->getView()->translate('Phone') . "-1" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_2',
                array(
                    'label' => $this->getView()->translate('Phone') . "-2",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_2', 'placeholder' => $this->getView()->translate('Phone') . "-2" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'u_phone_3',
                array(
                    'label' => $this->getView()->translate('Phone') . "-3",
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'u_phone_3', 'placeholder' => $this->getView()->translate('Phone') . "-3" ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));


            /***************************************************
            Additional Info
            ****************************************************/
            $this->addElement('checkbox',   'u_is_phoned',
                array(
                    'label' => $this->getView()->translate('Is Phoned'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('textarea', 'u_notes',
                array(
                    'label' => $this->getView()->translate('Notes'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'u_notes', 'placeholder' => '', 'rows' => 6 ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));



        }
        
    } #class
?>