<?
    class Ai_Form_Equipment extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'eq_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'eq_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'eq_order',
                array(
                    'label' => $this->getView()->translate('Order'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'eq_order', 'placeholder' => $this->getView()->translate('Order') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'eq_uid',
                array(
                    'label' => $this->getView()->translate('UID'),
                    'required' => false,
                    'attribs' => array('class' => array('form-control'), 'id' => 'eq_uid', 'placeholder' => $this->getView()->translate('UID') ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            foreach($locale->locales as $lang => $val)
            {
                $this->addElement('text', 'eq_name_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Object type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'eq_name_' . $lang, 'placeholder' => '' ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));

                $this->addElement('textarea', 'eq_description_' . $lang,
                    array(
                        'label' => $this->getView()->translate('Amenity type name'),
                        'required' => false,
                        'attribs' => array('class' => 'form-control', 'id' => 'eq_description_' . $lang, 'placeholder' => '', 'rows' => 3 ),
                        'errorMessages' => array(''),
                        'decorators' => array('HorizontalForm')
                ));
            }



        }

    } #class
