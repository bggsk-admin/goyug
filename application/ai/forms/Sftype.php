<?
    class Ai_Form_Sftype extends Zend_Form
    {

        ##########################################################################################
        public function init()
        {
            $locale  = new Zend_Session_Namespace('locale');

            $this->addPrefixPath( 'Ai_Form_Decorator', 'Ai/Form/Decorator', 'decorator' );
            $this->setDecorators( array('FormElements', array('HorizontalForm'), 'Form' ));

            $this->setMethod("post");

            $this->addElement('hidden',     'sft_id',       array('decorators' => array('ViewHelper')) );

            $this->addElement('checkbox',   'sft_enable',
                array(
                    'label' => $this->getView()->translate('Public'),
                    'required' => false,
                    'attribs' => array('class' => ''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sft_name',
                array(
                    'label' => $this->getView()->translate('Type name'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'sft_name', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sft_cssclass',
                array(
                    'label' => $this->getView()->translate('CSS class prefix'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'sft_cssclass', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sft_width',
                array(
                    'label' => $this->getView()->translate('Width'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'sft_width', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));

            $this->addElement('text', 'sft_height',
                array(
                    'label' => $this->getView()->translate('Height'),
                    'required' => false,
                    'attribs' => array('class' => 'form-control', 'id' => 'sft_name', 'placeholder' => '' ),
                    'errorMessages' => array(''),
                    'decorators' => array('HorizontalForm')
            ));



        }

    } #class
